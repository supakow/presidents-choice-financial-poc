#!/bin/bash

function new_tab() {
  TAB_NAME=$1
  COMMAND=$2
  osascript \
    -e "tell application \"Terminal\"" \
    -e "tell application \"System Events\" to keystroke \"t\" using {command down}" \
    -e "do script \"printf '\\\e]1;$TAB_NAME\\\a'; $COMMAND\" in front window" \
    -e "end tell" > /dev/null
}

# content services
new_tab "Content Services" "cd launchpad-webapps/contentservices; ./run-cs.sh"

# portalserver
new_tab "Portal Server" "cd launchpad-webapps/portalserver; ./run-portal.sh"

# theme
new_tab "Theme" "cd launchpad-webapps/theme-webapp; ./run-theme.sh"