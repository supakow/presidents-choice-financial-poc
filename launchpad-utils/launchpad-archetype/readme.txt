for released versions:

$ mvn archetype:generate -DarchetypeGroupId=com.backbase.launchpad -DarchetypeArtifactId=launchpad-archetype -DarchetypeVersion=<released-version>

for SNAPSHOT versions:
- currently you need to have a launchpad-trunk checked out, we do not upload SNAPSHOT to artifactory
- inside launchpad-trunk/launchpad-archetype
  $ mvn clean install -- this way you have the archetype locally in your repo
- now you can generate a project with the archetype with:
  $ mvn archetype:generate -DarchetypeCatalog=local -DarchetypeGroupId=com.backbase.launchpad -DarchetypeArtifactId=launchpad-archetype -DarchetypeVersion=<snapshot-version>

Tips to speed things up:
- In order to skip the archetypeGroup, archetypeArtifactId, version parameters you can do the following:

  cd launchpad-trunk/launchpad-archetype
  mvn install archetype:update-local-catalog

  This will register the archetype inside your .m2/archetype-catalog.xml as
  <archetype>
    <groupId>com.backbase.launchpad</groupId>
    <artifactId>launchpad-archetype</artifactId>
    <version>0.10.0-SNAPSHOT</version>
    <description>Launchpad Archetype</description>
  </archetype>

  This step you just need to do once per version.

- Then in order to generate a project you can simply use
  $ mvn archetype:generate -DarchetypeCatalog=local
  and select interactively the archetype
