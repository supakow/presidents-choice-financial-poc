/*globals portalRestClient console angular */

(function($) {

	"use strict";

	$(document).ajaxSend(function() {
		$("body").addClass("waiting");
	});

	$(document).ajaxComplete(function() {
		$("body").removeClass("waiting");
	});

	var postUri = "/services/rest/contact-dialogues/messages/new";


	var migrate = angular.module("lpNotifications", []);

	migrate.controller("AddNotificationController", [ "$scope", "$http", function($scope, $http) {

		$scope.error =  null;
		$scope.success = null;

		$scope.contextPath = "";
		$scope.setDefaultContextPath = function() {
			$scope.contextPath = "/portalserver";
		};


		var getDefaultModel = function() {
			return  {
				id: Math.random() * 10,
				message: "",
				closable: true,
				expiresOn: (1000 * 60),
				level: "INFO",
				link_rel : "",
				link_uri : ""
			};
		};

		$scope.model = getDefaultModel();

		$scope.send = function() {

			$scope.model.expiresOn = parseInt($scope.model.expiresOn, 10) + new Date().getTime();
			$scope.model["link.rel"] = $scope.model.link_rel;
			$scope.model["link.uri"] = $scope.model.link_uri;
			$http.post($scope.contextPath + postUri, $.param($scope.model), {
				headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/x-www-form-urlencoded;'
				}
			}).success(function( response ) {
				$scope.error = null;
				$scope.success = "Message sent";
				$scope.model = getDefaultModel();
			}).error(function(response) {
				$scope.error = "An error occured, see console for details";
			});

		};
	}]);
})(jQuery);