/*globals portalRestClient console angular */

(function($, portalRestClient) {

	"use strict";

	$(document).ajaxSend(function() {
		$("body").addClass("waiting");
	});

	$(document).ajaxComplete(function() {
		$("body").removeClass("waiting");
	});


	var migrate = angular.module("lpMigrate", []);

	migrate.controller("CatalogFindReplaceController", function($scope) {

		var CatalogPropertyFindReplace = function() {
			this.contextPath = "/portalserver";
			this.contextItemName = "";
			this.properties = [];
			this.report = "";
			this.error = "";
			this.updateSuccessCount = 0;
			this.updateFailCount = 0;
		};
		CatalogPropertyFindReplace.prototype.addEmptyProperty = function() {
			this.properties.push({
				name: "",
				find: "",
				replace: ""
			});
		};
		CatalogPropertyFindReplace.prototype.removeProperty = function(index) {
			this.properties.splice( index, 1 );
		};

		CatalogPropertyFindReplace.prototype.loadCatalog = function() {

			var self = this;

			var req = portalRestClient.maketGetCatalogRequest({
				contextPath: self.contextPath,
				contextItemName: self.contextItemName,
				modifiers: {
					ps: 1000
				}
			});
			var xhr = portalRestClient.sendRequest(req.url, req.method, "", null);
			xhr.fail(function(e) {
				console.warn(e);
				self.error = e;
			});

			return xhr;
		};
		CatalogPropertyFindReplace.prototype.dryRun = function() {
			this.run(true);
		};

		CatalogPropertyFindReplace.prototype.run = function(dryRun) {
			var self = this;
			dryRun = dryRun || false;
			var xhr = this.loadCatalog();
			xhr.done(function(data) {
				self.updateProperties($(data), dryRun);
			});
			xhr.fail(function(e) {
				self.error = "The server responded with an error when attempting to fetch the catalog. See the console for more details";
				console.warn(e);
			});
		};

		CatalogPropertyFindReplace.prototype.updateProperties = function(catalog, dryRun) {

			var self = this;

			var updatedCatalog = [];

			catalog.children().children().each(function() {
				var item = $(this);
				var updatedItemData = {};
				updatedItemData.itemType = item[0].tagName;
				updatedItemData.name = item.children("name").text();
				updatedItemData.contextItemName = item.children("contextItemName").text();
				if(item.children("extendedItemName").length) {
					updatedItemData.extendedItemName = item.children("extendedItemName").text();
				}
				updatedItemData.properties = [];

				self.properties.forEach(function(propertyUpdateInfo) {
					var matchingProperty = item.find("property[name=" + propertyUpdateInfo.name +"]");
					if(matchingProperty.length) {
						var val =  matchingProperty.children("value");
						var find = new RegExp(propertyUpdateInfo.find);
						if(val.text().match(find)) {
							updatedItemData.properties.push({
								name: propertyUpdateInfo.name,
								oldValue: val.text(),
								value: val.text().replace(find, propertyUpdateInfo.replace),
								type: val.attr("type")
							});
						}
					}
				});
				if(updatedItemData.properties.length) {
					updatedCatalog.push(updatedItemData);
				}

			});
			console.info("Items to update:");
			console.info(updatedCatalog);
			this.report = updatedCatalog.length + " items with matching properties found. See console for details.";
			$scope.$apply();

			updatedCatalog.forEach(function(item) {

				var req = portalRestClient.makePutCatalogRequest({
					server: "",
					contextPath: "/portalserver",
					itemType: item.itemType,
					name: item.name,
					properties: item.properties
				});

				console.info("New request:");
				console.info(req.url + " [" + req.method + "]");
				console.info(req.body);
				console.info("======================================================");
				if(!dryRun) {
					var xhr = portalRestClient.sendRequest(req.url, req.method, req.body, null);
					xhr.done(function() {
						$scope.$apply(function() {
							self.updateSuccessCount++;
							self.report = self.updateSuccessCount + " items successfully updated. See console for details";
							console.info("Update request completed");
						});

					});
					xhr.fail(function(e) {
						$scope.$apply(function() {
							self.updateFailCount++;
							self.error = self.updateFailCount + " items failed to update. See console for details";
							console.warn(e);
						});
					});
				}
			});

		};

		var pfr = new CatalogPropertyFindReplace();
		pfr.addEmptyProperty();

		$scope.pfr = pfr;
	});

	migrate.controller("PortalFindReplaceController", function($scope) {

		var PortalPropertyFindReplace = function() {
			this.contextPath = "/portalserver";
			this.contextItemName = "";
			this.property = {};
			this.report = "";
			this.error = "";
			this.updateSuccessCount = 0;
			this.updateFailCount = 0;
		};

		PortalPropertyFindReplace.prototype.loadPortal = function() {

			var self = this;

			var req = portalRestClient.makeGetPortalRequest({
				contextPath: self.contextPath,
				contextItemName: self.contextItemName
			});
			var xhr = portalRestClient.sendRequest(req.url, req.method, "", null);
			xhr.fail(function(e) {
				console.warn(e);
				self.error = e;
			});

			return xhr;
		};
		PortalPropertyFindReplace.prototype.dryRun = function() {
			this.run(true);
		};

		PortalPropertyFindReplace.prototype.run = function(dryRun) {
			var self = this;
			dryRun = dryRun || false;
			var xhr = this.loadPortal();
			xhr.done(function(data) {
				self.updateProperties($(data), dryRun);
			});
			xhr.fail(function(e) {
				self.error = "The server responded with an error when attempting to fetch the catalog. See the console for more details";
				console.warn(e);
			});
		};

		PortalPropertyFindReplace.prototype.updateProperties = function(portal, dryRun) {

			var self = this;

			var itemsToUpdate = [];
			portal.find("property[name='" + this.property.name + "']").each(function(i, matchingProperty) {

				matchingProperty = $(matchingProperty);

				var updatedItemData = {};
				updatedItemData.properties = [];

				var val =  matchingProperty.children("value");
				var find = new RegExp(self.property.find);
				if(val.text().match(find)) {

					var item = matchingProperty.parent().parent();
					updatedItemData.itemType = item[0].tagName;
					updatedItemData.name = item.children("name").text();
					updatedItemData.contextItemName = item.children("contextItemName").text();
					if(item.children("extendedItemName").length) {
						updatedItemData.extendedItemName = item.children("extendedItemName").text();
					}
					updatedItemData.properties.push({
						name: self.property.name,
						oldValue: val.text(),
						value: $.trim(val.text().replace(find, self.property.replace)),
						type: val.attr("type")
					});
					itemsToUpdate.push(updatedItemData);
				}
			});

			console.info("Items to update:");
			console.info(itemsToUpdate);
			this.report = itemsToUpdate.length + " items with matching properties found. See console for details.";
			$scope.$apply();

			itemsToUpdate.forEach(function(item) {

				var restClientMethod;
				if(item.itemType === "page") {
					restClientMethod = "makePutPageRequest";
				} else if(item.itemType === "container") {
					restClientMethod = "makePutContainerRequest";
				} else if(item.itemType === "widget") {
					restClientMethod = "makePutWidgetRequest";
				} else {
					console.log("Update for item type [" + item.itemType + "] not supported.");
					console.log(item);
					return;
				}

				var req = portalRestClient[restClientMethod]({
					server: "",
					contextPath: "/portalserver",
					name: item.name,
					properties: item.properties,
					contextItemName: item.contextItemName,
					extendedItemName: item.extendedItemName
				});

				console.info("New request:");
				console.info(req.url + " [" + req.method + "]");
				console.info(req.body);
				console.info("======================================================");
				if(!dryRun) {
					var xhr = portalRestClient.sendRequest(req.url, req.method, req.body, null);
					xhr.done(function() {
						$scope.$apply(function() {
							self.updateSuccessCount++;
							self.report = self.updateSuccessCount + " items successfully updated. See console for details";
							console.info("Update request completed");
						});

					});
					xhr.fail(function(e) {
						$scope.$apply(function() {
							self.updateFailCount++;
							self.error = self.updateFailCount + " items failed to update. See console for details";
							console.warn(e);
						});
					});
				}
			});

		};

		var pfr = new PortalPropertyFindReplace();


		$scope.pfr = pfr;
	});
})(jQuery, portalRestClient);