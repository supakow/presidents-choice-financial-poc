<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Set the mock error</title>
        <link href="../support/bootstrap/css/bootstrap.min.css" type="text/css" rel="stylesheet">

    </head>
    <body>
        <div class="container">
            <h2>Set the mock error</h2>
            <p>...returned by <a target="_blank" href="${pageContext.request.contextPath}/static/mock-error/mock-error.jsp">${pageContext.request.contextPath}/static/mock-error/mock-error.jsp</a></p>
            <p>Set this file as your widget's data endpoint, to test error responses.</p>

            <div class="alert alert-info">
                Error currently set as:<br>
                <b>Error code:</b> ${sessionScope.errorCode}<br>
                <b>Message:</b> ${sessionScope.message}<br>
                <b>Status:</b> ${sessionScope.statusCode}<br>
                <b>Response delay:</b> ${sessionScope.delay}
            </div>

            <c:if test="${pageContext.request.method == 'POST'}">
                <c:set var="errorCode" scope="session" value="${param.errorCode != '' ? param.errorCode : '500'}"/>
                <c:set var="message" scope="session" value="${param.message != '' ? param.message : 'An unknown error occured'}"/>
                <c:set var="statusCode" scope="session" value="${param.statusCode != '' ? param.statusCode : '500'}"/>
                <c:set var="delay" scope="session" value="${param.delay != '' ? param.delay : '0'}"/>
                <%
                    response.setStatus(303);
                    response.setHeader("Location", request.getContextPath() + "/static/mock-error/set-mock-error.jsp");
                %>
            </c:if>

            <form class="clearfix form-horizontal" method="POST">
                <div class="control-group">
                    <label class="control-label">Error code</label>
                    <div class="controls">
                        <input type="text" name="errorCode" value="${sessionScope.errorCode}">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Message</label>
                    <div class="controls">
                        <input type="text" name="message" value="${sessionScope.message}"><br>
                        <span class="label label-info">Note:</span> This message is sent in the error response, but not usually used by the frontend.
                    </div>

                </div>
                <div class="control-group">
                    <label class="control-label">HTTP status</label>
                    <div class="controls">
                        <input type="text" name="statusCode" value="${sessionScope.statusCode}">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Response delay</label>
                    <div class="controls">
                        <input type="text" name="delay" value="${sessionScope.delay}">
                    </div>
                </div>
                <button type="submit" class="btn btn-success pull-right">Submit</button>
            </form>
        </div>
    </body>
</html>