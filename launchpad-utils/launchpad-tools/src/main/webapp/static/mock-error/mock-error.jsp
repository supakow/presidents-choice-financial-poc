<%--
    This JSP generates a sample launchpad json error response.
    Use set-mock-error.jsp to set the error values
--%>

<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="code" value="${sessionScope.errorCode != null ? sessionScope.errorCode : 500}"/>
<c:set var="message" value="${sessionScope.message != null ? sessionScope.message : 'An error occured'}"/>

<%
    Object delay = request.getSession().getAttribute("delay");
    if(delay != null) {
        int delayValue = Integer.parseInt((String)delay);
        if(delayValue > 0) {
            Thread.sleep(delayValue);
        }
    }

    int statusCode = 500;
    Object status = request.getSession().getAttribute("statusCode");
    if(status !=  null) {
        statusCode = Integer.parseInt((String)status);
    }
    response.setStatus(statusCode);
%>

{
    "errors":[{
        "code": "${code}",
        "message": "${message}"
    }]
}