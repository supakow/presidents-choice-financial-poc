define([
    "lp!angular",
    "lp!launchpad/lib/util",
    "lp!launchpad/lib/responsive"
], function(angular, util, responsive) {

    "use strict";

	var widgetModuleName = "my-project.my-widget";
    var module = angular.module("my-project.my-widget", []);

    module.controller("widgetController", function($scope, $rootElement, widget) {

		$scope.name = widget.getPreference("name");

	    // Responsive
	    responsive.enable($rootElement)
	        .rule({
	            "max-width": 400,
	            then: function() {
	                $scope.responsiveClass = "lp-small-size";
	                util.applyScope($scope);
	            }
	        }).rule({
	            "min-width": 401,
	            then: function() {
	                $scope.responsiveClass = "lp-large-size";
	                util.applyScope($scope);
	            }
	        });
    });

    return function(widget) {
        module.value("widget", widget);
        angular.bootstrap(widget.body, [ widgetModuleName ]);
    };
});