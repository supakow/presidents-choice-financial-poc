Prerequisites
=============
1. Install phonegap
npm install phonegap -g

2. Install Android SDKs and add android tools and platform-tools to your path

2. If you wish to run the iOS simulator from the command line:
npm install ios-sim


Build relative dependencies
============================
mvn clean -P rebuild


Run Portal statically on jetty
==============================
mvn process-resources
mvn jetty:run

Go to http://localhost:7979

Generate Android/iOS project files
==================================
mvn package -Pandroid/-Pios

Run Portal on an Android/iOS emulator or device
===========================================
mvn install -Pandroid/-Pios