package com.backbase.launchpad.maven.unpack;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.eclipse.aether.RepositorySystem;
import org.eclipse.aether.RepositorySystemSession;
import org.eclipse.aether.artifact.Artifact;
import org.eclipse.aether.artifact.DefaultArtifact;
import org.eclipse.aether.repository.RemoteRepository;
import org.eclipse.aether.resolution.ArtifactRequest;
import org.eclipse.aether.resolution.ArtifactResolutionException;
import org.eclipse.aether.resolution.ArtifactResult;

import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>This goal resolves widget artifacts and unpacks them in a defined location
 * <p>Widgets should be packaged as zip files
 * <p>This goal also optionally aggregates and transforms widget configuration files packaged with widgets into a
 * useful location
 *
 * @goal unpack
 * @phase process-sources
 */
public class UnpackWidgetsMojo extends AbstractMojo {

    /**
     * A list of widgets artifacts to unpack
     * @parameter
     */
    private Widget[] widgets;

    /**
     * <p>If a widget artifact item does not include output directory configuration, the unpacker will try to
     * determine the directory to unpack to by reading the widgets <code>src</code> property from its
     * configuration> file.
     * <p>This path is expected to be relative to the <code>widgetBaseDirectory</code>
     * @parameter default-value="${project.build.directory}"
     */
    private File widgetBaseDirectory;

    /**
     *
     * If evaluating a widget's <code>src</code> directory to determine where to unpack it to, it may be necessary to substitute
     * the variable $(contextRoot) with a real context root name.
     * @parameter default-value="${project.build.finalName}"
     */
    private String contextRoot;

    /**
     * If specified this will copy each widget's configuration file to the specified directory and
     * optionally apply XSL transforms.
     * @parameter
     */
    private AggregateConfigXml aggregateConfigXml;


    /**
     * The entry point to Aether, i.e. the component doing all the work.
     *
     * @component
     */
    private RepositorySystem repoSystem;

    /**
     * The current repository/network configuration of Maven.
     *
     * @parameter default-value="${repositorySystemSession}"
     * @readonly
     */
    private RepositorySystemSession repoSession;

    /**
     * The project's remote repositories to use for the resolution of plugins and their dependencies.
     *
     * @parameter default-value="${project.remotePluginRepositories}"
     * @readonly
     */
    private List<RemoteRepository> remoteRepos;

    /**
     * Resolves all the widget artifacts and unpacks them
     * @throws MojoFailureException
     * @throws MojoExecutionException
     */
    public void execute() throws MojoFailureException, MojoExecutionException {

        if(widgets != null) {
            Map<Widget, Artifact> widgetsToArtifacts = new HashMap<Widget, Artifact>();

            //resolve widgets
            for(Widget widget : widgets) {
                Artifact resolvedArtifact =
                    resolveArtifact(widget.getGroupId(), widget.getArtifactId(), widget.getVersion());
                widgetsToArtifacts.put(widget, resolvedArtifact);
            }

            //process widgets
            getLog().info("Unpacking " + widgets.length + " widget" + ( widgets.length == 1 ? "" : "s") +  " using base directory: " + widgetBaseDirectory + "; using context root: " + contextRoot);
            for (Map.Entry<Widget, Artifact> entry : widgetsToArtifacts.entrySet()) {
                unpackWidget(entry.getKey(), entry.getValue().getFile());
            }
        }
    }

    private Artifact resolveArtifact(String groupId, String artifactId, String version )
            throws MojoFailureException, MojoExecutionException {

        Artifact artifact = new DefaultArtifact(groupId, artifactId, "zip", version);
        ArtifactRequest request = new ArtifactRequest();
        request.setArtifact( artifact );
        request.setRepositories( remoteRepos );

        getLog().info( "Resolving widget artifact " + artifact + " from " + remoteRepos );

        ArtifactResult result;
        try {
            result = repoSystem.resolveArtifact( repoSession, request );
        }
        catch ( ArtifactResolutionException e ) {
            throw new MojoExecutionException( e.getMessage(), e );
        }

        if(result == null) {
            String message = "Could not resolve widget artifact " + artifact;
            throw new MojoExecutionException(message);
        }

        getLog().info( "Resolved widget artifact " + artifact + " from " + result.getRepository());
        return result.getArtifact();
    }

    private void unpackWidget(Widget widget, File widgetZip) throws MojoExecutionException {

        WidgetUnpacker unpacker = new WidgetUnpacker(getLog());
        InputStream xslTransform = null;
        try {
            getLog().info("Unpacking widget (" + widget + ")");
            File aggregatedConfigXmlDirectory = null;
            if(aggregateConfigXml != null) {
                aggregatedConfigXmlDirectory = aggregateConfigXml.getDirectory();
                xslTransform = getConfigTransformXsl(aggregateConfigXml.getTransformXsl());
            }
            unpacker.unpackWidget(widget, widgetZip, widgetBaseDirectory, contextRoot, aggregatedConfigXmlDirectory, xslTransform);
        } catch (WidgetUnpackingException e) {
            String message =
                    "Unable to unpack widget ( " + widget + ") from file (" + widgetZip + "): " + e.getMessage();
            throw new MojoExecutionException(message, e);
        } finally {
            try {
                if(xslTransform != null) {
                    xslTransform.close();
                }
            } catch (IOException e) {
                throw new MojoExecutionException(e.getMessage(), e);
            }
        }
    }

    private InputStream getConfigTransformXsl(String transformXsl) {

        InputStream xslIn = null;
        if(transformXsl != null) {
            if("catalog".equals(transformXsl)) {
                xslIn = this.getClass().getResourceAsStream("/launchpad/unpack/catalog.xsl");
            } else if("widgets".equals(transformXsl)) {
                xslIn = this.getClass().getResourceAsStream("/launchpad/unpack/widgets.xsl");
            } else {
                File xslInFile = new File(transformXsl);
                try {
                    xslIn = new FileInputStream(xslInFile);
                } catch (FileNotFoundException e) {
                    getLog().warn("Could not find transform stylesheet: " + xslInFile);
                    xslIn = null;
                }
            }
        }

        return xslIn;
    }

    public Widget[] getWidgets() {
        return widgets;
    }

    public void setRepoSystem(RepositorySystem repoSystem) {
        this.repoSystem = repoSystem;
    }

    public void setRepoSession(RepositorySystemSession repoSession) {
        this.repoSession = repoSession;
    }

    public void setRemoteRepos(List<RemoteRepository> remoteRepos) {
        this.remoteRepos = remoteRepos;
    }
}
