package com.backbase.launchpad.maven.unpack;

import java.io.File;

/**
 * Wraps information concerning aggregating widget configuration files
 */
public class AggregateConfigXml {

    /**
     * Copies each widget's configuration file to the specified directory.
     * @parameter
     * @required
     */
    private File directory;

    /**
     * An XSL transform to perform on the widget's configuration file
     * <p>Special values:
     * <ul>
     *     <li>config</li>
     *     <li>widgets</li>
     * </ul>
     * <p>maybe used which will wrap the aggregated configuration xml in those respective tag names.
     * @parameter
     */
    private String transformXsl;

    public File getDirectory() {
        return directory;
    }

    public void setDirectory(File directory) {
        this.directory = directory;
    }

    public String getTransformXsl() {
        return transformXsl;
    }

    public void setTransformXsl(String transformXsl) {
        this.transformXsl = transformXsl;
    }
}
