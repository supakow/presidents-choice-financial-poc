package com.backbase.launchpad.maven.unpack;

import com.google.common.io.Files;
import org.apache.maven.plugin.logging.Log;
import org.codehaus.plexus.archiver.ArchiverException;
import org.codehaus.plexus.archiver.zip.ZipUnArchiver;
import org.codehaus.plexus.util.FileUtils;
import org.xml.sax.InputSource;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.*;

/**
 * Class including all widget unpacking logic
 */
public class WidgetUnpacker {

    private Log logger = null;

    private final static String SRC_XPATH = "//property[@name='src']/value/text()";

    private final static String LOG_PREFIX = "    ";

    public WidgetUnpacker() {
    }

    public WidgetUnpacker(Log logger) {

        this.logger = logger;
    }

    /**
     * Full widget unpacking process
     * @param widget Widget artifact pojo
     * @param widgetZip Zip archive to extract widget from
     * @param widgetBase Base directory to unpack widgets to
     * @param contextPath Context path used when replacing $(contextRoot) in widget 'src' properties
     * @param transformStylesheet InputStream of a transform stylesheet, maybe null
     */
    public void unpackWidget(Widget widget, File widgetZip, File widgetBase, String contextPath, File aggregatedConfigXmlDirectory, InputStream transformStylesheet)
            throws WidgetUnpackingException {

        File extractedWidgetPath = null;
        try {
            //extract to a temporary location
            extractedWidgetPath = extractWidget(widget, widgetZip);

            //figure where to unpack the widget
            if(widget.getUnpackDirectory() == null) {
                determineUnpackPath(widget, extractedWidgetPath, widgetBase, contextPath);
            }

            //then copy
            if(widget.getUnpackDirectory() != null) {
                copyToUnpackPath(widget, extractedWidgetPath);
            }

            //aggregate config files
            if(aggregatedConfigXmlDirectory != null) {
                File aggregatedConfigFile =
                    copyConfigToAggregatedLocation(widget, extractedWidgetPath, aggregatedConfigXmlDirectory);

                //optionally, transform the config
                if(transformStylesheet != null) {
                    performTransform(transformStylesheet, aggregatedConfigFile);
                }
            }
        } finally {
            if(extractedWidgetPath != null) {
                extractedWidgetPath.delete();
            }
        }
    }

    /**
     * Unpacks a zipped widget to a temporary location
     * @param widgetZip
     * @return The path the widget was extracted to
     */
    protected File extractWidget(Widget widget, File widgetZip) throws WidgetUnpackingException {

        File tempPath = Files.createTempDir();

        try {
            ZipUnArchiver unArchiver = new ZipUnArchiver(widgetZip);
            unArchiver.extract(widget.getArchivePrefix(), tempPath);
        } catch(ArchiverException e) {
            String message = "Problem extracting zip file to temporary location " +
                    "(from [" + widgetZip + "] to [" + tempPath + "]): " + e.getMessage();
            throw new WidgetUnpackingException(message, e);
        }

        return new File(tempPath, widget.getArchivePrefix());
    }

    /**
     * Basic XSLT operation
     * @param stylesheet
     * @param configXml
     */
    protected void performTransform(InputStream stylesheet, File configXml) throws WidgetUnpackingException {

        try {
            TransformerFactory factory = TransformerFactory.newInstance();
            StreamSource xslSource = new StreamSource(stylesheet);
            Transformer transformer = factory.newTransformer(xslSource);
            StreamSource xmlIn = new StreamSource(configXml);
            ByteArrayOutputStream xmlOutStream = new ByteArrayOutputStream();
            StreamResult xmlOut = new StreamResult(xmlOutStream);
            transformer.transform(xmlIn, xmlOut);

            FileOutputStream outputStream = new FileOutputStream(configXml);
            xmlOutStream.writeTo(outputStream);
            info("Aggregated config XML has been transformed: " + configXml);
        } catch (Exception e) {
            String message = "Problem performing XSL tranform on widget config (" + configXml + "): " + e.getMessage();
            throw new WidgetUnpackingException(message, e);
        }
    }

    /**
     * Sets the widget's unpacking directory if it is not already set.
     * The directory to set is determined by parsing the src property in the widget's xml config file.
     * The config file should be named config.xml
     * @param widget
     * @param tempPath
     * @param unpackBase
     * @param contextPath
     * @throws IOException
     */
    protected void determineUnpackPath(Widget widget, File tempPath, File unpackBase, String contextPath)
            throws WidgetUnpackingException {

        File configFile = new File(tempPath, widget.getConfigXml());
        if(!configFile.exists()) {
            Throwable e = new FileNotFoundException("Could not open widget config xml (" + configFile + ")");
            throw new WidgetUnpackingException(e);
        } else {
            //read config file src directory
            String unpackPath = findWidgetSourcePath(configFile, contextPath);
            if(unpackPath != null) {
                File unpackDirectory = new File(unpackBase, unpackPath);
                widget.setUnpackDirectory(unpackDirectory);
            }
        }
    }

    private String findWidgetSourcePath(File configFile, String contextPath) throws WidgetUnpackingException {

        String srcProperty = null;
        String srcPath = null;

        //get the src property
        try {
            XPathFactory factory = XPathFactory.newInstance();
            XPath xpath = factory.newXPath();
            XPathExpression expression = xpath.compile(SRC_XPATH);
            srcProperty = expression.evaluate(new InputSource(configFile.getAbsolutePath()));
        } catch (XPathExpressionException e) {
            String message = "Unable to evaluate src property from xml config (" + configFile + "): " + e.getMessage();
            warn(message, e);
        }

        if(srcProperty == null || srcProperty.isEmpty()) {
            info("Could not evaluate src property from xml config. Widget will not be unpacked.");
        } else {
            //fix the context path
            if(contextPath != null) {
                srcProperty = srcProperty.replaceAll("\\$\\(contextRoot\\)", contextPath);
            }
            srcPath = srcProperty.substring(0, srcProperty.lastIndexOf("/"));

        }
        return srcPath;
    }

    protected File copyConfigToAggregatedLocation(Widget widget, File extractedWidgetDirectory, File aggregateConfigXmlDirectory)
        throws WidgetUnpackingException{

        File aggregatedFile;
        try {
            //create if doesn't exist
            if(!aggregateConfigXmlDirectory.exists()) {
                Files.createParentDirs(aggregateConfigXmlDirectory);
                aggregateConfigXmlDirectory.mkdir();
            }

            File configFile = new File(extractedWidgetDirectory, widget.getConfigXml());
            String aggregatedConfigFileName = widget.getArtifactId() + "-" + widget.getConfigXml();
            aggregatedFile = new File(aggregateConfigXmlDirectory, aggregatedConfigFileName);
            FileUtils.copyFile(configFile, new File(aggregateConfigXmlDirectory, aggregatedConfigFileName));
            info("Widget config aggregated to: " + aggregateConfigXmlDirectory);
        } catch (IOException e) {
            throw new WidgetUnpackingException(e);
        }

        return aggregatedFile;
    }

    /**
     * Copies a widget to its untended final unpacking location, creating that location if it
     * doesn't exist
     * @param widget
     * @param extractedWidgetDirectory
     * @throws IOException
     */
    protected void copyToUnpackPath(Widget widget, File extractedWidgetDirectory) throws WidgetUnpackingException {

        if(widget.getUnpackDirectory() == null) {
            throw new WidgetUnpackingException("No unpacking directory is set on the widget. Cannot unpack!");
        }

        try {
            if(!widget.getUnpackDirectory().exists()) {
                Files.createParentDirs(widget.getUnpackDirectory());
                widget.getUnpackDirectory().mkdir();
            }
            FileUtils.copyDirectoryStructure(extractedWidgetDirectory, widget.getUnpackDirectory());
            info("Widget unpacked to: " + widget.getUnpackDirectory());
        } catch(IOException e) {
            String message = "Failure copying widget to final unpacking location (" + widget.getUnpackDirectory() +
                    "): " + e.getMessage();
            throw new WidgetUnpackingException(message, e);
        }
    }

    private void info(String message) {
        if(logger != null) {
            logger.info(LOG_PREFIX + message);
        }

    }

    private void warn(String message) {
        if(logger != null) {
            logger.warn(LOG_PREFIX + message);
        }
    }

    private void warn(String message, Throwable e) {
        if(logger != null) {
            logger.warn(LOG_PREFIX + message, e);
        }
    }
}
