package com.backbase.launchpad.maven.unpack;

import java.io.File;

/**
 * Represents a single widget artifact
 */
public class Widget {

    /**
     * The group id of the widget to unpack
     * @parameter
     * @required
     */
    private String groupId;

    /**
     * The artifact id of the widget to unpack
     * @parameter
     * @required
     */
    private String artifactId;

    /**
     * The version number of the widget to unpack
     * @parameter
     * @required
     */
    private String version;

    /**
     * The directory to unpack this widget to.
     * <p>If ignored, this directory will be automatically determined by parsing the widget's <i>config.xml</i>
     * file and parsing its <code>src</code> directory. Generally speaking, this paramter should be ignored
     * unless there is a specific reason.
     * @parameter
     */
    private File unpackDirectory;

    /**
     * If
     * @parameter
     */
    private String archivePrefix;

    /**
     * This is the name of the widget's configuration file.
     * <p>Defaults to <i>config.xml</i>
     * @parameter
     */
    private String configXml;

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getArtifactId() {
        return artifactId;
    }

    public void setArtifactId(String artifactId) {
        this.artifactId = artifactId;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public File getUnpackDirectory() {
        return unpackDirectory;
    }

    public void setUnpackDirectory(File unpackDirectory) {
        this.unpackDirectory = unpackDirectory;
    }

    public String getArchivePrefix() {
        if(archivePrefix != null) {
            return archivePrefix;
        } else {
            return artifactId;
        }
    }

    public void setArchivePrefix(String archivePrefix) {
        this.archivePrefix = archivePrefix;
    }

    public String getConfigXml() {
        if(configXml != null) {
            return configXml;
        } else {
            return "config.xml";
        }

    }

    public void setConfigXml(String configXml) {
        this.configXml = configXml;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Widget widget = (Widget) o;

        if (!artifactId.equals(widget.artifactId)) return false;
        if (!groupId.equals(widget.groupId)) return false;
        if (!version.equals(widget.version)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = groupId.hashCode();
        result = 31 * result + artifactId.hashCode();
        result = 31 * result + version.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "WIDGET:" + groupId + ":" + artifactId + ":" + version + ":zip";
    }
}
