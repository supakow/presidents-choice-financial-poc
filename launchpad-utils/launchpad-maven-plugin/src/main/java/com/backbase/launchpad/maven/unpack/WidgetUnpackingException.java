package com.backbase.launchpad.maven.unpack;

/**
 * General exception for wrapping widget unpacking problems
 */
public class WidgetUnpackingException extends Exception {

    public WidgetUnpackingException(Throwable throwable) {
        super(throwable);
    }

    public WidgetUnpackingException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public WidgetUnpackingException(String s) {
        super(s);
    }
}
