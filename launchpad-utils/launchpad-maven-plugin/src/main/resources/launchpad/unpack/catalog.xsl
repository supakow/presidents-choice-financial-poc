<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <xsl:element name="catalog">
            <xsl:copy-of select="*" />
        </xsl:element>
    </xsl:template>
</xsl:stylesheet>