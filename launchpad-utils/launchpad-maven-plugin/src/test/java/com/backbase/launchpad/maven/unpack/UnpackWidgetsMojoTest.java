package com.backbase.launchpad.maven.unpack;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.testing.AbstractMojoTestCase;
import org.eclipse.aether.RepositorySystem;
import org.eclipse.aether.RepositorySystemSession;
import org.junit.Test;

import java.io.File;

import static org.mockito.Mockito.mock;

/**
 * Created by philip on 24-2-14.
 */
public class UnpackWidgetsMojoTest extends AbstractMojoTestCase {

    /** {@inheritDoc} */
    protected void setUp() throws Exception {
        super.setUp();
    }

    /** {@inheritDoc} */
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * @throws Exception
     *             if any
     */
    @Test
    public void testExecute() throws Exception {
        File pom = getTestFile("src/test/resources/launchpad/unpack/pom.xml");
        assertNotNull(pom);
        assertTrue(pom.exists());

        UnpackWidgetsMojo mojo = (UnpackWidgetsMojo) lookupMojo("unpack", pom);
        assertNotNull(mojo);

        RepositorySystem repositorySystem = mock(RepositorySystem.class);

        RepositorySystemSession repositorySystemSession = mock(RepositorySystemSession.class);
        mojo.setRepoSession(repositorySystemSession);
        mojo.setRepoSystem(repositorySystem);

        assertEquals("launchpad-accounts", mojo.getWidgets()[0].getArtifactId());

        try {
            mojo.execute();
        }
        catch(MojoExecutionException e) {
            //This failure is ok
        }
    }
}
