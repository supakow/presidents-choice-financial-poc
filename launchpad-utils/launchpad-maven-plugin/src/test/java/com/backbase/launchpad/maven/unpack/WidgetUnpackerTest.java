package com.backbase.launchpad.maven.unpack;

import org.apache.commons.io.FileUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.xml.sax.InputSource;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

/**
 * Created by philip on 25-2-14.
 */
public class WidgetUnpackerTest {

    @Rule
    public TemporaryFolder tempFolder = new TemporaryFolder();

    @Test
    public void unpackWidget() throws WidgetUnpackingException, XPathExpressionException {

        Widget widget = new Widget();
        widget.setArtifactId("launchpad-accounts");

        File widgetZip = new File(this.getClass().getResource("/launchpad/unpack/launchpad-accounts.zip").getFile());
        File base = getTempDir();
        File aggregatedConfig = getTempDir();
        InputStream configTransform = this.getClass().getResourceAsStream("/launchpad/unpack/catalog.xsl");

        WidgetUnpacker unpacker = new WidgetUnpacker();
        unpacker.unpackWidget(widget, widgetZip, base, "portalserver", aggregatedConfig, configTransform);

        File widgetPath = new File(base, "/portalserver/static/launchpad/widgets/accounts/index.html");
        assertTrue(widgetPath.exists());

        File testXml = new File(aggregatedConfig, widget.getArtifactId() + "-" + widget.getConfigXml());
        XPathFactory factory = XPathFactory.newInstance();
        XPath xpath = factory.newXPath();
        XPathExpression expression = xpath.compile("//catalog");
        String root = expression.evaluate(new InputSource(testXml.getAbsolutePath()));
        assertTrue(root.length() > 0);
    }

    @Test
    public void extractWidget() throws WidgetUnpackingException {

        Widget widget = new Widget();
        widget.setArtifactId("launchpad-accounts");

        File widgetZip = new File(this.getClass().getResource("/launchpad/unpack/launchpad-accounts.zip").getFile());

        WidgetUnpacker unpacker = new WidgetUnpacker();
        File tempFile = unpacker.extractWidget(widget, widgetZip);

        assertTrue(new File(tempFile, "index.html").exists());
    }

    @Test
    public void determineUnpackPath() throws WidgetUnpackingException {

        Widget widget = new Widget();
        widget.setArtifactId("launchpad-accounts");

        File widgetDir = new File(this.getClass().getResource("/launchpad/unpack/launchpad-accounts").getFile());
        File unpackBase = new File("C:/temp");
        String contextPath = "/portalserver";

        WidgetUnpacker unpacker = new WidgetUnpacker();
        unpacker.determineUnpackPath(widget, widgetDir, unpackBase, contextPath);

        URI expectedDir = new File(unpackBase + contextPath + "/static/launchpad/widgets/accounts").toURI();
        assertEquals(expectedDir, widget.getUnpackDirectory().toURI());
    }

    @Test
    public void performTransform() throws WidgetUnpackingException, IOException, XPathExpressionException {

        File accountsXml = new File(this.getClass().getResource("/launchpad/unpack/launchpad-accounts/config.xml").getFile());
        InputStream stylesheet = this.getClass().getResourceAsStream("/launchpad/unpack/catalog.xsl");

        File tempDir = getTempDir();
        FileUtils.copyFileToDirectory(accountsXml, tempDir);
        File testXml = new File(tempDir, "config.xml");

        WidgetUnpacker unpacker = new WidgetUnpacker();
        unpacker.performTransform(stylesheet, testXml);

        XPathFactory factory = XPathFactory.newInstance();
        XPath xpath = factory.newXPath();
        XPathExpression expression = xpath.compile("//catalog");
        String root = expression.evaluate(new InputSource(testXml.getAbsolutePath()));
        assertTrue(root.length() > 0);
    }

    @Test
    public void copyToUnpackPath() throws WidgetUnpackingException, IOException {

        File tempDir = getTempDir();

        Widget widget = new Widget();
        widget.setUnpackDirectory(tempDir);

        WidgetUnpacker unpacker = new WidgetUnpacker();

        File widgetDir = new File(this.getClass().getResource("/launchpad/unpack/launchpad-accounts").getFile());
        unpacker.copyToUnpackPath(widget, widgetDir);

        File widgetPath = new File(tempDir, "index.html");
        assertTrue(widgetPath.exists());
    }

    private File getTempDir() {

        //use this for debugging
        //File tempDir = Files.createTempDir();

        //auto deleted junit temp dir
        File tempDir = tempFolder.getRoot();
        return tempDir;
    }
}
