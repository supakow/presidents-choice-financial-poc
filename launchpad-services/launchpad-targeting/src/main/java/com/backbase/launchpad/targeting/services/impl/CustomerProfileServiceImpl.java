package com.backbase.launchpad.targeting.services.impl;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

import java.util.Collections;
import java.util.Map;

import org.apache.camel.EndpointInject;
import org.apache.camel.ProducerTemplate;

import com.backbase.launchpad.targeting.services.CustomerProfileService;

/**
 * Default implementation of {@link CustomerProfileService} leveraging party-data-management integration service.
 */
public class CustomerProfileServiceImpl implements CustomerProfileService {

    @EndpointInject(uri = "seda:provider/party/party-data-management/party")
    private ProducerTemplate customerInformationEndpoint;

    public Gender getGenderById(String id) {
        Gender result = Gender.UNKNOWN;

        Object payload = null;
        Map<String, Object> headers = Collections.<String, Object>singletonMap("BackbasePartyId", id);
        Object endpointRequestResult = customerInformationEndpoint.requestBodyAndHeaders(payload, headers);
        if (endpointRequestResult != null) {
            JSONObject resultAsJson = (JSONObject) JSONSerializer.toJSON(endpointRequestResult);
            JSONObject details = (JSONObject) resultAsJson.get("details");
            try {
                result = Gender.valueOf((String) details.get("gender"));
            } catch (IllegalArgumentException e) {
                result = Gender.UNKNOWN;
            }
        }

        return result;
    }
}
