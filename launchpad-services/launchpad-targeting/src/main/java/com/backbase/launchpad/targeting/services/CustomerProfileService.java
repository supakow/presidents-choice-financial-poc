package com.backbase.launchpad.targeting.services;


import com.backbase.launchpad.targeting.CustomerProfileCollector;

/**
 * Business related contract for the {@link CustomerProfileCollector}
 */
public interface CustomerProfileService {

    public enum Gender {
        MALE, FEMALE, UNKNOWN
    }

    /**
     * Returns the gender the customer based on the id. {@link Gender.UNKNOWN} is returned if it is not known.
     * @param id the id of the customer
     * @return {@link Gender} instance
     */
    Gender getGenderById(String id);
}
