package com.backbase.launchpad.targeting;

import static com.backbase.launchpad.targeting.services.CustomerProfileService.Gender;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.backbase.launchpad.targeting.services.CustomerProfileService;
import com.backbase.portal.targeting.connectorframework.content.contexts.definition.PossibleValue;
import com.backbase.portal.targeting.connectorframework.content.contexts.definition.ResultEntries;
import com.backbase.portal.targeting.connectorframework.content.contexts.definition.SegmentDefinition;
import com.backbase.portal.targeting.connectorframework.content.contexts.definition.SelectorDefinition;
import com.backbase.portal.targeting.connectorframework.content.contexts.definition.StaticContextCollector;
import com.backbase.portal.targeting.rulesengine.type.RuleEngineTypes;

/**
 * Integration with targeting using {@link StaticContextCollector}
 */
public class CustomerProfileCollector extends StaticContextCollector {

    private static final Logger LOG = LoggerFactory.getLogger(CustomerProfileCollector.class);

    private static final String CUSTOMER_PROFILE_COLLECTOR = "customerProfile";

    public static final String CUSTOMER_GENDER = "customer.gender";

    private CustomerProfileService customerProfileService;

    public CustomerProfileCollector() {
        super(CUSTOMER_PROFILE_COLLECTOR, "Launchpad customer profile collector",
                "$(contextRoot)/static/backbase.com.2012.darts/media/contexts/targeting-context-crm.png",
                "Customer Profile");
    }

    public void setCustomerProfileCollector(CustomerProfileService customerProfileService) {
        this.customerProfileService = customerProfileService;
    }

    public List<SelectorDefinition> defineSelectors(String s, Map<String, String> map) {
        List<SelectorDefinition> selectors = new ArrayList<SelectorDefinition>();

        SelectorDefinition genderSelectorDefinition = new SelectorDefinition(CUSTOMER_GENDER, RuleEngineTypes.STRING, "Gender");
        genderSelectorDefinition.addPossibleValues(new PossibleValue("MALE"), new PossibleValue("FEMALE"));
        selectors.add(genderSelectorDefinition);

        return selectors;
    }

    public List<SegmentDefinition> defineSegments(String s, Map<String, String> map) {
        List<SegmentDefinition> segments = new ArrayList<SegmentDefinition>();
        segments.add(new SegmentDefinition("car", "CarLoan"));
        segments.add(new SegmentDefinition("home", "HomeLoan"));
        return segments;
    }

    @Override
    public ResultEntries executeTask(Map<String, String> requestMap, ResultEntries resultEntries) {
        String customerId = requestMap.get("session.customerId");
        if (customerId != null) {
            LOG.info("Content targeted to customer with id:" + customerId);
            Gender gender = customerProfileService.getGenderById(customerId);
            resultEntries.addSelectorEntry(CUSTOMER_GENDER, gender.name());
        }
        return resultEntries;
    }

}
