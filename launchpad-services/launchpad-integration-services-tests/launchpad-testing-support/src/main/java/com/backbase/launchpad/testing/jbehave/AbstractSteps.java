package com.backbase.launchpad.testing.jbehave;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;

import java.io.File;
import java.nio.charset.Charset;

import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.test.junit4.CamelSpringTestSupport;
import org.jbehave.core.annotations.BeforeStory;
import org.restlet.Component;
import org.restlet.Response;
import org.springframework.context.support.AbstractXmlApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.xmldb.api.DatabaseManager;
import org.xmldb.api.base.Collection;
import org.xmldb.api.modules.CollectionManagementService;

import com.google.common.io.Files;

//FIXME: Move data access and Camel related stuff to abstract DAO classes bundled for Demo Provider so that it would be possible to provide different DAO bundle for different provider
public abstract class AbstractSteps extends CamelSpringTestSupport {
    protected Component restlet;

    @Override
    public boolean isCreateCamelContextPerClass() {
        return true;
    }

    @Override
    protected CamelContext createCamelContext() throws Exception {
        CamelContext camelContext = super.createCamelContext();

        RouteBuilder dataPreLoadRoute = new RouteBuilder() {
            @Override
            public void configure() throws Exception {
                from("direct:loadData")
                        .recipientList().simple("${header.XMLDBURL}");
            }
        };

        dataPreLoadRoute.addRoutesToCamelContext(camelContext);

        return camelContext;
    }

    protected void loadDataFromFile(String dataFileName, String destination) {
        String payLoad;
        try {
            File file = new File(this.getClass().getClassLoader().getResource(dataFileName).toURI());

            payLoad = Files.toString(file, Charset.forName("UTF-8"));
        } catch (Exception ex) {
            if (ex instanceof RuntimeException) {
                throw (RuntimeException) ex;
            }

            throw new RuntimeException(ex);
        }

        loadData(payLoad, destination);
    }

    protected void loadData(String xmlPayload, String collection) {
        String destination = "xmldb:exist:///db/launchpad" + collection + "?username=admin&password=";

        template.sendBodyAndHeader("direct:loadData", xmlPayload, "XMLDBURL", destination);
    }

    @Override
    protected AbstractXmlApplicationContext createApplicationContext() {
        return new ClassPathXmlApplicationContext(
                new String[] {
                        "camel-context-test.xml",
                        "classpath*:META-INF/spring/backbase-mashup-service.xml",
                        "classpath*:META-INF/spring/restlet-components.xml"
                });
    }

    @BeforeStory
    public void setup() throws Exception {
        this.setUp();

        restlet = applicationContext.getBean(Component.class);
    }

    @BeforeStory
    public void cleanUpDatabase() {
        try {
            Collection parentCollection = DatabaseManager.getCollection("xmldb:exist:///db", "admin", "");
            CollectionManagementService mgt = (CollectionManagementService) parentCollection.getService("CollectionManagementService", "1.0");

            mgt.removeCollection("launchpad");
            parentCollection.close();
        } catch (Exception ex) {
            //IGNORE
        }
    }

    protected void assertThatRequestIsSucceded(Response response) {
        assertThat("the response is returned", response, notNullValue());
        assertThat("the response entity is available", response.getEntity(), notNullValue());
        assertThat("response status code", response.getStatus().getCode(), equalTo(200));
    }

    protected void assertThatRequestIsFailed(Response response) {
        assertThat("the response is returned", response, notNullValue());
        assertThat("the response entity is available", response.getEntity(), notNullValue());
        assertThat("response status code", response.getStatus().getCode(), equalTo(500));
    }
}
