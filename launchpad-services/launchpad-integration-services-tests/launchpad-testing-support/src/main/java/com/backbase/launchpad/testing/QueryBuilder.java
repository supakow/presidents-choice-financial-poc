package com.backbase.launchpad.testing;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * HTTP URL Query String Builder
 *
 * @author V.Goldin
 */
public class QueryBuilder {
    private static final String DEFAULT_CHARSET = "UTF-8";
    private final String charset;
    private final StringBuilder queryBuilder = new StringBuilder();

    private QueryBuilder(String charset) {
        this.charset = charset;
    }

    private QueryBuilder() {
        this.charset = DEFAULT_CHARSET;
    }

    public QueryBuilder addParam(String name, Object value) {
        this.queryBuilder.append(name);
        this.queryBuilder.append("=");

        try {
            this.queryBuilder.append(URLEncoder.encode(value.toString(), charset));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }

        this.queryBuilder.append("&");

        return this;
    }

    public String build() {
        return queryBuilder.toString();
    }

    public static QueryBuilder newQuery(String charset) {
        return new QueryBuilder(charset);
    }

    public static QueryBuilder newQuery() {
        return new QueryBuilder();
    }
}
