Story: Managing a Contact Details
Narrative: As a user I want to manage my contact details so that the institution can contact me

Scenario: Providing a Valid Phone Number
Given the user profile is available
When the user provides a valid phone number
Then the phone number should be stored

Scenario: Providing an Invalid Phone Number
Given the user profile is available
When the user provides a invalid phone number
Then the application should provide an error message

Scenario: Providing a valid Email Address
Given the user profile is available
When the user provides a valid email address
Then the email address should be stored

Scenario: Providing an invalid Email Address
Given the user profile is available
When the user provides a invalid email address
Then the application should provide an error message