Story: Two-factor authentication is required for certain users

Narrative: As a CISO I want to enforce two-factor authentication for certain user so that I can minimize a risk of unauthorised access to her account

Scenario: Required second step with valid login/password
Given a second T-OTP step verification is required
When user login with valid username and password
Then the application asks to provide Time-based One-Time-Password

Scenario: Required second step with invalid login/password
Given a second T-OTP step verification is required
When user login with invalid username and password
Then the application asks to provide Time-based One-Time-Password

Scenario: Successful authentication with valid OTP valid login/password
Given a second T-OTP step verification is required
And user provided valid username and password
When user provides a valid T-OTP
Then the application confirms successful authentication

Scenario: Failed authentication with invalid OTP valid login/password
Given a second T-OTP step verification is required
And user provided valid username and password
When user provides an invalid T-OTP
Then the application informs about the failed authentication

Scenario: Failed authentication with valid OTP invalid login/password
Given a second T-OTP step verification is required
And user provided invalid username and password
When user provides a valid T-OTP
Then the application informs about the failed authentication

Scenario: Failed authentication with valid but expired OTP and valid login/password
Given a second T-OTP step verification is required
And user provided valid username and password
When user provides a valid OTP after delaying for 60 seconds
Then the application informs about the failed authentication

Scenario: Successful authentication with just a valid login/password
Given a second step verification is not required
When user login with valid username and password
Then the application confirms successful authentication

Scenario: Failed authentication with invalid login/password
Given a second step verification is not required
When user login with invalid username and password
Then the application informs about the failed authentication