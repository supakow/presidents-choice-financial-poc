Narrative: As a logged-in user I want to retrieve and edit my preferences.

!-- currently the only preference is "preferredName"

Scenario: A logged-in user is presented with his/her preferences upon request
Given the user with username lisa exists
When the user with username lisa requests his/her preferences
Then the user with username lisa is presented with his/her preference

Scenario: Return empty string when preference does not exist
Given the user with username jonny exists
When the user with username jonny requests his/her preferences
Then the user with username jonny is presented with his/her preference

Scenario: A logged-in user edits his/her preferences upon request
Given the user with username peter exists
When the user with username peter modifies her preferredName to Pan
Then the preferredName of the user with username peter is modified to Pan