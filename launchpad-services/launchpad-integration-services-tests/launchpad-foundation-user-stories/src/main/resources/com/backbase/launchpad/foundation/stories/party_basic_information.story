Narrative: As a logged-in user I want to receive basic information about my profile.

Scenario: A logged-in user is presented with his/her basic information upon request
Given the system has basic information about the user with username lisa
When the user with username lisa requests basic information about his/her profile
Then the user with username lisa is presented with the basic information


