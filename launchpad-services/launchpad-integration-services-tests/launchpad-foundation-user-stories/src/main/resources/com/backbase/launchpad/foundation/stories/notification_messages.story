Narrative:
As an Application Support Team I want to push a certain notification message to the user

Scenario: Deactivate the only active message (hidden by the end-user or deactivated by the business user)
Given a user has a 1 active INFO level non-sticky message
When user hides a message
Then the message should not be provided

Scenario: Retrieve the message with the given severity and stickiness
Given a user has a message of <level> with <stickiness>
When user requests a list of messages
Then the message should be provided reflecting the <level> and <stickiness>

Examples:
|level|stickiness|
|INFO|false|
|SEVERE|true|
|WARNING|true|