package com.backbase.launchpad.foundation.steps;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import java.util.UUID;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.data.MediaType;
import org.restlet.data.Method;
import com.backbase.launchpad.testing.QueryBuilder;
import static com.backbase.launchpad.testing.QueryBuilder.*;
import static org.hamcrest.Matchers.*;

import com.backbase.launchpad.testing.jbehave.AbstractSteps;

public class NotificationMessagesSteps extends AbstractSteps {
    private JSONObject subject = null;
    private String messageId = UUID.randomUUID().toString();

    @Given("a user has a $numberOfMessages $status $level level $stickiness message")
    public void newSubmitedMessage(Integer numberOfMessages, String status, String level, String stickiness) {
        Boolean stickyObject = (stickiness.equals("non-sticky") ? false : true);
        cleanUpDatabase();
        sendNewMessage(level, stickyObject, System.currentTimeMillis() + 360000L);
    }

    @Given("a user has a message of <level> with <stickiness>")
    public void hasMessage(@Named("level") String level, @Named("stickiness") Boolean stickiness) {
        cleanUpDatabase();
        sendNewMessage(level, stickiness, System.currentTimeMillis() + 360000L);
    }

    @When("user hides a message")
    public void userHidesMessage() {
        deactivateMessage();
    }

    @When("user requests a list of messages")
    public void whenUserRequestsAListOfMessages() {
        retrieveMessages();
    }

    @Then("the message should not be provided")
    public void shouldNotBeProvided() {
        retrieveMessages();

        assertThat("no messages are retrieved", subject.getJSONArray("messages").size(), is(Integer.valueOf(0)));
    }

    @Then("the message should be provided reflecting the <level> and <stickiness>")
    public void shouldProvideWithLevelAndStickiness(@Named("level") String level, @Named("stickiness") Boolean stickiness) {
        retrieveMessages();

        assertThat("no messages are retrieved", subject.getJSONArray("messages").size(), is(Integer.valueOf(1)));

        JSONObject message = subject.getJSONArray("messages").getJSONObject(0);
        assertThat("valid level is provided", message.getString("level"), equalTo(level));
        assertThat("valid stickiness attribute is provided", message.getBoolean("closable"), equalTo(stickiness));
    }

    private void sendNewMessage(String level, Boolean sticky, Long expiresOn) {
        QueryBuilder query = newQuery()
                .addParam("backbasepartyid", "3")
                .addParam("id", messageId)
                .addParam("closable", sticky)
                .addParam("expiresOn", expiresOn)
                .addParam("level", level)
                .addParam("message", "Test message");

        Request request = new Request();
        request.setResourceRef("http://localhost:80/contact-dialogues/messages/new");
        request.setMethod(Method.POST);
        request.setEntity(query.build(), MediaType.APPLICATION_WWW_FORM);

        Response response = new Response(request);
        restlet.handle(request, response);

        assertThatRequestIsSucceded(response);
    }

    private void deactivateMessage() {
        QueryBuilder query = newQuery().addParam("backbasepartyid", "3");

        Request request = new Request();
        request.setResourceRef("http://localhost:80/contact-dialogues/messages/" + messageId + "/deactivate");
        request.setEntity(query.build(), MediaType.APPLICATION_WWW_FORM);
        request.setMethod(Method.PUT);

        Response response = new Response(request);
        restlet.handle(request, response);

        assertThatRequestIsSucceded(response);
    }

    private void retrieveMessages() {
        Request request = new Request();
        request.setResourceRef("http://localhost:80/contact-dialogues/messages");
        request.getResourceRef().addQueryParameter("backbasepartyid", "3");
        request.setMethod(Method.GET);

        Response response = new Response(request);
        restlet.handle(request, response);

        assertThatRequestIsSucceded(response);

        subject = (JSONObject) JSONSerializer.toJSON(response.getEntityAsText());
    }
}
