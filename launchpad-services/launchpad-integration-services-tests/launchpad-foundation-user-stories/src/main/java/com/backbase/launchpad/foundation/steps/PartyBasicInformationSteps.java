package com.backbase.launchpad.foundation.steps;

import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.startsWith;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.data.Method;

import com.backbase.launchpad.testing.jbehave.AbstractSteps;

public class PartyBasicInformationSteps extends AbstractSteps {

    private JSONObject subject;

    @Given("the system has basic information about the user with username $username")
    public void systemHasBasicInformationAboutTheUser(String username) {
        loadDataFromFile("data/party-basic-information/" + username + ".xml", "/customers");

        // for the last-logged-in time calculation session data needs to be loaded
        loadDataFromFile("data/party-basic-information/session.xml", "/sessions");
    }

    @When("the user with username $username requests basic information about his/her profile")
    public void requestBasicInformation(String username) {
        Request request = new Request();
        request.setResourceRef("http://localhost:80/party-data-management/party");
        if ("lisa".equals(username)) {
            request.getResourceRef().addQueryParameter("backbasepartyid", "3");
        } else {
            throw new IllegalArgumentException("could not find user with username " + username);
        }
        request.setMethod(Method.GET);

        Response response = new Response(request);
        restlet.handle(request, response);

        assertThatRequestIsSucceded(response);

        subject = (JSONObject) JSONSerializer.toJSON(response.getEntityAsText());
    }

    @Then("the user with username $username is presented with the basic information")
    public void returnBasicInformation(String username) {

        assertThat(subject.get("username").toString(), is(equalTo("lisa")));
        assertThat(subject.get("firstName").toString(), is(equalTo("Lisa")));
        assertThat(subject.get("lastName").toString(), is(equalTo("Nijenhuis")));

        JSONObject details = (JSONObject) subject.get("details");
        assertThat(details.get("dateOfBirth").toString(), is(equalTo("1985-03-10")));
        assertThat(details.get("gender").toString(), is(equalTo("FEMALE")));
        assertThat(details.get("SSN").toString(), is(endsWith("91")));
        assertThat(details.get("citizenship").toString(), is(equalTo("American")));

        JSONObject activities = (JSONObject) subject.get("activities");
        assertThat(activities.get("lastLoggedIn").toString(), is(startsWith("2013-12-12")));
    }

}
