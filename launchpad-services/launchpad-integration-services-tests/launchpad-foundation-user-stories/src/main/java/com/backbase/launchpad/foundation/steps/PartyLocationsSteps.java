package com.backbase.launchpad.foundation.steps;

import static com.backbase.launchpad.testing.QueryBuilder.newQuery;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.core.Is.is;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.data.MediaType;
import org.restlet.data.Method;

import com.backbase.launchpad.testing.QueryBuilder;
import com.backbase.launchpad.testing.jbehave.AbstractSteps;

public class PartyLocationsSteps extends AbstractSteps {
    private static final String RESTLET_LOCATION = "http://localhost:80/party-data-management/locations";
    private Object subject;

    @Given("the user profile is available")
    public void givenNoConditions() {
        cleanUpDatabase();

        loadDataFromFile("data/users/lisa.xml", "/customers");
    }

    @When("the user provides a valid phone number")
    public void whenTheUserProvidesAValidPhoneNumber() {
        QueryBuilder query = newQuery()
                .addParam("backbasepartyid", "3")
                .addParam("phoneNumber", "+16464787538");

        Response response = updateLocation(query);
        assertThatRequestIsSucceded(response);

        subject = loadProfile();
    }

    @When("the user provides a invalid phone number")
    public void whenTheUserProvidesAInvalidPhoneNumber() {
        QueryBuilder query = newQuery()
                .addParam("backbasepartyid", "3")
                .addParam("phoneNumber", "abc16464787538");

        subject = updateLocation(query);
    }

    @When("the user provides a valid email address")
    public void whenTheUserProvidesAValidEmailAddress() {
        QueryBuilder query = newQuery()
                .addParam("backbasepartyid", "3")
                .addParam("emailAddress", "test@backbase.com");

        updateLocation(query);

        subject = loadProfile();
    }

    @When("the user provides a invalid email address")
    public void whenTheUserProvidesAInvalidEmailAddress() {
        QueryBuilder query = newQuery()
                .addParam("backbasepartyid", "3")
                .addParam("emailAddress", "testAtbackbase.com");

        subject = updateLocation(query);
    }

    @Then("the application should provide an error message")
    public void thenTheApplicationShouldProvideAnErrorMessage() {
        assertThatRequestIsFailed((Response) subject);
    }

    @Then("the phone number should be stored")
    public void thenThePhoneNumberShouldBeStored() {
        assertThat("phoneNumber is set", ((JSONObject) subject).getString("phoneNumber"), is("+16464787538"));
    }

    @Then("the email address should be stored")
    public void thenTheEmailAddressShouldBeStored() {
        Request request = new Request();
        request.setResourceRef(RESTLET_LOCATION);
        request.getResourceRef().addQueryParameter("backbasepartyid", "3");
        request.setMethod(Method.GET);

        Response response = new Response(request);
        restlet.handle(request, response);

        assertThatRequestIsSucceded(response);

        subject = (JSONObject) JSONSerializer.toJSON(response.getEntityAsText());
        assertThat("emailAddress is set", ((JSONObject) subject).getString("emailAddress"), is("test@backbase.com"));
    }

    /*
        Internal utility methods
     */
    private JSONObject loadProfile() {
        Request request;Response response;
        request = new Request();
        request.setResourceRef(RESTLET_LOCATION);
        request.getResourceRef().addQueryParameter("backbasepartyid", "3");
        request.setMethod(Method.GET);

        response = new Response(request);
        restlet.handle(request, response);

        assertThatRequestIsSucceded(response);

        return (JSONObject) JSONSerializer.toJSON(response.getEntityAsText());
    }

    private Response updateLocation(QueryBuilder query) {
        Request request = new Request();
        request.setResourceRef(RESTLET_LOCATION);
        request.setMethod(Method.PUT);
        request.setEntity(query.build(), MediaType.APPLICATION_WWW_FORM);

        Response response = new Response(request);
        restlet.handle(request, response);

        return response;
    }
}
