package com.backbase.launchpad.foundation;

import static org.jbehave.core.io.CodeLocations.codeLocationFromClass;
import static org.jbehave.core.reporters.Format.CONSOLE;
import static org.jbehave.core.reporters.Format.HTML;
import static org.jbehave.core.reporters.Format.XML;

import java.util.List;

import org.jbehave.core.configuration.Configuration;
import org.jbehave.core.configuration.MostUsefulConfiguration;
import org.jbehave.core.io.CodeLocations;
import org.jbehave.core.io.LoadFromClasspath;
import org.jbehave.core.io.StoryFinder;
import org.jbehave.core.io.UnderscoredCamelCaseResolver;
import org.jbehave.core.junit.JUnitStories;
import org.jbehave.core.reporters.CrossReference;
import org.jbehave.core.reporters.StoryReporterBuilder;
import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.jbehave.core.steps.SilentStepMonitor;

import com.backbase.launchpad.foundation.steps.CustomerPreferencesSteps;
import com.backbase.launchpad.foundation.steps.NotificationMessagesSteps;
import com.backbase.launchpad.foundation.steps.PartyBasicInformationSteps;
import com.backbase.launchpad.foundation.steps.PartyLocationsSteps;
import com.backbase.launchpad.foundation.steps.SecondFactorAuthenticationSteps;

public class FoundationStories extends JUnitStories {
    public FoundationStories(){
        configuredEmbedder().embedderControls().doIgnoreFailureInStories(true).doIgnoreFailureInView(true);
    }

    @Override
    public Configuration configuration() {
        Class<? extends FoundationStories> embedderClass = this.getClass();

        return new MostUsefulConfiguration()
                .useStoryLoader(new LoadFromClasspath(embedderClass.getClassLoader()))
                .useStoryPathResolver(new UnderscoredCamelCaseResolver())
                .useStoryReporterBuilder(new StoryReporterBuilder()
                        .withCodeLocation(CodeLocations.codeLocationFromClass(embedderClass))
                        .withDefaultFormats()
                        .withFormats(CONSOLE, HTML, XML)
                        .withCrossReference(new CrossReference()))
                .useStepMonitor(new SilentStepMonitor());
    }

    @Override
    public InjectableStepsFactory stepsFactory() {
        return new InstanceStepsFactory(configuration(),
                new SecondFactorAuthenticationSteps(),
                new NotificationMessagesSteps(),
                new PartyLocationsSteps(),
                new PartyBasicInformationSteps(),
                new CustomerPreferencesSteps()
        );
    }

    @Override
    protected List<String> storyPaths() {
        return new StoryFinder().findPaths(codeLocationFromClass(this.getClass()), "**/*.story", "**/excluded*.story");
    }

}
