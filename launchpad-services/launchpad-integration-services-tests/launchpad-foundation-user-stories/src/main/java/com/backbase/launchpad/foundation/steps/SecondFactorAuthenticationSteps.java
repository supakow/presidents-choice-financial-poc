package com.backbase.launchpad.foundation.steps;

import static com.backbase.launchpad.testing.QueryBuilder.newQuery;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

import org.apache.commons.codec.binary.Base32;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.data.MediaType;
import org.restlet.data.Method;

import com.backbase.launchpad.services.crosschannel.authentication.TOTP;
import com.backbase.launchpad.testing.QueryBuilder;
import com.backbase.launchpad.testing.jbehave.AbstractSteps;

public class SecondFactorAuthenticationSteps extends AbstractSteps {
    private static final int TWO_MINUTES_WINDOW = 2;
    private JSONObject subject;
    private String username;
    private Boolean required = true;

    @Given("a second step verification is not required")
    public void givenASecondStepVerificationIsNotRequired() {
        username = "thomas";
        required = false;

        prepareUserAndSessionData(username);
    }

    @Given("a second T-OTP step verification is required")
    public void givenASecondStepVerificationIsRequired() {
        username = "lisa";

        prepareUserAndSessionData(username);
    }

    @Given("user provided $validity username and password")
    public void givenUserProvidedInvalidUsernameAndPassword(String validity) {
        String password = "password";
        if (validity.equals("invalid")) {
            // -- preserve username if the T-TOP is required, since its a user attribute
            if (!required) username = "invalid";

            password = password + "123";
        }

        initiateSession(username, password);
    }

    @When("user login with $validity username and password")
    public void whenUserLoginWithValidUsernameAndPassword(String validity) {
        String password = "password";
        if (validity.equals("invalid")) {
            if (!required) username = "invalid";

            password = password + "123";
        }

        initiateSession(username, password);
    }

    @When("user provides an invalid T-OTP")
    public void whenUserProvidesAnInvalidOTP() {
        verifyOTP(12345L);
    }

    @When("user provides a valid T-OTP")
    public void whenUserProvidesAValidOTP() {
        long totp = TOTP.getCode(sharedSecret(), System.currentTimeMillis() / 30000L);

        verifyOTP(totp);
    }

    @When("user provides a valid OTP after delaying for $delay seconds")
    public void whenUserProvidesAValidOTPAfterDelaying(Integer delay) {
        long totp = TOTP.getCode(sharedSecret(),
                ((System.currentTimeMillis() + delay * 1000 * TWO_MINUTES_WINDOW) / 30000L));

        verifyOTP(totp);
    }

    @Then("the application asks to provide Time-based One-Time-Password")
    public void thenTheApplicationAsksToProvideTimebasedOneTimePassword() {
        JSONObject session = subject.getJSONObject("session");
        JSONObject next = subject.getJSONObject("next");
        JSONArray actions = next.getJSONArray("actions");

        String workflowId = session.getString("id");

        assertThat("session id is provided", workflowId, notNullValue());
        assertThat("valid session status is provided'", session.getString("status"), equalTo("Initiated"));
        assertThat("next action is provided", next, notNullValue());
        assertThat("next action is verifyOTP", next.getString("rel"), equalTo("verifyOTP"));
        assertThat("at least on action for the next step is provided", actions.size(), greaterThan(0));
        assertThat("valid action 'rel' is provided", actions.getJSONObject(0).getString("rel"), equalTo("verify"));
        assertThat("valid action 'uri' is provided", actions.getJSONObject(0).getString("uri"), equalTo("/authentication/session/"+workflowId+"/verifyOTP"));
    }

    @Then("the application confirms successful authentication")
    public void thenTheApplicationConfirmsSuccessfulAuthentication() {
        JSONObject session = subject.getJSONObject("session");

        assertThat("id is provided", session.getString("id"), notNullValue());
        assertThat("valid session status is provided", session.getString("status"), equalTo("Verified"));
    }


    @Then("the application informs about the failed authentication")
    public void thenTheApplicationInformsAboutTheFailedAuthentication() {
        JSONArray errors = subject.getJSONArray("errors");
        JSONObject error = errors.getJSONObject(0);

        assertThat("valid error code is provided", error.getString("code"), equalTo("CANNOT_AUTHENTICATE"));
    }

    /**
     * Inner utility methods
     */
    private void verifyOTP(long totp) {
        String workflowId = (String) subject.getJSONObject("session").get("id");

        QueryBuilder queryBuilder = newQuery()
                .addParam("otp_code", totp);

        Request request = new Request();
        request.setResourceRef("http://localhost:80/authentication/session/"+workflowId+"/verifyOTP");
        request.setMethod(Method.POST);
        request.setEntity(queryBuilder.build(), MediaType.APPLICATION_WWW_FORM);

        Response response = new Response(request);
        restlet.handle(request, response);

        assertThat("response status code", response.getStatus().getCode(), not(equalTo(500)));

        subject = (JSONObject) JSONSerializer.toJSON(response.getEntityAsText());
    }

    private byte[] sharedSecret() {
        return new Base32().decode("7KWYN6ZMLV5RURKA");
    }

    private void loadUserData(String username) {
        loadDataFromFile("data/users/" + username + ".xml", "/customers");
    }

    private void initiateSession(String username, String password) {
        QueryBuilder queryBuilder = newQuery()
                .addParam("username", username)
                .addParam("password", password);

        Request request = new Request();
        request.setResourceRef("http://localhost:80/authentication/session/initiate");
        request.setMethod(Method.POST);
        request.setEntity(queryBuilder.build(), MediaType.APPLICATION_WWW_FORM);

        Response response = new Response(request);
        restlet.handle(request, response);

        assertThat("response status code", response.getStatus().getCode(), not(equalTo(500)));

        subject = (JSONObject) JSONSerializer.toJSON(response.getEntityAsText());
    }

    private void prepareUserAndSessionData(String username) {
        cleanUpDatabase();

        loadUserData(username);
        loadDataFromFile("data/sessions/dummy_session.xml", "/sessions");
    }
}
