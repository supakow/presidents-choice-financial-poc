package com.backbase.launchpad.foundation.steps;

import static com.backbase.launchpad.testing.QueryBuilder.newQuery;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

import java.util.HashMap;
import java.util.Map;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.data.MediaType;
import org.restlet.data.Method;

import com.backbase.launchpad.testing.QueryBuilder;
import com.backbase.launchpad.testing.jbehave.AbstractSteps;

public class CustomerPreferencesSteps extends AbstractSteps {

    private static Map<String, String> userIdMap = new HashMap<String, String>();
    private JSONObject subject;

    static {
        userIdMap.put("lisa", "3");   // lisa has preferredName preference
        userIdMap.put("jonny", "5");  // jonny has no preferredName preference
        userIdMap.put("peter", "6");
    }

    @Given("the user with username ${username} exists")
    public void systemHasUser(String username) {
        loadDataFromFile("data/customer-preferences/" + username + ".xml", "/customers");
    }

    @When("the user with username ${username} requests his/her preferences")
    public void requestPreferences(String username) {
        Request request = new Request();
        request.setResourceRef("http://localhost:80/party-data-management/preferences");
        request.getResourceRef().addQueryParameter("backbasepartyid", userIdMap.get(username));
        request.setMethod(Method.GET);

        Response response = new Response(request);
        restlet.handle(request, response);

        assertThatRequestIsSucceded(response);

        subject = (JSONObject) JSONSerializer.toJSON(response.getEntityAsText());
    }

    @Then("the user with username ${username} is presented with his/her preference")
    public void returnPreferences(String username) {
        if ("lisa".equals(username)) {
            assertThat(subject.get("preferredName").toString(), is(equalTo("Lisa Nijenhuis")));
        } else if ("jonny".equals(username)) {
            assertThat(subject.get("preferredName").toString(), is(equalTo("")));
        }
    }

    @When("the user with username ${username} modifies her preferredName to ${preferredName}")
    public void modifyPreferences(String username, String preferredName) {

        QueryBuilder query = newQuery().addParam("backbasepartyid", userIdMap.get(username)).addParam("preferredName", preferredName);

        Request request = new Request();
        request.setResourceRef("http://localhost:80/party-data-management/preferences\"");
        request.setEntity(query.build(), MediaType.APPLICATION_WWW_FORM);
        request.setMethod(Method.PUT);

        Response response = new Response(request);
        restlet.handle(request, response);
    }

    @Then("the preferredName of the user with username ${username} is modified to ${preferredName}")
    public void checkModifications(String username, String preferredName) {
        Request request = new Request();
        request.setResourceRef("http://localhost:80/party-data-management/preferences");
        request.getResourceRef().addQueryParameter("backbasepartyid", userIdMap.get(username));
        request.setMethod(Method.GET);

        Response response = new Response(request);
        restlet.handle(request, response);
        subject = (JSONObject) JSONSerializer.toJSON(response.getEntityAsText());

        if ("peter".equals(username)) {
            assertThat(subject.get("preferredName").toString(), is(equalTo(preferredName)));
        } else {
            fail();
        }
    }

}
