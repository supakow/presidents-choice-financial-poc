Narrative: As a User I want to transfer money from my current account

Story: Transferring money from current account

Scenario:
Given a user has a current account
And the account has a 100.00 EUR balance
When user initiates a 99.00 EUR payment order from the current account
Then the payment order is initiated
