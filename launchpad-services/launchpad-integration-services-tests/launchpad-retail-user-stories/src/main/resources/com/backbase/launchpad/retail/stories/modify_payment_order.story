Narrative: As a User I want to modify my initiated payment order so that I can correct the wrong information

Story: Modifying initiated payment order

Scenario:
Given a user has a payment order initiated
When the user updates payment order providing John as a beneficiary and Gift as a remittance information
Then the beneficiary of the payment order should be set to John
Then the remittance information of the payment order should be set to Gift

Scenario:
Given a user has two payment orders initiated
When the user requests a list of initiated payments
Then the application provides a list with two payments