Narrative: As a User I want to see my daily and weekly turnovers over specified period of time

Scenario: Daily turnovers
Given the user has a transactions for the specified period of time
When the user requests daily turnovers for the specified period of time
Then the application provides a turnover for each day in the specified period of time

Scenario: Weekly turnovers
Given the user has a transactions for the specified period of time
When the user requests weekly turnovers for the specified period of time
Then the application provides a turnover for each full week in the specified period of time