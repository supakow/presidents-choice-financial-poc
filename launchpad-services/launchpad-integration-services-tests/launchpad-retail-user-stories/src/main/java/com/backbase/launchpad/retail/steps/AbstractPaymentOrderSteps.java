package com.backbase.launchpad.retail.steps;

import static com.backbase.launchpad.testing.QueryBuilder.newQuery;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

import java.math.BigDecimal;
import java.util.UUID;

import org.restlet.Request;
import org.restlet.Response;
import org.restlet.data.MediaType;
import org.restlet.data.Method;

import com.backbase.launchpad.testing.QueryBuilder;
import com.backbase.launchpad.testing.jbehave.AbstractSteps;

public abstract class AbstractPaymentOrderSteps extends AbstractSteps {
    protected String paymentOrderId = UUID.randomUUID().toString();
    protected JSONObject subject = null;

    protected void initiatePaymentOrder(BigDecimal amount, String currency, String debitAccountId) {
        QueryBuilder query = newQuery()
                .addParam("backbasepartyid", "3")
                .addParam("accountId", debitAccountId)
                .addParam("counterpartyName", "CPN")
                .addParam("counterpartyAccount", "CPA")
                .addParam("instructedAmount", amount)
                .addParam("paymentMode", "once")
                .addParam("instructedCurrency", currency);

        Request request = new Request();
        request.setResourceRef("http://localhost:80/payment-order/" + paymentOrderId);
        request.setMethod(Method.POST);
        request.setEntity(query.build(), MediaType.APPLICATION_WWW_FORM);

        Response response = new Response(request);
        restlet.handle(request, response);
    }

    protected void retrievePaymentOrderById(String paymentOrderId) {
        Request request = new Request();
        request.setResourceRef("http://localhost:80/payment-order/" + paymentOrderId);
        request.getResourceRef().addQueryParameter("backbasepartyid", "3");
        request.setMethod(Method.GET);

        Response response = new Response(request);
        restlet.handle(request, response);

        assertThatRequestIsSucceded(response);

        subject = (JSONObject) JSONSerializer.toJSON(response.getEntityAsText());
    }
}
