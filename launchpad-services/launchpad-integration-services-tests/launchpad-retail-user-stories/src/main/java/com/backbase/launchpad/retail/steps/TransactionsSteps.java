package com.backbase.launchpad.retail.steps;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Pending;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import com.backbase.launchpad.testing.jbehave.AbstractSteps;

public class TransactionsSteps extends AbstractSteps {
    @Given("the user has a transactions for the specified period of time")
    @Pending
    public void givenTheUserHasATransactionsForTheSpecifiedPeriodOfTime() {
        // PENDING
    }

    @When("the user requests weekly turnovers for the specified period of time")
    @Pending
    public void whenTheUserRequestsWeeklyTurnoversForTheSpecifiedPeriodOfTime() {
        // PENDING
    }

    @Then("the application provides a turnover for each full week in the specified period of time")
    @Pending
    public void thenTheApplicationProvidesATurnoverForEachFullWeekInTheSpecifiedPeriodOfTime() {
        // PENDING
    }

    @When("the user requests daily turnovers for the specified period of time")
    @Pending
    public void whenTheUserRequestsDailyTurnoversForTheSpecifiedPeriodOfTime() {
        // PENDING
    }

    @Then("the application provides a turnover for each day in the specified period of time")
    @Pending
    public void thenTheApplicationProvidesATurnoverForEachDayInTheSpecifiedPeriodOfTime() {
        // PENDING
    }
}
