package com.backbase.launchpad.retail.steps;

import static com.backbase.launchpad.testing.QueryBuilder.newQuery;
import static org.hamcrest.Matchers.equalTo;

import java.math.BigDecimal;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Pending;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.data.MediaType;
import org.restlet.data.Method;

import com.backbase.launchpad.testing.QueryBuilder;


public class ModifyPaymentOrderSteps extends AbstractPaymentOrderSteps {
    @Given("a user has a payment order initiated")
    public void userHasPaymentOrderInitiated() {
        cleanUpDatabase();

        initiatePaymentOrder(BigDecimal.valueOf(100), "EUR", "DEBIT_ACCOUNT");
    }

    @When("the user updates payment order providing $beneficiary as a beneficiary and $remittance as a remittance information")
    public void whenTheUserUpdatesPaymentOrder(String beneficiary, String remittance) {
        updatePaymentOrder(beneficiary, remittance);
        retrievePaymentOrderById(paymentOrderId);
    }

    @Then("the beneficiary of the payment order should be set to $beneficiary")
    public void thenTheBeneficiaryIsUpdated(String beneficiary) {
        assertThat("the beneficiary is updated", subject.getString("counterpartyName"), equalTo(beneficiary));
    }

    @Then("the remittance information of the payment order should be set to $remittance")
    public void thenTheRemittanceInformationIsUpdated(String remittance) {
        assertThat("the beneficiary is updated", subject.getString("remittanceInformation"), equalTo(remittance));
    }

    @Given("a user has two payment orders initiated")
    @Pending
    public void givenAUserHasTwoPaymentOrdersInitiated() {
        // PENDING
    }

    @When("the user requests a list of initiated payments")
    @Pending
    public void whenTheUserRequestsAListOfInitiatedPayments() {
        // PENDING
    }

    @Then("the application provides a list with two payments")
    @Pending
    public void thenTheApplicationProvidesAListWithTwoPayments() {
        // PENDING
    }

    private void updatePaymentOrder(String beneficiary, String remittance) {
        QueryBuilder query = newQuery()
                .addParam("backbasepartyid", "3")
                .addParam("counterpartyName", beneficiary)
                .addParam("remittanceInformation", remittance);

        Request request = new Request();
        request.setResourceRef("http://localhost:80/payment-order/" + paymentOrderId);
        request.setMethod(Method.PUT);
        request.setEntity(query.build(), MediaType.APPLICATION_WWW_FORM);

        Response response = new Response(request);
        restlet.handle(request, response);
    }
}
