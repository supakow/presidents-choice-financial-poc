package com.backbase.launchpad.retail.steps;
import static org.hamcrest.Matchers.*;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.MatcherAssert.*;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;


public class SubmitPaymentOrderSteps extends AbstractPaymentOrderSteps {
    private static final String DEBIT_ACCOUNT_ID = "MYACCOUNT";
    private BigDecimal paymentAmount = null;
    private String paymentCurrency = null;

    @Given("a user has a current account")
    public void customerHasAnAccount() {
        loadDataFromFile("data/accounts/MYACCOUNT.xml", "/accounts");
    }

    @Given("the account has a $balance $currency balance")
    public void theAccountsHas(BigDecimal balance, String currency) {
        // -- loaded by the previous step
    }

    @When("user initiates a $amount $currency payment order from the current account")
    public void customerIniatesAPaymentOrder(BigDecimal amount, String currency) {
        paymentAmount = amount;
        paymentCurrency = currency;

        initiatePaymentOrder(amount, currency, DEBIT_ACCOUNT_ID);
    }

    @Then("the payment order is initiated")
    public void paymentOrderShouldBeInitiated() {
        retrievePaymentOrderById(paymentOrderId);

        assertThat("payment order is initiated", subject, notNullValue());
        assertThat("payment order status is 'Received'", subject.getString("status"), equalTo("Received"));
        assertThat("accountId is valid", subject.getString("accountId"), equalTo(DEBIT_ACCOUNT_ID));
        assertThat("counterpartyName is valid", subject.getString("counterpartyName"), equalTo("CPN"));
        assertThat("counterpartyAccount is valid", subject.getString("counterpartyAccount"), equalTo("CPA"));
        assertThat("instructedAmount is valid", subject.getDouble("instructedAmount"), equalTo(paymentAmount.doubleValue()));
        assertThat("paymentMode is valid", subject.getString("schedule"), equalTo("null"));
        assertThat("instructedCurrency is valid", subject.getString("instructedCurrency"), equalTo(paymentCurrency));
    }
}
