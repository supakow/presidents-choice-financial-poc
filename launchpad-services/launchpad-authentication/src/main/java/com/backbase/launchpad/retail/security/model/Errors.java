package com.backbase.launchpad.retail.security.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: veniamin
 * Date: 10/16/13
 * Time: 5:27 PM
 * To change this template use File | Settings | File Templates.
 */
public class Errors extends ArrayList<Errors.Error> {
    public static class Error {
        private Integer code;
        private String message;

        public Error(Integer code, String message) {
            this.code = code;
            this.message = message;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }
    }
}
