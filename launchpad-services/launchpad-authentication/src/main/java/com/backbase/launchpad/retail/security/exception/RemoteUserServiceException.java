package com.backbase.launchpad.retail.security.exception;

public class RemoteUserServiceException extends RuntimeException {
    public RemoteUserServiceException(Exception ex) {
        super(ex);
    }

    public RemoteUserServiceException(String message, Exception ex) {
        super(message, ex);
    }
}
