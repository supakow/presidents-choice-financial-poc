package com.backbase.launchpad.retail.security.model;

/**
 * Simple POJO for holding login status info. This class can be serialized to JSON.
 */
public class LoginStatus {

	private String username;
    private String successView;
	
	public LoginStatus(){
	}

	public LoginStatus(String username, String successView) {
		this.username = username;
        this.successView = successView;
	}

    public String getUsername() {
        return username;
    }

    public String getSuccessView() {
        return successView;
    }
}
