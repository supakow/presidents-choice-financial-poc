package com.backbase.launchpad.retail.security.successview;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.backbase.portal.foundation.business.service.ItemBusinessService;
import com.backbase.portal.foundation.commons.exceptions.BeanNotFoundException;
import com.backbase.portal.foundation.commons.exceptions.ItemNotFoundException;
import com.backbase.portal.foundation.domain.conceptual.BasePropertyValue;
import com.backbase.portal.foundation.domain.conceptual.Item;

/**
 * {@link SuccessViewResolver} implementation leveraging portal server API.
 *
 * @author Zoltan Altfatter
 */
public class PortalServerSuccessViewResolver implements SuccessViewResolver {

    private static final Logger LOGGER = LoggerFactory.getLogger(PortalServerSuccessViewResolver.class);

    @Autowired
    @Qualifier("pageBusinessService")
    private ItemBusinessService pageBusinessService;

    private static final String SUCCESS_VIEW_PROPERTY_NAME = "successView";
    private static final String DEFAULT_SUCCESS_VIEW_VALUE = "/home";

    /**
     * {@inheritDoc}
     */
    @Override
    public String getSuccessView(String portalName, String pageName) {
        String result = DEFAULT_SUCCESS_VIEW_VALUE;
        if (StringUtils.isEmpty(pageName) || StringUtils.isEmpty(pageName)) {
            LOGGER.info("portal_name and page_name was empty, success view set to default " + DEFAULT_SUCCESS_VIEW_VALUE);
            return result;
        }
        try {
            Item page = pageBusinessService.getItem(portalName, pageName, false);
            BasePropertyValue property = page.getProperty(SUCCESS_VIEW_PROPERTY_NAME);
            if (property != null) {
                Object value = property.getValue();
                if (value != null) {
                    result = value.toString();
                }
            }
            LOGGER.info("success view resolved to " + result);
        } catch (ItemNotFoundException e) {
            throw new IllegalArgumentException("error resolving the success view after login. Could not find page " + pageName + " in portal " + portalName);
        }

        return result;
    }
}
