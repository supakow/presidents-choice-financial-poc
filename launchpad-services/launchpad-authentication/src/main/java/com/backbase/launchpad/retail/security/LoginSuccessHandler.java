package com.backbase.launchpad.retail.security;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.MapUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;

import com.backbase.launchpad.retail.security.model.LoginStatus;
import com.backbase.launchpad.retail.security.successview.SuccessViewResolver;
import com.backbase.portal.foundation.commons.security.CustomAuthenticationSuccessHandler;
import com.backbase.portal.foundation.domain.conceptual.UserPropertyDefinition;
import com.backbase.portal.foundation.domain.model.User;

//TODO: This depends on the portal manager jars!!!
public class LoginSuccessHandler extends CustomAuthenticationSuccessHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoginSuccessHandler.class);
    private static final String ACCEPT = "Accept";
    private static final String ACCEPT_JSON = "application/json";

    private static final String PORTAL_NAME_PARAMETER_NAME = "portal_name";
    private static final String PAGE_NAME_PARAMETER_NAME = "page_name";

    @Autowired
    private RemoteAuthenticationService remoteAuthenticationService;

    @Autowired
    private SuccessViewResolver successViewResolver;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
            Authentication authentication) throws IOException, ServletException {

        LOGGER.info("onAuthenticationSuccess() name={}, isAuthenticated={}", authentication.getName(),
                authentication.isAuthenticated());

        SecurityContextHolderAwareRequestWrapper securityContext =
                new SecurityContextHolderAwareRequestWrapper(request, "role_");
        LOGGER.info("UserPrincipal name=" + securityContext.getUserPrincipal().getName());

        SavedRequest savedRequest = (SavedRequest) request.getSession().getAttribute(
                CustomAuthenticationSuccessHandler.SPRING_SECURITY_SAVED_REQUEST);
        if (savedRequest != null) {
            LOGGER.info("SavedRequest redirectUrl=" + savedRequest.getRedirectUrl());
        }

        // Set the status to "Confirmed" and map the HTTP Session id to the workflow id.
        remoteAuthenticationService.confirmSession(getUserProperty("workflowId"), request.getSession().getId());

        String successView = successViewResolver.getSuccessView(request.getParameter(PORTAL_NAME_PARAMETER_NAME),
                request.getParameter(PAGE_NAME_PARAMETER_NAME));

        // save customerId in session object.
        request.getSession().setAttribute("customerId", getUserProperty("customerId")); //FIXME: Leave just one
        request.getSession().setAttribute("partyId", getUserProperty("customerId"));

        String accept = request.getHeader(ACCEPT);
        if (accept.contains(ACCEPT_JSON)) {
            // Reponse message
            response.setContentType(ACCEPT_JSON);
            ObjectMapper mapper = new ObjectMapper();
            LoginStatus status = new LoginStatus(authentication.getName(), successView);
            OutputStream out = response.getOutputStream();
            mapper.writeValue(out, status);

            out.flush();
            out.close();
        } else {
            super.onAuthenticationSuccess(request, response, authentication);
        }

    }

    private String getUserProperty(String userPropertyName) {
        String propertyValue = "";
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.getPrincipal() != null) {
            User user = (User) authentication.getPrincipal();
            Map<String, UserPropertyDefinition> propsMap = user.getPropertyDefinitions();
            // why not use propsMap.get(userPropertyName).getValue().getInternalValue() directly?
            if (!MapUtils.isEmpty(propsMap)) {
                for (String key : propsMap.keySet()) {
                    if (userPropertyName.equals(key)) {
                        propertyValue = propsMap.get(key).getValue().getInternalValue();
                        break;
                    }
                }
            }
        }
        return propertyValue;
    }

}
