package com.backbase.launchpad.retail.security;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationServiceException;

import com.backbase.launchpad.retail.security.model.Customer;
import com.backbase.portal.foundation.business.service.GroupBusinessService;
import com.backbase.portal.foundation.business.service.UserBusinessService;
import com.backbase.portal.foundation.commons.exceptions.FoundationException;
import com.backbase.portal.foundation.commons.exceptions.FoundationRuntimeException;
import com.backbase.portal.foundation.commons.exceptions.ItemNotFoundException;
import com.backbase.portal.foundation.domain.conceptual.LongPropertyValue;
import com.backbase.portal.foundation.domain.conceptual.StringPropertyValue;
import com.backbase.portal.foundation.domain.conceptual.UserPropertyDefinition;
import com.backbase.portal.foundation.domain.model.Group;
import com.backbase.portal.foundation.domain.model.User;

/**
 * Helper class used to retrieve an existing Portal user or
 * create a new with the default group(s) and properties
 *
 * @author A.Lapusta
 * @author V.Goldin
 */
public class PortalSecurityHelper {
    private static final Logger log = LoggerFactory.getLogger(PortalSecurityHelper.class);
    private static final String USER_GROUP_NAME = "user";

    @Autowired
    private UserBusinessService userBusinessService;

    @Autowired
    private GroupBusinessService groupBusinessService;

    public User retrieveOrCreateUser(Customer customer, String username, String workflowId) {
        User user;

        try {
            user = userBusinessService.getUser(username);

            // -- configure propertied for the existing user, in case she does not have them defined
            configureProperties(user, customer, workflowId);
        } catch (ItemNotFoundException e) {
            user = new User();
            user.setEnabled(true);
            user.setUsername(username);

            // we are setting the workflowId into the user password in order to prevent customer from logging in
            user.setPassword(workflowId);
            user.setGroups(prepareUserGroups());
            configureProperties(user, customer, workflowId);

            try {
                log.info("Creating portal user: " + username);

                user = userBusinessService.createUser(user);
            } catch (FoundationException ex) {
                throw new AuthenticationServiceException("Error creating user: " + username, ex);
            }
        }

        return user;
    }

    private List<Group> prepareUserGroups() {
        List<Group> groups = new ArrayList<Group>();
        groups.add(retrieveGroup(USER_GROUP_NAME));

        return groups;
    }

    private Group retrieveGroup(String groupName) {
        Group group;
        try {
            group = groupBusinessService.getGroup(groupName);
        } catch (ItemNotFoundException e) {
            throw new FoundationRuntimeException(e);
        }

        return group;
    }

    private void configureProperties(User user, Customer customer, String workflowId) {
        Map<String,UserPropertyDefinition> propertyDefinitions = user.getPropertyDefinitions();
        propertyDefinitions.put("workflowId", new UserPropertyDefinition("workflowId", new StringPropertyValue(workflowId)));
        propertyDefinitions.put("customerId", new UserPropertyDefinition("customerId", new LongPropertyValue(
                Long.valueOf(customer.getId()))));
    }


}
