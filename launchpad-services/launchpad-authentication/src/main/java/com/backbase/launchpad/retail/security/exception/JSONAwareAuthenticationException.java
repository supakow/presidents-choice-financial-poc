package com.backbase.launchpad.retail.security.exception;

import net.sf.json.JSONObject;
import org.springframework.security.core.AuthenticationException;

public class JSONAwareAuthenticationException extends AuthenticationException {
    private final JSONObject json;

    private JSONAwareAuthenticationException(JSONObject jsonObject) {
        super(jsonObject.toString());

        this.json = jsonObject;
    }

    public static JSONAwareAuthenticationException from(JSONObject json) {
        return new JSONAwareAuthenticationException(json);
    }

    public String toJSONString() {
        return json.toString();
    }
}
