package com.backbase.launchpad.retail.security.successview;

/**
 * Specifies the success view contract after a successful login.
 *
 * @author Zoltan Altfatter
 */
public interface SuccessViewResolver {

    /**
     * Returns the URL where the login widgets needs to redirect after a successful login.
     *
     * @param portalName a portal name
     * @param pageName a page name of the specified portal
     * @return a the URL to redirect as a String
     */
    String getSuccessView(String portalName, String pageName);

}
