package com.backbase.launchpad.retail.security.model;


/**
 * Represents a banking customer.
 */
public class Customer {
    private String id;
    private String username;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public static enum Gender {
        MALE, FEMALE
    }
}
