package com.backbase.launchpad.retail.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;

import com.backbase.launchpad.retail.security.model.Customer;
import com.backbase.portal.foundation.domain.model.User;

public class RemoteServiceAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

	@Autowired
	private RemoteAuthenticationService remoteAuthenticationService;

    @Autowired
    private PortalSecurityHelper portalSecurity;

	@Override
	protected void additionalAuthenticationChecks(UserDetails userDetails,
            UsernamePasswordAuthenticationToken authentication)
			throws AuthenticationException {
		// -- do nothing
	}

	@Override
	protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication)
			throws AuthenticationException {

        String workflowId = authentication.getCredentials().toString();

        Customer customer;
        try {
            customer = remoteAuthenticationService.verifySession(workflowId);
        } catch (AuthenticationException ex) {
            throw ex;
        } catch (RuntimeException ex) {
            String message = "Authentication is failed due to an unexpected remote service exception.";
            if (logger.isErrorEnabled()) {
                logger.error(message, ex);
            }

            throw new AuthenticationServiceException(message, ex);
        }

        if (customer == null) {
            throw new AuthenticationServiceException("Authentication is failed. Customer information is not available.");
        }

        User user = portalSecurity.retrieveOrCreateUser(customer, username, workflowId);

        return user;
	}

}
