package com.backbase.launchpad.retail.security;

import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

import java.util.HashMap;
import java.util.Map;

import org.apache.camel.CamelExecutionException;
import org.apache.camel.EndpointInject;
import org.apache.camel.ProducerTemplate;

import com.backbase.launchpad.retail.security.exception.JSONAwareAuthenticationException;
import com.backbase.launchpad.retail.security.exception.RemoteUserServiceException;
import com.backbase.launchpad.retail.security.model.Customer;

public class RemoteAuthenticationService {
    private static final String ATTRIBUTE_ID = "id";
    private static final String ATTRIBUTE_USERNAME = "username";

    private static final String WORKFLOW_ID_HEADER = "id";
    private static final String HTTP_SESSION_ID_HEADER = "BackbaseUserSessionId";

    @EndpointInject(uri = "seda:cross-channel/authentication/session/verify")
    private ProducerTemplate verifyEndpoint;

    @EndpointInject(uri = "seda:cross-channel/authentication/session/confirm")
    private ProducerTemplate confirmEndpoint;

    @EndpointInject(uri = "seda:cross-channel/authentication/session/queryInitiation")
    private ProducerTemplate initiationTimeEndpoint;


    /**
     * This method is used to "confirm" the pre-authenticated session. Maps the HTTP session id to the workflow id.
     *
     * @param workflowId the workflow id
     * @param httpSessionId the http session id
     * @return a {@link Customer} instance
     */
    public void confirmSession(String workflowId, String httpSessionId) {
        Map<String, Object> headers = new HashMap<String, Object>();
        headers.put(WORKFLOW_ID_HEADER, workflowId);
        headers.put(HTTP_SESSION_ID_HEADER, httpSessionId);

        confirmEndpoint.sendBodyAndHeaders(null, headers);
    }

    /**
     * Verify the session identified by the workflowId received as parameter.
     *
     * @param workflowId the workflow id
     * @return a {@link Customer} instance
     */
    public Customer verifySession(String workflowId) {
        Map<String, Object> headers = new HashMap<String, Object>();
        headers.put(WORKFLOW_ID_HEADER, workflowId);

        return executeRequestAndPopulateCustomer(verifyEndpoint, headers);
    }

    private Customer executeRequestAndPopulateCustomer(ProducerTemplate producer, Map<String, Object> headers) {
        Customer customer = null;
        Object json = null;

        try {
            json = producer.requestBodyAndHeaders(null, headers);
        } catch (CamelExecutionException ex) {
            throw new RemoteUserServiceException(ex);
        }

        if (json != null) {
            JSONObject verifyResult = (JSONObject) JSONSerializer.toJSON(json);
            if (verifyResult.containsKey("errors")) {
                throw JSONAwareAuthenticationException.from(verifyResult);
            }
            customer = getCustomer(verifyResult);
        }

        return customer;
    }

    private Customer getCustomer(JSONObject verifyResult) {
        Customer customer = new Customer();
        customer.setId((String) verifyResult.get(ATTRIBUTE_ID));
        customer.setUsername((String) verifyResult.get(ATTRIBUTE_USERNAME));

        return customer;
    }

}
