package com.backbase.launchpad.services.salesandservices.servicing.automation;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.List;

import static com.backbase.launchpad.services.salesandservices.servicing.automation.Automation.Action;
import static com.backbase.launchpad.services.salesandservices.servicing.automation.Automation.Action.ActionType.EMAIL;
import static com.backbase.launchpad.services.salesandservices.servicing.automation.Automation.Trigger;
import static com.backbase.launchpad.services.salesandservices.servicing.automation.Automation.Trigger.Direction.LESS;
import static com.backbase.launchpad.services.salesandservices.servicing.automation.Automation.Trigger.Direction.MORE_THAN_EQUAL;
import static com.backbase.launchpad.services.salesandservices.servicing.automation.Automation.Trigger.TriggerType.TRANSACTION_AMOUNT;
import static org.junit.Assert.*;

public class InMemoryAutomationRepositoryTest {

    // under test
    private InMemoryAutomationRepository repository;

    private static final String CUSTOMER_ID = "1234";

    @Before
    public void setUp() {
        repository = new InMemoryAutomationRepository();
    }

    @Test
    public void findOne() {
        assertNull(repository.findOne(CUSTOMER_ID, "NOT_EXISTING_ID"));
    }

    @Test
    public void update() {

        Automation savedAutomation = repository.save(CUSTOMER_ID, createAutomation());

        Automation one = repository.findOne(CUSTOMER_ID, savedAutomation.getId());

        assertTrue(one.isEnabled());
        assertEquals(new BigDecimal(99.99), one.getTrigger().getAmount());
        assertEquals(LESS, one.getTrigger().getDirection());
        assertEquals("hello@gmail.com", one.getAction().getValue());

        one.setEnabled(false);
        one.getTrigger().setAmount(new BigDecimal(199.99));
        one.getTrigger().setDirection(MORE_THAN_EQUAL);
        one.getAction().setValue("hoi@gmail.com");

        repository.save(CUSTOMER_ID, one);

        assertFalse(one.isEnabled());
        assertEquals(new BigDecimal(199.99), one.getTrigger().getAmount());
        assertEquals(MORE_THAN_EQUAL, one.getTrigger().getDirection());
        assertEquals("hoi@gmail.com", one.getAction().getValue());

    }

    @Test
    public void save() {

        List<Automation> automations = repository.findAll(CUSTOMER_ID);
        assertTrue(automations.isEmpty());

        Automation saved = repository.save(CUSTOMER_ID, createAutomation());

        Automation automation = repository.findOne(CUSTOMER_ID, saved.getId());

        assertNotNull(automation.getId());
        assertEquals(0, automation.getNrOfExecutions());
        assertNull(automation.getLastExecution());
        assertEquals("Balance notifier", automation.getName());
        assertTrue(automation.isEnabled());

        assertEquals(automation.getTrigger().getAmount(), new BigDecimal(99.99));
        assertEquals(LESS, automation.getTrigger().getDirection());
        assertEquals(TRANSACTION_AMOUNT, automation.getTrigger().getType());
        assertEquals("123456789", automation.getTrigger().getAccountId());

        assertEquals(EMAIL, automation.getAction().getType());
        assertEquals("hello@gmail.com", automation.getAction().getValue());

    }

    private Automation createAutomation() {
        Automation automation = new Automation();
        automation.setName("Balance notifier");
        automation.setEnabled(true);

        Trigger trigger = new Trigger();
        trigger.setAmount(new BigDecimal(99.99));
        trigger.setDirection(LESS);
        trigger.setType(TRANSACTION_AMOUNT);
        trigger.setAccountId("123456789");
        automation.setTrigger(trigger);

        Action action = new Action();
        action.setType(EMAIL);
        action.setValue("hello@gmail.com");
        automation.setAction(action);

        return automation;
    }

}
