package com.backbase.launchpad.services.salesandservices.servicing.automation;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class AutomationTest {

    // under test
    private Automation automation;

    @Before
    public void setUp() {
        automation = new Automation();
    }

    @Test
    public void trigger() {
        assertNotNull(automation.getTrigger());
    }

    @Test
    public void action() {
        assertNotNull(automation.getAction());
    }

}
