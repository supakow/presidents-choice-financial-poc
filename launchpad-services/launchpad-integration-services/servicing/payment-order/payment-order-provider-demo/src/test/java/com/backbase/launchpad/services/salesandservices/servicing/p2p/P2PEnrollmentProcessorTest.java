package com.backbase.launchpad.services.salesandservices.servicing.p2p;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:META-INF/spring/p2p-config.xml")
public class P2PEnrollmentProcessorTest {

    @Autowired
    private P2PEnrollmentProcessor enrollmentProcessor;

    @Autowired
    private P2PEnrollmentRepository enrollmentRepository;

    private Exchange exchange;
    private Message in;
    private Message out;

    @Before
    public void setUp() {
        exchange = mock(Exchange.class);
        in = mock(Message.class);
        out = mock(Message.class);
    }

    @After
    public void tearDown() throws P2PEnrollmentNotFoundException {
        enrollmentRepository.delete("3");
    }

    @Test
    public void correctEmailAddress() throws Exception {

        when(in.getBody(String.class)).thenReturn("" +
                "{\"verification\": " +
                "   {\"email\":\"walter.white@gmail.com\", \"code\":\"1234\"} " +
                "}");
        when(exchange.getIn()).thenReturn(in);
        when(exchange.getOut()).thenReturn(out);
        when(exchange.getIn().getHeader(Exchange.HTTP_METHOD)).thenReturn("PUT");
        when(exchange.getIn().getHeader("backbasepartyId")).thenReturn("3");
        enrollmentRepository.create("3", new P2PEnrollment("walter.white@gmail.com", "666"));

        assertFalse(enrollmentRepository.find("3").isEmailVerified());
        enrollmentProcessor.process(exchange);
        verify(out).setBody("{\"status\":\"Ok\"}");
        assertTrue(enrollmentRepository.find("3").isEmailVerified());
    }

    @Test
    public void incorrectEmailAddress() throws Exception {
        when(in.getBody(String.class)).thenReturn("" +
                "{\"verification\": " +
                "   {\"email\":\"walter.white@gmail.com\", \"code\":\"5678\"} " +
                "}");
        when(exchange.getIn()).thenReturn(in);
        when(exchange.getOut()).thenReturn(out);
        when(exchange.getIn().getHeader(Exchange.HTTP_METHOD)).thenReturn("PUT");
        when(exchange.getIn().getHeader("backbasepartyId")).thenReturn("3");
        enrollmentRepository.create("3", new P2PEnrollment("walter.white@gmail.com", "666"));

        enrollmentProcessor.process(exchange);
        verify(out).setBody("{\"error\": \"wrong verification code\"}");
        verify(out).setHeader(Exchange.HTTP_RESPONSE_CODE, 409);
        assertFalse(enrollmentRepository.find("3").isEmailVerified());
    }

}
