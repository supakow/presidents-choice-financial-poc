package com.backbase.launchpad.services.salesandservices.servicing;

import com.backbase.launchpad.services.salesandservices.servicing.model.PaymentMode;
import com.backbase.launchpad.services.salesandservices.servicing.model.PaymentOrderType;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.test.junit4.CamelSpringTestSupport;
import org.junit.Before;
import org.junit.Test;
import org.restlet.data.Method;
import org.springframework.context.support.AbstractXmlApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.xmldb.api.DatabaseManager;
import org.xmldb.api.base.Collection;
import org.xmldb.api.modules.CollectionManagementService;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Integration test for payment order services.
 */
public class PaymentOrderRouteTest extends CamelSpringTestSupport {

    private static final long SCHEDULED_PAYMENT_START_DATE = 1396968035261L;
    private static final long SCHEDULED_PAYMENT_END_DATE = 1397968035261L;

    @Override
    public boolean isCreateCamelContextPerClass() {
        return true;
    }

    @Override
    protected CamelContext createCamelContext() throws Exception {
        CamelContext camelContext = super.createCamelContext();

        RouteBuilder dataPreLoadRoute = new RouteBuilder() {
            @Override
            public void configure() throws Exception {
                from("direct:loadData")
                    .recipientList().simple("${header.XMLDBURL}");
            }
        };

        dataPreLoadRoute.addRoutesToCamelContext(camelContext);

        return camelContext;
    }

    @Override
    protected AbstractXmlApplicationContext createApplicationContext() {
        return new ClassPathXmlApplicationContext(new String[]
                {"META-INF/spring/backbase-mashup-service.xml"});
    }

    @Override
    protected RouteBuilder createRouteBuilder() throws Exception {
        return getMandatoryBean(PaymentOrderRoute.class, "PaymentOrderRoute");
    }

    @Test
    public void authenticatePaymentOrderRoute() {
        String id = UUID.randomUUID().toString();

        template.sendBodyAndHeader("direct:loadData", "<o><id>"+id+"</id><accountId>1</accountId><status>Received</status></o>", "XMLDBURL", PaymentOrderRoute.XMLDB_STORE_PAYMENT_ORDER_EP);

        Map<String, Object> headers = new HashMap<String, Object>();
        headers.put(Exchange.HTTP_METHOD, Method.PUT);
        headers.put(Exchange.CONTENT_TYPE, "application/x-www-form-urlencoded; charset=UTF-8");
        headers.put("id", id);
        headers.put("authenticated", true);

        Object payment = template.requestBodyAndHeaders(PaymentOrderRoute.PAYMENT_ORDERS_INSTANCE_ENDPOINT, "", headers);
        assertEquals("{\"status\" : \"Pending\"}", payment);
    }

    @Test
    public void retrievePaymentOrder() throws Exception {
        template.sendBodyAndHeader("direct:loadData", "<o><id>1</id><accountId>1</accountId></o>", "XMLDBURL", PaymentOrderRoute.XMLDB_STORE_PAYMENT_ORDER_EP);

        Map<String, Object> headers = new HashMap<String, Object>();
        headers.put(Exchange.HTTP_METHOD, Method.GET);
        headers.put("id", "1");

        Object response = template.requestBodyAndHeaders(PaymentOrderRoute.PAYMENT_ORDERS_INSTANCE_ENDPOINT, "", headers);
        assertEquals("{\"id\":\"1\",\"accountId\":\"1\"}", bytesToString(response));
    }

    private String bytesToString(Object response) {
        return new String((byte[]) response);
    }

    @Test
    public void retrievePaymentOrdersWithOneAccount() throws Exception {
        cleanUpDatabase();

        template.sendBodyAndHeader("direct:loadData", "<o><partyId>1</partyId><id>1</id></o>", "XMLDBURL", PaymentOrderRoute.XMLDB_DEFAULT_COLLECTION_EP + "/accounts?username=admin&password=");
        template.sendBodyAndHeader("direct:loadData", "<o><id type=\"string\">1</id><accountId type=\"string\">1</accountId><status type=\"string\">Received</status><initiationDateTime type=\"number\">123123123</initiationDateTime></o>", "XMLDBURL", PaymentOrderRoute.XMLDB_STORE_PAYMENT_ORDER_EP);
        template.sendBodyAndHeader("direct:loadData", "<o><id type=\"string\">2</id><accountId type=\"string\">1</accountId><status type=\"string\">Received</status><initiationDateTime type=\"number\">123123123</initiationDateTime></o>", "XMLDBURL", PaymentOrderRoute.XMLDB_STORE_PAYMENT_ORDER_EP);

        Object response = template.requestBodyAndHeader(PaymentOrderRoute.PAYMENT_ORDERS_COLLECTION_ENDPOINT,
                "", "BackbasePartyId", "1");

        String r = new String((byte[]) response);

        JSONArray result = (JSONArray) JSONSerializer.toJSON(r);
        JSONObject account = result.getJSONObject(0);
        String accountId = account.getString("@id");

        assertEquals("number of accounts", 1, result.size());
        assertNotNull("account id is provided", accountId);

        JSONArray paymentOrders = account.getJSONArray("paymentOrders");
        assertEquals("number of payments", 2, paymentOrders.size());

        assertNotNull("payment id is provided", paymentOrders.getJSONObject(0).get("id"));
        assertEquals("payment account id is equals account id", accountId, paymentOrders.getJSONObject(0).getString("accountId"));
        assertEquals("payment account id is equals account id", accountId, paymentOrders.getJSONObject(1).getString("accountId"));
    }

    @Test
    public void retrievePaymentOrdersWithTwoAccounts() throws Exception {
        cleanUpDatabase();

        template.sendBodyAndHeader("direct:loadData", "<o><partyId>1</partyId><id>1</id></o>", "XMLDBURL", PaymentOrderRoute.XMLDB_DEFAULT_COLLECTION_EP + "/accounts?username=admin&password=");
        template.sendBodyAndHeader("direct:loadData", "<o><partyId>1</partyId><id>2</id></o>", "XMLDBURL", PaymentOrderRoute.XMLDB_DEFAULT_COLLECTION_EP + "/accounts?username=admin&password=");
        template.sendBodyAndHeader("direct:loadData", "<o><id type=\"string\">1</id><accountId type=\"string\">1</accountId><status type=\"string\">Received</status><initiationDateTime type=\"number\">123123123</initiationDateTime></o>", "XMLDBURL", PaymentOrderRoute.XMLDB_STORE_PAYMENT_ORDER_EP);
        template.sendBodyAndHeader("direct:loadData", "<o><id type=\"string\">2</id><accountId type=\"string\">2</accountId><status type=\"string\">Received</status><initiationDateTime type=\"number\">123123123</initiationDateTime></o>", "XMLDBURL", PaymentOrderRoute.XMLDB_STORE_PAYMENT_ORDER_EP);
        template.sendBodyAndHeader("direct:loadData", "<o><id type=\"string\">3</id><accountId type=\"string\">2</accountId><status type=\"string\">Received</status><initiationDateTime type=\"number\">123123123</initiationDateTime></o>", "XMLDBURL", PaymentOrderRoute.XMLDB_STORE_PAYMENT_ORDER_EP);

        Object response = template.requestBodyAndHeader(PaymentOrderRoute.PAYMENT_ORDERS_COLLECTION_ENDPOINT,
                "", "BackbasePartyId", "1");

        String r = new String((byte[]) response);

        JSONArray result = (JSONArray) JSONSerializer.toJSON(r);
        JSONObject firstAccount = result.getJSONObject(0);
        JSONObject secondAccount = result.getJSONObject(1);

        String firstAccountId = firstAccount.getString("@id");
        String secondAccountId = secondAccount.getString("@id");

        assertEquals("number of accounts", 2, result.size());
        assertNotNull("account id is provided [1]", firstAccount);
        assertNotNull("account id is provided [2]", secondAccount);

        JSONArray paymentOrdersForFirstAccount = firstAccount.getJSONArray("paymentOrders");
        assertEquals("number of payments", 1, paymentOrdersForFirstAccount.size());

        JSONArray paymentOrdersForSecondAccount = secondAccount.getJSONArray("paymentOrders");
        assertEquals("number of payments", 2, paymentOrdersForSecondAccount.size());

        assertEquals("payment account id is equals account id [1]", firstAccountId, paymentOrdersForFirstAccount.getJSONObject(0).getString("accountId"));
        assertEquals("payment account id is equals account id [2]", secondAccountId, paymentOrdersForSecondAccount.getJSONObject(0).getString("accountId"));
        assertEquals("payment account id is equals account id [3]", secondAccountId, paymentOrdersForSecondAccount.getJSONObject(1).getString("accountId"));
    }

    @Test
    public void initiateBasicPaymentOrder() {
        cleanUpDatabase();

        String id = UUID.randomUUID().toString();
        Map<String, Object> headers = new HashMap<String, Object>();
        populateBasicData(headers);
        headers.put("paymentMode", PaymentMode.NON_RECURRING);
        headers.put("id", id);

        StringBuffer expected = constructExpectedPaymentOrder(id);

        template.requestBodyAndHeaders(PaymentOrderRoute.PAYMENT_ORDERS_INSTANCE_ENDPOINT, "", headers);
        Object payment = template.requestBodyAndHeader(PaymentOrderRoute.RETRIEVE_PAYMENT_ORDER_EP, "", "id", id);

        String actual = prepareResponseForAssertion(payment);

        assertEquals(expected.toString(), actual);
    }

    @Test
    public void initiateNonRecurringPaymentOrder() {
        String id = UUID.randomUUID().toString();

        Map<String, Object> headers = new HashMap<String, Object>();
        populateBasicData(headers);
        headers.put("id", id);
        headers.put("paymentMode", PaymentMode.NON_RECURRING);

        Object response = template.requestBodyAndHeaders(PaymentOrderRoute.PAYMENT_ORDERS_INSTANCE_ENDPOINT, "", headers);
        assertEquals("{\"status\" : \"Received\"}", response);

        StringBuffer expected = constructExpectedNonRecurringPaymentOrder(id);

        Object payment = template.requestBodyAndHeader(PaymentOrderRoute.RETRIEVE_PAYMENT_ORDER_EP, "", "id", id);

        String actual = prepareResponseForAssertion(payment);
        assertEquals(expected.toString(), actual);
    }

    @Test
    public void initiateWeeklyRecurringPaymentOrder() {
        String id = UUID.randomUUID().toString();

        Map<String, Object> headers = new HashMap<String, Object>();
        populateBasicData(headers);
        headers.put("id", id);
        headers.put("paymentMode", PaymentMode.RECURRING);
        headers.put("scheduledTransfer[frequency]", "WEEKLY");
        headers.put("scheduledTransfer[intervals]", "1,2,5");
        headers.put("scheduledTransfer[startdate]", SCHEDULED_PAYMENT_START_DATE);
        headers.put("scheduledTransfer[enddate]", SCHEDULED_PAYMENT_END_DATE);
        headers.put("scheduledTransfer[every]", "1");

        Object response = template.requestBodyAndHeaders(PaymentOrderRoute.PAYMENT_ORDERS_INSTANCE_ENDPOINT, "", headers);
        assertEquals("{\"status\" : \"Received\"}", response);

        String expected =
                    "{" +
                        "\"accountId\":\"#accountId#\"," +
                        "\"accountName\":\"#accountName#\"," +
                        "\"counterpartyAccount\":\"#counterpartyAccount#\"," +
                        "\"counterpartyEmail\":\"#counterpartyEmail#\"," +
                        "\"counterpartyIban\":\"#counterpartyIban#\"," +
                        "\"counterpartyName\":\"#counterpartyName#\"," +
                        "\"creditorReference\":\"#creditorReference#\"," +
                        "\"executionDateTime\":0," +
                        "\"id\":\"" + id + "\"," +
                        "\"initiationDateTime\":#dummy#," +
                        "\"instructedAmount\":123.12," +
                        "\"instructedCurrency\":\"#instructedCurrency#\"," +
                        "\"paymentCategory\":\"#paymentCategory#\"," +
                        "\"remittanceInformation\":\"#remittanceInformation#\"," +
                        "\"schedule\":{" +
                            "\"endDate\":1.397968035261E12," +
                            "\"every\":1," +
                            "\"frequency\":\"WEEKLY\"," +
                            "\"intervals\":\"1,2,5\"," +
                            "\"startDate\":1.396968035261E12" +
                        "}," +
                        "\"status\":\"Received\"," +
                        "\"type\":\"BANK\"" +
                        "}";


        Object payment = template.requestBodyAndHeader(PaymentOrderRoute.RETRIEVE_PAYMENT_ORDER_EP, "", "id", id);
        String actual = prepareResponseForAssertion(payment);
        assertEquals(expected.toString(), actual);
    }

    @Test
    public void initiateMonthlyRecurringPaymentOrder() {
        String id = UUID.randomUUID().toString();

        Map<String, Object> headers = new HashMap<String, Object>();
        populateBasicData(headers);
        headers.put("id", id);
        headers.put("paymentMode", PaymentMode.RECURRING);
        headers.put("scheduledTransfer[frequency]", "MONTHLY");
        headers.put("scheduledTransfer[intervals]", "1,2,5,10,21");
        headers.put("scheduledTransfer[startdate]", SCHEDULED_PAYMENT_START_DATE);
        headers.put("scheduledTransfer[enddate]", SCHEDULED_PAYMENT_END_DATE);
        headers.put("scheduledTransfer[every]", "1");

        Object response = template.requestBodyAndHeaders(PaymentOrderRoute.PAYMENT_ORDERS_INSTANCE_ENDPOINT, "", headers);
        assertEquals("{\"status\" : \"Received\"}", response);

        String expected =
                    "{" +
                        "\"accountId\":\"#accountId#\"," +
                        "\"accountName\":\"#accountName#\"," +
                        "\"counterpartyAccount\":\"#counterpartyAccount#\"," +
                        "\"counterpartyEmail\":\"#counterpartyEmail#\"," +
                        "\"counterpartyIban\":\"#counterpartyIban#\"," +
                        "\"counterpartyName\":\"#counterpartyName#\"," +
                        "\"creditorReference\":\"#creditorReference#\"," +
                        "\"executionDateTime\":0," +
                        "\"id\":\"" + id + "\"," +
                        "\"initiationDateTime\":#dummy#," +
                        "\"instructedAmount\":123.12," +
                        "\"instructedCurrency\":\"#instructedCurrency#\"," +
                        "\"paymentCategory\":\"#paymentCategory#\"," +
                        "\"remittanceInformation\":\"#remittanceInformation#\"," +
                        "\"schedule\":{" +
                            "\"endDate\":1.397968035261E12," +
                            "\"every\":1," +
                            "\"frequency\":\"MONTHLY\"," +
                            "\"intervals\":\"1,2,5,10,21\"," +
                            "\"startDate\":1.396968035261E12" +
                        "}," +
                        "\"status\":\"Received\"," +
                        "\"type\":\"BANK\"" +
                    "}";

        Object payment = template.requestBodyAndHeader(PaymentOrderRoute.RETRIEVE_PAYMENT_ORDER_EP, "", "id", id);
        String actual = prepareResponseForAssertion(payment);
        assertEquals(expected.toString(), actual);
    }

    @Test
    public void initiateYearlyRecurringPaymentOrder() {
        String id = UUID.randomUUID().toString();

        Map<String, Object> headers = new HashMap<String, Object>();
        populateBasicData(headers);
        headers.put("id", id);
        headers.put("paymentMode", PaymentMode.RECURRING);
        headers.put("scheduledTransfer[frequency]", "YEARLY");
        headers.put("scheduledTransfer[intervals]", "1,2,5,10,12");
        headers.put("scheduledTransfer[startdate]", SCHEDULED_PAYMENT_START_DATE);
        headers.put("scheduledTransfer[enddate]", SCHEDULED_PAYMENT_END_DATE);
        headers.put("scheduledTransfer[every]", "1");

        Object response = template.requestBodyAndHeaders(PaymentOrderRoute.PAYMENT_ORDERS_INSTANCE_ENDPOINT, "", headers);
        assertEquals("{\"status\" : \"Received\"}", response);

        String expected =
                    "{" +
                        "\"accountId\":\"#accountId#\"," +
                        "\"accountName\":\"#accountName#\"," +
                        "\"counterpartyAccount\":\"#counterpartyAccount#\"," +
                        "\"counterpartyEmail\":\"#counterpartyEmail#\"," +
                        "\"counterpartyIban\":\"#counterpartyIban#\"," +
                        "\"counterpartyName\":\"#counterpartyName#\"," +
                        "\"creditorReference\":\"#creditorReference#\"," +
                        "\"executionDateTime\":0," +
                        "\"id\":\"" + id + "\"," +
                        "\"initiationDateTime\":#dummy#," +
                        "\"instructedAmount\":123.12," +
                        "\"instructedCurrency\":\"#instructedCurrency#\"," +
                        "\"paymentCategory\":\"#paymentCategory#\"," +
                        "\"remittanceInformation\":\"#remittanceInformation#\"," +
                        "\"schedule\":{" +
                            "\"endDate\":1.397968035261E12," +
                            "\"every\":1," +
                            "\"frequency\":\"YEARLY\"," +
                            "\"intervals\":\"1,2,5,10,12\"," +
                            "\"startDate\":1.396968035261E12" +
                        "}," +
                        "\"status\":\"Received\"," +
                        "\"type\":\"BANK\"" +
                    "}";


        Object payment = template.requestBodyAndHeader(PaymentOrderRoute.RETRIEVE_PAYMENT_ORDER_EP, "", "id", id);
        String actual = prepareResponseForAssertion(payment);
        assertEquals(expected.toString(), actual);
    }

    private StringBuffer constructExpectedNonRecurringPaymentOrder(String id) {
        StringBuffer expected = new StringBuffer();
        expected.append("{");
        expected.append("\"accountId\":\"#accountId#\",");
        expected.append("\"accountName\":\"#accountName#\",");
        expected.append("\"counterpartyAccount\":\"#counterpartyAccount#\",");
        expected.append("\"counterpartyEmail\":\"#counterpartyEmail#\",");
        expected.append("\"counterpartyIban\":\"#counterpartyIban#\",");
        expected.append("\"counterpartyName\":\"#counterpartyName#\",");
        expected.append("\"creditorReference\":\"#creditorReference#\",");
        expected.append("\"executionDateTime\":0,");
        expected.append("\"id\":\""+id+"\",");
        expected.append("\"initiationDateTime\":#dummy#,");
        expected.append("\"instructedAmount\":123.12,");
        expected.append("\"instructedCurrency\":\"#instructedCurrency#\",");
        expected.append("\"paymentCategory\":\"#paymentCategory#\",");
        expected.append("\"remittanceInformation\":\"#remittanceInformation#\",");
        expected.append("\"schedule\":null,");
        expected.append("\"status\":\"Received\",");
        expected.append("\"type\":\"BANK\"");
        expected.append("}");

        return expected;
    }

    private void populateBasicData(Map<String, Object> headers) {
        headers.put(Exchange.HTTP_METHOD, Method.POST);
        headers.put("accountId","#accountId#");
        headers.put("accountName","#accountName#");
        headers.put("counterpartyName","#counterpartyName#");
        headers.put("counterpartyAccount","#counterpartyAccount#");
        headers.put("counterpartyEmail","#counterpartyEmail#");
        headers.put("counterpartyIban","#counterpartyIban#");
        headers.put("instructedAmount","123.12");
        headers.put("instructedCurrency","#instructedCurrency#");
        headers.put("remittanceInformation","#remittanceInformation#");
        headers.put("creditorReference","#creditorReference#");
        headers.put("paymentCategory","#paymentCategory#");
        headers.put("type", PaymentOrderType.BANK.toString());
    }

    private StringBuffer constructExpectedPaymentOrder(String id) {
        StringBuffer expected = new StringBuffer();
        expected.append("{");
        expected.append("\"accountId\":\"#accountId#\",");
        expected.append("\"accountName\":\"#accountName#\",");
        expected.append("\"counterpartyAccount\":\"#counterpartyAccount#\",");
        expected.append("\"counterpartyEmail\":\"#counterpartyEmail#\",");
        expected.append("\"counterpartyIban\":\"#counterpartyIban#\",");
        expected.append("\"counterpartyName\":\"#counterpartyName#\",");
        expected.append("\"creditorReference\":\"#creditorReference#\",");
        expected.append("\"executionDateTime\":0,");
        expected.append("\"id\":\""+id+"\",");
        expected.append("\"initiationDateTime\":#dummy#,");
        expected.append("\"instructedAmount\":123.12,");
        expected.append("\"instructedCurrency\":\"#instructedCurrency#\",");
        expected.append("\"paymentCategory\":\"#paymentCategory#\",");
        expected.append("\"remittanceInformation\":\"#remittanceInformation#\",");
        expected.append("\"schedule\":null,");
        expected.append("\"status\":\"Received\",");
        expected.append("\"type\":\"BANK\"");
        expected.append("}");

        return expected;
    }

    private String prepareResponseForAssertion(Object response) {
        String actual = null;
        if (response instanceof byte[]) {
            actual = bytesToString(response);

            actual = replaceInitiationDateTimeWithDummyValue(actual);
        }
        return actual;
    }

    private String replaceInitiationDateTimeWithDummyValue(String actual) {
        actual = actual.replaceAll("(.*\"initiationDateTime\":)(.*?)(,.*)", "$1#dummy#$3");
        return actual;
    }

    @Before
    public void cleanUpDatabase() {
        try {
            Collection parentCollection = DatabaseManager.getCollection("xmldb:exist:///db", "admin", "");
            CollectionManagementService mgt = (CollectionManagementService) parentCollection.getService("CollectionManagementService", "1.0");

            mgt.removeCollection("launchpad");
            parentCollection.close();
        } catch (Exception ex) {
            //IGNORE
        }
    }
}
