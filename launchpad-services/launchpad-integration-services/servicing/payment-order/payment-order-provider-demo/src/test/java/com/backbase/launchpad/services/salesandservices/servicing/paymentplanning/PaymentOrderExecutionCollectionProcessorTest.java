package com.backbase.launchpad.services.salesandservices.servicing.paymentplanning;

import com.backbase.launchpad.services.salesandservices.servicing.model.Frequency;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class PaymentOrderExecutionCollectionProcessorTest {

    // under test
    private PaymentOrderExecutionCollectionProcessor processor;

    @Before
    public void setUp() {
        processor = new PaymentOrderExecutionCollectionProcessor();
    }

    @Test
    public void translateToPaymentOrderExecutionsWithWeeklyFrequency() {

        PaymentOrder paymentOrder = new PaymentOrder();
        paymentOrder.setFrequency(Frequency.WEEKLY);
        paymentOrder.setEvery(2);
        paymentOrder.setStartDate(new DateTime("2014-05-07"));
        paymentOrder.setEndDate(new DateTime("2014-05-21"));
        paymentOrder.setIntervals("1,3,7");

        List<PaymentOrderExecution> paymentOrderExecutions = processor.translateToPaymentOrderExecutions(paymentOrder);

        assertTrue(paymentOrderExecutions.size() == 4);

        for (PaymentOrderExecution paymentOrderExecution : paymentOrderExecutions) {
            LocalDate date = paymentOrderExecution.getDate();
            assertTrue(date.equals(new LocalDate("2014-05-12")) ||
                    date.equals(new LocalDate("2014-05-07")) ||
                    date.equals(new LocalDate("2014-05-11")) ||
                    date.equals(new LocalDate("2014-05-21")));
        }
    }


    @Test
    public void translateToPaymentOrderExecutionsWithMonthlyFrequency() {

        PaymentOrder paymentOrder = new PaymentOrder();
        paymentOrder.setFrequency(Frequency.MONTHLY);
        paymentOrder.setEvery(2);
        paymentOrder.setStartDate(new DateTime("2014-05-07"));
        paymentOrder.setEndDate(new DateTime("2014-07-21"));
        paymentOrder.setIntervals("5,7,13");

        List<PaymentOrderExecution> paymentOrderExecutions = processor.translateToPaymentOrderExecutions(paymentOrder);

        assertTrue(paymentOrderExecutions.size() == 5);

        for (PaymentOrderExecution paymentOrderExecution : paymentOrderExecutions) {
            LocalDate date = paymentOrderExecution.getDate();
            assertTrue(date.equals(new LocalDate("2014-06-05")) ||
                    date.equals(new LocalDate("2014-05-07")) ||
                    date.equals(new LocalDate("2014-05-13")) ||
                    date.equals(new LocalDate("2014-07-07")) ||
                    date.equals(new LocalDate("2014-07-13")));
        }
    }

    @Test
    public void translateToPaymentOrderExecutionsWithMonthlyFrequencyEdgeCase() {

        PaymentOrder paymentOrder = new PaymentOrder();
        paymentOrder.setFrequency(Frequency.MONTHLY);
        paymentOrder.setEvery(1);
        paymentOrder.setStartDate(new DateTime("2014-05-10"));
        paymentOrder.setEndDate(new DateTime("2014-07-21"));
        paymentOrder.setIntervals("9,18");

        List<PaymentOrderExecution> paymentOrderExecutions = processor.translateToPaymentOrderExecutions(paymentOrder);

        assertTrue(paymentOrderExecutions.size() == 5);

        for (PaymentOrderExecution paymentOrderExecution : paymentOrderExecutions) {
            LocalDate date = paymentOrderExecution.getDate();
            System.out.println(date);
            assertTrue(date.equals(new LocalDate("2014-06-09")) ||
                    date.equals(new LocalDate("2014-05-18")) ||
                    date.equals(new LocalDate("2014-07-09")) ||
                    date.equals(new LocalDate("2014-06-18")) ||
                    date.equals(new LocalDate("2014-07-18")));
        }
    }



    @Test
    public void translateToPaymentOrderExecutionsWithYearlyFrequency() {

        PaymentOrder paymentOrder = new PaymentOrder();
        paymentOrder.setFrequency(Frequency.YEARLY);
        // since intervals represent months the actual day of execution will be the day of start date
        paymentOrder.setStartDate(new DateTime("2014-05-07"));
        paymentOrder.setEndDate(new DateTime("2016-07-01"));
        paymentOrder.setIntervals("1,5,7"); // these are months now
        paymentOrder.setEvery(1);

        List<PaymentOrderExecution> paymentOrderExecutions = processor.translateToPaymentOrderExecutions(paymentOrder);

        assertTrue(paymentOrderExecutions.size() == 7);

        for (PaymentOrderExecution paymentOrderExecution : paymentOrderExecutions) {
            LocalDate date = paymentOrderExecution.getDate();
            assertTrue(date.equals(new LocalDate("2014-05-07")) ||
                    date.equals(new LocalDate("2014-07-07")) ||
                    date.equals(new LocalDate("2015-01-07")) ||
                    date.equals(new LocalDate("2015-05-07")) ||
                    date.equals(new LocalDate("2015-07-07")) ||
                    date.equals(new LocalDate("2016-01-07")) ||
                    date.equals(new LocalDate("2016-05-07")));
        }
    }

    @Test
    public void translateToPaymentOrderExecutionsWithYearlyFrequencyEdgeCase() {

        PaymentOrder paymentOrder = new PaymentOrder();
        paymentOrder.setFrequency(Frequency.YEARLY);
        // since intervals represent months the actual day of execution will be the day of start date
        // if day of the start date is bigger then the interval month, then the end of the month day will be selected
        paymentOrder.setStartDate(new DateTime("2014-05-31"));
        paymentOrder.setEndDate(new DateTime("2016-07-01"));
        paymentOrder.setIntervals("1,4,7");  // these are months now
        paymentOrder.setEvery(1);

        List<PaymentOrderExecution> paymentOrderExecutions = processor.translateToPaymentOrderExecutions(paymentOrder);

        assertTrue(paymentOrderExecutions.size() == 6);

        for (PaymentOrderExecution paymentOrderExecution : paymentOrderExecutions) {
            LocalDate date = paymentOrderExecution.getDate();
            assertTrue(date.equals(new LocalDate("2015-01-31")) ||
                    date.equals(new LocalDate("2015-04-30")) ||
                    date.equals(new LocalDate("2014-07-31")) ||
                    date.equals(new LocalDate("2016-01-31")) ||
                    date.equals(new LocalDate("2016-04-30")) ||
                    date.equals(new LocalDate("2015-07-31")));
        }
    }

    @Test
    public void translateToPaymentOrderExecutionsWithFirstOfTheMonthFrequency() {

        PaymentOrder paymentOrder = new PaymentOrder();
        paymentOrder.setFrequency(Frequency.START_OF_THE_MONTH);
        paymentOrder.setStartDate(new DateTime("2014-05-07"));
        paymentOrder.setEndDate(new DateTime("2014-07-21"));

        List<PaymentOrderExecution> paymentOrderExecutions = processor.translateToPaymentOrderExecutions(paymentOrder);

        assertTrue(paymentOrderExecutions.size() == 2);

        for (PaymentOrderExecution paymentOrderExecution : paymentOrderExecutions) {
            LocalDate date = paymentOrderExecution.getDate();
            assertTrue(date.equals(new LocalDate("2014-06-01")) ||
                    date.equals(new LocalDate("2014-07-01")));
        }
    }

    @Test
    public void translateToPaymentOrderExecutionsWithEndOfTheMonthFrequency() {

        PaymentOrder paymentOrder = new PaymentOrder();
        paymentOrder.setFrequency(Frequency.END_OF_THE_MONTH);
        paymentOrder.setStartDate(new DateTime("2014-05-07"));
        paymentOrder.setEndDate(new DateTime("2014-10-12"));

        List<PaymentOrderExecution> paymentOrderExecutions = processor.translateToPaymentOrderExecutions(paymentOrder);

        assertTrue(paymentOrderExecutions.size() == 5);

        for (PaymentOrderExecution paymentOrderExecution : paymentOrderExecutions) {
            LocalDate date = paymentOrderExecution.getDate();
            assertTrue(date.equals(new LocalDate("2014-05-31")) ||
                    date.equals(new LocalDate("2014-06-30")) ||
                    date.equals(new LocalDate("2014-07-31")) ||
                    date.equals(new LocalDate("2014-08-31")) ||
                    date.equals(new LocalDate("2014-09-30")));
        }
    }

    @Test
    public void translateToPaymentOrderExecutionsWithLastFridayOfTheMonthFrequency() {

        PaymentOrder paymentOrder = new PaymentOrder();
        paymentOrder.setFrequency(Frequency.LAST_FRIDAY_OF_THE_MONTH);
        paymentOrder.setStartDate(new DateTime("2014-05-07"));
        paymentOrder.setEndDate(new DateTime("2014-07-21"));

        List<PaymentOrderExecution> paymentOrderExecutions = processor.translateToPaymentOrderExecutions(paymentOrder);

        assertTrue(paymentOrderExecutions.size() == 2);

        for (PaymentOrderExecution paymentOrderExecution : paymentOrderExecutions) {
            LocalDate date = paymentOrderExecution.getDate();
            assertTrue(date.equals(new LocalDate("2014-05-30")) || date.equals(new LocalDate("2014-06-27")));
        }
    }

    @Test
    public void createPaymentOrderExecution() {
        PaymentOrder paymentOrder = new PaymentOrder();
        paymentOrder.setAmount(new BigDecimal(314));
        paymentOrder.setCurrency("EUR");
        paymentOrder.setCounterpartyName("myCounterparty");
        paymentOrder.setAccountName("myAccountName");
        paymentOrder.setId("myPaymentOrderId");
        PaymentOrderExecution paymentOrderExecution = processor.createPaymentOrderExecution(paymentOrder, new LocalDate("2014-05-07"));

        assertTrue(paymentOrderExecution.getDate().equals(new LocalDate("2014-05-07")));
        assertTrue(paymentOrderExecution.getCounterpartyName().equals("myCounterparty"));
        assertTrue(paymentOrderExecution.getAccountName().equals("myAccountName"));
        assertTrue(paymentOrderExecution.getPaymentOrderId().equals("myPaymentOrderId"));
        assertTrue(paymentOrderExecution.getAmount().equals(new BigDecimal(314)));
        assertTrue(paymentOrderExecution.getCurrency().equals("EUR"));
    }
}