package com.backbase.launchpad.services.salesandservices.servicing.p2p;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Unit test for {@link P2PEnrollmentRepository}
 */
public class P2PEnrollmentRepositoryTest {

    private P2PEnrollmentRepository p2pEnrollmentRepository;

    @Before
    public void setUp() {
        p2pEnrollmentRepository = new InMemoryP2PEnrollmentRepository();
    }

    @Test(expected = P2PEnrollmentNotFoundException.class)
    public void findNotFound() throws P2PEnrollmentNotFoundException {
        p2pEnrollmentRepository.find("3");
    }

    @Test
    public void create() throws P2PEnrollmentNotFoundException, P2PEnrollmentAlreadyExistsException {
        String customerId = "3";
        p2pEnrollmentRepository.create(customerId,  new P2PEnrollment("walter.white@gmail.com", "666"));

        P2PEnrollment p2pEnrollment = p2pEnrollmentRepository.find(customerId);
        assertEquals("walter.white@gmail.com", p2pEnrollment.getEmail());
        assertEquals("666", p2pEnrollment.getAccountNumber());
    }

    @Test(expected = P2PEnrollmentAlreadyExistsException.class)
    public void createAlreadyExists() throws P2PEnrollmentAlreadyExistsException {
        p2pEnrollmentRepository.create("3",  new P2PEnrollment("walter.white@gmail.com", "666"));
        p2pEnrollmentRepository.create("3",  new P2PEnrollment("walter.white@gmail.com", "666"));
    }

    @Test(expected = P2PEnrollmentNotFoundException.class)
    public void updateNotFound() throws P2PEnrollmentNotFoundException {
        p2pEnrollmentRepository.update("3", new P2PEnrollment("walter.white@gmail.com", "666"));
    }

    @Test
    public void updateEmail() throws P2PEnrollmentNotFoundException, P2PEnrollmentAlreadyExistsException {
        String customerId = "3";
        p2pEnrollmentRepository.create(customerId, new P2PEnrollment("walter.white@gmail.com", "666"));

        P2PEnrollment p2pEnrollment = p2pEnrollmentRepository.find(customerId);
        assertEquals("walter.white@gmail.com", p2pEnrollment.getEmail());
        assertEquals("666", p2pEnrollment.getAccountNumber());

        p2pEnrollmentRepository.update(customerId, new P2PEnrollment("cal.lightman@gmail.com", null));
        p2pEnrollment = p2pEnrollmentRepository.find(customerId);
        assertEquals("cal.lightman@gmail.com", p2pEnrollment.getEmail());
        assertEquals("666", p2pEnrollment.getAccountNumber());
    }

    @Test
    public void updateAccountNumber() throws P2PEnrollmentNotFoundException, P2PEnrollmentAlreadyExistsException {
        String customerId = "3";
        p2pEnrollmentRepository.create(customerId, new P2PEnrollment("walter.white@gmail.com", "666"));

        P2PEnrollment p2pEnrollment = p2pEnrollmentRepository.find(customerId);
        assertEquals("walter.white@gmail.com", p2pEnrollment.getEmail());
        assertEquals("666", p2pEnrollment.getAccountNumber());

        p2pEnrollmentRepository.update(customerId, new P2PEnrollment(null, "999"));
        p2pEnrollment = p2pEnrollmentRepository.find(customerId);
        assertEquals("walter.white@gmail.com", p2pEnrollment.getEmail());
        assertEquals("999", p2pEnrollment.getAccountNumber());
    }


}
