package com.backbase.launchpad.services.salesandservices.servicing.p2p;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class P2PEnrollmentProcessor implements Processor {

    private static final String VERIFICATION_CODE = "1234";

    @Autowired
    private P2PEnrollmentRepository p2pEnrollmentRepository;

    @Override
    public void process(Exchange exchange) throws Exception {
        Message in = exchange.getIn();
        Message out = exchange.getOut();
        Object header = in.getHeader(Exchange.HTTP_METHOD);
        String customerId = (String) in.getHeader("backbasepartyId");
        if ("GET".equals(header)) {
            handleGET(customerId, in, out);
        } else if ("POST".equals(header)) {
            handlePOST(customerId, in, out);
        } else if ("DELETE".equals(header)) {
            handleDELETE(customerId, in, out);
        } else if ("PUT".equals(header)) {
            handlePUT(customerId, in, out);
        } else {
            setErrorMessage(out, "Only GET, DELETE, PUT and POST are supported on /p2p-enrollment endpoint", 405);
        }

    }

    private void handlePUT(String customerId, Message in, Message out) {
        try {
            P2PEnrollment p2pEnrollment = p2pEnrollmentRepository.find(customerId);
            JSONObject parsedP2PEnrollment = getPayloadOrSendError(in);

            Object verification = parsedP2PEnrollment.get("verification");
            if (verification != null && verification instanceof JSONObject) {
                verifyEmail(customerId, out, ((JSONObject) verification).get("email"), ((JSONObject) verification).get("code"));
                return;
            }

            Object parsedEmail = parsedP2PEnrollment.get("email");
            String email = null;
            if (parsedEmail instanceof String) {
                email = (String) parsedEmail;
            } else {
                if (parsedEmail != null) {
                    setErrorMessage(out, "email must be a string value", 400);
                    return;
                }
            }
            Object parsedAccountNumber = parsedP2PEnrollment.get("accountNumber");
            String accountNumber = null;
            if (parsedAccountNumber instanceof String) {
                accountNumber = (String) parsedAccountNumber;
            } else {
                if (parsedAccountNumber != null) {
                    setErrorMessage(out, "accountNumber must be a string value", 400);
                    return;
                }
            }
            if (email != null) {
                p2pEnrollment.setEmail(email);
                p2pEnrollment.setEmailVerified(false);
            }
            if (accountNumber != null) {
                p2pEnrollment.setAccountNumber(accountNumber);
            }
            p2pEnrollmentRepository.update(customerId, p2pEnrollment);
            out.setHeader(Exchange.HTTP_RESPONSE_CODE, 204);
        } catch (P2PEnrollmentNotFoundException e) {
            setErrorMessage(out, "customer did not enroll yet for P2P", 404);
        } catch (InvalidP2PEnrollmentPayloadException e) {
            setErrorMessage(out, e.getMessage(), 400);
        }
    }

    private void verifyEmail(String customerId, Message out, Object email, Object code) throws P2PEnrollmentNotFoundException {
        if (!(email instanceof String) || !(code instanceof String)) {
            setErrorMessage(out, "'email' and 'code' fields of 'verification' should be strings", 400);
        }
        if (!VERIFICATION_CODE.equals(code)) {
            setErrorMessage(out, "wrong verification code", 409);
        } else {
            P2PEnrollment p2pEnrollment = p2pEnrollmentRepository.find(customerId);
            p2pEnrollment.setEmailVerified(true);
            p2pEnrollmentRepository.update(customerId, p2pEnrollment);
            out.setBody("{\"status\":\"Ok\"}");
        }
    }

    private void handleDELETE(String customerId, Message in, Message out) {
        try {
            p2pEnrollmentRepository.find(customerId);
            p2pEnrollmentRepository.delete(customerId);
            out.setHeader(Exchange.HTTP_RESPONSE_CODE, 204);
        } catch (P2PEnrollmentNotFoundException e) {
            setErrorMessage(out, "customer did not enroll yet for P2P", 404);
        }

    }

    private void handlePOST(String customerId, Message in, Message out) {
        try {
            JSONObject parsedP2PEnrollment = getPayloadOrSendError(in);
            Object parsedEmail = parsedP2PEnrollment.get("email");
            String email = null;
            if (parsedEmail instanceof String) {
                email = (String) parsedEmail;
            } else {
                if (parsedEmail != null) {
                    setErrorMessage(out, "email must be a string value", 400);
                    return;
                }
            }
            Object parsedAccountNumber = parsedP2PEnrollment.get("accountNumber");
            String accountNumber = null;
            if (parsedAccountNumber instanceof String) {
                accountNumber = (String) parsedAccountNumber;
            } else {
                if (parsedAccountNumber != null) {
                    setErrorMessage(out, "accountNumber must be a string value", 400);
                    return;
                }
            }
            if (email == null || accountNumber == null) {
                setErrorMessage(out, "'email' and 'accountNumber' fields are mandatory", 400);
            } else {
                p2pEnrollmentRepository.create(customerId, new P2PEnrollment(email, accountNumber));
                out.setHeader(Exchange.HTTP_RESPONSE_CODE, 201);
            }
        } catch (P2PEnrollmentAlreadyExistsException e) {
            setErrorMessage(out, "customer already enrolled for P2P", 409);
        } catch (InvalidP2PEnrollmentPayloadException e) {
            setErrorMessage(out, e.getMessage(), 400);
        }

    }

    private void handleGET(String customerId, Message in, Message out) {
        try {
            P2PEnrollment p2pEnrollment = p2pEnrollmentRepository.find(customerId);
            out.setBody(render(p2pEnrollment));
        } catch (P2PEnrollmentNotFoundException e) {
            setErrorMessage(out, "customer did not enroll yet for P2P", 404);
        }
    }

    private void setErrorMessage(Message out, String message, int code) {
        out.setBody("{\"error\": \"" + message + "\"}");
        out.setHeader(Exchange.HTTP_RESPONSE_CODE, code);
    }

    private JSONObject getPayloadOrSendError(Message in) throws InvalidP2PEnrollmentPayloadException {
        JSONObject payload;
        String bodyAsString = in.getBody(String.class);
        try {
            if (bodyAsString == null) {
                throw new ParseException(-1);
            }
            Object parsedData = new JSONParser().parse(bodyAsString);
            if (!(parsedData instanceof JSONObject)) {
                throw new ParseException(-1);
            }
            payload = (JSONObject) parsedData;
        } catch (ParseException e) {
            throw new InvalidP2PEnrollmentPayloadException("Payload is not properly formatted JSON");
        }
        return payload;
    }

    private JSONObject render(P2PEnrollment p2pEnrollment) {
        JSONObject p2pEnrollmentAsJSON = new JSONObject();
        p2pEnrollmentAsJSON.put("email", p2pEnrollment.getEmail());
        p2pEnrollmentAsJSON.put("accountNumber", p2pEnrollment.getAccountNumber());
        p2pEnrollmentAsJSON.put("accountId", p2pEnrollment.getAccountId());
        p2pEnrollmentAsJSON.put("emailVerified", p2pEnrollment.isEmailVerified());
        return p2pEnrollmentAsJSON;
    }
}
