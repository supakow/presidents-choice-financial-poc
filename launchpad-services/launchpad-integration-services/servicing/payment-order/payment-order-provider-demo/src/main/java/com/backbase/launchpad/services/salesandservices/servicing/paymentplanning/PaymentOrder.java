package com.backbase.launchpad.services.salesandservices.servicing.paymentplanning;

import com.backbase.launchpad.services.salesandservices.servicing.model.Frequency;
import org.joda.time.DateTime;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.math.BigDecimal;

/**
 * Payment order model for the payment planning functionality. It is using JAXB to extract the XML model from existdb
 * and work with objects.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class PaymentOrder {

    private String id;
    private String accountName;
    private String counterpartyName;

    @XmlJavaTypeAdapter(BigDecimalAdapter.class)
    private BigDecimal amount;

    private String currency;

    @XmlJavaTypeAdapter(DateTimeAdapter.class)
    private DateTime executionDate;  // non-recurring payment order will have just a future execution date.

    // in case of a recurring payment order we have a period
    @XmlJavaTypeAdapter(DateTimeAdapter.class)
    private DateTime startDate;  // the start date of a recurring payment order

    @XmlJavaTypeAdapter(DateTimeAdapter.class)
    private DateTime endDate;  // the end date of a recurring payment order

    // the following properties are relevant for recurring payment order and control how
    // many {@link PaymentOrderExecutions} will be created

    private Frequency frequency;
    private String intervals;
    private int every;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getCounterpartyName() {
        return counterpartyName;
    }

    public void setCounterpartyName(String counterpartyName) {
        this.counterpartyName = counterpartyName;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public DateTime getExecutionDate() {
        return executionDate;
    }

    public void setExecutionDate(DateTime executionDate) {
        this.executionDate = executionDate;
    }

    public DateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(DateTime startDate) {
        this.startDate = startDate;
    }

    public DateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(DateTime endDate) {
        this.endDate = endDate;
    }

    public Frequency getFrequency() {
        return frequency;
    }

    public void setFrequency(Frequency frequency) {
        this.frequency = frequency;
    }

    public String getIntervals() {
        return intervals;
    }

    public void setIntervals(String intervals) {
        this.intervals = intervals;
    }

    public int getEvery() {
        return every;
    }

    public void setEvery(int every) {
        this.every = every;
    }

    static class BigDecimalAdapter extends XmlAdapter<String, BigDecimal> {

        @Override
        public BigDecimal unmarshal(String value) throws Exception {
            return new BigDecimal(value);
        }

        @Override
        public String marshal(BigDecimal amount) throws Exception {
            return amount.toString();
        }
    }

    static class DateTimeAdapter extends XmlAdapter<String, DateTime> {

        @Override
        public DateTime unmarshal(String value) throws Exception {
            return new DateTime(Long.parseLong(value));
        }

        @Override
        public String marshal(DateTime v) throws Exception {
            return v.toString();
        }
    }

    static class FrequencyAdapter extends XmlAdapter<String, Frequency> {

        @Override
        public Frequency unmarshal(String value) throws Exception {
            return Frequency.valueOf(value);
        }

        @Override
        public String marshal(Frequency transactionType) throws Exception {
            return transactionType.name();
        }
    }
}
