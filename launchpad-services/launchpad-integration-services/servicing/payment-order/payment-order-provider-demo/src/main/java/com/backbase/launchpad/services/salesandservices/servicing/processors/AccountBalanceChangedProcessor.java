package com.backbase.launchpad.services.salesandservices.servicing.processors;

import static com.backbase.launchpad.services.notification.model.Event.ACCOUNT_BALANCE_CHANGED;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import com.backbase.launchpad.services.notification.model.Message;

/**
 * Camel processor creating the event notification message which will be broadcasted by the event-notification module.
 */
public class AccountBalanceChangedProcessor implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {

        String sessionId = (String) exchange.getIn().getHeader("sessionid");
        exchange.getOut().setBody(new Message(ACCOUNT_BALANCE_CHANGED, sessionId));

    }
}
