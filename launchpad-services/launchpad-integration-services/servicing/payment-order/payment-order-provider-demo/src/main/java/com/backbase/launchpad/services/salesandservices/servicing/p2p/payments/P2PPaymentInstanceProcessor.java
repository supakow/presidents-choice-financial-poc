package com.backbase.launchpad.services.salesandservices.servicing.p2p.payments;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

@Component
public class P2PPaymentInstanceProcessor implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {
        exchange.getOut().setHeader(Exchange.HTTP_RESPONSE_CODE, 204);
    }
}
