package com.backbase.launchpad.services.salesandservices.servicing;

import org.apache.camel.*;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.JavaUuidGenerator;

import static org.apache.camel.builder.PredicateBuilder.*;

/**
 * Camel routes for executing or denying a payment order.
 */
public class ExecutePaymentOrderRoutes extends RouteBuilder {

    private static final int NOTIFICATION_EXPIRE_TIME = 24 * 60 * 60 * 1000; // milliseconds
    private static final int TIMER_PERIOD_FOR_PAYMENT_EXECUTION = 10 * 1000; // milliseconds

    @EndpointInject(uri = "seda:provider/contact-dialogues/messages/new")
    private Endpoint messageEndpoint;

    @EndpointInject(uri = "timer://executePayments?fixedRate=true&period=" + TIMER_PERIOD_FOR_PAYMENT_EXECUTION)
    private Endpoint executePaymentsEndpoint;

    @EndpointInject(uri = "xmldb:exist:///db/launchpad/payments?XPath=//o[status/text() = 'Pending']&username=admin&password=")
    private Endpoint pendingPaymentsEndpoint;

    @EndpointInject(uri = "direct:updatePaymentOrderStatus")
    private Endpoint updatePaymentOrderStatusEndpoint;

    @EndpointInject(uri = "direct:sendNotificationMessage")
    private Endpoint sendNotificationMessageEndpoint;

    @EndpointInject(uri = "direct:sendErrorNotificationMessage")
    private Endpoint sendErrorNotificationMessageEndpoint;

    @EndpointInject(uri = "direct:updateAccountBalance")
    private Endpoint updateAccountBalanceEndpoint;

    @EndpointInject(uri = "direct:paymentOrderPrerequisite")
    private Endpoint paymentOrderPrerequisiteEndpoint;

    @EndpointInject(uri = "direct:runAutomations")
    private Endpoint runAutomationsEndpoint;


    // create message for the event-notification module
    @EndpointInject(uri = "direct:createAccountBalanceChangedMessage")
    private Endpoint createAccountBalanceChangedMessageEndpoint;


    @Override
    public void configure() throws Exception {
        Predicate isValidPaymentOrder =
                and(isNotNull(simple("${body}")), xpath("/o/status = 'ENABLED'").booleanResult());

        JavaUuidGenerator uuidGenerator = new JavaUuidGenerator();

        from(executePaymentsEndpoint)
            .to(pendingPaymentsEndpoint)
            .split(simple("${body}"))
                .setHeader("accountId", xpath("//o/accountId/text()").stringResult())
                .setHeader("paymentOrderId", xpath("//o/id/text()").stringResult())
                .setHeader("type", xpath("//o/type/text()").stringResult())
                .setHeader("counterpartyEmail", xpath("//o/counterpartyEmail/text()").stringResult())
                .setProperty("paymentOrder", body())
                .log(LoggingLevel.INFO, "Checking prerequisites of the payment order...")
                .recipientList().simple("xmldb:exist:///db/launchpad/accounts?XPath=//o[id = '${header.accountId}']")
                .filter(not(isValidPaymentOrder))
                    .setHeader("paymentOrderStatus", constant("Rejected"))
                    .setHeader("accountName", xpath("//o/name/text()").stringResult())
                    .to(updatePaymentOrderStatusEndpoint)
                    .setHeader("message", simple("The payment order is failed because the ${header.accountName} is blocked"))
                    .log(LoggingLevel.INFO, "Failed payment order: {\"id\" : \"${headers.paymentOrderId}\"}")
                    .to(sendErrorNotificationMessageEndpoint)
                    .stop()
                .end()
                .setBody().property("paymentOrder")
                .removeProperty("paymentOrder")
                .setHeader("transactionId", method(uuidGenerator, "generateUuid"))
                .setHeader("currentDateTime", method(System.class, "currentTimeMillis"))
                .setHeader("categoryId", simple("a1fae863-01e9-4f73-81a6-8d4e26c00f9d"))
                .to("xslt:paymentOrderToTransaction.xslt")
                .log(LoggingLevel.INFO, "Transaction: ${body}")
                .to("xmldb:exist:///db/launchpad/transactions?username=admin&password=")
                .setHeader("transactionAmount", xpath("//o/transactionAmount/text()").stringResult())
                .setHeader("counterpartyName", xpath("//o/counterpartyName/text()").stringResult())
                .setHeader("instructedAmount", xpath("//o/instructedAmount/text()").stringResult())
                .setHeader("instructedCurrency", xpath("//o/instructedCurrency/text()").stringResult())
                .log(LoggingLevel.INFO, "Executing pending payment: {\"id\" : \"${headers.paymentOrderId}\"}")
                .to(updateAccountBalanceEndpoint)
                .setHeader("paymentOrderStatus", constant("Executed"))
                .to(updatePaymentOrderStatusEndpoint)
                .to(sendNotificationMessageEndpoint)
                .to(runAutomationsEndpoint)
                .to(createAccountBalanceChangedMessageEndpoint)
            .end();

        from(updateAccountBalanceEndpoint)
                .setHeader("XMLDB_SERVICE_TYPE", constant("XQUERY"))
                .setHeader("balanceElementName", constant("availableBalance"))
                .to("velocity:updateAccountBalanceXQuery.vm")
                .to("xmldb:exist:///db/launchpad/accounts?username=admin&password=")
                .setHeader("balanceElementName", constant("balance"))
                .to("velocity:updateAccountBalanceXQuery.vm")
                .to("xmldb:exist:///db/launchpad/accounts?username=admin&password=");

        from(updatePaymentOrderStatusEndpoint)
                .setHeader("XMLDB_SERVICE_TYPE", constant("XQUERY"))
                .to("velocity:updatePaymentOrderStatusXQuery.vm")
                .to("xmldb:exist:///db/launchpad/payments?username=admin&password=");

        from(sendNotificationMessageEndpoint)
                .removeHeaders("*", "counterpartyName", "instructedAmount", "instructedCurrency", "accountId")
                .recipientList().simple("xmldb:exist:///db/launchpad/accounts?XPath=//o[id = '${header.accountId}']")
                .setHeader("backbasepartyid", xpath("//o/partyId/text()").stringResult())
                .setHeader("id", constant(uuidGenerator.generateUuid()))
                .setHeader("message",
                        simple("The payment order to ${header.counterpartyName} with the amount of ${header.instructedAmount} ${header.instructedCurrency} has been executed."))
                .setHeader("level", constant("INFO"))
                .setHeader("closable", constant("true"))
                .process(new NotificationMessageExpireTimeProcessor())
                .inOnly(messageEndpoint);

        from(sendErrorNotificationMessageEndpoint)
                .removeHeaders("*", "accountId", "message")
                .recipientList().simple("xmldb:exist:///db/launchpad/accounts?XPath=//o[id = '${header.accountId}']")
                .setHeader("backbasepartyid", xpath("//o/partyId/text()").stringResult())
                .setHeader("id", constant(uuidGenerator.generateUuid()))
                .setHeader("level", constant("SEVERE"))
                .setHeader("closable", constant("true"))
                .process(new NotificationMessageExpireTimeProcessor())
                .inOnly(messageEndpoint);

        from(runAutomationsEndpoint)
                .log("Headers: ${headers}")
                .filter().method("emailActionHelper", "shouldSendEmail(${header.instructedAmount})")
                .setHeader("to", method("emailActionHelper", "getRecipient()"))
                .setHeader("subject", constant("launchpad"))
                .setHeader("what", method("emailActionHelper", "getEmailBody(${header.instructedAmount}, ${header.instructedCurrency})"))
                .to("bean:emailActionHelper?method=incrementNumberOfExecution()")
                .to("bean:emailActionHelper?method=setExecutionDateTime()")
                .to("bean:emailAction?method=send(${header.to}, ${header.subject}, ${header.what}) ");
    }

    /**
     * Helper class to provide notification message expire time.
     */
    private static class NotificationMessageExpireTimeProcessor implements Processor {
        public void process(Exchange exchange) throws Exception {
            exchange.getIn().setHeader("expiresOn", System.currentTimeMillis() + NOTIFICATION_EXPIRE_TIME);
        }
    }


}
