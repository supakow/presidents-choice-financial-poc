package com.backbase.launchpad.services.salesandservices.servicing;

import java.math.BigDecimal;

import com.backbase.launchpad.services.salesandservices.servicing.model.*;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;

public class  InitiatePaymentOrder implements Processor {

    public void process(Exchange exchange) throws Exception {
        Message message = exchange.getIn();

        PaymentOrder paymentOrder = new PaymentOrder();
        paymentOrder.setId(message.getHeader("id", String.class));
        paymentOrder.setAccountId(message.getHeader("accountId", String.class));
        paymentOrder.setAccountName(message.getHeader("accountName", String.class));
        paymentOrder.setCounterpartyName(message.getHeader("counterpartyName", String.class));
        paymentOrder.setCounterpartyIban(message.getHeader("counterpartyIban", String.class));
        paymentOrder.setCounterpartyAccount(message.getHeader("counterpartyAccount", String.class));
        paymentOrder.setCounterpartyEmail(message.getHeader("counterpartyEmail", String.class));
        paymentOrder.setInstructedAmount(new BigDecimal(message.getHeader("instructedAmount", String.class)));
        paymentOrder.setInstructedCurrency(message.getHeader("instructedCurrency", String.class));
        paymentOrder.setRemittanceInformation(message.getHeader("remittanceInformation", String.class));
        paymentOrder.setCreditorReference(message.getHeader("creditorReference", String.class));
        paymentOrder.setPaymentCategory(message.getHeader("paymentCategory", String.class));
        paymentOrder.setStatus("Received");
        paymentOrder.setExecutionDateTime(message.getHeader("onDate", Long.class));
        paymentOrder.setType(PaymentOrderType.valueOf(message.getHeader("type", String.class)));

        buildScheduleIfRequired(message, paymentOrder);

        paymentOrder.validateState();

        exchange.getIn().setBody(paymentOrder);
    }

    private void buildScheduleIfRequired(Message message, PaymentOrder paymentOrder) {
        String paymentMode = message.getHeader("paymentMode", String.class);

        if (isRecurringPayment(paymentMode)) {
            Schedule schedule = new Schedule();
            schedule.setStartDate(message.getHeader("scheduledtransfer[startdate]", Long.class));
            schedule.setEndDate(message.getHeader("scheduledtransfer[enddate]", Long.class));
            schedule.setFrequency(Frequency.valueOf(message.getHeader("scheduledTransfer[frequency]", String.class)));
            schedule.setIntervals(message.getHeader("scheduledtransfer[intervals]", String.class));
            schedule.setEvery(message.getHeader("scheduledtransfer[every]", Integer.class));
            paymentOrder.setSchedule(schedule);
        }
    }

    private boolean isRecurringPayment(String paymentMode) {
        return PaymentMode.RECURRING.equals(PaymentMode.valueOf(paymentMode));
    }

}
