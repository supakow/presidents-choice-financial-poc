package com.backbase.launchpad.services.salesandservices.servicing;

import static org.apache.camel.builder.PredicateBuilder.isEqualTo;
import static org.apache.camel.builder.PredicateBuilder.isNotNull;

import net.sf.json.JSON;

import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.Predicate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.spi.IdempotentRepository;
import org.restlet.data.MediaType;
import org.restlet.data.Method;
import org.springframework.beans.factory.annotation.Autowired;

import com.backbase.mashup.camel.json.XmlJsonDataFormatWrapper;

public class PaymentOrderRoute extends RouteBuilder {

    protected static final String XMLDB_DEFAULT_COLLECTION_EP = "xmldb:exist:///db/launchpad";
    protected static final String XMLDB_PAYMENT_COLLECTION_URI = XMLDB_DEFAULT_COLLECTION_EP + "/payments";
    protected static final String PAYMENT_ORDERS_COLLECTION_ENDPOINT = "seda:provider/servicing/payment-orders";
    protected static final String PAYMENT_ORDERS_INSTANCE_ENDPOINT = "seda:provider/servicing/payment-orders/paymentOrderId";
    protected static final String XMLDB_STORE_PAYMENT_ORDER_EP = XMLDB_PAYMENT_COLLECTION_URI + "?username=admin&password=";
    protected static final String XMLDB_RETRIEVE_PAYMENT_ORDER_EP = XMLDB_PAYMENT_COLLECTION_URI + "?XPath=//o[id = '${header.id}']";
    protected static final String RETRIEVE_PAYMENT_ORDER_EP = "direct:retrievePaymentOrder";
    private static final String PAYMENT_ORDER_STATUS_REPORT_EP = "direct:initiatePaymentOrderStatusReport";
    private static final String INITIATE_PAYMENT_ORDER_EP = "direct:initiatePaymentOrder";
    private static final String SCHEDULE_PAYMENT_ORDER_FOR_EXECUTION_ENDPOINT = "direct:schedulePaymentOrderForExecution";
    private static final String SCHEDULE_PAYMENT_ORDER_QUERY_ENDPOINT = "velocity:schedulePaymentOrderXQuery.vm";
    private static final String RETRIEVE_PAYMENT_ORDERS_XQUERY_EP = "velocity:retrievePaymentOrdersXQuery.vm";

    private static final double PAYMENT_ORDER_AMOUNT_LIMIT_WITHOUT_AUTHORIZATION = 1000.0;
    private static final String PAYMENT_ORDER_LIMIT_CHECK_ENDPOINT = "direct:checkLimitEndpoint";

    private static final String XMLDB_XQUERY_SERVICE = "XMLDB_SERVICE_TYPE";


    @Autowired
    private InitiatePaymentOrder initiatePaymentOrder;
    @Autowired
    private XmlJsonDataFormatWrapper xmlJsonDataFormat;
    @Autowired
    private IdempotentRepository<String> paymentOrderInitiationIdempotentRepository;

    @Override
    public void configure() throws Exception {
        paymentOrderContentBasedRouter();
        retrievePaymentOrdersRoute();
        initiatePaymentOrderRoute();
        retrievePaymentOrderRoute();
        schedulePaymentOrderForExecution();
        paymentOrderStatusReportRoute();
        checkPaymentOrderToBeScheduledForExecution();
    }

    private void paymentOrderContentBasedRouter() {
        Predicate isInitiate = header(Exchange.HTTP_METHOD.toLowerCase()).isEqualTo(Method.POST);
        Predicate isUpdate = header(Exchange.HTTP_METHOD.toLowerCase()).isEqualTo(Method.PUT);

        from(PAYMENT_ORDERS_INSTANCE_ENDPOINT)
            .setHeader(Exchange.CONTENT_TYPE).constant(MediaType.APPLICATION_JSON)
            .choice()
                .when(isInitiate)
                    .log(LoggingLevel.INFO, "Initiating Payment Order: {\"id\": \"${header.id}\"}")
                    .to(INITIATE_PAYMENT_ORDER_EP)
                .when(isUpdate)
                    .log(LoggingLevel.INFO, "Checking prerequisites for the payment order to be scheduled for execution: {\"id\": \"${header.id}\"}")
                    .to(PAYMENT_ORDER_LIMIT_CHECK_ENDPOINT)
                .otherwise()
                    .log(LoggingLevel.INFO, "Retrieving Payment Order: {\"id\": \"${header.id}\"}")
                    .to(RETRIEVE_PAYMENT_ORDER_EP)
            .end();
    }

    private void initiatePaymentOrderRoute() {
        from(INITIATE_PAYMENT_ORDER_EP)
            .idempotentConsumer(header("id"), paymentOrderInitiationIdempotentRepository)
                .removeOnFailure(true).skipDuplicate(false)
                .filter(property(Exchange.DUPLICATE_MESSAGE).isEqualTo(true))
                .to(PAYMENT_ORDER_STATUS_REPORT_EP)
                .stop()
            .end()
            .process(initiatePaymentOrder)
            .convertBodyTo(JSON.class)
            .log("Received payment order: ${body}")
            .unmarshal(xmlJsonDataFormat)
            .convertBodyTo(String.class)
            .log("Store payment order as XML : ${body}")
            .to(XMLDB_STORE_PAYMENT_ORDER_EP)
            .to(PAYMENT_ORDER_STATUS_REPORT_EP);
    }

    private void paymentOrderStatusReportRoute() {
        from(PAYMENT_ORDER_STATUS_REPORT_EP)
            .recipientList().simple(XMLDB_RETRIEVE_PAYMENT_ORDER_EP)
            .convertBodyTo(String.class)
            .transform().xpath("normalize-space(/o/status/text())", String.class)
            .setBody(simple("{\"status\" : \"${body}\"}"));
    }

    private void retrievePaymentOrdersRoute() {
        from(PAYMENT_ORDERS_COLLECTION_ENDPOINT)
            .setHeader(XMLDB_XQUERY_SERVICE).constant("XQUERY")
            .to(RETRIEVE_PAYMENT_ORDERS_XQUERY_EP)
            .recipientList().simple(XMLDB_DEFAULT_COLLECTION_EP)
            .marshal(xmlJsonDataFormat)
            .setHeader(Exchange.CONTENT_TYPE).constant(MediaType.APPLICATION_JSON);
    }

    private void retrievePaymentOrderRoute() {
        from(RETRIEVE_PAYMENT_ORDER_EP)
            .recipientList().simple(XMLDB_RETRIEVE_PAYMENT_ORDER_EP)
            .convertBodyTo(String.class)
            .marshal(xmlJsonDataFormat);
    }


    private void checkPaymentOrderToBeScheduledForExecution() {
        Predicate isOverLimit = header("amount").isGreaterThan(1000);
        Predicate isPasswordProvided = isNotNull(simple("${header.auth_password}"));
        Predicate isPasswordCorrect = isEqualTo(simple("${header.auth_password}"), constant("password")); // TODO use Spring Security

        from(PAYMENT_ORDER_LIMIT_CHECK_ENDPOINT)
            .log(LoggingLevel.INFO, "Payment order id: + ${header.id}")
            .recipientList().simple(XMLDB_RETRIEVE_PAYMENT_ORDER_EP)
            .setHeader("amount", xpath("/o/instructedAmount/text()").stringResult())
            .log(LoggingLevel.INFO, "Payment oder amount: + ${header.amount}")
            .choice()
                .when(isOverLimit)
                    .log(LoggingLevel.INFO, "Payment order over " + PAYMENT_ORDER_AMOUNT_LIMIT_WITHOUT_AUTHORIZATION + " limit , authorization needed")
                    .choice()
                        .when(isPasswordProvided)
                            .choice()
                                .when(isPasswordCorrect)
                                    .log(LoggingLevel.INFO, "Payment order authorization successful")
                                    .to(SCHEDULE_PAYMENT_ORDER_FOR_EXECUTION_ENDPOINT)
                                .otherwise()
                                    .log(LoggingLevel.INFO, "Payment oder authorization unsuccessful")
                                    .setBody(simple("{\"status\" : \"UNSUCCESSFUL_AUTHORIZATION\"}"))
                            .endChoice()
                        .otherwise()
                            .log(LoggingLevel.INFO, "Payment order needs to be authorized, but no password was provided")
                            .setBody(simple("{\"status\" : \"AUTHORIZATION_NEEDED\"}"))
                    .endChoice()
                .otherwise()
                    .log(LoggingLevel.INFO, "Payment order does not need authorization, under " + PAYMENT_ORDER_AMOUNT_LIMIT_WITHOUT_AUTHORIZATION + " limit")
                    .to(SCHEDULE_PAYMENT_ORDER_FOR_EXECUTION_ENDPOINT)
            .end();
    }

    /**
     * Sets the payment status from "Received" to "Pending"
     */
    private void schedulePaymentOrderForExecution() {
        from(SCHEDULE_PAYMENT_ORDER_FOR_EXECUTION_ENDPOINT)
            .to(SCHEDULE_PAYMENT_ORDER_QUERY_ENDPOINT)
            .setHeader(XMLDB_XQUERY_SERVICE).constant("XQUERY")
            .to(XMLDB_STORE_PAYMENT_ORDER_EP)
            .to(PAYMENT_ORDER_STATUS_REPORT_EP);
    }
}
