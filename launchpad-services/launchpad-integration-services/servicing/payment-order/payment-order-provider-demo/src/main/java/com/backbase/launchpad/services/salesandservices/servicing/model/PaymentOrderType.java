package com.backbase.launchpad.services.salesandservices.servicing.model;

public enum PaymentOrderType {

    BANK, P2P_EMAIL;

}
