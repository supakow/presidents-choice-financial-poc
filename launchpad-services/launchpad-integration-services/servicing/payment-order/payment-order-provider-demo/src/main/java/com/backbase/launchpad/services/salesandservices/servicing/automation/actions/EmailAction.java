package com.backbase.launchpad.services.salesandservices.servicing.automation.actions;

/**
 * Contract for sending an email
 */
public interface EmailAction {

    void send(String to, String subject, String what);
}
