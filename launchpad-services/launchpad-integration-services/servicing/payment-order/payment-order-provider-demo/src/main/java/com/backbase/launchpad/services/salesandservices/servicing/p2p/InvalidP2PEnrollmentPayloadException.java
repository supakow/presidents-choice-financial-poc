package com.backbase.launchpad.services.salesandservices.servicing.p2p;

/**
 * Exception signaling when receiving an invalid {@link P2PEnrollment} as a payload for the endpoint
 * POST or PUT  /p2p-enrollment
 */
public class InvalidP2PEnrollmentPayloadException extends Exception {

    public InvalidP2PEnrollmentPayloadException(String message) {
        super(message);
    }
}
