package com.backbase.launchpad.services.salesandservices.servicing.model;

public enum PaymentMode {

    RECURRING, NON_RECURRING;

}
