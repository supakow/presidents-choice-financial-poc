package com.backbase.launchpad.services.salesandservices.servicing.p2p;

public class P2PEnrollmentAlreadyExistsException extends Exception {

    public P2PEnrollmentAlreadyExistsException(String message) {
        super(message);
    }
}
