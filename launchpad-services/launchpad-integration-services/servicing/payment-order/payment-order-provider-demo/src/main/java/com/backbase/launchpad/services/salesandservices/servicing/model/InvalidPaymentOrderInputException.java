package com.backbase.launchpad.services.salesandservices.servicing.model;

public class InvalidPaymentOrderInputException extends Exception {

    public InvalidPaymentOrderInputException(String message) {
        super(message);
    }

}
