package com.backbase.launchpad.services.salesandservices.servicing.automation;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;

import static com.backbase.launchpad.services.salesandservices.servicing.automation.Automation.Action.ActionType;
import static com.backbase.launchpad.services.salesandservices.servicing.automation.Automation.Trigger.Direction.valueOf;
import static com.backbase.launchpad.services.salesandservices.servicing.automation.Automation.Trigger.TriggerType;
import static com.backbase.launchpad.services.salesandservices.servicing.automation.Automation.Trigger.TriggerType.TRANSACTION_AMOUNT;

@Component
public class AutomationCollectionProcessor extends AutomationProcessor implements Processor {

    @Autowired
    private AutomationRepository automationRepository;

    @Override
    public void process(Exchange exchange) throws Exception {

        Message in = exchange.getIn();
        Message out = exchange.getOut();
        Object header = in.getHeader(Exchange.HTTP_METHOD);
        String customerId = (String) in.getHeader("backbasepartyId");
        if ("GET".equals(header)) {
            handleGet(customerId, out);
        } else if ("POST".equals(header)) {
            handlePOST(customerId, in, out);
        } else {
            setErrorMessage(out, "Only GET and POST is supported on /automations endpoint.", 405);
        }
    }

    private void handlePOST(String customerId, Message in, Message out) {
        try {
            JSONObject parsedAutomation = getPayloadOrSendError(in);
            Automation automation = new Automation();
            String name = (String) parsedAutomation.get("name");

            if (name == null) {
                setErrorMessage(out, "'name' field is mandatory", 400);
                return;
            } else {
                automation.setName(name);
            }

            Boolean enabled = (Boolean) parsedAutomation.get("enabled");
            automation.setEnabled(enabled != null ? enabled.booleanValue() : false);
            automation.setActionIcon((String) parsedAutomation.get("actionIcon"));
            automation.setTriggerIcon((String) parsedAutomation.get("triggerIcon"));

            JSONObject parsedTrigger = (JSONObject) parsedAutomation.get("trigger");
            if (parsedTrigger != null) {
                TriggerType triggerType = TRANSACTION_AMOUNT;
                Number amount = (Number) parsedTrigger.get("amount");
                String direction = (String) parsedTrigger.get("direction");
                String accountId = (String) parsedTrigger.get("accountId");

                automation.getTrigger().setType(triggerType);
                automation.getTrigger().setAmount(new BigDecimal(amount.toString()));
                automation.getTrigger().setDirection(valueOf(direction));
                automation.getTrigger().setAccountId(accountId);
            }

            JSONObject parsedAction = (JSONObject) parsedAutomation.get("action");
            if (parsedAction != null) {
                ActionType actionType = ActionType.EMAIL;
                String actionValue = (String) parsedAction.get("value");

                automation.getAction().setType(actionType);
                automation.getAction().setValue(actionValue);
            }

            Automation savedAutomation = automationRepository.save(customerId, automation);
            out.setBody(renderDetails(savedAutomation));
            out.getExchange().getOut().setHeader(Exchange.HTTP_RESPONSE_CODE, 201);
        } catch (InvalidAutomationPayloadException e) {
            setErrorMessage(out, e.getMessage(), 400);
        }
    }

    private void handleGet(String customerId, Message out) {
        JSONArray response = new JSONArray();
        List<Automation> automations = automationRepository.findAll(customerId);
        if (automations.isEmpty()) {
            out.setHeader(Exchange.HTTP_RESPONSE_CODE, 204);
        } else {
            for (Automation automation : automationRepository.findAll(customerId)) {
                response.add(render(automation));
            }
            out.setBody(response);
        }
    }


}
