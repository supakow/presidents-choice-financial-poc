package com.backbase.launchpad.services.salesandservices.servicing.paymentplanning;

import org.joda.time.LocalDate;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Models a payment order execution.
 * For a non-recurring payment order, there is only one payment order execution.
 * For a recurring payment order, there could be many associated payment order executions.
 *
 */
public class PaymentOrderExecution {

    private String id;
    private String paymentOrderId;
    private String accountName;
    private LocalDate date;
    private BigDecimal amount;
    private String currency;
    private String counterpartyName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPaymentOrderId() {
        return paymentOrderId;
    }

    public void setPaymentOrderId(String paymentOrderId) {
        this.paymentOrderId = paymentOrderId;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCounterpartyName() {
        return counterpartyName;
    }

    public void setCounterpartyName(String counterpartyName) {
        this.counterpartyName = counterpartyName;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("[");
        sb.append("\"paymentOrderId\":\"").append(paymentOrderId).append("\"").append(",");
        sb.append("\"accountName\":\"").append(accountName).append("\"").append(",");
        sb.append("\"date\":\"").append(date).append("\"").append(",");
        sb.append("\"amount\":\"").append(amount).append("\"").append(",");
        sb.append("\"counterpartyName\":\"").append(counterpartyName).append("\"").append(",");
        sb.append("]");
        return sb.toString();
    }


}
