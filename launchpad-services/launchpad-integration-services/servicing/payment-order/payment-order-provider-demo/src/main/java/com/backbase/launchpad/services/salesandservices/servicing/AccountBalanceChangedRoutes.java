package com.backbase.launchpad.services.salesandservices.servicing;

import org.apache.camel.Endpoint;
import org.apache.camel.EndpointInject;
import org.apache.camel.builder.RouteBuilder;

import com.backbase.launchpad.services.salesandservices.servicing.processors.AccountBalanceChangedProcessor;

public class AccountBalanceChangedRoutes  extends RouteBuilder {

    protected static final String XMLDB_SERVICE_TYPE = "XMLDB_SERVICE_TYPE";

    @EndpointInject(uri = "direct:createAccountBalanceChangedMessage")
    private Endpoint createAccountBalanceChangedMessageEndpoint;

    @EndpointInject(uri = "seda:provider/event-notification")
    private Endpoint sendAccountBalanceChangedMessageEndpoint;

    // internal endpoints
    @EndpointInject(uri = "velocity:retrieveSessionId.vm")
    private Endpoint retrieveSessionId;

    @EndpointInject(uri = "xmldb:exist:///db/launchpad/sessions?username=admin&password=")
    protected Endpoint sessionEntityEndpoint;

    private static final String CUSTOMERS_ENDPOINT = "xmldb:exist:///db/launchpad/customers?XPath=/customer[id/text() = '${header.backbasepartyid}']";


    @Override
    public void configure() throws Exception {

        from(createAccountBalanceChangedMessageEndpoint)
                .recipientList().simple(CUSTOMERS_ENDPOINT)
                .setHeader("username", xpath("/customer/username/text()").stringResult())
                .setHeader(XMLDB_SERVICE_TYPE, constant("XQUERY"))
                .to(retrieveSessionId)
                .to(sessionEntityEndpoint)
                .transform().xpath("/httpSessionId/text()", String.class)
                .setHeader("sessionId", simple("${body}"))
                .process(new AccountBalanceChangedProcessor())
                .inOnly(sendAccountBalanceChangedMessageEndpoint);
    }
}
