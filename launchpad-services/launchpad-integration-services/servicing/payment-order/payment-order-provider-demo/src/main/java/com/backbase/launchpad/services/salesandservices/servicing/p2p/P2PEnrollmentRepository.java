package com.backbase.launchpad.services.salesandservices.servicing.p2p;

/**
 * Repository interface for {@link P2PEnrollment}
 */
public interface P2PEnrollmentRepository {

    P2PEnrollment find(String customerId) throws P2PEnrollmentNotFoundException;

    void create(String customerId, P2PEnrollment p2PEnrollment) throws P2PEnrollmentAlreadyExistsException;

    void update(String customerId, P2PEnrollment p2PEnrollment) throws P2PEnrollmentNotFoundException;

    void delete(String customerId) throws P2PEnrollmentNotFoundException;
}
