package com.backbase.launchpad.services.salesandservices.servicing.automation.actions;

import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

/**
 * Implementation of {@link EmailAction} leveraging {@link JavaMailSender}
 */
public class EmailActionImpl implements EmailAction {

    private JavaMailSender javaMailSender;

    public EmailActionImpl(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    @Override
    public void send(String to, String subject, String what) {
        SimpleMailMessage message = new SimpleMailMessage();

        message.setTo(to);
        message.setSubject(subject);
        message.setText(what);

        javaMailSender.send(message);
    }
}
