package com.backbase.launchpad.services.salesandservices.servicing.automation;

import org.joda.time.DateTime;

import java.math.BigDecimal;

public class Automation {

    private String id;
    private String name;
    private boolean enabled;
    private String triggerIcon;
    private String actionIcon;
    private DateTime lastExecution;
    private int nrOfExecutions;
    private Trigger trigger = new Trigger();
    private Action action = new Action();

    public static class Action {

        private ActionType type;
        private String value;

        public ActionType getType() {
            return type;
        }

        public void setType(ActionType type) {
            this.type = type;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public static enum ActionType {
            EMAIL
        }
    }

    public static class Trigger {

        private TriggerType type;
        private String accountId;
        private BigDecimal amount;
        private Direction direction;

        public TriggerType getType() {
            return type;
        }

        public void setType(TriggerType type) {
            this.type = type;
        }

        public String getAccountId() {
            return accountId;
        }

        public void setAccountId(String accountId) {
            this.accountId = accountId;
        }

        public BigDecimal getAmount() {
            return amount;
        }

        public void setAmount(BigDecimal amount) {
            this.amount = amount;
        }

        public Direction getDirection() {
            return direction;
        }

        public void setDirection(Direction direction) {
            this.direction = direction;
        }

        public static enum TriggerType {
            TRANSACTION_AMOUNT
        }

        public static enum Direction {
            LESS("less"), EQUAL("equal"), MORE("more"), LESS_THAN_EQUAL("less than or equal"), MORE_THAN_EQUAL("more than or equal");

            private String name;

            Direction(String name) {
                this.name = name;
            }

            public String getName() {
                return name;
            }
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Trigger getTrigger() {
        return trigger;
    }

    public void setTrigger(Trigger trigger) {
        this.trigger = trigger;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public String getTriggerIcon() {
        return triggerIcon;
    }

    public void setTriggerIcon(String triggerIcon) {
        this.triggerIcon = triggerIcon;
    }

    public String getActionIcon() {
        return actionIcon;
    }

    public void setActionIcon(String actionIcon) {
        this.actionIcon = actionIcon;
    }

    public DateTime getLastExecution() {
        return lastExecution;
    }

    public void setLastExecution(DateTime lastExecution) {
        this.lastExecution = lastExecution;
    }

    public int getNrOfExecutions() {
        return nrOfExecutions;
    }

    public void setNrOfExecutions(int nrOfExecutions) {
        this.nrOfExecutions = nrOfExecutions;
    }
}
