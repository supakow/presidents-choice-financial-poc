package com.backbase.launchpad.services.salesandservices.servicing.paymentplanning;

import org.apache.camel.Endpoint;
import org.apache.camel.EndpointInject;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.converter.jaxb.JaxbDataFormat;
import org.apache.camel.spi.DataFormat;

public class PaymentOrderExecutionRoute extends RouteBuilder {

    private static final String XMLDB_SERVICE_TYPE = "XMLDB_SERVICE_TYPE";

    // Main endpoint
    @EndpointInject(uri = "seda:provider/servicing/payment-order-executions")
    private Endpoint paymentOrderExecutionEndpoint;

    // Internal endpoints
    @EndpointInject(uri = "velocity:createPaymentOrderQueryEndpoint.vm")
    private Endpoint createPaymentOrderQueryEndpoint;

    private static final String PAYMENT_ORDER_STORAGE = "xmldb:exist:///db/launchpad/payments?username=admin&password=";


    @Override
    public void configure() throws Exception {
        DataFormat jaxb = new JaxbDataFormat(PaymentOrderList.class.getPackage().getName());

        from(paymentOrderExecutionEndpoint)
            .setHeader(XMLDB_SERVICE_TYPE, constant("XQUERY"))
            .to(createPaymentOrderQueryEndpoint)
                .to(PAYMENT_ORDER_STORAGE)
                .convertBodyTo(String.class)
                .unmarshal(jaxb)
                .process(new PaymentOrderExecutionCollectionProcessor());
    }
}
