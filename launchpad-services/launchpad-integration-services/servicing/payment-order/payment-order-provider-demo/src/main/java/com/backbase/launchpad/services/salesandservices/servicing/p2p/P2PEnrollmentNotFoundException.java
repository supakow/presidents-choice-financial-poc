package com.backbase.launchpad.services.salesandservices.servicing.p2p;

public class P2PEnrollmentNotFoundException extends Exception {

    public P2PEnrollmentNotFoundException(String message) {
        super(message);
    }
}

