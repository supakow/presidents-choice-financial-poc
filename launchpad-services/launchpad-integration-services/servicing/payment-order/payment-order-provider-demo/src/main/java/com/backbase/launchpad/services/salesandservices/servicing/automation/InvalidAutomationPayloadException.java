package com.backbase.launchpad.services.salesandservices.servicing.automation;

public class InvalidAutomationPayloadException extends Throwable {

    public InvalidAutomationPayloadException(String s) {
        super(s);
    }
}
