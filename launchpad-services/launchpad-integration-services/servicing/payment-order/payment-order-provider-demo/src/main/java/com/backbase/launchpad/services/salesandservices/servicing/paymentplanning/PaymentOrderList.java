package com.backbase.launchpad.services.salesandservices.servicing.paymentplanning;

import com.backbase.launchpad.services.salesandservices.servicing.paymentplanning.PaymentOrder;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "paymentOrders")
@XmlAccessorType(XmlAccessType.FIELD)
public class PaymentOrderList {

    @XmlElement(name = "paymentOrder")
    private List<PaymentOrder> paymentOrders = new ArrayList<PaymentOrder>();

    public List<PaymentOrder> getPaymentOrders() {
        return paymentOrders;
    }

    public void setPaymentOrders(List<PaymentOrder> paymentOrders) {
        this.paymentOrders = paymentOrders;
    }

    public void addPaymentOrder(PaymentOrder paymentOrder) {
        paymentOrders.add(paymentOrder);
    }
}
