package com.backbase.launchpad.services.salesandservices.servicing.automation;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.math.BigDecimal;
import java.text.DecimalFormat;

public class AutomationProcessor {

    protected JSONObject render(Automation automation) {
        JSONObject automationAsJson = new JSONObject();

        automationAsJson.put("id", automation.getId());
        automationAsJson.put("name", automation.getName());
        automationAsJson.put("enabled", automation.isEnabled());
        automationAsJson.put("triggerIcon", automation.getTriggerIcon());
        automationAsJson.put("actionIcon", automation.getActionIcon());
        if (automation.getLastExecution() != null) {
            automationAsJson.put("lastExecution", automation.getLastExecution().toString());
        }
        automationAsJson.put("nrOfExecutions", automation.getNrOfExecutions());

        return automationAsJson;
    }

    protected JSONObject renderDetails(Automation automation) {
        JSONObject automationAsJson = new JSONObject();

        automationAsJson.put("id", automation.getId());
        automationAsJson.put("name", automation.getName());
        automationAsJson.put("enabled", automation.isEnabled());
        automationAsJson.put("triggerIcon", automation.getTriggerIcon());
        automationAsJson.put("actionIcon", automation.getActionIcon());
        if (automation.getLastExecution() != null) {
            automationAsJson.put("lastExecution", automation.getLastExecution().toString());
        }
        automationAsJson.put("nrOfExecutions", automation.getNrOfExecutions());

        JSONObject trigger = new JSONObject();
        trigger.put("type", automation.getTrigger().getType() != null ? automation.getTrigger().getType().toString() : null);
        trigger.put("accountId", automation.getTrigger().getAccountId());
        trigger.put("amount", automation.getTrigger().getAmount() != null ? automation.getTrigger().getAmount() : null);
        trigger.put("direction", automation.getTrigger().getDirection() != null ? automation.getTrigger().getDirection().toString() : null);
        automationAsJson.put("trigger", trigger);

        JSONObject action = new JSONObject();
        action.put("type", automation.getAction().getType() != null ? automation.getAction().getType().toString() : null);
        action.put("value", automation.getAction().getValue());
        automationAsJson.put("action", action);

        return automationAsJson;
    }

    protected JSONObject getPayloadOrSendError(Message in) throws InvalidAutomationPayloadException {
        JSONObject payload;
        String bodyAsString = in.getBody(String.class);
        try {
            if (bodyAsString == null) {
                throw new ParseException(-1);
            }
            Object parsedData = new JSONParser().parse(bodyAsString);
            if (!(parsedData instanceof JSONObject)) {
                throw new ParseException(-1);
            }
            payload = (JSONObject) parsedData;
        } catch (ParseException e) {
            throw new InvalidAutomationPayloadException("Payload is not properly formatted JSON");
        }
        return payload;
    }

    protected void setErrorMessage(Message out, String message, int code) {
        out.setBody("{\"error\": \"" + message + "\"}");
        out.setHeader(Exchange.HTTP_RESPONSE_CODE, code);
    }
}
