package com.backbase.launchpad.services.salesandservices.servicing.model;

public enum Frequency {
    WEEKLY,
    MONTHLY,
    YEARLY,
    START_OF_THE_MONTH,
    END_OF_THE_MONTH,
    LAST_FRIDAY_OF_THE_MONTH;
}
