package com.backbase.launchpad.services.salesandservices.servicing.automation;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AutomationRoute extends RouteBuilder {

    private String AUTOMATION_COLLECTION_ENDPOINT = "seda:provider/automations";
    private String AUTOMATION_INSTANCE_ENDPOINT = "seda:provider/automations/automationId";

    @Autowired
    private AutomationCollectionProcessor collectionProcessor;

    @Autowired
    private AutomationInstanceProcessor instanceProcessor;

    @Override
    public void configure() throws Exception {
        from(AUTOMATION_COLLECTION_ENDPOINT).process(collectionProcessor);

        from(AUTOMATION_INSTANCE_ENDPOINT).process(instanceProcessor);
    }
}
