package com.backbase.launchpad.services.salesandservices.servicing.automation.actions;

import com.backbase.launchpad.services.salesandservices.servicing.automation.Automation;
import com.backbase.launchpad.services.salesandservices.servicing.automation.AutomationRepository;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.backbase.launchpad.services.salesandservices.servicing.automation.Automation.Trigger.Direction;

/**
 * Encapsulates logic when to send an email. Will be called from camel routes.
 */
@Service
public class EmailActionHelper {

    private final String CUSTOMER_ID = "3";

    private DateTime lastExecution;

    @Autowired
    private AutomationRepository automationRepository;

    public boolean shouldSendEmail(String amount) {
        boolean result = false;
        List<Automation> automations = automationRepository.findAll(CUSTOMER_ID);
        if (!automations.isEmpty()) {
            // currently returning just the first automation
            Automation automation = automations.get(0);
            BigDecimal triggerAmount = automation.getTrigger().getAmount();
            BigDecimal transactionAmount = new BigDecimal(amount);
            Direction direction = automation.getTrigger().getDirection();
            boolean isEmailProvided = false;
            if (automation.getAction() != null && automation.getAction().getValue() != null) {
                isEmailProvided = true;
            }
            if (automation.isEnabled() && passAmountCheck(transactionAmount, triggerAmount, direction) && isEmailProvided) {
                result = true;
            }
        }
        return result;
    }

    public void incrementNumberOfExecution() {
        Automation automation = automationRepository.findAll(CUSTOMER_ID).get(0);
        automation.setNrOfExecutions(automation.getNrOfExecutions() + 1);

        automationRepository.save(CUSTOMER_ID, automation);
    }

    public void setExecutionDateTime() {
        Automation automation = automationRepository.findAll(CUSTOMER_ID).get(0);
        lastExecution = new DateTime();
        automation.setLastExecution(lastExecution);

        automationRepository.save(CUSTOMER_ID, automation);
    }

    public String getRecipient() {
        Automation automation = automationRepository.findAll(CUSTOMER_ID).get(0);
        return automation.getAction().getValue();
    }

    public String getEmailBody(String amount, String currency) {
        Automation automation = automationRepository.findAll(CUSTOMER_ID).get(0);
        Direction direction = automation.getTrigger().getDirection();
        String triggerAmount = automation.getTrigger().getAmount().toString();
        StringBuilder sb = new StringBuilder();
        sb.append("Hello, \n\nThis message is to alert you of a debit transfer of ");
        sb.append(amount).append(" ").append(currency);
        sb.append(" at ").append(new DateTime().toString());
        sb.append(" which is ").append(direction.getName());
        sb.append(" to ").append(triggerAmount).append(" ").append(currency);
        sb.append("\n\n");
        sb.append("You cannot reply to this message, please login to your online or mobile banking application.");
        sb.append("\n\n");
        sb.append("Yours automatically,");
        sb.append("\n\n");
        sb.append("Backbase Bank");
        return sb.toString();
    }

    private boolean passAmountCheck(BigDecimal transactionAmount, BigDecimal triggerAmount, Direction direction) {
        boolean result = false;

        switch (direction) {
            case LESS : result = transactionAmount.compareTo(triggerAmount) < 0; break;
            case LESS_THAN_EQUAL: result = transactionAmount.compareTo(triggerAmount) <=0; break;
            case MORE : result = transactionAmount.compareTo(triggerAmount) > 0; break;
            case MORE_THAN_EQUAL: result = transactionAmount.compareTo(triggerAmount) >= 0; break;
            case EQUAL: result = transactionAmount.compareTo(triggerAmount) == 0; break;
        }

        return result;
    }



}
