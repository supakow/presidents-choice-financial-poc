package com.backbase.launchpad.services.salesandservices.servicing.p2p.payments;

import org.apache.camel.Endpoint;
import org.apache.camel.EndpointInject;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.converter.jaxb.JaxbDataFormat;
import org.apache.camel.spi.DataFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class P2PPaymentRoute extends RouteBuilder {

    protected static final String XMLDB_SERVICE_TYPE = "XMLDB_SERVICE_TYPE";

    // Main endpoint
    @EndpointInject(uri = "seda:provider/servicing/p2p-payments")
    private Endpoint p2pPaymentOrderCollectionEndpoint;

    @EndpointInject(uri = "seda:provider/servicing/p2p-payments/p2pPaymentId")
    private Endpoint p2pPaymentOrderInstanceEndpoint;

    // Internal dependencies
    @Autowired
    private P2PPaymentCollectionProcessor p2pPaymentCollectionProcessor;

    @Autowired
    private P2PPaymentInstanceProcessor p2PPaymentInstanceProcessor;

    @EndpointInject(uri = "velocity:createP2PPaymentOrderQuery.vm")
    private Endpoint createP2PPaymentOrderQuery;

    private static final String PAYMENT_ORDER_STORAGE = "xmldb:exist:///db/launchpad/payments?username=admin&password=";

    @Override
    public void configure() throws Exception {
        DataFormat jaxb = new JaxbDataFormat(P2PPaymentOrderList.class.getPackage().getName());

        from(p2pPaymentOrderCollectionEndpoint)
                .setHeader(XMLDB_SERVICE_TYPE, constant("XQUERY"))
                .to(createP2PPaymentOrderQuery)
                .to(PAYMENT_ORDER_STORAGE)
                .convertBodyTo(String.class)
                .unmarshal(jaxb)
                .process(p2pPaymentCollectionProcessor);

        from(p2pPaymentOrderInstanceEndpoint)
                .process(p2PPaymentInstanceProcessor);

    }
}
