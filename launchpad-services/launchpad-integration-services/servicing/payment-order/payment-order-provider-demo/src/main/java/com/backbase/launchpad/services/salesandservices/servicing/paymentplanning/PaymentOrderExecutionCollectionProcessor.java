package com.backbase.launchpad.services.salesandservices.servicing.paymentplanning;

import com.backbase.launchpad.services.salesandservices.servicing.model.Frequency;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.joda.time.DateTimeConstants;
import org.joda.time.LocalDate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PaymentOrderExecutionCollectionProcessor implements Processor {


    @Override
    public void process(Exchange exchange) throws Exception {
        Message in = exchange.getIn();
        Message out = exchange.getOut();
        Object header = in.getHeader(Exchange.HTTP_METHOD);
        if ("GET".equals(header)) {
            handleGet(in, out);
        } else {
            exchange.getOut().setBody("{\"error\": \"Only GET is supported on payment-order-executions collection endpoint.\"}");
        }
    }

    private void handleGet(Message in, Message out) {
        PaymentOrderList paymentOrderList = in.getBody(PaymentOrderList.class);

        LocalDate startDate;
        LocalDate endDate;

        try {
            startDate = new LocalDate((String) in.getHeader("startDate", String.class));
            endDate = new LocalDate((String) in.getHeader("endDate", String.class));
            List<PaymentOrder> paymentOrders = paymentOrderList.getPaymentOrders();
            List<PaymentOrderExecution> paymentOrderExecutions = new ArrayList<PaymentOrderExecution>();
            for (PaymentOrder paymentOrder : paymentOrders) {
                paymentOrderExecutions.addAll(translateToPaymentOrderExecutions(paymentOrder));
            }
            List<PaymentOrderExecution> filteredPaymentOrderExecutions = filterPaymentOrderExecutions(paymentOrderExecutions, startDate, endDate);
            out.setBody(createResponse(filteredPaymentOrderExecutions));
        } catch (IllegalArgumentException e) {
            out.setBody("{\"error\": \"Both startDate and endDate should be formatted as yyyy-MM-dd\"}");
        }
    }

    /**
     * Translate a {@link PaymentOrder} into a {@link PaymentOrderExecution}
     */
    protected List<PaymentOrderExecution> translateToPaymentOrderExecutions(PaymentOrder paymentOrder) {
        List<PaymentOrderExecution> paymentOrderExecutions = new ArrayList<PaymentOrderExecution>();

        if (isRecurring(paymentOrder)) {
            LocalDate startDate = paymentOrder.getStartDate().toLocalDate();
            LocalDate endDate = paymentOrder.getEndDate().toLocalDate();

            if (Frequency.WEEKLY.equals(paymentOrder.getFrequency())) {
                int[] intervals = getIntIntervals(paymentOrder.getIntervals());
                LocalDate dateCursor = startDate;
                int dayOfWeekOfDateCursor = dateCursor.getDayOfWeek();

                while (isBeforeOrEqual(dateCursor, endDate)) {
                    for (int interval : intervals) {
                        if (interval >= dayOfWeekOfDateCursor) {
                            LocalDate executionDate = dateCursor.plusDays(interval - dayOfWeekOfDateCursor);
                            if (isBeforeOrEqual(executionDate, endDate)) {
                                paymentOrderExecutions.add(createPaymentOrderExecution(paymentOrder, executionDate));
                            }
                        }
                        if (interval < dayOfWeekOfDateCursor) {
                            LocalDate executionDate = dateCursor.plusDays(7 - dayOfWeekOfDateCursor + interval);
                            if (isBeforeOrEqual(executionDate, endDate)) {
                                paymentOrderExecutions.add(createPaymentOrderExecution(paymentOrder, executionDate));
                            }
                        }
                    }
                    dateCursor = dateCursor.plusDays(7 * paymentOrder.getEvery());
                }
            }
            if (Frequency.MONTHLY.equals(paymentOrder.getFrequency())) {
                int[] intervals = getIntIntervals(paymentOrder.getIntervals());
                LocalDate dateCursor = startDate;
                int dayOfMonth = dateCursor.getDayOfMonth();
                while (isBeforeOrEqual(dateCursor, endDate)) {
                    for (int interval : intervals) {
                        if (interval >= dayOfMonth) {
                            LocalDate executionDate = dateCursor.plusDays(interval - dayOfMonth);
                            if (isBeforeOrEqual(executionDate, endDate)) {
                                paymentOrderExecutions.add(createPaymentOrderExecution(paymentOrder, executionDate));
                            }
                        }
                        if (interval < dayOfMonth) {
                            LocalDate executionDate = dateCursor.plusMonths(1).withDayOfMonth(interval);
                            if (isBeforeOrEqual(executionDate, endDate)) {
                                paymentOrderExecutions.add(createPaymentOrderExecution(paymentOrder, executionDate));
                            }
                        }
                    }
                    dateCursor = dateCursor.plusMonths(paymentOrder.getEvery());
                }
            }
            if (Frequency.YEARLY.equals(paymentOrder.getFrequency())) {
                int[] intervals = getIntIntervals(paymentOrder.getIntervals());
                LocalDate dateCursor = startDate;
                int dayOfMonth = dateCursor.getDayOfMonth();
                int monthOfYear = dateCursor.getMonthOfYear();
                while (isBeforeOrEqual(dateCursor, endDate)) {
                    for (int interval : intervals) {
                        if (interval >= monthOfYear) {
                            LocalDate executionDate = dateCursor.withMonthOfYear(interval);
                            int executionDateDayOfMonthMaxValue = executionDate.dayOfMonth().getMaximumValue();
                            if (executionDateDayOfMonthMaxValue < dayOfMonth) {
                                executionDate.withDayOfMonth(executionDateDayOfMonthMaxValue);
                            } else {
                                executionDate.withDayOfMonth(dayOfMonth);
                            }

                            if (isBeforeOrEqual(executionDate, endDate)) {
                                paymentOrderExecutions.add(createPaymentOrderExecution(paymentOrder, executionDate));
                            }
                        }
                        if (interval < monthOfYear) {
                            LocalDate executionDate = dateCursor.plusYears(1).withMonthOfYear(interval);
                            int executionDateDayOfMonthMaxValue = executionDate.dayOfMonth().getMaximumValue();
                            if (executionDateDayOfMonthMaxValue < dayOfMonth) {
                                executionDate.withDayOfMonth(executionDateDayOfMonthMaxValue);
                            } else {
                                executionDate.withDayOfMonth(dayOfMonth);
                            }
                            if (isBeforeOrEqual(executionDate, endDate)) {
                                paymentOrderExecutions.add(createPaymentOrderExecution(paymentOrder, executionDate));
                            }
                        }
                    }
                    dateCursor = dateCursor.plusYears(paymentOrder.getEvery());
                }
            }
            if (Frequency.START_OF_THE_MONTH.equals(paymentOrder.getFrequency())) {
                LocalDate executionDate = startDate;
                while (isBeforeOrEqual(executionDate, endDate)) {
                    if (executionDate.getDayOfMonth() == 1) {
                        paymentOrderExecutions.add(createPaymentOrderExecution(paymentOrder, executionDate));
                    }
                    executionDate = executionDate.plusMonths(1).withDayOfMonth(1);
                }
            }
            if (Frequency.END_OF_THE_MONTH.equals(paymentOrder.getFrequency())) {
                LocalDate executionDate = startDate.dayOfMonth().withMaximumValue();
                while (isBeforeOrEqual(executionDate, endDate)) {
                    paymentOrderExecutions.add(createPaymentOrderExecution(paymentOrder, executionDate));
                    executionDate = executionDate.plusMonths(1).dayOfMonth().withMaximumValue();
                }
            }
            if (Frequency.LAST_FRIDAY_OF_THE_MONTH.equals(paymentOrder.getFrequency())) {
                LocalDate executionDate = getLastFridayOfTheMonth(startDate);
                while (isBeforeOrEqual(executionDate, endDate)) {
                    paymentOrderExecutions.add(createPaymentOrderExecution(paymentOrder, executionDate));
                    executionDate = getLastFridayOfTheMonth(executionDate.plusMonths(1));
                }
            }

        } else {
            paymentOrderExecutions.add(createPaymentOrderExecution(paymentOrder, paymentOrder.getExecutionDate().toLocalDate()));
        }
        return paymentOrderExecutions;
    }


    private LocalDate getLastFridayOfTheMonth(LocalDate date) {
        LocalDate result = date.dayOfMonth().withMaximumValue().withDayOfWeek(DateTimeConstants.FRIDAY);
        if (date.getMonthOfYear() != result.getMonthOfYear()) {
            result = result.minusWeeks(1);
        }
        return result;
    }

    private boolean isBeforeOrEqual(LocalDate date1, LocalDate date2) {
       return date1.equals(date2) || date1.isBefore(date2);
    }


    private int[] getIntIntervals(String intervals) {
        String[] tokens = intervals.split(",");
        int[] result = new int[tokens.length];
        int i = 0;
        for (String token : tokens) {
            result[i++] = Integer.parseInt(token);
        }
        return result;
    }

    protected PaymentOrderExecution createPaymentOrderExecution(PaymentOrder paymentOrder, LocalDate date) {
        PaymentOrderExecution paymentOrderExecution = new PaymentOrderExecution();
        paymentOrderExecution.setAmount(paymentOrder.getAmount());
        paymentOrderExecution.setCounterpartyName(paymentOrder.getCounterpartyName());
        paymentOrderExecution.setAccountName(paymentOrder.getAccountName());
        paymentOrderExecution.setCurrency(paymentOrder.getCurrency());
        paymentOrderExecution.setPaymentOrderId(paymentOrder.getId());
        paymentOrderExecution.setDate(date);
        return paymentOrderExecution;
    }


    /**
     * Check if the given {@link PaymentOrder} is recurring or not.
     */
    private boolean isRecurring(PaymentOrder paymentOrder) {
        return paymentOrder.getFrequency() != null;
    }

    /**
     * Given a start and end local date return the scheduled payment orders.
     */
    private List<PaymentOrderExecution> filterPaymentOrderExecutions(
            List<PaymentOrderExecution> paymentOrderExecutions, LocalDate start, LocalDate end) {
        List<PaymentOrderExecution> response = new ArrayList<PaymentOrderExecution>();
        for (PaymentOrderExecution paymentOrderExecution : paymentOrderExecutions) {
            if (contains(paymentOrderExecution.getDate(), start, end)) {
                response.add(paymentOrderExecution);
            }
        }
        return response;
    }

    /**
     * Returns if the first date is between the period (inclusive) defined by the next two parameters.
     */
    private boolean contains(LocalDate date, LocalDate start, LocalDate end) {
        return (date.isAfter(start) || date.isEqual(start)) && (date.isBefore(end) || date.isEqual(end));
    }

    /**
     * Create the response.
     */
    public JSONArray createResponse(List<PaymentOrderExecution> paymentOrderExecutions) {
        JSONArray response = new JSONArray();
        for (PaymentOrderExecution paymentOrderExecution : paymentOrderExecutions) {
            response.add(marshal(paymentOrderExecution));
        }
        return response;
    }

    /**
     * Marshal a {@link PaymentOrderExecution} into JSON
     */
    public JSONObject marshal(PaymentOrderExecution paymentOrderExecution) {
        JSONObject response = new JSONObject();
        response.put("paymentOrderId", paymentOrderExecution.getPaymentOrderId());
        response.put("accountName", paymentOrderExecution.getAccountName());
        response.put("amount", paymentOrderExecution.getAmount());
        response.put("currency", paymentOrderExecution.getCurrency());
        response.put("date", paymentOrderExecution.getDate().toString());
        response.put("counterpartyName", paymentOrderExecution.getCounterpartyName());
        return response;
    }


}
