package com.backbase.launchpad.services.salesandservices.servicing.p2p.payments;

import org.joda.time.DateTime;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.math.BigDecimal;
import java.util.Random;

/**
 * P2P payment order model for retrieving the P2P payments for the "Send-to-friend" widget.
 * It is using JAXB to extract the XML model from existdb and work with objects.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class P2PPaymentOrder {

    private String id;
    private String counterpartyName;
    private String counterpartyEmail;

    @XmlJavaTypeAdapter(BigDecimalAdapter.class)
    private BigDecimal amount;
    private String currency;

    @XmlJavaTypeAdapter(DateTimeAdapter.class)
    private DateTime initiationDateTime;

    private String description;
    private Status status = Status.values()[new Random().nextInt(2)]; // set random

    enum Status {
        PENDING, SENT
    }

    static class BigDecimalAdapter extends XmlAdapter<String, BigDecimal> {

        @Override
        public BigDecimal unmarshal(String value) throws Exception {
            return new BigDecimal(value);
        }

        @Override
        public String marshal(BigDecimal amount) throws Exception {
            return amount.toString();
        }
    }

    static class DateTimeAdapter extends XmlAdapter<String, DateTime> {

        @Override
        public DateTime unmarshal(String value) throws Exception {
            return new DateTime(Long.parseLong(value));
        }

        @Override
        public String marshal(DateTime v) throws Exception {
            return v.toString();
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCounterpartyName() {
        return counterpartyName;
    }

    public void setCounterpartyName(String counterpartyName) {
        this.counterpartyName = counterpartyName;
    }

    public String getCounterpartyEmail() {
        return counterpartyEmail;
    }

    public void setCounterpartyEmail(String counterpartyEmail) {
        this.counterpartyEmail = counterpartyEmail;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public DateTime getInitiationDateTime() {
        return initiationDateTime;
    }

    public void setInitiationDateTime(DateTime initiationDateTime) {
        this.initiationDateTime = initiationDateTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
