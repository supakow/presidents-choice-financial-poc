package com.backbase.launchpad.services.salesandservices.servicing.automation;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

import static com.backbase.launchpad.services.salesandservices.servicing.automation.Automation.Action.ActionType.EMAIL;
import static com.backbase.launchpad.services.salesandservices.servicing.automation.Automation.Trigger.Direction.valueOf;
import static com.backbase.launchpad.services.salesandservices.servicing.automation.Automation.Trigger.TriggerType.TRANSACTION_AMOUNT;

@Component
public class AutomationInstanceProcessor extends AutomationProcessor implements Processor {

    @Autowired
    private AutomationRepository automationRepository;

    @Override
    public void process(Exchange exchange) throws Exception {
        Message in = exchange.getIn();
        Message out = exchange.getOut();
        Object header = in.getHeader(Exchange.HTTP_METHOD);
        String customerId = (String) in.getHeader("backbasepartyId");
        if ("GET".equals(header)) {
            handleGet(customerId, in, out);
        } else if ("DELETE".equals(header)) {
            handleDELETE(customerId, in, out);
        } else if ("PUT".equals(header)) {
            handlePUT(customerId, in, out);
        } else {
            setErrorMessage(out, "GET, DELETE, PATCH and PUT are supported on automations/{automationId} endpoint", 405);
        }
    }

    private void handlePUT(String customerId, Message in, Message out) {
        Automation automation = automationRepository.findOne(customerId, getAutomationId(in));
        if (automation == null) {
            setErrorMessage(out, "automation not found", 404);
        } else {
            try {
                JSONObject parsedAutomation = getPayloadOrSendError(in);
                String name = (String) parsedAutomation.get("name");
                if (name != null) {
                    automation.setName(name);
                }

                Boolean enabled = (Boolean) parsedAutomation.get("enabled");
                if (enabled != null) {
                    automation.setEnabled(enabled.booleanValue());
                }

                JSONObject parsedTrigger = (JSONObject) parsedAutomation.get("trigger");
                if (parsedTrigger != null) {

                    automation.getTrigger().setType(TRANSACTION_AMOUNT);
                    Number amount = (Number) parsedTrigger.get("amount");
                    if (amount != null) {
                        automation.getTrigger().setAmount(new BigDecimal(amount.toString()));
                    }
                    String direction = (String) parsedTrigger.get("direction");
                    if (direction != null) {
                        automation.getTrigger().setDirection(valueOf(direction));
                    }
                    String accountId = (String) parsedTrigger.get("accountId");
                    if (accountId != null) {
                        automation.getTrigger().setAccountId(accountId);
                    }

                }

                JSONObject parsedAction = (JSONObject) parsedAutomation.get("action");
                if (parsedAction != null) {
                    automation.getAction().setType(EMAIL);
                    String actionValue = (String) parsedAction.get("value");
                    if (actionValue != null) {
                        automation.getAction().setValue(actionValue);
                    }
                }

                automationRepository.save(customerId, automation);
            } catch (InvalidAutomationPayloadException e) {
                setErrorMessage(out, e.getMessage(), 400);
            }
        }

    }

    private void handleDELETE(String customerId, Message in, Message out) {
        Automation automation = automationRepository.findOne(customerId, getAutomationId(in));
        if (automation == null) {
            setErrorMessage(out, "automation not found", 404);
        } else {
            automationRepository.delete(customerId, getAutomationId(in));
        }
    }

    private void handleGet(String customerId, Message in, Message out) {
        Automation automation = automationRepository.findOne(customerId, getAutomationId(in));
        if (automation == null) {
            setErrorMessage(out, "automation not found", 404);
        } else {
            out.setBody(renderDetails(automation));
        }
    }

    private String getAutomationId(Message in) {
        return in.getHeader("automationId", String.class);
    }
}
