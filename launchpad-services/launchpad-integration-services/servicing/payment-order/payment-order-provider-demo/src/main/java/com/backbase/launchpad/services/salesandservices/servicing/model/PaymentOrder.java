package com.backbase.launchpad.services.salesandservices.servicing.model;

import java.math.BigDecimal;

/**
 * Models a payment order.
 */
public class PaymentOrder {

    private String id;
    private String accountId;
    private String accountName;
    private Long initiationDateTime;
    private Long executionDateTime;
    private BigDecimal instructedAmount;
    private String instructedCurrency;
    private String counterpartyIban;     // used in SEPA (Single Euro Payments Area)
    private String counterpartyAccount;  // used in US, format: <routing-number>-<account-number>
    private String counterpartyName;
    private String counterpartyEmail;    // used for P2P payment order via email
    private String remittanceInformation;
    private String creditorReference;
    private String paymentCategory;
    private String status;
    private Schedule schedule;
    private PaymentOrderType type;

    public PaymentOrder() {
        initiationDateTime = System.currentTimeMillis();
    }

    public void validateState() throws InvalidPaymentOrderInputException {
        notNull(accountId, "accountId");
        notNull(id, "id");
        notNull(initiationDateTime, "initiationDateTime");
        notNull(instructedAmount, "instructedAmount");
        notNull(instructedCurrency, "instructedCurrency");
        notNull(counterpartyName, "counterpartyName");
        if (counterpartyIban == null && counterpartyAccount == null && counterpartyEmail == null) {
            throw new InvalidPaymentOrderInputException("counterpartyIban or counterpartyAccount or counterpartyEmail must be specified");
        }
    }

    public static void notNull(Object value, String name) throws InvalidPaymentOrderInputException {
        if (value == null) {
            throw new InvalidPaymentOrderInputException(name + " must be specified");
        }
    }


    public String getId() {
        return id;
    }

    public String getAccountId() {
        return accountId;
    }

    public String getAccountName() {
        return accountName;
    }

    public BigDecimal getInstructedAmount() {
        return instructedAmount;
    }

    public String getInstructedCurrency() {
        return instructedCurrency;
    }

    public String getCounterpartyIban() {
        return counterpartyIban;
    }

    public String getCounterpartyName() {
        return counterpartyName;
    }

    public Long getInitiationDateTime() {
        return initiationDateTime;
    }

    public Long getExecutionDateTime() {
        return executionDateTime;
    }

    public String getRemittanceInformation() {
        return remittanceInformation;
    }

    public String getCreditorReference() {
        return creditorReference;
    }

    public String getStatus() {
        return status;
    }

    public String getPaymentCategory() {
        return paymentCategory;
    }

    public Schedule getSchedule() {
        return schedule;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public void setExecutionDateTime(Long executionDateTime) {
        this.executionDateTime = executionDateTime;
    }

    public void setInstructedAmount(BigDecimal instructedAmount) {
        this.instructedAmount = instructedAmount;
    }

    public void setInstructedCurrency(String instructedCurrency) {
        this.instructedCurrency = instructedCurrency;
    }

    public void setCounterpartyIban(String counterpartyIban) {
        this.counterpartyIban = counterpartyIban;
    }

    public void setCounterpartyName(String counterpartyName) {
        this.counterpartyName = counterpartyName;
    }

    public void setRemittanceInformation(String remittanceInformation) {
        this.remittanceInformation = remittanceInformation;
    }

    public void setCreditorReference(String creditorReference) {
        this.creditorReference = creditorReference;
    }

    public void setPaymentCategory(String paymentCategory) {
        this.paymentCategory = paymentCategory;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }


    public void setCounterpartyAccount(String counterpartyAccount) {
        this.counterpartyAccount = counterpartyAccount;
    }

    public String getCounterpartyAccount() {
        return counterpartyAccount;
    }

    public PaymentOrderType getType() {
        return type;
    }

    public void setType(PaymentOrderType type) {
        this.type = type;
    }

    public String getCounterpartyEmail() {
        return counterpartyEmail;
    }

    public void setCounterpartyEmail(String counterpartyEmail) {
        this.counterpartyEmail = counterpartyEmail;
    }
}
