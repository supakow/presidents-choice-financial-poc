package com.backbase.launchpad.services.salesandservices.servicing.p2p.payments;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class P2PPaymentCollectionProcessor implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {
        Message in = exchange.getIn();
        Message out = exchange.getOut();
        Object header = in.getHeader(Exchange.HTTP_METHOD);
        if ("GET".equals(header)) {
            handleGet(in, out);
        } else {
            out.setBody("{\"error\": \"Only GET is supported on p2p-payments collection endpoint.\"}");
        }
    }

    private void handleGet(Message in, Message out) {
        P2PPaymentOrderList p2pPaymentOrderList = in.getBody(P2PPaymentOrderList.class);
        out.setBody(marshal(p2pPaymentOrderList.getPaymentOrders()));
    }

    /**
     * Marshal a list of {@link P2PPaymentOrder} into JSON
     */
    private JSONArray marshal (List<P2PPaymentOrder> p2pPaymentOrders) {
        JSONArray response = new JSONArray();
        for (P2PPaymentOrder p2pPaymentOrder : p2pPaymentOrders) {
            response.add(marshal(p2pPaymentOrder));
        }
        return response;
    }

    /**
     * Marshal a {@link P2PPaymentOrder} into JSON
     */
    private JSONObject marshal(P2PPaymentOrder p2pPaymentOrder) {
        JSONObject response = new JSONObject();
        response.put("id", p2pPaymentOrder.getId());

        JSONObject counterparty = new JSONObject();
        counterparty.put("name", p2pPaymentOrder.getCounterpartyName());

        JSONObject contact = new JSONObject();
        contact.put("value", p2pPaymentOrder.getCounterpartyEmail());
        contact.put("type", "EMAIL");
        counterparty.put("contact", contact);

        counterparty.put("logo", "");
        response.put("counterparty", counterparty);

        response.put("initiationDateTime", p2pPaymentOrder.getInitiationDateTime().toString());
        response.put("amount", p2pPaymentOrder.getAmount());
        response.put("currency", p2pPaymentOrder.getCurrency());
        response.put("description", p2pPaymentOrder.getDescription());
        response.put("status", p2pPaymentOrder.getStatus());
        return response;
    }
}
