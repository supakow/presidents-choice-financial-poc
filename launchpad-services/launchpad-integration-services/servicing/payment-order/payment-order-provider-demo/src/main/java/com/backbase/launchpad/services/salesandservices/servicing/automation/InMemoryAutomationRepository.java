package com.backbase.launchpad.services.salesandservices.servicing.automation;

import com.google.common.collect.Lists;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class InMemoryAutomationRepository implements AutomationRepository {

    private Map<String, HashMap<String, Automation>> automations = new HashMap<String, HashMap<String, Automation>>();

    @Override
    public List<Automation> findAll(String customerId) {
        HashMap<String, Automation> automationsForCustomer = automations.get(customerId);
        if (automationsForCustomer == null) {
            return Collections.emptyList();
        } else {
            // newly created automations get on top of the list
            return Lists.reverse(new ArrayList(automationsForCustomer.values()));
        }
    }

    @Override
    public Automation findOne(String customerId, String id) {
        HashMap<String, Automation> customerAutomations = automations.get(customerId);
        if (customerAutomations == null) {
            return null;
        } else {
            return customerAutomations.get(id);
        }
    }

    @Override
    public boolean exists(String customerId, String name) {
        return false;
    }

    @Override
    public Automation save(String customerId, Automation automation) {
        Automation newAutomation = copy(automation);
        if (automation.getId() == null) {
            newAutomation.setId(UUID.randomUUID().toString());
        }
        addAutomationToCustomer(customerId, newAutomation);
        return newAutomation;
    }

    @Override
    public void delete(String customerId, String categoryId) {
        automations.get(customerId).remove(categoryId);
    }

    private void addAutomationToCustomer(String customerId, Automation automation) {
        HashMap<String, Automation> automationsForCustomer = automations.get(customerId);
        if (automationsForCustomer == null) {
            automationsForCustomer = new LinkedHashMap<String, Automation>();
            automationsForCustomer.put(automation.getId(), automation);
            automations.put(customerId, automationsForCustomer);
        } else {
            automationsForCustomer.put(automation.getId(), automation);
        }

    }

    private Automation copy(Automation automation) {

        Automation result = new Automation();

        result.setId(automation.getId());
        result.setName(automation.getName());
        result.setEnabled(automation.isEnabled());
        result.setNrOfExecutions(automation.getNrOfExecutions());
        result.setLastExecution(automation.getLastExecution());
        result.setTriggerIcon(automation.getTriggerIcon());
        result.setActionIcon(automation.getActionIcon());

        result.getTrigger().setAccountId(automation.getTrigger().getAccountId());
        result.getTrigger().setType(automation.getTrigger().getType());
        result.getTrigger().setDirection(automation.getTrigger().getDirection());
        result.getTrigger().setAmount(automation.getTrigger().getAmount());

        result.getAction().setType(automation.getAction().getType());
        result.getAction().setValue(automation.getAction().getValue());

        return result;
    }






}
