package com.backbase.launchpad.services.salesandservices.servicing.automation;

import java.util.List;

public interface AutomationRepository {

    List<Automation> findAll(String customerId);

    Automation findOne(String customerId, String id);

    Automation save(String customerId, Automation automation);

    boolean exists(String customerId, String name);

    void delete(String customerId, String categoryId);

}
