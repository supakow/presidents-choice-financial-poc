package com.backbase.launchpad.services.salesandservices.servicing.p2p;

/**
 * Models a P2P enrollment at a bank.
 */
public class P2PEnrollment {

    private String email;
    private String accountNumber; // used for receiving P2P payments
    private boolean emailVerified;
    private String accountId;

    public P2PEnrollment(String email, String accountNumber) {
        this.email = email;
        this.accountNumber = accountNumber;
    }

    public String getEmail() {
        return email;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public boolean isEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified(boolean emailVerified) {
        this.emailVerified = emailVerified;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }
}
