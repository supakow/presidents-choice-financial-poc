package com.backbase.launchpad.services.salesandservices.servicing.p2p;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class InMemoryP2PEnrollmentRepository implements P2PEnrollmentRepository {

    private Map<String, P2PEnrollment> p2pEnrollments;

    public InMemoryP2PEnrollmentRepository() {
        p2pEnrollments = new HashMap<String, P2PEnrollment>();
    }

    private static final String P2P_ACCOUNT_ID_LISA = "2d96db20-d194-4fe0-9ab9-5da030381555";
    private static final String P2P_ACCOUNT_ID_THOMAS = "c8b46fe8-7888-45af-b9af-1422688d1ef8";


    @Override
    public P2PEnrollment find(String customerId) throws P2PEnrollmentNotFoundException {
        P2PEnrollment p2pEnrollment = p2pEnrollments.get(customerId);
        if (p2pEnrollment == null) {
            throw new P2PEnrollmentNotFoundException("customer with id " + customerId + " did not enroll for P2P");
        }
        return p2pEnrollment;
    }

    @Override
    public void create(String customerId, P2PEnrollment p2PEnrollment) throws P2PEnrollmentAlreadyExistsException {
        P2PEnrollment existingP2PEnrollment = p2pEnrollments.get(customerId);
        p2PEnrollment.setAccountId(customerId.equals("3") ? P2P_ACCOUNT_ID_LISA : (customerId.equals("2") ? P2P_ACCOUNT_ID_THOMAS : null));
        if (existingP2PEnrollment != null) {
            throw new P2PEnrollmentAlreadyExistsException("customer with id " + customerId + " has already enrolled for P2P");
        }
        p2pEnrollments.put(customerId, p2PEnrollment);
    }

    @Override
    public void update(String customerId, P2PEnrollment newP2PEnrollment) throws P2PEnrollmentNotFoundException {
        P2PEnrollment p2pEnrollment = find(customerId);
        if (p2pEnrollment != null) {
            if (newP2PEnrollment.getEmail() != null) {
                p2pEnrollment.setEmail(newP2PEnrollment.getEmail());
            }
            if (newP2PEnrollment.getAccountNumber() != null) {
                p2pEnrollment.setAccountNumber(newP2PEnrollment.getAccountNumber());
            }
        }

    }

    @Override
    public void delete(String customerId) throws P2PEnrollmentNotFoundException {
        P2PEnrollment p2pEnrollment = p2pEnrollments.get(customerId);
        if (p2pEnrollment == null) {
            throw new P2PEnrollmentNotFoundException("customer with id " + customerId + " did not enroll for P2P");
        }
        p2pEnrollments.remove(customerId);
    }

}
