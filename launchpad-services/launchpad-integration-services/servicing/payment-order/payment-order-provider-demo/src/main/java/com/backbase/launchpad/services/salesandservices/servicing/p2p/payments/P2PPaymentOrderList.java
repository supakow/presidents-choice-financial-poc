package com.backbase.launchpad.services.salesandservices.servicing.p2p.payments;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "paymentOrders")
@XmlAccessorType(XmlAccessType.FIELD)
public class P2PPaymentOrderList {

    @XmlElement(name = "paymentOrder")
    private List<P2PPaymentOrder> paymentOrders = new ArrayList<P2PPaymentOrder>();

    public List<P2PPaymentOrder> getPaymentOrders() {
        return paymentOrders;
    }

    public void setPaymentOrders(List<P2PPaymentOrder> paymentOrders) {
        this.paymentOrders = paymentOrders;
    }
}