package com.backbase.launchpad.services.salesandservices.servicing.p2p;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class P2PEnrollmentRoute extends RouteBuilder {

    // Main route
    public static final String P2P_ENROLLMENT_ENDPOINT = "seda:provider/servicing/p2p-enrollment";

    @Autowired
    private P2PEnrollmentProcessor p2pEnrollmentProcessor;

    @Override
    public void configure() throws Exception {
        from(P2P_ENROLLMENT_ENDPOINT).process(p2pEnrollmentProcessor);
    }

}
