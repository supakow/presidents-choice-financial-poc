<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:param name="transactionId" />
    <xsl:param name="categoryId" />
    <xsl:param name="currentDateTime" />

    <xsl:template match="/o">
        <o>
            <id type="string"><xsl:value-of select="$transactionId"/></id>
            <accountId type="string"><xsl:value-of select="accountId"/></accountId>
            <categoryId type="string"><xsl:value-of select="$categoryId"/></categoryId>
            <bookingDateTime type="number"><xsl:value-of select="$currentDateTime"/></bookingDateTime>

            <xsl:choose>
                <xsl:when test="type='P2P_EMAIL'">
                    <counterpartyAccount type="string"><xsl:value-of select="counterpartyEmail"/></counterpartyAccount>
                </xsl:when>
                <xsl:when test="type='BANK'">
                    <counterpartyAccount type="string"><xsl:value-of select="counterpartyIban"/></counterpartyAccount>
                </xsl:when>
            </xsl:choose>

            <counterpartyName type="string"><xsl:value-of select="counterpartyName"/></counterpartyName>
            <instructedAmount type="number"><xsl:value-of select="instructedAmount"/></instructedAmount>
            <instructedCurrency type="string"><xsl:value-of select="instructedCurrency"/></instructedCurrency>
            <creditDebitIndicator type="string">DBIT</creditDebitIndicator>
            <tags class="array">
                <e type="string"><xsl:value-of select="paymentCategory"/></e>
            </tags>
            <remittanceInformation type="string"><xsl:value-of select="remittanceInformation"/></remittanceInformation>
            <transactionCurrency type="string"><xsl:value-of select="instructedCurrency"/></transactionCurrency>
            <transactionAmount type="number"><xsl:value-of select="instructedAmount"/></transactionAmount>
            <transactionType type="string">Online Transfer</transactionType>
        </o>
    </xsl:template>
</xsl:stylesheet>