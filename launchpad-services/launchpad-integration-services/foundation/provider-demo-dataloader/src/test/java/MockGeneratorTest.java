import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;

import groovy.lang.GroovyClassLoader;
import groovy.lang.GroovyObject;

import java.io.File;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.backbase.launchpad.testdata.model.Account;
import com.backbase.launchpad.testdata.model.Transaction;
import com.backbase.launchpad.testdata.model.TransactionDetails;

public class MockGeneratorTest {
    GroovyObject groovyObject;

    @Before
    public void setUp() throws Exception {
        ClassLoader parent = getClass().getClassLoader();
        GroovyClassLoader loader = new GroovyClassLoader(parent);

        // when you run it from intellij you need to use:
        // Class groovyClass = loader.parseClass( new File("foundation/provider-demo-dataloader/src/main/groovy/com/backbase/launchpad/testdata/MockGenerator.groovy"))

        Class groovyClass = loader.parseClass( new File("src/main/groovy/com/backbase/launchpad/testdata/MockGenerator.groovy"));

        groovyObject = (GroovyObject) groovyClass.newInstance();
    }


    @Test
    public void testGenerateAccounts() throws Exception {
        Object[] args = {"3"};
        List<Account> accounts = (List<Account>) groovyObject.invokeMethod("generateAccounts", args);


        assertEquals(3, accounts.size());
    }

    @Test
    public void testGenerateTransactions() throws Exception {
        Object[] args = {"3"};
        List<Transaction> transactions = (List<Transaction>) groovyObject.invokeMethod("generateTransactions", args);

        assertNotSame(0, transactions.size());
        assertNotNull(transactions.get(0).counterpartyName);
    }

    @Test
    public void testGenerateTransactionDetails() throws Exception {
        Object[] args = {"3"};
        TransactionDetails details = (TransactionDetails) groovyObject.invokeMethod("generateTransactionDetails", args);

        assertNotNull(details);
    }
}
