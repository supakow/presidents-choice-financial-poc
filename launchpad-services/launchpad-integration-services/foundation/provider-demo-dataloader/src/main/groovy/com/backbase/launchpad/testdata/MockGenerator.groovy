package com.backbase.launchpad.testdata
import com.backbase.launchpad.testdata.model.Account
import com.backbase.launchpad.testdata.model.Transaction
import com.backbase.launchpad.testdata.model.TransactionDetails
import org.apache.commons.lang.RandomStringUtils
import org.apache.commons.lang.math.RandomUtils
import org.joda.time.DateTime

/**
 * Mock data generator stored in existdb for the launchpad services.
 */
class MockGenerator {
    def incomingPaymentTitles = ["Salary", "Interest"]
    def outgoingPaymentTitles = ["Cash Withdrawl", "Online Transfer", "Payment Debit Card", "Payment Credit Card", "Bill Payment", "Transaction"]
    def randomNames = [["Texaco", "texaco.png"], ["Safeway Inc.", null], ["Francine", "francine.png"], ["Macy's", null], ["CH Energy Group", null], ["AT&T", null],
            ["T-Mobile", "tmobile.png"], ["Atlantic City Electric", "atlanticelec.png"], ["Murray's Bagels", null], ["The Tea Lounge", "tealounge.png"], ["McDonald's", null]]

    def generatedNumbers = new HashSet()
    def currency = "EUR"

    def randomBanks = new ArrayList()
    def merchantTypes = new ArrayList()

    def ibans = new HashMap<String, String[]>();

    public MockGenerator() {
        MockGenerator.getResourceAsStream("/randomBanks.csv").eachLine { line -> randomBanks.add(line.split(",")) }
        MockGenerator.getResourceAsStream("/randomMerchantTypes.csv").eachLine { line -> merchantTypes.add(line.split(",")[1]) }


        ibans.put("3", ['NL66INGB0280680457', 'NL67RABO0842497587', 'NL56ABNA0519431642']);
        ibans.put("2", ['NL46INGB0738492396', 'NL47ABNA0268432082', 'NL38ABNA0814097596']);

    }

    def generateAccounts(String customerId) {
        def accounts = new ArrayList<Account>()

        println("generateAccounts called with customerId " : customerId);

        // ignoring admin customer
        if (customerId.equals("2") || customerId.equals("3")) {
            println("Populating customer with id:" + customerId)
            accounts.add(createAccountsFor(customerId, "CASH", "Personal Checking Account", 1, ibans.get(customerId)[0], "ENABLED"))
            accounts.add(createAccountsFor(customerId, "CREDIT", "Business Account", -1, ibans.get(customerId)[1], "ENABLED"))
            accounts.add(createAccountsFor(customerId, "INVESTMENT", "Shared Account", 1, ibans.get(customerId)[2], "BLOCKED"))
        }

        // US customer
        if (customerId.equals("4")) {
            println("Populating customer with id:" + customerId)
            accounts.add(createAccountsFor(customerId, "CASH", "Checking Account", 1, "121181976 1231241241", "ENABLED"))
            accounts.add(createAccountsFor(customerId, "CREDIT", "Credit Card Shopping", 1, "121181976 747567853", "ENABLED"))
            accounts.add(createAccountsFor(customerId, "CREDIT", "Credit Card Online", 1, "121181976 1363461371", "ENABLED"))
            accounts.add(createAccountsFor(customerId, "INVESTMENT", "Savings Account", 1, "121181976 262236346", "BLOCKED"))
            accounts.add(createAccountsFor(customerId, "INVESTMENT", "Pension Account", 1, "121181976 8583568358", "BLOCKED"))
        }

        return accounts;
    }

    def generateTransactions(String accountId) {
        def transactions = new ArrayList();
        int numberOfDaysOfHistory = 30;

        for (int i = 0; i < numberOfDaysOfHistory; i++) {
            // transaction every two days to spread them a bit
            def transactionDate = new DateTime().minusDays(i * 2)

            //Salary
            if (transactionDate.dayOfMonth == 25) {
                def counterparty = randomNames[RandomUtils.nextInt(randomNames.size() - 1)];

                Transaction transaction = new Transaction()
                transaction.id = UUID.randomUUID().toString()
                transaction.accountId = accountId
                transaction.bookingDateTime = transactionDate.minusMinutes(RandomUtils.nextInt(3600)).getMillis();
                transaction.counterpartyAccount = generateRandomNumber(9)
                transaction.transactionAmount = randomAmount(3000, 4000, 1)
                transaction.creditDebitIndicator = "CRDT"
                transaction.transactionCurrency = currency
                transaction.counterpartyName = counterparty[0]
                transaction.transactionType = incomingPaymentTitles[RandomUtils.nextInt(incomingPaymentTitles.size() - 1)]
                transaction.categoryId = "a1fae863-01e9-4f73-81a6-8d4e26c00f9d"; // PENDING
                transaction.tags.add("Deposit")
                transactions.add(transaction);
            }

            for (int j = 0; j < RandomUtils.nextInt(5); j++) {
                //Outgoing payments
                def counterparty = randomNames[RandomUtils.nextInt(randomNames.size() - 1)];

                Transaction transaction = new Transaction()
                transaction.id = UUID.randomUUID().toString()
                transaction.transactionAmount = randomAmount(1, 300, 1)
                transaction.transactionCurrency = currency;
                transaction.accountId = accountId
                transaction.bookingDateTime = transactionDate.minusMinutes(RandomUtils.nextInt(3600)).getMillis();
                transaction.counterpartyAccount = generateRandomNumber(9)
                transaction.counterPartyLogoPath = (counterparty[1] != null) ? "/portalserver/static/launchpad/widgets/transactions/media/" + counterparty[1] : "";
                transaction.transactionAmount = randomAmount(0.1, 400, 1)
                transaction.creditDebitIndicator = "DBIT"
                transaction.counterpartyName = counterparty[0]
                transaction.transactionType = outgoingPaymentTitles[RandomUtils.nextInt(outgoingPaymentTitles.size() - 1)]
                transaction.categoryId = "a1fae863-01e9-4f73-81a6-8d4e26c00f9d"; // PENDING
                transaction.tags.add("Withdrawal")

                transactions.add(transaction);
            }
        }

        return transactions;
    }

    def generateTransactionDetails(String transactionId) {
        TransactionDetails details = new TransactionDetails();
        details.transactionId = transactionId;
        details.remittanceInformation = "Description"
        details.merchantType = merchantTypes.get(RandomUtils.nextInt(merchantTypes.size() - 1))
        details.address.street = "Delancey St"
        details.address.city = "New York"
        details.address.country = "USA"
        details.address.state = "NY"
        details.location.latitude = "40.7172772"
        details.location.longitude = "-73.9854509"

        return details;
    }

    private def generateRandomNumber(length) {
        def result = RandomStringUtils.randomNumeric(length);

        if (generatedNumbers.contains(result))
            return generateRandomNumber(10)
        else
            generatedNumbers.add(result)
        return result

    }

    private def randomAmount(min, max, factor) {
        BigDecimal amount = new BigDecimal((min * factor) + (int) (Math.random() * (((max * factor)) - ((min * factor)) + 1)))

        return amount;
    }

    private def createAccountsFor(customerId, groupCode, accountName, factor, iban, enabled) {

        Account account = new Account()
        account.id = UUID.randomUUID().toString()
        account.iban = iban
        account.bban = iban.substring(9)

        if (customerId.equals("4")) {
            account.currency = "USD"
        } else {
            account.currency = currency
        }

        account.partyId = customerId
        account.name = accountName
        account.availableBalance = randomAmount(10000, 12000, factor)
        account.balance = account.availableBalance
        account.groupCode = groupCode
        account.status = enabled

        return account;
    }

    public static void main(String[] args) {
        println UUID.randomUUID();
    }
}
