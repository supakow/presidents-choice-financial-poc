package com.backbase.launchpad.testdata.model;

public class TransactionDetails {
    public Address address = new Address();
    public Location location = new Location();
    public String merchantType;
    public String transactionId;
    public String remittanceInformation;

    public Address getAddress() {
        return address;
    }

    public Location getLocation() {
        return location;
    }

    public String getMerchantType() {
        return merchantType;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public String getRemittanceInformation() {
        return remittanceInformation;
    }


    public static class Address {
        public String street;
        public String city;
        public String state;
        public String country;

        public String getStreet() {
            return street;
        }

        public String getCity() {
            return city;
        }

        public String getState() {
            return state;
        }

        public String getCountry() {
            return country;
        }
    }

    public static class Location {
        public String latitude;
        public String longitude;

        public String getLatitude() {
            return latitude;
        }

        public String getLongitude() {
            return longitude;
        }
    }
}
