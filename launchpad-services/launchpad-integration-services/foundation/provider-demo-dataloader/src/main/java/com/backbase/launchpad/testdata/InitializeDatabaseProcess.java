package com.backbase.launchpad.testdata;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.ServiceStatus;
import org.xmldb.api.DatabaseManager;
import org.xmldb.api.base.Collection;
import org.xmldb.api.modules.CollectionManagementService;

import java.util.concurrent.TimeUnit;

public class InitializeDatabaseProcess implements Processor {
    public static final String ROUTE_ID = "providerMockDataLoaderRoute";

    @Override
    public void process(Exchange exchange) throws Exception {
        String xmldbDatabaseUrl = exchange.getIn().getHeader("XMLDB_DATABASE_URL", String.class);
        String username = exchange.getIn().getHeader("XMLDB_USERNAME", String.class);
        String password = exchange.getIn().getHeader("XMLDB_PASSWORD", String.class);

        Collection start = DatabaseManager.getCollection(xmldbDatabaseUrl + "/db/launchpad");
        if (start != null) {
            Collection collection = DatabaseManager.getCollection(xmldbDatabaseUrl + "/db", username, password);
            CollectionManagementService mgt = (CollectionManagementService) collection.getService("CollectionManagementService", "1.0");

            mgt.removeCollection("launchpad");
        }

        ServiceStatus status = exchange.getContext().getRouteStatus(ROUTE_ID);
        if (status.isStarted()) {
            exchange.getContext().stopRoute(ROUTE_ID, 30000, TimeUnit.MILLISECONDS);
        }

        exchange.getContext().startRoute(ROUTE_ID);
    }
}
