package com.backbase.launchpad.testdata.model;

import java.math.BigDecimal;

public class Account {

    public String id;
    public String partyId;
    public String bban;
    public String iban;
    public String currency;
    public String name;
    public BigDecimal availableBalance;
    public BigDecimal balance;
    public String groupCode;
    public String status;

    public String getId() {
        return id;
    }

    public String getPartyId() {
        return partyId;
    }

    public String getBban() {
        return bban;
    }

    public String getIban() {
        return iban;
    }

    public String getCurrency() {
        return currency;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getAvailableBalance() {
        return availableBalance;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public String getStatus() {
        return status;
    }
}
