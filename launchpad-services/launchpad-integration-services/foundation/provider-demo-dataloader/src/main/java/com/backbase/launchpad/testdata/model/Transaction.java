package com.backbase.launchpad.testdata.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Transaction {
    public String id;
    public String accountId;
    public Long bookingDateTime;
    public BigDecimal transactionAmount;
    public String transactionCurrency;
    public BigDecimal instructedAmount;
    public String instructedCurrency;
    public String creditDebitIndicator;
    public String counterpartyAccount;
    public String counterpartyName;
    public String transactionType;
    public String counterPartyLogoPath;
    public String categoryId;

    public List<String> tags = new ArrayList<String>();

    public String getId() {
        return id;
    }

    public String getAccountId() {
        return accountId;
    }

    public Long getBookingDateTime() {
        return bookingDateTime;
    }

    public BigDecimal getTransactionAmount() {
        return transactionAmount;
    }

    public String getTransactionCurrency() {
        return transactionCurrency;
    }

    public BigDecimal getInstructedAmount() {
        return instructedAmount;
    }

    public String getInstructedCurrency() {
        return instructedCurrency;
    }

    public String getCreditDebitIndicator() {
        return creditDebitIndicator;
    }

    public String getCounterpartyAccount() {
        return counterpartyAccount;
    }

    public String getCounterpartyName() {
        return counterpartyName;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public String getCategoryId() { return categoryId; }

    public String getCounterPartyLogoPath() {
        return counterPartyLogoPath;
    }

    public List<String> getTags() {
        return tags;
    }
}
