package com.backbase.launchpad.services.notification.provider;

import java.util.Collection;

import javax.servlet.http.Cookie;

import org.atmosphere.cpr.AtmosphereResource;
import org.atmosphere.cpr.Broadcaster;
import org.atmosphere.cpr.BroadcasterFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.backbase.launchpad.services.notification.model.Message;

/**
 * Handles broadcasting events to connected clients (AtmosphereResources)
 */
public class AtmospherePublishService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AtmospherePublishService.class);

    public void publish(Message message) {
        BroadcasterFactory broadcasterFactory = BroadcasterFactory.getDefault();

        // could be null during bootstrapping, when Atmosphere did not initialize itself yet
        if (broadcasterFactory != null) {
            Broadcaster eventBroadcaster = broadcasterFactory.lookup("/event");
            if (eventBroadcaster != null) {
                Collection<AtmosphereResource> atmosphereResources = eventBroadcaster.getAtmosphereResources();
                for (AtmosphereResource atmosphereResource : atmosphereResources) {
                    if (message.getSessionId().equals(getSessionId(atmosphereResource))) {
                        LOGGER.info("Broadcasting message with event {} to client with session id {}",
                                message.getEvent(), message.getSessionId());
                        eventBroadcaster.broadcast(message, atmosphereResource);
                    }
                }

            } else {
                LOGGER.error("Check your atmosphere configuration, '/event' broadcaster was not configured");
            }
        }
    }

    public String getSessionId(AtmosphereResource resource) {
        String result = "";
        Cookie[] cookies = resource.getRequest().getCookies();
        for (Cookie cookie : cookies) {
            if ("JSESSIONID".equals(cookie.getName())) {
                result = cookie.getValue();
            }
        }
        if (result.isEmpty()) {
            LOGGER.error("Atmosphere did not receive session id, event broadcasting will not work");
        }
        return result;
    }

}