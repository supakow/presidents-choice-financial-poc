package com.backbase.launchpad.services.notification.provider;

import java.io.IOException;

import org.atmosphere.config.managed.Decoder;
import org.codehaus.jackson.map.ObjectMapper;

import com.backbase.launchpad.services.notification.model.Message;

/**
 * Decode a String into a {@link Message}.
 */
public class MessageDecoder implements Decoder<String, Message> {

    private final ObjectMapper mapper = new ObjectMapper();

    @Override
    public Message decode(String s) {
        try {
            return mapper.readValue(s, Message.class);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}