package com.backbase.launchpad.services.notification.model;

import java.util.Date;

public class Message {

    private Event event;
    private String sessionId;
    private long time;

    public Message() {
    }

    public Message(Event event, String sessionId) {
        this.event = event;
        this.sessionId = sessionId;
        this.time = new Date().getTime();
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
}
