package com.backbase.launchpad.services.contactdialogue;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import com.backbase.launchpad.services.model.Level;
import com.backbase.launchpad.services.model.Message;

/**
 * Processor responsible for creating a {@link Message} value object with values from camel exchange headers.
 */
public class NewMessageProcessor implements Processor {

    public void process(Exchange exchange) throws Exception {
        Message.Link link = null;

        String linkRel = header(exchange, "link.rel", String.class);
        String linkUri = header(exchange, "link.uri", String.class);
        if (linkRel != null && linkUri != null) {
            link = new Message.Link();
            link.rel = linkRel;
            link.uri = linkUri;
        }

        Message message = new Message();
        message.partyId = header(exchange, "backbasepartyid", String.class);
        message.id = header(exchange, "id", String.class);
        message.closable = header(exchange, "closable", Boolean.class);
        message.createdOn = System.currentTimeMillis();
        message.level = Level.valueOf(header(exchange, "level", String.class));
        message.message = header(exchange, "message", String.class);
        message.expiresOn = header(exchange, "expiresOn", Long.class);
        message.deactivated = false;

        if (link != null) {
            message.links.add(link);
        }

        exchange.getIn().setBody(message);
    }

    private <T> T header(Exchange exchange, String name, Class<T> type) {
        return exchange.getIn().getHeader(name, type);
    }
}
