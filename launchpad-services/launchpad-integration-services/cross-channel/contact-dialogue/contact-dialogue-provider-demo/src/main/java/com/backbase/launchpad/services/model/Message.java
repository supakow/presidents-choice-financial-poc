package com.backbase.launchpad.services.model;

import java.util.ArrayList;
import java.util.List;

/**
 * An intermediary value object about for storing a message.
 * It is used by {@link XmlJsonDataFormatWrapper} to convert it to json.
 */
public class Message {

    public String partyId;
    public String id;
    public Level level;
    public Long createdOn;
    public String message;
    public Long expiresOn;
    public Boolean closable;
    public Boolean deactivated;
    public List<Link> links = new ArrayList<Link>();

    public String getPartyId() {
        return partyId;
    }

    public String getId() {
        return id;
    }

    public Level getLevel() {
        return level;
    }

    public Long getCreatedOn() {
        return createdOn;
    }

    public String getMessage() {
        return message;
    }

    public Long getExpiresOn() {
        return expiresOn;
    }

    public Boolean getClosable() {
        return closable;
    }

    public Boolean getDeactivated() {
        return deactivated;
    }

    public List<Link> getLinks() {
        return links;
    }

    public static class Link {
        public String rel;
        public String uri;

        public String getRel() {
            return rel;
        }

        public String getUri() {
            return uri;
        }
    }
}
