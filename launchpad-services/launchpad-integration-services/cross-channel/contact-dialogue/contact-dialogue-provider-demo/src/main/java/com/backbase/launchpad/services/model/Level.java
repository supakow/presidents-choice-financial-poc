package com.backbase.launchpad.services.model;

/**
 * Represents severity of the {@link Message}.
 */
public enum Level {

    WARNING,
    INFO,
    SEVERE

}
