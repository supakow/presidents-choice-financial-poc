package com.backbase.launchpad.services.authentication;

import junit.framework.Assert;

import org.apache.commons.codec.binary.Base32;
import org.junit.Test;

import com.backbase.launchpad.services.crosschannel.authentication.TOTP;

/**
 * Unit tests for {@link TOTP}.
 */
public class TOTPTest {

    @Test
    public void codeGeneration() {
        String secret = "7KWYN6ZMLV5RURKA";
        byte[] s = new Base32().decode(secret);
        long time = System.currentTimeMillis() / 30000L;

        int totp = TOTP.getCode(s, time);
        boolean subject = TOTP.verifyCode(secret, totp, time, 1);

        Assert.assertEquals(true, subject);
    }

    @Test
    public void codeGenerationWithDelay() {
        String secret = "7KWYN6ZMLV5RURKA";
        byte[] s = new Base32().decode(secret);
        long t1 = ((System.currentTimeMillis() + 120000) / 30000L);
        long t2 = System.currentTimeMillis() / 30000L;

        int totp = TOTP.getCode(s, t1);
        boolean subject = TOTP.verifyCode(secret, totp, t2, 2);

        Assert.assertEquals(false, subject);
    }
}
