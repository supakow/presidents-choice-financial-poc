package com.backbase.launchpad.services.crosschannel.authentication.processor;

import static com.backbase.launchpad.services.crosschannel.authentication.builder.ActionsBuilder.newActions;
import static com.backbase.launchpad.services.crosschannel.authentication.builder.SessionBuilder.newSession;
import static com.google.common.base.Preconditions.checkNotNull;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import com.backbase.launchpad.services.crosschannel.authentication.AuthenticationRoutes;

/**
 * Camel {@link Processor} for creating the response for the second step of the authentication workflow.
 */
public class ConstructNextStepResponse implements Processor {

    public void process(Exchange exchange) throws Exception {
        String secondFactor = exchange.getIn().getHeader("secondFactor", String.class);
        String id = exchange.getIn().getHeader("id", String.class);

        checkNotNull("id", id);

        String json = newSession()
                .withIdentity(id)
                .withStatus(AuthenticationRoutes.INITIATED_SESSION_STATUS)
                .withNextActions(secondFactor,
                        newActions()
                                .addAction("verify",
                                        "/authentication/session/" + id + "/" + secondFactor))
                .build();

        exchange.getIn().setBody(json);
    }
}
