package com.backbase.launchpad.services.crosschannel.authentication.processor;

import static com.google.common.base.Preconditions.checkNotNull;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import com.google.common.base.Strings;

/**
 * Created with IntelliJ IDEA.
 * User: veniamin
 * Date: 11/18/13
 * Time: 5:05 PM
 * To change this template use File | Settings | File Templates.
 */
public class VerificationRequiredDecision implements Processor {
    public void process(Exchange exchange) throws Exception {
        String username = exchange.getIn().getHeader("username", String.class);
        String secondFactor = exchange.getIn().getHeader("secondFactor", String.class);
        Boolean passwordAuthenticationPassed =
                exchange.getProperty("passwordAuthenticationPassed", Boolean.class);

        checkNotNull("username", username);
        checkNotNull("passwordAuthenticationPassed", passwordAuthenticationPassed);

        boolean isAdditionalVerificationRequired = !Strings.isNullOrEmpty(secondFactor);
        if (passwordAuthenticationPassed) {
            if (!isAdditionalVerificationRequired) {
                exchange.setProperty("sessionIsVerified", true);
            }
        } else {
            if (!isAdditionalVerificationRequired) {
                exchange.setProperty("authenticationFailed", true);
            }
        }

        // -- regardless of password authentication status, if second step verification is required for the user, force "fake" second step
        if (isAdditionalVerificationRequired) {
            exchange.setProperty("sessionIsVerified", false);
        }
    }
}
