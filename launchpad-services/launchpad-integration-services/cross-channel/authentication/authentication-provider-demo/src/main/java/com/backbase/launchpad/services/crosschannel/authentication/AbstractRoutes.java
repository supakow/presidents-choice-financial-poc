package com.backbase.launchpad.services.crosschannel.authentication;

import org.apache.camel.Endpoint;
import org.apache.camel.EndpointInject;
import org.apache.camel.builder.RouteBuilder;

/**
 * Defines Camel routes which are common across the demo authentication provider module.
 */
public abstract class AbstractRoutes extends RouteBuilder  {

    protected static final String XMLDB_SERVICE_TYPE = "XMLDB_SERVICE_TYPE";

    @EndpointInject(uri = "xmldb:exist:///db/launchpad/sessions?username=admin&password=")
    protected Endpoint sessionEntityEndpoint;

    @EndpointInject(uri ="direct:updateAccessTimeRoute")
    protected Endpoint updateAccessTimeEndpoint;

    @EndpointInject(uri ="direct:mapHttpSessionRoute")
    protected Endpoint mapHttpSessionEndpoint;

    @EndpointInject(uri = "direct:setInitiationTimeRoute.vm")
    protected Endpoint setInitiationTimeEndpoint;

    @EndpointInject(uri ="direct:updateSessionStatusRoute")
    protected Endpoint updateSessionStatusEndpoint;

    /**
     * Redefined in the concrete implementations. This implementation is left empty.
     */
    @Override
    public void configure() throws Exception {
    }
}
