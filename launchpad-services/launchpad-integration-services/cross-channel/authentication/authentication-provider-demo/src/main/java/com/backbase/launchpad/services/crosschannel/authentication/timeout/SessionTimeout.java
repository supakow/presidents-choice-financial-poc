package com.backbase.launchpad.services.crosschannel.authentication.timeout;

/**
 * Session timeout constants.
 */
public interface SessionTimeout {

    String SESSION_ACCESS_TIME = "accessTime";

    String SESSION_EXPIRED = "sessionExpired";

    int MAX_SESSION_TIMEOUT = 600; // seconds;

}
