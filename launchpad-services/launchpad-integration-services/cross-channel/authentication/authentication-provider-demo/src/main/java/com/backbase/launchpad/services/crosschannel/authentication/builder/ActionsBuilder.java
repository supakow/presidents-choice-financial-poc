package com.backbase.launchpad.services.crosschannel.authentication.builder;

import static com.google.common.base.Preconditions.checkNotNull;

import net.sf.json.JSON;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class ActionsBuilder {
    private JSONArray actions = new JSONArray();
    private ActionsBuilder() {}

    public static ActionsBuilder newActions() {
        return new ActionsBuilder();
    }

    public ActionsBuilder addAction(String rel, String uri) {
        checkNotNull(rel, "rel is not provided");
        checkNotNull(uri, "uri is not provided");

        JSONObject action = new JSONObject();
        action.put("rel", rel);
        action.put("uri", uri);

        actions.add(action);

        return this;
    }

    public JSON build() {
        return actions;
    }
}
