package com.backbase.launchpad.services.crosschannel.authentication.timeout;

import net.sf.json.JSONObject;

/**
 * Helper class to provide the json response for the session timeout services.
 */
public class SessionTimeoutResponse {

    private int remainingTime;

    private SessionTimeoutResponse() {
    }

    public static SessionTimeoutResponse newSessionTimeoutResponse() {
        return new SessionTimeoutResponse();
    }

    public SessionTimeoutResponse withRemainingTime(int remainingTime) {
        this.remainingTime = remainingTime;
        return this;
    }


    public String build() {
        JSONObject response = new JSONObject();
        response.put("remainingTime", remainingTime);

        return response.toString();
    }
}
