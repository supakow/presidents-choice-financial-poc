package com.backbase.launchpad.services.crosschannel.authentication.builder;

import static com.google.common.base.Preconditions.checkNotNull;

import net.sf.json.JSONObject;

/**
 * Helper class for creating the json response from camel processors related to authentication workflow.
 */
public class SessionBuilder {

    private String id;
    private String status;
    private String nextRel;
    private ActionsBuilder actionsBuilder;

    private SessionBuilder() {}

    public static SessionBuilder newSession() {
        return new SessionBuilder();
    }

    public SessionBuilder withIdentity(String id) {
        this.id = id;

        return this;
    }

    public SessionBuilder withStatus(String status) {
        this.status = status;

        return this;
    }

    public SessionBuilder withNextActions(String rel, ActionsBuilder builder) {
        this.nextRel = rel;
        this.actionsBuilder = builder;

        return this;
    }

    public String build() {
        checkNotNull(id, "id is not provided");
        checkNotNull(status, "status is not provided");

        JSONObject session = new JSONObject();
        session.put("id", id);
        session.put("status", status);

        JSONObject json = new JSONObject();
        json.put("session", session);

        if (nextRel != null && actionsBuilder != null) {
            JSONObject nextActions = new JSONObject();
            nextActions.put("rel", nextRel);
            nextActions.put("actions", actionsBuilder.build());

            json.put("next", nextActions);
        }

        return json.toString();
    }
}