package com.backbase.launchpad.services.crosschannel.authentication;

import static org.apache.camel.builder.PredicateBuilder.and;
import static org.apache.camel.builder.PredicateBuilder.isNotNull;
import static org.apache.camel.builder.PredicateBuilder.not;

import org.apache.camel.Endpoint;
import org.apache.camel.EndpointInject;
import org.apache.camel.Predicate;

import com.backbase.launchpad.services.crosschannel.authentication.timeout.AccessSessionResponseProcessor;
import com.backbase.launchpad.services.crosschannel.authentication.timeout.CheckSessionExpiredProcessor;
import com.backbase.launchpad.services.crosschannel.authentication.timeout.CurrentTimeProcessor;
import com.backbase.launchpad.services.crosschannel.authentication.timeout.InvalidSessionResponseProcessor;
import com.backbase.launchpad.services.crosschannel.authentication.timeout.ValidateSessionResponseProcessor;

/**
 * Defines Camel routes for session management.
 */
public class SessionTimeoutRoutes extends AbstractRoutes {

    private static final String SESSION_ENDPOINT_URI = "xmldb:exist:///db/launchpad/sessions?XPath=/o[httpSessionId/text() = '${headers.BackbaseUserSessionId}']";

    // main endpoints

    @EndpointInject(uri = "seda:provider/cross-channel/authentication/session/validate")
    private Endpoint validateSessionEndpoint;

    @EndpointInject(uri = "seda:provider/cross-channel/authentication/session")
    private Endpoint accessSessionEndpoint;

    // internal endpoints

    @EndpointInject(uri = "velocity:updateAccessTimeXQuery.vm")
    private Endpoint updateAccessTimeQueryEndpoint;

    @EndpointInject(uri = "velocity:mapHttpSessionXQuery.vm")
    private Endpoint mapHttpSessionQueryEndpoint;

    @EndpointInject(uri = "direct:validateSessionResponseRoute")
    private Endpoint validateSessionResponseEndpoint;

    @EndpointInject(uri = "direct:accessSessionResponseRoute")
    private Endpoint accessSessionResponseEndpoint;

    @EndpointInject(uri = "direct:checkSessionRoute")
    private Endpoint checkSessionEndpoint;

    @EndpointInject(uri = "direct:invalidSessionResponseRoute")
    private Endpoint invalidSessionResponseEndpoint;

    @Override
    public void configure() throws Exception {
        updateAccessTimeRoute();
        mapHttpSessionRoute();

        validateSessionRoute();
        accessSessionRoute();
        createValidateSessionResponseRoute();
        createAccessSessionResponseRoute();
        createInvalidSessionResponseRoute();
        checkSessionRoute();
    }

    /**
     * Set the access time of the session.
     */
    private void updateAccessTimeRoute() {
        from(updateAccessTimeEndpoint)
                .setHeader(XMLDB_SERVICE_TYPE, constant("XQUERY"))
                .process(new CurrentTimeProcessor())
                .to(updateAccessTimeQueryEndpoint)
                .to(sessionEntityEndpoint);
    }

    /**
     * Map the HTTP session id to the workflow id.
     */
    private void mapHttpSessionRoute() {
        from(mapHttpSessionEndpoint)
                .setHeader(XMLDB_SERVICE_TYPE, constant("XQUERY"))
                .to(mapHttpSessionQueryEndpoint)
                .to(sessionEntityEndpoint);

    }

    /**
     * Check if the session was invalidated externally or the session is expired
     */
    private void checkSessionRoute() {
        Predicate isValidSession = and(isNotNull(simple("${body}")), xpath("/o/status = 'Confirmed'").booleanResult());
        Predicate isSessionExpired = simple("${property.sessionExpired} == true");

        from(checkSessionEndpoint)
            .recipientList().simple((SESSION_ENDPOINT_URI))
            .filter(not(isValidSession))
                .to(invalidSessionResponseEndpoint)
                .stop()
            .end()
            .setProperty("accessTime", xpath("/o/accessTime/text()").numberResult())
            .process(new CheckSessionExpiredProcessor())
            .choice()
                .when(isSessionExpired)
                .to(invalidSessionResponseEndpoint)
                .stop()
            .end();
    }

    /**
     *  Configure the remainingTime endpoint.
     */
    private void validateSessionRoute() {
        from(validateSessionEndpoint)
            .to(checkSessionEndpoint)
            .recipientList().simple((SESSION_ENDPOINT_URI))
            .setHeader("accessTime", xpath("/o/accessTime/text()").numberResult())
            .to(validateSessionResponseEndpoint);
    }

    /**
     * Configure the access session endpoint.
     */
    private void accessSessionRoute() {
        from(accessSessionEndpoint)
            .to(checkSessionEndpoint)
            .to(updateAccessTimeEndpoint)
            .to(accessSessionResponseEndpoint);
    }

    /**
     * Create the response for validate session endpoint.
     */
    private void createValidateSessionResponseRoute() {
        from(validateSessionResponseEndpoint)
            .process(new ValidateSessionResponseProcessor());
    }

    /**
     * Create the response for access session endpoint.
     */
    private void createAccessSessionResponseRoute() {
        from(accessSessionResponseEndpoint)
                .process(new AccessSessionResponseProcessor());

    }

    /**
     * Create error message in case of invalid session.
     */
    private void createInvalidSessionResponseRoute() {
        from(invalidSessionResponseEndpoint)
                .process(new InvalidSessionResponseProcessor());
    }



}
