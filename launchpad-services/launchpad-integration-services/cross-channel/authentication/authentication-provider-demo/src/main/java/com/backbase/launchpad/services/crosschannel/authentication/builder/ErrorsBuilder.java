package com.backbase.launchpad.services.crosschannel.authentication.builder;

import static com.google.common.base.Preconditions.checkNotNull;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class ErrorsBuilder {
    private JSONArray errors = new JSONArray();

    private ErrorsBuilder() {}

    public static ErrorsBuilder newErrors() {
        return new ErrorsBuilder();
    }

    public ErrorsBuilder addError(String code, String message) {
        checkNotNull(code, "code is not provided");
        checkNotNull(message, "message is not provided");

        JSONObject error = new JSONObject();
        error.put("code", code);
        error.put("message", message);

        errors.add(error);

        return this;
    }

    public String build() {
        JSONObject json = new JSONObject();
        json.put("errors", errors);

        return json.toString();
    }
}