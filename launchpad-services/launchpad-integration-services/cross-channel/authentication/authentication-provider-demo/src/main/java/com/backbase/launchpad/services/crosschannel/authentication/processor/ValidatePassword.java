package com.backbase.launchpad.services.crosschannel.authentication.processor;

import static com.google.common.base.Preconditions.checkNotNull;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;

/**
 * Created with IntelliJ IDEA.
 * User: veniamin
 * Date: 11/18/13
 * Time: 5:06 PM
 * To change this template use File | Settings | File Templates.
 */
public class ValidatePassword implements Processor {
    private PasswordEncoder passwordEncoder = new ShaPasswordEncoder(256);

    public void process(Exchange exchange) throws Exception {
        String username = exchange.getIn().getHeader("username", String.class);
        String profilePassword = exchange.getIn().getHeader("profilePassword", String.class);
        String password = exchange.getIn().getHeader("password", String.class);

        checkNotNull("profilePassword", profilePassword);
        checkNotNull("password", password);

        String encodedPassword = passwordEncoder.encodePassword(password, username);
        exchange.setProperty("passwordAuthenticationPassed", profilePassword.equals(encodedPassword));
    }
}
