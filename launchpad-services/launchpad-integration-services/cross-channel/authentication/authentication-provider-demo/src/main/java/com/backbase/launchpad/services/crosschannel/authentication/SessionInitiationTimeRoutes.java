package com.backbase.launchpad.services.crosschannel.authentication;

import org.apache.camel.Endpoint;
import org.apache.camel.EndpointInject;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;

/**
 * Camel routes related for setting and querying the session initiation (lastLoggedIn) time.
 */
public class SessionInitiationTimeRoutes extends AbstractRoutes {

    private static final String INITIATION_TIME = "initiationTime";

    @EndpointInject(uri = "seda:cross-channel/authentication/session/queryInitiation")
    private Endpoint queryInitiationTimeEndpoint;

    @EndpointInject(uri = "velocity:retrieveInitiationTimeXQuery.vm")
    private Endpoint retrieveInitiationTimeQueryEndpoint;

    @EndpointInject(uri = "velocity:setInitiationTimeXQuery.vm")
    private Endpoint setInitiationTimeQueryEndpoint;

    @Override
    public void configure() {
        retrieveInitiationTimeRoute();
        setInitiationTimeRoute();
    }

    private void retrieveInitiationTimeRoute() {
        from(queryInitiationTimeEndpoint)
                .setHeader(XMLDB_SERVICE_TYPE, constant("XQUERY"))
                .to(retrieveInitiationTimeQueryEndpoint)
                .to(sessionEntityEndpoint)
                .choice()
                    .when(simple("${body} != null"))
                    .transform().xpath("/initiationTime/text()", String.class)
                .end();
    }

    /**
     * Set the initiation time of the session.
     */
    private void setInitiationTimeRoute() {
        from(setInitiationTimeEndpoint)
                .setHeader(XMLDB_SERVICE_TYPE, constant("XQUERY"))
                .process(new Processor() {
                    @Override
                    public void process(Exchange exchange) throws Exception {
                        exchange.getIn().setHeader(INITIATION_TIME, System.currentTimeMillis());
                    }
                })
                .to(setInitiationTimeQueryEndpoint)
                .to(sessionEntityEndpoint);
    }
}
