package com.backbase.launchpad.services.crosschannel.authentication.timeout;

import static com.backbase.launchpad.services.crosschannel.authentication.builder.ErrorsBuilder.newErrors;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

/**
 * Camel {@link Processor} to provide the response in case of invalid session.
 */
public class InvalidSessionResponseProcessor implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {
        String json = newErrors()
                .addError("INVALID_SESSION", "The session is invalid.")
                .build();

        exchange.getIn().setHeader(Exchange.HTTP_RESPONSE_CODE, 401);
        exchange.getIn().setBody(json);
    }
}
