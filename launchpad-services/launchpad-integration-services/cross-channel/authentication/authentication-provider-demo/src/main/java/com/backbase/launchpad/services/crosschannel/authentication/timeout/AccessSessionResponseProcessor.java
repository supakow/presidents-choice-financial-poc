package com.backbase.launchpad.services.crosschannel.authentication.timeout;

import static com.backbase.launchpad.services.crosschannel.authentication.timeout.SessionTimeout.MAX_SESSION_TIMEOUT;
import static com.backbase.launchpad.services.crosschannel.authentication.timeout.SessionTimeoutResponse.newSessionTimeoutResponse;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

/**
 * Camel {@link Processor} to provide the response for access session endpoint.
 */
public class AccessSessionResponseProcessor implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {
        exchange.getIn().setBody(newSessionTimeoutResponse()
                .withRemainingTime(MAX_SESSION_TIMEOUT)
                .build());
    }

}
