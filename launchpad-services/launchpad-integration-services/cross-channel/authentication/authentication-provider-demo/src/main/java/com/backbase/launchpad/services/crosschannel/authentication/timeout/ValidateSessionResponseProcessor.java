package com.backbase.launchpad.services.crosschannel.authentication.timeout;

import static com.backbase.launchpad.services.crosschannel.authentication.timeout.SessionTimeout.MAX_SESSION_TIMEOUT;
import static com.backbase.launchpad.services.crosschannel.authentication.timeout.SessionTimeout.SESSION_ACCESS_TIME;
import static com.backbase.launchpad.services.crosschannel.authentication.timeout.SessionTimeoutResponse.newSessionTimeoutResponse;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;

/**
 * Camel {@Processor} to provide the response for the validate session endpoint.
 */
public class ValidateSessionResponseProcessor implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {
        Message in = exchange.getIn();
        Integer accessTime = in.getHeader(SESSION_ACCESS_TIME, Integer.class);
        int difference = (int) (getCurrentTimeInSeconds() - accessTime);
        int remainingTime = MAX_SESSION_TIMEOUT - difference;
        in.setBody(newSessionTimeoutResponse()
                .withRemainingTime(remainingTime)
                .build());
    }

    private long getCurrentTimeInSeconds() {
        return System.currentTimeMillis() / 1000;
    }
}
