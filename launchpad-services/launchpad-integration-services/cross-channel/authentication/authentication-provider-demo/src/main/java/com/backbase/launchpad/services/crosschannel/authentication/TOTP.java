package com.backbase.launchpad.services.crosschannel.authentication;

import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base32;

/**
 * Time-based One-Time Password utilities
 */
public class TOTP {

    public static int getCode(byte[] secret, long timeIndex) {

        SecretKeySpec signKey = new SecretKeySpec(secret, "HmacSHA1");
        ByteBuffer buffer = ByteBuffer.allocate(8);
        buffer.putLong(timeIndex);

        byte[] timeBytes = buffer.array();

        Mac mac;
        try {
            mac = Mac.getInstance("HmacSHA1");
            mac.init(signKey);
        } catch (GeneralSecurityException ex) {
            throw new RuntimeException(ex);
        }

        byte[] hash = mac.doFinal(timeBytes);
        int offset = hash[19] & 0xf;
        int truncatedHash = hash[offset] & 0x7f;

        for (int i = 1; i < 4; i++) {
            truncatedHash <<= 8;
            truncatedHash |= hash[offset + i] & 0xff;
        }

        return (truncatedHash %= 1000000);
    }

    public static boolean verifyCode(String secret, int code, long timeIndex, int variance) {
        byte[] secretBytes = new Base32().decode(secret);
        for (int i = -variance; i <= variance; i++) {
            if (getCode(secretBytes, timeIndex + i) == code) {
                return true;
            }
        }

        return false;
    }
}