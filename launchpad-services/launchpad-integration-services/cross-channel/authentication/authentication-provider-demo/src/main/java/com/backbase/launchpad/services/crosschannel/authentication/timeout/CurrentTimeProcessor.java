package com.backbase.launchpad.services.crosschannel.authentication.timeout;

import static com.backbase.launchpad.services.crosschannel.authentication.timeout.SessionTimeout.SESSION_ACCESS_TIME;

import java.util.concurrent.TimeUnit;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

/**
 * Camel {@link Processor} responsible for providing the current time of the request for
 * remainingTime and accessTime endpoints.
 */
public class CurrentTimeProcessor implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {
        exchange.getIn().setHeader(SESSION_ACCESS_TIME, getCurrentTimeInSeconds());
    }

    private long getCurrentTimeInSeconds() {
        return TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
    }
}
