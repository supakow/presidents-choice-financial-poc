package com.backbase.launchpad.services.crosschannel.authentication;

import org.apache.camel.Endpoint;
import org.apache.camel.EndpointInject;

/**
 * Camel routes related for updating the session status.
 */
public class SessionStatusRoutes extends AbstractRoutes {

    @EndpointInject(uri = "velocity:updateSessionStatusQuery.vm")
    private Endpoint updateSessionEntityQueryEndpoint;

    @Override
    public void configure() {
        from(updateSessionStatusEndpoint)
                .setHeader(XMLDB_SERVICE_TYPE, constant("XQUERY"))
                .to(updateSessionEntityQueryEndpoint)
                .to(sessionEntityEndpoint);
    }

}
