package com.backbase.launchpad.services.crosschannel.authentication;

import static com.backbase.launchpad.services.crosschannel.authentication.builder.ErrorsBuilder.newErrors;
import static com.backbase.launchpad.services.crosschannel.authentication.builder.SessionBuilder.newSession;
import static org.apache.camel.builder.PredicateBuilder.and;
import static org.apache.camel.builder.PredicateBuilder.isNotNull;
import static org.apache.camel.builder.PredicateBuilder.not;

import org.apache.camel.Endpoint;
import org.apache.camel.EndpointInject;
import org.apache.camel.Exchange;
import org.apache.camel.Predicate;
import org.apache.camel.Processor;
import org.apache.camel.impl.JavaUuidGenerator;
import org.apache.camel.spi.UuidGenerator;

import com.backbase.launchpad.services.crosschannel.authentication.processor.ConstructNextStepResponse;
import com.backbase.launchpad.services.crosschannel.authentication.processor.ValidatePassword;
import com.backbase.launchpad.services.crosschannel.authentication.processor.VerificationRequiredDecision;
import com.backbase.mashup.camel.json.XmlJsonDataFormatWrapper;

/**
 * Defines Camel routes specific to authentication workflow.
 */
public class AuthenticationRoutes extends AbstractRoutes {
    public static final String VERIFIED_SESSION_STATUS = "Verified";
    public static final String INITIATED_SESSION_STATUS = "Initiated";
    public static final String CONFIRMED_SESSION_STATUS = "Confirmed";
    private static final String USER_PROFILE_ENDPOINT_URI = "xmldb:exist:///db/launchpad/customers?XPath=/customer[username/text() = '${header.username}']";
    private static final String RETRIEVE_SESSION_ENDPOINT_URI = "xmldb:exist:///db/launchpad/sessions?XPath=//o[id = '${header.id}']";
    private static final String RETRIEVE_USER_ENDPOINT_URI = "xmldb:exist:///db/launchpad/customers?XPath=//customer[username/text() = '${header.username}']";
    private static final int TWO_MINUTES_WINDOW = 2;

    @EndpointInject(uri = "seda:provider/cross-channel/authentication/session/initiate")
    private Endpoint initiateSessionEndpoint;

    @EndpointInject(uri = "velocity:createSessionXML.vm")
    private Endpoint createSessionEntityEndpoint;

    @EndpointInject(uri = "seda:provider/cross-channel/authentication/session/verifyOTP")
    private Endpoint verifyOTPEndpoint;

    @EndpointInject(uri = "seda:cross-channel/authentication/session/confirm")
    private Endpoint confirmSessionEndpoint;

    @EndpointInject(uri = "seda:cross-channel/authentication/session/verify")
    private Endpoint verifySessionEndpoint;

    @EndpointInject(uri = "direct:authentifactionFailed")
    private Endpoint authentifactionFailedEndpoint;

    @EndpointInject(uri = "direct:authenticationSucceededRoute")
    private Endpoint authenticationSucceededEndpoint;

    private XmlJsonDataFormatWrapper xmlJsonDataFormat;

    private UuidGenerator uuidGenerator = new JavaUuidGenerator();

    @Override
    public void configure() throws Exception {
        initiateSessionRoute();
        verityOTPRoute();
        confirmSessionRoute();
        verifySessionRoute();
        authenticationSucceededRoute();
        authenticationFailedRoute();
    }

    private void initiateSessionRoute() {
        /**
         * If the username and password is wrong we are still generating a sessionid and returning a successful response,
         * without storing an actual session.
         *
         * The actual authentication failure error response will be provided in the verification step in that case.
         */
        Predicate isPasswordAuthenticationPassed = simple("${property.passwordAuthenticationPassed} == true");
        Predicate isAuthenticationFailed = simple("${property.authenticationFailed} == true");
        Predicate isSessionVerified = simple("${property.sessionIsVerified} == true");

        from(initiateSessionEndpoint)
            .setHeader("id", method(uuidGenerator, "generateUuid"))
            .recipientList().simple(USER_PROFILE_ENDPOINT_URI)
            .choice()
                .when(simple("${body} == null"))
                .to(authentifactionFailedEndpoint)
                .otherwise()
                    .setHeader("profilePassword", xpath("/customer/password/text()").stringResult())
                    .setHeader("secondFactor", xpath("/customer/secondFactor/text()").stringResult())
                    .process(new ValidatePassword())
                        .choice()
                            .when(isPasswordAuthenticationPassed)
                            .to(createSessionEntityEndpoint)
                            .to(sessionEntityEndpoint)
                    .end()
                    .process(new VerificationRequiredDecision())
                    .choice()
                        .when(isAuthenticationFailed).to(authentifactionFailedEndpoint)
                        .when(isSessionVerified).to(authenticationSucceededEndpoint)
                        .otherwise().process(new ConstructNextStepResponse());
    }

    private void verityOTPRoute() {
        Predicate isVerificationFailed = not(simple("${body} == true"));
        Predicate isSessionInitiated = not(simple("${body} == null"));

        from(verifyOTPEndpoint)
            .recipientList().simple(RETRIEVE_SESSION_ENDPOINT_URI)
            .filter(not(isSessionInitiated))
                .to(authentifactionFailedEndpoint)
                .stop()
            .end()
            .setHeader("username", xpath("//o/username/text()").stringResult())
            .recipientList().simple(RETRIEVE_USER_ENDPOINT_URI)
            .transform().xpath("//customer/secretKey/text()", String.class)
            .process(new Processor() {
                public void process(Exchange exchange) throws Exception {
                    String sharedSecret = exchange.getIn().getBody(String.class);
                    Integer otpCode = exchange.getIn().getHeader("otp_code", Integer.class);

                    long counter = System.currentTimeMillis() / 1000 / 30;
                    exchange.getIn().setBody(TOTP.verifyCode(sharedSecret, otpCode, counter, TWO_MINUTES_WINDOW));
                }

            })
            .choice()
                .when(isVerificationFailed).to(authentifactionFailedEndpoint)
                .otherwise().to(authenticationSucceededEndpoint);
    }

    /**
     * Activated internally by the authentication provider during authentication, to verify that the session is
     * pre-authenticated and ready to be confirmed.
     */
    private void verifySessionRoute() {
        Predicate isSessionVerified = and(isNotNull(simple("${body}")), xpath("/o/status = 'Verified'").booleanResult());
        from(verifySessionEndpoint)
            .recipientList().simple(RETRIEVE_SESSION_ENDPOINT_URI)
            .filter(not(isSessionVerified))
                .throwException(
                        new IllegalStateException("The session is not found or it is not ready to be confirmed."))
            .end()
            .setHeader("username", xpath("//o/username/text()").stringResult())
            .recipientList().simple(RETRIEVE_USER_ENDPOINT_URI)
            .convertBodyTo(String.class)
            .marshal(xmlJsonDataFormat)
            .convertBodyTo(String.class);
    }

    /**
     * Activated internally by the authentication provider on a successful login.
     * Invoked after the verification step to complete the authentication workflow and confirm the pre-authenticated
     * session.
     */
    private void confirmSessionRoute() {
        from(confirmSessionEndpoint)
                .setHeader("sessionStatus", constant(CONFIRMED_SESSION_STATUS))
                .to(updateSessionStatusEndpoint)
                .to(mapHttpSessionEndpoint)
                .to(updateAccessTimeEndpoint)
                .to(setInitiationTimeEndpoint);
    }

    private void authenticationFailedRoute() {
        from(authentifactionFailedEndpoint)
            .process(new Processor() {
                public void process(Exchange exchange) throws Exception {
                    String json = newErrors()
                            .addError("CANNOT_AUTHENTICATE", "The authentication is failed.")
                            .build();

                    exchange.getIn().setHeader(Exchange.HTTP_RESPONSE_CODE, 401);
                    exchange.getIn().setBody(json);
                }
            });
    }

    private void authenticationSucceededRoute() {
        from(authenticationSucceededEndpoint)
            .setHeader("sessionStatus", constant(VERIFIED_SESSION_STATUS))
            .to(updateSessionStatusEndpoint)
            .process(new Processor() {
                public void process(Exchange exchange) throws Exception {
                    String json = newSession()
                            .withIdentity(exchange.getIn().getHeader("id", String.class))
                            .withStatus(VERIFIED_SESSION_STATUS)
                            .build();

                    exchange.getIn().setBody(json);
                }
            });
    }

    /**
     * Inner Builder Classes
     */
    public void setXmlJsonDataFormat(XmlJsonDataFormatWrapper xmlJsonDataFormat) {
        this.xmlJsonDataFormat = xmlJsonDataFormat;
    }
}
