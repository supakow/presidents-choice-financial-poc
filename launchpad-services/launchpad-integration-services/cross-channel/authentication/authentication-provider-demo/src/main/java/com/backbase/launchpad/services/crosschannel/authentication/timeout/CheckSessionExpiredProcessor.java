package com.backbase.launchpad.services.crosschannel.authentication.timeout;

import static com.backbase.launchpad.services.crosschannel.authentication.timeout.SessionTimeout.MAX_SESSION_TIMEOUT;
import static com.backbase.launchpad.services.crosschannel.authentication.timeout.SessionTimeout.SESSION_ACCESS_TIME;
import static com.backbase.launchpad.services.crosschannel.authentication.timeout.SessionTimeout.SESSION_EXPIRED;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

/**
 * Camel {@link Processor} for checking if the core banking session expired or not.
 */
public class CheckSessionExpiredProcessor implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {
        long accessTime = exchange.getProperty(SESSION_ACCESS_TIME, Long.class);
        long difference = getCurrentTimeInSeconds() - accessTime;

        // core banking session has expired
        if (difference > MAX_SESSION_TIMEOUT) {
            exchange.setProperty(SESSION_EXPIRED, true);
        } else {
            exchange.setProperty(SESSION_EXPIRED, false);
        }
    }

    private long getCurrentTimeInSeconds() {
        return System.currentTimeMillis() / 1000;
    }
}
