CREATE TABLE accounts (
  id CHAR(40) PRIMARY KEY,
  name VARCHAR2(50) NOT NULL,
  bban VARCHAR2(50) NOT NULL,
  iban VARCHAR2(50) NOT NULL,
  balance DECIMAL NOT NULL,
  availableBalance DECIMAL NOT NULL,
  currency CHAR(3) NOT NULL,
  type VARCHAR(30) NOT NULL,
  status VARCHAR(30) NOT NULL,
  partyId VARCHAR(30) NOT NULL
);

CREATE TABLE transactions (
  id CHAR(40) PRIMARY KEY,
  accountId CHAR(40) NOT NULL,
  categoryId CHAR(40) NOT NULL,
  bookingDateTime TIMESTAMP NOT NULL,
  counterpartyAccount VARCHAR2(50),
  counterpartyName VARCHAR2(50),
  counterPartyLogoPath VARCHAR2(200),
  creditDebitIndicator CHAR(6) NOT NULL,
  instructedAmount DECIMAL,
  instructedCurrency CHAR(3),
  transactionAmount DECIMAL,
  transactionCurrency CHAR(3),
  transactionType VARCHAR2(50),
  transactionDescription VARCHAR2(150),
-- transaction details part
  city VARCHAR2(50),
  country VARCHAR2(50),
  state VARCHAR2(50),
  street VARCHAR2(150),
  latitude DECIMAL,
  longitude DECIMAL,
  merchantType VARCHAR2(50),
  remittanceInformation VARCHAR2(50)
);

ALTER TABLE transactions ADD FOREIGN KEY(accountId) REFERENCES accounts(id);

CREATE TABLE paymentOrders (
  id CHAR(40) PRIMARY KEY,
  accountId CHAR(40) NOT NULL,
  accountName VARCHAR2(50) NOT NULL,
  counterpartyAccount VARCHAR2(50),
  counterpartyEmail VARCHAR2(50),
  counterpartyIban VARCHAR2(50),
  counterpartyName VARCHAR2(50),
  creditorReference VARCHAR2(50),
  initiationDate TIMESTAMP NOT NULL,
  executionDate TIMESTAMP,
  amount DECIMAL NOT NULL,
  currency CHAR(3) NOT NULL,
  paymentCategory VARCHAR2(50),
  remittanceInformation VARCHAR2(50),
  frequency VARCHAR2(50),
  every INT,
  startDate TIMESTAMP,
  endDate TIMESTAMP,
  intervals VARCHAR2(50),
  status VARCHAR2(50) NOT NULL,
  type VARCHAR2(50) NOT NULL
);

ALTER TABLE paymentOrders ADD FOREIGN KEY(accountId) REFERENCES accounts(id);

