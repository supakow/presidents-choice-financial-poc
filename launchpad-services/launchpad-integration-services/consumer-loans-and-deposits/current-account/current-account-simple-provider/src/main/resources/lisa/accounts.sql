INSERT INTO accounts VALUES (
  '4efc90d1-dc99-4922-b070-6104c19c6731',
  'Personal Checking Account',
  '280680457',
  'NL66INGB0280680457',
  9207.99,
  9103.50,
  'EUR',
  'CASH',
  'ENABLED',
  '3'
);

INSERT INTO accounts VALUES (
  '49978d2b-f039-493b-a57c-ff4dc12d0dcd',
  'Business Account',
  '842497587',
  'NL67RABO0842497587',
  131517,
  131517,
  'EUR',
  'CREDIT',
  'ENABLED',
  '3'
);

INSERT INTO accounts VALUES (
  'c3742491-8fbf-495e-918f-eb063013428f',
  'Shared Account',
  '519431642',
  'NL56ABNA0519431642',
  60705,
  60400,
  'EUR',
  'INVESTMENT',
  'ENABLED',
  '3'
);


