package com.backbase.launchpad.services.integration;

import com.backbase.launchpad.services.model.Account;
import com.backbase.launchpad.services.repository.AccountRepository;
import com.backbase.launchpad.services.utils.ExchangeUtils;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Camel {@link Processor} supporting the accounts route.
 * It leverages the {@link AccountRepository} to retrieve the accounts.
 */
@Service
public class AccountProcessor extends BaseProcessor {
    private final static Logger LOGGER = LoggerFactory.getLogger(AccountProcessor.class);

    @Autowired
    private AccountRepository accountRepository;

    @Override
    public void process(Exchange exchange) throws Exception {
        LOGGER.info("Account processor called");

        final String partyId = ExchangeUtils.getQueryParameter(exchange, "partyId", "");
        final List<Account> accounts = accountRepository.findAll(partyId);
        if (accounts.isEmpty()) {
            handleError(exchange, "404", "Account(s) for partyId=" + partyId + " not found");
        } else {
            createResponse(exchange, accounts);
        }
    }

}