package com.backbase.launchpad.services.repository.impl;

import com.backbase.launchpad.services.model.Currency;
import com.backbase.launchpad.services.model.Transaction;
import com.backbase.launchpad.services.model.TransactionIndicator;
import com.backbase.launchpad.services.repository.TransactionRepository;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

@Repository
public class H2TransactionRepository implements TransactionRepository {
    private final static Logger LOGGER = LoggerFactory.getLogger(H2TransactionRepository.class);

    private static final String TRANSACTIONS_OF_A_CUSTOMER = "select * from transactions where accountId = ?";
    private static final String TRANSACTIONS_FOR_A_GIVEN_PERIOD = TRANSACTIONS_OF_A_CUSTOMER +
            " and bookingDateTime >= ? and bookingDateTime <= ? order by bookingDateTime desc";
    private static final String TRANSACTION_DETAILS =
            "select id, city, country, state, street, latitude, longitude, merchantType, remittanceInformation from transactions where id = ?";


    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<Transaction> findByAccountId(String accountId, String order, int offset, int limit) {
        final String orderDirection = "-".equals(order.charAt(0)) ? "desc" : "asc";
        final String orderColumn = order.substring(1);

        final String sql = TRANSACTIONS_OF_A_CUSTOMER+ " order by " + orderColumn + " " + orderDirection
                + " limit ? offset ?";
        return jdbcTemplate.query(sql, new Object[]{accountId, limit, offset}, new TransactionMapper());
    }

    @Override
    public List<Transaction> findByAccountId(String accountId, long start, long end) {
        return jdbcTemplate.query(TRANSACTIONS_FOR_A_GIVEN_PERIOD, new Object[]{accountId, new Timestamp(start), new Timestamp(end)}, new TransactionMapper());
    }

    @Override
    public Transaction getDetails(String transactionId) {
        Transaction transaction = null;

        try {
            transaction = jdbcTemplate.queryForObject(TRANSACTION_DETAILS, new Object[]{transactionId}, new RowMapper<Transaction>() {

                @Override
                public Transaction mapRow(ResultSet rs, int rowNb) throws SQLException {
                    final Transaction transaction = new Transaction();

                    final String id = rs.getString("id");
                    transaction.setId(id);
                    transaction.addToAddress("city", rs.getString("city"));
                    transaction.addToAddress("country", rs.getString("country"));
                    transaction.addToAddress("state", rs.getString("state"));
                    transaction.addToAddress("street", rs.getString("street"));
                    transaction.addToLocation("latitude", rs.getString("latitude"));
                    transaction.addToLocation("longitude", rs.getString("longitude"));
                    transaction.setMerchantType(rs.getString("merchantType"));
                    transaction.setRemittanceInformation(rs.getString("remittanceInformation"));

                    return transaction;
                }

            });
        } catch (DataAccessException e) {
            LOGGER.warn("Transaction with ID: " + transactionId + " was not found");
        }

        return transaction;
    }

    private class TransactionMapper implements RowMapper<Transaction> {

        @Override
        public Transaction mapRow(ResultSet rs, int rowNb) throws SQLException {
            final Transaction transaction = new Transaction();

            transaction.setAccountId(rs.getString("accountId"));
            final Timestamp date = rs.getTimestamp("bookingDateTime");
            transaction.setBookingDateTime(new DateTime(date));
            transaction.setCategoryId(rs.getString("categoryId"));
            transaction.setCounterpartyAccount(rs.getString("counterpartyAccount"));
            transaction.setCounterPartyLogoPath(rs.getString("counterPartyLogoPath"));
            transaction.setCounterpartyName(rs.getString("counterpartyName"));
            final String id = rs.getString("id");
            transaction.setId(id);
            transaction.setIndicator(TransactionIndicator.valueOf(rs.getString("creditDebitIndicator")));
            transaction.setInstructedAmount(rs.getBigDecimal("instructedAmount"));
            final String instructedCurrency = rs.getString("instructedCurrency");
            if (instructedCurrency != null) {
                transaction.setInstructedCurrency(Currency.valueOf(instructedCurrency));
            }
            transaction.setTransactionAmount(rs.getBigDecimal("transactionAmount"));
            final String transactionCurrency = rs.getString("transactionCurrency");
            if (transactionCurrency != null) {
                transaction.setTransactionCurrency(Currency.valueOf(transactionCurrency));
            }
            transaction.setTransactionType(rs.getString("transactionType"));
            transaction.setTransactionDescription(rs.getString("transactionDescription"));

            return transaction;
        }
    }

}
