package com.backbase.launchpad.services.model;

import org.codehaus.jackson.annotate.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Models the turnover of an account at the end of a day.
 */
@JsonPropertyOrder({"date", "deposit", "withdrawal"})
public class AccountTurnover implements Serializable {
    private static final long serialVersionUID = 979289342401721135L;

    private long date;
    private BigDecimal deposit;
    private BigDecimal withdrawal;

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public BigDecimal getDeposit() {
        return deposit;
    }

    public void setDeposit(BigDecimal deposit) {
        this.deposit = deposit;
    }

    public BigDecimal getWithdrawal() {
        return withdrawal;
    }

    public void setWithdrawal(BigDecimal withdrawal) {
        this.withdrawal = withdrawal;
    }

}