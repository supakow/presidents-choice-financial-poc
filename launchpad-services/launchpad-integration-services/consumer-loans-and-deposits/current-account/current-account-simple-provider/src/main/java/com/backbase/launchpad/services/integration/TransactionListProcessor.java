package com.backbase.launchpad.services.integration;

import com.backbase.launchpad.services.model.Transaction;
import com.backbase.launchpad.services.repository.TransactionRepository;
import com.backbase.launchpad.services.utils.ExchangeUtils;
import org.apache.camel.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TransactionListProcessor extends BaseProcessor {
    private final static Logger LOGGER = LoggerFactory.getLogger(TransactionListProcessor.class);

    private static final int DEFAULT_OFFSET = 0;
    private static final int DEFAULT_LIMIT = 20;
    private static final String DEFAULT_SORT = "-bookingDateTime";

    @Autowired
    private TransactionRepository transactionRepository;

    @Override
    public void process(Exchange exchange) throws Exception {
        LOGGER.info("Transaction processor called");

        int offset = ExchangeUtils.getQueryParameterAsInt(exchange, "f", DEFAULT_OFFSET) - 1;
        if (offset < DEFAULT_OFFSET) {
            offset = DEFAULT_OFFSET;
        }
        int limit = ExchangeUtils.getQueryParameterAsInt(exchange, "l", DEFAULT_LIMIT);
        if (limit < 1) {
            limit = 1;
        }
        final String sort = ExchangeUtils.getQueryParameter(exchange, "sort", DEFAULT_SORT);

        final String accountId = ExchangeUtils.getHeaderValue(exchange, "accountId");
        final List<Transaction> transactions = transactionRepository.findByAccountId(accountId, sort, offset, limit);
        if (transactions.isEmpty()) {
            handleError(exchange, "404", "Transactions for accountId=" + accountId + " not found");
        } else {
            createResponse(exchange, transactions);
        }
    }

}