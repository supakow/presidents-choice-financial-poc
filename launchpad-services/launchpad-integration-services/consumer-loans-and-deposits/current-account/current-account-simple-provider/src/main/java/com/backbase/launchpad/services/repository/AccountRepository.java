package com.backbase.launchpad.services.repository;

import com.backbase.launchpad.services.model.Account;

import java.util.List;

/**
 * Repository for {@link Account} entity.
 */
public interface AccountRepository {

    List<Account> findAll(String partyId);

    Account findById(String accountId);

}