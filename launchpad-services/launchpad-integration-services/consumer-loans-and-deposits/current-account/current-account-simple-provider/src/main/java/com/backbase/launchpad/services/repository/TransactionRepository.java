package com.backbase.launchpad.services.repository;

import com.backbase.launchpad.services.model.Transaction;

import java.util.List;

public interface TransactionRepository {

    List<Transaction> findByAccountId(String accountId, String order, int offset, int limit);

    List<Transaction> findByAccountId(String accountId, long start, long end);

    Transaction getDetails(String transactionId);

}
