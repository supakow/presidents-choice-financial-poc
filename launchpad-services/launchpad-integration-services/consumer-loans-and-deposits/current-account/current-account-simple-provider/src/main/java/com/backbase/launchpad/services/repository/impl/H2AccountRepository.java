package com.backbase.launchpad.services.repository.impl;

import com.backbase.launchpad.services.model.Account;
import com.backbase.launchpad.services.model.AccountStatus;
import com.backbase.launchpad.services.model.AccountType;
import com.backbase.launchpad.services.model.Currency;
import com.backbase.launchpad.services.repository.AccountRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Implementation of {@link com.backbase.launchpad.services.repository.AccountRepository} using H2 database.
 */
@Repository
public class H2AccountRepository implements AccountRepository {
    private final static Logger LOGGER = LoggerFactory.getLogger(H2AccountRepository.class);

    private static String ALL_ACCOUNTS_OF_A_CUSTOMER = "select * from accounts where partyId = ?";
    private static String ONE_ACCOUNT = "select * from accounts where id = ?";

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<Account> findAll(String partyId) {
        return jdbcTemplate.query(ALL_ACCOUNTS_OF_A_CUSTOMER, new Object[] {partyId}, new AccountRowMapper());
    }

    @Override
    public Account findById(String accountId) {
        Account account = null;
        try {
            account = jdbcTemplate.queryForObject(ONE_ACCOUNT, new Object[] {accountId}, new AccountRowMapper());
        } catch (DataAccessException e) {
            LOGGER.warn("Account with id \"" + accountId + "\" was not found");
        }

        return account;
    }

    /**
     * Row mapper for Account.
     */
    private class AccountRowMapper implements RowMapper<Account> {
        @Override
        public Account mapRow(ResultSet rs, int rowNum) throws SQLException {
            final Account account = new Account();

            account.setId(rs.getString("id"));
            account.setName(rs.getString("name"));
            account.setBban(rs.getString("bban"));
            account.setIban(rs.getString("iban"));
            account.setBalance(rs.getBigDecimal("balance"));
            account.setAvailableBalance(rs.getBigDecimal("availableBalance"));
            account.setCurrency(Currency.valueOf(rs.getString("currency")));
            account.setType(AccountType.valueOf(rs.getString("type")));
            account.setStatus(AccountStatus.valueOf(rs.getString("status")));
            account.setPartyId(rs.getString("partyId"));

            return account;
        }
    }

}