package com.backbase.launchpad.services.model;

/**
 * Supported currencies
 */
public enum Currency {
    EUR, USD
}

