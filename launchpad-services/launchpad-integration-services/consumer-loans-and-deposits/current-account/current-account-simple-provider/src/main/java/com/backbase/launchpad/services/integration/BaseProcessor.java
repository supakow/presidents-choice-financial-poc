package com.backbase.launchpad.services.integration;

import com.backbase.launchpad.services.model.ErrorMessage;
import com.backbase.launchpad.services.model.Transaction;
import com.backbase.launchpad.services.utils.JsonUtils;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.joda.time.DateTime;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Base class for CAMEL processors.
 */
public abstract class BaseProcessor implements Processor {

    /**
     * Creates an error JSON response. Sets the Content-Type header to "application/json".
     *
     * @param exchange {@link org.apache.camel.Exchange}
     * @param errorCode the HTTP status code
     * @param message the error message
     * @throws java.io.IOException
     */
    protected void handleError(Exchange exchange, String errorCode, String message) throws IOException {
        exchange.getOut().setHeader(Exchange.HTTP_RESPONSE_CODE, errorCode);
        exchange.getOut().setHeader(Exchange.CONTENT_TYPE, "application/json");
        exchange.getOut().setBody(JsonUtils.convertToJson(new ErrorMessage(errorCode, message)));
    }

    /**
     * Creates the JSON response. Sets the Content-type header to "application/json".
     *
     * @param exchange a {@link Exchange} instance
     * @param response the response to be serialized into json
     * @throws IOException
     */
    protected void createResponse(Exchange exchange, Object response) throws IOException {
        exchange.getOut().setHeader(Exchange.CONTENT_TYPE, "application/json");
        exchange.getOut().setBody(JsonUtils.convertToJson(response));
    }

    /**
     * Retrieves transactions for a given day.
     *
     * @param day a {@link org.joda.time.DateTime} instance identifying a day
     * @param transactions a list of transactions
     * @return a list of transactions on a day
     */
    protected List<Transaction> getTransactionsForDay(DateTime day, List<Transaction> transactions) {
        final DateTime endDay = day.withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59);
        final List<Transaction> result = new ArrayList<Transaction>();
        for (final Transaction transaction : transactions) {
            if (transaction.getBookingDateTime().isAfter(day) && transaction.getBookingDateTime().isBefore(endDay)) {
                result.add(transaction);
            }
        }

        return result;
    }

}
