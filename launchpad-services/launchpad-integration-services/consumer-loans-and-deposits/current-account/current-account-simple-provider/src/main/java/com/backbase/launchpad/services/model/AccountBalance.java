package com.backbase.launchpad.services.model;

import org.codehaus.jackson.annotate.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Models the balance of an account at the end of a day.
 */
@JsonPropertyOrder({"date", "balance"})
public class AccountBalance implements Serializable {
    private static final long serialVersionUID = -313932543255404058L;

    private long date;
    private BigDecimal balance;

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

}