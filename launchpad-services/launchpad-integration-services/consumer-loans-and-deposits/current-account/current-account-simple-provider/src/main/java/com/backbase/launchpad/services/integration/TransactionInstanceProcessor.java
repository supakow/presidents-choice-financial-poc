package com.backbase.launchpad.services.integration;

import com.backbase.launchpad.services.model.Transaction;
import com.backbase.launchpad.services.repository.TransactionRepository;
import com.backbase.launchpad.services.utils.ExchangeUtils;
import org.apache.camel.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TransactionInstanceProcessor extends BaseProcessor {
    private final static Logger LOGGER = LoggerFactory.getLogger(TransactionInstanceProcessor.class);

    @Autowired
    private TransactionRepository transactionRepository;

    @Override
    public void process(Exchange exchange) throws Exception {
        LOGGER.info("TransactionInstance processor called");

        final String transactionId = ExchangeUtils.getHeaderValue(exchange, "transactionId");
        final Transaction transaction = transactionRepository.getDetails(transactionId);
        if (transaction == null) {
            handleError(exchange, "404", "Transaction with transactionId " + "\"" + transactionId + "\"" + " not found");
        } else {
            createResponse(exchange, transaction);
        }
    }

}