package com.backbase.launchpad.services.model;

import org.codehaus.jackson.annotate.JsonPropertyOrder;

import java.io.Serializable;

/**
 * An error message as a response for a REST request.
 */
@JsonPropertyOrder({"code", "message"})
public class ErrorMessage implements Serializable {
    private static final long serialVersionUID = -5159369962981982103L;

    private String code;
    private String message;

    public ErrorMessage() {
        this(null, null);
    }

    public ErrorMessage(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}