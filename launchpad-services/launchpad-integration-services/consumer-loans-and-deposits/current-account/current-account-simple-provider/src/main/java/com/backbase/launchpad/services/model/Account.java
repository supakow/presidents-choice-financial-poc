package com.backbase.launchpad.services.model;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

import java.math.BigDecimal;

/**
 * Models an account at a bank.
 */
@JsonPropertyOrder({"id", "name", "bban", "iban", "balance", "availableBalance", "currency", "groupCode", "type",
        "status", "partyId"})
public class Account extends BaseEntity {
    private static final long serialVersionUID = 557279650263985462L;

    private String name;

    // TODO find proper model for storing account numbers in US and SEPA region.

    // TODO currently we use the "bban" and "iban" fields to store the actual bank account number
    // TODO for US we use the workaround storing the bank account in the "iban" field as the following format:
    // TODO "<routing-number> <account-number>"

    private String bban; // Basic Bank Account Number
    private String iban; // International Bank Account Number

    private BigDecimal balance;
    private BigDecimal availableBalance;
    private Currency currency;

    @JsonProperty("groupCode")
    private AccountType type;
    private AccountStatus status;
    private String partyId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBban() {
        return bban;
    }

    public void setBban(String bban) {
        this.bban = bban;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getAvailableBalance() {
        return availableBalance;
    }

    public void setAvailableBalance(BigDecimal availableBalance) {
        this.availableBalance = availableBalance;
    }

    public AccountType getType() {
        return type;
    }

    public void setType(AccountType type) {
        this.type = type;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public String getPartyId() {
        return partyId;
    }

    public void setPartyId(String partyId) {
        this.partyId = partyId;
    }

    public AccountStatus getStatus() {
        return status;
    }

    public void setStatus(AccountStatus status) {
        this.status = status;
    }
}
