package com.backbase.launchpad.services.model;

import java.io.Serializable;

/**
 * Base entity model with identity.
 */
public class BaseEntity implements Serializable {

    private static final long serialVersionUID = -3706532005880574568L;

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
