package com.backbase.launchpad.services.model;

import org.codehaus.jackson.annotate.JsonProperty;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.Map;
import java.util.TreeMap;

/**
 * A banking transaction, belongs to an account.
 */
public class Transaction extends BaseEntity {
    private static final long serialVersionUID = -8396276018774572673L;

    private String accountId;
    private String categoryId;
    private DateTime bookingDateTime;
    private String counterpartyAccount;
    private String counterpartyName;
    private String counterPartyLogoPath;
    @JsonProperty("creditDebitIndicator")
    private TransactionIndicator indicator;
    private BigDecimal instructedAmount;
    private Currency instructedCurrency;
    private BigDecimal transactionAmount;
    private Currency transactionCurrency;
    private String transactionType;
    @JsonProperty("description")
    private String transactionDescription;

    // transaction details part
    private Map<String, Object> address = new TreeMap<String, Object>();
    private Map<String, Object> location = new TreeMap<String, Object>();
    private String merchantType;
    private String remittanceInformation;

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public DateTime getBookingDateTime() {
        return bookingDateTime;
    }

    public void setBookingDateTime(DateTime bookingDateTime) {
        this.bookingDateTime = bookingDateTime;
    }

    public String getCounterpartyAccount() {
        return counterpartyAccount;
    }

    public void setCounterpartyAccount(String counterpartyAccount) {
        this.counterpartyAccount = counterpartyAccount;
    }

    public String getCounterpartyName() {
        return counterpartyName;
    }

    public void setCounterpartyName(String counterpartyName) {
        this.counterpartyName = counterpartyName;
    }

    public String getCounterPartyLogoPath() {
        return counterPartyLogoPath;
    }

    public void setCounterPartyLogoPath(String counterPartyLogoPath) {
        this.counterPartyLogoPath = counterPartyLogoPath;
    }

    public TransactionIndicator getIndicator() {
        return indicator;
    }

    public void setIndicator(TransactionIndicator indicator) {
        this.indicator = indicator;
    }

    public BigDecimal getInstructedAmount() {
        return instructedAmount;
    }

    public void setInstructedAmount(BigDecimal instructedAmount) {
        this.instructedAmount = instructedAmount;
    }

    public Currency getInstructedCurrency() {
        return instructedCurrency;
    }

    public void setInstructedCurrency(Currency instructedCurrency) {
        this.instructedCurrency = instructedCurrency;
    }

    public BigDecimal getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(BigDecimal transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public Currency getTransactionCurrency() {
        return transactionCurrency;
    }

    public void setTransactionCurrency(Currency transactionCurrency) {
        this.transactionCurrency = transactionCurrency;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getTransactionDescription() {
        return transactionDescription;
    }

    public void setTransactionDescription(String transactionDescription) {
        this.transactionDescription = transactionDescription;
    }

    public Map<String, Object> getAddress() {
        return address;
    }

    public void addToAddress(String key, Object value) {
        this.address.put(key, value);
    }

    public Map<String, Object> getLocation() {
        return location;
    }

    public void addToLocation(String key, Object value) {
        this.location.put(key, value);
    }

    public String getMerchantType() {
        return merchantType;
    }

    public void setMerchantType(String merchantType) {
        this.merchantType = merchantType;
    }

    public String getRemittanceInformation() {
        return remittanceInformation;
    }

    public void setRemittanceInformation(String remittanceInformation) {
        this.remittanceInformation = remittanceInformation;
    }

}

