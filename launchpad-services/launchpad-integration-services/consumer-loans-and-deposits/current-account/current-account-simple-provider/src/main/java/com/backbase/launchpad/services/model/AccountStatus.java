package com.backbase.launchpad.services.model;

/**
 * Models status of a account. An account can be in one of the statuses defined here.
 */
public enum AccountStatus {
    ENABLED, BLOCKED
}

