package com.backbase.launchpad.services.utils;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import java.io.IOException;

/**
 * JSON utility class.
 */
public final class JsonUtils {

    private static ObjectMapper objectMapper = new ObjectMapper();

    // do not allow to create an instance of this class;
    private JsonUtils() {
    }

    /**
     * Converts object to a JSON string.
     *
     * @param object objects to be converted
     * @return JSON string
     * @throws java.io.IOException
     */
    public static String convertToJson(Object object) throws IOException {
        if (object == null) {
            return null;
        }

        return objectMapper.writeValueAsString(object);
    }

    /**
     * Converts JSON string into java POJO.
     * @param json JSON string
     * @param typeReference type reference to specify JAVA class of returned object.
     * @return JSON as JAVA POJO
     * @throws IOException
     */
    public static Object convertToPojo(String json, TypeReference<? extends Object> typeReference) throws IOException {
        return objectMapper.readValue(json, typeReference);
    }

}
