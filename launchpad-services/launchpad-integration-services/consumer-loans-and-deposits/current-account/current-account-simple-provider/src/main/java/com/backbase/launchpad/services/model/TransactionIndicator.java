package com.backbase.launchpad.services.model;

/**
 * Indicates that the transaction is a DEBIT or CREDIT.
 */
public enum TransactionIndicator {
    DBIT, CRDT
}
