package com.backbase.launchpad.services.integration;

import com.backbase.launchpad.services.model.Account;
import com.backbase.launchpad.services.model.AccountBalance;
import com.backbase.launchpad.services.model.Transaction;
import com.backbase.launchpad.services.repository.AccountRepository;
import com.backbase.launchpad.services.repository.TransactionRepository;
import com.backbase.launchpad.services.utils.ExchangeUtils;
import org.apache.camel.Exchange;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

@Service
public class AccountBalanceProcessor extends BaseProcessor {
    private final static Logger LOGGER = LoggerFactory.getLogger(AccountBalanceProcessor.class);

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    @Override
    public void process(Exchange exchange) throws Exception {
        LOGGER.info("AccountBalance processor called");

        final long start = ExchangeUtils.getQueryParameterAsLong(exchange, "start", -1);
        if (start == -1) {
            handleError(exchange, "400", "Mandatory parameter: [start] is missing");
        } else {
            final long end = ExchangeUtils.getQueryParameterAsLong(exchange, "end", System.currentTimeMillis());
            final DateTime startTime = new DateTime(start);
            final DateTime endTime = new DateTime(end);
            if (startTime.isAfter(endTime)) {
                handleError(exchange, "400", "start time cannot be after end time");
            } else {
                final String accountId = ExchangeUtils.getHeaderValue(exchange, "accountId");
                final Account account = accountRepository.findById(accountId);
                if (account == null) {
                    handleError(exchange, "404", "Account for accountId=" + accountId + " not found");
                } else {
                    final List<Transaction> transactions = transactionRepository.findByAccountId(accountId, start, end);
                    final List<AccountBalance> accountBalances =
                            calculateAccountBalances(startTime, endTime, transactions, account.getBalance());

                    createResponse(exchange, accountBalances);
                }
            }
        }
    }

    /**
     * Calculates account balances for the given period.
     */
    private List<AccountBalance> calculateAccountBalances(DateTime startTime, DateTime endTime,
                                                          List<Transaction> transactions, BigDecimal currentBalance) {
        final LinkedList<AccountBalance> balances = new LinkedList<AccountBalance>();

        final Days days = Days.daysBetween(startTime, endTime);
        DateTime currentTime = endTime;
        for (int i = 1; i <= days.getDays(); i++) {
            final AccountBalance dayAccountBalance = new AccountBalance();
            dayAccountBalance.setBalance(currentBalance);
            dayAccountBalance.setDate(currentTime.withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59)
                    .getMillis());

            final List<Transaction> transactionsPerDay =
                    getTransactionsForDay(currentTime.withMillisOfDay(0), transactions);
            for (final Transaction transaction : transactionsPerDay) {
                switch (transaction.getIndicator()) {
                    case CRDT:
                        dayAccountBalance.setBalance(dayAccountBalance.getBalance().subtract(
                                transaction.getTransactionAmount()));
                        break;
                    case DBIT:
                        dayAccountBalance.setBalance(dayAccountBalance.getBalance().add(
                                transaction.getTransactionAmount()));
                        break;
                }
            }

            currentTime = currentTime.minusDays(1);
            currentBalance = dayAccountBalance.getBalance();
            balances.addFirst(dayAccountBalance);
        }

        return balances;
    }

}