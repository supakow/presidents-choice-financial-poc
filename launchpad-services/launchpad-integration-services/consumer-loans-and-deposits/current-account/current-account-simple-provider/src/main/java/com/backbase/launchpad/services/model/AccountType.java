package com.backbase.launchpad.services.model;

/**
 * Type of an account. An account belongs to one account type.
 */
public enum AccountType {
    CASH, CREDIT, INVESTMENT
}