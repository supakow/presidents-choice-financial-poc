package com.backbase.launchpad.services.utils;

import org.apache.camel.Exchange;
import org.apache.camel.util.URISupport;

import java.net.URISyntaxException;
import java.util.Map;

/**
 * Utility class for handling {@link Exchange}.
 */
public final class ExchangeUtils {

    private ExchangeUtils() {
        super();
    }

    /**
     * Gets HTTP query parameters.
     *
     * @param exchange {@link Exchange}
     * @return map containing HTTP query parameters.
     * @throws URISyntaxException
     */
    public static Map<String, Object> getQueryParameters(Exchange exchange) throws URISyntaxException {
        return URISupport.parseQuery(exchange.getIn().getHeader(Exchange.HTTP_QUERY, String.class));
    }

    /**
     * Gets HTTP parameter value or if not present returns the passed default.
     *
     * @param exchange {@link Exchange}
     * @param parameterName HTTP parameter name.
     * @param defaultValue default value to be returned if parameter is not present.
     * @return HTTP parameter value or default.
     * @throws URISyntaxException
     */
    public static String getQueryParameter(Exchange exchange, String parameterName, String defaultValue)
            throws URISyntaxException {
        final Map<String, Object> queryParameters = getQueryParameters(exchange);
        final String parameterValue = (String) queryParameters.get(parameterName);
        if (parameterValue == null) {
            return defaultValue;
        }

        return parameterValue;
    }

    /**
     * Gets HTTP parameter value as int or if not present returns the passed default.
     *
     * @param exchange {@link Exchange}
     * @param parameterName HTTP parameter name.
     * @param defaultValue default value to be returned if parameter is not present.
     * @return HTTP parameter value as int or default.
     * @throws URISyntaxException
     */
    public static int getQueryParameterAsInt(Exchange exchange, String parameterName, int defaultValue)
            throws URISyntaxException {
        final Map<String, Object> queryParameters = getQueryParameters(exchange);
        final String parameterValue = (String) queryParameters.get(parameterName);
        if (parameterValue == null) {
            return defaultValue;
        }

        try {
            return Integer.parseInt(parameterValue);
        } catch (NumberFormatException e) {
            return defaultValue;
        }
    }

    /**
     * Gets HTTP parameter value as long or if not present returns the passed default.
     *
     * @param exchange {@link Exchange}
     * @param parameterName HTTP parameter name.
     * @param defaultValue default value to be returned if parameter is not present.
     * @return HTTP parameter value as long or default.
     * @throws URISyntaxException
     */
    public static long getQueryParameterAsLong(Exchange exchange, String parameterName, long defaultValue)
            throws URISyntaxException {
        final Map<String, Object> queryParameters = getQueryParameters(exchange);
        final String parameterValue = (String) queryParameters.get(parameterName);
        if (parameterValue == null) {
            return defaultValue;
        }

        try {
            return Long.parseLong(parameterValue);
        } catch (NumberFormatException e) {
            return defaultValue;
        }
    }

    /**
     * Gets value of given header.
     *
     * @param exchange {@link Exchange}
     * @param header header name
     * @return header value.
     * @throws URISyntaxException
     */
    public static String getHeaderValue(Exchange exchange, String header) throws URISyntaxException {
        return exchange.getIn().getHeader(header, String.class);
    }

}
