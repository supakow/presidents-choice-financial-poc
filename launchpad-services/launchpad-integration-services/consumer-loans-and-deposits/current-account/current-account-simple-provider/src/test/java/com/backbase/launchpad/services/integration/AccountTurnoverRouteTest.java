package com.backbase.launchpad.services.integration;

import com.jayway.jsonpath.JsonPath;
import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.ProducerTemplate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:META-INF/spring/test-backbase-mashup-service.xml")
@DirtiesContext
public class AccountTurnoverRouteTest {

    @Autowired
    private ApplicationContext applicationContext;

    private ProducerTemplate producerTemplate;

    @Before
    public void setUp() {
        producerTemplate = getCamelContext().createProducerTemplate();
    }

    @Test
    public void getAccountTurnoverCheckErrors() throws Exception {
        final Map<String, Object> headers = new HashMap<String, Object>();

        // no start time
        String response =
                producerTemplate.requestBodyAndHeaders("seda:provider/consumer-loans-and-deposits/current-accounts/turnover", null,
                        headers, String.class);
        String code = JsonPath.read(response, "code");
        assertThat(code, is(equalTo("400")));

        // end time before start time
        headers.put(Exchange.HTTP_QUERY, "start=1000&end=900");
        response =
                producerTemplate.requestBodyAndHeaders("seda:provider/consumer-loans-and-deposits/current-accounts/turnover", null,
                        headers, String.class);
        code = JsonPath.read(response, "code");
        assertThat(code, is(equalTo("400")));

        // non-existing account
        headers.put(Exchange.HTTP_QUERY, "start=1000&end=1900");
        headers.put("accountId", "non-exiting");
        response =
                producerTemplate.requestBodyAndHeaders("seda:provider/consumer-loans-and-deposits/current-accounts/turnover", null,
                        headers, String.class);
        code = JsonPath.read(response, "code");
        assertThat(code, is(equalTo("404")));
    }

    @Test
    public void getAccountTurnover() throws Exception {
        final Map<String, Object> headers = new HashMap<String, Object>();
        headers.put("accountId", "4efc90d1-dc99-4922-b070-6104c19c6731");

        // no end time: results found
        headers.put(Exchange.HTTP_QUERY, "start=1000");
        String response =
                producerTemplate.requestBodyAndHeaders("seda:provider/consumer-loans-and-deposits/current-accounts/turnover", null,
                        headers, String.class);
        List<String> dates = JsonPath.read(response, "$..date");
        assertFalse(dates.isEmpty());
        List<String> deposits = JsonPath.read(response, "$..deposit");
        assertFalse(deposits.isEmpty());
        List<String> withdrawal = JsonPath.read(response, "$..withdrawal");
        assertFalse(withdrawal.isEmpty());

        // no transactions in the given date range
        headers.put(Exchange.HTTP_QUERY, "start=1000&end=19000");
        response =
                producerTemplate.requestBodyAndHeaders("seda:provider/consumer-loans-and-deposits/current-accounts/turnover", null,
                        headers, String.class);
        dates = JsonPath.read(response, "$..date");
        assertTrue(dates.isEmpty());
        deposits = JsonPath.read(response, "$..deposit");
        assertTrue(deposits.isEmpty());
        withdrawal = JsonPath.read(response, "$..withdrawal");
        assertTrue(withdrawal.isEmpty());

        // transactions within given date range
        headers.put(Exchange.HTTP_QUERY, "start=1000&end=" + System.currentTimeMillis());
        response =
                producerTemplate.requestBodyAndHeaders("seda:provider/consumer-loans-and-deposits/current-accounts/turnover", null,
                        headers, String.class);
        dates = JsonPath.read(response, "$..date");
        assertFalse(dates.isEmpty());
        deposits = JsonPath.read(response, "$..deposit");
        assertFalse(deposits.isEmpty());
        withdrawal = JsonPath.read(response, "$..withdrawal");
        assertFalse(withdrawal.isEmpty());
    }

    private CamelContext getCamelContext() {
        return applicationContext.getBean("testCamelContext", CamelContext.class);
    }

}