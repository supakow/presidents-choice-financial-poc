package com.backbase.launchpad.services.integration;

import com.jayway.jsonpath.JsonPath;
import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.ProducerTemplate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:META-INF/spring/test-backbase-mashup-service.xml")
@DirtiesContext
public class TransactionRouteTest {

    @Autowired
    private ApplicationContext applicationContext;

    private ProducerTemplate producerTemplate;

    @Before
    public void setUp() {
        producerTemplate = getCamelContext().createProducerTemplate();
    }

    @Test
    public void findTransactionsForNonExistingAccount() throws Exception {
        final Map<String, Object> headers = new HashMap<String, Object>();
        headers.put("accountId", "bartekAccount");
        headers.put(Exchange.HTTP_QUERY, "f=0&l=20&sort=-bookingDateTime");
        final String response =
                producerTemplate.requestBodyAndHeaders("seda:provider/consumer-loans-and-deposits/current-accounts/accountId/transactions",
                        null, headers, String.class);

        final String code = JsonPath.read(response, "code");
        assertThat(code, is(equalTo("404")));
    }

    @Test
    public void findTransactionsForExistingAccount() throws Exception {
        final Map<String, Object> headers = new HashMap<String, Object>();
        headers.put("accountId", "4efc90d1-dc99-4922-b070-6104c19c6731");
        headers.put(Exchange.HTTP_QUERY, "f=0&l=20&sort=-bookingDateTime");
        final String response =
                producerTemplate.requestBodyAndHeaders("seda:provider/consumer-loans-and-deposits/current-accounts/accountId/transactions",
                        null, headers, String.class);

        final List<String> categoryIds = JsonPath.read(response, "$..categoryId");
        assertFalse(categoryIds.isEmpty());
        final List<String> bookingDateTimes = JsonPath.read(response, "$..bookingDateTime");
        assertFalse(bookingDateTimes.isEmpty());
        final List<String> creditDebitIndicators = JsonPath.read(response, "$..creditDebitIndicator");
        assertFalse(creditDebitIndicators.isEmpty());
    }

    private CamelContext getCamelContext() {
        return applicationContext.getBean("testCamelContext", CamelContext.class);
    }

}
