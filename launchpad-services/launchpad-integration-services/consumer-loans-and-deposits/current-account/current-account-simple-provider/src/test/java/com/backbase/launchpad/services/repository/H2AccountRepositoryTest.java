package com.backbase.launchpad.services.repository;


import com.backbase.launchpad.services.model.Account;
import com.backbase.launchpad.services.repository.impl.H2AccountRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:META-INF/spring/app-context.xml")
@DirtiesContext
public class H2AccountRepositoryTest {

    @Autowired
    private H2AccountRepository h2AccountRepository;

    @Test
    public void findAllForExistingCustomer() {
        final String customerId = "3";
        final List<Account> accounts = h2AccountRepository.findAll(customerId);
        assertEquals(3, accounts.size());
    }

    @Test
    public void findAllForNonExistingCustomer() {
        final String customerId = "-3";
        final List<Account> accounts = h2AccountRepository.findAll(customerId);
        assertTrue(accounts.isEmpty());
    }

    @Test
    public void findById() {
        final String accountId = "4efc90d1-dc99-4922-b070-6104c19c6731";
        Account account = h2AccountRepository.findById(accountId);
        assertNotNull(account);
    }

}