package com.backbase.launchpad.services.repository;

import com.backbase.launchpad.services.model.Transaction;
import com.backbase.launchpad.services.repository.impl.H2TransactionRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:META-INF/spring/app-context.xml")
@DirtiesContext
public class H2TransactionRepositoryTest {

    @Autowired
    private H2TransactionRepository h2TransactionRepository;

    @Test
    public void findByAccountIdNonExisting() {
        final List<Transaction> transactions =
                h2TransactionRepository.findByAccountId("non-exiting", "-bookingDateTime", 0, 100);
        assertThat(transactions.isEmpty(), is(true));
    }

    @Test
    public void findByAccountIdExisting() {
        final List<Transaction> transactions =
                h2TransactionRepository.findByAccountId("4efc90d1-dc99-4922-b070-6104c19c6731", "-bookingDateTime", 0,
                        100);
        assertThat(transactions.size(), is(25));
    }

    @Test
    public void getDetailsNonExisting() {
        assertNull(h2TransactionRepository.getDetails("non-existing"));
    }

    @Test
    public void getDetailsExisting() {
        final Transaction details = h2TransactionRepository.getDetails("8cf73bef-c6ee-4771-bcbe-593e26ff8095");
        assertNotNull(details);
        assertNotNull(details.getAddress().get("city"));
        assertNotNull(details.getLocation().get("latitude"));
    }

    @Test
    public void findTransactionsWithinAWeek() {

        int aWeekInMilliSeconds = 7 * 24 * 60 * 60 * 1000;

        System.out.println(aWeekInMilliSeconds);
        List<Transaction> transactions =
                h2TransactionRepository.findByAccountId("4efc90d1-dc99-4922-b070-6104c19c6731",
                        System.currentTimeMillis() - aWeekInMilliSeconds, System.currentTimeMillis());
        assertThat(transactions.size(), is(25));
    }

}
