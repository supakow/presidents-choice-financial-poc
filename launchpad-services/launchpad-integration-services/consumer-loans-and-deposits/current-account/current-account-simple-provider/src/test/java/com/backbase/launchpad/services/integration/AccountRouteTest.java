package com.backbase.launchpad.services.integration;

import com.jayway.jsonpath.JsonPath;
import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.ProducerTemplate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static com.jayway.jsonassert.JsonAssert.with;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:META-INF/spring/test-backbase-mashup-service.xml")
@DirtiesContext
public class AccountRouteTest {

    @Autowired
    private ApplicationContext applicationContext;

    private ProducerTemplate producerTemplate;

    @Before
    public void setUp() {
        producerTemplate = getCamelContext().createProducerTemplate();
    }

    @Test
    public void findAccountForExistingCustomer() throws Exception {
        final String customerQuery = "partyId=3";
        final String response = producerTemplate.requestBodyAndHeader("seda:provider/consumer-loans-and-deposits/current-accounts", null,
                Exchange.HTTP_QUERY, customerQuery, String.class);

        final List<String> names = JsonPath.read(response, "$..name");
        assertThat(names, hasSize(3));
        with(response).assertThat("$..name",
                hasItems("Personal Checking Account", "Business Account", "Shared Account"));
    }

    @Test
    public void findAccountForNonExistingCustomer() throws Exception {
        final String customerQuery = "partyId=dummy";
        final String response = producerTemplate.requestBodyAndHeader("seda:provider/consumer-loans-and-deposits/current-accounts", null,
                Exchange.HTTP_QUERY, customerQuery, String.class);

        final String code = JsonPath.read(response, "code");
        assertThat(code, is(equalTo("404")));
    }

    private CamelContext getCamelContext() {
        return applicationContext.getBean("testCamelContext", CamelContext.class);
    }

}

