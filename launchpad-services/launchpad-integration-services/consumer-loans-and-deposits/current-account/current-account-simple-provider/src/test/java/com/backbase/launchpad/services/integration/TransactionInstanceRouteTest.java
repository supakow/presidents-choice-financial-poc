package com.backbase.launchpad.services.integration;

import com.jayway.jsonpath.JsonPath;
import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:META-INF/spring/test-backbase-mashup-service.xml")
@DirtiesContext
public class TransactionInstanceRouteTest {

    @Autowired
    private ApplicationContext applicationContext;

    private ProducerTemplate producerTemplate;

    @Before
    public void setUp() {
        producerTemplate = getCamelContext().createProducerTemplate();
    }

    @Test
    public void getDetailsNonExistingTransaction() throws Exception {
        final Map<String, Object> headers = new HashMap<String, Object>();
        headers.put("transactionId", "non-existing");
        final String response =
                producerTemplate.requestBodyAndHeaders(
                        "seda:provider/consumer-loans-and-deposits/current-accounts/transaction/details",
                        null, headers, String.class);

        final String code = JsonPath.read(response, "code");
        assertThat(code, is(equalTo("404")));
    }

    @Test
    public void findTransactionsForExistingAccount() throws Exception {
        final Map<String, Object> headers = new HashMap<String, Object>();
        headers.put("transactionId", "8cf73bef-c6ee-4771-bcbe-593e26ff8095");
        final String response =
                producerTemplate.requestBodyAndHeaders(
                        "seda:provider/consumer-loans-and-deposits/current-accounts/transaction/details",
                        null, headers, String.class);

        final List<String> addressCity = JsonPath.read(response, "$..address.city");
        assertFalse(addressCity.isEmpty());
        final List<String> locationLatitude = JsonPath.read(response, "$..location.latitude");
        assertFalse(locationLatitude.isEmpty());

    }

    private CamelContext getCamelContext() {
        return applicationContext.getBean("testCamelContext", CamelContext.class);
    }

}