package com.backbase.launchpad.services.currentaccount.turnover;

import org.apache.camel.Endpoint;
import org.apache.camel.EndpointInject;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.converter.jaxb.JaxbDataFormat;
import org.apache.camel.spi.DataFormat;

import com.backbase.launchpad.services.currentaccount.model.TransactionList;

/**
 * Camel route for account balance turnover functionality.
 */
public class AccountTurnoverRoute extends RouteBuilder {

    private static final String XMLDB_SERVICE_TYPE = "XMLDB_SERVICE_TYPE";

    // Main endpoint
    @EndpointInject(uri = "seda:provider/consumer-loans-and-deposits/current-accounts/turnover")
    private Endpoint accountTurnoverEndpoint;

    // Internal endpoints
    @EndpointInject(uri = "velocity:retrieveTransactionsXQuery.vm")
    private Endpoint retrieveTransactionsXQuery;

    @EndpointInject(uri = "xmldb:exist:///db/launchpad/transactions?username=admin&password=")
    protected Endpoint transactionsEndpoint;

    @Override
    public void configure() throws Exception {

        DataFormat jaxb = new JaxbDataFormat(TransactionList.class.getPackage().getName());

        from(accountTurnoverEndpoint)
            .setHeader(XMLDB_SERVICE_TYPE, constant("XQUERY"))
            .to(retrieveTransactionsXQuery)
            .to(transactionsEndpoint)
            .convertBodyTo(String.class)
            .unmarshal(jaxb)
            .process(new AccountTurnoverProcessor());
    }
}
