package com.backbase.launchpad.services.currentaccount.p2p;

import com.backbase.launchpad.services.currentaccount.p2p.model.TransactionList;
import org.apache.camel.Endpoint;
import org.apache.camel.EndpointInject;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.converter.jaxb.JaxbDataFormat;
import org.apache.camel.spi.DataFormat;

public class TransactionsRoute extends RouteBuilder {

    private static final String XMLDB_SERVICE_TYPE = "XMLDB_SERVICE_TYPE";

    // Main endpoints
    @EndpointInject(uri = "seda:provider/transactions")
    private Endpoint transactionsEndpoint;
    @EndpointInject(uri = "seda:provider/transactions/transactionId")
    private Endpoint transactionInstanceEndpoint;


    // Internal endpoints
    @EndpointInject(uri = "velocity:retrieveAllTransactionsXQuery.vm")
    private Endpoint retrieveAllTransactionsXQuery;

    @EndpointInject(uri = "xmldb:exist:///db/launchpad/transactions?username=admin&password=")
    protected Endpoint existDBTransactionsEndpoint;

    @Override
    public void configure() throws Exception {

        DataFormat jaxb = new JaxbDataFormat(TransactionList.class.getPackage().getName());

        from(transactionsEndpoint)
            .setHeader(XMLDB_SERVICE_TYPE, constant("XQUERY"))
            .to(retrieveAllTransactionsXQuery)
            .to(existDBTransactionsEndpoint)
            .convertBodyTo(String.class)
            .unmarshal(jaxb)
            .process(new TransactionCollectionProcessor());

        from(transactionInstanceEndpoint).process(new TransactionInstanceProcessor());

    }
}
