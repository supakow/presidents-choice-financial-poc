package com.backbase.launchpad.services.currentaccount.balance;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.joda.time.DateTime;
import org.joda.time.Days;

import com.backbase.launchpad.services.currentaccount.model.Transaction;
import com.backbase.launchpad.services.currentaccount.model.TransactionList;

/**
 * Camel {@link Processor} which calculates the account balance between a start date and end date.
 */
public class AccountBalanceProcessor implements Processor {
    public void process(Exchange exchange) throws Exception {
        Message in = exchange.getIn();
        String start = in.getHeader("start", String.class);
        String end = in.getHeader("end", String.class);
        String balance = in.getHeader("balance", String.class);

        DateTime endTime;
        DateTime startTime;

        try {
            endTime = new DateTime(Long.parseLong(end));
            startTime = new DateTime(Long.parseLong(start));
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("start or end parameters are not numbers");
        }

        if (startTime.isAfter(endTime)) {
            throw new IllegalArgumentException("start time cannot be after end time");
        }

        TransactionList transactions = in.getBody(TransactionList.class);
        AccountBalances balances = calculateAccountBalance(startTime, endTime, transactions, new BigDecimal(balance));

        exchange.getIn().setBody(createResponse(balances));
    }

    /**
     * Calculates account balances for the given period.
     */
    private AccountBalances calculateAccountBalance(DateTime startTime, DateTime endTime, TransactionList transactions, BigDecimal currentAmount) {
        AccountBalances balances = new AccountBalances();

        Days days = Days.daysBetween(startTime, endTime);
        DateTime currentTime = endTime;

        for (int i = 1; i <= days.getDays(); i++) {
            List<Transaction> transactionsPerDay = getTransactionsForDay(currentTime.withMillisOfDay(0), transactions);

            DayAccountBalance dayAccountBalance = new DayAccountBalance();
            dayAccountBalance.setAmount(currentAmount);
            dayAccountBalance.setDate(currentTime.withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59));

            for (Transaction transaction : transactionsPerDay) {
                switch (transaction.getCreditDebitIndicator()) {
                    case CRDT:
                        dayAccountBalance.setAmount(dayAccountBalance.getAmount().subtract(transaction.getAmount()));
                        break;
                    case DBIT:
                        dayAccountBalance.setAmount(dayAccountBalance.getAmount().add(transaction.getAmount()));
                        break;
                }
            }

            currentTime = currentTime.minusDays(1);
            currentAmount = dayAccountBalance.getAmount();
            balances.addAccountBalance(dayAccountBalance);

        }

        return balances;
    }


    /**
     * Retrieves transactions for a given day.
     *
     * @param day a {@link DateTime} instance identifying a day
     * @param transactions a list of transactions
     * @return a list of transactions on a day
     */
    private List<Transaction> getTransactionsForDay(DateTime day, TransactionList transactions) {
        DateTime endDay = day.withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59);
        List<Transaction> result = new ArrayList<Transaction>();
        for (Transaction transaction : transactions.getTransactions()) {
            if (transaction.getTime().isAfter(day) && transaction.getTime().isBefore(endDay)) {
                result.add(transaction);
            }
        }
        return result;
    }

    /**
     * Creates the response from the {@link AccountBalances} instance.
     */
    private String createResponse(AccountBalances balances) {
        JSONArray response = new JSONArray();
        for (DayAccountBalance balance : balances.getBalances()) {
            JSONObject dayBalance = new JSONObject();
            dayBalance.put("date", balance.getDate().toDate().getTime());
            dayBalance.put("amount", balance.getAmount().setScale(2, RoundingMode.CEILING));
            response.add(dayBalance);
        }
        return response.toString();
    }

    // Utility classes during computing the result

    private static class DayAccountBalance {

        private DateTime date;
        private BigDecimal amount;

        public DateTime getDate() {
            return date;
        }

        public void setDate(DateTime date) {
            this.date = date;
        }

        public BigDecimal getAmount() {
            return amount;
        }

        public void setAmount(BigDecimal amount) {
            this.amount = amount;
        }
    }

    private static class AccountBalances {

        private LinkedList<DayAccountBalance>
                balances = new LinkedList<DayAccountBalance>();

        public List<DayAccountBalance> getBalances() {
            return balances;
        }

        public void addAccountBalance(DayAccountBalance balance) {
            balances.addFirst(balance);
        }
    }


}
