package com.backbase.launchpad.services.currentaccount.turnover;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.joda.time.DateTime;
import org.joda.time.Days;

import com.backbase.launchpad.services.currentaccount.model.Transaction;
import com.backbase.launchpad.services.currentaccount.model.TransactionList;

/**
 * Camel {@link Processor} which calculates the account turnover between a start date and end date.
 */
public class AccountTurnoverProcessor implements Processor {
    public void process(Exchange exchange) throws Exception {
        Message in = exchange.getIn();
        String start = in.getHeader("start", String.class);
        String end = in.getHeader("end", String.class);

        DateTime endTime;
        DateTime startTime;

        try {
            endTime = new DateTime(Long.parseLong(end));
            startTime = new DateTime(Long.parseLong(start));
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("start or end parameters are not numbers");
        }

        if (startTime.isAfter(endTime)) {
            throw new IllegalArgumentException("start time cannot be after end time");
        }

        TransactionList transactions = in.getBody(TransactionList.class);
        AccountTurnovers turnovers = calculateAccountBalance(startTime, endTime, transactions);

        exchange.getIn().setBody(createResponse(turnovers));
    }

    private String createResponse(AccountTurnovers turnovers) {
        JSONArray response = new JSONArray();
        for (DayAccountTurnover turnover : turnovers.getTurnovers()) {
            JSONObject dayBalance = new JSONObject();
            dayBalance.put("date", turnover.date.toDate().getTime());
            dayBalance.put("deposit", turnover.deposit.setScale(2, RoundingMode.CEILING));
            dayBalance.put("withdrawal", turnover.withdrawal.setScale(2, RoundingMode.CEILING));
            response.add(dayBalance);
        }
        return response.toString();
    }

    /**
     * Calculates account turnover for the given period.
     */
    private AccountTurnovers calculateAccountBalance(DateTime startTime, DateTime endTime,
            TransactionList transactions) {

        AccountTurnovers turnovers = new AccountTurnovers();

        Days days = Days.daysBetween(startTime, endTime);
        DateTime currentTime = endTime;

        for (int i = 1; i <= days.getDays(); i++) {
            List<Transaction> transactionsPerDay = getTransactionsForDay(currentTime.withMillisOfDay(0), transactions);

            DayAccountTurnover dayAccountTurnover = new DayAccountTurnover();
            dayAccountTurnover.date = currentTime.withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59);

            BigDecimal deposit = BigDecimal.ZERO;
            BigDecimal withdrawal = BigDecimal.ZERO;

            for (Transaction transaction : transactionsPerDay) {
                switch (transaction.getCreditDebitIndicator()) {
                    case CRDT:
                        deposit = deposit.add(transaction.getAmount());
                        break;
                    case DBIT:
                        withdrawal = withdrawal.add(transaction.getAmount());
                        break;
                }
            }

            dayAccountTurnover.deposit = deposit;
            dayAccountTurnover.withdrawal = withdrawal;

            currentTime = currentTime.minusDays(1);
            turnovers.addAccountBalance(dayAccountTurnover);
        }

        return turnovers;
    }

    /**
     * Retrieves transactions for a given day.
     *
     * @param day a {@link DateTime} instance identifying a day
     * @param transactions a list of transactions
     * @return a list of transactions on a day
     */
    private List<Transaction> getTransactionsForDay(DateTime day, TransactionList transactions) {
        DateTime endDay = day.withHourOfDay(23).withMinuteOfHour(59).withSecondOfMinute(59);
        List<Transaction> result = new ArrayList<Transaction>();
        for (Transaction transaction : transactions.getTransactions()) {
            if (transaction.getTime().isAfter(day) && transaction.getTime().isBefore(endDay)) {
                result.add(transaction);
            }
        }
        return result;
    }

    // Utility classes during computing the result

    private static class AccountTurnovers {

        private LinkedList<DayAccountTurnover> balances = new LinkedList<DayAccountTurnover>();

        public List<DayAccountTurnover> getTurnovers() {
            return balances;
        }

        public void addAccountBalance(DayAccountTurnover turnover) {
            balances.addFirst(turnover);
        }
    }

    private static class DayAccountTurnover {
        private DateTime date;
        private BigDecimal deposit;
        private BigDecimal withdrawal;
    }
}
