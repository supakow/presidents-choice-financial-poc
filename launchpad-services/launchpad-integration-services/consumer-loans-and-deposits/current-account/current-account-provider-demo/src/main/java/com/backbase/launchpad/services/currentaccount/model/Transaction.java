package com.backbase.launchpad.services.currentaccount.model;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.joda.time.DateTime;

/**
 * Represents a transaction in the context of calculating the account balance.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Transaction {

    @XmlJavaTypeAdapter(BigDecimalAdapter.class)
    private BigDecimal amount;

    @XmlJavaTypeAdapter(DateTimeAdapter.class)
    private DateTime time;

    @XmlJavaTypeAdapter(CreditDebitIndicatorAdapter.class)
    private CreditDebitIndicator creditDebitIndicator;

    public Transaction() {
    }

    public Transaction(BigDecimal amount, DateTime time, CreditDebitIndicator type) {
        this.amount = amount;
        this.time = time;
        this.creditDebitIndicator = type;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setBigDecimal(BigDecimal amount) {
        this.amount = amount;
    }

    public DateTime getTime() {
        return time;
    }

    public void setTime(DateTime time) {
        this.time = time;
    }

    public CreditDebitIndicator getCreditDebitIndicator() {
        return creditDebitIndicator;
    }

    public void setCreditDebitIndicator(CreditDebitIndicator type) {
        this.creditDebitIndicator = type;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Transaction[time=");
        sb.append(time);
        sb.append(",amount=");
        sb.append(amount);
        sb.append(",creditDebitIndicator=");
        sb.append(creditDebitIndicator);
        sb.append("]");
        return sb.toString();
    }

    public static enum CreditDebitIndicator {
        CRDT, DBIT
    }

    static class CreditDebitIndicatorAdapter extends XmlAdapter<String, CreditDebitIndicator> {

        @Override
        public CreditDebitIndicator unmarshal(String value) throws Exception {
            return CreditDebitIndicator.valueOf(value);
        }

        @Override
        public String marshal(CreditDebitIndicator transactionType) throws Exception {
            return transactionType.name();
        }
    }

    static class BigDecimalAdapter extends XmlAdapter<String, BigDecimal> {

        @Override
        public BigDecimal unmarshal(String value) throws Exception {
            return new BigDecimal(value);
        }

        @Override
        public String marshal(BigDecimal amount) throws Exception {
            return amount.toString();
        }
    }

    static class DateTimeAdapter extends XmlAdapter<String, DateTime> {

        @Override
        public DateTime unmarshal(String value) throws Exception {
            return new DateTime(Long.parseLong(value));
        }

        @Override
        public String marshal(DateTime v) throws Exception {
            return v.toString();
        }
    }
}

