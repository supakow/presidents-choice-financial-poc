package com.backbase.launchpad.services.currentaccount.p2p;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class TransactionInstanceProcessor implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {
        exchange.getOut().setHeader(Exchange.HTTP_RESPONSE_CODE, 204);
    }
}
