package com.backbase.launchpad.services.currentaccount.transaction;

import com.backbase.mashup.camel.json.XmlJsonDataFormatWrapper;
import org.apache.camel.Endpoint;
import org.apache.camel.EndpointInject;
import org.apache.camel.Exchange;
import org.apache.camel.Predicate;
import org.apache.camel.builder.RouteBuilder;
import org.restlet.data.Method;
import org.springframework.beans.factory.annotation.Autowired;

public class TransactionRoute extends RouteBuilder {

    // main endpoint
    @EndpointInject(uri = "seda:provider/consumer-loans-and-deposits/current-accounts/accountId/transactions/transactionId")
    private Endpoint singleTransactionEndpoint;

    private static final String XMLDB_XQUERY_SERVICE = "XMLDB_SERVICE_TYPE";
    protected static final String VELOCITY_UPDATE_CATEGORY = "velocity:updateTransactionCategory.vm";
    private static final String XMLDB_TRANSACTION_REQUEST_BY_ID = "xmldb:exist:///db/launchpad/transactions?XPath=//o[id = '${header.transactionId}']";
    protected static final String XMLDB_STORE_TRANSACTION = "xmldb:exist:///db/launchpad/transactions?username=admin&password=";

    @Autowired
    private XmlJsonDataFormatWrapper xmlJsonDataFormat;

    @Override
    public void configure() throws Exception {
        Predicate isUpdate = header(Exchange.HTTP_METHOD).isEqualTo(Method.PUT);
        Predicate isRetrieve = header(Exchange.HTTP_METHOD).isEqualTo(Method.GET);

        from(singleTransactionEndpoint)
           .choice()
                .when(isUpdate)
                    .log("Update transaction")
                    .to(VELOCITY_UPDATE_CATEGORY)
                    .setHeader(XMLDB_XQUERY_SERVICE).constant("XQUERY")
                    .to(XMLDB_STORE_TRANSACTION)
                    .setBody().simple("{ \"status\" : \"OK\" }")
           .end()
           .choice()
                .when(isRetrieve)
                    .log("Retrieve transaction")
                    .recipientList(simple(XMLDB_TRANSACTION_REQUEST_BY_ID))
                    .marshal(xmlJsonDataFormat)
                    .convertBodyTo(String.class)
           .end();

    }
}
