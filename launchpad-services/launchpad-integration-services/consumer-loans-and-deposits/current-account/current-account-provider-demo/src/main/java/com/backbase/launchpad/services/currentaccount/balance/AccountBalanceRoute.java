package com.backbase.launchpad.services.currentaccount.balance;

import static org.apache.camel.builder.PredicateBuilder.isNotNull;

import org.apache.camel.Endpoint;
import org.apache.camel.EndpointInject;
import org.apache.camel.Exchange;
import org.apache.camel.Predicate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.converter.jaxb.JaxbDataFormat;
import org.apache.camel.spi.DataFormat;

import com.backbase.launchpad.services.currentaccount.model.TransactionList;

/**
 * Camel route for account balance functionality.
 */
public final class AccountBalanceRoute extends RouteBuilder {

    private static final String XMLDB_SERVICE_TYPE = "XMLDB_SERVICE_TYPE";

    // Main endpoint
    @EndpointInject(uri = "seda:provider/consumer-loans-and-deposits/current-accounts/balance")
    private Endpoint accountBalanceEndpoint;

    // Internal endpoints
    @EndpointInject(uri = "velocity:queryAccountBalanceXQuery.vm")
    private Endpoint queryAccountBalanceXQuery;

    @EndpointInject(uri = "velocity:retrieveTransactionsXQuery.vm")
    private Endpoint retrieveTransactionsXQuery;

    @EndpointInject(uri = "xmldb:exist:///db/launchpad/transactions?username=admin&password=")
    protected Endpoint transactionsEndpoint;

    @EndpointInject(uri = "xmldb:exist:///db/launchpad/accounts?username=admin&password=")
    protected Endpoint accountsEndpoint;

    @Override
    public void configure() throws Exception {

        DataFormat jaxb = new JaxbDataFormat(TransactionList.class.getPackage().getName());

        Predicate accountFound = isNotNull(simple("${body}"));

        from(accountBalanceEndpoint)
                .setHeader(XMLDB_SERVICE_TYPE, constant("XQUERY"))
                .to(queryAccountBalanceXQuery)
                .to(accountsEndpoint)
                .convertBodyTo(String.class)
                .choice()
                    .when(accountFound)
                        .log("Body: ${body}")
                        .setHeader("balance", xpath("//o/balance/text()").stringResult())
                        .to(retrieveTransactionsXQuery)
                        .to(transactionsEndpoint)
                        .convertBodyTo(String.class)
                        .unmarshal(jaxb)
                        .process(new AccountBalanceProcessor())
                    .otherwise()
                        .setHeader(Exchange.HTTP_RESPONSE_CODE).constant(400)
                        .setBody(constant("{\"errors\" : [{\"code\" : 198, \"message\" : \"The requested account does not exist.\"}]}"))

                .end();

    }

}
