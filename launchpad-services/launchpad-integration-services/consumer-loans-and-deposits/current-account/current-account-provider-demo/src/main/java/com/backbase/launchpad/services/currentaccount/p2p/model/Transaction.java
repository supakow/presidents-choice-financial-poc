package com.backbase.launchpad.services.currentaccount.p2p.model;

import org.joda.time.DateTime;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.math.BigDecimal;

/**
 * Represents a transaction in the context of calculating the account balance.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Transaction {

    private String id;
    private String accountId;
    private String categoryId;

    @XmlJavaTypeAdapter(DateTimeAdapter.class)
    private DateTime bookingDateTime;

    private String counterpartyAccount;
    private String counterpartyName;

    @XmlJavaTypeAdapter(BigDecimalAdapter.class)
    private BigDecimal instructedAmount;
    private String instructedCurrency;

    @XmlJavaTypeAdapter(BigDecimalAdapter.class)
    private BigDecimal transactionAmount;
    private String transactionCurrency;

    @XmlJavaTypeAdapter(CreditDebitIndicatorAdapter.class)
    private CreditDebitIndicator creditDebitIndicator;

    private String transactionType;

    public Transaction() {
    }

    public Transaction(String id,
                       String accountId,
                       String categoryId,
                       DateTime bookingDateTime,
                       String counterpartyAccount,
                       String counterpartyName,
                       BigDecimal instructedAmount,
                       String instructedCurrency,
                       BigDecimal transactionAmount,
                       String transactionCurrency,
                       CreditDebitIndicator creditDebitIndicator,
                       String transactionType) {
        this.id = id;
        this.accountId = accountId;
        this.categoryId = categoryId;
        this.bookingDateTime = bookingDateTime;
        this.counterpartyAccount = counterpartyAccount;
        this.counterpartyName = counterpartyName;
        this.instructedAmount = instructedAmount;
        this.instructedCurrency = instructedCurrency;
        this.transactionAmount = transactionAmount;
        this.transactionCurrency = transactionCurrency;
        this.creditDebitIndicator = creditDebitIndicator;
        this.transactionType = transactionType;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Transaction[id=");
        sb.append(id);
        sb.append(",accountId=");
        sb.append(accountId);
        sb.append(",categoryId=");
        sb.append(categoryId);
        sb.append(",bookingDateTime=");
        sb.append(bookingDateTime);
        sb.append(",counterpartyAccount=");
        sb.append(counterpartyAccount);
        sb.append(",counterpartyName=");
        sb.append(counterpartyName);
        sb.append(",instructedAmount=");
        sb.append(instructedAmount);
        sb.append(",instructedCurrency=");
        sb.append(instructedCurrency);
        sb.append(",transactionAmount=");
        sb.append(transactionAmount);
        sb.append(",transactionCurrency=");
        sb.append(transactionCurrency);
        sb.append(",creditDebitIndicator=");
        sb.append(creditDebitIndicator);
        sb.append(",transactionType=");
        sb.append(transactionType);
        sb.append("]");
        return sb.toString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public DateTime getBookingDateTime() {
        return bookingDateTime;
    }

    public void setBookingDateTime(DateTime bookingDateTime) {
        this.bookingDateTime = bookingDateTime;
    }

    public String getCounterpartyAccount() {
        return counterpartyAccount;
    }

    public void setCounterpartyAccount(String counterpartyAccount) {
        this.counterpartyAccount = counterpartyAccount;
    }

    public String getCounterpartyName() {
        return counterpartyName;
    }

    public void setCounterpartyName(String counterpartyName) {
        this.counterpartyName = counterpartyName;
    }

    public BigDecimal getInstructedAmount() {
        return instructedAmount;
    }

    public void setInstructedAmount(BigDecimal instructedAmount) {
        this.instructedAmount = instructedAmount;
    }

    public String getInstructedCurrency() {
        return instructedCurrency;
    }

    public void setInstructedCurrency(String instructedCurrency) {
        this.instructedCurrency = instructedCurrency;
    }

    public BigDecimal getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(BigDecimal transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getTransactionCurrency() {
        return transactionCurrency;
    }

    public void setTransactionCurrency(String transactionCurrency) {
        this.transactionCurrency = transactionCurrency;
    }

    public CreditDebitIndicator getCreditDebitIndicator() {
        return creditDebitIndicator;
    }

    public void setCreditDebitIndicator(CreditDebitIndicator creditDebitIndicator) {
        this.creditDebitIndicator = creditDebitIndicator;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public static enum CreditDebitIndicator {
        CRDT, DBIT
    }

    static class CreditDebitIndicatorAdapter extends XmlAdapter<String, CreditDebitIndicator> {

        @Override
        public CreditDebitIndicator unmarshal(String value) throws Exception {
            return CreditDebitIndicator.valueOf(value);
        }

        @Override
        public String marshal(CreditDebitIndicator transactionType) throws Exception {
            return transactionType.name();
        }
    }

    static class BigDecimalAdapter extends XmlAdapter<String, BigDecimal> {

        @Override
        public BigDecimal unmarshal(String value) throws Exception {
            return new BigDecimal(value);
        }

        @Override
        public String marshal(BigDecimal amount) throws Exception {
            return amount.toString();
        }
    }

    static class DateTimeAdapter extends XmlAdapter<String, DateTime> {

        @Override
        public DateTime unmarshal(String value) throws Exception {
            return new DateTime(Long.parseLong(value));
        }

        @Override
        public String marshal(DateTime v) throws Exception {
            return v.toString();
        }
    }
}
