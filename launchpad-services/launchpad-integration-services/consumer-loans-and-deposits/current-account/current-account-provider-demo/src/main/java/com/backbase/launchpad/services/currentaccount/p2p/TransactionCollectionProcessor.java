package com.backbase.launchpad.services.currentaccount.p2p;

import com.backbase.launchpad.services.currentaccount.p2p.model.Transaction;
import com.backbase.launchpad.services.currentaccount.p2p.model.TransactionList;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;

import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class TransactionCollectionProcessor implements Processor {

    private String[] logos = new String[]{
            "/portalserver/static/launchpad/widgets/transactions/media/atlanticelec.png",
            "/portalserver/static/launchpad/widgets/transactions/media/tealounge.png",
            "/portalserver/static/launchpad/widgets/transactions/media/texaco.png",
            "/portalserver/static/launchpad/widgets/transactions/media/tmobile.png"
    };

    private String[] statuses = new String[]{
            "SENT", "PENDING"
    };

    private Random generator = new Random();
    private int DEFAULT_OFFSET = 0;
    private int DEFAULT_LIMIT = 10;

    @Override
    public void process(Exchange exchange) throws Exception {

        Message in = exchange.getIn();
        Integer offsetAsInteger = in.getHeader("offset", Integer.class);
        int offset;
        if (offsetAsInteger == null) {
            offset = DEFAULT_OFFSET;
        } else {
            offset = offsetAsInteger.intValue();
        }
        Integer limitAsInteger = in.getHeader("limit", Integer.class);
        int limit;
        if (limitAsInteger == null) {
            limit = DEFAULT_LIMIT;
        } else {
            limit = limitAsInteger.intValue();
        }

        String[] accountIds = in.getHeader("accountIds", String.class).split(",");

        TransactionList transactions = in.getBody(TransactionList.class);

        List<Transaction> result = new ArrayList<Transaction>();
        for (String accountId : accountIds) {
            result.addAll(filter(transactions, accountId));
        }

        List<Transaction> pagintedResult = new ArrayList<Transaction>();
        if (result.isEmpty()) {
            exchange.getOut().setHeader(Exchange.HTTP_RESPONSE_CODE, 204);
        } else {
            if (offset >= result.size() || limit == 0) {
                exchange.getOut().setHeader(Exchange.HTTP_RESPONSE_CODE, 204);
            } else {
                int nr = 0;
                for (int i = offset; i < result.size(); i++) {
                    pagintedResult.add(result.get(i));
                    nr++;
                    if (nr == limit) break;
                }
            }
            String response = createResponse(pagintedResult);
            exchange.getOut().setBody(response);
        }
    }

    private String createResponse(List<Transaction> transactions) {
        JSONArray result = new JSONArray();
        for (Transaction transaction : transactions) {
            result.add(toJson(transaction));
        }
        return result.toString();
    }

    private JSONObject toJson(Transaction transaction) {
        JSONObject transactionAsJson = new JSONObject();
        transactionAsJson.put("id", transaction.getId());
        transactionAsJson.put("accountId", transaction.getAccountId());
        transactionAsJson.put("categoryId", transaction.getCategoryId());
        transactionAsJson.put("bookingDateTime", transaction.getBookingDateTime().toString());

        JSONObject counterparty = new JSONObject();
        counterparty.put("name", transaction.getCounterpartyName());
        counterparty.put("account", transaction.getCounterpartyAccount());
        if (transaction.getTransactionType().equals("P2P_EMAIL")) {
            counterparty.put("type", "EMAIL");
        } else {
            counterparty.put("type", "IBAN");
        }
        counterparty.put("logo", logos[generator.nextInt(4)]);
        transactionAsJson.put("counterparty", counterparty);

        transactionAsJson.put("instructedAmount", transaction.getInstructedAmount().setScale(2, RoundingMode.CEILING));
        transactionAsJson.put("instructedCurrency", transaction.getInstructedCurrency());
        transactionAsJson.put("transactionAmount", transaction.getTransactionAmount().setScale(2, RoundingMode.CEILING));
        transactionAsJson.put("transactionCurrency", transaction.getTransactionCurrency());
        transactionAsJson.put("creditDebitIndicator", transaction.getCreditDebitIndicator() == Transaction.CreditDebitIndicator.CRDT ? "CREDIT" : "DEBIT");

        transactionAsJson.put("description", "Lorem ipsum dolor sit amet, consectetur adipiscing elit.");
        transactionAsJson.put("status", statuses[generator.nextInt(2)]);
        return transactionAsJson;
    }


    private List<Transaction> filter(TransactionList transactions, String accountId) {
        List<Transaction> result = new ArrayList<Transaction>();
        for (Transaction transaction : transactions.getTransactions()) {
            if (transaction.getAccountId().equals(accountId)) {
                result.add(transaction);
            }
        }
        return result;
    }




}
