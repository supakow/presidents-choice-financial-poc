package com.backbase.launchpad.services.currentaccount.balance;

import static com.backbase.launchpad.services.currentaccount.model.Transaction.CreditDebitIndicator.CRDT;
import static com.backbase.launchpad.services.currentaccount.model.Transaction.CreditDebitIndicator.DBIT;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

import java.math.BigDecimal;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.impl.DefaultExchange;
import org.apache.camel.impl.DefaultMessage;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.backbase.launchpad.services.currentaccount.model.Transaction;
import com.backbase.launchpad.services.currentaccount.model.TransactionList;

/**
 * Unit test for {@link AccountBalanceProcessor}
 */
@RunWith(MockitoJUnitRunner.class)
public class AccountBalanceProcessorTest {

    // under test
    private AccountBalanceProcessor processor;

    // collaborators

    private Exchange exchange;
    private Message message;
    @Mock
    private CamelContext camelContext;

    @Before
    public void setUp() {
        processor = new AccountBalanceProcessor();
        exchange = new DefaultExchange(camelContext);
        message = new DefaultMessage();
        exchange.setIn(message);
    }

    @Test
    public void process() throws Exception {

        DateTime dateTime = new DateTime("2013-12-15T22:55:06.144+01:00");

        message.setHeader("start", "" + dateTime.minusWeeks(1).toDate().getTime());
        message.setHeader("end", "" + dateTime.toDate().getTime());
        message.setHeader("balance", "" + 12000);

        TransactionList transactions = new TransactionList();
        transactions.addTransaction(new Transaction(new BigDecimal(25.50), dateTime.minusDays(2), CRDT));
        transactions.addTransaction(
                new Transaction(new BigDecimal(50.23), dateTime.minusDays(2).withHourOfDay(15), DBIT));
        transactions.addTransaction(new Transaction(new BigDecimal(100), dateTime.minusDays(5), DBIT));
        transactions.addTransaction(new Transaction(new BigDecimal(500), dateTime.minusDays(10), CRDT));
        message.setBody(transactions);

        // call method
        processor.process(exchange);

        String result = exchange.getIn().getBody(String.class);
        String expectedResult = "[{\"date\":1386629999144,\"amount\":12124.73},{\"date\":1386716399144,\"amount\":12124.73},{\"date\":1386802799144,\"amount\":12024.73},{\"date\":1386889199144,\"amount\":12024.73},{\"date\":1386975599144,\"amount\":12024.73},{\"date\":1387061999144,\"amount\":12000},{\"date\":1387148399144,\"amount\":12000}]";

        assertThat(result.contains("12124.73"), is(true));
        assertThat(result.contains("12024.73"), is(true));
        assertThat(result.contains("12000"), is(true));

    }
}
