package com.backbase.launchpad.services.currentaccount.turnover;

import static com.backbase.launchpad.services.currentaccount.model.Transaction.CreditDebitIndicator.CRDT;
import static com.backbase.launchpad.services.currentaccount.model.Transaction.CreditDebitIndicator.DBIT;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

import java.math.BigDecimal;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.impl.DefaultExchange;
import org.apache.camel.impl.DefaultMessage;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.backbase.launchpad.services.currentaccount.model.Transaction;
import com.backbase.launchpad.services.currentaccount.model.TransactionList;

@RunWith(MockitoJUnitRunner.class)
public class AccountTurnoverProcessorTest {

    // under test
    private AccountTurnoverProcessor processor;

    // collaborators

    private Exchange exchange;
    private Message message;
    @Mock
    private CamelContext camelContext;

    @Before
    public void setUp() {
        processor = new AccountTurnoverProcessor();
        exchange = new DefaultExchange(camelContext);
        message = new DefaultMessage();
        exchange.setIn(message);
    }

    @Test
    public void process() throws Exception {

        DateTime time = new DateTime("2013-12-15T22:55:06.144+01:00");

        message.setHeader("start", "" + time.minusWeeks(1).toDate().getTime());
        message.setHeader("end", "" + time.toDate().getTime());

        TransactionList transactions = new TransactionList();
        transactions.addTransaction(new Transaction(new BigDecimal(25.50), time.minusDays(2), CRDT));
        transactions.addTransaction(
                new Transaction(new BigDecimal(50.23), time.minusDays(2).withHourOfDay(15), DBIT));
        transactions.addTransaction(
                new Transaction(new BigDecimal(10.25), time.minusDays(2).withHourOfDay(7), DBIT));
        transactions.addTransaction(new Transaction(new BigDecimal(100), time.minusDays(5), DBIT));
        transactions.addTransaction(new Transaction(new BigDecimal(500), time.minusDays(10), CRDT));
        message.setBody(transactions);

        // call method
        processor.process(exchange);

        String result = exchange.getIn().getBody(String.class);

        assertThat(result.contains("\"withdrawal\":100"), is(true));
        assertThat(result.contains("\"deposit\":25.5,\"withdrawal\":60.48"), is(true));
        assertThat(result.contains("\"deposit\":0"), is(true));
        assertThat(result.contains("\"withdrawal\":0"), is(true));
    }

    @Test(expected = IllegalArgumentException.class)
    public void processStartAfterEndDate() throws Exception {

        DateTime time = new DateTime();

        message.setHeader("start", "" + time.toDate().getTime());
        message.setHeader("end", "" + time.minusWeeks(1).toDate().getTime());

        // call method
        processor.process(exchange);
    }

    @Test(expected = IllegalArgumentException.class)
    public void processStartDateNotNumber() throws Exception {

        DateTime time = new DateTime();

        message.setHeader("start", "hello");

        // call method
        processor.process(exchange);
    }

    @Test(expected = IllegalArgumentException.class)
    public void processEndDateNotNumber() throws Exception {

        DateTime time = new DateTime();

        message.setHeader("end", "hello");

        // call method
        processor.process(exchange);
    }

}
