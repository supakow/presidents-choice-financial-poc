package com.backbase.launchpad.services.pfm.transactioncategory;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Unit test for {@link InMemoryTransactionCategoryRepository}
 */
public class InMemoryTransactionCategoryRepositoryTest {

    // under test
    private InMemoryTransactionCategoryRepository repository;

    @Test
    public void findAll() {
        repository = new InMemoryTransactionCategoryRepository();
        assertEquals(repository.findAll("3").size(), 19);
        assertEquals(repository.findAll("2").size(), 19);
    }

    @Test
    public void findOne() {
        repository = new InMemoryTransactionCategoryRepository();
        assertNotNull(repository.findOne("3", "a1fae863-01e9-4f73-81a6-8d4e26c00f9d"));
    }

    @Test
    public void save() {
        repository = new InMemoryTransactionCategoryRepository();
        assertEquals(repository.findAll("2").size(), 19);
        assertEquals(repository.findAll("3").size(), 19);
        repository.save("3", new TransactionCategory());
        assertEquals(repository.findAll("3").size(), 20);
        assertEquals(repository.findAll("2").size(), 19);
    }

    @Test
    public void delete() {
        repository = new InMemoryTransactionCategoryRepository();
        assertEquals(repository.findAll("2").size(), 19);
        assertEquals(repository.findAll("3").size(), 19);
        repository.delete("3", "a1fae863-01e9-4f73-81a6-8d4e26c00f9d");
        assertEquals(repository.findAll("3").size(), 18);
        assertEquals(repository.findAll("2").size(), 19);
    }

    @Test
    public void exists() {
        repository = new InMemoryTransactionCategoryRepository();
        assertFalse(repository.exists("2", "dummy-category-name"));
        assertTrue(repository.exists("2", "Diningout"));
    }



}
