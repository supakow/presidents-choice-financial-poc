package com.backbase.launchpad.services.pfm.transactioncategory;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.test.junit4.CamelSpringTestSupport;
import org.junit.Test;
import org.restlet.data.Method;
import org.springframework.context.support.AbstractXmlApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.HashMap;
import java.util.Map;

public class TransactionCategoryRouteTest extends CamelSpringTestSupport {

    @Test
    public void transactionCategories() {
        Map<String, Object> headers = new HashMap<String, Object>();
        headers.put(Exchange.HTTP_METHOD, Method.GET);

        // Object response = template.requestBodyAndHeaders(TransactionCategoryRoute.TRANSACTION_CATEGORY_COLLECTION_ENDPOINT, "", headers);
    }

    @Override
    protected AbstractXmlApplicationContext createApplicationContext() {
        return new ClassPathXmlApplicationContext(new String[]
                {"META-INF/spring/backbase-mashup-service.xml"});
    }
}
