package com.backbase.launchpad.services.pfm.transactioncategory;

import java.util.List;

/**
 * Repository interface for {@link TransactionCategory}.
 */
public interface TransactionCategoryRepository {

    List<TransactionCategory> findAll(String customerId);

    TransactionCategory findOne(String customerId, String categoryId);

    TransactionCategory save(String customerId, TransactionCategory category);

    void delete(String customerId, String categoryId);

    boolean exists(String customerId, String categoryName);
}
