package com.backbase.launchpad.services.pfm.transactioncategory;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * Utilities for {@link TransactionCategoryCollectionProcessor} and
 * {@link TransactionCategoryInstanceProcessor} processors.
 */
public class TransactionCategoryProcessor {

    protected JSONObject render(TransactionCategory transactionCategory) {
        JSONObject category = new JSONObject();
        category.put("id", transactionCategory.getId());
        category.put("name", transactionCategory.getName());
        category.put("color", transactionCategory.getColor());
        category.put("type", transactionCategory.getType().toString());
        category.put("subcategories", new JSONArray()); // currently we don't include subcategories
        return category;
    }

    protected JSONObject getPayloadOrSendError(Message in) throws InvalidTransactionCategoryPayloadException {
        JSONObject payload;
        String bodyAsString = in.getBody(String.class);
        try {
            if (bodyAsString == null) {
                throw new ParseException(-1);
            }
            Object parsedData = new JSONParser().parse(bodyAsString);
            if (!(parsedData instanceof JSONObject)) {
                throw new ParseException(-1);
            }
            payload = (JSONObject) parsedData;
        } catch (ParseException e) {
            throw new InvalidTransactionCategoryPayloadException("Payload is not properly formatted JSON");
        }
        return payload;
    }

    protected void setErrorMessage(Message out, String message, int code) {
        out.setBody("{\"error\": \"" + message + "\"}");
        out.setHeader(Exchange.HTTP_RESPONSE_CODE, code);
    }

}
