package com.backbase.launchpad.services.pfm.transactioncategory;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Camel route handling managing transaction categories.
 */
@Component
public class TransactionCategoryRoute extends RouteBuilder {

    // Main endpoints
    public static final String TRANSACTION_CATEGORY_COLLECTION_ENDPOINT = "seda:provider/pfm/transaction-categories";
    public static final String TRANSACTION_CATEGORY_INSTANCE_ENDPOINT = "seda:provider/pfm/transaction-categories/categoryId";

    @Autowired
    private TransactionCategoryCollectionProcessor transactionCategoryCollectionProcessor;

    @Autowired
    private TransactionCategoryInstanceProcessor transactionCategoryInstanceProcessor;

    @Override
    public void configure() throws Exception {
        from(TRANSACTION_CATEGORY_COLLECTION_ENDPOINT)
                .process(transactionCategoryCollectionProcessor);

        from(TRANSACTION_CATEGORY_INSTANCE_ENDPOINT).
                process(transactionCategoryInstanceProcessor);
    }
}
