package com.backbase.launchpad.services.pfm.transactioncategory;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Camel processor handling the /transaction-categories/{categoryId} instance endpoint.
 */
@Component
public class TransactionCategoryInstanceProcessor extends TransactionCategoryProcessor implements Processor {

    @Autowired
    private TransactionCategoryRepository transactionCategoryRepository;

    @Override
    public void process(Exchange exchange) throws Exception {
        Message in = exchange.getIn();
        Message out = exchange.getOut();
        Object header = in.getHeader(Exchange.HTTP_METHOD);
        String customerId = (String) in.getHeader("backbasepartyId");
        if ("GET".equals(header)) {
            handleGet(customerId, in, out);
        } else if ("DELETE".equals(header)) {
            handleDELETE(customerId, in, out);
        } else if ("PUT".equals(header)) {
            handlePUT(customerId, in, out);
        } else {
            setErrorMessage(out, "GET, DELETE, PATCH and PUT are supported on transaction-categories instance endpoint", 405);
        }
    }

    private void handlePUT(String customerId, Message in, Message out) {
        TransactionCategory category = transactionCategoryRepository.findOne(customerId, getCategoryId(in));
        if (category == null) {
            setErrorMessage(out, "transaction category not found", 404);
        } else {
            try {
                JSONObject parsedCategory = getPayloadOrSendError(in);
                String name = (String) parsedCategory.get("name");
                if (name != null) {
                    category.setName(name);
                }
                String color = (String) parsedCategory.get("color");
                if (color != null) {
                    category.setColor(color);
                }
                transactionCategoryRepository.save(customerId, category);
            } catch (InvalidTransactionCategoryPayloadException e) {
                setErrorMessage(out, e.getMessage(), 400);
            }
        }
    }

    private void handleDELETE(String customerId, Message in, Message out) {
        TransactionCategory category = transactionCategoryRepository.findOne(customerId, getCategoryId(in));
        if (category == null) {
            setErrorMessage(out, "transaction category not found", 404);
        } else {
            transactionCategoryRepository.delete(customerId, getCategoryId(in));
            out.getExchange().getOut().setHeader(Exchange.HTTP_RESPONSE_CODE, 204);
        }
    }

    private void handleGet(String customerId, Message in, Message out) {
        TransactionCategory category = transactionCategoryRepository.findOne(customerId, getCategoryId(in));
        if (category == null) {
            setErrorMessage(out, "transaction category not found", 404);
        } else {
            out.setBody(render(category));
        }
    }

    private String getCategoryId(Message in) {
        return in.getHeader("categoryId", String.class);
    }


}
