package com.backbase.launchpad.services.pfm.transactioncategory;

import org.springframework.stereotype.Component;

import java.util.*;

import static com.backbase.launchpad.services.pfm.transactioncategory.TransactionCategory.Type.CONTROL;
import static com.backbase.launchpad.services.pfm.transactioncategory.TransactionCategory.Type.NORMAL;
import static com.backbase.launchpad.services.pfm.transactioncategory.TransactionCategory.Type.TEMPORARY;

/**
 * In-memory implementation of {@link TransactionCategoryRepository}
 */
@Component
public class InMemoryTransactionCategoryRepository implements TransactionCategoryRepository {

    private Map<String, LinkedHashMap<String, TransactionCategory>> categories = new HashMap<String, LinkedHashMap<String, TransactionCategory>>();

    public InMemoryTransactionCategoryRepository() {
        populate();
    }

    @Override
    public List<TransactionCategory> findAll(String customerId) {
        LinkedHashMap<String, TransactionCategory> customerCategories = categories.get(customerId);
        return new ArrayList(customerCategories.values());
    }

    @Override
    public TransactionCategory findOne(String customerId, String categoryId) {
        return categories.get(customerId).get(categoryId);
    }

    @Override
    public TransactionCategory save(String customerId, TransactionCategory category) {
        if (category.getId() != null) {
            // update
            categories.get(customerId).put(category.getId(), category);
            return category;
        } else {
            // save
            String categoryId = UUID.randomUUID().toString();
            TransactionCategory newCategory = new TransactionCategory(categoryId, category.getName(), category.getColor());
            categories.get(customerId).put(categoryId, newCategory);
            return newCategory;
        }
    }

    @Override
    public void delete(String customerId, String categoryId) {
        categories.get(customerId).remove(categoryId);
    }

    @Override
    public boolean exists(String customerId, String categoryName) {
        Collection<TransactionCategory> customerCategories = categories.get(customerId).values();
        for (TransactionCategory category : customerCategories) {
            if (category.getName().equals(categoryName)) {
                return true;
            }
        }
        return false;
    }

    private void populate() {
        LinkedHashMap<String, TransactionCategory> defaultCategories = new LinkedHashMap<String, TransactionCategory>();
        defaultCategories.put("a1fae863-01e9-4f73-81a6-8d4e26c00f9d",
                new TransactionCategory("a1fae863-01e9-4f73-81a6-8d4e26c00f9d", "Pending", "#ffffff", TEMPORARY));
        defaultCategories.put("388d76aa-a3e4-4bb0-b77a-f435edfdf149",
                new TransactionCategory("388d76aa-a3e4-4bb0-b77a-f435edfdf149", "Excluded", "#ff6262", CONTROL));
        defaultCategories.put("00cc9919-ba0c-4702-917b-1fba4c256b4d",
                new TransactionCategory("00cc9919-ba0c-4702-917b-1fba4c256b4d", "Uncategorised", "#c0c0c0", CONTROL));
        defaultCategories.put("3df82ab4-367d-44ad-b7fd-8fd1198d4ab5",
                new TransactionCategory("3df82ab4-367d-44ad-b7fd-8fd1198d4ab5", "Clothing", "#ffc03e", NORMAL));
        defaultCategories.put("b1e0a904-916b-4c8c-a4c8-50c9777fccfc",
                new TransactionCategory("b1e0a904-916b-4c8c-a4c8-50c9777fccfc", "Education", "#66bb66", NORMAL));
        defaultCategories.put("191dfb1e-e4e5-4a12-ac57-321d0c6751f6",
                new TransactionCategory("191dfb1e-e4e5-4a12-ac57-321d0c6751f6", "Entertainment", "#fffb00", NORMAL));
        defaultCategories.put("17d32325-6061-486d-9a13-2671d64f0804",
                new TransactionCategory("17d32325-6061-486d-9a13-2671d64f0804", "Diningout", "#7dbfff", NORMAL));
        defaultCategories.put("088654a9-4d2d-4233-a93f-8425df0c9fd1",
                new TransactionCategory("088654a9-4d2d-4233-a93f-8425df0c9fd1", "Groceries", "#00fa92", NORMAL));
        defaultCategories.put("9ab60271-b6bd-42e8-b53a-1e5118a0622f",
                new TransactionCategory("9ab60271-b6bd-42e8-b53a-1e5118a0622f", "Fees", "#ff85ff", NORMAL));
        defaultCategories.put("ef512669-7c35-4943-81cb-16ad9a82e907",
                new TransactionCategory("ef512669-7c35-4943-81cb-16ad9a82e907", "Health", "#1bbf00", NORMAL));
        defaultCategories.put("bfd8200d-796d-4b6a-9995-e72bd51f3ff6",
                new TransactionCategory("bfd8200d-796d-4b6a-9995-e72bd51f3ff6", "Home", "#3dbf11", NORMAL));
        defaultCategories.put("6bba7249-98ae-432d-85ee-cbba8731d38f",
                new TransactionCategory("6bba7249-98ae-432d-85ee-cbba8731d38f", "Income", "#008f00", NORMAL));
        defaultCategories.put("6387bc51-29a3-4982-8db7-88f7008d4f63",
                new TransactionCategory("6387bc51-29a3-4982-8db7-88f7008d4f63", "Personal", "#6dafee", NORMAL));
        defaultCategories.put("fc9c910b-bd5d-4705-9817-8fa81d4fd930",
                new TransactionCategory("fc9c910b-bd5d-4705-9817-8fa81d4fd930", "Payment", "#7dbf22", NORMAL));
        defaultCategories.put("45ec13cd-c82c-4cc1-afc5-fa6a356fc8ec",
                new TransactionCategory("45ec13cd-c82c-4cc1-afc5-fa6a356fc8ec", "Savings", "#945200", NORMAL));
        defaultCategories.put("4ad5d251-62d0-4ace-830d-6a96cd77605a",
                new TransactionCategory("4ad5d251-62d0-4ace-830d-6a96cd77605a", "Transfer", "#5e5e5e", NORMAL));
        defaultCategories.put("3455dedd-7bc5-48e4-b01f-ba8b74edd057",
                new TransactionCategory("3455dedd-7bc5-48e4-b01f-ba8b74edd057", "Transportation", "#0096ff", NORMAL));
        defaultCategories.put("febeafd9-67c3-4299-b740-c4d8bb62065d",
                new TransactionCategory("febeafd9-67c3-4299-b740-c4d8bb62065d", "Travel", "#ff9300", NORMAL));
        defaultCategories.put("95bca6f4-d332-4899-b290-a20d406f1496",
                new TransactionCategory("95bca6f4-d332-4899-b290-a20d406f1496", "Utilities", "#9437ff", NORMAL));
        categories.put("2", defaultCategories);
        categories.put("3", new LinkedHashMap<String, TransactionCategory>(defaultCategories));
        categories.put("4", new LinkedHashMap<String, TransactionCategory>(defaultCategories));
    }

}
