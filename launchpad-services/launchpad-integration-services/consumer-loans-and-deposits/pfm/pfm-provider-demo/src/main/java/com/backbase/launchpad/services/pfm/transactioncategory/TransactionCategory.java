package com.backbase.launchpad.services.pfm.transactioncategory;

import java.util.Collections;
import java.util.List;

import static com.backbase.launchpad.services.pfm.transactioncategory.TransactionCategory.Type.NORMAL;

/**
 * Model for a transaction category. One transaction can belong to exactly one category.
 */
public class TransactionCategory {

    private String id;
    private String name;
    private String color;
    private Type type;
    // currently categories have no subcategories
    private List<TransactionCategory> subcategories = Collections.EMPTY_LIST;

    public TransactionCategory(String id, String name, String color, Type type) {
        this.id = id;
        this.name = name;
        this.color = color;
        this.type = type;
    }

    public TransactionCategory(String id, String name, String color) {
        this.id = id;
        this.name = name;
        this.color = color;
        this.type = NORMAL;
    }

    public TransactionCategory() {
    }

    public Type getType() {
        return type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public List<TransactionCategory> getSubcategories() {
        return subcategories;
    }

    public void addSubcategory(TransactionCategory category) {
        throw new UnsupportedOperationException("currently subcategories are not supported");
    }

    public static enum Type {
        TEMPORARY, CONTROL, NORMAL
    }
}
