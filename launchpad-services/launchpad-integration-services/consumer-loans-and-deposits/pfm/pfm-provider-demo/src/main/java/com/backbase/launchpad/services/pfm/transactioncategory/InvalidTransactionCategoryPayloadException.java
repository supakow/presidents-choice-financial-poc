package com.backbase.launchpad.services.pfm.transactioncategory;

/**
 * Exception signaling when receiving an invalid {@link TransactionCategory} as a payload for the endpoints
 * POST   pfm/transaction-categories
 * PUT    pfm/transaction-categories/{categoryId}
 */
public class InvalidTransactionCategoryPayloadException extends Exception {

    public InvalidTransactionCategoryPayloadException(String message) {
        super(message);
    }
}
