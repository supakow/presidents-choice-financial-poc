package com.backbase.launchpad.services.pfm.transactioncategory;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Camel processor handling the /transaction-categories collection endpoint.
 */
@Component
public class TransactionCategoryCollectionProcessor extends TransactionCategoryProcessor implements Processor {

    @Autowired
    private TransactionCategoryRepository transactionCategoryRepository;

    @Override
    public void process(Exchange exchange) throws Exception {

        Message in = exchange.getIn();
        Message out = exchange.getOut();
        Object header = in.getHeader(Exchange.HTTP_METHOD);
        String customerId = (String) in.getHeader("backbasepartyId");
        if ("GET".equals(header)) {
            handleGet(customerId, out);
        } else if ("POST".equals(header)) {
            handlePOST(customerId, in, out);
        } else {
            setErrorMessage(out, "Only GET and POST is supported on transaction-categories collection endpoint.", 405);
        }
    }

    private void handlePOST(String customerId, Message in, Message out) {
        try {
            JSONObject parsedCategory = getPayloadOrSendError(in);
            TransactionCategory category = new TransactionCategory();
            String name = (String) parsedCategory.get("name");
            String color = (String) parsedCategory.get("color");
            if (color == null || name == null) {
                setErrorMessage(out, "'name' and 'color' fields are mandatory to create a transaction category", 400);
            } else {
                if (transactionCategoryRepository.exists(customerId, name)) {
                    setErrorMessage(out, "transaction category already exists", 409);
                } else {
                    category.setColor(color);
                    category.setName(name);
                    TransactionCategory savedCategory = transactionCategoryRepository.save(customerId, category);
                    out.setBody(render(savedCategory));
                    out.getExchange().getOut().setHeader(Exchange.HTTP_RESPONSE_CODE, 201);
                }
            }
        } catch (InvalidTransactionCategoryPayloadException e) {
            setErrorMessage(out, e.getMessage(), 400);
        }
    }

    private void handleGet(String customerId, Message out) {
        JSONArray response = new JSONArray();
        List<TransactionCategory> transactionCategories = transactionCategoryRepository.findAll(customerId);
        for (TransactionCategory transactionCategory : transactionCategories) {
            response.add(render(transactionCategory));
        }
        out.setBody(response);
    }

}
