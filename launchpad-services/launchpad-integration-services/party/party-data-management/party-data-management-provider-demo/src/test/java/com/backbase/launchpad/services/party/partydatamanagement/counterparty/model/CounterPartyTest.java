package com.backbase.launchpad.services.party.partydatamanagement.counterparty.model;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.fail;

import org.junit.Test;

public class CounterPartyTest {

	@Test
	public void idIsRequired() {
		CounterParty victim = createVictim();
		victim.setId(null);
		testRequired(victim, "id");
	}

	@Test
	public void partyIdIsRequired() {
		CounterParty victim = createVictim();
		victim.setPartyId(null);
		testRequired(victim, "partyId");
	}

	@Test
	public void nameIsRequired() {
		CounterParty victim = createVictim();
		victim.setName(null);
		testRequired(victim, "name");
	}

	@Test
	public void photoUrlIsRequired() {
		CounterParty victim = createVictim();
		victim.setPhotoUrl(null);
		testRequired(victim, "photoUrl");
	}


	@Test
	public void accountIsRequired() {
		CounterParty victim = createVictim();
		victim.setAccount(null);
		testRequired(victim, "account");
	}

	private void testRequired(CounterParty victim, String fieldName) {
		try {
			victim.validate();
			fail(fieldName + " is required. Should fail");
		} catch (InvalidCounterPartyInputException e) {
			assertEquals(fieldName + " must be specified", e.getMessage());
		}
	}

	private CounterParty createVictim() {
		CounterParty victim = new CounterParty();
		victim.setId("1");
		victim.setPartyId("12");
		victim.setName("My Name");
		victim.setPhotoUrl("+1233121");
		victim.setAccount("1344445");

        return victim;
	}
}
