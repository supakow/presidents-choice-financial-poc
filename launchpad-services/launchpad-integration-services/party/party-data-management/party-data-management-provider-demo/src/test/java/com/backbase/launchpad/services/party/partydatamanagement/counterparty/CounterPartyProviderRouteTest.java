package com.backbase.launchpad.services.party.partydatamanagement.counterparty;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.test.junit4.CamelSpringTestSupport;
import org.junit.Before;
import org.junit.Test;
import org.restlet.data.Method;
import org.springframework.context.support.AbstractXmlApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.xmldb.api.DatabaseManager;
import org.xmldb.api.base.Collection;
import org.xmldb.api.modules.CollectionManagementService;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

public class CounterPartyProviderRouteTest extends CamelSpringTestSupport {
    @Override
    public boolean isCreateCamelContextPerClass() {
        return true;
    }

    @Test
    public void retrieveCounterParties() {
        StringBuilder counterParty = loadDefaultCounterParty(UUID.randomUUID().toString());

        Map<String, Object> headers = new HashMap<String, Object>();
        headers.put(Exchange.HTTP_METHOD, Method.GET);
        headers.put("backbasepartyid", "3");

        counterParty.insert(0, "[");
        counterParty.append("]");

        String party = counterParty.toString();
        Object response = template.requestBodyAndHeaders(CounterPartyProviderRoute.COUNTER_PARTIES_EP, "", headers);
        assertEquals(party, response);
    }

    @Test
    public void addCounterParty() {
        String id = UUID.randomUUID().toString();
        LinkedHashMap<String, Object> data = populateData(id);

        Map<String, Object> headers = new HashMap<String, Object>();
        headers.put(Exchange.HTTP_METHOD, Method.POST);
        headers.put("backbasepartyid", "3");

        for (String field : data.keySet()) {
            headers.put(field, data.get(field));
        }

        Object response = template.requestBodyAndHeaders(CounterPartyProviderRoute.COUNTER_PARTY_EP, "", headers);
        assertEquals("{\"status\":\"OK\"}", response);
    }

    @Test
    public void updateCounterParty() {
        String id = UUID.randomUUID().toString();

        // --updated
        LinkedHashMap<String, Object> data = populateData(id);
        data.put("name", "Updated");

        Map<String, Object> headers = new HashMap<String, Object>();
        headers.put(Exchange.HTTP_METHOD, Method.PUT);
        for (String field : data.keySet()) {
            headers.put(field, data.get(field));
        }

        Object response = template.requestBodyAndHeaders(CounterPartyProviderRoute.COUNTER_PARTY_EP, "", headers);
        assertEquals("{\"updateStatus\": \"OK\"}", response);
    }

    @Test
    public void deactivateCounterParty() {
        String id = UUID.randomUUID().toString();
        loadDefaultCounterParty(id);

        Map<String, Object> headers = new HashMap<String, Object>();
        headers.put(Exchange.HTTP_METHOD, Method.DELETE);
        headers.put("id", id);

        Object response = template.requestBodyAndHeaders(CounterPartyProviderRoute.COUNTER_PARTY_EP, "", headers);
        assertEquals(Long.valueOf(1), response);
    }

    @Test
    public void retrieveCounterPartyById() {
        String id = UUID.randomUUID().toString();
        StringBuilder counterParty = loadDefaultCounterParty(id);

        Map<String, Object> headers = new HashMap<String, Object>();
        headers.put(Exchange.HTTP_METHOD, Method.GET);
        headers.put("id", id);
        headers.put("backbasepartyid", "3");

        Object response = template.requestBodyAndHeaders(CounterPartyProviderRoute.COUNTER_PARTY_EP, "", headers);
        assertEquals(counterParty.toString(), response);
    }

    @Override
    protected CamelContext createCamelContext() throws Exception {
        CamelContext camelContext = super.createCamelContext();

        RouteBuilder dataPreLoadRoute = new RouteBuilder() {
            @Override
            public void configure() throws Exception {
                from("direct:loadData")
                        .recipientList().simple("${header.XMLDBURL}");
            }
        };

        dataPreLoadRoute.addRoutesToCamelContext(camelContext);

        return camelContext;
    }

    private StringBuilder loadDefaultCounterParty(String id) {
        LinkedHashMap<String, Object> data = populateData(id);
        StringBuilder counterParty = constructJSONRepresentation(data);
        StringBuilder counterPartyXml = constructXMLRepresentation(data);

        template.requestBodyAndHeader("direct:loadData", counterPartyXml.toString(), "XMLDBURL", CounterPartyProviderRoute.XMLDB_STORE_COUNTER_PARTY_EP);

        return counterParty;
    }

    private LinkedHashMap<String, Object> populateData(String id) {
        LinkedHashMap<String, Object> data = new LinkedHashMap<String, Object>();
        data.put("id", id);
        data.put("account", "54999939");
        data.put("name", "Меир Абергиль");
        data.put("partyId", "3");
        data.put("photoUrl", "http://www.dummy.org/myPhoto");
        data.put("active", "true");

        return data;
    }

    private StringBuilder constructXMLRepresentation(LinkedHashMap<String, Object> data) {
        StringBuilder counterPartyXml = new StringBuilder();
        counterPartyXml.append("<o>");
        for (String field : data.keySet()) {
            Object v = data.get(field);

            counterPartyXml.append("<"+field+">"+ v +"</"+field+">");
        }
        counterPartyXml.append("</o>");
        return counterPartyXml;
    }

    private StringBuilder constructJSONRepresentation(LinkedHashMap<String, Object> data) {
        StringBuilder counterParty = new StringBuilder();
        counterParty.append("{");

        int nFields = data.size();
        int i = 0;
        for (String field : data.keySet()) {
            Object v = data.get(field);

            String value = null;
            if (v != null) {
                value = (v instanceof String) ? "\"" + v + "\"": v.toString();
            }

            counterParty.append("\""+field+"\":"+ value);

            if (nFields - i > 1) {
                counterParty.append(",");
            }

            i++;
        }
        counterParty.append("}");
        return counterParty;
    }

    @Override
    protected AbstractXmlApplicationContext createApplicationContext() {
        return new ClassPathXmlApplicationContext(new String[]
                {"META-INF/spring/backbase-mashup-service.xml"});
    }

    @Override
    protected RouteBuilder createRouteBuilder() throws Exception {
        return getMandatoryBean(CounterPartyProviderRoute.class, "counterPartyProviderRoute");
    }

    @Before
    public void cleanUpDatabase() {
        try {
            Collection parentCollection = DatabaseManager.getCollection("xmldb:exist:///db", "admin", "");
            CollectionManagementService mgt = (CollectionManagementService) parentCollection.getService("CollectionManagementService", "1.0");

            mgt.removeCollection("launchpad");
            parentCollection.close();
        } catch (Exception ex) {
            //IGNORE
        }
    }
}
