package com.backbase.launchpad.services.party.partydatamanagement.counterparty;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.test.junit4.CamelSpringTestSupport;
import org.junit.Before;
import org.junit.Test;
import org.restlet.data.Method;
import org.springframework.context.support.AbstractXmlApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.xmldb.api.DatabaseManager;
import org.xmldb.api.base.Collection;
import org.xmldb.api.modules.CollectionManagementService;

public class CounterPartyDetailsProviderRouteTest extends CamelSpringTestSupport {
    @Override
    public boolean isCreateCamelContextPerClass() {
        return true;
    }

    @Test
    public void addCounterPartyDetails() {
        String id = UUID.randomUUID().toString();
        Map<String, Object> data = populateData(id);

        Map<String, Object> headers = new HashMap<String, Object>();
        headers.put(Exchange.HTTP_METHOD, Method.POST);
        for (String field : data.keySet()) {
            headers.put(field, "" + data.get(field));
        }

        Object response = template.requestBodyAndHeaders(CounterPartyDetailsProviderRoute.COUNTER_PARTY_DETAILS_EP, "", headers);
        assertEquals("{\"status\":\"OK\"}", response);
    }

    @Test
    public void updateCounterParty() {
        String id = UUID.randomUUID().toString();

        // --updated
        Map<String, Object> data = populateData(id);
        data.put("address", "Updated");
        data.put("city", " ");
        data.put("phone", " ");
        data.put("dateOfBirth", " ");
        data.put("email", " ");
        data.put("address", " ");
        data.put("state", " ");

        Map<String, Object> headers = new HashMap<String, Object>();
        headers.put(Exchange.HTTP_METHOD, Method.PUT);

        for (String field : data.keySet()) {
            headers.put(field, "" + data.get(field));
        }

        Object response = template.requestBodyAndHeaders(CounterPartyDetailsProviderRoute.COUNTER_PARTY_DETAILS_EP, "", headers);
        assertEquals("{\"updateStatus\": \"OK\"}", response);
    }

    @Test
    public void retrieveCounterPartyById() {
        String id = UUID.randomUUID().toString();
        StringBuilder counterParty = loadDefaultCounterParty(id);

        Map<String, Object> headers = new HashMap<String, Object>();
        headers.put(Exchange.HTTP_METHOD, Method.GET);
        headers.put("id", id);

        Object response = template.requestBodyAndHeaders(CounterPartyDetailsProviderRoute.COUNTER_PARTY_DETAILS_EP, "", headers);
        assertEquals(counterParty.toString(), response.toString());
    }

    @Override
    protected CamelContext createCamelContext() throws Exception {
        CamelContext camelContext = super.createCamelContext();

        RouteBuilder dataPreLoadRoute = new RouteBuilder() {
            @Override
            public void configure() throws Exception {
                from("direct:loadData")
                        .recipientList().simple("${header.XMLDBURL}");
            }
        };

        dataPreLoadRoute.addRoutesToCamelContext(camelContext);

        return camelContext;
    }

    private StringBuilder loadDefaultCounterParty(String id) {
        Map<String, Object> data = populateData(id);
        StringBuilder counterParty = constructJSONRepresentation(data);
        StringBuilder counterPartyXml = constructXMLRepresentation(data);

        template.requestBodyAndHeader("direct:loadData", counterPartyXml.toString(), "XMLDBURL", CounterPartyDetailsProviderRoute.XMLDB_STORE_COUNTER_PARTY_DETAILS_EP);

        return counterParty;
    }

    private Map<String, Object> populateData(String id) {
        LinkedHashMap<String, Object> data = new LinkedHashMap<String, Object>();
        data.put("id", id);
        data.put("address", "Address");
        data.put("city", "City");
        data.put("state", "NY");
        data.put("phone", "+171644555448");
        data.put("email", "e@mail.com");
        data.put("dateOfBirth", 1.38365973E12);

        return data;
    }

    private StringBuilder constructXMLRepresentation(Map<String, Object> data) {

        Map<String, String> metaData = CounterPartyDetailsMetaDataProcessor.metaData();

        StringBuilder counterPartyXml = new StringBuilder();
        counterPartyXml.append("<o>");
        for (String field : data.keySet()) {
            String type = metaData.get(field);
            Object v = data.get(field);

            counterPartyXml.append("<"+field+" type=\""+type+"\">"+ v +"</"+field+">");
        }
        counterPartyXml.append("</o>");

        return counterPartyXml;
    }

    private StringBuilder constructJSONRepresentation(Map<String, Object> data) {
        StringBuilder counterParty = new StringBuilder();
        counterParty.append("{");

        int nFields = data.size();
        int i = 0;
        for (String field : data.keySet()) {
            Object v = data.get(field);

            String value = null;
            if (v != null) {
                value = (v instanceof String) ? "\"" + v + "\"": v.toString();
            }

            counterParty.append("\""+field+"\":"+ value);

            if (nFields - i > 1) {
                counterParty.append(",");
            }

            i++;
        }
        counterParty.append("}");
        return counterParty;
    }

    @Override
    protected AbstractXmlApplicationContext createApplicationContext() {
        return new ClassPathXmlApplicationContext(new String[]
                {"META-INF/spring/backbase-mashup-service.xml"});
    }

    @Override
    protected RouteBuilder createRouteBuilder() throws Exception {
        return getMandatoryBean(CounterPartyDetailsProviderRoute.class, "counterPartyDetailsProviderRoute");
    }

    private String bytesToString(Object response) {
        return new String((byte[]) response);
    }

    @Before
    public void cleanUpDatabase() {
        try {
            Collection parentCollection = DatabaseManager.getCollection("xmldb:exist:///db", "admin", "");
            CollectionManagementService mgt = (CollectionManagementService) parentCollection.getService("CollectionManagementService", "1.0");

            mgt.removeCollection("launchpad");
            parentCollection.close();
        } catch (Exception ex) {
            //IGNORE
        }
    }
}
