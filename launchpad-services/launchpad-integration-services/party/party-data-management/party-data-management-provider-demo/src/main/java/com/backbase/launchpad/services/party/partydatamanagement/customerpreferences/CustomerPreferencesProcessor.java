package com.backbase.launchpad.services.party.partydatamanagement.customerpreferences;

import net.sf.json.JSONObject;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;

/**
 * Camel processor for creating the response for retrieving the customer preferences.
 */
public class CustomerPreferencesProcessor implements Processor {

    private static final String PREFERRED_NAME = "preferredName";
    private static final String PFM_ENABLED = "pfmEnabled";

    @Override
    public void process(Exchange exchange) throws Exception {
        Message in = exchange.getIn();

        JSONObject response = new JSONObject();
        response.put(PREFERRED_NAME, in.getHeader(PREFERRED_NAME, String.class));
        response.put(PFM_ENABLED, in.getHeader(PFM_ENABLED, String.class));

        exchange.getOut().setBody(response.toString());
    }
}
