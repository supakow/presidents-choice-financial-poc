package com.backbase.launchpad.services.party.partydatamanagement.counterparty.simple;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CounterpartyRoute extends RouteBuilder {

    private final String COUNTERPARTIES_ENDPOINT = "seda:provider/counterparties";

    @Autowired
    private CounterpartyProcessor counterpartyProcessor;

    @Override
    public void configure() throws Exception {
        from(COUNTERPARTIES_ENDPOINT).process(counterpartyProcessor);
    }
}
