package com.backbase.launchpad.services.party.partydatamanagement.customerpreferences;

import org.apache.camel.*;
import org.apache.camel.builder.RouteBuilder;
import org.restlet.data.Method;
import org.springframework.stereotype.Component;

import static org.apache.camel.builder.PredicateBuilder.isNotNull;
import static org.apache.camel.builder.PredicateBuilder.or;

/**
 * Camel {@link Route} for interaction with customer preferences.
 */
@Component
public class CustomerPreferencesRoute extends RouteBuilder {

    private static final String XMLDB_SERVICE_TYPE = "XMLDB_SERVICE_TYPE";

    // main endpoints
    @EndpointInject(uri = "seda:provider/party/party-data-management/preference")
    private Endpoint customerPreferencesEndpoint;

    // internal endpoints
    private static final String CUSTOMER_ENDPOINT_URI = "xmldb:exist:///db/launchpad/customers?XPath=//customer[id = '${header.backbasepartyid}']";

    @EndpointInject(uri = "xmldb:exist:///db/launchpad/customers?username=admin&password=")
    private Endpoint storeToDBEndpoint;

    @EndpointInject(uri = "velocity:updatePreferredNameXQuery.vm")
    private Endpoint updatePreferredNameXQueryEndpoint;

    @EndpointInject(uri = "velocity:updatePFMEnabledXQuery.vm")
    private Endpoint updatePFMEnabledXQueryEndpoint;

    @EndpointInject(uri = "direct:retrievePreference")
    private Endpoint retrievePreferenceEndpoint;

    @EndpointInject(uri = "direct:updatePreference")
    private Endpoint updatePreferenceEndpoint;

    @EndpointInject(uri = "direct:storePreferenceEndpoint")
    private Endpoint storePreferenceEndpoint;

    @Override
    public void configure() throws Exception {

        Predicate isUpdate = header(Exchange.HTTP_METHOD).isEqualTo(Method.PUT);
        Predicate isRetrieve = header(Exchange.HTTP_METHOD).isEqualTo(Method.GET);

        Predicate isPreferredNameProvided = isNotNull(header("preferredName"));
        Predicate isPfmEnabledProvided = isNotNull(header("pfmEnabled"));
        Predicate isPfmEnabledBoolean = or(header("pfmEnabled").isEqualTo("true"), header("pfmEnabled").isEqualTo("false"));

        from(customerPreferencesEndpoint)
        .choice()
            .when(isUpdate).to(updatePreferenceEndpoint)
            .when(isRetrieve).to(retrievePreferenceEndpoint)
        .end();

        from(updatePreferenceEndpoint)
                .choice()
                    .when(isPreferredNameProvided)
                        .to(updatePreferredNameXQueryEndpoint)
                        .to(storePreferenceEndpoint)
                .end()
                .choice()
                    .when(isPfmEnabledProvided)
                        .choice()
                            .when(isPfmEnabledBoolean)
                                 .to(updatePFMEnabledXQueryEndpoint)
                                 .to(storePreferenceEndpoint)
                                 .setBody().simple("{ \"status\" : \"OK\"}")
                                 .log("'pfmEnabled' field value accepted")
                            .otherwise()
                                .setBody().simple("{ \"error\" : \"'pfmEnabled' should be a boolean value: 'true' or 'false'\"}")
                                .log("'pfmEnabled' field value is not accepted")
                        .end()
                .end();



        from(storePreferenceEndpoint)
                .setHeader(XMLDB_SERVICE_TYPE).constant("XQUERY")
                .to(storeToDBEndpoint);

        from(retrievePreferenceEndpoint)
            .recipientList().simple(CUSTOMER_ENDPOINT_URI)
            .setHeader("preferredName", xpath("/customer/preferredName/text()").stringResult())
            .setHeader("pfmEnabled", xpath("/customer/pfmEnabled/text()").stringResult())
            .process(new CustomerPreferencesProcessor());
    }
}
