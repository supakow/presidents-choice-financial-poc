package com.backbase.launchpad.services.party.partydatamanagement.counterparty.model;

public class InvalidCounterPartyInputException extends Exception {
	private static final long serialVersionUID = -3353849588597452143L;

	public InvalidCounterPartyInputException(String message) {
		super(message);
	}
}