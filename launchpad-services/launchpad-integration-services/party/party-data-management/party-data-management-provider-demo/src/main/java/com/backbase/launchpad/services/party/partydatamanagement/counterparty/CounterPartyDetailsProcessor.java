package com.backbase.launchpad.services.party.partydatamanagement.counterparty;

import com.backbase.launchpad.services.party.partydatamanagement.Constants;
import com.google.common.base.Strings;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.beanutils.*;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.*;

@Component
public class CounterPartyDetailsProcessor implements Processor {
    private static Map<String, Class> typesMap = new HashMap<String, Class>();

    static {
        typesMap.put("string", String.class);
        typesMap.put("number", BigDecimal.class);
    }

    public void process(Exchange exchange) throws Exception {
        Map<String, String> metaData = exchange.getProperty(Constants.META_DATA_PROPERTY_NAME, Map.class);
        DynaBean counterPartyDetails = createDynaBean(exchange, metaData);
        exchange.getIn().setBody(counterPartyDetails);
    }

    private DynaBean createDynaBean(Exchange exchange, Map<String, String> metaData) throws IllegalAccessException, InstantiationException {
        DynaProperty[] properties = populateDynaBeanProperties(metaData);

        DynaClass counterPartyDetailsClass =
                new BasicDynaClass("counterPartyDetails", null, properties);

        DynaBean counterPartyDetails = counterPartyDetailsClass.newInstance();
        setPropertiesValue(exchange, metaData, counterPartyDetails);

        return counterPartyDetails;
    }

    private void setPropertiesValue(Exchange exchange, Map<String, String> metaData, DynaBean counterPartyDetails) {
        counterPartyDetails.set("id", exchange.getIn().getHeader("id", String.class));

        for (String field : metaData.keySet()) {
            String type = metaData.get(field);
            Object value = exchange.getIn().getHeader(field, typesMap.get(type));

            counterPartyDetails.set(field, value);
        }
    }

    private DynaProperty[] populateDynaBeanProperties(Map<String, String> metaData) {
        List<DynaProperty> props = new ArrayList<DynaProperty>();
        props.add(new DynaProperty("id", String.class));

        for (String field : metaData.keySet()) {
            String type = metaData.get(field);

            props.add(new DynaProperty(field, typesMap.get(type)));
        }

        DynaProperty[] properties = new DynaProperty[props.size()];
        properties = props.toArray(properties);
        return properties;
    }
}
