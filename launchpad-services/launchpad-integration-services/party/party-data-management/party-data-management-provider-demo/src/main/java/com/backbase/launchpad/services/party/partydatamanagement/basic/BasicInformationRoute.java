package com.backbase.launchpad.services.party.partydatamanagement.basic;

import static com.backbase.launchpad.services.party.partydatamanagement.basic.BasicInformationConstants.CITIZENSHIP;
import static com.backbase.launchpad.services.party.partydatamanagement.basic.BasicInformationConstants.DATE_OF_BIRTH;
import static com.backbase.launchpad.services.party.partydatamanagement.basic.BasicInformationConstants.FIRST_NAME;
import static com.backbase.launchpad.services.party.partydatamanagement.basic.BasicInformationConstants.GENDER;
import static com.backbase.launchpad.services.party.partydatamanagement.basic.BasicInformationConstants.INITIATION_TIME;
import static com.backbase.launchpad.services.party.partydatamanagement.basic.BasicInformationConstants.LANGUAGE;
import static com.backbase.launchpad.services.party.partydatamanagement.basic.BasicInformationConstants.LAST_NAME;
import static com.backbase.launchpad.services.party.partydatamanagement.basic.BasicInformationConstants.NATIONAL_IDENTITY_NUMBER;
import static com.backbase.launchpad.services.party.partydatamanagement.basic.BasicInformationConstants.USERNAME;

import org.apache.camel.Endpoint;
import org.apache.camel.EndpointInject;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class BasicInformationRoute extends RouteBuilder {
    private static final String XMLDB_SERVICE_TYPE = "XMLDB_SERVICE_TYPE";

    // main endpoints
    @EndpointInject(uri = "seda:provider/party/party-data-management/party")
    private Endpoint customerInformationEndpoint;

    // internal endpoints
    @EndpointInject(uri = "velocity:retrieveInitiationTimeXQuery.vm")
    private Endpoint retrieveInitiationTimeQueryEndpoint;

    @EndpointInject(uri = "xmldb:exist:///db/launchpad/sessions?username=admin&password=")
    protected Endpoint sessionEntityEndpoint;

    private static final String CUSTOMER_ENDPOINT_URI = "xmldb:exist:///db/launchpad/customers?XPath=/customer[id = '${header.backbasepartyid}']";

    @Override
    public void configure() throws Exception {
        from(customerInformationEndpoint)
            .recipientList().simple(CUSTOMER_ENDPOINT_URI)
            .setHeader(USERNAME, xpath("/customer/username/text()").stringResult())
            .setHeader(FIRST_NAME, xpath("/customer/firstName/text()").stringResult())
            .setHeader(LAST_NAME, xpath("/customer/lastName/text()").stringResult())
            .setHeader(DATE_OF_BIRTH, xpath("/customer/dateOfBirth/text()").stringResult())
            .setHeader(GENDER, xpath("/customer/sex/text()").stringResult())
            .setHeader(CITIZENSHIP, xpath("/customer/citizenship/text()").stringResult())
            .setHeader(NATIONAL_IDENTITY_NUMBER, xpath("/customer/nationalIdentityNumber/text()").stringResult())
            .setHeader(LANGUAGE, xpath("/customer/language/text()").stringResult())
            .setHeader(XMLDB_SERVICE_TYPE, constant("XQUERY"))
            .to(retrieveInitiationTimeQueryEndpoint)
            .to(sessionEntityEndpoint)
            .setHeader(INITIATION_TIME, xpath("/initiationTime/text()").stringResult())
            .process(new BasicInformationProcessor())
            .removeHeaders("*");
    }
}
