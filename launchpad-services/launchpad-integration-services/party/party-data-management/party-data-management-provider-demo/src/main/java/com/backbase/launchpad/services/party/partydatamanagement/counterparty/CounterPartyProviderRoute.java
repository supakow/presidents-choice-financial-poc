package com.backbase.launchpad.services.party.partydatamanagement.counterparty;

import net.sf.json.JSON;

import org.apache.camel.Exchange;
import org.apache.camel.Predicate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.spi.IdempotentRepository;
import org.restlet.data.MediaType;
import org.restlet.data.Method;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.backbase.mashup.camel.json.XmlJsonDataFormatWrapper;

@Component
public class CounterPartyProviderRoute extends RouteBuilder {
    protected static final String COUNTER_PARTIES_EP = "seda:provider/party/party-data-management/counter-parties";
    protected static final String COUNTER_PARTY_EP = "seda:provider/party/party-data-management/counter-party";

    private static final String DIRECT_COUNTER_PARTY_EP = "direct:provider/party/party-data-management/counter-party";
    private static final String ADD_COUNTER_PARTY_EP = DIRECT_COUNTER_PARTY_EP
            + "/add";
    private static final String UPDATE_COUNTER_PARTY_EP = DIRECT_COUNTER_PARTY_EP
            + "/update";
    private static final String DEACTIVATE_COUNTER_PARTY_EP = DIRECT_COUNTER_PARTY_EP
            + "/deactivate";
    private static final String RETRIEVE_COUNTER_PARTY_EP = DIRECT_COUNTER_PARTY_EP
            + "/retrieve";

    private static final String UPDATE_COUNTER_PARTY_XUPDATE_EP = "velocity:updateCounterPartyXUpdate.vm";
    private static final String DEACTIVATE_COUNTER_PARTY_XQUERY_EP = "velocity:deactivateCounterPartyXQuery.vm";
    private static final String RETRIEVE_COUNTER_PARTIES_XQUERY_EP = "velocity:retrieveCounterPartiesXQuery.vm";

    private static final String XMLDB_DEFAULT_COLLECTION_EP = "xmldb:exist:///db/launchpad";
    private static final String XMLDB_COUNTER_PARTY_COLLECTION_URI = "xmldb:exist:///db/launchpad/counterParties";
    protected static final String XMLDB_STORE_COUNTER_PARTY_EP = XMLDB_COUNTER_PARTY_COLLECTION_URI
            + "?username=admin&password=";
    private static final String XMLDB_SELECT_COUNTER_PARTY_EP = XMLDB_COUNTER_PARTY_COLLECTION_URI
            + "?XPath=//o[id = '${header.id}' and active = 'true']";

    private static final String XMLDB_XQUERY_EXPRESSION = "XMLDB_XQUERY_EXPRESSION";
    private static final String XMLDB_XQUERY_SERVICE = "XMLDB_SERVICE_TYPE";

    @Autowired
    private InitiateCounterParty initiateCounterParty;

    @Autowired
    private XmlJsonDataFormatWrapper xmlJsonDataFormat;

    @Autowired
    private IdempotentRepository<String> counterpartyIdempotentRepository;

    public void configure() throws Exception {
        counterPartiesContentBasedRouting();
        retrieveCounterParties();
        addCounterParty();
        updateCounterParty();
        deactivateCounterParty();
        retrieveCounterPartyById();
    }

    private void retrieveCounterParties() {
        from(COUNTER_PARTIES_EP)
                .setHeader(XMLDB_XQUERY_SERVICE).simple("XQUERY")
                .to(RETRIEVE_COUNTER_PARTIES_XQUERY_EP)
                .to(XMLDB_DEFAULT_COLLECTION_EP)
                .marshal(xmlJsonDataFormat).convertBodyTo(String.class)
                .setHeader(Exchange.CONTENT_TYPE).constant(MediaType.APPLICATION_JSON);
    }

    private void counterPartiesContentBasedRouting() {
        Predicate isAdd = header(Exchange.HTTP_METHOD).isEqualTo(Method.POST);
        Predicate isUpdate = header(Exchange.HTTP_METHOD).isEqualTo(Method.PUT);
        Predicate isDelete = header(Exchange.HTTP_METHOD).isEqualTo(Method.DELETE);
        Predicate isRetrieve = header(Exchange.HTTP_METHOD).isEqualTo(Method.GET);

        from(COUNTER_PARTY_EP)
                .choice()
                .when(isAdd).to(ADD_COUNTER_PARTY_EP)
                .when(isUpdate).to(UPDATE_COUNTER_PARTY_EP)
                .when(isDelete).to(DEACTIVATE_COUNTER_PARTY_EP)
                .when(isRetrieve).to(RETRIEVE_COUNTER_PARTY_EP)
                .end();
    }

    private void retrieveCounterPartyById() {
        from(RETRIEVE_COUNTER_PARTY_EP)
                .recipientList(simple(XMLDB_SELECT_COUNTER_PARTY_EP))
                .marshal(xmlJsonDataFormat)
                .convertBodyTo(String.class);
    }

    private void deactivateCounterParty() {
        from(DEACTIVATE_COUNTER_PARTY_EP)
                .to(XMLDB_SELECT_COUNTER_PARTY_EP)
                .to(DEACTIVATE_COUNTER_PARTY_XQUERY_EP)
                .setHeader(XMLDB_XQUERY_SERVICE).constant("XUPDATE")
                .to(XMLDB_STORE_COUNTER_PARTY_EP);
    }

    private void updateCounterParty() {
        from(UPDATE_COUNTER_PARTY_EP)
                .to(UPDATE_COUNTER_PARTY_XUPDATE_EP)
                .setHeader(XMLDB_XQUERY_SERVICE).constant("XUPDATE")
                .to(XMLDB_STORE_COUNTER_PARTY_EP)
                .setBody().simple("{\"updateStatus\": \"OK\"}");
    }

    private void addCounterParty() {
        from(ADD_COUNTER_PARTY_EP)
                .idempotentConsumer(header("id"), counterpartyIdempotentRepository).removeOnFailure(true)
                .skipDuplicate(false)
                .filter(property(Exchange.DUPLICATE_MESSAGE).isEqualTo(true))
                .setBody().constant("{\"status\":\"OK\"}")
                .stop()
                .end()
                .process(this.initiateCounterParty).convertBodyTo(JSON.class)
                .unmarshal(this.xmlJsonDataFormat).convertBodyTo(String.class)
                .to(XMLDB_STORE_COUNTER_PARTY_EP).marshal(xmlJsonDataFormat)
                .setBody().constant("{\"status\":\"OK\"}");
    }
}