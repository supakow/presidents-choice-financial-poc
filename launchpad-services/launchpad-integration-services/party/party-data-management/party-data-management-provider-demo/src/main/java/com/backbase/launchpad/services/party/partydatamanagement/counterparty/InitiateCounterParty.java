package com.backbase.launchpad.services.party.partydatamanagement.counterparty;

import com.backbase.launchpad.services.party.partydatamanagement.counterparty.model.CounterParty;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

@Component
public class InitiateCounterParty implements Processor {

	public void process(Exchange exchange) throws Exception {
		Message message = exchange.getIn();
		CounterParty party = new CounterParty();
		party.setId(message.getHeader("id", String.class));
		party.setPartyId(message.getHeader("backbasepartyid", String.class));
		party.setName(message.getHeader("name", String.class));
        party.setPhotoUrl(message.getHeader("photoUrl", String.class));
		party.setAccount(message.getHeader("account", String.class));

		party.validate();

		exchange.getIn().setBody(party);
	}
}