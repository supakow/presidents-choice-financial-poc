package com.backbase.launchpad.services.party.partydatamanagement.basic;

public class BasicInformationConstants {

    public static final String USERNAME = "username";
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";

    // details
    public static final String DATE_OF_BIRTH = "dateOfBirth";
    public static final String GENDER = "gender";
    public static final String CITIZENSHIP = "citizenship";
    public static final String NATIONAL_IDENTITY_NUMBER = "nationalIdentityNumber";

    // activities
    public static final String INITIATION_TIME = "initiationTime";

    public static final String LANGUAGE = "language";

}
