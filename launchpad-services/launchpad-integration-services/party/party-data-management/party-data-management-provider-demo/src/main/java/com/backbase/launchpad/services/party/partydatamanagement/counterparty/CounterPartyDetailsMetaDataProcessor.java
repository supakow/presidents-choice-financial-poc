package com.backbase.launchpad.services.party.partydatamanagement.counterparty;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

import java.util.LinkedHashMap;
import java.util.Map;

import com.backbase.launchpad.services.party.partydatamanagement.Constants;

@Component
public class CounterPartyDetailsMetaDataProcessor implements Processor {
    public void process(Exchange exchange) throws Exception {
        exchange.setProperty(Constants.META_DATA_PROPERTY_NAME, metaData());
    }

    protected static Map<String, String> metaData() {
        LinkedHashMap<String, String> metaData = new LinkedHashMap<String, String>();
        metaData.put("address", "string");
        metaData.put("city", "string");
        metaData.put("state", "string");
        metaData.put("phone", "string");
        metaData.put("email", "string");
        metaData.put("dateOfBirth", "number");

        return metaData;
    }
}
