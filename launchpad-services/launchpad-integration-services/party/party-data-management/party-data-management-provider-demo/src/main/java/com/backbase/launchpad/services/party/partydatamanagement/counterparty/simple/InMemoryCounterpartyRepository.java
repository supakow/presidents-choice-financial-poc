package com.backbase.launchpad.services.party.partydatamanagement.counterparty.simple;

import org.joda.time.LocalDate;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class InMemoryCounterpartyRepository {

    private static Map<String, List<Counterparty>> counterparties = new HashMap<String, List<Counterparty>>();

    public void save(String customerId, Counterparty counterparty) {
        counterparty.setId(UUID.randomUUID().toString());
        counterparties.get(customerId).add(counterparty);
    }

    public List<Counterparty> findAll(String customerId) {
        return counterparties.get(customerId);
    }

    public boolean exists(String customerId, String name) {
        for (Counterparty counterparty : counterparties.get(customerId)) {
            if (counterparty.getName().equals(name)) {
                return true;
            }
        }
        return false;
    }

    static {
        List<Counterparty> counterpartiesForLisa = new ArrayList<Counterparty>();

        Counterparty counterparty = new Counterparty();
        counterparty.setId("889e4f6e-f16f-4bbb-9335-917d3e2027e5");
        counterparty.setName("Walter White");
        counterparty.setActive(true);
        counterparty.setDateOfBirth(new LocalDate(1963, 11, 10));
        counterparty.setPartyId("3");
        counterparty.setEmail("walter.white@gmail.com");
        counterparty.setPhone("+1-202-555-0153");
        counterparty.setPhotoUrl("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAE0AAABNCAYAAADjCemwAAADXklEQVR4Xu3bTWsTURQG4DMJTS1NRNq0LpLWpO1eUBTc+1EU3CkF9U/5IwqKWtRVEVFwIVQsWBT8IM20tE1tp2knDaRJSMY7EfuRzNxM3g7C3DnZZk7Ap2/e3JyM2tLjJYv40ZOAxmg9ebUuZrTezRgNMGM0RkMEgBnuNEYDBIARThqjAQLACCeN0QABYISTxmiAADDCSWM0QAAY4aQxGiAAjHDSGA0QAEY4aYwGCAAjnDRGAwSAEU4aowECwAgnjdEAAWCEk8ZogAAwwkljNEAAGAl80lI3UxQ7GwP+6R5HNCLzh0nFpeLhQKDRIn0Rmno0RZFYxKMAdtl+fp/W59fVQZt8MEnRM1FMw+NUKVeijTcbCqE9FGj9jObx709kvz0z9zIUS4hOO8Wdw5ZlkRYV5eXyUCppnnW7XNgX72vhOyW2UW3QytwK1fZqarw9/ULLzmSp/1x/58uJ9K7Nr1FZL594LtCfnn6gjd0Zo8GxQceX2lncoe2F7Y7nQo02em2Uhi4OOYK1HzOOXxRatMREglI3UuI/BXSaVYtVyj/NuwY5lGjS4q80SH+uU71cZ7TjAm7FbzUtWn29SpVCRVqXoUta+naa4uNxRxTjk0HGZ6Pr50uo0GTF336AlcmFBk1W/AfGAenP9K4J+3dBKNC6FX9uNkfNepPRThT/fXHiH+o88VsNUfyvRPH/lhd/u6bySUtNpyiRSTimaOvjFhW/HC0XvUZNabSRqyM0fGnY0aL0S+zI3h7tyLyC2dcpi9Yq/uvixO+w1LXfjvbmAn0oiSYrfvukn3+S76n4Q9FpWUnx63M6VY0qGrLWnHJJkxX/5odN2vu2dyow5dBkxW9+N6nwvnBqMKXQ4hfilL6Vdi5+8QV85SVe/Ep2WnQgShMzE447/lqpRsuzy74kTKmvUW7Fb3810l/oVNs9+lHED73AfxC4Fr/4UaTwrkDmT9MPpxOvEWi05JUkJS8nnXdji2I3ttB9N4aIBhZt4PwAjd8dd/6RV6SsvFr+e4+H9+WFo58W0cjete1+3T18PrBosv0Ykh7ZjDI3wLSOGNPiiOF+N4FvdsrclsBoQCb+K1rbGimwnQY4+zbCaAAlozEaIACMcNIYDRAARjhpjAYIACOcNEYDBIARThqA9gcP5uZOjndwaQAAAABJRU5ErkJggg==");
        counterpartiesForLisa.add(counterparty);

        counterparty = new Counterparty();
        counterparty.setId("2e622fea-8a00-435a-9ad8-04c3b06998c8");
        counterparty.setName("Cal Lightman");
        counterparty.setActive(true);
        counterparty.setDateOfBirth(new LocalDate(1969, 3, 24));
        counterparty.setPartyId("3");
        counterparty.setEmail("cal.lightman@gmail.com");
        counterparty.setPhone("+1-202-555-0154");
        counterparty.setPhotoUrl("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAE0AAABNCAYAAADjCemwAAAGE0lEQVR4Xu2Z+08TWxDHpy2lPEWggFy48qjGV0w0PqPRSEwkEhMJiRoU9U/yX1CjUVBB/UElEo0mRqPRmKvRaJAWuJTLrfIshVKgl9mb3bt7ena3nZ9Ocmf4qbtnlpnPfnd2zqznyh9X0sCWEwEPQ8uJl7aYoeXOjKERmDE0hkYhQPDhmsbQCAQILqw0hkYgQHBhpTE0AgGCCyuNoREIEFxYaQyNQIDgwkpjaAQCBBdWGkMjECC4sNIYGoEAwYWVxtAIBAgurDSGRiBAcGGlMTQCAYILK42hEQgQXFhpDI1AgODCSmNoBAIEF1YaQyMQILiw0hgagQDBxVBa62+t0FTaBKvpVe0yHo8Hno8/h++z310ve6TmCOxYv8Pw9Xq8EI6HoX+s39W3oaQB8H/rhr4TCxOQWE4oFU/fSJ8RowHtRN0J2Fy62ZLkl5kvGjg362zuhIr8CsuyudQcXP9x3c0VDlYdhD2VeyzrxhJjkFhJKBXP/ZH7mdBCpSForWsFz9qfbhOLE3A3ctcx8RJ/CVxovgB5njzLOlRs70ivphonO/37aagvrrcsefvzLUwmJ5WK593Pd5nQCn2FWvIBX8A4ubiyqKkltZqyzXtXxS44XH1Yev79r/fwJvbGEdql0CUo9ZcaaxA23qj4clypeGLJWCY0PHKm8QxUF1QbJ9OQhr7hPhhfGLdN/FT9KcC6JDP06x3utfUtyy+DzqZO8Hl8xpqZpRm4MXRD+61aPHqQlpbjaM1R2Fm+05Kkm1ouhy4DPqIyc1OqTKVDc0PweOyxdjnV4pFCayxphLb6NktdG50fhYejD6VQagtrob2hHbxrfzJDpQ5EB2zfwPjW3LRuk8X15cRL+DT1STumWjxSaH6vHy6GLgLWN93iqThc+3FNCgVrGapFt9nUrFb/KgOVxjFsWZ5Gn0r9zzedh/JAuXEOfe9E7sDU0pR2TLV4pNDwYEdDB6CCdNMLs7kQ6ufEmhOeC2utAvZsuuFb8Fb4VgY0fKSxnuV78x3XqhYPBpuxjTpUfQh2V+y2JPk69ho+/PpgOYZq7Ap1WZLGdVjIza3LSnoFusPdhnr0i8haHJkqVYtHCk1Wp8zFWU96W9k2aKltMeqfDgdbBWwjCnwFBmQZ9GMbjlkUiYsHxgfg28w3y81RLR4pNKwj2K8V5xUbwU8vTcPNoZuWZMQdhPkxFB8p2ctEfLSTq0m4Hb4NWEPNplo8Umh4UOzSsUD3RHoA4enW1dwF2Gfp9nXmKzwbf6b9PBA8AHuDe41z88vzcHXwqvEbQYhqjC3GtP8hM9XikY6G9gX3wf7gfkv85kdHbEqxtXgy9gTwMUarKayBjo0dgJtvNDz/YOQB4J4STdZKfJ7+DC/+eiGFplo8UmiyTt2sJDEJWRMrKtEMRSzuCPXRn48gEo9IoakWj+0QUkza/Pi0b2yHuqI6I0FUkHkKgCfExhVblp7wv4+f+Li57RzQR6V4bKG11bVp8yzdMLHuSDckV5JaAkV5RcY5nEqYpwB4Qny7muuiuPVy26Pi9VSKxxaauC/U6xIWcfNWy675lfVxWLNGE6MZm3TsAbEtcTKV4rGFJpuToaKwFTF3/OaphJj02cazUFVQZRwenB2E6EJU24jrls0kBdeqFI/jhxVxbzgcH9YeSxFEf1Q+1hanFNiy4GBzy7otBjSxHXFSmyrxOEITG1hsPP0+PwS8/w0qZV28nji2FifrTxpTkOX0Msyn5i39ndMURQSoSjyO0MRiLiZh18Xr62TdvHgN2UvETm2qxOMITTaJMCeUzTcEp8mu0wRFBk6VeFw/Fp9rOgfBQFB68z9OfoRXf7/K6a1nXuz0ErG7qArxuEJr2dAC29dvz8gB33r3hu+5fm2y+1qFF5RNTxzvwNpJFeJxhSabe2Fi2X7XxLU4bKwIWL+L4nFUKao1F1MhHldoWMyxgzd/2stVJcdrj8PWsq0WNvgmxW2VPtrOFpwK8bhCw2TE2RceM38AcUtYpg67MbjbtVSIJyto2STyf1rD0Ah3m6ExNAIBggsrjaERCBBcWGkMjUCA4MJKY2gEAgQXVhoB2j9fKqZ8PggA+AAAAABJRU5ErkJggg==");
        counterpartiesForLisa.add(counterparty);

        counterparties.put("3", counterpartiesForLisa);

        List<Counterparty> counterpartiesForThomas = new ArrayList<Counterparty>();

        counterparty = new Counterparty();
        counterparty.setId("2e622fea-8a00-435a-9ad8-04c3b06998c8");
        counterparty.setName("Dexter Morgan");
        counterparty.setActive(true);
        counterparty.setDateOfBirth(new LocalDate(1971, 2, 8));
        counterparty.setPartyId("2");
        counterparty.setEmail("dexter.morgan@gmail.com");
        counterparty.setPhone("+1-253-515-0164");
        counterparty.setPhotoUrl("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAE0AAABNCAYAAADjCemwAAAGE0lEQVR4Xu2Z+08TWxDHpy2lPEWggFy48qjGV0w0PqPRSEwkEhMJiRoU9U/yX1CjUVBB/UElEo0mRqPRmKvRaJAWuJTLrfIshVKgl9mb3bt7ena3nZ9Ocmf4qbtnlpnPfnd2zqznyh9X0sCWEwEPQ8uJl7aYoeXOjKERmDE0hkYhQPDhmsbQCAQILqw0hkYgQHBhpTE0AgGCCyuNoREIEFxYaQyNQIDgwkpjaAQCBBdWGkMjECC4sNIYGoEAwYWVxtAIBAgurDSGRiBAcGGlMTQCAYILK42hEQgQXFhpDI1AgODCSmNoBAIEF1YaQyMQILiw0hgagQDBxVBa62+t0FTaBKvpVe0yHo8Hno8/h++z310ve6TmCOxYv8Pw9Xq8EI6HoX+s39W3oaQB8H/rhr4TCxOQWE4oFU/fSJ8RowHtRN0J2Fy62ZLkl5kvGjg362zuhIr8CsuyudQcXP9x3c0VDlYdhD2VeyzrxhJjkFhJKBXP/ZH7mdBCpSForWsFz9qfbhOLE3A3ctcx8RJ/CVxovgB5njzLOlRs70ivphonO/37aagvrrcsefvzLUwmJ5WK593Pd5nQCn2FWvIBX8A4ubiyqKkltZqyzXtXxS44XH1Yev79r/fwJvbGEdql0CUo9ZcaaxA23qj4clypeGLJWCY0PHKm8QxUF1QbJ9OQhr7hPhhfGLdN/FT9KcC6JDP06x3utfUtyy+DzqZO8Hl8xpqZpRm4MXRD+61aPHqQlpbjaM1R2Fm+05Kkm1ouhy4DPqIyc1OqTKVDc0PweOyxdjnV4pFCayxphLb6NktdG50fhYejD6VQagtrob2hHbxrfzJDpQ5EB2zfwPjW3LRuk8X15cRL+DT1STumWjxSaH6vHy6GLgLWN93iqThc+3FNCgVrGapFt9nUrFb/KgOVxjFsWZ5Gn0r9zzedh/JAuXEOfe9E7sDU0pR2TLV4pNDwYEdDB6CCdNMLs7kQ6ufEmhOeC2utAvZsuuFb8Fb4VgY0fKSxnuV78x3XqhYPBpuxjTpUfQh2V+y2JPk69ho+/PpgOYZq7Ap1WZLGdVjIza3LSnoFusPdhnr0i8haHJkqVYtHCk1Wp8zFWU96W9k2aKltMeqfDgdbBWwjCnwFBmQZ9GMbjlkUiYsHxgfg28w3y81RLR4pNKwj2K8V5xUbwU8vTcPNoZuWZMQdhPkxFB8p2ctEfLSTq0m4Hb4NWEPNplo8Umh4UOzSsUD3RHoA4enW1dwF2Gfp9nXmKzwbf6b9PBA8AHuDe41z88vzcHXwqvEbQYhqjC3GtP8hM9XikY6G9gX3wf7gfkv85kdHbEqxtXgy9gTwMUarKayBjo0dgJtvNDz/YOQB4J4STdZKfJ7+DC/+eiGFplo8UmiyTt2sJDEJWRMrKtEMRSzuCPXRn48gEo9IoakWj+0QUkza/Pi0b2yHuqI6I0FUkHkKgCfExhVblp7wv4+f+Li57RzQR6V4bKG11bVp8yzdMLHuSDckV5JaAkV5RcY5nEqYpwB4Qny7muuiuPVy26Pi9VSKxxaauC/U6xIWcfNWy675lfVxWLNGE6MZm3TsAbEtcTKV4rGFJpuToaKwFTF3/OaphJj02cazUFVQZRwenB2E6EJU24jrls0kBdeqFI/jhxVxbzgcH9YeSxFEf1Q+1hanFNiy4GBzy7otBjSxHXFSmyrxOEITG1hsPP0+PwS8/w0qZV28nji2FifrTxpTkOX0Msyn5i39ndMURQSoSjyO0MRiLiZh18Xr62TdvHgN2UvETm2qxOMITTaJMCeUzTcEp8mu0wRFBk6VeFw/Fp9rOgfBQFB68z9OfoRXf7/K6a1nXuz0ErG7qArxuEJr2dAC29dvz8gB33r3hu+5fm2y+1qFF5RNTxzvwNpJFeJxhSabe2Fi2X7XxLU4bKwIWL+L4nFUKao1F1MhHldoWMyxgzd/2stVJcdrj8PWsq0WNvgmxW2VPtrOFpwK8bhCw2TE2RceM38AcUtYpg67MbjbtVSIJyto2STyf1rD0Ah3m6ExNAIBggsrjaERCBBcWGkMjUCA4MJKY2gEAgQXVhoB2j9fKqZ8PggA+AAAAABJRU5ErkJggg==");
        counterpartiesForThomas.add(counterparty);

        counterparty = new Counterparty();
        counterparty.setId("2e622fea-8a00-435a-9ad8-04c3b06998c8");
        counterparty.setName("Rick Grimes");
        counterparty.setActive(true);
        counterparty.setDateOfBirth(new LocalDate(1965, 3, 24));
        counterparty.setPartyId("2");
        counterparty.setEmail("rick.grimes@gmail.com");
        counterparty.setPhone("+1-253-515-0764");
        counterparty.setPhotoUrl("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAE0AAABNCAYAAADjCemwAAAGE0lEQVR4Xu2Z+08TWxDHpy2lPEWggFy48qjGV0w0PqPRSEwkEhMJiRoU9U/yX1CjUVBB/UElEo0mRqPRmKvRaJAWuJTLrfIshVKgl9mb3bt7ena3nZ9Ocmf4qbtnlpnPfnd2zqznyh9X0sCWEwEPQ8uJl7aYoeXOjKERmDE0hkYhQPDhmsbQCAQILqw0hkYgQHBhpTE0AgGCCyuNoREIEFxYaQyNQIDgwkpjaAQCBBdWGkMjECC4sNIYGoEAwYWVxtAIBAgurDSGRiBAcGGlMTQCAYILK42hEQgQXFhpDI1AgODCSmNoBAIEF1YaQyMQILiw0hgagQDBxVBa62+t0FTaBKvpVe0yHo8Hno8/h++z310ve6TmCOxYv8Pw9Xq8EI6HoX+s39W3oaQB8H/rhr4TCxOQWE4oFU/fSJ8RowHtRN0J2Fy62ZLkl5kvGjg362zuhIr8CsuyudQcXP9x3c0VDlYdhD2VeyzrxhJjkFhJKBXP/ZH7mdBCpSForWsFz9qfbhOLE3A3ctcx8RJ/CVxovgB5njzLOlRs70ivphonO/37aagvrrcsefvzLUwmJ5WK593Pd5nQCn2FWvIBX8A4ubiyqKkltZqyzXtXxS44XH1Yev79r/fwJvbGEdql0CUo9ZcaaxA23qj4clypeGLJWCY0PHKm8QxUF1QbJ9OQhr7hPhhfGLdN/FT9KcC6JDP06x3utfUtyy+DzqZO8Hl8xpqZpRm4MXRD+61aPHqQlpbjaM1R2Fm+05Kkm1ouhy4DPqIyc1OqTKVDc0PweOyxdjnV4pFCayxphLb6NktdG50fhYejD6VQagtrob2hHbxrfzJDpQ5EB2zfwPjW3LRuk8X15cRL+DT1STumWjxSaH6vHy6GLgLWN93iqThc+3FNCgVrGapFt9nUrFb/KgOVxjFsWZ5Gn0r9zzedh/JAuXEOfe9E7sDU0pR2TLV4pNDwYEdDB6CCdNMLs7kQ6ufEmhOeC2utAvZsuuFb8Fb4VgY0fKSxnuV78x3XqhYPBpuxjTpUfQh2V+y2JPk69ho+/PpgOYZq7Ap1WZLGdVjIza3LSnoFusPdhnr0i8haHJkqVYtHCk1Wp8zFWU96W9k2aKltMeqfDgdbBWwjCnwFBmQZ9GMbjlkUiYsHxgfg28w3y81RLR4pNKwj2K8V5xUbwU8vTcPNoZuWZMQdhPkxFB8p2ctEfLSTq0m4Hb4NWEPNplo8Umh4UOzSsUD3RHoA4enW1dwF2Gfp9nXmKzwbf6b9PBA8AHuDe41z88vzcHXwqvEbQYhqjC3GtP8hM9XikY6G9gX3wf7gfkv85kdHbEqxtXgy9gTwMUarKayBjo0dgJtvNDz/YOQB4J4STdZKfJ7+DC/+eiGFplo8UmiyTt2sJDEJWRMrKtEMRSzuCPXRn48gEo9IoakWj+0QUkza/Pi0b2yHuqI6I0FUkHkKgCfExhVblp7wv4+f+Li57RzQR6V4bKG11bVp8yzdMLHuSDckV5JaAkV5RcY5nEqYpwB4Qny7muuiuPVy26Pi9VSKxxaauC/U6xIWcfNWy675lfVxWLNGE6MZm3TsAbEtcTKV4rGFJpuToaKwFTF3/OaphJj02cazUFVQZRwenB2E6EJU24jrls0kBdeqFI/jhxVxbzgcH9YeSxFEf1Q+1hanFNiy4GBzy7otBjSxHXFSmyrxOEITG1hsPP0+PwS8/w0qZV28nji2FifrTxpTkOX0Msyn5i39ndMURQSoSjyO0MRiLiZh18Xr62TdvHgN2UvETm2qxOMITTaJMCeUzTcEp8mu0wRFBk6VeFw/Fp9rOgfBQFB68z9OfoRXf7/K6a1nXuz0ErG7qArxuEJr2dAC29dvz8gB33r3hu+5fm2y+1qFF5RNTxzvwNpJFeJxhSabe2Fi2X7XxLU4bKwIWL+L4nFUKao1F1MhHldoWMyxgzd/2stVJcdrj8PWsq0WNvgmxW2VPtrOFpwK8bhCw2TE2RceM38AcUtYpg67MbjbtVSIJyto2STyf1rD0Ah3m6ExNAIBggsrjaERCBBcWGkMjUCA4MJKY2gEAgQXVhoB2j9fKqZ8PggA+AAAAABJRU5ErkJggg==");
        counterpartiesForThomas.add(counterparty);

        counterparties.put("2", counterpartiesForThomas);

        List<Counterparty> counterpartiesForJohn = new ArrayList<Counterparty>();

        counterparty = new Counterparty();
        counterparty.setId("41242fea-8a00-435a-9ad8-04c3b06998c8");
        counterparty.setName("Tony Soprano");
        counterparty.setActive(true);
        counterparty.setDateOfBirth(new LocalDate(1971, 2, 8));
        counterparty.setPartyId("4");
        counterparty.setEmail("tony.soprano@gmail.com");
        counterparty.setPhone("+1-253-515-0164");
        counterparty.setPhotoUrl("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAE0AAABNCAYAAADjCemwAAAGE0lEQVR4Xu2Z+08TWxDHpy2lPEWggFy48qjGV0w0PqPRSEwkEhMJiRoU9U/yX1CjUVBB/UElEo0mRqPRmKvRaJAWuJTLrfIshVKgl9mb3bt7ena3nZ9Ocmf4qbtnlpnPfnd2zqznyh9X0sCWEwEPQ8uJl7aYoeXOjKERmDE0hkYhQPDhmsbQCAQILqw0hkYgQHBhpTE0AgGCCyuNoREIEFxYaQyNQIDgwkpjaAQCBBdWGkMjECC4sNIYGoEAwYWVxtAIBAgurDSGRiBAcGGlMTQCAYILK42hEQgQXFhpDI1AgODCSmNoBAIEF1YaQyMQILiw0hgagQDBxVBa62+t0FTaBKvpVe0yHo8Hno8/h++z310ve6TmCOxYv8Pw9Xq8EI6HoX+s39W3oaQB8H/rhr4TCxOQWE4oFU/fSJ8RowHtRN0J2Fy62ZLkl5kvGjg362zuhIr8CsuyudQcXP9x3c0VDlYdhD2VeyzrxhJjkFhJKBXP/ZH7mdBCpSForWsFz9qfbhOLE3A3ctcx8RJ/CVxovgB5njzLOlRs70ivphonO/37aagvrrcsefvzLUwmJ5WK593Pd5nQCn2FWvIBX8A4ubiyqKkltZqyzXtXxS44XH1Yev79r/fwJvbGEdql0CUo9ZcaaxA23qj4clypeGLJWCY0PHKm8QxUF1QbJ9OQhr7hPhhfGLdN/FT9KcC6JDP06x3utfUtyy+DzqZO8Hl8xpqZpRm4MXRD+61aPHqQlpbjaM1R2Fm+05Kkm1ouhy4DPqIyc1OqTKVDc0PweOyxdjnV4pFCayxphLb6NktdG50fhYejD6VQagtrob2hHbxrfzJDpQ5EB2zfwPjW3LRuk8X15cRL+DT1STumWjxSaH6vHy6GLgLWN93iqThc+3FNCgVrGapFt9nUrFb/KgOVxjFsWZ5Gn0r9zzedh/JAuXEOfe9E7sDU0pR2TLV4pNDwYEdDB6CCdNMLs7kQ6ufEmhOeC2utAvZsuuFb8Fb4VgY0fKSxnuV78x3XqhYPBpuxjTpUfQh2V+y2JPk69ho+/PpgOYZq7Ap1WZLGdVjIza3LSnoFusPdhnr0i8haHJkqVYtHCk1Wp8zFWU96W9k2aKltMeqfDgdbBWwjCnwFBmQZ9GMbjlkUiYsHxgfg28w3y81RLR4pNKwj2K8V5xUbwU8vTcPNoZuWZMQdhPkxFB8p2ctEfLSTq0m4Hb4NWEPNplo8Umh4UOzSsUD3RHoA4enW1dwF2Gfp9nXmKzwbf6b9PBA8AHuDe41z88vzcHXwqvEbQYhqjC3GtP8hM9XikY6G9gX3wf7gfkv85kdHbEqxtXgy9gTwMUarKayBjo0dgJtvNDz/YOQB4J4STdZKfJ7+DC/+eiGFplo8UmiyTt2sJDEJWRMrKtEMRSzuCPXRn48gEo9IoakWj+0QUkza/Pi0b2yHuqI6I0FUkHkKgCfExhVblp7wv4+f+Li57RzQR6V4bKG11bVp8yzdMLHuSDckV5JaAkV5RcY5nEqYpwB4Qny7muuiuPVy26Pi9VSKxxaauC/U6xIWcfNWy675lfVxWLNGE6MZm3TsAbEtcTKV4rGFJpuToaKwFTF3/OaphJj02cazUFVQZRwenB2E6EJU24jrls0kBdeqFI/jhxVxbzgcH9YeSxFEf1Q+1hanFNiy4GBzy7otBjSxHXFSmyrxOEITG1hsPP0+PwS8/w0qZV28nji2FifrTxpTkOX0Msyn5i39ndMURQSoSjyO0MRiLiZh18Xr62TdvHgN2UvETm2qxOMITTaJMCeUzTcEp8mu0wRFBk6VeFw/Fp9rOgfBQFB68z9OfoRXf7/K6a1nXuz0ErG7qArxuEJr2dAC29dvz8gB33r3hu+5fm2y+1qFF5RNTxzvwNpJFeJxhSabe2Fi2X7XxLU4bKwIWL+L4nFUKao1F1MhHldoWMyxgzd/2stVJcdrj8PWsq0WNvgmxW2VPtrOFpwK8bhCw2TE2RceM38AcUtYpg67MbjbtVSIJyto2STyf1rD0Ah3m6ExNAIBggsrjaERCBBcWGkMjUCA4MJKY2gEAgQXVhoB2j9fKqZ8PggA+AAAAABJRU5ErkJggg==");
        counterpartiesForJohn.add(counterparty);

        counterparty = new Counterparty();
        counterparty.setId("77432fea-8a00-435a-9ad8-04c3b06998c8");
        counterparty.setName("Francis Underwood");
        counterparty.setActive(true);
        counterparty.setDateOfBirth(new LocalDate(1965, 3, 24));
        counterparty.setPartyId("4");
        counterparty.setEmail("francis.underwood@gmail.com");
        counterparty.setPhone("+1-253-515-0764");
        counterparty.setPhotoUrl("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAE0AAABNCAYAAADjCemwAAAGE0lEQVR4Xu2Z+08TWxDHpy2lPEWggFy48qjGV0w0PqPRSEwkEhMJiRoU9U/yX1CjUVBB/UElEo0mRqPRmKvRaJAWuJTLrfIshVKgl9mb3bt7ena3nZ9Ocmf4qbtnlpnPfnd2zqznyh9X0sCWEwEPQ8uJl7aYoeXOjKERmDE0hkYhQPDhmsbQCAQILqw0hkYgQHBhpTE0AgGCCyuNoREIEFxYaQyNQIDgwkpjaAQCBBdWGkMjECC4sNIYGoEAwYWVxtAIBAgurDSGRiBAcGGlMTQCAYILK42hEQgQXFhpDI1AgODCSmNoBAIEF1YaQyMQILiw0hgagQDBxVBa62+t0FTaBKvpVe0yHo8Hno8/h++z310ve6TmCOxYv8Pw9Xq8EI6HoX+s39W3oaQB8H/rhr4TCxOQWE4oFU/fSJ8RowHtRN0J2Fy62ZLkl5kvGjg362zuhIr8CsuyudQcXP9x3c0VDlYdhD2VeyzrxhJjkFhJKBXP/ZH7mdBCpSForWsFz9qfbhOLE3A3ctcx8RJ/CVxovgB5njzLOlRs70ivphonO/37aagvrrcsefvzLUwmJ5WK593Pd5nQCn2FWvIBX8A4ubiyqKkltZqyzXtXxS44XH1Yev79r/fwJvbGEdql0CUo9ZcaaxA23qj4clypeGLJWCY0PHKm8QxUF1QbJ9OQhr7hPhhfGLdN/FT9KcC6JDP06x3utfUtyy+DzqZO8Hl8xpqZpRm4MXRD+61aPHqQlpbjaM1R2Fm+05Kkm1ouhy4DPqIyc1OqTKVDc0PweOyxdjnV4pFCayxphLb6NktdG50fhYejD6VQagtrob2hHbxrfzJDpQ5EB2zfwPjW3LRuk8X15cRL+DT1STumWjxSaH6vHy6GLgLWN93iqThc+3FNCgVrGapFt9nUrFb/KgOVxjFsWZ5Gn0r9zzedh/JAuXEOfe9E7sDU0pR2TLV4pNDwYEdDB6CCdNMLs7kQ6ufEmhOeC2utAvZsuuFb8Fb4VgY0fKSxnuV78x3XqhYPBpuxjTpUfQh2V+y2JPk69ho+/PpgOYZq7Ap1WZLGdVjIza3LSnoFusPdhnr0i8haHJkqVYtHCk1Wp8zFWU96W9k2aKltMeqfDgdbBWwjCnwFBmQZ9GMbjlkUiYsHxgfg28w3y81RLR4pNKwj2K8V5xUbwU8vTcPNoZuWZMQdhPkxFB8p2ctEfLSTq0m4Hb4NWEPNplo8Umh4UOzSsUD3RHoA4enW1dwF2Gfp9nXmKzwbf6b9PBA8AHuDe41z88vzcHXwqvEbQYhqjC3GtP8hM9XikY6G9gX3wf7gfkv85kdHbEqxtXgy9gTwMUarKayBjo0dgJtvNDz/YOQB4J4STdZKfJ7+DC/+eiGFplo8UmiyTt2sJDEJWRMrKtEMRSzuCPXRn48gEo9IoakWj+0QUkza/Pi0b2yHuqI6I0FUkHkKgCfExhVblp7wv4+f+Li57RzQR6V4bKG11bVp8yzdMLHuSDckV5JaAkV5RcY5nEqYpwB4Qny7muuiuPVy26Pi9VSKxxaauC/U6xIWcfNWy675lfVxWLNGE6MZm3TsAbEtcTKV4rGFJpuToaKwFTF3/OaphJj02cazUFVQZRwenB2E6EJU24jrls0kBdeqFI/jhxVxbzgcH9YeSxFEf1Q+1hanFNiy4GBzy7otBjSxHXFSmyrxOEITG1hsPP0+PwS8/w0qZV28nji2FifrTxpTkOX0Msyn5i39ndMURQSoSjyO0MRiLiZh18Xr62TdvHgN2UvETm2qxOMITTaJMCeUzTcEp8mu0wRFBk6VeFw/Fp9rOgfBQFB68z9OfoRXf7/K6a1nXuz0ErG7qArxuEJr2dAC29dvz8gB33r3hu+5fm2y+1qFF5RNTxzvwNpJFeJxhSabe2Fi2X7XxLU4bKwIWL+L4nFUKao1F1MhHldoWMyxgzd/2stVJcdrj8PWsq0WNvgmxW2VPtrOFpwK8bhCw2TE2RceM38AcUtYpg67MbjbtVSIJyto2STyf1rD0Ah3m6ExNAIBggsrjaERCBBcWGkMjUCA4MJKY2gEAgQXVhoB2j9fKqZ8PggA+AAAAABJRU5ErkJggg==");
        counterpartiesForJohn.add(counterparty);

        counterparties.put("4", counterpartiesForJohn);
    }

}
