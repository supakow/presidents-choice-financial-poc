package com.backbase.launchpad.services.party.partydatamanagement.counterparty.simple;

public class InvalidCounterpartyException extends Exception {

    public InvalidCounterpartyException(String message) {
        super(message);
    }
}
