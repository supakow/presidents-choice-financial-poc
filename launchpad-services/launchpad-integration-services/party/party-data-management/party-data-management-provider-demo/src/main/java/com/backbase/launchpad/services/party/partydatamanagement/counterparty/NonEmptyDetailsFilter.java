package com.backbase.launchpad.services.party.partydatamanagement.counterparty;

import com.backbase.launchpad.services.party.partydatamanagement.Constants;
import com.google.common.base.Strings;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class NonEmptyDetailsFilter implements Processor {
    public void process(Exchange exchange) throws Exception {
        Map<String, String> detailsMetaData = exchange.getProperty(Constants.META_DATA_PROPERTY_NAME,
                Map.class);

        JSONObject jsonObject = JSONObject.fromObject(exchange.getIn().getBody(String.class));

        for (String field: detailsMetaData.keySet()) {
            Object value = jsonObject.get(field);
            boolean nonEmpty = false;

            if (value != null) {
                if (value instanceof String) {
                    if (!Strings.isNullOrEmpty((String) value)) {
                        nonEmpty = true;
                    }
                } else if (value instanceof Number) {
                    if (((Number) value).intValue() != 0) {
                        nonEmpty = true;
                    }
                } else if (value instanceof JSONArray) {
                    if (((JSONArray) value).size() > 0) {
                        nonEmpty = true;
                    }
                } else {
                    nonEmpty = true;
                }
            }

            if (!nonEmpty) {
                jsonObject.remove(field);
            }
        }

        exchange.getIn().setBody(JSONSerializer.toJSON(jsonObject));
    }
}
