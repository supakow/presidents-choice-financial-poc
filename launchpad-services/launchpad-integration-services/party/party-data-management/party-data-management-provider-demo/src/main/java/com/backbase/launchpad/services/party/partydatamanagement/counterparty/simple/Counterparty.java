package com.backbase.launchpad.services.party.partydatamanagement.counterparty.simple;

import org.joda.time.LocalDate;

/**
 * Represents a natural person or legal entity who receives money.
 */
public class Counterparty {

    private String id;
    private String name;
    private String partyId;
    private String photoUrl;
    private boolean active = true;
    private String email;
    private String phone;
    private LocalDate dateOfBirth;
    private String account;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPartyId() {
        return partyId;
    }

    public void setPartyId(String partyId) {
        this.partyId = partyId;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Counterparty)) {
            return false;
        }
        Counterparty other = (Counterparty) object;
        return name == null ? other.name == null : name.equals(other);
    }
}
