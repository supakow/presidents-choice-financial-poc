package com.backbase.launchpad.services.party.partydatamanagement.location;

import static org.apache.camel.builder.PredicateBuilder.isNotNull;

import org.apache.camel.Endpoint;
import org.apache.camel.EndpointInject;
import org.apache.camel.Exchange;
import org.apache.camel.Predicate;
import org.apache.camel.builder.RouteBuilder;
import org.restlet.data.MediaType;
import org.restlet.data.Method;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.backbase.launchpad.services.party.partydatamanagement.Constants;
import com.backbase.launchpad.services.party.partydatamanagement.PopulateFieldValueProcessor;
import com.backbase.mashup.camel.json.XmlJsonDataFormatWrapper;

/**
 * Mediation Routes for Party Locations services
 * (i.e. customer contact information)
 *
 * @author V.Goldin
 */
@Component
public class PartyLocationsRoutes extends RouteBuilder {
    protected static final String UPDATE_PARTY_LOCATION_EP =
            "velocity:updatePartyLocation.vm";
    protected static final String XMLDB_PARTY_LOCATIONS_COLLECTION_URI =
            "xmldb:exist:///db/launchpad/customers";
    protected static final String XMLDB_STORE_PARTY_LOCATIONS_EP = XMLDB_PARTY_LOCATIONS_COLLECTION_URI
            + "?username=admin&password=";
    private static final String XMLDB_XQUERY_SERVICE = "XMLDB_SERVICE_TYPE";

    @EndpointInject(uri = "seda:provider/party/party-data-management/locations")
    private Endpoint entryPointEndpoint;

    @EndpointInject(uri = "seda:provider/party/party-data-management/locations/meta-data")
    private Endpoint metaDataEndpoint;

    @EndpointInject(uri = "direct:retrievePartyLocations")
    private Endpoint retrievePartyLocationsEndpoint;

    @EndpointInject(uri = "direct:updatePartyLocations")
    private Endpoint updatePartyLocationsEndpoint;

    @Autowired
    private XmlJsonDataFormatWrapper xmlJsonDataFormat;

    @Autowired
    private PartyLocationMetaDataProcessor metaDataProcessor;

    @Autowired
    private PopulateFieldValueProcessor populateFieldValueProcessor;

    @Override
    public void configure() throws Exception {
        methodBasedRouter();

        retrieveLocationsRoute();
        updateLocationRoute();
        metaDataRoute();
    }

    private void metaDataRoute() {

    }

    private void methodBasedRouter() {
        Predicate isRetrieve = header(Exchange.HTTP_METHOD.toLowerCase()).isEqualTo(Method.GET);
        Predicate isUpdate = header(Exchange.HTTP_METHOD.toLowerCase()).isEqualTo(Method.PUT);

        from(entryPointEndpoint)
        .setHeader(Exchange.CONTENT_TYPE).constant(MediaType.APPLICATION_JSON)
        .choice()
            .when(isRetrieve).to(retrievePartyLocationsEndpoint)
            .when(isUpdate).to(updatePartyLocationsEndpoint)
        .end();
    }

    private void retrieveLocationsRoute() {
        from(retrievePartyLocationsEndpoint)
            .recipientList().simple(
                "xmldb:exist:///db/launchpad/customers?XPath=//customer[id = '${header.backbasepartyid}']/locations") // -- retrieve the data from the xml database
            .convertBodyTo(String.class)    // -- convert the content to string to avoid passing over bytes without explicit encoding
            .marshal(xmlJsonDataFormat)     // -- marshal the xml content to the corresponding JSON
            .convertBodyTo(String.class);   // -- convert the resulting JSON object to a String representation
    }

    private void updateLocationRoute() {
        Predicate valueIsNotNull = isNotNull(header("value"));

        from(updatePartyLocationsEndpoint)
            .process(metaDataProcessor)
            .split().simple("${property." + Constants.META_DATA_PROPERTY_NAME + ".keySet}")
                .process(populateFieldValueProcessor)
                .setHeader("value").simple("${headers[${body}]}")
                .filter(valueIsNotNull)
                    .choice()
                        .when(simple("${body} == 'phoneNumber'"))
                            .validate(header("value").regex("^\\+(?:[0-9]?){6,14}[0-9]$"))
                        .endChoice()
                        .when(simple("${body} == 'emailAddress'"))
                            .validate(header("value").regex("^[A-Za-z0-9+_.-]+@[A-Za-z0-9.-]+$"))
                        .endChoice()
                    .end()
                    .to(UPDATE_PARTY_LOCATION_EP)
                    .setHeader(XMLDB_XQUERY_SERVICE).constant("XQUERY")
                    .to(XMLDB_STORE_PARTY_LOCATIONS_EP)
                .end()
            .end()
            .setBody().constant("{\"status\" : \"Updated\"}");
    }
}
