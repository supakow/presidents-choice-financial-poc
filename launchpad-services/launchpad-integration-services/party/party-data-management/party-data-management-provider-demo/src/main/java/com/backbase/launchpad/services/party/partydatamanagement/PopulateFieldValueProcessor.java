package com.backbase.launchpad.services.party.partydatamanagement;

import com.google.common.base.Strings;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;
import java.util.Map;

@Component
public class PopulateFieldValueProcessor implements Processor {
    public void process(Exchange exchange) throws Exception {
        String field = exchange.getIn().getBody(String.class);
        String value = (String) exchange.getIn().getHeader(field);
        Map<String, String> detailsMetaData = exchange.getProperty(Constants.META_DATA_PROPERTY_NAME,
                Map.class);

        String fieldType = detailsMetaData.get(field);
        if (fieldType != null) {
            // -- sometimes the consumer provides a double-space filled string, hence we need to trim() it first, because Strings does not recognizes double-space as an empty string
            value = Strings.emptyToNull(Strings.nullToEmpty(value).trim());

            if (value == null && fieldType.equals("number")) {
                value = "0";
            }

            if (value != null) {
                exchange.getIn().setHeader(field, value);
            }
        }
    }
}
