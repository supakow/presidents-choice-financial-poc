package com.backbase.launchpad.services.party.partydatamanagement.counterparty.simple;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CounterpartyProcessor implements Processor {

    @Autowired
    private InMemoryCounterpartyRepository counterpartyRepository;

    @Override
    public void process(Exchange exchange) throws Exception {

        Message in = exchange.getIn();
        Message out = exchange.getOut();
        Object header = in.getHeader(Exchange.HTTP_METHOD);
        String customerId = (String) in.getHeader("backbasepartyId");
        if ("GET".equals(header)) {
            handleGet(customerId, out);
        } else if ("POST".equals(header)) {
            handlePost(customerId, in, out);
        } else {
            setErrorMessage(out, "GET and POST are supported on /counterparties endpoint", 405);
        }
    }

    private void handlePost(String customerId, Message in, Message out) {
        try {
            org.json.simple.JSONObject counterpartAsJson = getPayloadOrSendError(in);
            String name = (String) counterpartAsJson.get("name");
            if (name == null) {
                setErrorMessage(out, "'name' is mandatory", 400);
                return;
            }
            String account = (String) counterpartAsJson.get("account");
            String phone = (String) counterpartAsJson.get("phone");
            String email = (String) counterpartAsJson.get("email");
            if (account == null && phone == null && email == null) {
                setErrorMessage(out, "One of the 'account','phone' or 'email' is mandatory", 400);
                return;
            }
            if (counterpartyRepository.exists(customerId, name)) {
                setErrorMessage(out, "counterparty with name " + name + " already exists", 409);
                return;
            }
            Counterparty counterparty = new Counterparty();
            counterparty.setName(name);
            counterparty.setAccount(account);
            counterparty.setPhone(phone);
            counterparty.setEmail(email);

            counterpartyRepository.save(customerId, counterparty);
            out.getExchange().getOut().setHeader(Exchange.HTTP_RESPONSE_CODE, 201);
        } catch (InvalidCounterpartyException e) {
            setErrorMessage(out, e.getMessage(), 400);
        }
    }

    private void handleGet(String customerId, Message out) {
        JSONArray response = new JSONArray();
        List<Counterparty> counterparties = counterpartyRepository.findAll(customerId);
        for (Counterparty counterparty : counterparties) {
            response.add(render(counterparty));
        }
        out.setBody(response);
    }

    private void setErrorMessage(Message out, String message, int code) {
        out.setBody("{\"error\": \"" + message + "\"}");
        out.setHeader(Exchange.HTTP_RESPONSE_CODE, code);
    }

    private JSONObject render(Counterparty counterparty) {
        JSONObject counterpartyAsJson = new JSONObject();
        counterpartyAsJson.put("id", counterparty.getId());
        counterpartyAsJson.put("name", counterparty.getName());
        counterpartyAsJson.put("partyId", counterparty.getPartyId());
        counterpartyAsJson.put("photoUrl", counterparty.getPhotoUrl());
        counterpartyAsJson.put("active", counterparty.isActive());
        counterpartyAsJson.put("email", counterparty.getEmail());
        counterpartyAsJson.put("phone", counterparty.getPhone());
        counterpartyAsJson.put("account", counterparty.getAccount());
        if (counterparty.getDateOfBirth() != null) {
            counterpartyAsJson.put("dateOfBirth", counterparty.getDateOfBirth().toString());
        }
        return counterpartyAsJson;
    }

    private org.json.simple.JSONObject getPayloadOrSendError(Message in) throws InvalidCounterpartyException {
        org.json.simple.JSONObject payload;
        String bodyAsString = in.getBody(String.class);
        try {
            if (bodyAsString == null) {
                throw new ParseException(-1);
            }
            Object parsedData = new JSONParser().parse(bodyAsString);
            if (!(parsedData instanceof org.json.simple.JSONObject)) {
                throw new ParseException(-1);
            }
            payload = (org.json.simple.JSONObject) parsedData;
        } catch (ParseException e) {
            throw new InvalidCounterpartyException("Payload is not properly formatted JSON");
        }
        return payload;
    }


}
