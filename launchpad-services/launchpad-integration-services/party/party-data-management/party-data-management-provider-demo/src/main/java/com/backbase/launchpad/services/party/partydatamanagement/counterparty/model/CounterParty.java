package com.backbase.launchpad.services.party.partydatamanagement.counterparty.model;

public class CounterParty {
	private String id;
	private String partyId;
	private String name;
	private String photoUrl;
	private String account;
    private boolean active = true;

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }


	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPartyId() {
		return this.partyId;
	}

	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhotoUrl() {
		return this.photoUrl;
	}

	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}

	public String getAccount() {
		return this.account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public void validate() throws InvalidCounterPartyInputException {
		assertNotNull(this.partyId, "partyId");
		assertNotNull(this.id, "id");
		assertNotNull(this.name, "name");
		assertNotNull(this.photoUrl, "photoUrl");
		assertNotNull(this.account, "account");
	}

	private static void assertNotNull(Object value, String fieldName)
			throws InvalidCounterPartyInputException {
		if (value == null)
			throw new InvalidCounterPartyInputException(fieldName
					+ " must be specified");
	}
}