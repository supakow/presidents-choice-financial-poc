package com.backbase.launchpad.services.party.partydatamanagement.basic;

import static com.backbase.launchpad.services.party.partydatamanagement.basic.BasicInformationConstants.CITIZENSHIP;
import static com.backbase.launchpad.services.party.partydatamanagement.basic.BasicInformationConstants.DATE_OF_BIRTH;
import static com.backbase.launchpad.services.party.partydatamanagement.basic.BasicInformationConstants.FIRST_NAME;
import static com.backbase.launchpad.services.party.partydatamanagement.basic.BasicInformationConstants.GENDER;
import static com.backbase.launchpad.services.party.partydatamanagement.basic.BasicInformationConstants.INITIATION_TIME;
import static com.backbase.launchpad.services.party.partydatamanagement.basic.BasicInformationConstants.LANGUAGE;
import static com.backbase.launchpad.services.party.partydatamanagement.basic.BasicInformationConstants.LAST_NAME;
import static com.backbase.launchpad.services.party.partydatamanagement.basic.BasicInformationConstants.NATIONAL_IDENTITY_NUMBER;
import static com.backbase.launchpad.services.party.partydatamanagement.basic.BasicInformationConstants.USERNAME;

import net.sf.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.commons.lang.StringUtils;

/**
 * Camel processor for customer profile functionality.
 */
public class BasicInformationProcessor implements Processor {

    private static final DateFormat LAST_LOGIN_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

    public void process(Exchange exchange) throws Exception {

        Message in = exchange.getIn();

        JSONObject response = new JSONObject();
        response.put("username", in.getHeader(USERNAME, String.class));
        response.put("firstName", in.getHeader(FIRST_NAME, String.class));
        response.put("lastName", in.getHeader(LAST_NAME, String.class));

        // details
        JSONObject details = new JSONObject();
        details.put("dateOfBirth", in.getHeader(DATE_OF_BIRTH, String.class));
        details.put("gender", in.getHeader(GENDER, String.class));
        details.put("citizenship", in.getHeader(CITIZENSHIP, String.class));

        String nationalIdentityNumber = in.getHeader(NATIONAL_IDENTITY_NUMBER, String.class);
        String maskedIdentityNumber = StringUtils.overlay(nationalIdentityNumber,
                StringUtils.repeat("x", nationalIdentityNumber.length() - 2), 0,
                nationalIdentityNumber.length() - 2);

        String language = in.getHeader(LANGUAGE, String.class);
        if ("NL".equals(language)) {
            details.put("BSN", maskedIdentityNumber);
        } else if ("EN".equals(language)) {
            details.put("SSN", maskedIdentityNumber);
        } else {
            // default for any other language to SSN
            details.put("SSN", maskedIdentityNumber);
        }
        response.put("details", details);

        // activities
        JSONObject activities = new JSONObject();
        String initiationTime = in.getHeader(INITIATION_TIME, String.class);
        if (!StringUtils.isEmpty(initiationTime)) {
            Date initiationTimeAsDate = new Date(Long.parseLong(initiationTime));
            activities.put("lastLoggedIn", LAST_LOGIN_DATE_FORMAT.format(initiationTimeAsDate));
        } else {
            activities.put("lastLoggedIn", "");
        }
        response.put("activities", activities);

        exchange.getIn().setBody(response.toString());
    }

}
