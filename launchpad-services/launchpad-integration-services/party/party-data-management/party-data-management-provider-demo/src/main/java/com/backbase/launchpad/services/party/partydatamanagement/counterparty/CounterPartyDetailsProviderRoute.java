package com.backbase.launchpad.services.party.partydatamanagement.counterparty;

import net.sf.json.JSON;

import org.apache.camel.Exchange;
import org.apache.camel.Predicate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.spi.IdempotentRepository;
import org.restlet.data.Method;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.backbase.launchpad.services.party.partydatamanagement.Constants;
import com.backbase.launchpad.services.party.partydatamanagement.PopulateFieldValueProcessor;
import com.backbase.mashup.camel.json.XmlJsonDataFormatWrapper;

@Component
public class CounterPartyDetailsProviderRoute extends RouteBuilder {

    protected static final String COUNTER_PARTY_DETAILS_EP =
            "seda:provider/party/party-data-management/counter-party/details";

    private static final String DIRECT_COUNTER_PARTY_DETAILS_EP =
            "direct:provider/party/party-data-management/counter-party/details";
    private static final String ADD_COUNTER_PARTY_DETAILS_EP = DIRECT_COUNTER_PARTY_DETAILS_EP
            + "/add";
    private static final String UPDATE_COUNTER_PARTY_DETAILS_EP = DIRECT_COUNTER_PARTY_DETAILS_EP
            + "/update";
    private static final String RETRIEVE_COUNTER_PARTY_DETAILS_EP = DIRECT_COUNTER_PARTY_DETAILS_EP
            + "/retrieve";

    private static final String UPDATE_COUNTER_PARTY_DETAILS_ELEMENT_EP =
            "velocity:updateCounterPartyDetailsXUpdate.vm";
    private static final String XMLDB_COUNTER_PARTY_DETAILS_COLLECTION_URI =
            "xmldb:exist:///db/launchpad/counterPartyDetails";
    protected static final String XMLDB_STORE_COUNTER_PARTY_DETAILS_EP = XMLDB_COUNTER_PARTY_DETAILS_COLLECTION_URI
            + "?username=admin&password=";
    private static final String XMLDB_SELECT_COUNTER_PARTY_DETAILS_EP = XMLDB_COUNTER_PARTY_DETAILS_COLLECTION_URI
            + "?XPath=//o[id = '${header.id}']";

    private static final String XMLDB_XQUERY_SERVICE = "XMLDB_SERVICE_TYPE";

    @Autowired
    private CounterPartyDetailsProcessor counterPartyDetailsProcessor;

    @Autowired
    private NonEmptyDetailsFilter nonEmptyDetailsFilter;

    @Autowired
    private CounterPartyDetailsMetaDataProcessor metaDataProcessor;

    @Autowired
    private XmlJsonDataFormatWrapper xmlJsonDataFormat;

    @Autowired
    private IdempotentRepository<String> counterpartyDetailsIdempotentRepository;

    @Autowired
    private PopulateFieldValueProcessor populateFieldValueProcessor;

    public void configure() throws Exception {
        counterPartyDetailsContentBasedRouting();

        addCounterPartyDetails();
        updateCounterPartyDetails();
        retrieveCounterPartyDetailsById();
	}

    private void counterPartyDetailsContentBasedRouting() {
		Predicate isAdd = header(Exchange.HTTP_METHOD).isEqualTo(Method.POST);
		Predicate isUpdate = header(Exchange.HTTP_METHOD).isEqualTo(Method.PUT);
		Predicate isRetrieve = header(Exchange.HTTP_METHOD).isEqualTo(Method.GET);

		from(COUNTER_PARTY_DETAILS_EP)
            .choice()
                .when(isAdd).to(ADD_COUNTER_PARTY_DETAILS_EP)
                .when(isUpdate).to(UPDATE_COUNTER_PARTY_DETAILS_EP)
                .when(isRetrieve).to(RETRIEVE_COUNTER_PARTY_DETAILS_EP)
            .end();
    }

    private void retrieveCounterPartyDetailsById() {
        from(RETRIEVE_COUNTER_PARTY_DETAILS_EP)
                .process(this.metaDataProcessor)
                .recipientList(simple(XMLDB_SELECT_COUNTER_PARTY_DETAILS_EP))
                .marshal(this.xmlJsonDataFormat).convertBodyTo(String.class, "UTF-8")
                .process(nonEmptyDetailsFilter);
    }

    private void updateCounterPartyDetails() {
        from(UPDATE_COUNTER_PARTY_DETAILS_EP)
            .process(metaDataProcessor)
            .split().simple("${property."+ Constants.META_DATA_PROPERTY_NAME +".keySet}")
                .process(populateFieldValueProcessor)
                .setHeader("value").simple("${headers[${body}]}")
                .to(UPDATE_COUNTER_PARTY_DETAILS_ELEMENT_EP)
                .setHeader(XMLDB_XQUERY_SERVICE).constant("XQUERY")
                .to(XMLDB_STORE_COUNTER_PARTY_DETAILS_EP)
            .end()
        .setBody().simple("{\"updateStatus\": \"OK\"}");
    }

    private void addCounterPartyDetails() {
        from(ADD_COUNTER_PARTY_DETAILS_EP)
            .idempotentConsumer(header("id"), counterpartyDetailsIdempotentRepository).removeOnFailure(true).skipDuplicate(false)
            .filter(property(Exchange.DUPLICATE_MESSAGE).isEqualTo(true))
                .setBody().constant("{\"status\":\"OK\"}")
                .stop()
            .end()
            .process(this.metaDataProcessor)
            .split().simple("${property."+Constants.META_DATA_PROPERTY_NAME+".keySet}")
                .process(populateFieldValueProcessor)
            .end()
            .process(this.counterPartyDetailsProcessor).convertBodyTo(JSON.class)
            .unmarshal(this.xmlJsonDataFormat).convertBodyTo(String.class)
            .to(XMLDB_STORE_COUNTER_PARTY_DETAILS_EP)
            .setBody().constant("{\"status\":\"OK\"}");
    }
}