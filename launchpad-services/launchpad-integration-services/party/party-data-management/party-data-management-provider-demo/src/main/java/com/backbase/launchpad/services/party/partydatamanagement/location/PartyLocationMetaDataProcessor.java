package com.backbase.launchpad.services.party.partydatamanagement.location;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

import com.backbase.launchpad.services.party.partydatamanagement.Constants;

/**
 * Camel processor populating Camel Exchange  property named
 * {@code Constants.META_DATA_PROPERTY_NAME} with the {@code Map}
 * of expected Party Locations (Camel headers)
 *
 * @author V.Goldin
 */
@Component
public class PartyLocationMetaDataProcessor implements Processor {
    public void process(Exchange exchange) throws Exception {
        exchange.setProperty(Constants.META_DATA_PROPERTY_NAME, metaData());
    }

    protected static Map<String, String> metaData() {
        LinkedHashMap<String, String> metaData = new LinkedHashMap<String, String>();
        metaData.put("phoneNumber", "string");
        metaData.put("emailAddress", "string");

        return metaData;
    }
}
