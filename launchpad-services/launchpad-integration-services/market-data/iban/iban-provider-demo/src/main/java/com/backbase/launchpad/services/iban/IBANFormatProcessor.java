package com.backbase.launchpad.services.iban;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

/**
 * Camel processor computing providing a list of IBAN formats.
 */
public class IBANFormatProcessor implements Processor {

    private static List<IBANFormat> ibans = new ArrayList<IBANFormat>();

    @Override
    public void process(Exchange exchange) throws Exception {
        JSONArray result = new JSONArray();
        for (IBANFormat iban : ibans) {
            JSONObject ibanAsJson = new JSONObject();
            ibanAsJson.put("country_code", iban.getCountryCode());
            ibanAsJson.put("country_name", iban.getCountryName());
            ibanAsJson.put("iban_length", iban.getLength());
            ibanAsJson.put("iban_regex", iban.getRegex());
            result.add(ibanAsJson);
        }

        exchange.getOut().setBody(result.toString());
    }

    static {
        ibans.add(new IBANFormat("NL", "The Netherlands", 18, "^NL\\d{2}[A-Za-z]{4}\\d{10}$"));
        ibans.add(new IBANFormat("HU", "Hungary", 28, "^HU\\d{26}$"));
        ibans.add(new IBANFormat("IE", "Ireland", 22, "^IE\\d{2}[A-Za-z]{4}\\d{14}$"));
        ibans.add(new IBANFormat("DE", "Germany", 22, "^DE\\d{20}$"));
        ibans.add(new IBANFormat("GR", "Greece", 27, "^GR\\d{25}$"));
        ibans.add(new IBANFormat("GB", "United Kingdom", 22, "^GB\\d{2}[A-Za-z]{4}\\d{14}$"));
    }
}
