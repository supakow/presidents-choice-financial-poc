package com.backbase.launchpad.services.iban;

import org.apache.camel.Endpoint;
import org.apache.camel.EndpointInject;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

/**
 * Camel route providing the IBAN format list.
 */
@Component
public class IBANFormatRoute extends RouteBuilder {

    // main endpoint
    @EndpointInject(uri = "seda:provider/market-data/iban")
    private Endpoint iban;

    @Override
    public void configure() throws Exception {
        from(iban).process(new IBANFormatProcessor());
    }
}