package com.backbase.launchpad.services.iban;

/**
 * Represents an IBAN format for a country.
 */
public class IBANFormat {

    private String countryCode;
    private String countryName;
    private int length;
    private String regex;

    public IBANFormat(String countryCode, String countryName, int length, String regex) {
        this.countryCode = countryCode;
        this.countryName = countryName;
        this.length = length;
        this.regex = regex;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public int getLength() {
        return length;
    }

    public String getRegex() {
        return regex;
    }
}
