package com.backbase.launchpad.services.currency.rate;

import java.util.ArrayList;
import java.util.List;

public class CurrencyRates {

    private List<CurrencyRate> preferred = new ArrayList<CurrencyRate>();
    private List<CurrencyRate> rest = new ArrayList<CurrencyRate>();

    public void addToPreferred(CurrencyRate currencyRate) {
        preferred.add(currencyRate);
    }

    public void addToRest(CurrencyRate currencyRate) {
        rest.add(currencyRate);
    }

    public List<CurrencyRate> getPreferred() {
        return preferred;
    }

    public List<CurrencyRate> getRest() {
        return rest;
    }

}
