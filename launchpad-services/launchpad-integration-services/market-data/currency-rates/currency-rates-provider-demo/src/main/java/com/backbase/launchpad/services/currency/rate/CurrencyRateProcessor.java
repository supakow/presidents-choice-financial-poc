package com.backbase.launchpad.services.currency.rate;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.camel.Endpoint;
import org.apache.camel.EndpointInject;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;

/**
 * Camel processor which returns the currency rates of the
 * default currency ({@see DefaultCurrencyRoute} against all supported currencies. ({@see CurrencyCode})
 */
public class CurrencyRateProcessor implements Processor {

    // main endpoint
    @EndpointInject(uri = "seda:provider/market-data/currency-rates")
    private Endpoint currencyRateEndpoint;

    private static Map<CurrencyCode, CurrencyRates> currencyRatesByCurrency = new HashMap<CurrencyCode, CurrencyRates>();

    /**
     * {@inheritDoc}
     */
    @Override
    public void process(Exchange exchange) throws Exception {
        String currency = (String) exchange.getIn().getHeader("currency");
        if (currency == null) {
            exchange.getOut().setBody(createErrorMessage());
        } else {
            if (isValidCurrency(currency)) {
                CurrencyCode currencyCode = CurrencyCode.valueOf(currency);
                exchange.getOut().setBody(createResponse(currencyCode));
            } else {
                exchange.getOut().setBody(createErrorMessage());
            }
        }
    }

    /**
     * Checks if the given currency valid or not.
     *
     * @param currency currency as a string
     * @return true if valid, false otherwise
     */
    private static boolean isValidCurrency(String currency) {
        for (CurrencyCode currencyCode : CurrencyCode.values()) {
            if (currencyCode.name().equals(currency)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Creates error message if currency is not available
     *
     * @return JSON formatted error message
     */
    private String createErrorMessage() {
        return "{\"errors\" : [{\"code\" : 154, \"message\" : \"Unknown currency\"}]}";
    }

    /**
     * Creates the final response for the given currency code.
     *
     * @param currencyCode a given currency code
     * @return result formatted as a JSON
     */
    private String createResponse(CurrencyCode currencyCode) {
        JSONObject response = new JSONObject();
        CurrencyRates currencyRates = currencyRatesByCurrency.get(currencyCode);
        if (currencyRates != null) {
            response.put("preferred", toJson(currencyRates.getPreferred()));
            response.put("rest", toJson(currencyRates.getRest()));
        }
        return response.toString();
    }

    /**
     * Converts a list of {@link CurrencyRate} to JSON
     *
     * @param currencyRates a list of currency rates.
     * @return currency rates as a JSONArray instance
     */
    private JSONArray toJson(List<CurrencyRate> currencyRates) {
        JSONArray result = new JSONArray();
        for (CurrencyRate currencyRate : currencyRates) {
            JSONObject currencyRateAsJson = new JSONObject();
            currencyRateAsJson.put("currency_code", currencyRate.getCurrencyCode());
            currencyRateAsJson.put("exchange_rate", currencyRate.getExchangeRate());
            result.add(currencyRateAsJson);
        }
        return result;
    }

    // populate currency rates.

    static {

        // from EUR
        CurrencyRates euro = new CurrencyRates();
        euro.addToPreferred(new CurrencyRate(CurrencyCode.USD, 1.38));
        euro.addToPreferred(new CurrencyRate(CurrencyCode.GBP, 0.82));
        euro.addToRest(new CurrencyRate(CurrencyCode.HUF, 310.11));
        euro.addToRest(new CurrencyRate(CurrencyCode.PLN, 4.22));
        euro.addToRest(new CurrencyRate(CurrencyCode.CHF, 1.22));
        currencyRatesByCurrency.put(CurrencyCode.EUR, euro);

        // from USD
        CurrencyRates usd = new CurrencyRates();
        usd.addToPreferred(new CurrencyRate(CurrencyCode.EUR, 0.72));
        usd.addToPreferred(new CurrencyRate(CurrencyCode.GBP, 0.60));
        usd.addToRest(new CurrencyRate(CurrencyCode.HUF, 226.41));
        usd.addToRest(new CurrencyRate(CurrencyCode.PLN, 3.05));
        usd.addToRest(new CurrencyRate(CurrencyCode.CHF, 0.88));
        currencyRatesByCurrency.put(CurrencyCode.USD, usd);

        // from GBP
        CurrencyRates gbp = new CurrencyRates();
        gbp.addToPreferred(new CurrencyRate(CurrencyCode.USD, 1.66));
        gbp.addToPreferred(new CurrencyRate(CurrencyCode.EUR, 1.20));
        gbp.addToRest(new CurrencyRate(CurrencyCode.HUF, 375.08));
        gbp.addToRest(new CurrencyRate(CurrencyCode.PLN, 5.05));
        gbp.addToRest(new CurrencyRate(CurrencyCode.CHF, 1.45));
        currencyRatesByCurrency.put(CurrencyCode.GBP, gbp);

        // from HUF
        CurrencyRates huf = new CurrencyRates();
        huf.addToPreferred(new CurrencyRate(CurrencyCode.USD, 0.0044));
        huf.addToPreferred(new CurrencyRate(CurrencyCode.EUR, 0.0032));
        huf.addToRest(new CurrencyRate(CurrencyCode.GBP, 0.0027));
        huf.addToRest(new CurrencyRate(CurrencyCode.PLN, 0.013));
        huf.addToRest(new CurrencyRate(CurrencyCode.CHF, 0.0039));
        currencyRatesByCurrency.put(CurrencyCode.HUF, huf);

        // from PLN
        CurrencyRates pln = new CurrencyRates();
        pln.addToPreferred(new CurrencyRate(CurrencyCode.USD, 0.33));
        pln.addToPreferred(new CurrencyRate(CurrencyCode.EUR, 0.24));
        pln.addToRest(new CurrencyRate(CurrencyCode.HUF, 74.24));
        pln.addToRest(new CurrencyRate(CurrencyCode.GBP, 0.20));
        pln.addToRest(new CurrencyRate(CurrencyCode.CHF, 1.22));
        currencyRatesByCurrency.put(CurrencyCode.PLN, pln);

        // from CHF
        CurrencyRates chf = new CurrencyRates();
        chf.addToPreferred(new CurrencyRate(CurrencyCode.USD, 1.38));
        chf.addToPreferred(new CurrencyRate(CurrencyCode.EUR, 0.82));
        chf.addToRest(new CurrencyRate(CurrencyCode.HUF, 258.05));
        chf.addToRest(new CurrencyRate(CurrencyCode.PLN, 3.48));
        chf.addToRest(new CurrencyRate(CurrencyCode.GBP, 0.69));
        currencyRatesByCurrency.put(CurrencyCode.CHF, chf);
    }

}
