package com.backbase.launchpad.services.currency.rate;

/**
 * Supported currencies.
 */
public enum CurrencyCode {

    EUR, USD, GBP, HUF, PLN, CHF;

}
