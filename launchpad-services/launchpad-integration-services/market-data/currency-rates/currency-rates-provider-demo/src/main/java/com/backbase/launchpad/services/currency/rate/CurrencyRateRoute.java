package com.backbase.launchpad.services.currency.rate;

import org.apache.camel.Endpoint;
import org.apache.camel.EndpointInject;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

/**
 *  Camel route providing the currency rates for a given currency.
 */
@Component
public class CurrencyRateRoute extends RouteBuilder {

    // main endpoint
    @EndpointInject(uri = "seda:provider/market-data/currency-rates")
    private Endpoint currencyRates;

    @Override
    public void configure() throws Exception {
        from(currencyRates).process(new CurrencyRateProcessor());
    }
}
