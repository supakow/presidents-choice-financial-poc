package com.backbase.launchpad.services.currency.rate;

/**
 * Currency rate related to the default currency. {@see DefaultCurrencyRoute}
 */
public class CurrencyRate {

    private CurrencyCode currencyCode;
    private double exchangeRate;

    public CurrencyRate(CurrencyCode code, double exchangeRate) {
        this.currencyCode = code;
        this.exchangeRate = exchangeRate;
    }

    public CurrencyCode getCurrencyCode() {
        return currencyCode;
    }

    public double getExchangeRate() {
        return exchangeRate;
    }
}
