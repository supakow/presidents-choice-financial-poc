package com.backbase.launchpad.services.currency;

import net.sf.json.JSONObject;

import org.apache.camel.Endpoint;
import org.apache.camel.EndpointInject;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

/**
 * Camel route providing the default currency.
 */
@Component
public class DefaultCurrencyRoute extends RouteBuilder {

    private static final String DEFAULT_CURRENCY = "EUR";

    // main endpoint
    @EndpointInject(uri = "seda:provider/market-data/currency-rates/default")
    private Endpoint defaultCurrencyEndpoint;

    @Override
    public void configure() throws Exception {
        from(defaultCurrencyEndpoint).process(new Processor() {
            @Override
            public void process(Exchange exchange) throws Exception {
                JSONObject response = new JSONObject();
                response.put("currency_code", DEFAULT_CURRENCY);
                exchange.getOut().setBody(response.toString());
            }
        });
    }

}