package com.backbase.launchpad.services.mobile;

import org.apache.camel.Endpoint;
import org.apache.camel.EndpointInject;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Camel route providing the Presentation model route
 */
@Component
public class PresentationModelRoute extends RouteBuilder {

    // main endpoint
    @EndpointInject(uri = "seda:provider/mobile/presentation-model")
    private Endpoint presentationModel;

    @Autowired
    private Processor presentationModelProcessor;

    @Override
    public void configure() throws Exception {
        from(presentationModel).process(presentationModelProcessor);
    }
}