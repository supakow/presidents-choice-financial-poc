package com.backbase.launchpad.services.mobile;

import com.google.common.io.CharStreams;
import com.google.common.io.Closeables;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Camel processor computing providing a mobile presentation model
 */
@Component
public class PresentationModelProcessor implements Processor {

    private static final String SAMPLE_JSON_RESOURCE =
            "/com/backbase/launchpad/services/mobile/sample-retail-presentation-model.json";

    private String sampleJson = null;

    @Override
    public void process(Exchange exchange) throws Exception {

        InputStream sampleJsonStream = null;
        InputStreamReader supplierStream = null;
        try {
            if (sampleJson == null) {
                sampleJsonStream = this.getClass().getResourceAsStream(SAMPLE_JSON_RESOURCE);
                supplierStream = new InputStreamReader(sampleJsonStream, "UTF-8");
                sampleJson = CharStreams.toString(supplierStream);
            }
            exchange.getOut().setBody(sampleJson);
        } finally {
            Closeables.close(sampleJsonStream, false);
            Closeables.close(supplierStream, false);
        }
    }
}
