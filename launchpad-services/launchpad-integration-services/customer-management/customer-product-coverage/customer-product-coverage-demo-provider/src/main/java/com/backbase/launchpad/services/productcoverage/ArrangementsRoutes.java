package com.backbase.launchpad.services.productcoverage;

import org.apache.camel.Endpoint;
import org.apache.camel.EndpointInject;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.backbase.mashup.camel.json.XmlJsonDataFormatWrapper;

@Component
public class ArrangementsRoutes extends RouteBuilder {
    protected static final String XMLDB_CURRENT_ACCOUNT_COLLECTION_URI =
            "xmldb:exist:///db/launchpad/accounts?XPath=//o[partyId = '${header.backbasepartyid}']";
    protected static final String XMLDB_CUSTOMER_COLLECTION_URI =
            "xmldb:exist:///db/launchpad/customers?XPath=//customer[id = '${header.backbasepartyid}']";

    @EndpointInject(uri = "seda:provider/customer-product-coverage/arrangements")
    private Endpoint arrangmentsEntryPoint;

    @Autowired
    private XmlJsonDataFormatWrapper xmlJsonDataFormat;

    @Override
    public void configure() throws Exception {

        from(arrangmentsEntryPoint)
        .recipientList().simple(XMLDB_CUSTOMER_COLLECTION_URI)
        .setHeader("customerFirstName").xpath("/customer/firstName/text()", String.class)
        .setHeader("customerMiddleName").xpath("/customer/middleName/text()", String.class)
        .setHeader("customerLastName").xpath("/customer/lastName/text()", String.class)
        .setHeader("customerFullName").simple("${headers.customerFirstName} ${headers.customerMiddleName} ${headers.customerLastName}")
        .recipientList().simple(XMLDB_CURRENT_ACCOUNT_COLLECTION_URI)
        .to("velocity:accountsWrapper.vm")
        .to("xslt:accountsToArrangements.xsl")
        .convertBodyTo(String.class)
        .marshal(xmlJsonDataFormat)
        .end();
    }
}
