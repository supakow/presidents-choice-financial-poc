<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:param name="customerFullName"/>

    <xsl:template match="/">
        <arrangements class="array">
            <xsl:for-each select="accounts/o">
                <o>
                    <product><xsl:value-of select="name"/></product>
                    <conditions class="array">
                        <condition>
                            <name>Beneficiaries</name>
                            <value><xsl:value-of select="normalize-space($customerFullName)"/></value>
                        </condition>
                        <xsl:if test="groupCode = 'CASH'">
                        <condition>
                            <name>Max. Overdraft</name>
                            <value>5000.00</value>
                            <type>currency</type>
                        </condition>
                        </xsl:if>
                        <xsl:if test="groupCode = 'INVESTMENT'">
                        <condition>
                            <name>APR</name>
                            <value>2.1</value>
                            <type>percent</type>
                        </condition>
                        </xsl:if>
                    </conditions>
                </o>
            </xsl:for-each>
        </arrangements>
    </xsl:template>

</xsl:stylesheet>