/*
 *	----------------------------------------------------------------
 *	Copyright Backbase b.v. 2003/2012
 *	All rights reserved.
 *	----------------------------------------------------------------
 *	Version 5.5
 *	----------------------------------------------------------------
 */

//	Backbase Commander
//  ----------------------------------------------------------------
b$.module('b$.debug', function() {
//  ----------------------------------------------------------------

	var htmlAPI = b$._private.htmlAPI;
	var htmlCoreAPI = b$._private.html;

	var BDOMDocument = b$.bdom.Document;
	var BDOMNamespace = b$.bdom.Namespace;

	var PreferencesView = b$.view.preferences;

	var LayoutElement = b$.bdom.getNamespace('http://backbase.com/2012/view').getClass('layoutElement');
	var DisplayElement = b$.bdom.getNamespace('http://backbase.com/2012/view').getClass('displayElement');

	var Class = b$.Class;

	// CMDR API START
	b$.cmdr = {
		inspecting: false,
		currentHighlightedElm: null,
		refreshMenu: function() {
			var bdom = $('#cmdr-doc-menu', window.cmdrDocument)[0];
			var sOptions = '';
			var nameSpace = '';
			for (var i=0; i < b$.bdom.domImplementation.documents.length; i++) {
				if(window.cmdrDocument != b$.bdom.domImplementation.documents[i]) {
					nameSpace = b$.bdom.domImplementation.documents[i].lookupNamespaceURI().split('/');
					nameSpace = nameSpace[nameSpace.length-1];
					sOptions += '<li><a href="#" onmousedown="b$.cmdr.inspectDocument('+i+')">'+nameSpace+'</a></li>';
				}
			};
			bdom.innerDOM(sOptions);
		},
		inspectDocument: function(i) {
			console.log('Inspect Document '+i)
			if (window.cmdrDocument != b$.bdom.domImplementation.documents[i]) {
				b$.debug.cmdr.inspectDocument(b$.bdom.domImplementation.documents[i]);
			}
		},
		inspectElements: function() {
			(b$.cmdr.inspecting == true) ? b$.cmdr.stopInspect() : b$.cmdr.startInspect();
		},
		inspectHandler: function(event) {
			var oViewController, //  = b$.cmdr.getViewControllerFromHtml(event.target);
				event = event || window.event;

			if (b$.cmdr.higlighter) b$.cmdr.higlighter.style.display = 'none';
			oViewController =  b$.cmdr.getViewControllerFromHtml(document.elementFromPoint(event.x || event.clientX, event.y || event.clientY));
			if (!oViewController || oViewController.localName == "application") return;
			//console.log(oViewController);

			for (var i=0; i < b$.bdom.domImplementation.documents.length; i++) {
				if (window.cmdrDocument != b$.bdom.domImplementation.documents[i] && oViewController.ownerDocument.documentElement == b$.bdom.domImplementation.documents[i].documentElement) {
					b$.cmdr.inspectDocument(i);
				}
			};
			window.cmdrDocument.all['inspector'].inspect(oViewController);

			for (x in window.cmdrDocument.all) {
				if (window.cmdrDocument.all[x]._jxid == 'DEBUG-' + oViewController._jxid) {
					htmlAPI.addClass(window.cmdrDocument.all[x].htmlNode.firstChild, 'bc-treerow-selected');
				};
			}
			b$.cmdr.currentHighlightedElm = null;
			b$.cmdr.stopInspect();
		},
		highlightElement: function(event) {
			var oViewController, // = b$.cmdr.getViewControllerFromHtml(document.elementFromPoint(event.x, event.y)); //event.target),
				highlighter = b$.cmdr.higlighter,
				highlighterStyle = highlighter ? highlighter.style : null,
				_oViewController,
				oVCLocalName,
				event = event || window.event;

			if (!highlighter) {
				highlighter = b$.cmdr.higlighter = document.body.parentNode.appendChild(document.createElement('div'));
				highlighter.className = 'bc-cmdr-inspector';
				highlighterStyle = highlighter.style;
			}
			highlighterStyle.display = 'none'; // to get right element with document.elementFromPoint
			oViewController = b$.cmdr.getViewControllerFromHtml(document.elementFromPoint(event.x || event.clientX, event.y || event.clientY));
			oVCLocalName = oViewController && oViewController.localName ? oViewController.localName : null;

			if ((oViewController && oVCLocalName != 'application' && oVCLocalName != 'treeitem' && oViewController.model) ||
					(b$.cmdr.currentHighlightedElm == oViewController && highlighterStyle)) {
				highlighterStyle.display = 'block';
			}

			// stop here if ... and the same element as before
			if (!oViewController || !oViewController.model || oVCLocalName == 'application' || b$.cmdr.currentHighlightedElm == oViewController ||
					(oViewController.model.namespaceURI && oViewController.model.namespaceURI.indexOf('UADisplay') != 0)) {
				return;
			}

			b$.cmdr.currentHighlightedElm = oViewController;
			
			if (oVCLocalName == 'treeitem') {
				_oViewController = oViewController.model; // b$.cmdr.getViewControllerFromHtml(oViewController.model.htmlNode);
				if (_oViewController) {
					highlighterStyle.display = 'block';
					oViewController = _oViewController;
				}
			}

			var dims = htmlAPI.getBoxObject(oViewController.htmlNode);
			highlighterStyle.cssText = 'top:'+(dims.top+1)+'px;left:'+(dims.left+1)+'px;width:'+(dims.w-2)+'px;height:'+(dims.h-2)+'px;display:block';
		},
		getViewControllerFromHtml: function(elm) {
			while (elm) {
				if (elm.viewController) {
					return elm.viewController;
				}
				var elm = elm.parentNode;
			}
		},
		startInspect: function(event) {
			b$.cmdr.inspecting = true;

			b$._private.html.addEventListener(document, 'mousedown', b$.cmdr.inspectHandler);
			b$._private.html.addEventListener(document, 'mousemove', b$.cmdr.highlightElement);
		},
		stopInspect: function(event) {
			b$._private.html.removeEventListener(document, 'mousedown', b$.cmdr.inspectHandler);
			b$._private.html.removeEventListener(document, 'mousemove', b$.cmdr.highlightElement);
			if (Inspector && Inspector._toggle_event_Listeners) Inspector._toggle_event_Listeners();

			// htmlAPI.removeClass(b$.cmdr.currentHighlightedElm.htmlNode, 'bc-cmdr-inspected');
			if (b$.cmdr.higlighter) b$.cmdr.higlighter.style.cssText = 'top:-10px;left:-10px;width:0px;height:0px';

			b$.cmdr.inspecting = false;
		}
	};

	var dragDefaultHandlers = b$.portal.defaultDragHandlers;

	var Commander = Class.extend(function(){
	},{
		initialized: false,
		document: null,


		log:  function(msg){
			this.cmdrDocument.dispatchCustomEvent('log',false,false,{msg:msg});
		},
		inspectDocument:  function(bdocModel, callback){
			var This = this;
			this.init(function(){
//				this.inspectDocument(bdoc);

				if(This.currentDocument) {
					This.domReflector.disconnectFromModel(This.currentDocument, This.cmdrDocument);

					// Clear tree...
					var belm = This.cmdrDocument.all['portals'];
					var kids = belm.childNodes;
					for(var i=0;i<kids.length;i++){
						kids[i].destroy();
					}
				}

				This.currentDocument = bdocModel;

				var model = bdocModel.documentElement;
				var belm = This.cmdrDocument.all['portals'];

				This.domReflector.reflectDeep(model, This.cmdrDocument, null, belm);
				This.domReflector.connectToModel(bdocModel, This.cmdrDocument);

			})
		},
		init:  function(callback){
			if(!this.initialized){
				this.initialized = true;

				var bdoc = b$.bdom.domImplementation.createDocument();
				this.cmdrDocument = bdoc;
				bdoc.insertDisplayChild = function(bdom){
		//			document.body.parentNode.appendChild(bdom.htmlNode);
				};
				bdoc.bMoveDisplayChildren = true;

				window.cmdrDocument = bdoc;

				// Drag and Drop
				if (b$.portal.portalView) {
					bdoc.dragManager = b$.portal.portalView.dragManager;
				}
				else {
					var DnDnR = new b$.view.bdom.dd.DnDnR();
					DnDnR.initialize(document, bdoc);
					bdoc.dragManager = DnDnR;
				}



				bdoc.addEventListener('dbg-inspect-node', function(event){
					bdoc.all['inspector'].inspect(event.detail.node);
				})

				bdoc.addEventListener('xxx*', function(event){
					if(event.type != 'mousemove')	{
						console.log('DEBUG : ',event);
					}
				})


				var openDialog = null;
				bdoc.addEventListener('mousedown', function(event){
					if(event.target) {
					}
					var html = event.htmlTarget;
					var dt = html.getAttribute('data-toggle');
					if(dt=='dropdown'){
						if($(html.parentNode).hasClass('bc-open')){
							$(html.parentNode).removeClass('bc-open');
							openDialog = null;
						}
						else {
							if(openDialog)$(openDialog).removeClass('bc-open');
							openDialog = html.parentNode;
							$(html.parentNode).addClass('bc-open');
						}
					}
					else {
						if(openDialog)$(openDialog).removeClass('bc-open');
						openDialog = null;
					}
				})


				b$._private.html.addEventListener(document, 'keydown', function(ev){
					if(ev.keyCode == 27) {
//						myExplorer.toggleWindow();
						bdoc.documentElement.toggleWindow();
					}
				});

				this.getReflector();


				// URI resolve for the xml and css
				var aScripts = document.getElementsByTagName('script');
				var thisFile;
				for (var i = 0; i < aScripts.length; i++) {
					if (aScripts[i].src.indexOf('b$.debug.js') !== -1) {
						thisFile = aScripts[i];
						break;
					};
				};

				var URI = b$._private.uri.URI;
				var oScriptUri = new URI(thisFile.src);
				var base = new URI(location.href);
				var commanderLoc = new URI('../dom-commander.xml');

				var oThisScript = oScriptUri.resolve(base, oScriptUri);
				var CommanderXML = commanderLoc.resolve(oThisScript, commanderLoc);

                var element = document.createElement('link');
                element.type = 'text/css';
                element.rel = 'stylesheet';
                var cssLoc = new URI('../css/backbase-portal-debug.css');
                var cssURI = cssLoc.resolve(oThisScript, cssLoc);
                element.href = cssURI.getPath();
				document.getElementsByTagName('head')[0].appendChild(element);

				b$(bdoc).load(CommanderXML.getPath(), function(){
					if(callback)callback();
				});

			}
			else if(callback)callback();

		},
		getReflector:  function(){
			if(!this.domReflector) {

				var DOMReflector = b$.bdom.util.DOMReflector
				var RFL_DOM2Control = b$.bdom.util.RFL_DOM2Control
				var DOMTextReflection = b$.bdom.util.DOMTextReflection;

				var domReflector = new DOMReflector();
				domReflector.getID = function(model) {
		//			console.dir(model)
					return 'DEBUG-'+model._jxid;
				}
				domReflector.addToSet('default',[
					new RFL_DOM2Control({
						doMatch: function(model) {
							return true;
							return (model.nodeType==1)?true:false;
						},
						ns: 'http://backbase.com/2012/bc',
						localName: 'treeitem',
						render: true,
						initInstance: function(bdom, model) {
							bdom.model = model;
							var s;
								//escape = b$.escapeXML;

							if(model.nodeType==1) {
			//					bdom.setAttribute('label', (model.prefix)? model.prefix +':'+model.localName : model.localName);
								bdom.setAttribute('label', b$.escapeXML(model.nodeValue));
								s = '<'+((model.prefix) ? (model.prefix+':'+model.localName) : model.localName);
								for(var i=0;i<model.attributes.length;i++){
									if(model.attributes[i].nodeName!='_jxid')
									s += ' '+model.attributes[i].nodeName+'="'+model.attributes[i].nodeValue+'"';
									// s += ' <span class="bc-node-attribute">'+model.attributes[i].nodeName+'="<span class="bc-node-attribute-value">'+escape(model.attributes[i].nodeValue)+'</span>"</span>';
								}
								s += '>';
							}
							else {
								s = model.nodeValue;
							}
							bdom.setAttribute('label', b$.escapeXML(s));
						}
					}),
					new DOMTextReflection({
						initInstance: function(bdom, model) {
							bdom.model = model;
							bdom.setAttribute('label', b$.escapeXML(model.nodeValue));
						}
					})
				]);


				this.domReflector = domReflector;
			}
			return this.domReflector;
		}

	},{
	});



	var cmdr = new Commander();
	b$.debug.cmdr = cmdr;

//  ----------------------------------------------------------------
	this.createDebugDocument = function (bdocModel) {
//  ----------------------------------------------------------------
		cmdr.inspectDocument(bdocModel);
		return;

		return bdocDebug;
	}


//  ----------------------------------------------------------------
//  ----------------------------------------------------------------
	var BaseElement = DisplayElement.extend(null,{
		cnBasePrefix:'bc-',
		localName:'baseElement',
		namespaceURI:'http://backbase.com/2012/bc'
	});

//  ----------------------------------------------------------------
//  ----------------------------------------------------------------
	var Element = LayoutElement.extend(function (bdomDocument, node) {
		LayoutElement.apply(this, arguments);

//			layout: new layouts.AutoLayout(this);
//			this.layout = new b$.view.bdom.layout['default'](this);
//			this.layout = new layouts.BoxLayout(this);
//			this.layout = new b$.view.bdom.layout.AutoLayout(this);

		if(this.ownerDocument.bcCheckCommand)
			this.ownerDocument.bcCheckCommand(this);

	},{
		cnBasePrefix: 'bc-',
		localName:'element',
		namespaceURI:'http://backbase.com/2012/bc',
		getAreaOrderedChildren: function(){
			return [this.childNodes];
		},
		mapAttribute:function(name, value){
			console.log('Map Attribute')
			switch(name){
				case 'test':
					alert(value)
					break;
			}
		}
	}, {
		handlers: {
		}
	});


	var Panel = Element.extend(null,{
		localName:'panel'
	}, {
		template:function(t){
			return '<div class="bc-box bc-panel '+t.cnBase+' '+t.getAttribute('class')+' --area" style="overflow:auto;'+t.getAttribute('style')+'"></div>';
		},
		handlers: {
		},
		attributes: {
			layout: 'AutoLayout',
			orient: 'vertical',
			flex: '1',
			margin: '0',
			padding: '0'
		}
	});


	var Vbox = Element.extend(null,{
		localName:'vbox'
	}, {
		template:function(t){
			var r = window.Math.floor(window.Math.random() * 99) + 10;
			var s = 'background-color:#'+r+''+r+'00;';
//			return '<div class="bc-box bc-vbox" style="'+s+this.getAttribute('style')+'"></div>';
			return '<div class="bc-box bc-vbox"></div>';
		},
		handlers: {
		},
		attributes: {
			layout:'BoxLayout',
			orient: 'vertical',
			flex:'1',
			margin:'0',
			padding: '0'
		}
	});


	var Hbox = Element.extend(null,{
		localName:'hbox'
	}, {
		template:function(){
			var r = window.Math.floor(window.Math.random() * 99) + 10;
			var s = 'background-color:#'+r+'00'+r+';';
//			return '<div class="bc-box bc-hbox" style="'+s+this.getAttribute('style')+'"></div>';
			return '<div class="bc-box bc-hbox"></div>';
		},
		handlers: {
		},
		attributes: {
			layout:'BoxLayout',
			orient: 'horizontal',
			flex:'1',
			margin:'0',
			padding: '0'
		}
	});


	var Border = Element.extend(null,{
		localName:'border',
		orient: 'vertical',
		layout: 'BoxLayout',
		xsetWidthHeight: function(){
			Element.prototype.setWidthHeight.apply(this, arguments);
		}
	},{
		template: function(t){
			return '<div class="'+t.cnBase+'"><div class="'+t.cnBase+'--area"></div></div>';
		},
		handlers: {
		}
	});







	var Splitter_current = null;
	var Splitter_onmouseup = function(event){
		document.removeEventListener('mouseup', Splitter_onmouseup);
		document.removeEventListener('mousemove', Splitter_onmousemove);
		Splitter_current = null;
	}
	var Splitter_onmousemove = function(event){
		var prev = Splitter_current.previousSibling;
		if(prev) {
//			prev.maxHeight = Splitter_nStartH + event.clientY - Splitter_nStartY;
			prev.height = Splitter_nStartH + event.clientY - Splitter_nStartY;
			prev.maxHeight = 0;
			prev.flex = 0;

//			console.log(Splitter_nStartH + event.clientY - Splitter_nStartY)
//			console.log(prev.maxHeight)
			prev.parentNode.reflow();
		}

	}


	var Splitter = Element.extend(null,{
		localName:'splitter',
		xsetWidthHeight: function(){
			Element.prototype.setWidthHeight.apply(this, arguments);
		},
		onmouseup: function(event){
			document.removeEventListener('mouseup', this.onmouseup);
		}
	},{
		template: function(t){
			return '<div class="bc-box '+t.cnBase+' --area"></div>';
		},
		handlers: {
			'mousedown':function(event){
				if(event.target==this){

					Splitter_current = this;
					Splitter_sibling = this.previousSibling;

					Splitter_nStartX = event.clientX;
					Splitter_nStartY = event.clientY;
					Splitter_nStartH = Splitter_sibling.htmlNode.offsetHeight;

					document.addEventListener('mouseup', Splitter_onmouseup);
					document.addEventListener('mousemove', Splitter_onmousemove);
					event.preventDefault();
				}
			}
		},
		attributes: {
			layout:'AutoLayout',
			orient: 'horizontal',
			flex:'0',
			width:'100',
			height:'10',
			margin:'0',
			padding: '0'
		}
	});





	var oldLeft = 0;
	var oldTop = 0;

	var Explorer = Element.extend(null,{
		localName:'explorer',

		toggleWindow:function(){
//			var myWindow = $('window',this.ownerDocument)[0];
//			var myWindow = $('div',this)[0];
//			debugger;
//			var myWindow = $('bc:rdock', this)[0];
//			var myWindow = jQuery('bc:rdock', this)[0];
//			var myWindow = b$.ua.querySelectorAll(this.ownerDocument, 'bc:rdock')[0];

			var myWindow = b$.ua.querySelectorAll(this.ownerDocument, 'rdock')[0];

			if(myWindow){

				if(myWindow.place == 'open') {
					myWindow.setPlace('close');
				}
				else {
					myWindow.setPlace('open');
				}
				return;

				if ($(myWindow.htmlNode).hasClass('bb-debugger-fade-in')){
					oldLeft = myWindow.htmlNode.offsetLeft;
					oldW = myWindow.htmlNode.offsetWidth;
					oldTop = myWindow.htmlNode.offsetTop;

					$(myWindow.htmlNode).removeClass('bb-debugger-fade-in');
					$(document.body).removeClass('bb-debugger-fade-in');
				}
				else {
					$(myWindow.htmlNode).addClass('bb-debugger-fade-in');
					$(document.body).addClass('bb-debugger-fade-in');
				}
			}

		}

	});

	var RDock = Element.extend(null,{

		localName: 'rdock',
		DOMReady: function(){

			this.setPlace('open');

			return;
			this.margins = {
				top:51,
				right:5,
				bottom:5,
				left:5
			};
//			this.setPlace('open');
			return;

			document.body.style.position = 'relative';
//			document.body.style.left = '0px';
//			document.body.style.width = '1200px';

//				if (!$(document.body).hasClass('fade'))
					$(document.body).addClass('bb-debugger-fade').addClass('bb-debugger-fade-in');

			var r = b$._private.dimCalc.getViewportWH();

			this.htmlNode.style.height = r.h+'px';
			this.htmlNode.style.width = (r.w-1201)+'px';

//			this.htmlNode.style.left = document.body.offsetWidth+'px';
//			this.htmlNode.style.top = '0px';
//			this.htmlNode.style.left = '1200px';
//			this.htmlNode.style.right = '0px';
//			this.htmlNode.style.width = '500px';
//			this.htmlNode.style.height = document.body.clientHeight+'px';
			//xthis.htmlNode.style.position = 'static';
		},
		setPlace:function(type){

			if(type==='open'){
				document.body.style.position = 'relative';
//				document.body.style.left = '0px';


				$(document.body).addClass('bb-debugger-fade').addClass('bb-debugger-fade-in');
				$(this.htmlNode).addClass('bb-debugger-fade-in');
//				this.htmlNode.style.top = '0px';
//				this.htmlNode.style.left = (b$._private.html.getViewportWidth()-501)+'px';
//				this.resize(500, b$._private.html.getViewportHeight());
			}
			else {//close
				document.body.style.position = 'relative';
//				document.body.style.left = '0px';
				$(document.body).removeClass('bb-debugger-fade-in');
				$(this.htmlNode).removeClass('bb-debugger-fade-in');
			}
			this.place = type;
		},

		injectDisplaySelf:function(){
			document.body.parentNode.appendChild(this.htmlNode);
		},
		init:function(ev){
		}

	},{
		template:function(t){
			return '<div class="'+t.cnBase+' bb-debugger-fade bb-debugger-fade-in " style="">'+
						'<div style="height:100%;position:relative;" class="'+t.cnBase+'-body '+t.cnBase+'--area">' +
						'</div>' +
					'</div>';
		},
		handlers: {
			DOMReady: function(event){
//				console.log('RDOCK DOMReady')
			//	document.body.parentNode.appendChild(this.htmlNode);
			},
			mousedown: function(event){
//				if(event.target == this){
					var trg = this.getDisplayEventTarget(event.htmlTarget);
					if(this.isDisplay('act',trg)){
						var v = trg.getAttribute('data-action');
						switch(v){
							case 'cmdr-cache':
								console.log('data-action : '+v);
								b$.portal.portalServer.clearCache();
								break;
						}
					}
//				}
			}
		},
		attributes: {
			layout: 'BoxLayout',
			orient: 'vertical',
			flex: '1'
		}

	});



	var Toolbar = Element.extend(null,{
		localName:'toolbar'
	},{
		template:function(t){
			return '<div class="'+t.cnBase+'">XXXXXXXXXXXXX<div class="'+t.cnBase+'--area"></div></div>';
			return '<div class="'+t.cnBase+'"><div class="'+t.cnBase+'--area"></div></div>';

		},
		handlers: {
		}
	});


	var Inspector = Element.extend(function (bdomDocument, node) {
		Element.apply(this, arguments);
	},{
		localName:'inspector',
		orient: 'vertical',
		layout: 'BoxLayout',

		inspectClass:function(Klass){
			var s = '';
			this.currClass = Klass;
			s += this.print_Class(Klass);
			this.getDisplay('label').innerHTML = s;

		},
		inspect:function(bdom, sMode){

			if(bdom) {
				if(this.pf) {
					this.pf.destroy();
					this.pf = null;
				}
				if(!sMode) sMode = this.currMode || 'dom';
				this.currMode = sMode;
				this.getDisplay('label').innerHTML = this.print_All(bdom, sMode);
				this.currObj = bdom;
			}
		},
		print_All:function(bdom, sMode){
			var s = this.print_Header(bdom, sMode);
			s += '<div class="bc-print-area">';

			if (sMode !='evts') this._toggle_event_Listeners();

			if(sMode =='dom'){
				s += this.print_DOM(bdom, sMode);
			}
			else if(sMode =='class'){
				s += this.print_Class(bdom, sMode);
			}
			else if(sMode =='pref'){
//				s += this.print_Pref(bdom, sMode);

				// var p=null;
				// if(bdom.model && bdom.model.getPreference)
				// 	p = bdom.model;
				// else if(bdom.getPreference)
				// 	p = bdom;
				// if(p) {
				// 	var pf = this.ownerDocument.createElementNS('http://backbase.com/2012/view', 'PreferenceForm');
				// 	pf.buildFieldsFromPreferences(p);
				// 	this.appendChild(pf);
				// 	this.pf = pf;
				// }
			
				// for now, to show at least something...
				s += '<table class="bc-data-logger">';
				for (var n = 0, prefs = bdom.model.preferences.array, m = prefs.length; n < m; n++) {
					s += '<tr><td class="bc-data-name" align="right"' + (prefs[n].label ? ' title="' +prefs[n].label + '"' : '') + '>' +
					      prefs[n].name + ':</td><td>' +
					      prefs[n].value + '</td></tr>';
				}
				s += '</table>';
			}
			else if(sMode =='evts'){
				s += this.print_Evts(bdom, sMode);
			}
			else if(sMode =='sec'){
				s += this.print_Sec(bdom, sMode);
			}
			else if(sMode =='inh'){
				s += this.print_Inh(bdom, sMode);
			}


			s += '</div>';
			return s;
		},
		print_Header:function(bdom, sMode){
			var s = '';
			var title = '';
			if(bdom.getPreference)
				title = bdom.getPreference('title');

			s += '<div class="bc-floating-header">';
			s += '<div class="bc-element-breadcrump" Xstyle="padding-left:10px;">';

			var elm = bdom,
				elms = [];

			while(elm) {
				if(elm.parentNode) elms.push('<span' + (elm == bdom ? ' class="bc-breadcrump-current"' : '') + '>'+elm.localName+'</span>');// (elm.id ? '#' + elm.id : '')+ // s += ;//  &gt; 
				elm = elm.parentNode;
			}
			s += elms.reverse().join('');

//			s += '<big><span class="bc-color3">'+bdom.localName+'</span>:<span class="bc-color4">'+title+'</span></big>';
//			s += '<span class="bc-color3">'+bdom.namespaceURI+'</span>';

//			s += '<small class="bc-color2">('+bdom._jxid+')</small>';

			if(bdom.save) {
				s += '<button class="bc-btn bc-btn-mini bc-btn-primary --act" type="button" data-action="cmdr-save">save</button> ';
			}

			s += '</b></div>';

			s += '<ul Xstyle="float:right;" class="bc-nav-ul bc-nav bc-nav-pills">' +
				'<li class="bc-nav-li --act'+((sMode=='dom')?' active':'')+'" data-action="insp-dom"><a href="#">DOM</a></li>' +
				'<li class="bc-nav-li --act'+((sMode=='class')?' active':'')+'" data-action="insp-class"><a href="#">Class</a></li>' +
				'<li class="bc-nav-li --act'+((sMode=='pref')?' active':'')+'" data-action="insp-pref"><a href="#">Preferences</a></li>' +
				'<li class="bc-nav-li --act'+((sMode=='evts')?' active':'')+'" data-action="insp-evts"><a href="#">Events</a></li>' +
				'<li class="bc-nav-li --act'+((sMode=='sec')?' active':'')+'" data-action="insp-sec"><a href="#">Security</a></li>' +
				'<li class="bc-nav-li --act'+((sMode=='inh')?' active':'')+'" data-action="insp-inh"><a href="#">Inherit</a></li>' +
				'</ul>';

			s += '</div>';
//			s += '<hr style="clear:both;"/>';
			// s += '<div style="clear:both;border-top:1px solid #333;"></div>';
			return s;
		},

		print_DOM:function(bdom, sMode){
			var s = '';
			//s += '<div>';
			s += '<div>';
				s += '<b class="bc-print-area-headline">Properties</b>';
			s += '</div>';

			switch(bdom.nodeType){
				case 1:
					s+='<table class="bc-table-striped" style="width:100%;">';
						s+='<tr><td>localName</td><td>'+bdom.localName+'</td></tr>';
						s+='<tr><td>baseURI</td><td>'+bdom.baseURI+'</td></tr>';
						s+='<tr><td>namespaceURI</td><td>'+bdom.namespaceURI+'</td></tr>';
						s+='<tr><td>prefix</td><td>'+bdom.prefix+'</td></tr>';
					s += '</table>';
					break;
				case 3:
					s+='<table class="bc-table-striped" style="width:100%;">';
						s+='<tr><td>nodeValue</td><td>'+bdom.nodeValue+'</td></tr>';
					s += '</table>';
					break;
			}
			var atts = bdom.node.attributes
			if(atts) {
				s += '<div>';
					s += '<b class="bc-print-area-headline">Attributes</b>';
				s += '</div>';
				s += '<table class="bc-table-striped --act" style="width:100%;">';
				for(var i=0;i<atts.length;i++) {
	//				s += ' '+ +' = '+ atts[i].nodeValue+'<br/>';
					s +='<tr>';
					s +='<td>'+atts[i].nodeName+'</td>';
					s +='<td data-action="edit">'+atts[i].nodeValue+'</td>';
					s +='</tr>';
				}
				s += '</table>';
			}

			//s += '</div>';

			return s;

		},
		// test dbg print
//		print_Class: function(Class){
		print_Class: function(bdom){
			return this._print_Class(bdom);
		},
		_print_Class: function(bdom){
			var constr = bdom.constructor;
			var aConstructors = [];
			var oRes = {};
			var Class = constr.prototype;
			var keys = [];
			var classkeys = {};

			while(constr){
				aConstructors.push(constr)
				var pro = constr.prototype;
				for(var n in pro) {
					if(pro.hasOwnProperty(n) && n!='toString'){
						if(n!='constructor'){
							if(!oRes[n])oRes[n] = [];
							oRes[n].push(constr);
						}
					}
				}
				classkeys[constr.CLASSID] = [];
				constr = constr.superClass;
			}

			for(var n in oRes) {
				if(oRes.hasOwnProperty(n)){
//					if(!classkeys[oRes[n].CLASSID])
//						classkeys[oRes[n].CLASSID] = [];

					if(oRes[n][0].CLASSID)
						classkeys[oRes[n][0].CLASSID].push(n);
					else
						classkeys[oRes[n][0].CLASSID].push(n);

					keys.push(n);
				}
//				console.log(n,oRes[n]);
			}


//			console.dir(oRes);
			this.currClassRef = oRes;

			var s = '';
			s+='<table class="bc-table-striped" style="width:100%;">';
//			for(var n in oRes) {
			for(var m in classkeys) {
				var k = classkeys[m]
				k.sort();
				for(var i=0;i<k.length;i++) {
					var n = k[i];
					if(n!='constructor'){
//						console.log(n);
						s+='<tr>';

						// Property
						if(typeof bdom[n] == 'function'){
							var x = bdom[n]+'';
							x=x.replace('"','\"')
							s+='<td><b><a title="'+b$.escapeXML(x)+'">'+n+'</a></b></td>';
						}
						else
							s+='<td>'+n+'</td>';

						// Class name
						if(oRes[n]) {
//							s+='<td class="--act" data-action="insp-class">'+oRes[n][0].CLASSID+'</td>';
							s+='<td class="--act" data-action="insp-class">';
							s+=oRes[n][0].CLASSID+' ';
							s+='<small>';
							for(var x=1;x<oRes[n].length;x++) {
								s+=oRes[n][x].CLASSID+' ';
							}
							s+='</small>';
							s+='</td>';
						}
						else
							s+='<td class="">'+'</td>';

						s+='</tr>';
					}
				}
			}
/*
			for(var i=0;i<keys.length;i++) {
				var n = keys[i];
				if(n!='constructor'){
					console.log(n);
					s+='<tr>';
					s+='<td>'+n+'</td>';
					if(oRes[n])
						s+='<td class="--act" data-action="insp-class">'+oRes[n].CLASSID+'</td>';
					else
						s+='<td class="">'+'</td>';
					s+='</tr>';
				}
			}
*/
			s+='</table>';
			return s;

		},
		print_Pref:function(bdom, sMode){
			var s = '';
			s+='<div><big class="bc-color3">Preferences</big></div>';
			return s;
		},
		print_Evts:function(bdom, sMode){
			var $container,
				/**
				 * prints events from element or its children (1st degree)
				 * into the $container...
				 * @param  {event} event
				 */
				printEvents = function (event) {
					var target = event.target,
						aStackTargets = event.aStackTargets, // _jxid, nodeName
						title = "--- aStackTargets ---\n";

					for(var n in aStackTargets) {
						title += n + ': ' + aStackTargets[n].node.nodeName + (aStackTargets[n]._jxid ?  '#' + aStackTargets[n]._jxid : '') + "\n";
					}
					if (bdom == event.target || (event.target.parentNode && event.target.parentNode == bdom)) { //.viewContext
						if (!$container) { // data log container
							$container = $('.bc-data-logger');
							$container.on('click', 'a', function(event){return false})
							$container.html('');
						}

						$container.append('<tr class="bc-data-' + (event.view ? 'v': 'm') + '"><td align="right"><b>' + (event.view ? 'v': 'm') + '<b></td>' +
							'<td align="right"><span>' + event.type + '</span>:</td>' +
							'<td class="bc-data-name"><a href="#" title="' + title + '">' + event.target.localName  + '</a><i>#' + event.target._jxid + '<i></td></tr>');
					}
				};

			this._toggle_event_Listeners();
			this._toggle_event_Listeners({'model' : bdom, 'view' : this}, printEvents);

			var s = '';
			s += '<table class="bc-data-logger"><tr><td>...listening to events</td></tr></div>';
			return s;
		},
		print_Sec:function(bdom, sMode){
			var s = '';
			s+='<div><big class="bc-color3">Security</big></div>';
			return s;
		},
		print_Inh:function(bdom, sMode){
			var s = '';
			s+='<div><big class="bc-color3">Inhertance</big></div>';
			return s;
		},
		_toggle_event_Listeners:function(models, callback){
			var start = false;
			// caching functionality for dtaching events
			if (!models) {
				models = this._toggle_event_Listeners.models;
				callback = this._toggle_event_Listeners.callback;
			} else {
				this._toggle_event_Listeners.models = models;
				this._toggle_event_Listeners.callback = callback;
				start = true;
			}

			for (var n in models) {
				if (models[n] && models[n].ownerDocument) models[n].ownerDocument[start ? 'addEventListener' : 'removeEventListener']('*',function(evt){
					if(evt.type.indexOf('mouse')!=0)
						callback(evt);
				}, true);
			}
		}
	}, {
		template: function(t){
			return '<div class="'+t.cnBase+'"><div class="inspect--label"></div><div class="--area"></div></div>';
		},
		handlers: {
			DOMFocusIn: function(event){
//				console.log('FOCUS ', event.htmlTarget, event)
//				event.htmlTarget.style.backgroundColor = '#f00';
			},
			DOMFocusOut: function(event){
//				console.log('BLUR ', event)
//				event.htmlTarget.style.backgroundColor = '#f0f';
				var name = event.htmlTarget.previousSibling.innerHTML;
				var value = event.htmlTarget.innerHTML;
				if(name)
				this.currObj.setAttribute(name, value);
			},
			mousedown: function(event){
				if(event.target == this){
					var trg = this.getDisplayEventTarget(event.htmlTarget);
					console.log(trg);
					if(this.isDisplay('act',trg)){
						var v = trg.getAttribute('data-action');
						switch(v){
							case 'XXinsp-class':
								if(this.currClassRef){
									var name = trg.previousSibling.innerHTML;
									this.inspectClass(this.currClassRef[name]);
		//.							this.inspectClass(curr[name].CLASSID);
								}
								break;
							case 'insp-dom':
								this.inspect(this.currObj, 'dom');
								break;
							case 'insp-class':
								this.inspect(this.currObj, 'class');
								break;
							case 'insp-pref':
								this.inspect(this.currObj, 'pref');
								break;
							case 'insp-evts':
								this.inspect(this.currObj, 'evts');
								break;
							case 'insp-sec':
								this.inspect(this.currObj, 'sec');
								break;
							case 'insp-inh':
								this.inspect(this.currObj, 'inh');
							case 'cmdr-save':
								console.log('Saving...', this);
								this.currObj.save();
								break;
						}

						if(event.htmlTarget.getAttribute('data-action')=='edit') {
	//						console.log('EDIT')
							event.htmlTarget.contentEditable = true;
	//						var input = this.getDisplay('input');
/*
							var currObj = this.currObj;
							event.htmlTarget.addEventListener('blur',function(event){

								currObj.setAttribute(name, event.target.innertText);
							});
	//						input.
*/
						}

					}
				}
			}
		}
	});




	var Tree = Element.extend(function (bdomDocument, node) {
		Element.apply(this, arguments);
	},{
		localName:'tree',
		startDrag:function(item){
//			console.log('startDrag' ,this.dragItem)
			b$.ua.setMouseTrap(this);
			this.dragItem = item;
		},
		endDrag:function(target){
//			console.log('endDrag' ,this.dragItem)
			if(this.dragItem){
//				this.htmlNode.style.backgroundColor = '';
				b$.ua.releaseMouseTrap(this);



				if((this.localName || this.tagName.toLowerCase()) == 'echo-namespaces') { // !localName in IE8 .. should we do this to any?
//					console.dir(target);
					if(target && target.model) {
						var localName = this.dragItem.getAttribute('label');
						var ns = this.dragItem.parentNode.getAttribute('label');

						var node = target.model.ownerDocument.createElementNS(ns,localName);
						node.setAttribute('style','')
						target.model.appendChild(node);
						var txt = target.model.ownerDocument.createTextNode('Hello World!');
						node.appendChild(txt);
	//					target.model.parentNode.insertBefore(node,target.model);
	//					target.model.parentNode.insertBefore(this.dragItem.model,target.model);
					}
				}
				else if(!_fDOM_isDescendant(target, this.dragItem)){

					target.model.parentNode.insertBefore(this.dragItem.model,target.model);
//					target.model.appendChild(this.dragItem.model);
				}
				this.dragItem = null;
			}
		}
	}, {
		template:function(t){
			return '<div class="'+t.cnBase+' '+t.getAttribute('class')+'" style="position:relative;overflow:auto;padding:10px;'+t.getAttribute('style')+'">'+
//				'<div class="--area" style="position:relative;width:100%;height:100%;"></div></div>';
				'</div>';
		},
		handlers: {
			'mousemove': function(event){
			},
			'mouseup': function(event){
				console.log('mouseup endDrag' ,this.localName)
				this.endDrag(event.target);
			},
			click:function(event){
				if(event.target == this){
					var trg = this.getDisplayEventTarget(event.htmlTarget);
					if(this.isDisplay('act',trg)){
						var v = trg.getAttribute('data-action');
						switch(v){
							case 'cmdr-cut':
								break;
							case 'cmdr-copy':
								var inspector = $('inspector',this.ownerDocument)[0];
								if(inspector && inspector.currObj) {
									this.copyObj = inspector.currObj;
								}
								break;
							case 'cmdr-paste':
								if(this.copyObj) {
									var inspector = $('inspector',this.ownerDocument)[0];
									if(inspector && inspector.currObj) {
										var clone = this.copyObj.cloneNode(true);
										var _jxid = clone._jxid+' copy ' + window.Math.floor(window.Math.random() * 10000000);
										clone._jxid = _jxid;
										clone.setAttribute('_jxid',_jxid);
//										clone.model = this.copyObj.model;
										inspector.currObj.appendChild(clone);
//										inspector.currObj.appendChild(this.copyObj.cloneNode(true));
									}
								}
								break;
							case 'cmdr-delete':
								break;
						}
					}
				}
			}
		}
	});


	var EchoNamespaces = Tree.extend(null,{
		localName:'echo-namespaces',
		DOMReady: function(){
			var namespaces = b$.bdom.namespaces;
			var s = '';
			for(var n in namespaces){
				s+='<bc:treeitem label="'+n+'">';
				for(var m in namespaces[n].classes){
					s+='<bc:treeitem label="'+m+'">';
//					namespaces[n].classes[m]
					s+='</bc:treeitem>';
				}
				s+='</bc:treeitem>';
			}

//			var l = $('*',this.ownerDocument).length;
//			var dt = new Date();
			this.innerDOM(s);
//			dt = new Date()-dt;
//			l = $('*',this.ownerDocument).length - l;
//			console.log('echo-namespaces: '+l+' in '+dt)
		}
	},{
		template:Tree.template,
		handlers: {
			'DOMReady': function(event){
				this.DOMReady();
			},
			'mousemove': function(event){
			},
			'mouseup': function(event){
				this.endDrag(b$.getVC(event.htmlTarget));
			}
		}
	});




	var htmlAPI = b$._private.htmlAPI;

//	var TreeItem = Element.extend(function (bdomDocument, node) {
	var TreeItem = BaseElement.extend(function (bdomDocument, node) {
//		Element.apply(this, arguments);
		BaseElement.apply(this, arguments);
		this.expanded = true;
	},{
		localName:'treeitem',
		expand:function(){
			if(!this.expanded) {
				this.expanded = true;
				var html = this.htmlNode.firstChild.firstChild;
				htmlAPI.removeClass(html, 'bc-tree-collapsed');
				htmlAPI.addClass(html, 'bc-tree-expanded');
				this.htmlNode.lastChild.style.display = 'block';
			}
		},
		collapse:function(){
			if(this.expanded) {
				this.expanded = false;
				var html = this.htmlNode.firstChild.firstChild;
				htmlAPI.removeClass(html, 'bc-tree-expanded');
				htmlAPI.addClass(html, 'bc-tree-collapsed');
				this.htmlNode.lastChild.style.display = 'none';
			}
		},
		toggle:function(){
			if(this.expanded)
				this.collapse();
			else
				this.expand();
		},
		findAncestorTree: function(){
			var parent = this.parentNode;
			while(parent) {
				if(parent instanceof Tree)
					break;
				parent = parent.parentNode;
			}
			return parent;
		},
		getTreePositionOffset: function(){
			var padd = 0;
			var parent = this.parentNode;
			while(parent) {
				if(parent instanceof Tree)
					break;
				padd += 16;
				parent = parent.parentNode;
			}
			return padd;
		},
		DOMReady: function(){
			this.setTreePositionOffset();
			if (this.firstChild) { // this.childNodes.length
				this.toggle();
			}
		},
		setTreePositionOffset: function(){
			if(this.localName == this.parentNode.localName){
				var html = this.parentNode.htmlNode.firstChild.firstChild;
				htmlAPI.addClass(html, 'bc-tree-expanded');
			}
			return true;
			this.htmlNode.firstChild.style.paddingLeft = this.getTreePositionOffset()+'px';
			return true;
		},
		/**
		 * getAttributes: Returns all available attributes of treeitem (sorted alpahbetical)
		 * 
		 * @param  {Boolean} sort: triggers sorting alphabeticaly
		 * @return {Object} attribute:value pair of treeitem
		 */
		getAttributes: function(sort) {
			var attrList = this.model ? this.model.attributes : null,
				attrlistDump = {},
				keys = [],
				tempList = {};

			if (!attrList) return false;

			for (var n = 0, m = attrList.length; n < m; n++) keys.push(attrList[n].nodeName);
			if (sort) keys.sort(function (a, b) {
				    return a.toLowerCase().localeCompare(b.toLowerCase());
			});

			for (var n = 0, m = keys.length; n < m; n++) if (keys[n].indexOf('_') != 0) attrlistDump[keys[n]] = attrList[keys[n]].nodeValue;

			return attrlistDump;
		},
		/**
		 * renderAttributes: Receives an object with attribute:value pairs and
		 * returns an HTML representative for each pair or false if none available
		 * 
		 * @param  {Object} attrs attribute list
		 * @return {String} html representative for each attribute:value pair or false if none available
		 */
		renderAttributes: function(attrs) {
			var html = ['&lt;'+(this.model ? this.model.nodeName : '')];

			for (var n in attrs) {
			 html.push('<span class="bc-node-attribute">'+n+'="<span class="bc-node-attribute-value">'+attrs[n]+'</span>"</span>');
			}

			return attrs ? html.join(' ')+'&gt;' : false;
		}
	}, {
		template:function(t){
			var html = this.model ? this.renderAttributes(this.getAttributes(true)) : t.getAttribute('label'),
				hasChildren = this.firstChild ? this.childNodes.length : (this.model ? this.model.childNodes.length : false);

			return '<div class="'+t.cnBase+'">'+
						'<div class="bc-treerow">'+
							'<i class="bc-icon xbc-tree-expanded bc-treeitem--expand' + (hasChildren ? '' : ' bc-tree-nochildren') + '"></i>'+
//							'<i class="xbc-icon xbc-icon-folder-open"></i>'+
							'<a class="bc-treeitem--label bc-inline-block">'+html+'</a>'+
						'</div>'+
						'<div class="bc-treeitem--area" xstyle="display:none;"></div>'+
					'</div>';
		},
		handlers: {
			'mouseover': function(event){
				if(event.target == this) {
					htmlAPI.addClass(this.htmlNode, this.cnBase+'--hover');
				}
			},
			'mouseout': function(event){
				if(event.target == this) {
					htmlAPI.removeClass(this.htmlNode, this.cnBase+'--hover');
				}
			},
			'mousedown': function(event){
				if(event.target == this) {
					var oSelectedTree = $('.bc-treerow-selected', this.ownerDocument.all['portals'].htmlNode)[0];
					if (oSelectedTree && this.model) {htmlAPI.removeClass(oSelectedTree, 'bc-treerow-selected')};
					var tree = this.findAncestorTree();
					if(tree) {
						tree.startDrag(this);
						event.preventDefault();
					}
				}
			},
			'mouseup': function(event) {
				if(event.target == this) {
					var tree = this.findAncestorTree();
					if(tree) {
						event.preventDefault();
					}
				}
			},
			'click': function(event){
				if(event.target == this) {
					var html = this.getDisplayEventTarget(event.htmlTarget);
					if (html == this.getDisplay("expand")){
						this.toggle();
					}
					else if (html == this.getDisplay("label")){
						if (this.model) htmlAPI.addClass(this.htmlNode.firstChild, 'bc-treerow-selected');
						b$.cmdr.currentHighlightedElm = this;

						this.dispatchCustomEvent('dbg-inspect-node',true,false,{'node':this.model});
					}
				}
/*
			},
//			'DOMNodeInserted': function(event){
			'DOMReady': function(event){
				if(event.target == this){
					if(this.htmlNode)this.setTreePositionOffset();
				}
*/
			}
		}
	});


	var Console = Element.extend(function (bdomDocument, node) {
		Element.apply(this, arguments);
	},{
		localName:'console',
		initConsole:function(){
			this.textarea = this.getDisplay('ta');

		},
		addText:function(s){
			this.textarea.value += '\n'+s;

		},
		setText:function(s){
			this.textarea.value = s;
		}
	}, {
		template:function(t){
			return '<div class="'+t.cnBase+'">'+
						'<div class="'+t.cnBase+'--inner">'+
							'<textarea class="'+t.cnBase+'--ta"></textarea>'+
						'</div>'+
						'<div style="position:absolute;top:0px;">'+
						'<button data-act="xml" class="--button">xml</button>'+
						'<button data-act="test" class="--button">test</button>'+
						'</div>'+
					'</div>';
		},
		handlers: {
			'click': function(event){
				if(event.target == this){
					var html = this.getDisplayEventTarget(event.htmlTarget);
//					if (html == this.getDisplay('button')) {
						switch (html.getAttribute('data-act')) {
							case 'test':
								b$.debug.cmdr.log('This is a test...');
								break;
							case 'xml':
//								var s = new XMLSerializer().serializeToString(b$.debug.cmdr.currentDocument);
								var s = b$.debug.cmdr.currentDocument.toXML();

								b$.debug.cmdr.log(s);
								break;
							default:
//								b$.debug.cmdr.log('xxx');
								break;
//							case '':
//								this.domActivate();
//								break;
//						}
					}
				}
			},
			'DOMReady': function(event){
//				if(event.target == this){
					this.initConsole();
					this.setText('Console v1.2');
					var This = this;
					cmdr.cmdrDocument.addEventListener('log',function(evt){
//						console.dir(evt);
						This.addText(evt.detail.msg)
					});
//				}
			}
		}
	});




	// Auto attach

//	var myDebug;
	b$._private.html.addEventListener(document, 'keyup', function(event){
		if(event.keyCode == 27) {
//			if(!myDebug){
			if(!window.cmdrDocument){
//				b$.debug.createDebugDocument(window.testDocument);
				b$.debug.createDebugDocument(b$.bdom.domImplementation.documents[0]);
			}
		}
	});







	return;
});

//  ----------------------------------------------------------------
//  ----------------------------------------------------------------

