<%--
Copyright © 2011 Backbase B.V.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="org.springframework.security.web.savedrequest.*" %>
<%@ page session="false"%>
<%String buildVersion = com.backbase.portal.foundation.presentation.util.BuildConfigUtils.getBuildVersion();%>
<%
	String portalContextRoot = request.getContextPath();
	boolean dashboard = false;
	String redirectUrl = request.getParameter("redirectUrl");
%><%--
	SavedRequest savedRequest = (SavedRequest)request.getSession().getAttribute("SPRING_SECURITY_SAVED_REQUEST");
	if(redirectUrl != null && !redirectUrl.equals("") && !redirectUrl.equals("null") && redirectUrl.contains("dashboard"))
		dashboard = true;
	else if(savedRequest != null)
	{
		String savedRequestRedirectUrl = savedRequest.getRedirectUrl();
		if(savedRequestRedirectUrl != null && !savedRequestRedirectUrl.equals("") && savedRequestRedirectUrl.contains("dashboard"))
			dashboard = true;
	}
	else if(request.getRequestURL().toString().contains("dashboard"))
		dashboard = true;
--%>
<!DOCTYPE html>
<html>
	<head>
	  <link rel="stylesheet" type="text/css" href="<%=portalContextRoot%>/static/dashboard/build/common.min.css?v=<%=buildVersion%>" />
	  <link rel="stylesheet" type="text/css" href="<%=portalContextRoot%>/static/dashboard/build/dashboard-all.min.css?v=<%=buildVersion%>" />
	  <script type="text/javascript" src="<%=portalContextRoot%>/static/ext-lib/jquery-1.8.3.js" ></script>
	  <link rel="shortcut icon" href="<%=portalContextRoot%>/static/dashboard/media/favicon.ico" />
	</head>
	<body class="dashboard" onload="customOnload()">
		<div class="bd-loginContainer">
			<form action="<%=portalContextRoot%>/j_spring_security_check" method="POST" name="f" class="bd-loginForm">
				<label for="j_username" style="margin-right: 20px;">Username2<input type="text" name="j_username" id="j_username" class="bd-roundcorner-5 bd-login-font-color bd-autoTest-login-username"/></label>
				<label for="j_password">Password<input type="password" name="j_password" id="j_password" class="bd-roundcorner-5 bd-login-font-color bd-autoTest-login-password"/></label>
				<c:if test="${param.login_error eq 'accessdenied'}">
					<div class="spacer"></div>
					<div class="bd-error">
						<p class="bd-error-message">The requested page requires authorization.</p>
						<p class="bd-error-description">Please login with a valid Username and Password.</p>
					</div>
				</c:if>
				<c:if test="${param.login_error eq 'failure'}">
					<div class="spacer"></div>
					<div class="bd-error">
						<p class="bd-error-message">The Username or Password you entered is not correct.</p>
						<p class="bd-error-description">Please re-enter your Username and Password.</p>
					</div>
				</c:if>
				<c:if test="${param.login_error eq 'invalidDestination'}">
					<div class="spacer"></div>
					<div class="bd-error">
						<p class="bd-error-message">This user has no permissions to access this page.</p>
						<p class="bd-error-description">Please re-enter the URL to the portal you want to see.</p>
					</div>
				</c:if>
				<div class="bd-buttons">
					  <button type="submit" class="bd-button bd-gradient-grey bd-roundcorner-5 bd-noshadow">
						<%--if(dashboard){Sign into Portal Manager }else{} --%> Log in  - Testing Page
					</button>
				</div>
			</form>
		</div>
		<!-- for IE 6 browser -->
		<!--[if lt IE 7]> <div style=' clear: both; height: 59px; padding:0 0 0 15px; position: relative;'> <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a></div> <![endif]-->
		<div class="bd-ie6block" id='ie6block'>
		  <form  class="bd-loginForm">
		      <div style='font-size:14px; text-align:left;'>
                  You appear to be using Internet Explorer 6 or 7.
                  <br/>
                  Portal Manager will not work using an obsolete browser.
                  <br/>
                  Please upgrade.
		      </div>
                <div class="bd-buttons bd-ie6block-button">
                      <button type="button" class="bd-button bd-gradient-grey bd-roundcorner-5" id='tryItButton'>
                        Try It Anyway
                    </button>
                </div>
           </form>
        </div>
		<script>
			function customOnload()
			{
				document.f.j_username.focus();

				//for IE6 browser
				if($.browser.msie && $.browser.version=="6.0"){
					$('.bd-loginContainer').hide();
					$('#ie6block').show();

					$('#tryItButton').click(function(){
						$('#ie6block').fadeOut("slow",function(){
							$('.bd-loginContainer').fadeIn("slow");
						});
					});
				}
			}
		</script>
	</body>
</html>
