<%--
Copyright © 2011 Backbase B.V.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="org.springframework.security.web.savedrequest.*" %>
<%@ page session="false"%>
<%String portalContextRoot = request.getContextPath();%>
<%String buildVersion = com.backbase.portal.foundation.presentation.util.BuildConfigUtils.getBuildVersion();%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>PC Financial</title>
    <link rel="stylesheet" href="<%=portalContextRoot%>/static/assets/css/global.css">
    <link rel="stylesheet" href="<%=portalContextRoot%>/static/assets/css/index.css">
    <link rel="shortcut icon" href="/portalserver/static/favicon.ico" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <script type="text/javascript" src="//use.typekit.net/oup4hmx.js"></script>
    <script type="text/javascript">try{Typekit.load();}catch(e){}</script>
    <script type="text/javascript" src="//code.jquery.com/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="<%=portalContextRoot%>/static/assets/js/parallax.js"></script>
    <script type="text/javascript" src="<%=portalContextRoot%>/static/assets/js/index.js"></script>
</head>
<body onload="customOnload()">
<div id="hat">
    <div id="header">
        <div id="hamburger"></div><div id="logo"></div>
        <ul class="nav">
            <li><a href="#">Banking</a>
                <div class="megamenu">
                    <div class="contain">
                        <div class="close">&times;</div>
                        <ul class="head-left-col">
                            <li><a href="#">Bank Accounts</a></li>
                            <li><a href="#">Mortgages</a></li>
                            <li><a href="#">Investments</a></li>
                            <li><a href="#">Borrowing</a></li>
                        </ul><div class="head-middle-col">
                        <h5>View and Compare Features</h5>
                        <h6>Chequing Accounts</h6>
                        <div class="item">No Fee Bank Account</div>
                        <h6>Savings Accounts</h6>
                        <div class="item">Interest Plus&trade; Savings Account</div>
                    </div><div class="head-right-col">
                        <h4>No Fee Bank Account</h4>
                        <div class="description">With the President's Choice Financial<sup>&reg;</sup> no fee bank account, you get free daily banking. Period. You even earn interest on the balance in your account, plus the ability to earn PC<sup>&reg;</sup> points toward free groceries.</div><a href="#" class="more">Learn More</a>
                    </div><div class="head-sidebar">
                        <div class="cell">
                            <h4>You might also like:</h4>
                            <b class="red"><img class="thumb" src="<%=portalContextRoot%>/static/assets/img/navcard.png" alt="Presidents Choice Financial World Mastercard">Presidents Choice Financial World Mastercard</b>
                        </div>
                        <div class="cell">
                            <h4>Rewards picked for you.</h4>
                            <p><img src="<%=portalContextRoot%>/static/assets/img/rewards.png" alt="PC Rewards" class="thumb">Get on your way to offers, dinner suggestions, and more&mdash;all tailored to you.</p>
                        </div>
                    </div>
                    </div>
                </div>
            </li><li><a href="#">Credit Cards</a>
            <div class="megamenu">
                <div class="contain">
                    <div class="close">&times;</div>
                    <ul class="head-left-col">
                        <li><a href="#">Bank Accounts</a></li>
                        <li><a href="#">Mortgages</a></li>
                        <li><a href="#">Investments</a></li>
                        <li><a href="#">Borrowing</a></li>
                    </ul><div class="head-middle-col">
                    <h5>View and Compare Features</h5>
                    <h6>Chequing Accounts</h6>
                    <div class="item">No Fee Bank Account</div>
                    <h6>Savings Accounts</h6>
                    <div class="item">Interest Plus&trade; Savings Account</div>
                </div><div class="head-right-col">
                    <h4>No Fee Bank Account</h4>
                    <div class="description">With the President's Choice Financial<sup>&reg;</sup> no fee bank account, you get free daily banking. Period. You even earn interest on the balance in your account, plus the ability to earn PC<sup>&reg;</sup> points toward free groceries.</div><a href="#" class="more">Learn More</a>
                </div><div class="head-sidebar">
                    <div class="cell">
                        <h4>You might also like:</h4>
                        <b class="red"><img class="thumb" src="<%=portalContextRoot%>/static/assets/img/navcard.png" alt="Presidents Choice Financial World Mastercard">Presidents Choice Financial World Mastercard</b>
                    </div>
                    <div class="cell">
                        <h4>Rewards picked for you.</h4>
                        <p><img src="<%=portalContextRoot%>/static/assets/img/rewards.png" alt="PC Rewards" class="thumb">Get on your way to offers, dinner suggestions, and more&mdash;all tailored to you.</p>
                    </div>
                </div>
                </div>
            </div>
        </li><li><a href="#">Insurance</a>
            <div class="megamenu">
                <div class="contain">
                    <div class="close">&times;</div>
                    <ul class="head-left-col">
                        <li><a href="#"></a></li><li><a href="#"></a></li><li><a href="#"></a></li><li><a href="#"></a></li>
                    </ul><div class="head-middle-col">
                    <h5>View and Compare Features</h5>
                    <h6>Chequing Accounts</h6>
                    <div class="item">No Fee Bank Account</div>
                    <h6>Savings Accounts</h6>
                    <div class="item">Interest Plus&trade; Savings Account</div>
                </div><div class="head-right-col">
                    <h4>No Fee Bank Account</h4>
                    <div class="description">With the President's Choice Financial<sup>&reg;</sup> no fee bank account, you get free daily banking. Period. You even earn interest on the balance in your account, plus the ability to earn PC<sup>&reg;</sup> points toward free groceries.</div><a href="#" class="more">Learn More</a>
                </div><div class="head-sidebar">
                    <div class="cell">
                        <h4>You might also like:</h4>
                        <b class="red"><img class="thumb" src="<%=portalContextRoot%>/static/assets/img/navcard.png" alt="Presidents Choice Financial World Mastercard">Presidents Choice Financial World Mastercard</b>
                    </div>
                    <div class="cell">
                        <h4>Rewards picked for you.</h4>
                        <p><img src="<%=portalContextRoot%>/static/assets/img/rewards.png" alt="PC Rewards" class="thumb">Get on your way to offers, dinner suggestions, and more&mdash;all tailored to you.</p>
                    </div>
                </div>
                </div>
            </div>
        </li><li><a href="#">Gift Cards</a>
            <div class="megamenu">
                <div class="contain">
                    <div class="close">&times;</div>
                    <ul class="head-left-col">
                        <li><a href="#"></a></li><li><a href="#"></a></li><li><a href="#"></a></li><li><a href="#"></a></li>
                    </ul><div class="head-middle-col">
                    <h5>View and Compare Features</h5>
                    <h6>Chequing Accounts</h6>
                    <div class="item">No Fee Bank Account</div>
                    <h6>Savings Accounts</h6>
                    <div class="item">Interest Plus&trade; Savings Account</div>
                </div><div class="head-right-col">
                    <h4>No Fee Bank Account</h4>
                    <div class="description">With the President's Choice Financial<sup>&reg;</sup> no fee bank account, you get free daily banking. Period. You even earn interest on the balance in your account, plus the ability to earn PC<sup>&reg;</sup> points toward free groceries.</div><a href="#" class="more">Learn More</a>
                </div><div class="head-sidebar">
                    <div class="cell">
                        <h4>You might also like:</h4>
                        <b class="red"><img class="thumb" src="<%=portalContextRoot%>/static/assets/img/navcard.png" alt="Presidents Choice Financial World Mastercard">Presidents Choice Financial World Mastercard</b>
                    </div>
                    <div class="cell">
                        <h4>Rewards picked for you.</h4>
                        <p><img src="<%=portalContextRoot%>/static/assets/img/rewards.png" alt="PC Rewards" class="thumb">Get on your way to offers, dinner suggestions, and more&mdash;all tailored to you.</p>
                    </div>
                </div>
                </div>
            </div>
        </li><li><a href="#">Rewards</a>
            <div class="megamenu">
                <div class="contain">
                    <div class="close">&times;</div>
                    <ul class="head-left-col">
                        <li><a href="#"></a></li><li><a href="#"></a></li><li><a href="#"></a></li><li><a href="#"></a></li>
                    </ul><div class="head-middle-col">
                    <h5>View and Compare Features</h5>
                    <h6>Chequing Accounts</h6>
                    <div class="item">No Fee Bank Account</div>
                    <h6>Savings Accounts</h6>
                    <div class="item">Interest Plus&trade; Savings Account</div>
                </div><div class="head-right-col">
                    <h4>No Fee Bank Account</h4>
                    <div class="description">With the President's Choice Financial<sup>&reg;</sup> no fee bank account, you get free daily banking. Period. You even earn interest on the balance in your account, plus the ability to earn PC<sup>&reg;</sup> points toward free groceries.</div><a href="#" class="more">Learn More</a>
                </div><div class="head-sidebar">
                    <div class="cell">
                        <h4>You might also like:</h4>
                        <b class="red"><img class="thumb" src="<%=portalContextRoot%>/static/assets/img/navcard.png" alt="Presidents Choice Financial World Mastercard">Presidents Choice Financial World Mastercard</b>
                    </div>
                    <div class="cell">
                        <h4>Rewards picked for you.</h4>
                        <p><img src="<%=portalContextRoot%>/static/assets/img/rewards.png" alt="PC Rewards" class="thumb">Get on your way to offers, dinner suggestions, and more&mdash;all tailored to you.</p>
                    </div>
                </div>
                </div>
            </div></li>
        </ul>
        <div class="close"></div>
        <div class="login">
            <div class="button3 red">Log In</div>
            <div class="flyout" class="red">
                <div class="close"></div>
                <input type="text" placeholder="Username" id="usernameSource" autocorrect="off" autocapitalize="off"><input type="password" placeholder="Password" id="passwordSource" autocorrect="off" autocapitalize="off">
                <a href="#" class="forgot">Forgot Password?</a><div class="button3" id="submitSource">Submit</div>

                <form action="<%=portalContextRoot%>/j_spring_security_check" method="POST" name="f" class="bd-loginForm-dashboard" style="display:none;">
                    <input type="text" name="j_username" autocapitalize="off" id="j_username" value="username" class="bd-login-input bd-login-username bd-autoTest-login-username"/>
                    <input type="password" name="j_password" id="j_password"  value="password" class="bd-login-input bd-login-password bd-autoTest-login-password"/>
                    <a href="#" class="forgot">Forgot Password?</a><div class="button3" id="submitSource">Submit</div><button id="submitTarget" data-cid="bc-btn-primary-login" type="submit"  style="display:none">Submit</button>
                </form>
            </div>
        </div>
    </div>
    <div class="close"></div>
</div>
<div id="hero1" class="pane hero">
    <ul class="hero-nav">
        <li><a href="#hero1" class="current">Expect more from your bank</a></li>
        <li><a href="#hero2">Take your bank with you</a></li>
        <li><a href="#hero3">Tools to help manage money</a></li>
        <li><a href="#hero4">Top it all off with PC Points</a></li>
    </ul>
    <div class="full">
        <div class="half">
            <div class="welcome"><div class="greetings">Welcome, Peter. You have <b>650,785</b> PC Points!</div><div class="more">See how you could have earned more.</div></div>
        </div><div class="half">
        <h1>Bank less,<br/>do more.</h1><h2>With all your accounts in one place, you can spend less time banking and more time doing everything else.</h2>
    </div>
    </div>
</div>
<div class="sliding pane red horizontal">
    <ul class="slides">
        <li><div class="full"><div class="half"><h3>More convenience.</h3><h4><b>PC Financial MasterCard</b> lets you shop faster. With PayPass you just tap at the register and you're on your way.</h4><a href="#" class="button2">Learn More</a> <a href="#" class="button2">Apply Now</a></div><div class="half"><div class="mastercard">
            <div class="card"></div>
            <div class="hand"></div>
            <div class="waves">
                <div class="wave"></div>
                <div class="wave"></div>
                <div class="wave"></div>
            </div>
        </div></div></div></li>
        <li><div class="full coins">
            <div class="half">
                <h3>More value.</h3><h4>No fee chequing &amp; savings accounts that actually pay you interest.</h4><a href="#" class="button2">Learn More</a>
            </div><div class="half">
        </div>
        </div></li>
        <li><div class="full"><div class="half"><h3>More value.</h3><h4>No fee chequing &amp; savings accounts that actually pay you interest.</h4><a href="#" class="button2">Learn More</a> <a href="#" class="button2">Apply Now</a></div><div class="half"><div class="mocards">
            <div class="card card1"></div>
            <div class="card card2"></div>
        </div></div></div></li>
        <li><div class="full"><div class="half">
            <h3>More choice.</h3><h4>Owning property is even easier with competitive rates, flexible payment options to help you feel right at home.</h4><a href="#" class="button2">Learn More</a></div><div class="half"><div class="houses">
            <div class="house"></div><div class="house"></div><div class="house"></div><div class="house"></div><br>
            <div class="house"></div><div class="house"></div><div class="house"></div><div class="house"></div><br>
            <div class="house"></div><div class="house"></div><div class="house"></div><div class="house"></div>
        </div></div></div></li>
    </ul>
</div>
<div id="hero2" class="pane hero">
    <ul class="hero-nav">
        <li><a href="#hero1">Expect more from your bank</a></li>
        <li><a href="#hero2" class="current">Take your bank with you</a></li>
        <li><a href="#hero3">Tools to help manage money</a></li>
        <li><a href="#hero4">Top it all off with PC Points</a></li>
    </ul>
    <div class="full">
        <div class="half"><h1>Worry less,<br/>know more.</h1><h2>You get everything you need to help make the most of your money. With PC Financial it's a finger tap away, anywhere, anytime you need it.</h2></div><div class="half"></div>
    </div>
</div>
<div id="hero3" class="pane hero">
    <div class="full">
        <div class="half">
            <h3>Forward thinking.</h3><h4>Set your goals and we'll find the fastest way to get you there. Use tools and alerts that show you the best way to put your money towards the things you really want.</h4>
        </div><div class="half"><div class="phone">
        <div id="tip1" class="tooltip" data-caption="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet expedita, animi sapiente autem vel nam. Lorem ipsum dolor sit."></div>
        <div id="tip2" class="tooltip" data-caption="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Possimus qui quam quod, consectetur minus quaerat. Lorem ipsum dolor."></div>
        <div id="tip3" class="tooltip on" data-caption="Add new tools and information with a click or tap. Your dashboard can be as simple or as comprehensive as you want it to be. You're in control."></div>
        <div id="tip4" class="tooltip" data-caption="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic iure, labore magni, dicta ad aliquam cirrogat. Lorem ipsum dolor sit."></div>
    </div></div>
    </div>
    <div class="pane" style="background-color:#777;color:#fff;padding:4em 0;margin-bottom:-4em">
        <ul class="hero-nav">
            <li><a href="#hero1">Expect more from your bank</a></li>
            <li><a href="#hero2">Take your bank with you</a></li>
            <li><a href="#hero3" class="current">Tools to help manage money</a></li>
            <li><a href="#hero4">Top it all off with PC Points</a></li>
        </ul>
        <div class="full">
            <div class="half"><h4 id="tooltipText">Add new tools and information with a click or tap. Your dashboard can be as simple or as comprehensive as you want it to be. You're in control.</h4></div>
        </div>
    </div>
</div>
<div id="hero4" class="pane hero">
    <ul class="hero-nav">
        <li><a href="#hero1">Expect more from your bank</a></li>
        <li><a href="#hero2">Take your bank with you</a></li>
        <li><a href="#hero3">Tools to help manage money</a></li>
        <li><a href="#hero4" class="current">Top it all off with PC Points</a></li>
    </ul>
    <div class="full">
        <div class="half"></div><div class="half"><h1>Do more,<br/>earn more.</h1><h2>Get rewarded with PC Points for banking with us. That's on top of the points you can earn shopping with your PC Plus card!</h2></div>
    </div>
</div>
<div class="sliding pane horizontal backgrounds">
    <ul class="slides">
        <li style="background-image:url(<%=portalContextRoot%>/static/assets/img/desk.jpg)">
            <div class="full">
                <div class="half">
                    <h3>Earn faster!</h3>
                    <h4>Collect PC Points every time you use your PC Financial Mastercard, and earn double when you shop at participating stores where President's Choice&reg; products are sold.</h4>
                    <a href="#" class="button2 red">Learn More</a>
                </div><div class="half"></div>
            </div>
        </li>
        <li>
            <div class="full">
                <div class="half">
                    <h3>So many rewards.</h3>
                    <h4>Use your PC points for free groceries. Or flowers. Or photo finishing. Redeem right at the checkout in participating stores where President's Choice&reg; products are sold. No hassles, no waiting.</h4>
                    <a href="#" class="button2 red">Learn More</a>
                </div><div class="half"></div>
            </div>
        </li>
        <li>
            <div class="full">
                <div class="half">
                    <h3>Put your mortgage to work.</h3>
                    <h4>Let your mortgage buy your groceries. That's right, you can earn PC points with your mortgage. Great rates, flexible payment options and you get points on top.</h4>
                    <a href="#" class="button2 red">Learn More</a>
                </div><div class="half"></div>
            </div>
        </li>
        <li>
            <div class="full">
                <div class="half">
                    <h3>Maximize your rewards.</h3>
                    <h4>Join PC Plus and you're on your way to offers, dinner suggestions, and more&mdash;all tailored to you.</h4>
                    <a href="#" class="button2 red">Join Now</a>
                </div><div class="half"><div class="pcplus"></div></div>
            </div>
        </li>
    </ul>
</div>
<div id="shoe">
    <div id="foot">
        <ul class="heel">
            <li><a href="#">Today's Rates</a></li>
            <li><a href="#">Find a Bank Machine or Pavilion</a></li>
            <li><a href="#">Contact Us</a></li>
        </ul><ul class="toes">
        <li>
            <h4 class="metatarsal">Banking</h4><!-- Really stretching the foot metaphor here... -->
            <ul class="phalanges">
                <li><a href="#">Chequing &amp; Savings</a></li>
                <li><a href="#">Credit Cards</a></li>
                <li><a href="#">Mortgages</a></li>
                <li><a href="#">Investments</a></li>
                <li><a href="#">Borrowing</a></li>
            </ul>
        </li><li>
        <h4 class="metatarsal">Insurance</h4>
        <ul class="phalanges">
            <li><a href="#">Auto</a></li>
            <li><a href="#">Home</a></li>
            <li><a href="#">Pet</a></li>
            <li><a href="#">Travel</a></li>
            <li><a href="#">Life</a></li>
        </ul>
    </li><li>
        <h4 class="metatarsal">PC Plus</h4>
        <ul class="phalanges">
            <li><a href="#">Your Points</a></li>
            <li><a href="#">Where to Collect</a></li>
            <li><a href="#">Help Centre</a></li>
            <li><a href="#">Flyers</a></li>
        </ul>
    </li>
    </ul>
    </div>
</div>
<script src="<%=portalContextRoot%>/static/assets/js/flexslider.js"></script>
<script>
    function customOnload()
    {
        jQuery('.bd-loginForm').submit(function(){
            var portalName = jQuery.cookie('portalName');

            if(portalName && window != top){
                // if it's a preview window redirect to source portal
                jQuery.cookie('redirectPortal', portalName, { path: '/' });
            }
            return true;
        });
        //document.f.j_username.focus();

        $("#submitSource").on("click",function(){
            $("#j_username").val($("#usernameSource").val());
            $("#j_password").val($("#passwordSource").val());
            $("#submitTarget").trigger("click");
        });
    }
</script>
</body>
</html>