<%--
Copyright © 2011 Backbase B.V.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="org.springframework.security.web.savedrequest.*" %>
<%@ page session="false"%>
<%String portalContextRoot = request.getContextPath();%>
<%String buildVersion = com.backbase.portal.foundation.presentation.util.BuildConfigUtils.getBuildVersion();%>
<!DOCTYPE html>
<html>
	<head>
	<title>Portal Manager - Login</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	  <link rel="stylesheet" type="text/css" href="<%=portalContextRoot%>/static/dashboard/build/dashboard-all.min.css?v=<%=buildVersion%>" />
	  <link rel="shortcut icon" href="<%=portalContextRoot%>/static/dashboard/media/favicon.ico" />
	  <script type="text/javascript">
	      window.bd = window.bd || {};
	      bd.contextRoot = '<%=portalContextRoot%>';
	  </script>
	  <script type="text/javascript" src="<%=portalContextRoot%>/static/ext-lib/jquery-1.8.3.js" ></script>
	  <script type="text/javascript" src="<%=portalContextRoot%>/static/dashboard/js/login/jquery.cookie.js"></script>
      <script type="text/javascript" src="<%=portalContextRoot%>/static/dashboard/js/login/loginDashboard.js"></script>
	</head>
	<body class="dashboard bd-login-dashboard" onload="customOnload()">

		<div class="bd-loginContainer-dashboard">
			<form action="<%=portalContextRoot%>/j_spring_security_check" method="POST" name="f" class="bd-loginForm-dashboard">
			     <div class="bd-login-container bd-roundcorner-5">
			         <div class="bd-login-header bd-roundcorner-5">
			             <div class="bd-login-header-icon"></div>
	                     <div class="bd-login-header-div">PORTAL MANAGER</div>
			         </div>

			         <div class="bd-login-body">
			             <div class="bd-login-input-area bd-roundcorner-5">
			                 <div class="bd-login-input-username">
				                 <div class="bd-login-input-username-icon"></div>
			                     <input type="text" name="j_username" autocapitalize="off" id="j_username" value="username" class="bd-login-input bd-login-username bd-autoTest-login-username"/>
		                     </div>

			                 <div class="bd-login-input-password">
				                 <div class="bd-login-input-password-icon"></div>
			                     <input type="password" name="j_password" id="j_password"  value="password" class="bd-login-input bd-login-password bd-autoTest-login-password"/>
		                     </div>

			             </div>
			             <div class="bd-login-error-msg-area">
			                 <c:if test="${param.login_error eq 'accessdenied'}">
			                         <p class="bd-error-message-dashboard bd-login-error">The requested page requires authorization.</p>
			                         <p class="bd-error-description-dashboard">Please login with a valid Username and Password.</p>
			                 </c:if>
			                 <c:if test="${param.login_error eq 'failure'}">
			                         <p class="bd-error-message-dashboard bd-login-error">The Username or Password you entered is not correct.</p>
			                         <p class="bd-error-description-dashboard">Please try again.</p>
			                 </c:if>
			                 <c:if test="${param.login_error eq 'logout'}">
			                         <p class="bd-error-message-dashboard">You have successfully logged out.</p>
			                         <p class="bd-error-description-dashboard">Please re-enter your Username and Password to log in.</p>
			                 </c:if>
			                 <c:if test="${param.login_error eq 'timeOut'}">
			                         <p class="bd-error-message-dashboard">Your session has timed out.</p>
			                         <p class="bd-error-description-dashboard">Please re-enter your Username and Password to log in.</p>
			                 </c:if>
			                 <c:if test="${param.login_error eq 'invalidDestination'}">
			                         <p class="bd-error-message-dashboard bd-login-error">This user has no permissions to access this page.</p>
			                         <p class="bd-error-description-dashboard">Please check the URL and try again.</p>
			                 </c:if>
                         </div>
                         <div class="bd-login-button-area">
                             <button data-cid="bc-btn-primary-login" type="submit"  class="bc-button bc-roundcorner-5 bc-primary bc-gradient-green bd-login-button">Log in</button>
                         </div>

                     </div>
			     </div>
			</form>
		</div>
		<!-- for IE 6 browser -->
		<!--[if lt IE 7]> <div style=' clear: both; height: 59px; padding:0 0 0 15px; position: relative;'> <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a></div> <![endif]-->
		<div class="bd-ie6block" id='ie6block'>
		  <form  class="bd-loginForm">
		      <div style='font-size:14px; text-align:left;'>
                  You appear to be using Internet Explorer 6 or 7.
                  <br/>
                  Portal Manager will not work using an obsolete browser.
                  <br/>
                  Please upgrade.
		      </div>
                <div class="bd-buttons bd-ie6block-button">
                      <button type="button" class="bd-button bd-gradient-grey bd-roundcorner-5" id='tryItButton'>
                        Try It Anyway
                    </button>
                </div>
           </form>
        </div>
	</body>
</html>
