<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Backbase Portal r5</title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
    <!-- <meta http-equiv="refresh" content="0;URL='home'" /> -->
    <script type="text/javascript"><!--
    if(navigator.userAgent.match(/iPad/i) != null){
        window.location = "/portalserver/pcf-tablet";
    }else if(navigator.userAgent.match(/iPhone/i) != null){
        window.location = "/portalserver/pcf-mobile";
    }else{
        window.location = "/portalserver/home";
    }
    //--></script>
</head>
</html>