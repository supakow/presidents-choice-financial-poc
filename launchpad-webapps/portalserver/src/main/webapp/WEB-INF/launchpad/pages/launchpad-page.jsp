<%--
    Backbase Launchpad Generic Page Template
--%>
<%@ page import="com.backbase.portal.foundation.presentation.util.BidiUtils" language="java"
         contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="org.springframework.web.servlet.support.RequestContextUtils" %>
<%@ page import="java.util.Locale" %>
<%@ include file="../common/directives.jspf" %>

<%
    Locale locale = RequestContextUtils.getLocale(request);
    String direction = BidiUtils.getDirValue(request);
    pageContext.setAttribute("direction", direction);
%>
<!DOCTYPE html>
<html xmlns:ng="http://angularjs.org" xmlns:lp="http://launchpad.backbase.com" class="${param.designmode ? 'bd-designMode-true' : 'bd-designMode-null'} ${param.device}" dir="${fn:toLowerCase(direction)}">
<head>

    <!--
        Powered by Backbase Launchpad © 2013.
        <c:catch>
        v${lpconf_build['launchpad.version']} (b${lpconf_build['launchpad.number']} / r${lpconf_build['launchpad.revision']})
        </c:catch>

        Running apps
        <c:forEach var="bundleName" items="${lpconf_bundles}">
            ${bundleName}
        </c:forEach>
        http://www.backbase.com
    -->

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <!--[if lte IE 8]>
    <script>
        document.createElement('lp-accounts-select');
        document.createElement('lp-accounts-header');
    </script>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="${contextPath}/static/launchpad/support/respondjs/html5shiv.js"></script>
    <script src="${contextPath}/static/launchpad/support/respondjs/respond.min.js"></script>
    <script src="${contextPath}/static/launchpad/support/es5-shim.js"></script>
    <![endif]-->

    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />

    <meta name="apple-mobile-web-app-capable" content="yes" />
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="/portalserver/static/pcfinancial/media/iconIphone.png" />
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="/portalserver/static/pcfinancial/media/iconStandard.png" />
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="/portalserver/static/pcfinancial/media/iconRetina.png" />
    <title>${item.propertyDefinitions["title"].value.value}</title>

    <link rel="shortcut icon" href="/portalserver/static/favicon.ico" />

    <jsp:include page="/WEB-INF/common/portalclient-libs.jsp"/>
    <jsp:include page="/WEB-INF/backbase.com.2012.aurora/common/aurora-libs.jsp"/>
    <jsp:include page="/WEB-INF/backbase.com.2012.veltro/common/ext-mobile-libs.jsp"/>
    <jsp:include page="/WEB-INF/launchpad/pages/launchpad-lib.jsp" />

    <%-- Portal styling patches --%>
    <style>
        .lp-page-children .bp-container {
            z-index: auto;
        }
    </style>

    <%-- Custom bundle heads --%>
    <c:forEach var="bundleName" items="${lpconf_bundles}">
        <c:catch>
            <%-- Custom head contents extension point. This include is optional --%>
            <jsp:include page="/WEB-INF/${bundleName}/includes/custom-head.jsp"/>
        </c:catch>
    </c:forEach>

    <%-- Custom theme --%>
    <c:catch var="themeMissing">
        <jsp:include page="/WEB-INF/common/theme.jsp" />
    </c:catch>
    <c:if test="${not empty themeMissing}">
        <!-- WARNING: Could not find a theme to include -->
    </c:if>

    <!--[if lt IE 9]>
    <link href="${lpconf_themePath}/../support/respondjs/respond-proxy.html" id="respond-proxy" rel="respond-proxy" />
    <link href="${contextPath}/static/launchpad/support/respondjs/respond.proxy.gif" id="respond-redirect" rel="respond-redirect" />
    <script src="${contextPath}/static/launchpad/support/respondjs/respond.proxy.js"></script>
    <![endif]-->

    <script src="${contextPath}/static/com.backbase.labs/lib/labs.js"></script>
    <script src="${contextPath}/static/loader.js"></script>
    <link href="${contextPath}/static/launchpad/lib/labs_compatibility.css" rel="stylesheet" type="text/css" />

</head>

<body>
<div class="pcf-loading-div">
    <div class="pcf-loading-div-img">
        <div class="pcf-loading-div-gif"></div>
        <div class="pcf-loading-div-logo"></div>
    </div>
</div>
<div class="pageContainer">
    <div id="main" data-pid="${item.name}" class="bp-page bp-portal-area bp-area">
        <div class="lp-page-children">
            <div id="lp-page-backdrop" class="lp-page-backdrop modal-backdrop" style="z-index: 4; display: none"></div>
            <c:forEach items="${item.children}" var="child">
                <b:include src="${child}"/>
            </c:forEach>
        </div>
    </div>
</div>

<c:forEach var="bundleName" items="${lpconf_bundles}">
    <c:catch>
        <!-- ******* ${bundleName} ***** -->
        <%-- Custom body contents extension point. This include is optional --%>
        <jsp:include page="/WEB-INF/${bundleName}/includes/custom-body.jsp"/>
    </c:catch>
</c:forEach>
</body>
</html>
