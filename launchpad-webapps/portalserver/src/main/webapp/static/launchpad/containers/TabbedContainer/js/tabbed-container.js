/*global b$, window, console $*/
(function (b$, window, $) {
    "use strict";

    var CLASS_FOR_ACTIVE_TAB = "active";
    var TAB_SELECTOR = ".nav-tabs > li";

    var tabs = {}, //holds all the tabs inside each tabbed container in the page
        tabsActive = {}, //holds which tabs are active (multiple tabbed containers in a page may have multiple active panels)
        designModeUI = {}, //holds the design Mode buttons and textfields
        designModeActivated = false, //determines if the user is editing a tab or not
        deletedChildren = []; //keeps a list of the deleted panels so tabs are not created for them in case of refresh

    var DeckContainer = b$.bdom.getNamespace('launchpad').getClass('DeckContainer');
//  ----------------------------------------------------------------
    var TabbedContainer = DeckContainer.extend(function (bdomDocument, node) {
        DeckContainer.apply(this, arguments);
        this.isPossibleDragTarget = true;
    }, {
        localName: 'TabbedContainer',
        namespaceURI: 'launchpad',
        /**
         * Functions to control access based on security profile.
         * TODO: these functions should be moved to a more meaningful place once it is available (lib folder?)
         */
        _userCanAdd: function(){
            return ["ADMIN"].indexOf(this.model.securityProfile) > -1;
            //return ["ADMIN", "CREATOR", "COLLABORATOR"].indexOf(this.model.securityProfile) > -1;
        },
        _userCanDelete: function(){
            return ["ADMIN"].indexOf(this.model.securityProfile) > -1;
            //return ["ADMIN", "CREATOR"].indexOf(this.model.securityProfile) > -1;
        },
        _userCanModify: function(){
            return ["ADMIN"].indexOf(this.model.securityProfile) > -1;
            //return ["ADMIN", "CREATOR", "COLLABORATOR", "CONTRIBUTOR"].indexOf(this.model.securityProfile) > -1;
        },
        /**
         * Removes all the tabs in the container (not including the panels)
         */
        _removeTabs: function(){
          var i;
          for(i=0; i < this.model.childNodes.length; i++){
            $(tabs[this.model.childNodes[i].name]).remove();
          }
        },
        /**
         * Loads the tabs based on the child panels inside the container
         */
        _loadTabs: function(){
          var i, refTab, newTab, panelName,
              self = this,
              len;

          var _tabClickedHandler = function(e){
            var panelName = e.data.panelName;
            $(tabs[tabsActive[self.model.name]]).removeClass(CLASS_FOR_ACTIVE_TAB);
            $(tabs[panelName]).addClass(CLASS_FOR_ACTIVE_TAB);
            tabsActive[self.model.name] = panelName;
            //TODO: find a better way to pass the id for the tab to show
            self.showPanel(panelName);
            if(!designModeActivated){
                self._loadEditTabButton();
            }
            $('.lp-page-children').addClass('pcf-tabbed-active');
          };
          refTab = $(this.htmlNode).find(TAB_SELECTOR).first();
          //create tabs for all panels based on a reference tab
          this._removeTabs();
          len = this.model.childNodes.length;
          for(i=0; i < this.model.childNodes.length; i++){
            if(deletedChildren.indexOf(this.model.childNodes[i].name) > -1){
                continue; //only create tabs if they are not in the deletedChildren list
            }

            panelName = this.model.childNodes[i].name;
            newTab = $(refTab).clone();
            // if(tabsActive[this.model.name] === panelName || (i === 0 && !tabsActive[this.model.name])){
            //   newTab.addClass(CLASS_FOR_ACTIVE_TAB);
            //   tabsActive[this.model.name] = panelName;
            // }
            //rename tab title based on name
            newTab.find(".tabbed-container-title").first().text(this.model.childNodes[i].getPreference('title'));
            newTab.click({panelName: panelName}, _tabClickedHandler);
            newTab.removeClass('hidden');

            if(i === 0){
                newTab.addClass('pcf-first-tab');
            }else if(i === len - 1){
                newTab.addClass('pcf-last-tab');
            }

            tabs[panelName] = newTab;
            $(this.htmlNode).find("ul.nav-tabs").append(newTab);
          }
        },
        /**
         * Loads the add panel button (+)
         */
        _loadAddPanelButton: function(){
            if(!this._userCanAdd()){
                return;
            }

            var self = this;
            //load add panel button
            var $addButton = $('<button type="button" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-plus"></span></button>');
            $addButton.click(function(){
              self.addPanel();
            });
            $(this.htmlNode)
              .find("ul")
              .append($addButton);
        },
        /**
         * Loads the edit tab panel button (pen icon)
         */
        _loadEditTabButton: function(){
            if(!this._userCanModify()){
                return;
            }

            var self = this;
            if(designModeUI.$editButton){
                designModeUI.$editButton.remove();
            }
            //load edit button
            designModeUI.$editButton = $('<button type="button" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-pencil"></span></button>');
            designModeUI.$editButton.click(function(e){
                    self._activateDesignModeUI.call(self);
                    e.stopPropagation();
                }
            );
            $(this.htmlNode)
              .find("li." + CLASS_FOR_ACTIVE_TAB)
              .append(designModeUI.$editButton);
        },
        /**
         * Loads the Save and remove buttons and sets the tab name into a textfield
         */
        _activateDesignModeUI: function(){
          var self = this;
          if(designModeActivated){
            return;
          }else{
            designModeActivated = true;
          }

          var $tab = $(self.htmlNode).find("li." + CLASS_FOR_ACTIVE_TAB);
          var $tabLink = $tab.find('a');

          var _saveEdit = function(e){
              if(designModeUI.$tabNameEditor.val()){
                  self.renamePanel(self.getActivePanel().model.name, designModeUI.$tabNameEditor.val());
                  $tabLink.text(designModeUI.$tabNameEditor.val());
                  self._deactivateDesignModeUI();
                  $tabLink.show();
              }
          };
          var _deletePanel = function(e){
              $tab.remove();
              deletedChildren.push(self.getActivePanel().model.name);
              self.removePanel(self.getActivePanel().model.name);
              e.stopPropagation();
              self._deactivateDesignModeUI();
          };

          $tabLink.hide();
          designModeUI.$editButton.hide();
          designModeUI.$tabNameEditor = $('<input type="text" value="' + $tabLink.text() + '"/></input>');
          $tab.append(designModeUI.$tabNameEditor);
          designModeUI.$tabNameEditor.focus();
          designModeUI.$saveEditButton = $('<button type="button" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-ok"></span></button>');
          designModeUI.$saveEditButton.click(_saveEdit);
          designModeUI.$tabNameEditor.keydown(function (e){
            if(e.keyCode === 13){
              _saveEdit();
            }
          });
          $tab.append(designModeUI.$saveEditButton);

          if(this._userCanDelete()){
              designModeUI.$deletePanelButton = $('<button type="button" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-trash"></span></button>');
              designModeUI.$deletePanelButton.click(_deletePanel);
              $tab.append(designModeUI.$deletePanelButton);
          }
        },
        /**
         * Removes the save and remove buttons and the tab name textfield
         */
        _deactivateDesignModeUI: function(){
            designModeUI.$saveEditButton.remove();
            designModeUI.$deletePanelButton.remove();
            designModeUI.$tabNameEditor.remove();
            designModeActivated = false;
        },
        /**
         * Sets up the container by loading the tabs once the panels are loaded
         * @constructor
         */
        createDisplay: function(e) {
            DeckContainer.prototype.createDisplay.call(this, e);
            this._loadTabs();
            this._loadAddPanelButton();
            this._loadEditTabButton();
            this._LoadClosePanelButton();
        },
        refreshHTML: function(e) {
            var self = this;
            DeckContainer.prototype.refreshHTML.call(this, e);
        },
        /**
         * Changes the name of a panel and updates the tab with the new name
         *  @param id: (string) name or index of the panel to be renamed
         *  @param newName: (string) new name for the panel
         */
        renamePanel: function(id, newName){
          DeckContainer.prototype.renamePanel.call(this, id, newName);
          if(typeof id === "number"){
            $(tabs[this._getPanels()[id].model.name]).find("a").first().text(newName);
          }else{
            $(tabs[id]).find("a").first().text(newName);
          }
        },
        /**
         * Removes a panel inside the container and its associated tab
         *  @param id: (string) name or index of the panel to be removed
         */
        removePanel: function(id){
          DeckContainer.prototype.removePanel.call(this, id);
          this._loadTabs();
        },

        /** overwrite the deck container's _displayInitialPanel method **/
        _displayInitialPanel : function(){ },
        /* handle close panel */
        _LoadClosePanelButton : function(){
            //var panelName = e.data.panelName;
                var self = this;
            $(self.htmlAreas).on('click', '.pcf-close-tab-btn .glyphicon-remove', function(){
                $(tabs[tabsActive[self.model.name]]).removeClass(CLASS_FOR_ACTIVE_TAB);
                //$(tabs[panelName]).addClass(CLASS_FOR_ACTIVE_TAB);
                //tabsActive[self.model.name] = panelName;
                $('.lp-page-children').removeClass('pcf-tabbed-active');
            });
        }

    }, {
        template: function(json) {
            var data = {item: json.model.originalItem};
            var sTemplate = window.launchpad.TabbedContainer(data);
            return sTemplate;
        }
    });
})(b$, window, $);
