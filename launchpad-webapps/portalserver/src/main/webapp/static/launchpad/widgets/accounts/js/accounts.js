/*global b$, gadgets */
define(["jquery",
    "angular",
    "launchpad/lib/i18n",
    "launchpad/lib/common/util",
    "launchpad/lib/ui/responsive",
    "launchpad/lib/accounts",
    "launchpad/lib/transactions",
    "launchpad/lib/ui"], function($, angular, i18n, util, responsive) {
    "use strict";

    var app = angular.module("launchpad-retail.accounts", ["i18n", "ui", "accounts", "transactions"]);

    /**
     * Angular Module & Controller
     */
    app.controller("AccountsController", ['$scope', '$rootElement', 'widget', 'AccountsModel', 'i18nUtils', function($scope, $rootElement, widget, AccountsModel, i18nUtils) {

        // Initialize
        var initialize = function() {
            // TODO: add locale support
            $scope.locale = "en";
            $scope.showGroups = util.parseBoolean(widget.getPreference("showGroups"));
            $scope.pcPlusLink = $('html').hasClass('mobile') ? util.getContextPath() + '/' + b$.portal.portalName + '/pc-plus' : '#';

            $scope.model = AccountsModel.getInstance({
                accountsEndpoint: widget.getPreference("accountsDataSrc"),
                groupsEndpoint: widget.getPreference("groupsDataSrc")
            });

            i18nUtils.loadMessages(widget, $scope.locale).success(function(bundle) {
                $scope.messages = bundle.messages;
            });

            $scope.defaultBalance = widget.getPreferenceFromParents("preferredBalanceView") || "current";

            $scope.title = widget.getPreference("title");

            $scope.model.load();
        };

        // Events
        widget.addEventListener("preferencesSaved", function () {
            widget.refreshHTML();
            initialize();
        });

        $scope.selectAccount = function(id){
            $scope.model.selected = id;

            gadgets.pubsub.publish("launchpad-retail.accountSelected", {
                accountId: id,
                originType: "accounts",
                _noBehavior: true // Do not allow behavior to re-open the widget
            }, true);

            if($('html').hasClass('mobile')){
                $('body').toggleClass('mobile-dashboard-transaction-on');
            }
        };

        $scope.accountKeydown = function(evt, accountId) {
            if (evt.which === 13 || evt.which === 32) {
                evt.preventDefault();
                evt.stopPropagation();
                $scope.selectAccount(accountId);
            }
        };

        $scope.payForAccount = function($event, id){
            $event.stopPropagation();
            gadgets.pubsub.publish("launchpad-retail.requestMoneyTransfer", {
                accountId: id
            });
        };

        gadgets.pubsub.subscribe("launchpad-retail.accountSelected", function(params) {
            $scope.model.selected = params.accountId;
            if (!$scope.$$phase) {
                $scope.$apply();
            }
        });

        //FIXME:  WTF move this to view
        $scope.showPcPlus = function(){
            var pcPlusWidget = $('.lp-chrome-launcher-tab-hidden')
                        .filter('[data-pid*="pcf-pc-plus"]')
                            .find('a[href*="#pcf-pc-plus"]');
            pcPlusWidget.click();
        };

        // Responsive
        responsive.enable($rootElement)
            .rule({
                "max-width": 200,
                then: function() {
                    $scope.responsiveClass = "lp-tile-size";
                    util.applyScope($scope);
                }
            })
            .rule({
                "min-width": 201,
                "max-width": 350,
                then: function() {
                    $scope.responsiveClass = "lp-small-size";
                    util.applyScope($scope);
                }
            }).rule({
                "min-width": 351,
                "max-width": 600,
                then: function() {
                    $scope.responsiveClass = "lp-normal-size";
                    util.applyScope($scope);
                }
            }).rule({
                "min-width": 601,
                then: function() {
                    $scope.responsiveClass = "lp-large-size";
                    util.applyScope($scope);
                }
            });

        initialize();
    }]);

    return function(widget) {
        app.value("widget", widget);
        angular.bootstrap(widget.body, ["launchpad-retail.accounts"]);
    };
});
