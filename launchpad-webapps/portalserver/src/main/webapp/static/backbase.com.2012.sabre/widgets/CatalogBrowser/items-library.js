/**
 * Copyright (c) 2013 Backbase B.V.
 */
(function() { // Start Closure

    // all strings should go here
    var MSG = {
        NO_RESULTS_FOR_FILTER: "No results. There is no item that matches this filter."
    };

    var scope = function() {
        return this;
    }();

    scope.makeLibraryWidget = function(oWidget) {
        b$.mixin(oWidget, widget);
        oWidget.updateHTML();
    };

    var widget = {
        /**
         * Mandatory flag to allow dragging items from widget
         * @type {Boolean}
         */
        isCatalog: true,
        /**
         * Currently active section
         * @type {String}
         */
        selectedSection: null,
        /**
         * Current widget state (open/closed)
         * @type {Boolean}
         */
        scrollAreaVisible: false,
        /**
         * Used for paging calculations
         * @type {Number}
         */
        thumbnailWidth: 120,
        /**
         * Current pager state
         * @type {Number}
         */
        currentPage: 0,
        /**
         * List of available tags in selected section
         * @type {Array}
         */
        tagsArray: [],
        /**
         * Selected tag
         * @type {String}
         */
        tagsFilter: '',

        /**
         * Various jQuery objects to access elements of widget
         * @type {jqObject}
         */
        $tagsHolder: null,
        $searchBox: null,
        $widgetWrapper: null,
        $toolbarWrapper: null,

        /**
         * Override functions to prevent design tools showup
         * @return {[type]} [description]
         */
        enableDesignMode: function(){},
        disableDesignMode: function(){},

        /**
         * Render widget body
         * @return {Void}
         */
        updateHTML: function() {
            var self = this;

            this.$widgetWrapper = jQuery('.pageDesignerItemWrapper', this.body);
            this.$tagsHolder = jQuery('.tb-tags-list', this.body);
            this.$toolbarWrapper = jQuery('.pageDesignerToolbarWrapper', this.body);

            /* Render body */
            var templateUrl = be.contextRoot + "/static/backbase.com.2012.sabre/widgets/CatalogBrowser/catalogTemplates/blankTemplate.html";
            this.$widgetWrapper.html(be.utils.processHTMLTemplateByUrl(templateUrl, {
                isVirtualWidget: true,
                noItemsMessage: MSG.NO_RESULTS_FOR_FILTER
            }));

            this.$stripViewPort = jQuery(".pm-composer-stripViewPort", this.body);
            this.$strip = jQuery(".pm-composer-strip", this.body);
            this.pagingControl = jQuery(".pm-designer-pager", this.body)[0];

            /* Build toolbar */
            be.utils.ajax({
                url: be.contextRoot + '/portals/' + encodeURI(b$.portal.portalName) + '/catalogTags.xml?ps=-1',
                encodeURI: false,
                cache: false,
                success: function(data) {
                    var $data = jQuery(data).eq(-1);
                    if (jQuery.browser.msie && jQuery.browser.version < 9) {
                        var doc = new ActiveXObject('Microsoft.XMLDOM');
                        doc.async = 'false';
                        doc.loadXML( data );
                        data = doc;
                        $data = jQuery(data).find('tags');
                    }
                    var sections = [];
                    $data.find('> tag[type="section"]').each(function(idx, item){
                        var label = jQuery(item).text();
                        sections.push({
                            name: label
                        });
                    });
                    self.$toolbarWrapper.html(be.utils.processHTMLTemplateByUrl(be.contextRoot + "/static/backbase.com.2012.sabre/widgets/CatalogBrowser/catalogTemplates/catalogToolbar.html", {
                        sections: sections.sort(function(a, b){
                            return a.name > b.name ? 1 : b.name > a.name ? -1 : 0;
                        }),
                        searchFilter: self.searchFilter ? self.searchFilter : ''
                    }));

                    if (self.selectedSection){
                        self.$toolbarWrapper.find('.tb-button[data-section="' + self.selectedSection + '"]').addClass('tb-active');
                    }

                    self.initSearch();
                }
            });
            this.initToolbar();

            /* Apply widget state (is exists) */
            this.loadWidgetState();
            this.$toolbarWrapper.parents('.pageDesignerWrapper').toggleClass('tb-tags-active', this.tagsOpen || false);

            /* Register interface handlers */
            this.registerEventHandlers();
        },

        /**
         * Initialize widget toolbar
         * @return {Void}
         */
        initToolbar: function(){
            var self = this;
            this.$toolbarWrapper.on('click', '.tb-button', function(event){
                var $this = jQuery(this);
                var section = $this.data('section');
                self.tagsFilter = '';
                self.applyFilters();

                $this.removeClass('tb-active').siblings().removeClass('tb-active');
                if ($this.hasClass('tb-openClose')){
                    if (self.scrollAreaVisible){
                        self.selectedSection = null;
                        self.hideScrollArea();
                    } else {
                        var $activeSection = $this;
                        self.selectedSection = $activeSection.data('section');
                        self.showScrollArea();
                        self.loadCatalog(self.selectedSection);
                        $activeSection.addClass('tb-active');
                    }
                } else if (self.selectedSection === section){
                    self.selectedSection = null;
                    self.hideScrollArea();
                } else {
                    self.selectedSection = section;
                    self.showScrollArea();
                    self.loadCatalog(section);
                    $this.addClass('tb-active');
                }

                self.saveWidgetState();
            });

            this.$toolbarWrapper.on('click', '.tb-filter-tags', function(event){
                var $this = jQuery(this);
                $this.parents('.pageDesignerWrapper').toggleClass('tb-tags-active');
                if (!$this.parents('.pageDesignerWrapper').hasClass('tb-tags-active')){
                    self.$tagsHolder.find('.tb-tag-name').removeClass('tb-tag-selected');
                    self.tagsFilter = '';
                    self.applyFilters();
                }
                self.tagsOpen = $this.parents('.pageDesignerWrapper').hasClass('tb-tags-active');
                self.saveWidgetState();
            });
        },

        /**
         * Initialize search feature
         * @return {Void}
         */
        initSearch: function(){
            var self = this;
            this.$searchBox = jQuery('.tb-search', this.body);
            this.$searchBox.off().on('keyup', 'input[type="text"]', function(event){
                if (event.keyCode === 27){
                    self.searchFilter = null;
                    jQuery(this).val('');
                    self.applyFilters(true);
                } else {
                    self.searchFilter = this.value;
                    self.applyFilters(true);
                }
            }).on('click', '.tb-search-cancel', function(){
                self.searchFilter = null;
                jQuery('input[type="text"]', self.$searchBox).val('');
                self.applyFilters(true);
            }).on('click', '.tb-search-button', function(){
                jQuery('input[type="text"]', self.$searchBox).focus();
            });
        },

        /**
         * Apply tags filter and search condotion to list of items in library
         * @param  {Boolean} forceTagsReload
         * @return {Void}
         */
        applyFilters: function(forceTagsReload){
            var self = this;
            this.$widgetWrapper.find('.pm-panelCatalogIcon, .pm-widgetCatalogIcon').each(function(idx, item){
                var $item = jQuery(item);
                $item.removeClass('tb-filtered');

                // Filter by tag
                if (self.tagsFilter && item.catalogItemJson.tagsArray.indexOf(self.tagsFilter) === -1){
                    $item.addClass('tb-filtered');
                }
                // Filter by title
                if (self.searchFilter){
                    var titleRe = new RegExp('(' + self.searchFilter + ')', 'i');
                    if (!$item.find('.pm-icon2-label').text().match(titleRe)){
                        $item.addClass('tb-filtered');
                    }
                }

                if (self.searchFilter || forceTagsReload){
                    self.updateTags();
                }
            });

            this.highlightSearchResult();
            if (this.pagingControl){
                this.recalculateViewPortSize(0);
            }

            this.saveWidgetState();
        },

        /**
         * Highlight found text substring in item titles
         * @return {Void}
         */
        highlightSearchResult: function() {
            var self = this;
            jQuery('.pm-panelCatalogIcon, .pm-widgetCatalogIcon', this.$widgetWrapper).each(function(idx, item) {
                var $item = jQuery(item),
                    itemName = String(item.catalogItemJson.preferences.title.value),
                    itemModel = item.catalogItemJson,
                    re;

                if (self.searchFilter) {
                    re = new RegExp('(' + self.searchFilter + ')', 'i');
                }
                if (!itemModel || !itemName.match(re) && !itemModel.preferences.title.value.match(re) || !self.searchFilter) {
                    if ($item.data('originalTitle') !== undefined) {
                        $item.find('.pm-icon2-label').text(Mustache.to_html('{{text}}', {
                            text: be.utils.truncateText($item.data('originalTitle'), 25)
                        }));
                    }
                } else {
                    $item.data('originalTitle', itemModel.preferences.title.value);
                    $item.find('.pm-icon2-label').html(Mustache.to_html('{{text}}', {
                        text: be.utils.truncateText(itemModel.preferences.title.value, 25)
                    }).replace(re, "<span class='tb-search-highlight'>$1</span>"));
                }
            });
        },

        /**
         * Listen to global page event messages
         * @param  {Object} e - Event object
         * @return {Void}
         */
        handleMessage: function(e) {
            if (e.data == 'hideScrollArea') {
                this.hideScrollArea();
            }
        },

        /**
         * Assign various event listeners to UI elements
         * @return {Void}
         */
        registerEventHandlers: function() {
            var self = this;

            /* Paging handlers */
            jQuery('.pm-composer', self.htmlNode).off().on('mousewheel DOMMouseScroll', function(event) {
                event.preventDefault();
                event.stopPropagation();
                self.handleScroll(event.originalEvent.wheelDelta || -event.originalEvent.detail);
            });
            jQuery('.pm-composer-previous', self.htmlNode).off().on('click', function(event) {
                self.handlePrevious();
            });
            jQuery('.pm-composer-next', self.htmlNode).off().on('click', function(event) {
                self.handleNext();
            });

            /* Tags handlers (filtering and scroll) */
            this.$tagsHolder.off().on('click', '.tb-tag-name', function(event){
                var $this = jQuery(this);
                var tag = $this.data('tag');
                if (self.tagsFilter === tag){
                    $this.removeClass('tb-tag-selected');
                    self.tagsFilter = '';
                } else {
                    $this.addClass('tb-tag-selected').siblings().removeClass('tb-tag-selected');
                    self.tagsFilter = tag;
                }
                self.applyFilters();
            }).on('click', '.tb-tag-next', function(event){
                self.scrollTags(-1);
            }).on('click', '.tb-tag-prev', function(event){
                self.scrollTags(1);
            }).on('mousewheel DOMMouseScroll', function(event) {
                event.preventDefault();
                event.stopPropagation();
                self.scrollTags(event.originalEvent.wheelDelta || -event.originalEvent.detail);
            });

            /* Listen to global messages */
            if (window.addEventListener){
                addEventListener("message", jQuery.proxy(this.handleMessage, this), false);
            } else {
                attachEvent("onmessage", jQuery.proxy(this.handleMessage, this));
            }
        },

        /**
         * Make a call to REST API to load items in current section
         * @param  {String} section
         * @return {Void}
         */
        loadCatalog: function(section) {
            var self = this;
            var portalServer = b$.portal.portalServer;
            be.utils.ajax({
                url: b$.portal.config.serverRoot + '/portals/' + encodeURI(b$.portal.portalName) + '/catalog.xml?f=type(eq)WIDGET|CONTAINER&ps=-1' + (section !== 'all' ? '&f=tag.section(eq)' + encodeURI(section) : ''),
                encodeURI: false,
                cache: false,
                success: function(data) {
                    var currentTbButton = self.$toolbarWrapper.find('.tb-active').data('section');
                    //if the loaded items are not belong to the selected catalog, ignore it
                    if(currentTbButton && section != currentTbButton) return;

                    var $catalog = $(data).eq(-1);
                    if ($.browser.msie) {
                        var doc = new ActiveXObject('Microsoft.XMLDOM');
                        doc.async = 'false';
                        doc.loadXML( data );
                        data = doc;
                    }else{
                        data = jQuery.parseXML(data);
                    }
                    var item = portalServer.itemXMLDOC2JSON(data);
                    var itemList = item.children;
                    itemList = self.addTagsListToJson(data, item.children);

                    for (var i = itemList.length - 1; i > -1; i--) {
                        if (top.bd && top.bd.PageMgmtTree && top.bd.PageMgmtTree.selectedLink.isMasterPage !== true) {
                            if (itemList[i].preferences.isManageableArea && //manageable area
                                itemList[i].preferences.isManageableArea.value == 'true') {
                                itemList.splice(i, 1);
                            }
                        } else { //in master page
                            if (itemList[i].preferences.parentLinkUUID || itemList[i].extendedItemName === "BreadcrumbNav") { //tab container or breadcrumb
                                itemList.splice(i, 1);
                            }
                        }
                    }

                    self.jsonData = itemList; //item.children;
                    self.appendHTMLIcons(self.jsonData);
                    self.updateTags();
                    self.applyFilters();
                    if (self.origCurrentPage){
                        self.setCurrentPage(self.origCurrentPage);
                        delete self.origCurrentPage;
                    }
                }
            });
        },

        /**
         * Update list of tags
         * @return {Void}
         */
        updateTags: function(){
            var self = this;
            this.tagsArray = [];
            jQuery('.pm-panelCatalogIcon:visible, .pm-widgetCatalogIcon:visible', this.$widgetWrapper).each(function(idx, item){
                self.tagsArray = self.tagsArray.concat(item.catalogItemJson.tagsArray);
            });

            var tmpArray = [];
            for (var i = 0, l = this.tagsArray.length; i < l; i++){
                if (tmpArray.indexOf(this.tagsArray[i]) === -1){
                    tmpArray.push(this.tagsArray[i]);
                }
            }
            this.tagsArray = tmpArray;

            var tplData = {
                tags: this.tagsArray.sort()
            };
            this.$tagsHolder.html(jQuery(be.utils.processHTMLTemplateByUrl(be.contextRoot + "/static/backbase.com.2012.sabre/widgets/CatalogBrowser/catalogTemplates/tagsTemplate.html", tplData)));

            this.$tagsHolder.find('.tb-tag-name[data-tag="' + this.tagsFilter + '"]').addClass('tb-tag-selected');
            this.$tagsStripViewport = this.$tagsHolder.find('.tb-tags-strip-viewport');
            this.$tagsHolder.removeClass('has_next has_prev');

            var totalWidth = 0, maxWidth = this.$tagsHolder.width();
            this.$tagsHolder.find('.tb-tag-name').each(function(idx, item){
                totalWidth += item.offsetWidth + 16;
            });

            this.$tagsStripViewport.width(totalWidth);

            if (totalWidth > maxWidth){
                this.$tagsHolder.addClass('has_next');
            }
        },

        /**
         * Scroll tags list
         * @param  {Number} dir - negative or positive for different scroll directions
         * @return {Void}
         */
        scrollTags: function(dir){
            if (this.$tagsStripViewport.width() < this.$tagsHolder.width()){
                return;
            }
            var curLeft = parseInt(this.$tagsStripViewport.css('left'), 10), newLeft;
            if (dir > 0){
                newLeft = curLeft + 200;
            } else {
                newLeft = curLeft - 200;
            }
            newLeft = Math.min(25, newLeft);
            newLeft = Math.max(newLeft, -(this.$tagsStripViewport.width() - this.$tagsHolder.width()));
            this.$tagsStripViewport.css('left', newLeft);
            this.$tagsHolder.toggleClass('has_prev', newLeft < 25);
            this.$tagsHolder.toggleClass('has_next', newLeft > -(this.$tagsStripViewport.width() - this.$tagsHolder.width()));
        },

        /**
         * Add tags list to item json
         * @param {Object} xmlData
         * @param {Object} jsonData
         */
        addTagsListToJson: function(xmlData, jsonData) {
            var $data = jQuery(xmlData);
            this.tagsArray = [];

            for (var i = 0, len = jsonData.length; i < len; i++) {
                var xpathStr = '//' + jsonData[i].tag + '[name = "' + jsonData[i].name + '"]/tags';
                var nodes = be.utils.xpathNodes(xmlData, xpathStr);
                var tagsArray = [];
                for (var j = 0; j < nodes[0].childNodes.length; j++) {
                    try {
                        if (jQuery(nodes[0].childNodes[j]).attr('type') === 'regular'){
                            tagsArray.push(nodes[0].childNodes[j].childNodes[0].nodeValue);
                        }
                    } catch (e) {}
                }
                jsonData[i].tagsArray = tagsArray;
            }
            return jsonData;
        },

        /**
         * Generate HTML for list of items
         * @param  {Object} jsonData
         * @return {Void}
         */
        appendHTMLIcons: function(jsonData) {
            var self = this;
            self.$strip.html('');
            if (jsonData && jsonData.length) {
                for (var i = 0; i < jsonData.length; i++) {
                    var elm = self.buildHTMLIcon(jsonData[i]);
                    self.$strip.append(elm);
                }
            }
            self.recalculateViewPortSize(0);
        },

        /**
         * Expand widget
         * @return {Void}
         */
        showScrollArea: function() {
            var oPageDesignWrapper = jQuery('.pageDesignerWrapper', this.htmlNode)[0];
            jQuery(oPageDesignWrapper).addClass('pageDesignerWrapper-open');
            jQuery('body').addClass('pageDesignerWithCatalog');
            this.scrollAreaVisible = true;
            jQuery('.tb-search > input').focus();
        },

        /**
         * Collapse widget
         * @return {Void}
         */
        hideScrollArea: function() {
            var oPageDesignWrapper = jQuery('.pageDesignerWrapper', this.htmlNode)[0];
            jQuery(oPageDesignWrapper).removeClass('pageDesignerWrapper-open');
            jQuery('body').removeClass('pageDesignerWithCatalog');
            this.scrollAreaVisible = false;
        },

        /**
         * Recalulate width of viewport
         * @param  {Number} pageNumber
         * @return {Void}
         */
        recalculateViewPortSize: function(pageNumber) {
            var viewPortWidth = parseInt(this.$stripViewPort.css("width"), 10);
            if (viewPortWidth < this.thumbnailWidth || isNaN(viewPortWidth))
                viewPortWidth = this.thumbnailWidth;
            var itemsPerPage = Math.floor(viewPortWidth / this.thumbnailWidth);

            var newPagenumber = isNaN(pageNumber) ? this.getNewPage(itemsPerPage) : pageNumber;
            this.setNumberOfItemsPerPage(itemsPerPage);

            this.$strip.css("width", this.thumbnailWidth * this.getNumberOfItems());
            this.$widgetWrapper.toggleClass('empty-set', this.getNumberOfItems() === 0);

            this.setCurrentPage(newPagenumber);
        },

        /**
         * Returns number of items in current set (taking into account filters)
         * @return {Number}
         */
        getNumberOfItems: function() {
            return jQuery('.pm-panelCatalogIcon:visible, .pm-widgetCatalogIcon:visible', this.$widgetWrapper).length;
        },
        getTotalNumberOfPages: function() {
            var iItems = this.getNumberOfItems();
            if (iItems)
                return Math.ceil(iItems / this.getNumberOfItemsPerPage());
            else
                return 0;
        },

        getNewPage: function(itemsPerPage) {
            var currentPage = this.getCurrentPage();
            var currentItemsPerPage = this.getNumberOfItemsPerPage();
            var nPosition = currentPage * currentItemsPerPage;
            return Math.floor(nPosition / itemsPerPage);
        },

        setNumberOfItemsPerPage: function(itemsPerPage) {
            this.itemsPerPage = itemsPerPage;
        },

        setCurrentPage: function(pageNumber) {
            if ((pageNumber > 0 && pageNumber < this.getTotalNumberOfPages()) || pageNumber === 0) {
                this.currentPage = pageNumber;
                this.scrollPage();
            }
            this.renderPagingControl();
            this.saveWidgetState();
        },

        getCurrentPage: function() {
            return this.currentPage;
        },

        getNumberOfItemsPerPage: function() {
            return this.itemsPerPage || 0;
        },

        handleNext: function() {
            this.setCurrentPage(this.getCurrentPage() + 1);
        },

        handlePrevious: function() {
            this.setCurrentPage(this.getCurrentPage() - 1);
        },

        scrolling: false,

        handleScroll: function(delta) {
            if (!this.scrolling) {
                this.scrolling = true;
                if (delta < 0) {
                    this.handleNext();
                } else if (delta > 0) {
                    this.handlePrevious();
                }
                var self = this;
                setTimeout(function() {
                    self.scrolling = false;
                }, 500);
            }
        },

        scrollPage: function() {
            var nCurrentPage = this.getCurrentPage();
            var nNewLeft = nCurrentPage * this.getNumberOfItemsPerPage() * this.thumbnailWidth;
            var self = this;

            if (self.origCurrentPage){
                this.$strip.css({
                    left: -nNewLeft
                });
                this.updateButtonClasses();
            } else {
                this.$strip.animate({
                    left: -nNewLeft
                }, 500, 'swing', function() {
                    self.updateButtonClasses();
                });
            }
        },

        updateButtonClasses: function() {
            var oPreviousButton = jQuery('.pm-composer-previous', this.htmlNode);
            var oNextButton = jQuery('.pm-composer-next', this.htmlNode);

            if (this.getCurrentPage() === 0) {
                oPreviousButton.addClass("pm-composer-previous-disabled");
            } else {
                oPreviousButton.removeClass("pm-composer-previous-disabled");
            }
            if (this.getCurrentPage() >= this.getTotalNumberOfPages() - 1) {
                oNextButton.addClass("pm-composer-next-disabled");
            } else {
                oNextButton.removeClass("pm-composer-next-disabled");
            }
        },

        renderPagingControl: function() {
            var oThis = this;
            var fnClick = function(oEvent) {
                var nNewPage = jQuery(oEvent.target).data("page");
                oThis.setCurrentPage(nNewPage);
            };
            this.pagingControl.innerHTML = "";

            for (var i = 0; i < this.getTotalNumberOfPages(); i++) {
                var oPage = jQuery('<div class="designer-pager-page"></div>')
                    .data("page", i)
                    .click(fnClick)
                    .toggleClass('designer-pager-page-current', i == this.getCurrentPage());
                this.pagingControl.appendChild(oPage[0]);
            }
        },

        buildHTMLIcon: function(json) {
            var thumbnailVar = json.preferences['thumbnailUrl'].value;
            thumbnailVar = thumbnailVar.replace("$(contextRoot)", b$.portal.config.serverRoot);

            var isDeprecated = false;
            for (var i = 0; i < json.tags.length; i++) {
                if (json.tags[i].value.toLowerCase() == 'deprecated') {
                    isDeprecated = true;
                };
            };

            var tplData = {
                isVirtualWidget: true,
                iconCss: jQuery('html').hasClass('ie-6') ? 'filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src="'+ thumbnailVar + '",sizingMethod="scale")' : 'background-image: url(' + thumbnailVar + ')',
                title: be.utils.truncateText(json.preferences['title'].value, 25),
                description: json.preferences.Description ? json.preferences.Description.value : null,
                deprecated: isDeprecated
            };

            var html = jQuery(be.utils.processHTMLTemplateByUrl(be.contextRoot + "/static/backbase.com.2012.sabre/widgets/CatalogBrowser/catalogTemplates/tileTemplate.html", tplData))[0];
            html.catalogItemJson = json; // reference
            return html;
        },

        /**
         * Save widget state outside the scope to make it available after iframe refresh
         * @return {Void}
         */
        saveWidgetState: function(){
            top.bd.CatalogBrowser = top.bd.CatalogBrowser || {};
            var state = {
                scrollAreaVisible: this.scrollAreaVisible,
                selectedSection: this.selectedSection,
                tagsOpen: this.tagsOpen,
                tagsFilter: this.tagsFilter,
                searchFilter: this.searchFilter,
                currentPage: this.currentPage
            };
            top.bd.CatalogBrowser.state = state;
            // console.log(state);
        },

        /**
         * Take widget state from outside the scope and apply
         * @return {Void}
         */
        loadWidgetState: function(){
            if (!top.bd.CatalogBrowser){
                return;
            }
            var state = top.bd.CatalogBrowser.state;
            this.scrollAreaVisible = state.scrollAreaVisible || false;
            this.selectedSection = state.selectedSection || 'all';
            if (this.scrollAreaVisible){
                var $activeSection = jQuery(this.body).find('.tb-button[data-section="' + (this.selectedSection || 'all') + '"]');
                this.showScrollArea();
                this.loadCatalog(this.selectedSection);
                $activeSection.addClass('tb-active');
            }
            this.tagsOpen = state.tagsOpen || false;
            this.searchFilter = state.searchFilter;
            this.tagsFilter = state.tagsFilter;
            this.origCurrentPage = state.currentPage;
        }
    };
})();
