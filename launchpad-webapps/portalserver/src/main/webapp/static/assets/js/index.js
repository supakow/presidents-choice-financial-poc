$(function(){

	var scrollCheck = function(){
		if (sy=window.scrollY) {
			$('#header').addClass('contracted');
			if ((sb=sy+$(window).innerHeight())>$('.mastercard').offset().top){
				$('.mastercard').addClass('up');
				if (sb>(phone=$('.phone')).offset().top)
					phone.addClass('up');
			}
		}
		else $('#header').removeClass('contracted');
	};

	$('#hamburger').click(function(){
		$('ul.nav').toggleClass('open');
	});
	$('.sliding').flexslider({
		animation: 'slide',
		prevText: "",
		nextText: "",
		start: scrollCheck
	});
	$('.nav > li > a').click(function(e){
		e.preventDefault();
		$(this.parentNode).siblings().removeClass('open');
		$(this.parentNode).toggleClass('open');
	});
	$('#hat > .close, #header > .close').click(function(){
		$('.nav > li, ul.nav').removeClass('open');
	});
	$('.contain > .close').click(function(){
		$('.nav > li').removeClass('open');
	});
	$('.tooltip').click(function(){
		$('.tooltip').removeClass('on');
		$(this).addClass('on');
		tooltipText.innerHTML = $(this).data('caption');
	});
	$('#hero1').css('background-size',$(window).innerWidth()+'px '+Math.round($(window).innerWidth*837/1255)+'px').paraBax();
	$('#hero2').css('background-size',$(window).innerWidth()+'px '+Math.round($(window).innerWidth*1065/1600)+'px').paraBax();
	$('#hero4').css('background-size',$(window).innerWidth()+'px '+Math.round($(window).innerWidth*868/1280)+'px').paraBax();

	$(window).on('scroll.navbar',scrollCheck);

	$('.login > .button3').click(function(){
		$('.login').addClass('open');
	});
	$('.login .close').click(function(){
		$('.login').removeClass('open');
	})
	$('a[href*=#]:not([href=#])').click(function(e) {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			if (target.length) {
				$('html,body').animate({
					scrollTop: (target.offset().top-46)
				}, 1000);
				e.preventDefault();
			}
		}
	});

	$('.clickable').click(function(){
		$("[data-page]:not(.clickable)", this.parentNode.parentNode).css('display','none');
		$("[data-page="+$(this).data("page")+"]:not(.clickable)", this.parentNode.parentNode).css('display','initial');
		$(this).siblings().removeClass('open');
		$(this).addClass('open');
	})

});