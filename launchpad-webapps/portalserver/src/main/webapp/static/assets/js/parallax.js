/*
 *
 * Parallax Backgrounds in jQuery
 * Parabax. Get it?
 *
 * Calum Smith 2014
 *
 * Bare necessities, still not very performant with big elements.
 *
 */

(function($){
	$.paraBax = function(el,options){
		var $l = $(el),
			opts = $.extend({},$.fn.paraBax.defaults, options),
			methods = {};

		$l.hollaBax = function(){
			// if (!drawing) {
			// 	drawing = true;
				window.requestAnimationFrame(function(){
					if ((os=$l.offset()).top<(vpt=window.scrollY)+$(window).innerHeight()&&vpt<os.top+$l.innerHeight()) {
						var pos = 35*((vpt-(os.top+$l.innerHeight()))/((os.top-$(window).innerHeight())-os.top));
						// start2 + (stop2 - start2) * ((value - start1) / (stop1 - start1))
						el.style.backgroundPosition = ('50% '+pos.toFixed(1)+'%');
					}
					// drawing = false;
				})
			// }
		}

		$l.data('paraBax',this);
		$l.data('paraInterval',setInterval($l.hollaBax,17));
	}
	$.fn.paraBax = function(options){
		options = options || {};
		var opts = $.extend({},$.fn.paraBax.defaults, options);

		return this.each(function(){
			new $.paraBax(this,opts);
		});
	}
	$.fn.paraBax.defaults = {

	}
})(jQuery);