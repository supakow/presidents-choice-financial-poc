var utils = function(widget){
	this.widget = widget;
};

utils.prototype	= {};

utils.prototype.PerformProxyRequest		= function (Pipe, URL, Params, TargetObject) {
	return top.$.ajax({
	    url: "/bb_portal/proxy?pipe=" + Pipe + "&url=" + URL + Params,
	    username:'admin',
	    password:'admin',
	    method:'POST',
	    dataType:'html',
	    async:true,
	    success: function(oData){
	    	top.$(TargetObject).css("opacity","0");
	    	TargetObject.innerHTML = oData;
	    	top.$(TargetObject).animate({
	    		opacity:1
	    	},300);
	    },
	    error: function(jqXHR, textStatus, errorThrown){
	    	TargetObject.innerHTML += "<p>" + textStatus + " : " + errorThrown + "</p>";
	    }
	});
};

utils.prototype.SetSetting				= function(Setting, Value){
    this.widget._coreWidget.setPreference(Setting,Value);
    this.widget._coreWidget.save();
};

utils.prototype.GetSetting				= function(Setting, DefaultValue) {
	var returnvalue = widget.preferences.getItem(Setting);
	if(returnvalue == null) {
		if(typeof(DefaultValue)!=="undefined") {
			returnvalue = DefaultValue;
		}
	}
	return returnvalue;
};

utils.prototype.addCMISBrowseButton     = function(){
   var oWidget = this.widget;
   
   var top2 = oWidget.viewGate.parent;
   
   //Use jQuery to build the button out of a string.
   var oButton = top2.$('<li class="aa-widget-head-right-list-item" style="width:30px!important;"><a title="Backbase CMIS" id="cmsButton_' + oWidget.id.replace(/ /i,"_") + '" class="bb_cmis_button" style="margin-right:5px"><i style="background:none;">cms</i></a></li>');
   
   //Add the bootstrap CSS and js library if not already there.
   var script   = top2.$('<script type="text/javascript" src="/bb_pe/utilities/jQuery_Plugins/bootstrap/js/bootstrap.js"/>');
   var script2  = top2.$('<script type="text/javascript" src="/bb_pe/utilities/cmis/browser_controller.js"/>');
  
   var style    = top2.$('<link rel="stylesheet" type="text/css"  href="/bb_pe/utilities/jQuery_Plugins/bootstrap/css/bootstrap.min.css"/>');
   var style2   = top2.$('<link rel="stylesheet" type="text/css" href="/bb_pe/utilities/cmis/cmis_browser.css" />');
   
   var jcmisbrowserdialog = top2.$("#bb_cmisbrowser_dialog");
   
   if(jcmisbrowserdialog.length==0){
       jcmisbrowserdialoghtml = top.$.ajax({
            url: "/bb_pe/utilities/cmis/browser.html",
            method:'GET',
            dataType:'text/html',
            async:false
       }).responseText; 

       top2.$("body").append(jcmisbrowserdialoghtml);
       jcmisbrowserdialog = top2.$("#bb_cmisbrowser_dialog");
       top2.$("head").append(script);
       top2.$("head").append(script2);
       top2.$("head").append(style);
       top2.$("head").append(style2);
       top2.$('#bb_cmisbrowser_dialog').modal({
           show:false
       });
       if(top2.bb.cmisBrowser == undefined){
    	   top2.bb.cmisBrowser = new top2.bb.cmisbrowser();
       }
   }
   
   var self = this;
   
   //You need to use the root documents jQuery object to target the w3c oWidgets by ID.
   //var oWidgetView = top2.document.getElementById("widgetContainer_" + oWidget.id);
   var oWidgetView  = top2.$(".bp-widget[data-pid='" + oWidget.name + "']");
   
   //the container oWidget object id is changed between 5.1 and 5.1.2
   if(oWidgetView==null) oWidgetView = top2.document.getElementById(oWidget.id);

   if(oWidgetView == null){
	   oWidgetView = oWidget.viewGate.parent.document.getElementById(oWidget.id);
	   if(oWidgetView == null){
		   oWidgetView = oWidget.viewGate.parent.document.getElementById("widgetContainer_" + oWidget.id);
	   }
   }
   
   top2.$(oWidgetView).find("#cmsButton_" + self.widget.id).unbind("click");
   top2.$(oWidgetView).find("#cmsButton_" + self.widget.id).remove();
   
   //Now rebuild the chrome button styling.
   var oBtnContnr   = top2.$(oWidgetView).find("ul.aa-widget-head-right");
   var iWidth       = top2.$(oBtnContnr).width() + 30;

   top2.$(oBtnContnr).width(iWidth);

   var oFirstButton = top2.$(oWidgetView).find(".aa-widget-head-right li").first();

   //Condition to check if buttons are even in the chrome.
   if(oFirstButton == undefined){//No buttons found, so simply append your's to the chrome
      $(oWidgetView).find(".bd-chromeButtons").append(oButton);
   }else{//you have buttons so insert your button first.
	   top2.$(oButton).insertBefore(oFirstButton);
   }
   self.widget.CMSButton    = oButton;//Store the CMS button reference to remove it from the jQuery register.
   
   top2.$("#cmsButton_" + self.widget.id.replace(/ /i,"_")).unbind("click");
   top2.$("#cmsButton_" + self.widget.id.replace(/ /i,"_")).click(function(event){
     top2.bb.cmisBrowser.showBrowser(self.widget); 
   });
};

utils.prototype.GetUniqueValue			= function(key){
	var datetime = new Date();
	alert(datetime.toUTCString());
};

utils.prototype.parseBool				= function(value){
	return value.toLowerCase() == "true" ? true : false;
};

utils.prototype.autoSize				= function(){
	var nHeight = document.documentElement.scrollHeight;
    window.frameElement.height = nHeight - 15;
    window.frameElement.width = '100%';				
}
