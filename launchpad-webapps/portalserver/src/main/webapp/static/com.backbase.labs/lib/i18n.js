if (!window.bb_labs) window.bb_labs = {};
if (!window.bb_labs.i18n) window.bb_labs.i18n = {};

bb_labs.i18n = function(sCSSselector){
	this.cssSelector	= "." + sCSSselector;
	this.context		= null;
	this.ptc 			= {};
	this.ptc.serverUrl	= b$.portal.config.serverRoot;
	this.ptc.pipe 		= "i18nPipe";
	this.ptc.showerrors = false;
	this.cache 			= bb_labs.getSessionValue("i18n_cache")!=""?JSON.parse(bb_labs.getSessionValue("i18n_cache")):{};
	this.locale			= bb_labs.getSessionValue("locale")!=""?bb_labs.getSessionValue("locale"):"en-us";
};

bb_labs.i18n.prototype 	= {};
bb_labs.i18n.prototype.bootContent = function(){

	//First get all the target items.
	var self = this;
	if(self.context!=null){
		//Loop through each item and get it's contents.
		top.$(self.context).find(self.cssSelector).each(function(index,element){
			self.loadContentIntoItem(element);
		});
		
	} else {
		//Loop through each item and get it's contents.
		top.$(self.cssSelector).each(function(index,element){
			self.loadContentIntoItem(element);
		});
		
	}
};
bb_labs.i18n.prototype.bootWidgetControlers = function(){
	var portalviews = b$.portal.portalView.all;
	for (var key in portalviews) {
		var portalview = portalviews[key];
		if(typeof(portalview.controller)!="undefined"){
			if(typeof(portalview.controller.i18n)!="undefined"){
				portalview.controller.i18n();
			}
		}
	}
};

bb_labs.i18n.prototype.loadContentIntoItem = function(oElem){

	var aClasses	= $(oElem).attr("class").split(" ");
	var sResource 	= "";
	var sType 		= "text";
	var failsafe	= false;
	
	$(aClasses).each(function(index,sName){

		if(sName.indexOf("resourceId_") != -1){
			sResource =  sName.split("resourceId_")[1];
		}
		
		if(sName.indexOf("resourceType_") != -1){
			sType =  sName.split("resourceType_")[1];
		}

		if(sName.indexOf("resourceFailSafe") != -1){
			failsafe =  true;
		}

	});

	var content = "";
	var isCSS	= false;
	
	switch(sType){
	
		case "text":
			content = this.getResource(sResource);
			if(content == null){
				if(failsafe){
					content = oElem.innerHTML;
				} else {
					content = "Invalid or Non-Existant Resource [" + sResource + "].";
					$(oElem).css("color","white");
					$(oElem).css("background-color","#ffb7c5");
				}
			}else{//Content is not null, check for arguments
				content = this.replaceContentArgs(content,oElem);
			}
			break;
			
		case "image":
			content 	= "<img src='" + this.getBinaryResource(sResource) + "'/>";
			break;
		
		case "css":
			content 	= this.getResource(sResource);
			isCSS		= true;
			break;
		case "flash":
			content		= "<embed width='100%' height='100%' name='plugin' src='" + this.getBinaryResource(sResource) + "' type='application/x-shockwave-flash'/>";
			break;
	}
	
	(isCSS == false)?$(oElem).html(content):$("head").append('<style type="text/css">' + content + '</style>');
};


bb_labs.i18n.prototype.replaceContentArgs = function(content,oElem){

	var self 		= this;
	var aAttrVal 	= [];
	var count 		= 0;
	var stop 		= false;

	while(stop == false){//Get all the argument atributes
		var attr = $(oElem).attr('arg_'+count);
		if(attr === undefined){
			stop = true;
		}else{
			aAttrVal.push(attr);
			count++;
		}
	}
	
	if(count == 0){//didn't find any arguments for replacement.
		return content;
	}

	for(var x=0;x<aAttrVal.length;x++){//Now do the replacement.
		var sToReplace 	= "~__%" + x + "%__~";
		var regex 		= new RegExp(sToReplace, 'g');
		content 		= content.replace(regex, aAttrVal[x]);
	}	
	return content;
};

bb_labs.i18n.prototype.setLocale = function(locale)
{
	this.locale = locale;
	bb_labs.setSessionValue("locale", locale);
};
bb_labs.i18n.prototype.setContext = function(context)
{
	this.context = context;
};



//Check if resource exist in cache
//if not, then gets it from cms using cmis
bb_labs.i18n.prototype.getResource = function(resourceID)
{
	return this.getResourceByLocale(resourceID, this.locale, false);
}
bb_labs.i18n.prototype.getResourceFailSafe = function(resourceID)
{
	var resourceid = resourceID.toLowerCase();
	var value = this.getResourceByLocale(resourceid, this.locale, false); 
	if(value==null) value = resourceID;
	return value;
}
bb_labs.i18n.prototype.getBinaryResource = function(resourceID)
{
	return this.getResourceByLocale(resourceID, this.locale, true);
}
bb_labs.i18n.prototype.getResourceByLocale = function(resourceID, locale, isBinary)
{
	var response = this.getResourceFromCache(resourceID, locale);
	if(response == null){
		var params = "&locale=" + locale + "&resourceid=" + resourceID;
		response = this.performProxyRequest(params); 
		this.setResourceInCache(resourceID, locale, response);
	}
	return response;
}


//Always perform a request to the server for resource from CMIS
//you need to call cache methods manually in your success handler
bb_labs.i18n.prototype.getResourceAsync = function(resourceID, successCallback, failureCallback)
{
	this.getResourceByLocaleAsync(resourceID, this.locale, false, successCallback, failureCallback);
}
bb_labs.i18n.prototype.getBinaryResourceAsync = function(resourceID, successCallback, failureCallback)
{
	this.getResourceByLocaleAsync(resourceID, this.locale, true, successCallback, failureCallback);
}
bb_labs.i18n.prototype.getResourceByLocaleAsync = function(resourceID, locale, isBinary, successCallback, failureCallback)
{
	var response = this.getResourceFromCache(resourceID, locale);
	if(response == null){
		var params = "&locale=" + locale + "&resourceid=" + resourceID;
		this.performProxyRequest(params, successCallback, failureCallback);
	} else {
		successCallback(response);
	}
}


/* CACHE */
bb_labs.i18n.prototype.setResourceInCache = function(resourceID, locale, resourceValue){
	if(this.cache[resourceID]==null) this.cache[resourceID] = {};
	this.cache[resourceID][locale] = resourceValue;
	bb_labs.setSessionValue("i18n_cache", JSON.stringify(this.cache));
}
bb_labs.i18n.prototype.getResourceFromCache = function(resourceID, locale){
	if(this.cache[resourceID]!=null){
		return this.cache[resourceID][locale];
	} else {
		return null;
	}
}


/* PTC */
bb_labs.i18n.prototype.performProxyRequest = function(params, successCallback, failureCallback){
	var self = this;
	if(typeof(successCallback)=="undefined"){
		var value = null;
		$.ajax({
			url: this.ptc.serverUrl + "/proxy?pipe=" + this.ptc.pipe + params,
			method: 'get',
			dataType: 'json',
			async:false,
			success: function(jsonresults){
				if(jsonresults.state=="success"){
					value = jsonresults.data;
				} else {
					if(self.ptc.showerrors) bb_labs.publishToRadioChannel("error", "i18n", jsonresults.message);
				}	
			},
		    error: function(ajaxErrorObject){
		    	if(self.ptc.showerrors) bb_labs.publishAjaxError(ajaxErrorObject);
		    }
		});	
		
		return value;
	}else {
		var data = $.ajax({
		    url: this.ptc.serverUrl + "/proxy?pipe=" + this.ptc.pipe + params,
		    method:'GET',
		    dataType:'json',
		    async:true,
		    success: successCallback,
		    error: failureCallback
		});	
		return data;
	}
};

$(document).ready(function(){
	bb_labs.i18n = new bb_labs.i18n("resource");	
});
