bb_labs.FormsBasic_ValidationRules = function(){};
bb_labs.FormsBasic_ValidationRules.prototype 		= {};

bb_labs.FormsBasic_ValidationRules.prototype.validate	= function(rule, fieldvalue){
	
	var validated = false;
	
	if(rule=="required"){
		validated = (fieldvalue.trim()!=""); 
	}
	
	if(rule=="number"){
		validated = !isNaN(parseFloat(fieldvalue)) && isFinite(fieldvalue);
	}
	
	if(rule=="date"){
		var pattern=/^\d{2}\/\d{2}\/\d{4}$/ //Basic check for format validity
	    if (!pattern.test(fieldvalue))
            validated=false;
	    else{ //Detailed check for valid date ranges
	        var monthfield=fieldvalue.split("/")[0]
	        var dayfield=fieldvalue.split("/")[1]
	        var yearfield=fieldvalue.split("/")[2]
	        var dayobj = new Date(yearfield, monthfield-1, dayfield)
	        if ((dayobj.getMonth()+1!=monthfield)||(dayobj.getDate()!=dayfield)||(dayobj.getFullYear()!=yearfield))
	            validated=false;
	        else
	        	validated=true;
	    }
	}

	if(rule=="currency"){
		var pattern = /^\d+(?:\.\d{0,2})$/;
		validated = fieldvalue.match(pattern); 
	}

	if(rule=="phonenumber"){
		var pattern = /^(\()?[2-9]{1}\d{2}(\))?(-|\s)?[2-9]{1}\d{2}(-|\s)\d{4}$/;
		validated = fieldvalue.match(pattern); 
	}
	
	if(rule=="zip"){
		var pattern = /(^\d{5}$)|(^\d{5}-\d{4}$)/;
		validated = fieldvalue.match(pattern); 
	}
	
	if(rule=="ssn"){
		var pattern = /^\d{3}-\d{2}-\d{4}$/;
		validated = fieldvalue.match(pattern); 
	}
	
	return validated;
};
