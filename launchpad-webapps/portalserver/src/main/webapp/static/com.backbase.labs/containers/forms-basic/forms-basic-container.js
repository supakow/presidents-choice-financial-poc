(function () {
    var Container = b$.bdom.getNamespace('http://backbase.com/2013/portalView').getClass('container');
    var forms_basic_container = Container.extend(function (bdomDocument, node) {
        Container.apply(this, arguments);
        this.isPossibleDragTarget = true;
        this.validator = new bb_labs.FormsBasic_Validator(this);
    }, {
        localName: 'LabsFormsBasicContainer',
        namespaceURI: 'http://backbase.com/2013/aurora',
        DOMReady: function() {
        	var self = this;

        	//append click events to pagination/breadcrumb
        	if($(".pagination", this.htmlNode).length > 0){
        		var $lis = $(".pagination li", this.htmlNode); 
            	$lis.each(function(liIndex){
            		var $li = $(this);
            		if(liIndex==0){
            			//prev button
            			$li.click(function(){self.previousPage()});
            		} else if(liIndex==$lis.length-1){
            			//next button
            			$li.click(function(){self.nextPage()});
            		} else {
            			//page button
            			$li.click(function(){self.setActivePage(liIndex)});
            		}
            	});
        	}
        	
        	this.setActivePage(1);
        },
        setActivePage: function(idx){
        	
        	//enable the correct pannel
        	//ALSO COUNT PANELS
        	var panelcount = 0;
        	$(".forms-basic-page", this.htmlNode).each(function(panelIndex) {
				var $panel = $(this);
				if((panelIndex+1)==idx){
					$panel.addClass("active");
				} else {
					$panel.removeClass("active");
				}
				panelcount++;
			});        	
        	
			//set pagination / breadcrumb
        	if($(".pagination", this.htmlNode).length > 0){
            	$(".pagination li", this.htmlNode).each(function(liIndex){
    				//liindex 0 is the prev button
            		var $li = $(this);
            		if(liIndex==idx){
    					$li.addClass("active");
    				} else {
    					$li.removeClass("active");
    				}
            	});
            	//set the previous and next
            	if(idx==1){
                	$(".pagination li.previous", this.htmlNode).addClass("disabled");
                } else {
                	$(".pagination li.previous", this.htmlNode).removeClass("disabled");
                }
            	if(idx==panelcount){
                	$(".pagination li.next", this.htmlNode).addClass("disabled");
                } else {
                	$(".pagination li.next", this.htmlNode).removeClass("disabled");
                }
        	}
        	
			//set progress bar
        	if($(".progress", this.htmlNode).length > 0){
        		var $progressbar = $(".progress-bar", this.htmlNode);
        		var percentage = 0;
        		if($progressbar.attr("data-zerobased")=="true"){
            		percentage = Math.round(idx/panelcount*100);
        		} else {
            		percentage = Math.round((idx-1)/panelcount*100);
        		}
        		$progressbar.attr("aria-valuenow", percentage);
        		$progressbar.css("width", percentage + "%");
        		$progressbar.html(percentage + "%");
        	}
        	
        	//Publish the activate event for use by summery widget
        	gadgets.pubsub.publish("FormsBasic:PageActivated", idx);
        },
        nextPage: function(){
        	var panelcounter = 0;
        	var activeidx = 1;
        	$(".forms-basic-page", this.htmlNode).each(function(panelIndex) {
				var $panel = $(this);
				if($panel.hasClass("active")){
					activeidx = panelcounter+1;
				}
				panelcounter++;
			});     
        	
        	var next = activeidx+1;
        	if(next>panelcounter) next = panelcounter; //avoid exceeding on next
        	this.setActivePage(next);
        },
        previousPage: function(){
        	var panelcounter = 0;
        	var activeidx = 1;
        	$(".forms-basic-page", this.htmlNode).each(function(panelIndex) {
				var $panel = $(this);
				if($panel.hasClass("active")){
					activeidx = panelcounter+1;
				}
				panelcounter++;
			});     
        	
        	var prev = activeidx-1;
        	if(prev<1) prev = 1; //avoid exceeding on next
        	this.setActivePage(prev);
        },
        getActivePage: function(){
        	var $activepanel = $(".forms-basic-page.active", this.htmlNode);
        	return $activepanel;
        },
        getActivePageIndex: function(){
        	return this.getActivePage().attr("data-idx");
        },
        getPage:function(pageIndex){
        	var $panel = null;
        	var panelcounter = 1;
        	$(".forms-basic-page", this.htmlNode).each(function(panelIndex) {
        		if(panelcounter == pageIndex){
    				$panel = $(this);
        		}
				panelcounter++;
			});  
        	return $panel;
        },
        getPageCount:function(){
        	var panelcounter = 0;
        	$(".forms-basic-page", this.htmlNode).each(function(panelIndex) {
				panelcounter++;
			});  
        	return panelcounter;
        },
        validator: function(){
        	return this.validator;
        },
        collectFields: function(pageIndex){
        	//collect fields from all containers up to the current page index
        	var jsonresponse = {};
        	var fields = [];

        	for(var i=0; i<=pageIndex;i++){
        		var $page = this.getPage(i);
        		if($page!=null){
        			var $fields = $("*[data-formsField-type]", $page);
        			$fields.each(function(fieldIndex) {
                		var $field = $(this);
                		fields[fields.length] = {
                			"type":$field.attr("data-formsField-type"), 
                			"value":$field.attr("data-formsField-value"),
                			"name":$field.attr("data-formsField-name"),
                			"label":$field.attr("data-formsField-label")
                        };
                	});
        		}
        	}
        	
        	jsonresponse["fields"] = fields;
        	
        	return jsonresponse;
        }
    });
})();
