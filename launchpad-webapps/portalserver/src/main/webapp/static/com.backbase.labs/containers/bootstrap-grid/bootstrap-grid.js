(function () {
    var Container = b$.bdom.getNamespace('http://backbase.com/2013/portalView').getClass('container');
    var bootstrapgrid_container = Container.extend(function (bdomDocument, node) {
        Container.apply(this, arguments);
        this.isPossibleDragTarget = true;
    }, {
        localName: 'LabsBootstrapGridContainer',
        namespaceURI: 'http://backbase.com/2013/aurora',
        DOMReady: function() {
        }
    });
})();
