bb_labs.FormsBasic_Validator = function(oContainerController){
	this.containerController = oContainerController;
	this.rules = new bb_labs.FormsBasic_ValidationRules();
	this.pages_widgetcontrols = new Array();
};
bb_labs.FormsBasic_Validator.prototype 		= {};

bb_labs.FormsBasic_Validator.prototype.register	= function(formsPageIndex, widgetController){
	var pagearray = this.pages_widgetcontrols[formsPageIndex];
	if(typeof(pagearray)=="undefined"){
		var pagearray = this.pages_widgetcontrols[formsPageIndex] = new Array();
	}
	pagearray[pagearray.length] = widgetController;
};

bb_labs.FormsBasic_Validator.prototype.rules	= function(){
	return this.rules();
};

bb_labs.FormsBasic_Validator.prototype.clientValidation	= function(rule, value){
	return this.rules.validate(rule, value);
};

bb_labs.FormsBasic_Validator.prototype.serverValidation	= function(url, value){
	var proxyurl = url + "&value=" + value;
	var returnvalue = "";
	$.ajax({
		url: proxyurl,
		method: 'get',
		dataType: 'html',
		async:false,
		success: function(proxycontent){
			try{
				var jsonresults = $.parseJSON(proxycontent);
				if(jsonresults.state=="success"){
					returnvalue = "";
				} else {
					returnvalue = jsonresults.message;
				}	
			} catch (exception){
				returnvalue = "Validation Exception";
				bb_labs.publishToRadioChannel("error", "Forms Basic", proxycontent);
			}
		},
	    error: function(ajaxErrorObject){
	    	try{
	    		var jsonresults = $.parseJSON(ajaxErrorObject.responseText);
	    		returnvalue = jsonresults.message;
	    	} catch (exception){
    			returnvalue = "Validation Exception";
    			bb_labs.publishToRadioChannel("error", "Forms Basic", proxycontent);
    		} 
	    }
	});
	return returnvalue;
};

bb_labs.FormsBasic_Validator.prototype.validateActivePage = function(){
	//loop through all widgets on current page and validate
	var validated = true;
	var currentpageindex = this.containerController.getActivePageIndex();
	var pagearray = this.pages_widgetcontrols[currentpageindex];
	if(typeof(pagearray)!="undefined"){
		for(var i=0; i<pagearray.length; i++){
			var widgetcontroller = pagearray[i];
			var currentvalidated = widgetcontroller.validate(); 
			if(!currentvalidated) validated = false;
		}
	}
	
	return validated;
};
