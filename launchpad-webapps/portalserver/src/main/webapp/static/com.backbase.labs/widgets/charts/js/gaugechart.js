bb_labs.GaugeChart 					= function(oWidget){
	this.widget = oWidget;
	this.init();
	this.i18n();
};
bb_labs.GaugeChart.prototype 		= {};
bb_labs.GaugeChart.prototype.init	= function(){

	var self = this;

	var $widget = $(this.widget.htmlNode);
	
	var size = this.widget.getPreference("size");
	var	trackcolor = this.widget.getPreference("trackcolor");
	var	scalecolor = this.widget.getPreference("scalecolor");
	var	linewidth = this.widget.getPreference("linewidth");

	var $piechart = $(".piechart", this.widget.body);
	var $percent = $(".percent", this.widget.body);
	var $title = $(".title", this.widget.body);
	$title.css("color", this.widget.getPreference("percentagecolor"));

	//pie charts is always square, thus width/height same
	$piechart.width(size);
	$piechart.height(size);
		
	$percent.css("color", this.widget.getPreference("percentagecolor"));
	
	$piechart.easyPieChart({
		easing: "easeOutBounce",
		size: size,	
		barColor:function(percent) {
			return "rgb(" + Math.round(200 * percent / 100) + ", " + Math.round(200 * (1 - percent / 100)) + ", 0)";
		},
		trackColor: trackcolor,
		scaleColor: scalecolor,
		lineWidth: linewidth,
		onStep: function(from, to, percent) {
			$(this.el).find(".percent").text(Math.round(percent));
		}
	});
	
	bb_labs.GaugeChart.prototype.setdata($piechart, this.widget);
	
};
bb_labs.GaugeChart.prototype.setdata =function($piechart, widget){ 
	var proxyurl = widget.getPreference("proxy");

	$.ajax({
		url: proxyurl,
		method: 'get',
		dataType: 'html',
		async:true,
		success: function(proxycontent){
			try{
				var jsonresults = $.parseJSON(proxycontent);
				if(jsonresults.state=="success"){
					$piechart.data('easyPieChart').update(jsonresults.data.cpu); 
					setTimeout(function(){bb_labs.GaugeChart.prototype.setdata($piechart, widget, Math.random()*200-100)},3000);
				} else {
					bb_labs.publishToRadioChannel("error", "Gauge", jsonresults.message);
				}	
			} catch (exception){
				bb_labs.publishToRadioChannel("error", "Gauge", proxycontent);
			}
		},
	    error: function(ajaxErrorObject){
	    	bb_labs.publishAjaxError(ajaxErrorObject);
	    }
	});
	
	
};
bb_labs.GaugeChart.prototype.i18n	= function(){
	var self = this;

	var $title = $(".title", this.widget.body);
	
	var titleresourceid = this.widget.getPreference("title");
	titleresourceid = bb_labs.replaceAll(titleresourceid, " ", "");
	$title.html(bb_labs.i18n.getResourceFailSafe(titleresourceid));
};

