bb_labs.FormsBasic_Utilities = function(oWidget){
	this.widget = oWidget;
};
bb_labs.FormsBasic_Utilities.prototype 		= {};


bb_labs.FormsBasic_Utilities.prototype.getBindingData	= function(bindingurl){
	var returnvalue = "";
	$.ajax({
		url: bindingurl,
		method: 'get',
		dataType: 'html',
		async:false,
		success: function(proxycontent){
			try{
				var jsonresults = $.parseJSON(proxycontent);
				if(jsonresults.state=="success"){
					returnvalue = jsonresults.data;
				} else {
					returnvalue = jsonresults.message;
					bb_labs.publishToRadioChannel("error", "Forms Basic", jsonresults.message);
				}	
			} catch (exception){
				bb_labs.publishToRadioChannel("error", "Forms Basic", proxycontent);
			}
		},
	    error: function(ajaxErrorObject){
	    	bb_labs.publishAjaxError(ajaxErrorObject);
	    }
	});
	return returnvalue;
};


bb_labs.FormsBasic_Utilities.prototype.getContainer	= function(){
	var bbcontainer = null;
	var $currentpage = this.getCurrentPage();
	if($currentpage!=null){
		var $containerdiv = $currentpage.parent();
		if($containerdiv.length>0){
			bbcontainer = $containerdiv[0].viewController;
		}
	} else {
		console.log("Forms Basic: Unable to locate Forms Page");
	}
	return bbcontainer;
};

bb_labs.FormsBasic_Utilities.prototype.getCurrentPageIndex	= function(){
	var index = 1;
	var $currentpage = this.getCurrentPage();
	if($currentpage!=null){
		index = $currentpage.attr("data-idx");
	} else {
		console.log("Forms Basic: Unable to locate Forms Page");
	}
	return index;
};

bb_labs.FormsBasic_Utilities.prototype.getCurrentPage	= function(){
	var $parentpage = null;
	var searchforparent = true;
	var $currentelement = $(this.widget.body);
	while(searchforparent) {
		$currentelement = $currentelement.parent();
		if($currentelement.hasClass("forms-basic-page")){
			$parentpage = $currentelement;
			searchforparent = false;
		} 
		
		//stop the loop if we reach body without finding panel
		if($currentelement.is("body")){
			$parentpage = null;
			searchforparent = false;
		}
	}
	return $parentpage;
};