bb_labs.TaskList = function(oWidget){
	this.widget = oWidget;
	this.init();
};

bb_labs.TaskList.prototype 		= {};
bb_labs.TaskList.prototype.init	= function(){
	var self = this;

	$(self.widget.body).find('.datatable').dataTable({
		"sDom": "<'row'<'col-lg-6'l><'col-lg-6'f>r>t<'row'<'col-lg-12'i><'col-lg-12 center'p>>",
		"bPaginate": false,
		"bFilter": false,
		"bLengthChange": false,
		"bInfo": false
	});

	/* ---------- Progress Bars ---------- */
	$(self.widget.body).find(".simpleProgress").each(function(){
		
		var value = parseInt($(this).html());
				
		$(this).progressbar({
			value: value
		});
		
	});

};