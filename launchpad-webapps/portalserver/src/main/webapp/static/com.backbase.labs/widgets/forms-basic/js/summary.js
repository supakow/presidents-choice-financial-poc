bb_labs.FormsBasic_Summary = function(oWidget){
	this.widget = oWidget;
	this.utils = new bb_labs.FormsBasic_Utilities(oWidget);
	this.containercontroller = null;
	
	this.init();
};
bb_labs.FormsBasic_Summary.prototype 		= {};
bb_labs.FormsBasic_Summary.prototype.init	= function(){
	var self = this;

	//user time out to give DOM time to catch up
	setTimeout(function(){
		self.containercontroller = self.utils.getContainer();
		var field_mustache_template = $("script[data-template='summary-field']", self.widget.body).html();
		var $field_container = $(".bb-labs-forms-basic-summary",self.widget.body);

		if(self.containercontroller.getActivePageIndex() == self.utils.getCurrentPageIndex()){
			var fields = self.containercontroller.collectFields(pageIndex);
			var field_html = Mustache.to_html(field_mustache_template, fields);
			$field_container.html(field_html);
		}
		
		gadgets.pubsub.subscribe("FormsBasic:PageActivated", function(pageIndex){
			if(pageIndex == self.utils.getCurrentPageIndex()){
				var fields = self.containercontroller.collectFields(pageIndex);
				var field_html = Mustache.to_html(field_mustache_template, fields);
				$field_container.html(field_html);
			}
		})
		
	},1000);
	
	
	
};
