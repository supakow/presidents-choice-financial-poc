if (!window.bb_labs) window.bb_labs = {};

bb_labs.ExampleWidget = (function() {
	var onload = function(oGadget) {
		init(oGadget);

		//What to do in designmode/portal manager
		if(bd!=null){
			if (bd.designMode == 'true') {
			}
		}
	};
	
	
	var init = function(oGadget) { 

		//var property = oGadget.model.getPreference('property');

		var div = jQuery('.div', oGadget.body);
		var results = performProxyRequest("");
		div.html(results);

		//gadgets.pubsub.subscribe("queuename", function(msg){
		//});
		
	};

	
	var performProxyRequest = function(queryString) {
		var proxycontent = jQuery.ajax({
		    //url: "/portalserver/proxy?pipe=examplePipe&url=about:blank" + queryString,
		    url: "/portalserver/proxy?pipe=examplePipeTransformed&url=about:blank" + queryString,
		    method:'GET',
		    dataType:'html',
		    async:false});
		return proxycontent.responseText;
	}
	
	var handleError = function(oGadget, errorSource, errorMessage){
		var widgetbody = jQuery(oGadget.body);
		widgetbody.html(errorMessage);
	}
	
	//public functions
	return {
		onload: onload
	};

}());