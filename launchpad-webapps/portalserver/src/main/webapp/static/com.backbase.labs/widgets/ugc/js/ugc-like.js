bb_labs.UGCLike = function(oWidget){
	this.widget = oWidget;
	this.reference = "";
	this.init();
	this.i18n();
};
bb_labs.UGCLike.prototype 		= {};
bb_labs.UGCLike.prototype.init	= function(){
	
	var self = this;

	this.reference = this.widget.getPreference("referenceid");
	if((this.reference=="")||(typeof(this.widget.getPreference("referenceid"))=="undefined")) this.reference = b$.portal.pageName;

	$(".comment", self.widget.body).click(function(){
		gadgets.pubsub.publish("ugc_comments_focus", self.widget.id);
	});

	self.showlike();
	
	var $like = $(".like", self.widget.body);
	$like.click(function(){
		var currentvalue = $like.attr("data-liked")=="true"?true:false; 
		self.like(!currentvalue);
	});
	
};

bb_labs.UGCLike.prototype.like = function(liked){
	var self = this;
	
	$.ajax({
		url: b$.portal.portalModel.serverURL + "/proxy?pipe=ugcPipe&widgetid=" + self.widget.id + "&portalname=" + b$.portal.portalName + "&reference=" + this.reference + "&type=like",
		type: 'post',
		data: "ugc_data=" + liked,
		dataType: 'json',
		async:true,
		success: function(jsonresults){
			if(jsonresults.state=="success"){
				var $like = $(".like", self.widget.body);
				$like.attr("data-liked", liked);
				self.showlike(liked);
			} else {
				bb_labs.publishToRadioChannel("error", "UGC", jsonresults.message);
			}	

		},
	    error: function(ajaxErrorObject){
	    	bb_labs.publishAjaxError(ajaxErrorObject);
	    }
	});

};

bb_labs.UGCLike.prototype.showlike	= function(liked){
	var self = this;
	

	if(typeof(liked)=="undefined"){
		$.ajax({
			url: b$.portal.portalModel.serverURL + "/proxy?pipe=ugcPipe&widgetid=" + self.widget.id + "&portalname=" + b$.portal.portalName + "&reference=" + this.reference + "&type=like&id=like",
			type: 'get',
			dataType: 'json',
			async:true,
			success: function(jsonresults){
				if(jsonresults.state=="success"){
					var $like = $(".like", self.widget.body);
					$like.attr("data-liked", jsonresults.data.content);
					self.i18n();
				} else {
					bb_labs.publishToRadioChannel("error", "UGC", jsonresults.message);
				}	

			},
		    error: function(ajaxErrorObject){
		    	bb_labs.publishAjaxError(ajaxErrorObject);
		    }
		});
	} else {
		self.i18n();
	}
};


bb_labs.UGCLike.prototype.i18n	= function(){
	var self = this;
	
	var $like = $(".like", self.widget.body);
	var liked = $like.attr("data-liked")=="true"?true:false; 
	
	if(liked){
		$like.html(bb_labs.i18n.getResourceFailSafe("unlike"));
	} else {
		$like.html(bb_labs.i18n.getResourceFailSafe("like"));
	}
	
};

