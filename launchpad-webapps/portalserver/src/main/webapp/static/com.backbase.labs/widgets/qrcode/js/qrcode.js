bb_labs.QRCode 				= function(oWidget){
	this.widget = oWidget;
	this.init();
};
bb_labs.QRCode.prototype 		= {};
bb_labs.QRCode.prototype.init	= function(){
	
	var self = this;
	
    $(".qrcode", this.widget.body).kendoQRCode({
        value: self.widget.getPreference("qrtext"),
        errorCorrection: self.widget.getPreference("errorcorrection"),
        size: self.widget.getPreference("size"),
        color: self.widget.getPreference("modulecolor"),
        background: self.widget.getPreference("bgcolor"),
        encoding: self.widget.getPreference("encoding")
    });
  
	var $widget = $(this.widget.htmlNode);
	$widget.css("max-width", $(".qrcode", this.widget.body).width());
};

