bb_labs.FormsBasic_Button = function(oWidget){
	this.widget = oWidget;
	this.utils = new bb_labs.FormsBasic_Utilities(oWidget);
	this.init();
};
bb_labs.FormsBasic_Button.prototype 		= {};
bb_labs.FormsBasic_Button.prototype.init	= function(){
	var self = this;
	
	var button_type = this.widget.getPreference("buttontype");
	var button_text = this.widget.getPreference("buttontext");
	var cssclass = this.widget.getPreference("cssclass");
	var actionurl = this.widget.getPreference("actionurl");
	var successpageindx = this.widget.getPreference("successpage");
	var width = this.widget.getPreference("width");
	var alignment = this.widget.getPreference("align");

	if(button_text==""){
		button_text = button_type;
	}
	
	var $buttondiv = $(".bb-labs-forms-basic-button", this.widget.body);
	$buttondiv.addClass("text-" + alignment);
	
	var $button = $("button", this.widget.body);
	$button.addClass(cssclass);
	$button.html(button_text);
	
	if(width!=""){
		$button.width(width);
	}
	
	$button.click(function(){
		var bbcontainer = self.utils.getContainer();
		
		if(bbcontainer!=null){

			if(bbcontainer.validator.validateActivePage()){
				if(button_type=="next"){
					bbcontainer.nextPage();
				}
				if(button_type=="previous"){
					bbcontainer.previousPage();
				}
				if(button_type=="submit"){
					var currentpageindex = self.utils.getCurrentPageIndex();
					var fields = bbcontainer.collectFields(currentpageindex);
					var submitdata = {};
					for(var i=0; i<fields.fields.length;i++){
						var field = fields.fields[i];
						submitdata[field.name] = field.value;
					}
					
					$.ajax({
						url: actionurl,
						method: 'post',
						dataType: 'json',
						async:false,
						data:submitdata,
						success: function(proxycontent){
							try{
								var jsonresults = proxycontent;
								if(jsonresults.state=="success"){
									
									if((successpageindx=="")||(successpageindx=="0")){
										if(currentpageindex < bbcontainer.getPageCount()){
											bbcontainer.nextPage();
										} else {
											bb_labs.publishToRadioChannel("success", "Forms Basic", "Successfully commited data. " + jsonresults.message);
										}
									} else {
										bbcontainer.setActivePage(successpageindx);
									}

									
								} else {
									bb_labs.publishToRadioChannel("error", "Forms Basic", jsonresults.message);
								}	
							} catch (exception){
								bb_labs.publishToRadioChannel("error", "Forms Basic", proxycontent);
							}
						},
					    error: function(ajaxErrorObject){
					    	bb_labs.publishAjaxError(ajaxErrorObject);
					    }
					});										
				}
			}
			
		} else {
			console.log("Forms Basic: Unable to locate Forms Container");
		}
	});

};
