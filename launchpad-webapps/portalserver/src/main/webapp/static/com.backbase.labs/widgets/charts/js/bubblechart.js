bb_labs.BubbleChart 					= function(oWidget){
	this.widget = oWidget;
	this.init();
	this.i18n();
};
bb_labs.BubbleChart.prototype 		= {};
bb_labs.BubbleChart.prototype.init	= function(){
	var self = this;

	var $container = $(".bb-labs-bubble-chart", self.widget.body);
	$container.css("height", self.widget.getPreference("height"));
	$container.css("max-width", self.widget.getPreference("maxwidth"));
	$container.css("min-width", self.widget.getPreference("minwidth"));
	
	self.drawChart();
}

bb_labs.BubbleChart.prototype.drawChart	= function(){
	var self = this;
	
	var titleresourceid = this.widget.getPreference("title");
	titleresourceid = bb_labs.replaceAll(titleresourceid, " ", "");
	var ytitleresourceid = this.widget.getPreference("ytitle");
	ytitleresourceid = bb_labs.replaceAll(ytitleresourceid, " ", "");
	
	var $container = $(".bb-labs-bubble-chart", self.widget.body);
	$.getJSON(self.widget.getPreference("proxy"), function(data) {
		$container.each(function(){
			var options = {
			       chart: {
			       		renderTo: this,
			       		type: 'bubble',
			       		zoomType: 'xy',
			       		spacingTop:0
				   },

				   credits: {
						enabled: false
					},

			       legend: {
					   enabled: false
					},

				   title: {
					   	text: bb_labs.i18n.getResourceFailSafe(titleresourceid)
				   },
			       
			       xAxis: {
			           type: 'datetime',
                        labels: {
                        	y: 20
                        },
                        minTickInterval: 86400000,
			           dateTimeLabelFormats: {
			               day: '%d/%m/%y'
			           }
			       },

			       yAxis: {
			           title: {
			               text:  bb_labs.i18n.getResourceFailSafe(ytitleresourceid)
			           }
			       },

			       tooltip: {
					   formatter: function() {
					       return '<b>' + this.series.name +'</b><br/>' +  bb_labs.i18n.getResourceFailSafe(ytitleresourceid) + ': ' + this.point.y + '<br/>Quantity: ' + this.point.z;
					   }
					},
				
				   series: data
		   };

	       var chart = new Highcharts.Chart(options);
			
		});
    }).fail( function(){
    	bb_labs.publishToRadioChannel("error", "Error drawing bubble chart", "Verify the configuration of this widget.");
    }).done( function(){
    }).always( function(){
    });
};

bb_labs.BubbleChart.prototype.i18n	= function(){
	var self = this;
	self.drawChart();
};
