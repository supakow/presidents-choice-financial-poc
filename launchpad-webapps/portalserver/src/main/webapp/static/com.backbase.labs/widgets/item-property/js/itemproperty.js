bb_labs.ItemProperty = function(oWidget){
	this.widget = oWidget;
	this.init();
	this.i18n();
};
bb_labs.ItemProperty.prototype 		= {};
bb_labs.ItemProperty.prototype.init	= function(){
	var self = this;
};

bb_labs.ItemProperty.prototype.i18n	= function(){
	var self = this;

	var item_type = this.widget.getPreference("itemtype");
	var preference = this.widget.getPreference("preference");
	var cssclass = this.widget.getPreference("cssclass");
	var padding = this.widget.getPreference("padding");
	
	var $property = $(".bb-labs-itemproperty", self.widget.body);
	
	if(cssclass!="") $property.addClass(cssclass);
	if((padding!="")&&(padding!="0")) $property.css("padding", padding);
	
	try{
		var preferencevalue = "";
		if(item_type=="widget"){
			preferencevalue = self.widget.getPreference(preference);
		} else if(item_type=="page") {
			var currentpagename = b$.portal.getCurrentPage().name;
			preferencevalue = b$.portal.portalModel.all[currentpagename].getPreference(preference);
		} else if(item_type=="portal"){
			if(preference=="title"){
				preferencevalue = bb_labs.replaceAll(b$.portal.portalName,"-"," "); 
			}
		}
		
		var resourceid = bb_labs.replaceAll(preferencevalue, " ", "");
		preferencevalue = bb_labs.i18n.getResourceFailSafe(resourceid);
		
		$property.html(preferencevalue);
	} catch(Exception){
		console.log("unable to get preference " + preference + " of " + item_type);
	}

};
