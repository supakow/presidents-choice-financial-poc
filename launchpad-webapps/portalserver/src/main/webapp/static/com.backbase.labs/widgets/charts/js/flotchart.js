bb_labs.FlotChart = function(oWidget){
	this.widget = oWidget;
	this.init();
};
bb_labs.FlotChart.prototype 		= {};
bb_labs.FlotChart.prototype.init	= function(){
	var self = this;
	setTimeout(function(){self.buildCharts();},200);
};

bb_labs.FlotChart.prototype.buildCharts	= function(){

	var self 	= this;
	var data1 	= [[0, 10], [1, 13], [2, 9], [3, 12],[4, 15], [5, 14], [6, 13], [7, 13],[8, 14], [9, 15], [10, 15], [11, 16],[12, 16], [13, 15], [14, 15], [15, 14]];
	var data2 	= [[0, 1], [1, 2], [2, 3], [3, 4],[4, 5], [5, 4], [6, 3], [7, 3],[8, 4], [9, 5], [10, 5], [11, 6],[12, 6], [13, 5], [14, 5], [15, 4]];
	
	
	$(self.widget.body).find(".backgroundColor").addClass(self.widget.getPreference("bgColor"));
	var oTarget = $(self.widget.body).find('.chart-info-box');
	
	$.plot(oTarget,
		[ { data: data1, 
			label: "Visits", 
			lines: { 
				show: true, 
				lineWidth: 2,
				color: "#fff" 
			},
			points: { 
				show: false
			},
			shadowSize: 0	
		  }, {
			data: data2,
			bars: { 
				show: true,
				fill: false, 
				barWidth: 0.1, 
				align: "center",
				lineWidth: 2
			}
		  }
		], {

			grid: {
				show: false
			},
			legend: {
				show: false
			},	
			colors: ["#fff", "rgba(0,0,0,0.1)"],
			xaxis: {ticks:2, tickDecimals: 0 },
			yaxis: {ticks:2, tickDecimals: 0 },
		}
	);

};