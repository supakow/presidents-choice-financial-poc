if (!window.be) window.be = {};
if (!window.bd) window.bd = {};

be.SharePointWidget = (function() {
	var onload = function(oGadget) {
		init(oGadget);
		if (bd.designMode == 'true') {
		}
	};
	
	
	var init = function(oGadget) { 

		var action = oGadget.model.getPreference('action');
		
		if(action == "getlists"){
			var querystring = "&action=getlists";
			var jsonresponse = performProxyRequest(querystring); 
			if(jsonresponse.success){
				var jsonlists = jsonresponse.items;
				buildlists(oGadget, jsonlists);
			} else {
				handleError(oGadget, "getlists", jsonresponse.errormessage);
			}
			
		} else if(action == "getlist"){
			var listname = oGadget.model.getPreference("listname");
			//if listname = "", then listern to pubsub
			//else publish this as the current active list
			if(listname!=""){
				gadgets.pubsub.publish("sharepoint_activelist", listname);			
			}

			gadgets.pubsub.subscribe("sharepoint_activelist", function(msg){
				var querystring = "&action=getlist&listname=" + msg;
				buildlistitems(oGadget, performProxyRequest(querystring));
			});
			
		} else if(action == "getlistitems"){
			var listname = oGadget.model.getPreference("listname");
			//if listname = "", then listern to pubsub
			//else publish this as the current active list
			if(listname!=""){
				gadgets.pubsub.publish("sharepoint_activelist", listname);			
			}

			gadgets.pubsub.subscribe("sharepoint_activelist", function(msg){
				var querystring = "&action=getlistitems&listname=" + msg;
				buildlistitems(oGadget, performProxyRequest(querystring));
			});
		}
		
	};

	var buildlistitems = function(oGadget, jsonListItems) {
		var sharepointlistdiv = jQuery('.sharepoint_list', oGadget.body);
		sharepointlistdiv.html(jsonListItems);
	}
	
	var buildlists = function(oGadget, jsonLists){
		var sharepointlistdiv = jQuery('.sharepoint_list', oGadget.body);
		var html = "";
		
		jQuery.each(jsonLists, function(index, obj){
			html += "<a href=\"javascript:gadgets.pubsub.publish('sharepoint_activelist', '" + obj.name + "');\">" + obj.description + "</a>";
			html += "<br/>";
		});		

		sharepointlistdiv.html(html);
	}
	
	
	var performProxyRequest = function(queryString) {
		var proxycontent = jQuery.ajax({
		    url: "/portalserver/proxy?pipe=sharepointJSONPipe&url=about:blank" + queryString,
		    method:'POST',
		    dataType:'json',
		    async:false});
		return jQuery.parseJSON(proxycontent.responseText);
	}
	
	var handleError = function(oGadget, errorSource, errorMessage){
		var widgetbody = jQuery(oGadget.body);
		widgetbody.html(errorMessage);
	}
	
	//public functions
	return {
		onload: onload
	};

}());