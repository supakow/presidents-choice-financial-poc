bb_labs.MD 			= function(oWidget){
	this.widget = oWidget;
	this.init();
	this.i18n();
};
bb_labs.MD.prototype 		= {};
bb_labs.MD.prototype.init	= function(){
	
	var self = this;

	var minheight = self.widget.getPreference("minheight");
	if(minheight.indexOf("px")==-1) minheight += "px";
	
	var $widget = $(self.widget.htmlNode);
	$widget.css("min-height", minheight);
	
	var widgetname = self.widget.getPreference("mdwidgetName");
	
	$(".bb-labs-md-loading", self.widget.body).addClass("active");
	
	$.ajax({
		url: "/portalserver/proxy?pipe=mdssowidget&widgetname=" + widgetname,
		method: 'get',
		dataType: 'json',
		async:true,
		success: function(proxycontent){
			if(proxycontent.state=="success"){
				var $frame = $(".bb-labs-md-frame", self.widget.body);
				if($frame.length==0){
					$("<iframe />", {
					    src: proxycontent.data.url,
					    class: "bb-labs-md-frame",
					    style: "min-height:" + minheight
					}).appendTo($(self.widget.body));					
				} else {
					$frame.attr("src", proxycontent.data.url);
					$frame.css("min-height", minheight);
				}
			}
			 $(".bb-labs-md-loading", self.widget.body).removeClass("active");
		},
	    error: function(ajaxErrorObject){
			 $(".bb-labs-md-loading", self.widget.body).removeClass("active");
	    }
	});
	
};
bb_labs.MD.prototype.i18n	= function(){
	//bb_labs.i18n.getResourceFailSafe("resourceid");
};

