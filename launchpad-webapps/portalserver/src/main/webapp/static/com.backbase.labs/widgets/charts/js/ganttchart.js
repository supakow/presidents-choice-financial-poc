bb_labs.GanttChart 					= function(oWidget){
	this.widget = oWidget;
	this.data = null;
	this.init();
	this.i18n();
};
bb_labs.GanttChart.prototype 		= {};
bb_labs.GanttChart.prototype.init	= function(){
	var self = this;

	var $widget = $(self.widget.htmlNode);
	$(self.widget.body).css("overflow", "hidden");
	
	var $container = $(".bb-labs-gantt-chart", self.widget.body);
	var url = self.widget.getPreference("proxy");

	$.ajax({
		url: url,
		type: 'get',
		dataType: 'json',
		async:true,
		success: function(data){
			self.data = data;
			self.renderChart(data, self.widget.getPreference("itemsperpage"));
		},
	    error: function(ajaxErrorObject){
	    	bb_labs.publishAjaxError(ajaxErrorObject);
	    }
	});
};

bb_labs.GanttChart.prototype.renderChart = function(data, itemsPerPage){
	var self = this;
	
	var $container = $(".bb-labs-gantt-chart", self.widget.body);
	
	$container.gantt({
		source: data,
		navigate: "scroll",
		scrollToToday: self.widget.getPreference("scrolltotoday"),
		maxScale: "hours",
		itemsPerPage: itemsPerPage,
		onItemClick: function(data) {
			alert("Item clicked - show some details");
		},
		onAddClick: function(dt, rowId) {
			alert("Empty space clicked - add an item!");
		},
		onRender: function() {
			if (window.console && typeof console.log === "function") {
				console.log("chart rendered");
			}
		}
	});

	$container.popover({
		selector: ".bar",
		title: "I'm a popover",
		content: "And I'm the content of said popover.",
		trigger: "hover"
	});			

};

bb_labs.GanttChart.prototype.maximize	= function(){
	var self = this;
	var $container = $(".bb-labs-gantt-chart", self.widget.body);
	$container.addClass("maximized");
	self.renderChart(self.data, (self.widget.getPreference("itemsperpage")*2));
	$("html, body").animate({ scrollTop: 0 }, "slow");
};

bb_labs.GanttChart.prototype.restore	= function(){
	var self = this;
	var $container = $(".bb-labs-gantt-chart", self.widget.body);
	$container.removeClass("maximized");
	self.renderChart(self.data, self.widget.getPreference("itemsperpage"));
};

bb_labs.GanttChart.prototype.i18n	= function(){
	var self = this;
};
