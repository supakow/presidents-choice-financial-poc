bb_labs.PubSubRadioTest 				= function(oWidget){
	this.widget = oWidget;
	this.init();
};
bb_labs.PubSubRadioTest.prototype 		= {};
bb_labs.PubSubRadioTest.prototype.init	= function(){
	var self = this;
	var widget_channel = this.widget.getPreference("channel");
	var widget_message = this.widget.getPreference("message");
	
	//Set the designmode message
	var $designmode_message = $(".designmode-message", this.widget.body);
	var html = $designmode_message.html();
	html = html.replace("[*channel*]", widget_channel);
	$designmode_message.html(html);
	
	
	//Subscribe to channel
	gadgets.pubsub.publish(widget_channel, widget_message);
	
};