bb_labs.FormsBasic_List = function(oWidget){
	this.widget = oWidget;
	this.utils = new bb_labs.FormsBasic_Utilities(oWidget);
	this.list_type = "select";
	this.list_name = this.widget.id + "_" + this.list_type;
	this.$list = null; 
	this.containercontroller = null;
	
	this.init();
};
bb_labs.FormsBasic_List.prototype 		= {};
bb_labs.FormsBasic_List.prototype.init	= function(){
	var self = this;

	this.list_type = this.widget.getPreference("listtype");
	if(this.widget.getPreference("listname")!=""){
		this.list_name = this.widget.getPreference("listname");
	}

	
	var required = this.widget.getPreference("required");
	var label = this.widget.getPreference("label");
	var hint = this.widget.getPreference("hint");
	var cssclass = this.widget.getPreference("cssclass");
	var style = this.widget.getPreference("style");
	var height = this.widget.getPreference("height");
	var defaultvalue = this.widget.getPreference("defaultvalue");
	var defaultbindingurl = this.widget.getPreference("defaultbindingurl");
	var listvalues = this.widget.getPreference("listvalues");
	var databindingurl = this.widget.getPreference("databindingurl");

	
	if(listvalues!=null){
		try{
			//we can't use " in the preferences, so we use ', and replace below
			listvalues = bb_labs.replaceAll(listvalues, "'", "\"");
			listvalues = $.parseJSON(listvalues);
		} catch(exception){
			listvalues = [];
		}
	}
	
	
	//get the data from the server
	if(databindingurl!=""){
		listvalues = this.utils.getBindingData(databindingurl);
	}
	
	
	//Render the list
	var list_jsonobject = {
			listname: this.list_name,
			label:label,
			hint:hint,
			required:required,
			values:listvalues
	};
	var list_mustache_template = $("script[data-template='" + this.list_type + "-list']", this.widget.body).html();
	var list_html = Mustache.to_html(list_mustache_template, list_jsonobject);

	//Add list to widget body
	var $listcontainer = $(".bb-labs-forms-basic-list", this.widget.body);
	$listcontainer.addClass("form-" + style);
	if(cssclass!="") $listcontainer.addClass(cssclass); 
	$listcontainer.html(list_html);

	
	//get the data from the server
	if(defaultbindingurl!=""){
		defaultvalue = this.utils.getBindingData(defaultbindingurl);
	}

	//set the defaults and change events
	this.$list = $("#" + this.list_name, $listcontainer);
	this.$list.val(defaultvalue);
	self.$list.attr("data-formsField-value", self.$list.val());
	if(height!="") self.$list.css("height", height);
	
	//Assocate change event to list items
	var $datatriggerelements = $("*[data-trigger='true']",$listcontainer);
	$datatriggerelements.each(function(elementIndex){
		$(this).change(function(){
			self.$list.attr("data-formsField-value", $(this).val());
			self.validate();
		});
	});
	
	
	//user time out to give DOM time to catch up
	setTimeout(function(){
		//Using this validator require a ".validate()" callback to be implemented
		self.containercontroller = self.utils.getContainer();
		self.containercontroller.validator.register(self.utils.getCurrentPageIndex(), self);
	},1000);

};


//This validate function is required if you register the widget for form validation
bb_labs.FormsBasic_List.prototype.validate	= function(){
	var validated = true;
	var required =  (this.widget.getPreference("required")=="true");
	var datavalidationurl = this.widget.getPreference("datavalidationurl");
	var listvalue = this.$list.attr("data-formsField-value");
	var errormessage = "";
	
	if(required){
		validated = this.containercontroller.validator.clientValidation("required", listvalue);
	}
		
	if((datavalidationurl!="")&&(validated)){
		var servermessage = this.containercontroller.validator.serverValidation(datavalidationurl, listvalue);
		if(servermessage!=""){
			validated = false;
			errormessage += " [" + servermessage + "]"; 
		}
	}

	if((!validated)&&(!required)&&(fieldvalue.trim()=="")){
		validated = true;
	}
	
	var $fieldcontainer = $(".bb-labs-forms-basic-list", this.widget.body);
	if(!validated){
		$fieldcontainer.addClass("has-error");
		$(".error",$fieldcontainer).html(errormessage);
	} else {
		$fieldcontainer.removeClass("has-error");
		$(".error",$fieldcontainer).html("");
	}
	
	return validated;
};