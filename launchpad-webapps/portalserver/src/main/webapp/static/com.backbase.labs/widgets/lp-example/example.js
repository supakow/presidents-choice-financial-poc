/*global bd */

/**
 * Launchpad Example widget
 * @author hendrik@backbase.com
 */
define(["jquery"], function ($) {

    "use strict";

    var Example = function (widget) {
        this.widget = widget;
        if (!bd.designMode) {
        }
    };

    Example.prototype.init = function () {

        var self = this;
        var widget = this.widget;
        var $widget = $(this.widget.body);

        if (!bd.designMode) {
        	
        	//add your code here that should happen for customer
        	/*
        	 * YOUR CODE HERE
        	 */
        	
        } else {
       
        	//add your code here that should happen for CXP Manager
        	/*
        	 * YOUR CODE HERE
        	 */
        }

    };

    
    return function(widget) {
        var widgetWrapper = new Example(widget);
        widgetWrapper.init();
        widget.addEventListener("preferencesSaved", function () {
            widgetWrapper.init();
        });
        return widgetWrapper;
    };
});