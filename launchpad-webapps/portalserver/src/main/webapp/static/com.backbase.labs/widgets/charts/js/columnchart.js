bb_labs.ColumnChart 					= function(oWidget){
	this.widget = oWidget;
	this.init();
	this.i18n();
};
bb_labs.ColumnChart.prototype 		= {};
bb_labs.ColumnChart.prototype.init	= function(){
	var self = this;
	var title = self.widget.getPreference("title"); //defines the chart title
	var color = self.widget.getPreference("color"); //defines the chart color

	//Move the widget to the bottom of the container
	var $container = $(this.widget.htmlNode.parentNode);
	var $widget = $(this.widget.htmlNode);
	var $columncontainer = $(".column-container", this.widget.body);

	//$columncontainer.css("max-width", self.widget.getPreference("maxwidth"));
	//$columncontainer.css("min-width", self.widget.getPreference("minwidth"));
	//$columncontainer.css("height", self.widget.getPreference("height"));

	$container.css("height", $columncontainer.css("height"));
	//$container.css("vertical-align", self.widget.getPreference("align"));
	//$widget.css("margin-top", ($container.innerHeight()-$(".column-container", this.widget.body).outerHeight()-10) + "px");
	
	
	$('.column-title', self.widget.body).text(title);
	$('.column-number, .column-title', self.widget.body).css("color",color);

    $.getJSON(self.widget.getPreference("proxy"), function(data) {
    	var $container = $('.column-container', self.widget.body);
    	$container.each(function(){


		    var options = {
		        chart: {
		        	renderTo: this,
		        	margin: 0,
			        type: 'column',
			        zoomType: 'xy',
			        backgroundColor: 'transparent'
			    },

			    credits: {
					enabled: false
				},

		        legend: {
				    enabled: false
				},

			    title: {
			    	text: null
			    },

			    plotOptions: {
				    series: {
				        shadow:false,
				        borderRadius:1,
				        borderWidth:0
				    },
				    column: {
				    	pointWidth: 6
				    }
				},
		        
		        xAxis: {
                    labels: {
                    	enabled: false
                    },
                    lineWidth: 0,
					minorGridLineWidth: 0,
					lineColor: 'transparent',
					minorTickLength: 0,
					tickLength: 0
		        },

		        yAxis: {
		        	labels: {
                    	enabled: false
                    },
		            title: {
		                enabled: false
		            },
                    lineWidth: 0,
					minorGridLineWidth: 0,
					lineColor: 'transparent',
					minorTickLength: 0,
					tickLength: 0,
					gridLineColor: 'transparent'
		        },

		        tooltip: {
				    enabled: false
				},
			
			    series: [{
			    	data: data.data,
			    	color: color
			    }]
		    };

	        var chart = new Highcharts.Chart(options);

	        $('.column-number', self.widget.body).text(data.total);

	    })
    }).fail( function(){
    	bb_labs.publishToRadioChannel("error", "Error drawing column chart", "Verify the configuration of this widget.");
    }).done( function(){
    }).always( function(){
    });
};

bb_labs.ColumnChart.prototype.i18n	= function(){
	var self = this;

	var $title = $('.column-title', self.widget.body);
	
	var titleresourceid = this.widget.getPreference("title");
	titleresourceid = bb_labs.replaceAll(titleresourceid, " ", "");
	$title.html(bb_labs.i18n.getResourceFailSafe(titleresourceid));

};

