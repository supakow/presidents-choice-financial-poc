bb_labs.UGCBookmarks = function(oWidget){
	this.widget = oWidget;
	this.reference = "";
	this.init();
	this.i18n();
};
bb_labs.UGCBookmarks.prototype 		= {};
bb_labs.UGCBookmarks.prototype.init	= function(){
	
	var self = this;

	this.reference = this.widget.getPreference("referenceid");
	if((this.reference=="")||(typeof(this.widget.getPreference("referenceid"))=="undefined")) this.reference = b$.portal.pageName;
	

	var $newbookmark = $(".add-bookmark", self.widget.body);
	$newbookmark.click(function(){
		var $modal = $(".modal", self.widget.body); 
    	$modal.modal({
    		backdrop:false,
    		keyboard: true,
    		show: true
    	});
    	
    	$(".btnsave", $modal).click(function(){
    		var icon = b$.portal.portalModel.serverURL + "/static/com.backbase.labs/widgets/ugc/media/bookmark.png";
    		var title = $(".bookmark-title", $modal).val();
    		var url = $(".bookmark-url", $modal).val();
    		self.saveBookmark(icon, url, title);
    		$modal.modal('toggle');
    	});
    });


	this.loadBookmarks();
	
	gadgets.pubsub.subscribe("ugc_bookmarkadded", function(message){
		self.loadBookmarks();
	});

};

bb_labs.UGCBookmarks.prototype.saveBookmark = function(icon, url, title){
	var self = this;
	
	var bookmarkJson = "{\"icon\":\"" + icon + "\",\"title\":\"" + title + "\",\"url\":\"" + url + "\"}";
	
	
	$.ajax({
		url: b$.portal.portalModel.serverURL + "/proxy?pipe=ugcPipe&widgetid=" + self.widget.id + "&portalname=" + b$.portal.portalName + "&reference=" + this.reference + "&type=bookmark",
		type: 'post',
		data: "ugc_data=" + bookmarkJson,
		dataType: 'json',
		async:true,
		success: function(jsonresults){
			if(jsonresults.state=="success"){
				self.loadBookmarks();
			} else {
				bb_labs.publishToRadioChannel("error", "UGC", jsonresults.message);
			}	

		},
	    error: function(ajaxErrorObject){
	    	bb_labs.publishAjaxError(ajaxErrorObject);
	    }
	});

};

bb_labs.UGCBookmarks.prototype.deleteBookmark = function(id){
	var self = this;
	
	$.ajax({
		url: b$.portal.portalModel.serverURL + "/proxy?pipe=ugcPipe&widgetid=" + self.widget.id + "&portalname=" + b$.portal.portalName + "&reference=" + this.reference + "&type=bookmark&id=" + id,
		type: 'delete',
		dataType: 'json',
		async:true,
		success: function(jsonresults){
			if(jsonresults.state=="success"){
				self.loadBookmarks();
			} else {
				bb_labs.publishToRadioChannel("error", "UGC", jsonresults.message);
			}	

		},
	    error: function(ajaxErrorObject){
	    	bb_labs.publishAjaxError(ajaxErrorObject);
	    }
	});

};

bb_labs.UGCBookmarks.prototype.loadBookmarks = function(){
	var self = this;

	var $bookmarks_container = $(".bb-labs-ugc-bookmarks .bookmarks",self.widget.body);

	$.ajax({
		url: b$.portal.portalModel.serverURL + "/proxy?pipe=ugcPipe&widgetid=" + self.widget.id + "&portalname=" + b$.portal.portalName + "&reference=" + this.reference + "&type=bookmark&filter=user",
		type: 'get',
		dataType: 'json',
		async:true,
		success: function(jsonresults){
			if(jsonresults.state=="success"){
				var data = jsonresults.data;
				var bookmarksjson = {};
				var bookmarks = bookmarksjson.bookmarks = [];
				for(var i=0; i<data.length;i++){
					var jsonobj = $.parseJSON(data[i].content);
					bookmarks[i] = jsonobj;
					bookmarks[i].id = data[i].id;
				}
				
				var mustache_template = $("script[data-template='bookmarks']", self.widget.body).html();
				var htmlresults = Mustache.to_html(mustache_template, bookmarksjson);

				$bookmarks_container.html(htmlresults);
				
				setTimeout(function(){
					var $bookmarks = $(".bookmark", $bookmarks_container);
					$bookmarks.hover(function(){
						$(".delete", this).addClass("active");						
					}, function(){
						$(".delete", this).removeClass("active");						
					});
					
					var $deletes = $(".delete", $bookmarks_container);
					$deletes.click(function(){
						var $delete = $(this);
						var id = $delete.parent().attr("data-id");
						self.deleteBookmark(id);
					});
					
				}, 1000);

			} else {
				bb_labs.publishToRadioChannel("error", "UGC", jsonresults.message);
			}	

		},
	    error: function(ajaxErrorObject){
	    	bb_labs.publishAjaxError(ajaxErrorObject);
	    }
	});
	
};


bb_labs.UGCBookmarks.prototype.i18n	= function(){
	var self = this;
};



