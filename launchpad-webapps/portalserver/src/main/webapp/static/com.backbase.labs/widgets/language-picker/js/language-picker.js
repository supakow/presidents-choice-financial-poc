bb_labs.LanguagePicker 			= function(oWidget){
	this.widget = oWidget;
	this.init();
};
bb_labs.LanguagePicker.prototype 		= {};
bb_labs.LanguagePicker.prototype.init	= function(){
	
	var self = this;
	var $widget = $(this.widget.htmlNode);
	var style = this.widget.getPreference("style");

	var default_locale = bb_labs.getSessionValue("locale")!=""?bb_labs.getSessionValue("locale"):this.widget.getPreference("defaultlocale");
	
	var $dropdown_container = $(".bb-labs-language-picker", this.widget.body);
	var $toggle = $(".dropdown-toggle", $dropdown_container);
	var $toggle_icon = $("img", $toggle);
	var $toggle_text = $("span", $toggle);
	var $toggle_caret = $("b", $toggle);
	
	if(style=="icon") {
		$toggle_icon.css("display","inline");
		$toggle_text.css("display","none");
		$toggle_caret.css("display","none");
		$dropdown_container.addClass("icononly");
	}
	if(style=="text") {
		$toggle_text.css("display","inline");
		$toggle_icon.css("display","none");
		$toggle_caret.css("display","inline-block");
		$dropdown_container.addClass("textonly");
	}
	if(style=="both") {
		$toggle_icon.css("display","inline");
		$toggle_text.css("display","inline");
		$toggle_caret.css("display","inline-block");
		$dropdown_container.removeClass("icononly");
		$dropdown_container.removeClass("textonly");
	}
	
	$widget.css("max-width", $dropdown_container.width());

	var $options = $(".dropdown-menu a", $dropdown_container);
	$options.each(function(){
		var $option = $(this);
		var $img = $("img", $option);
		var $text = $("span", $option);
		var option_locale = $option.attr("data-locale"); 
		
		$option.click(function(){
			self.changelocale(option_locale, $text.html(), $img.attr("src"));
		});
		
		if(option_locale==default_locale){
			self.changelocale(option_locale, $text.html(), $img.attr("src"));
		}
	})
	
};

bb_labs.LanguagePicker.prototype.changelocale	= function(locale, text, img_url){
	//console.log(locale + text + img_url);

	var $dropdown_container = $(".bb-labs-language-picker", this.widget.body);
	var $toggle = $(".dropdown-toggle", $dropdown_container);
	var $toggle_icon = $("img", $toggle);
	var $toggle_text = $("span", $toggle);
	//var $toggle_caret = $("b", $toggle);
	
	$toggle_icon.attr("src", img_url.replace(".png", "_gray.png"));
	$toggle_text.html(text);

	this.updatei18n(locale);
	
};

bb_labs.LanguagePicker.prototype.updatei18n = function(locale){
	bb_labs.i18n.setLocale(locale);
	bb_labs.i18n.bootContent();
	bb_labs.i18n.bootWidgetControlers();
};

