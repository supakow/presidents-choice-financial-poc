bb_labs.ActionButton 			= function(oWidget){
	this.widget = oWidget;
	this.init();
	this.i18n();
};
bb_labs.ActionButton.prototype 		= {};
bb_labs.ActionButton.prototype.init	= function(){
	
	var self = this;
	
	var $button = $(".bb-labs-action-button", this.widget.body);
	
	$(".bb-labs-action-button a", this.widget.body).attr("href", this.widget.getPreference("navurl"));
	$(".bb-labs-action-button a i", this.widget.body).attr("class", this.widget.getPreference("iconclass"));

	var $widget = $(this.widget.htmlNode);
	var width = this.widget.getPreference("width");

	var countervalue = 0;
	var showcounter = (this.widget.getPreference("counterdisplaytype")!="none"); 
	var $counter = $(".counter", self.widget.body); 
	
	if(showcounter){
		var $counterstyle = $("span", $counter);
		var $countervalue = $("p", $counter);
		if(this.widget.getPreference("counterdisplaytype")=="triangle"){
			$counterstyle.attr("class", "triangle-button");
			$counterstyle.css("color", self.widget.getPreference("counterbgcolor"));
			$countervalue.css("color", self.widget.getPreference("countercolor"));
		} else {
			$counterstyle.hide();
			$countervalue.css("background-color", self.widget.getPreference("counterbgcolor"));
			$countervalue.css("color", self.widget.getPreference("countercolor"));
			$countervalue.addClass("boxed");
		}
	} else {
		$counter.hide();
	}
	
	$button.css("min-width", "100%");
	if((width!="")&&(width!="0")){
		$widget.css("max-width", width);
	}

	if(showcounter){
		$.ajax({
			url: self.widget.getPreference("counterurl"),
			method: 'get',
			dataType: 'html',
			async:true,
			success: function(proxycontent){
				try{
					var jsonresults = $.parseJSON(proxycontent);
					countervalue = jsonresults.total;
					$countervalue.html(countervalue);		
				} catch (exception){
					$counter.hide();
					//bb_labs.publishToRadioChannel("error", "Action Button", proxycontent);
				}
			},
		    error: function(ajaxErrorObject){
				$counter.hide();
		    	//bb_labs.publishAjaxError(ajaxErrorObject);
		    }
		});
	}
};
bb_labs.ActionButton.prototype.i18n	= function(){
	var buttontext = bb_labs.i18n.getResourceFailSafe(this.widget.getPreference("title"));
	$(".message", this.widget.body).html(buttontext);
};

