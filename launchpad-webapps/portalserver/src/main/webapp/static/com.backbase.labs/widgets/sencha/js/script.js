bb_labs.Sencha 			= function(oWidget){
	this.chart = null;
	this.widget = oWidget;
	this.init();
	this.i18n();
};
bb_labs.Sencha.prototype 		= {};
bb_labs.Sencha.prototype.init	= function(){
	
	var self = this;

	$(".bb-labs-sencha .btn.save").click(function(){
        Ext.MessageBox.confirm('Confirm Download', 'Would you like to download the chart as an image?', function(choice){
            if(choice == 'yes'){
                self.chart.save({
                    type: 'image/png'
                });
            }
        });
	});
	
	Ext.require('Ext.chart.*');
	Ext.require(['Ext.fx.target.Sprite', 'Ext.layout.container.Fit', 'Ext.window.MessageBox']);

	Ext.onReady(function () {
	
		var store1 = Ext.create('Ext.data.JsonStore', {
	        fields: ['name', 'data1', 'data2', 'data3', 'data4', 'data5', 'data6', 'data7', 'data9', 'data9'],
	        data: self.generateData()
	    });
		

	    self.chart = Ext.create('Ext.chart.Chart', {
            id: 'chartCmp',
            xtype: 'chart',
            style: 'background:#fff',
            theme: 'Category2',
            insetPadding: 20,
            animate: true,
            store: store1,
            legend: {
                position: 'right'
            },
            axes: [{
                type: 'Radial',
                position: 'radial',
                label: {
                    display: true
                }
            }],
            series: [{
                showInLegend: true,
                type: 'radar',
                xField: 'name',
                yField: 'data1',
                style: {
                    opacity: 0.4
                }
            },{
                showInLegend: true,
                type: 'radar',
                xField: 'name',
                yField: 'data2',
                style: {
                    opacity: 0.4
                }
            },{
                showInLegend: true,
                type: 'radar',
                xField: 'name',
                yField: 'data3',
                style: {
                    opacity: 0.4
                }
            }]
        });

	    var panel = Ext.create('Ext.Panel', {
	        renderTo: $(".bb-labs-sencha .chart", self.widget.body).get(0),
	        width: 550,
	        height: 400,
	        minHeight: 200,
	        minWidth: 200,
	        border:0,
	        layout: 'fit',
	        items: self.chart
        });

	});
	
};

bb_labs.Sencha.prototype.generateData = function(n, floor){
    var data = [],
        p = (Math.random() *  11) + 1,
        i;
        
    floor = (!floor && floor !== 0)? 20 : floor;
    
    for (i = 0; i < (n || 12); i++) {
        data.push({
            name: Ext.Date.monthNames[i % 12],
            data1: Math.floor(Math.max((Math.random() * 100), floor)),
            data2: Math.floor(Math.max((Math.random() * 100), floor)),
            data3: Math.floor(Math.max((Math.random() * 100), floor)),
            data4: Math.floor(Math.max((Math.random() * 100), floor)),
            data5: Math.floor(Math.max((Math.random() * 100), floor)),
            data6: Math.floor(Math.max((Math.random() * 100), floor)),
            data7: Math.floor(Math.max((Math.random() * 100), floor)),
            data8: Math.floor(Math.max((Math.random() * 100), floor)),
            data9: Math.floor(Math.max((Math.random() * 100), floor))
        });
    }
    return data;
}

bb_labs.Sencha.prototype.i18n	= function(){
	//bb_labs.i18n.getResourceFailSafe("resourceid");
};

