if (!window.bb_labs) window.bb_labs = {};

bb_labs.SearchWidget = (function() {
	var onload = function(oWidget) {
		init(oWidget);

		if(bd!=null){
			if (bd.designMode == 'true') {
			}
		}
	};
	
	
	var init = function(oWidget) { 

		var self = this;
		
		$(".form-search .search", oWidget.body).click(function(){
			var searchinput = $('.search-query', oWidget.body);
			gadgets.pubsub.publish("search", searchinput.val());
		});

		gadgets.pubsub.subscribe("search", function(msg){
			var query = msg;
			if(query=="") query = "*";
			
			var searchresults = $('.search-results', oWidget.body);
			
			$.ajax({
				url: "/portalserver/proxy?pipe=searchPipe&q=" + query,
				method: 'get',
				dataType: 'html',
				async:true,
				success: function(proxycontent){
					try{
						var jsonresults = $.parseJSON(proxycontent);
						if(jsonresults.state=="success"){
							var mustache_results_template = $("script[data-template='search-results']", oWidget.body).html();
							var htmlresults = Mustache.to_html(mustache_results_template, jsonresults.data.response);
							searchresults.html(htmlresults);
						} else {
							bb_labs.publishToRadioChannel("error", "Search", jsonresults.message);
						}	
					} catch (exception){
						bb_labs.publishToRadioChannel("error", "Search", proxycontent);
					}
				},
			    error: function(ajaxErrorObject){
			    	bb_labs.publishAjaxError(ajaxErrorObject);
			    }
			});
			
		});
		
	};

	//public functions
	return {
		onload: onload
	};

}());