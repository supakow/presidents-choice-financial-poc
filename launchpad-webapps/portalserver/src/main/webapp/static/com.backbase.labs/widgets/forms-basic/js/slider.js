bb_labs.FormsBasic_Slider = function(oWidget){
    this.widget = oWidget;
    this.init();
};
bb_labs.FormsBasic_Slider.prototype 		= {};
bb_labs.FormsBasic_Slider.prototype.init	= function(){
    var self = this;
    var widgetPref = {
        "required": this.widget.getPreference("required"),
        "minimum": this.widget.getPreference("minimum"),
        "maximum": this.widget.getPreference("maximum"),
        "step": this.widget.getPreference("step"),
        "orientation": this.widget.getPreference("orientation"),
        "tooltip": this.widget.getPreference("tooltip"),
        "handle": this.widget.getPreference("handle"),
        "labelForSlider": this.widget.getPreference("labelForSlider"),
        displaySelectionLocation: this.widget.getPreference("displaySelectionLocation")
    };

    widgetPref.sliderValue = this.transformSelectionType(this.widget.getPreference("initialValue"), this.widget.getPreference("rangeEnd"));

    var oView  = Mustache.to_html(this.getTemplate("template_slider"),widgetPref);
    $(this.widget.body).find("div.bb-labs-forms-basic-slider").html(oView);
    $(this.widget.body).find('.bootstrap-slider').bootstrapslider().on("slide", function(e) {
        $(self.widget.body).find('.current-value-box').html(self.getCurrentValue());
    });
    $(self.widget.body).find('.current-value-box').html(self.getCurrentValue());
};

bb_labs.FormsBasic_Slider.prototype.getTemplate	= function(name){
    return $(this.widget.body).find('[data-template="'+name+'"]').html();
};

bb_labs.FormsBasic_Slider.prototype.transformSelectionType = function(initial,end) {
    if (end !== "") {
        return "[" + initial + "," + end + "]";
    } else {
        return initial;
    }
};

bb_labs.FormsBasic_Slider.prototype.getCurrentValue = function() {
    var curVal = $(this.widget.body).find('.bootstrap-slider').bootstrapslider("getValue");
    var retVal = "";
    if (typeof(curVal) === "object") {
        //loop through range of values
        for (var i=0; i < curVal.length; i++) {
            retVal += (curVal[i] + ":");
        }
        //remove final colon
        retVal = retVal.substring(0, retVal.length - 1);
    } else {
        //not a range, just return single value
        retVal = curVal;
    }
    return retVal;
};