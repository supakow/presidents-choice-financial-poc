bb_labs.ToDoList 					= function(oWidget){
	this.widget = oWidget;
	this.init();
};
bb_labs.ToDoList.prototype 		= {};
bb_labs.ToDoList.prototype.init	= function(){
	
	var $removes = $(".remove", this.widget.body);
	$removes.click(function(event){
		event.stopPropagation();
		
		var $remove = $(this);
		$remove.parent().remove();
	});
};
