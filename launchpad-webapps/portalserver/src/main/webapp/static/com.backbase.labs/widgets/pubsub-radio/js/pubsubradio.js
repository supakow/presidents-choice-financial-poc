bb_labs.PubSubRadio 				= function(oWidget){
	this.widget = oWidget;
	this.init();
};
bb_labs.PubSubRadio.prototype 		= {};
bb_labs.PubSubRadio.prototype.init	= function(){
	var self = this;
	var widget_channel = this.widget.getPreference("channel");
	var widget_type = this.widget.getPreference("type");
	
	//Set the designmode message
	var $designmode_message = $(".designmode-message", this.widget.body);
	var html = $designmode_message.html();
	html = html.replace("[*type*]", widget_type).replace("[*channel*]", widget_channel);
	$designmode_message.html(html);
	
	
	//Subscribe to channel
	gadgets.pubsub.subscribe(widget_channel, function(jsonMessage){
		var alert_template = $("script[data-template='alert']", self.widget.body).html();
		
		var data = {};
		data.type = widget_type;
		data.title = "";
		data.message = jsonMessage;
		
		try{
			console.log(jsonMessage);
			var $json = $.parseJSON(jsonMessage);
			data.title = $json.message;
			data.message = $json.details;
		} catch(exception) {
			console.log(exception);
		}

		//get and create alert element
		var alerthtml = Mustache.to_html(alert_template, data);
		var $alert = $(alerthtml);
		

		//hide empty title
		var $title = $(".title", $alert);
		//if($title.html()=="") $title.hide();
		$title.hide();  //for some reason close does not work if title shown.  ToDo!
		
		//add alert to widget
		$(".bb-labs-pubsubradio", self.widget.body).append($alert);
		
		//covert to alert for close button to work
		$alert.alert();
		$alert.fadeIn(500);

	});
	
};