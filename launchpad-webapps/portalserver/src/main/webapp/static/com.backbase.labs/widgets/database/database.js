if (!window.bb_labs) window.bb_labs = {};

bb_labs.DatabaseWidget = (function() {
	var onload = function(oGadget) {
		init(oGadget);
		if (bd.designMode == 'true') {
		}
	};
	
	
	var init = function(oGadget) { 
		var query = oGadget.model.getPreference('query');
		var div = jQuery('.div', oGadget.body);
		div.html(performProxyRequest("&query=" + query));
	};

	
	var performProxyRequest = function(queryString) {
		var proxycontent = jQuery.ajax({
		    url: "/portalserver/proxy?pipe=databasePipe&url=about:blank" + queryString,
		    method:'POST',
		    dataType:'json',
		    async:false});
		return proxycontent.responseText;
	}
	
	var handleError = function(oGadget, errorSource, errorMessage){
		var widgetbody = jQuery(oGadget.body);
		widgetbody.html(errorMessage);
	}
	
	//public functions
	return {
		onload: onload
	};

}());