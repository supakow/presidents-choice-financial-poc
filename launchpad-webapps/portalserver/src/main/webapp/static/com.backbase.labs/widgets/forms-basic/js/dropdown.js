bb_labs.FormsBasic_Dropdown = function(oWidget){
    this.widget = oWidget;
    this.init();
};
bb_labs.FormsBasic_Dropdown.prototype 		= {};
bb_labs.FormsBasic_Dropdown.prototype.init	= function(){
    var widgetPref = {
        "required": this.widget.getPreference("required"),
        "labelForDropdown": this.widget.getPreference("labelForDropdown"),
        "options": this.parseSavedOptions(this.widget.getPreference("hiddenSelectOptions"))
    };

    var oView  = Mustache.to_html(this.getTemplate("template_dropdown"),widgetPref);
    $(this.widget.body).find("div.bb-labs-forms-basic-dropdown").html(oView);

    $(this.widget).on('preferenceFormReady', $.proxy(function(event){
        this.settingsTabContent = $(event.target.htmlNode).find("#PreferenceEditorContent").parent();
        this.setupOriginalView();
        this.setupSelectView();
        this.originalView = this.settingsTabContent.find("#PreferenceEditorContent");
        this.selectView = this.settingsTabContent.find("div.select-view-container");
        this.showOriginalView();
        this.selectView.find("div.row-container").empty();
        this.addNewSelectOption(null, {"options": this.parseSavedOptions(this.widget.getPreference("hiddenSelectOptions"))});
    }, this));
};

bb_labs.FormsBasic_Dropdown.prototype.getTemplate	= function(name){
    return $(this.widget.body).find('[data-template="'+name+'"]').html();
};

bb_labs.FormsBasic_Dropdown.prototype.showSelectView = function() {
    this.originalView.hide();
    this.selectView.show();
};

bb_labs.FormsBasic_Dropdown.prototype.showOriginalView = function() {
    this.selectView.hide();
    this.originalView.show();
};

bb_labs.FormsBasic_Dropdown.prototype.saveOptions = function() {
    var optText = this.optionsToText();
    this.overwriteHiddenOptions(optText);
    this.showOriginalView();
};

bb_labs.FormsBasic_Dropdown.prototype.optionsToText = function() {
    var optionText = "[";
    this.selectView.find("div.select-option-row").each(function(index){
        //if label or value is empty, skip to next row
        if (($(this).find("input[name='label']").val() === "") || ($(this).find("input[name='value']").val() === "")) {
            return true;
        }
        optionText += "{\"label\":";
        optionText += "\"" + $(this).find("input[name='label']").val() + "\"";
        optionText += ",\"value\":";
        optionText += "\"" + $(this).find("input[name='value']").val() + "\"";
        optionText += "},";
    });
    //if last row, remove final comma
    optionText = optionText.substring(0,optionText.length-1);
    optionText += "]";
    return optionText;
};

bb_labs.FormsBasic_Dropdown.prototype.overwriteHiddenOptions = function(newOptions) {
    this.originalView.find("input[name='hiddenSelectOptions']").val(newOptions);
};

bb_labs.FormsBasic_Dropdown.prototype.parseSavedOptions = function(savedOptions) {
    return $.parseJSON(savedOptions);
};

bb_labs.FormsBasic_Dropdown.prototype.setupSelectView = function() {
    if (this.settingsTabContent.find("div.select-view-container").length) return;
    var selectViewContent = Mustache.to_html(this.getTemplate("template_select_view"));
    this.settingsTabContent.append(selectViewContent);
    this.settingsTabContent.find("button.save").on("click", $.proxy(this.saveOptions, this));
    this.settingsTabContent.find("button.close").on("click",this.removeOption);
    this.settingsTabContent.find("button.new-row").on("click", $.proxy(this.addNewSelectOption,this));
};

bb_labs.FormsBasic_Dropdown.prototype.setupOriginalView = function() {
    //hide hidden select options - .hide() isn't working here for some reason
    this.settingsTabContent.find("input[name='hiddenSelectOptions']").parents("tr.xbp-userPreferenceField").css("visibility", "hidden");

    //add link to select view
    var tbody = this.settingsTabContent.find("tbody.portalManagerPreferenceFieldView");
    //if link exists, don't add again
    if (tbody.find("button.options-link").length) return;
    var selectLink = Mustache.to_html(this.getTemplate("template_navigate_to_select_view"));
    tbody.append(selectLink);
    this.settingsTabContent.find("button.options-link").on("click", $.proxy(this.showSelectView, this));
};

bb_labs.FormsBasic_Dropdown.prototype.addNewSelectOption = function(event,options) {
    options = options || {"options":[
        {
            "label":"",
            "value":""
        }
    ]};
    var newRow  = Mustache.to_html(this.getTemplate("template_select_new_row"),options);
    newRow = $.parseHTML(newRow);
    $(newRow).find("button.close").on("click", this.removeOption);
    var temp = this;
    temp.settingsTabContent.find("div.row-container").append(newRow);
};

bb_labs.FormsBasic_Dropdown.prototype.removeOption = function(event) {
    $(event.target).parent().remove();
};