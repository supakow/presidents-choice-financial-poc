bb_labs.ActivityFeed = function(oWidget){
	this.widget = oWidget;
	this.init();
};

bb_labs.ActivityFeed.prototype 		= {};
bb_labs.ActivityFeed.prototype.init	= function(){
	var self = this;

	var $timeline = $("ul.timeline", self.widget.body);
	$timeline.css("min-height", self.widget.getPreference("minheight"));
	$timeline.css("max-height", self.widget.getPreference("maxheight"));
	
	//Initialize Feed from URL
	var sUrl = self.widget.getPreference("feedURL");

	if(sUrl != ""){
		$.ajax({
			url:sUrl,
			success:function(sData){
				var oView  = Mustache.to_html(self.getTemplate("template_feed_item"),sData);
				$(self.widget.body).find("ul.timeline").html(oView);
			},
			error:function(){
				console.log("There was an error getting the feed data.");
			}
		});
	}

	//Bind buttons
	$(this.widget.body).find(".actFilter").on("click",function(event){
		event.stopPropagation();
		var iFilter = ($(this).attr("data-option-value") != "all")?"." + $(this).attr("data-option-value"):"";
		$(self.widget.body).animate({
			opacity:0
		},300,function(){
			$(self.widget.body).find("ul.timeline li").css("display","none");
			$(self.widget.body).find("ul.timeline li"+iFilter).css("display","block");
			$(self.widget.body).animate({
				opacity:1
			},300);
		});
		
	});
};

bb_labs.ActivityFeed.prototype.maximize	= function(){
	var self = this;
	var $timeline = $("ul.timeline", self.widget.body);
	$timeline.css("min-height", self.widget.getPreference("minheight"));
	$timeline.css("max-height", "");
};
bb_labs.ActivityFeed.prototype.restore	= function(){
	var self = this;
	var $timeline = $("ul.timeline", self.widget.body);
	$timeline.css("min-height", self.widget.getPreference("minheight"));
	$timeline.css("max-height", self.widget.getPreference("maxheight"));
};

bb_labs.ActivityFeed.prototype.getTemplate	= function(name){
	var self = this;
	return $(self.widget.body).find('[data-template="'+name+'"]').html();
}