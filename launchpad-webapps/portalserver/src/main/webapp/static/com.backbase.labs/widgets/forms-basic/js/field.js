bb_labs.FormsBasic_Field = function(oWidget){
	this.widget = oWidget;
	this.utils = new bb_labs.FormsBasic_Utilities(oWidget);
	this.field_type = "input";
	this.field_name = this.widget.id + "_" + this.field_type;
	this.$field = null; 
	this.containercontroller = null;
	
	this.init();
};
bb_labs.FormsBasic_Field.prototype 		= {};
bb_labs.FormsBasic_Field.prototype.init	= function(){
	var self = this;
	
	this.field_type = this.widget.getPreference("fieldtype");
	if(this.widget.getPreference("fieldname")!=""){
		this.field_name = this.widget.getPreference("fieldname");
	}
	
	var required = this.widget.getPreference("required");
	var label = this.widget.getPreference("label");
	var hint = this.widget.getPreference("hint");
	var cssclass = this.widget.getPreference("cssclass");
	var prefix = this.widget.getPreference("prefix");
	var suffix = this.widget.getPreference("suffix");
	var style = this.widget.getPreference("style");
	var defaultvalue = this.widget.getPreference("defaultvalue");
	var databindingurl = this.widget.getPreference("databindingurl");

	//Render the field
	var field_jsonobject = {
			fieldname: this.field_name,
			label:label,
			hint:hint,
			required:required,
			prefix:prefix,
			suffix:suffix
	};
	var field_mustache_template = $("script[data-template='" + this.field_type + "-field']", this.widget.body).html();
	var field_html = Mustache.to_html(field_mustache_template, field_jsonobject);

	//add field to widget body
	var $fieldcontainer = $(".bb-labs-forms-basic-field", this.widget.body);
	$fieldcontainer.addClass("form-" + style);
	if(cssclass!="") $fieldcontainer.addClass(cssclass); 
	$fieldcontainer.html(field_html);
	
	//get the data from the server
	if(databindingurl!=""){
		defaultvalue = this.utils.getBindingData(databindingurl);
	}
	
	this.$field = $("#" + this.field_name, $fieldcontainer);
	this.$field.val(defaultvalue);
	self.$field.attr("data-formsField-value", self.$field.val());
	this.$field.change(function(){
		self.$field.attr("data-formsField-value", self.$field.val());
		self.validate();
	});

	//user time out to give DOM time to catch up
	setTimeout(function(){
		//Using this validator require a ".validate()" callback to be implemented
		self.containercontroller = self.utils.getContainer();
		self.containercontroller.validator.register(self.utils.getCurrentPageIndex(), self);
	},1000);
};

//This validate function is required if you register the widget for form validation
bb_labs.FormsBasic_Field.prototype.validate	= function(){
	var validated = true;
	var required =  (this.widget.getPreference("required")=="true");
	var validationrule = this.widget.getPreference("jsvalidationrule");
	var datavalidationurl = this.widget.getPreference("datavalidationurl");
	var fieldvalue = this.$field.attr("data-formsField-value");
	var errormessage = this.widget.getPreference("csvalidationerror");
	
	if(required){
		validated = this.containercontroller.validator.clientValidation("required", fieldvalue);
	}
	
	if((validationrule!="none")&&(validated)){
		validated = this.containercontroller.validator.clientValidation(validationrule, fieldvalue);
	}
	
	if((datavalidationurl!="")&&(validated)){
		var servermessage = this.containercontroller.validator.serverValidation(datavalidationurl, fieldvalue);
		if(servermessage!=""){
			validated = false;
			errormessage += " [" + servermessage + "]"; 
		}
	}

	if((!validated)&&(!required)&&(fieldvalue.trim()=="")){
		validated = true;
	}
	
	var $fieldcontainer = $(".bb-labs-forms-basic-field", this.widget.body);
	if(!validated){
		$fieldcontainer.addClass("has-error");
		$(".error",$fieldcontainer).html(errormessage);
	} else {
		$fieldcontainer.removeClass("has-error");
		$(".error",$fieldcontainer).html("");
	}
	
	return validated;
};