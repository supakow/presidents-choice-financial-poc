bb_labs.i18nManager 			= function(oWidget){
	this.widget = oWidget;
	this.defaultlocale = this.widget.getPreference("defaultlocale");
	this.localeArray = this.widget.getPreference("locale").split(",");
	this.init();
};
bb_labs.i18nManager.prototype 		= {};
bb_labs.i18nManager.prototype.init	= function(){
	
	var self = this;
	
	var $listresources = $(".listresources", this.widget.body);
	var $listresources_title = $("label", $listresources);
	var $listresources_select = $("select", $listresources);
	var $listresources_create = $(".create", $listresources);
	var $listresources_delete = $(".delete", $listresources);
	
	var $editresources_info = $(".editresources .info p");
	var $editresources_save = $(".editresources .controls .save");
	var $editresources_translate = $(".editresources .controls .translate");
	
	$listresources_title.html($listresources_title.html().replace("[*default*]", this.defaultlocale));
	$editresources_info.html($editresources_info.html().replace("[*default*]", this.defaultlocale));
	
	$listresources_select.change(function(){
		var resourceid = $listresources_select.val();
		if((resourceid!="")&&(resourceid!=null)){
			$listresources_delete.removeAttr("disabled");
			self.buildEditFields(resourceid);
		}
	});
	
	$listresources_create.click(function(){
		self.buildEditFields(null);
	});
	
	$listresources_delete.click(function(){
		var resourceid = $listresources_select.val();
		if((resourceid!="")&&(resourceid!=null)){
			self.deleteResource(resourceid);
		}
	});
	
	$editresources_save.click(function(){
		self.saveFields();
	});
	
	$editresources_translate.click(function(){
		self.autoTranslate();
	});
	
	this.buildListResources(this.defaultlocale);
	
	
};


bb_labs.i18nManager.prototype.buildListResources = function(locale){
	var self = this;
	
	var $editresources = $(".editresources", this.widget.body);
	var $listresources_select = $(".listresources select", this.widget.body);
	var $listresources_delete = $(".listresources .delete", this.widget.body);
	
	$listresources_select.children().remove();
	
	$.ajax({
		url: b$.portal.portalModel.serverURL + "/proxy?pipe=i18nPipe&locale=" + locale,
		method: 'get',
		dataType: 'json',
		async:true,
		success: function(jsonresults){
			if(jsonresults.state=="success"){
				var data = jsonresults.data;
				for(var i = 0; i < data.length; i++) {
				    var resource = data[i];
				    $listresources_select.append("<option value=\"" + resource.key + "\">" + "[" + resource.key + "] " + resource.value + "</option>");
				}					
				if($editresources.hasClass("active")){
					$editresources.toggleClass("hidden","active");
				}
				$listresources_delete.attr("disabled", "disabled");

			} else {
				bb_labs.publishToRadioChannel("error", "i18n Manager", jsonresults.message);
			}	
		},
	    error: function(ajaxErrorObject){
	    	bb_labs.publishAjaxError(ajaxErrorObject);
	    }
	});	
};

bb_labs.i18nManager.prototype.deleteResource = function(resourceId){
	var self = this;
	
	var allsuccess = true;
	for(var i=0; i<this.localeArray.length;i++){
		var locale = this.localeArray[i];
		$.ajax({
			url: b$.portal.portalModel.serverURL + "/proxy?pipe=i18nPipe&locale=" + locale + "&resourceid=" + resourceId,
			type: 'delete',
			dataType: 'json',
			async:false,
			success: function(jsonresults){
				if(jsonresults.state=="success"){
					allsuccess = true;
				} else {
					bb_labs.publishToRadioChannel("error", "i18n Manager", jsonresults.message);
					allsuccess = false;
				}	
			},
		    error: function(ajaxErrorObject){
		    	bb_labs.publishAjaxError(ajaxErrorObject);
				allsuccess = false;
		    }
		});	
	}
	if(allsuccess){
		bb_labs.publishToRadioChannel("info", "i18n Manager", resourceId + " successfully deleted.");
		this.buildListResources(this.defaultlocale);
		bb_labs.removeSessionValue("i18n_cache");
	}
};

bb_labs.i18nManager.prototype.buildEditFields = function(resourceId){
	var self = this;

	var $editresources = $(".editresources", this.widget.body);
	var $resourceid = $(".resourceid input", $editresources);
	var $fields = $(".fields", $editresources);
	
	if(resourceId!=null){
		$resourceid.val(resourceId);
	} else {
		$resourceid.val("");
	}
	
	var mustache_field_template = $("script[data-template='resource-edit']", this.widget.body).html();

	$fields.html("");
	for(var i=0; i<this.localeArray.length;i++){
		var locale = this.localeArray[i];

		if(resourceId==null){
			var fieldid = locale + self.widget.id;
			var mustachedata = {"id":fieldid, "locale":locale, "placeholder":locale, "value":""};
			var htmlresults = Mustache.to_html(mustache_field_template, mustachedata);
			$fields.append(htmlresults);
		} else {
			$.ajax({
				url: b$.portal.portalModel.serverURL + "/proxy?pipe=i18nPipe&locale=" + locale + "&resourceid=" + resourceId,
				method: 'get',
				dataType: 'json',
				async:true,
				success: function(jsonresults){
					if(jsonresults.state=="success"){
						var locale = this.url.substr(this.url.indexOf("&locale=")+8, 5);
						var fieldid = locale + self.widget.id;
						var mustachedata = {"id":fieldid, "locale":locale, "placeholder":locale, "value":jsonresults.data};
						var htmlresults = Mustache.to_html(mustache_field_template, mustachedata);
						$fields.append(htmlresults);
					} else {
						bb_labs.publishToRadioChannel("error", "i18n Manager", jsonresults.message);
					}	
				},
			    error: function(ajaxErrorObject){
			    	//bb_labs.publishAjaxError(ajaxErrorObject);
			    	var locale = this.url.substr(this.url.indexOf("&locale=")+8, 5);
					var fieldid = locale + self.widget.id;
					var mustachedata = {"id":fieldid, "locale":locale, "placeholder":locale, "value":""};
					var htmlresults = Mustache.to_html(mustache_field_template, mustachedata);
					$fields.append(htmlresults);

			    }
			});	
		}
		
		if($editresources.hasClass("hidden")){
			$editresources.toggleClass("hidden","active");
		}
		
	}
};

bb_labs.i18nManager.prototype.autoTranslate = function(){
	var self = this;
	
	var $editresources = $(".editresources", this.widget.body);
	var $resourceid = $(".resourceid input", $editresources);
	var $fields = $(".fields", $editresources);
	var $defaultfield = $("*[data-locale='" + this.defaultlocale + "']", $fields);
	
	var resourceId = $resourceid.val();
	var defaultfieldvalue = $defaultfield.val();
	if((resourceId!="")&&(defaultfieldvalue!="")){
		var allsuccess = true;
		for(var i=0; i<this.localeArray.length;i++){
			var locale = this.localeArray[i];
			if(locale!=this.defaultlocale){
				var $field = $("*[data-locale='" + locale + "']", $fields);
				
				$.ajax({
					url: "/portalserver/proxy?pipe=translate&from_locale=" + this.defaultlocale + "&to_locale=" + locale + "&value="+defaultfieldvalue,
					type: 'get',
					dataType: 'json',
					async:false,
					success: function(jsonresults){
						$field.val(jsonresults.data[0][0][0]);
					},
				    error: function(ajaxErrorObject){
				    	bb_labs.publishAjaxError(ajaxErrorObject);
						allsuccess = false;
				    }
				});
			}
		}
		if(allsuccess){
			self.saveFields(true);
		}
	}
}


bb_labs.i18nManager.prototype.saveFields = function(isCalledAfterTranslate){
	var self = this;

	var $editresources = $(".editresources", this.widget.body);
	var $resourceid = $(".resourceid input", $editresources);
	var $fields = $(".fields", $editresources);
	var $defaultfield = $("*[data-locale='" + this.defaultlocale + "']", $fields);
	
	var resourceId = $resourceid.val();
	var defaultfieldvalue = $defaultfield.val();
	if((resourceId!="")&&(defaultfieldvalue!="")){
		var allsuccess = true;

		for(var i=0; i<this.localeArray.length;i++){
			var locale = this.localeArray[i];
			var $field = $("*[data-locale='" + locale + "']", $fields);
			var value = $field.val();
			if(value==""){
				$field.val(defaultfieldvalue);
				value = defaultfieldvalue;
			}
			
			$.ajax({
				url: b$.portal.portalModel.serverURL + "/proxy?pipe=i18nPipe&locale=" + locale + "&resourceid=" + resourceId + "&value=" + value,
				type: 'post',
				dataType: 'json',
				async:false,
				success: function(jsonresults){
					if(jsonresults.state=="success"){
						allsuccess = true;
					} else {
						bb_labs.publishToRadioChannel("error", "i18n Manager", jsonresults.message);
						allsuccess = false;
					}	
				},
			    error: function(ajaxErrorObject){
			    	bb_labs.publishAjaxError(ajaxErrorObject);
					allsuccess = false;
			    }
			});

			//force loop to stop if error occured
			if(!allsuccess) i=this.localeArray.length;
		}
		
		if(allsuccess){
			if(isCalledAfterTranslate){
				bb_labs.publishToRadioChannel("info", "i18n Manager", "Resource [" + resourceId + "] successfully translated and saved");
			} else {
				bb_labs.publishToRadioChannel("info", "i18n Manager", "Resource [" + resourceId + "] successfully saved");
			}
			this.buildListResources(this.defaultlocale);
			bb_labs.removeSessionValue("i18n_cache");
			
			bb_labs.i18n.bootContent();
			bb_labs.i18n.bootWidgetControlers();
		}
	} else {
		bb_labs.publishToRadioChannel("error", "i18n Manager", "Resource Id and " + this.defaultlocale + " value required");
	}
};