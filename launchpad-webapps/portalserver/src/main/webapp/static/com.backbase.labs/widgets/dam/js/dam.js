bb_labs.DAM 			= function(oWidget){
	this.widget = oWidget;
	this.reference = "";
	this.init();
	this.i18n();
};
bb_labs.DAM.prototype 		= {};
bb_labs.DAM.prototype.init	= function(){
	
	var self = this;
	
	this.reference = this.widget.getPreference("referenceid");
	if(typeof(this.widget.getPreference("referenceid"))=="undefined") this.reference = "";


	$(".bb-labs-dam .nav-tabs a").click(function (e) {
		e.preventDefault();
		$(this).tab("show");
	});
		
	
	var $search_input = $(".search-query", this.widget.htmlNode);
	$search_input.keyup(function(evt){
		if(evt.keyCode==13){
			self.search();
		}
	});

	var $search_icon = $(".glyphicon-search", this.widget.htmlNode);
	$search_icon.click(function(){
		self.search();
	});

	this.loadDocumentsList();
	
};


bb_labs.DAM.prototype.loadDocumentsList = function(){
	
	var self = this;
	
	var $tab_container = $(".bb-labs-dam .tab-container", this.widget.body);
	var proxyurl = b$.portal.portalModel.serverURL + "/proxy?pipe=damPipe&widgetid=" + self.widget.id + "&portalname=" + b$.portal.portalName + "&reference=" + this.reference;
	
	$.ajax({
		url: proxyurl,
		type: 'get',
		dataType: 'json',
		async:true,
		success: function(jsonresults){
			if(jsonresults.state=="success"){
				
				jsonresults.widgetid = self.widget.id;
				jsonresults.proxyurl = proxyurl;
				jsonresults.minheight = self.widget.getPreference("minheight");
				jsonresults.maxheight = self.widget.getPreference("maxheight");

				var mustache_template_tabs = $("script[data-template='tabs']", self.widget.body).html();
				var htmlresults = Mustache.to_html(mustache_template_tabs, jsonresults);

				$tab_container.html(htmlresults);
				
				$(".nav-tabs a:first", $tab_container).tab("show");

				
			} else {
				bb_labs.publishToRadioChannel("error", "DAM", jsonresults.message);
			};
		},
	    error: function(ajaxErrorObject){
	    	bb_labs.publishAjaxError(ajaxErrorObject);
	    }
	});

};

bb_labs.DAM.prototype.search = function(){
	var self = this;
	var $search_input = $(".search-query", this.widget.htmlNode);
	var query = $search_input.val();
	if(query=="") query="*";
	var searchurl = this.widget.getPreference("searchUrl").replace("[*query*]", query);

	$.ajax({
		url: searchurl,
		method: 'get',
		dataType: 'html',
		async:true,
		success: function(proxycontent){
			try{
				var jsonresults = $.parseJSON(proxycontent);
				if(jsonresults.state=="success"){
					var mustache_results_template = $("script[data-template='search-results']", self.widget.body).html();
					var htmlresults = Mustache.to_html(mustache_results_template, jsonresults.data.response);
					$(".modal-body", self.widget.body).html(htmlresults);
			    
					var $modal = $(".modal", self.widget.body); 
			    	$modal.modal({
			    			backdrop:false,
			    		  keyboard: true,
			    		  show: true
			    	});

				} else {
					bb_labs.publishToRadioChannel("error", "Search", jsonresults.message);
				}	
			} catch (exception){
				bb_labs.publishToRadioChannel("error", "Search", proxycontent);
			}
		},
	    error: function(ajaxErrorObject){
	    	bb_labs.publishAjaxError(ajaxErrorObject);
	    }
	});
};


bb_labs.DAM.prototype.i18n	= function(){
	var placeholdertext = bb_labs.i18n.getResourceFailSafe("search");
	var $search_input = $(".search-query", this.widget.htmlNode);
	$search_input.attr("placeholder", placeholdertext);
};

