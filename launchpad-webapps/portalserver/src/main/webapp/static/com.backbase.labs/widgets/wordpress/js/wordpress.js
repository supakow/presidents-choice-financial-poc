if (!window.bb_labs) window.bb_labs = {};

bb_labs.WordpressWidget = (function() {
	var posts = [];
	
	var onload = function(oWidget) {
	
		$(".icon-new-post",oWidget.htmlNode).click(function(){
			newclicked(oWidget);
		});
	
	
		$(".cancelbtn", oWidget.body).click(function(){
			showlist(oWidget);
		});
	
		$(".savebtn", oWidget.body).click(function(){
			saveclicked(oWidget);
		});

		init(oWidget);

		//What to do in designmode/portal manager
		if(bd!=null){
			if (bd.designMode == 'true') {
			}
		}
	};
	
	var init = function(oWidget) { 
		$.ajax({
			url: "/portalserver/proxy?pipe=wordpressPipe&action=recentposts",
			method: 'get',
			dataType: 'html',
			async:true,
			success: function(proxycontent){
				$(".table", oWidget.body).html("");
				var jsonobj = $.parseJSON(proxycontent);
				if(jsonobj.state=="success"){
					posts = jsonobj.data;
					for(var x=0; x<posts.length;x++){
						$(".table", oWidget.body).append(getlistrow(oWidget, posts[x], x));
					}
				} else {
					bb_labs.publishToRadioChannel("error", "Wordpress list posts", jsonobj.message);
				}
			},
			error: function(response){
				bb_labs.publishAjaxError(response);
			}
		});
	};

	var getlistrow = function(oWidget, record, idx){
		var dateobj = new Date(record.DateCreated);
		var monthNames = [ "jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec" ];
	
		var row = $("<tr class=\"post_" + idx + "\"><td class=\"colimg\"><img src=\"/portalserver/static/com.backbase.labs/widgets/wordpress/media/post.png\"/></td><td class=\"coltitle\"><div class=\"news-title\">" + record.Title + "</div><div class=\"news-content\">" + record.Excerpt + "</div></td><td class=\"coldate\">" + (dateobj.getDate()+1) + "<br/><label class=\"small\">" + monthNames[dateobj.getMonth()] + "</label></td></tr>");
		row.click(function(){
			$(".details_title",oWidget.body).html(record.Title);
			$(".details_excerpt",oWidget.body).html(record.Excerpt);
			$(".details_content",oWidget.body).html(record.Description);

			$(".list",oWidget.body).fadeOut(400, function(){
				$(".details",oWidget.body).css("display","block");
				oWidget.setPerspective("Maximized");

				var oTarget = $(oWidget.body).parents(".bd-dialog-container").first();

				$(oTarget).height($(oWidget.body).height());

				$(".details",oWidget.body).animate({
					opacity:1
				},400);
			});
		});
		return row;
	}

	var showlist = function(oWidget){

        $(".details, .crud",oWidget.body).css({
            display:"none",
            opacity:0
        });

        $(".list",oWidget.body).css({
            display:"block",
            opacity:"0"
        });

        $(".list",oWidget.body).delay(200).animate({
            opacity:1
        },400);
	}
	
	var newclicked = function(oWidget){
		$(".list",oWidget.body).fadeOut(400, function(){
			$(".crud",oWidget.body).fadeIn();
		});
		$(".details",oWidget.body).fadeOut(400, function(){
			$(".crud",oWidget.body).fadeIn();
		});
	};

	var saveclicked = function(oWidget){
		var title = $(".crud_title",oWidget.body).val();
		var excerpt = $(".crud_excerpt",oWidget.body).val();
		var content = $(".crud_content",oWidget.body).val();

		$.ajax({
			url: "/portalserver/proxy?pipe=wordpressPipe&action=create&title=" + title + "&description=" + content + "&excerpt=" + excerpt,
			method: 'get',
			dataType: 'html',
			async:true,
			success: function(proxycontent){
				init(oWidget);
				$(".crud",oWidget.body).fadeOut(400, function(){
					$(".list",oWidget.body).fadeIn();
				});
			},
			error: function(response){
				bb_labs.publishAjaxError(response);
			}
		});

	
	};
	
	//public functions
	return {
		onload: onload,
		showlist: showlist
	};

}());