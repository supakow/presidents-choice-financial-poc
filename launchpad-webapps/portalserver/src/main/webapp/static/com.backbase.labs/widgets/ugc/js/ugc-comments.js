bb_labs.UGCComments = function(oWidget){
	this.widget = oWidget;
	this.reference = "";
	this.init();
	this.i18n();
};
bb_labs.UGCComments.prototype 		= {};
bb_labs.UGCComments.prototype.init	= function(){
	
	var self = this;

	this.reference = this.widget.getPreference("referenceid");
	if((this.reference=="")||(typeof(this.widget.getPreference("referenceid"))=="undefined")) this.reference = b$.portal.pageName;
	
	
	this.loadComments();
	
	gadgets.pubsub.subscribe("ugc_comments_focus", function(message){
		$(".newcomment textarea", self.widget.body).focus();
	});

};

bb_labs.UGCComments.prototype.saveComment = function($comment_textarea){
	var self = this;
	
	$.ajax({
		url: b$.portal.portalModel.serverURL + "/proxy?pipe=ugcPipe&widgetid=" + self.widget.id + "&portalname=" + b$.portal.portalName + "&reference=" + this.reference,
		type: 'post',
		data: "ugc_data=" + $comment_textarea.val(),
		dataType: 'json',
		async:true,
		success: function(jsonresults){
			if(jsonresults.state=="success"){
				self.loadComments();
			} else {
				bb_labs.publishToRadioChannel("error", "UGC", jsonresults.message);
			}	

		},
	    error: function(ajaxErrorObject){
	    	bb_labs.publishAjaxError(ajaxErrorObject);
	    }
	});

};

bb_labs.UGCComments.prototype.deleteComment = function(id){
	var self = this;
	
	$.ajax({
		url: b$.portal.portalModel.serverURL + "/proxy?pipe=ugcPipe&widgetid=" + self.widget.id + "&portalname=" + b$.portal.portalName + "&reference=" + this.reference + "&id=" + id,
		type: 'delete',
		dataType: 'json',
		async:true,
		success: function(jsonresults){
			if(jsonresults.state=="success"){
				self.loadComments();
			} else {
				bb_labs.publishToRadioChannel("error", "UGC", jsonresults.message);
			}	

		},
	    error: function(ajaxErrorObject){
	    	bb_labs.publishAjaxError(ajaxErrorObject);
	    }
	});

};

bb_labs.UGCComments.prototype.loadComments = function(){
	var self = this;

	var $comments_container = $(".bb-labs-ugc-comments",self.widget.body);
	
	$.ajax({
		url: b$.portal.portalModel.serverURL + "/proxy?pipe=ugcPipe&widgetid=" + self.widget.id + "&portalname=" + b$.portal.portalName + "&reference=" + this.reference,
		type: 'get',
		dataType: 'json',
		async:true,
		success: function(jsonresults){
			if(jsonresults.state=="success"){
				var data = jsonresults.data;
				for(var i=0; i<data.length;i++){
					data[i].icon = b$.portal.portalModel.serverURL + "/static/com.backbase.labs/widgets/ugc/media/anonymous-user.png";
					data[i].date = self.dateFromId(data[i].id);
				}
				var commentsjson = {};
				commentsjson.comments = data;
				
				var newcommentjson = {"icon":"/portalserver/static/com.backbase.labs/widgets/ugc/media/anonymous-user.png"};
				
				var mustache_template_comments = $("script[data-template='comments']", self.widget.body).html();
				var htmlresults = Mustache.to_html(mustache_template_comments, commentsjson);

				var mustache_template_newcomment = $("script[data-template='newcomment']", self.widget.body).html();
				htmlresults += Mustache.to_html(mustache_template_newcomment, newcommentjson);
				
				$comments_container.html(htmlresults);
				
				setTimeout(function(){
					var $newcomment_container = $(".newcomment", $comments_container)
					var $img = $("img", $newcomment_container);
					var $newcomment_textarea = $("textarea", $newcomment_container); 
					$newcomment_textarea.width($newcomment_container.width() - $img.width() - 45);

					$newcomment_textarea.change(function(){
						self.saveComment($(this));
					});
					/*$newcomment_textarea.keyup(function(e){ 
					    var code = e.which; // recommended to use e.which, it's normalized across browsers
					    if(code==13){
					    	e.preventDefault();
							self.saveComment($(this));
					    }
					});*/
					
					var $deletes = $(".delete", $comments_container);
					$deletes.click(function(){
						var $delete = $(this);
						var id = $delete.parent().parent().parent().attr("data-id");
						self.deleteComment(id);
					});
					
					var $likes = $(".like", $comments_container);
					$likes.each(function(){
						$like = $(this);
						var id = $like.parent().parent().parent().attr("data-id");
						self.showlike(null, id);
						$like.click(function(){
							var currentvalue = $like.attr("data-liked")=="true"?true:false; 
							self.like(!currentvalue, id);
						});
					});
					
				}, 1000);

			} else {
				bb_labs.publishToRadioChannel("error", "UGC", jsonresults.message);
			}	

		},
	    error: function(ajaxErrorObject){
	    	bb_labs.publishAjaxError(ajaxErrorObject);
	    }
	});
	
};

bb_labs.UGCComments.prototype.dateFromId	= function(id){
	var idarr = id.split(" - ");
	var isodate = "";
	if(idarr.length>1){
		isodate = idarr[1];
		//We convert : to - for time on the server, as : is not allowed in folder names
		var isodatearr = isodate.split("T");
		isodate = isodatearr[0] + "T" + bb_labs.replaceAll(isodatearr[1], "-", ":");
	}
	
	var friendlydate = new Date(isodate);

	/*var h = this.zerobaseNumber(friendlydate.getHours());
	var m = this.zerobaseNumber(friendlydate.getMinutes());
	var s = this.zerobaseNumber(friendlydate.getSeconds());
	return friendlydate.toDateString() + " " + h + ":" + m + ":" + s;
	*/
	return $.timeago(friendlydate);
};

bb_labs.UGCComments.prototype.zerobaseNumber	= function(number){
	var returnnumber = number;
	if(number.toString().length == 1){
		returnnumber = "0" + number;
	}
	return returnnumber;
};


bb_labs.UGCComments.prototype.like = function(liked, commentid){
	var self = this;
	
	$.ajax({
		url: b$.portal.portalModel.serverURL + "/proxy?pipe=ugcPipe&widgetid=" + self.widget.id + "&portalname=" + b$.portal.portalName + "&reference=" + this.reference + "/" + commentid + "&type=like",
		type: 'post',
		data: "ugc_data=" + liked,
		dataType: 'json',
		async:true,
		success: function(jsonresults){
			if(jsonresults.state=="success"){
				var $like = $("*[data-id='" + commentid + "'] .like", self.widget.body);
				$like.attr("data-liked", liked);
				self.showlike(liked, commentid);
			} else {
				bb_labs.publishToRadioChannel("error", "UGC", jsonresults.message);
			}	

		},
	    error: function(ajaxErrorObject){
	    	bb_labs.publishAjaxError(ajaxErrorObject);
	    }
	});

};

bb_labs.UGCComments.prototype.showlike	= function(liked, commentid){
	var self = this;
	

	if((typeof(liked)=="undefined")||(liked==null)){
		$.ajax({
			url: b$.portal.portalModel.serverURL + "/proxy?pipe=ugcPipe&widgetid=" + self.widget.id + "&portalname=" + b$.portal.portalName + "&reference=" + this.reference + "/" + commentid + "&type=like&id=like",
			type: 'get',
			dataType: 'json',
			async:true,
			success: function(jsonresults){
				if(jsonresults.state=="success"){
					var $like = $("*[data-id='" + commentid + "'] .like", self.widget.body);
					var liked = jsonresults.data.content;
					if(liked=="") liked = false;
					$like.attr("data-liked", liked);
					self.i18n();
				} else {
					//bb_labs.publishToRadioChannel("error", "UGC", jsonresults.message);
				}	

			},
		    error: function(ajaxErrorObject){
		    	//bb_labs.publishAjaxError(ajaxErrorObject);
		    }
		});
	} else {
		self.i18n();
	}
};


bb_labs.UGCComments.prototype.i18n	= function(){
	var self = this;
	
	var $likes = $(".like", self.widget.body);
	$likes.each(function(){
		var $like = $(this);
		var liked = $like.attr("data-liked")=="true"?true:false; 
		
		if(liked){
			$like.html(bb_labs.i18n.getResourceFailSafe("unlike"));
		} else {
			$like.html(bb_labs.i18n.getResourceFailSafe("like"));
		}
	});

	
};



