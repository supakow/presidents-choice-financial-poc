bb_labs.FormsBasic_DatePicker = function(oWidget){
    this.widget = oWidget;
    this.init();
};
bb_labs.FormsBasic_DatePicker.prototype 		= {};
bb_labs.FormsBasic_DatePicker.prototype.init	= function(){
    var self = this;
    var date;
    if(this.widget.getPreference("defaultDate") == "") {
        date = new Date();
    } else {
        date = this.formatUserDate(this.widget.getPreference("defaultDate"));
    }
    var displayDate = DateFormat.format.date(date, this.widget.getPreference("dateFormat"));
    var showPastDates = (this.widget.getPreference("showPastDates") === "true");

    var widgetPref = {
        "required": this.widget.getPreference("required"),
        "fieldName": this.widget.id,
        "labelForDatePicker": this.widget.getPreference("labelForDatePicker"),
        "displayDate": displayDate
    };

    var oView  = Mustache.to_html(self.getTemplate("template_date_picker"),widgetPref);
    $(self.widget.body).find("div.bb-labs-forms-basic-date-picker").html(oView);
    $(self.widget.body).find('.bootstrap-date-picker').bootstrapdatepicker({
        onRender: function(date) {
            if (!showPastDates) {
                var nowTemp = new Date();
                var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
                return date.valueOf() < now.valueOf() ? 'disabled' : '';
            }
        }
    }).on("changeDate", function(e){
        var date = new Date(e.date);
        $(e.currentTarget).val(DateFormat.format.date(date, self.widget.getPreference("dateFormat")));
    }).on("hide", function(e){
        //bootstrap datepicker tries to revert date format on hide for some reason, so need to reset it here
        var date = new Date(e.date);
        $(e.currentTarget).val(DateFormat.format.date(date, self.widget.getPreference("dateFormat")));
    });
};

bb_labs.FormsBasic_DatePicker.prototype.getTemplate	= function(name){
    return $(this.widget.body).find('[data-template="'+name+'"]').html();
};

bb_labs.FormsBasic_DatePicker.prototype.formatUserDate = function(date) {
    //make this look like unix timestamp so that date format plugin knows what to do
    return date + "EST01:10:20";
};