bb_labs.UGCBlog = function(oWidget){
	this.widget = oWidget;
	this.reference = "";
	this.blogid = "";
	this.init();
	this.i18n();
};
bb_labs.UGCBlog.prototype 		= {};
bb_labs.UGCBlog.prototype.init	= function(){
	
	var self = this;

	this.reference = this.widget.getPreference("referenceid");
	if((this.reference=="")||(typeof(this.widget.getPreference("referenceid"))=="undefined")) this.reference = "blogs";

	this.blogid = this.widget.getPreference("blogid");
	if((this.blogid=="")||(typeof(this.widget.getPreference("blogid"))=="undefined")) this.blogid = b$.portal.pageName;

	var $widget = $(this.widget.htmlNode);
	
	var $blog_editor = $(".blog-editor", this.widget.body);
	$blog_editor.css("min-height", this.widget.getPreference("minheight"));
	
	if($(".aa-button-preferences", $widget).length>0) { 
		
		$blog_editor.focusout(function(e){
			self.saveBlog(self.blogid, $(this).html());
		});
		
		$blog_editor.focusin(function(e){
			var $toolbar = $(".btn-toolbar", self.widget.body);
			$toolbar.addClass("active");
			$(".close", $toolbar).click(function(){
				$toolbar.removeClass("active");
			});
		});
		
		$blog_editor.wysiwyg({
			  hotKeys: {
			    'ctrl+b meta+b': 'bold',
			    'ctrl+i meta+i': 'italic',
			    'ctrl+u meta+u': 'underline',
			    'ctrl+z meta+z': 'undo',
			    'ctrl+y meta+y meta+shift+z': 'redo'
			  }
		});
	} else {
		$blog_editor.addClass("readonly");
	}
	

	this.loadBlog(this.blogid);
	
	

};

bb_labs.UGCBlog.prototype.loadBlog = function(blogId){
	var self = this;
	
	$.ajax({
		url: b$.portal.portalModel.serverURL + "/proxy?pipe=ugcPipe&widgetid=" + self.widget.id + "&portalname=" + b$.portal.portalName + "&reference=" + this.reference + "&type=blog&id=" + blogId,
		type: 'get',
		dataType: 'json',
		async:true,
		success: function(jsonresults){
			if(jsonresults.state=="success"){
				if(jsonresults.data.content!=""){
					$(".blog-editor", self.widget.body).html(jsonresults.data.content);
					self.i18n();
				}
			} else {
				bb_labs.publishToRadioChannel("error", "UGC", jsonresults.message);
			}	

		},
	    error: function(ajaxErrorObject){
	    	bb_labs.publishAjaxError(ajaxErrorObject);
	    }
	});
};


bb_labs.UGCBlog.prototype.saveBlog = function(blogId, blogContent){
	var self = this;
	
	$.ajax({
		url: b$.portal.portalModel.serverURL + "/proxy?pipe=ugcPipe&widgetid=" + self.widget.id + "&portalname=" + b$.portal.portalName + "&reference=" + this.reference + "&type=blog&id=" + blogId,
		type: 'post',
		data: "ugc_data=" + blogContent,
		dataType: 'json',
		async:true,
		success: function(jsonresults){
			if(jsonresults.state=="success"){
				
			} else {
				bb_labs.publishToRadioChannel("error", "UGC", jsonresults.message);
			}	

		},
	    error: function(ajaxErrorObject){
	    	bb_labs.publishAjaxError(ajaxErrorObject);
	    }
	});

};


bb_labs.UGCBlog.prototype.i18n	= function(){
	var self = this;


	
};



