if (!window.bb_labs) window.bb_labs = {};

bb_labs.MultiAuthManageWidget = (function() {
	var onload = function(oGadget) {
		init(oGadget);

		//$('.divRAW', oGadget.body).show();
		$('.divRAW .close', oGadget.Body).click(function(){
			$('.divRAW', oGadget.body).html("");
		});

		$('.divList .btnadd', oGadget.Body).click(function(){
			addAuth(oGadget);
		});
		
		$('.divEdit .btnsave', oGadget.Body).click(function(){
			saveAuth(oGadget);
		});
		
		$('.divEdit .btncancel', oGadget.Body).click(function(){
			$(".divEdit", oGadget.body).toggle("clip", null, 500);
		});

		
		//What to do in designmode/portal manager
		if(bd!=null){
			if (bd.designMode == 'true') {
				$('.divRAW', oGadget.body).show();
			}
		}
	};
	
	
	var init = function(oGadget) { 
		loadProviderList(oGadget);
		loadList(oGadget);
	};
	
	var loadList = function(oGadget){
		$('.divError', oGadget.body).hide();
		$('.divInfo', oGadget.body).html("Loading list...");
		$('.divInfo', oGadget.body).show();
		$.ajax({
		    url: "/portalserver/proxy?pipe=multiauthconfigPipe&type=list",
		    type:'GET',
		    dataType:'html',
		    async:false,
		    success: function(proxycontent){
				//divList //listBody
				$('.divRAW', oGadget.body).append(proxycontent);
				var jsonobj = $.parseJSON(proxycontent);
				$('.divInfo', oGadget.body).hide();
				buildList(oGadget, jsonobj.records.data);
		    },
		    error: function(error){
				$('.divInfo', oGadget.body).hide();
				$('.divRAW', oGadget.body).append(error.responseText);
				var jsonerrobj = $.parseJSON(error.responseText);
				handleError(oGadget, "loadList", jsonerrobj.message);
		    }
		});	
	}
	
	var addAuth = function(oGadget){
		$('.divError', oGadget.body).hide();
		$(".divEdit", oGadget.body).toggle("clip", null, 500);
	};
	
	var saveAuth = function(oGadget){
		$('.divError', oGadget.body).hide();
		$('.divInfo', oGadget.body).html("Saving...");
		$('.divInfo', oGadget.body).show();

		var authclient = $('#inputAuthClient', oGadget.body).val();
		var username = $('#inputUsername', oGadget.body).val();
		var password = $('#inputPassword', oGadget.body).val();
		
		$.ajax({
		    url: "/portalserver/proxy?pipe=multiauthconfigPipe&type=save&authclientname=" + authclient + "&username=" + username + "&password=" + password,
		    type:'GET',
		    dataType:'html',
		    async:false,
		    success: function(proxycontent){
				//divList //listBody
				$('.divRAW', oGadget.body).append(proxycontent);
				var jsonobj = $.parseJSON(proxycontent);
				$('.divInfo', oGadget.body).hide();
				$(".divEdit", oGadget.body).toggle("clip", null, 500);
				buildList(oGadget, jsonobj.records.data);
		    },
		    error: function(error){
				$('.divInfo', oGadget.body).hide();
				$('.divRAW', oGadget.body).append(error.responseText);
				var jsonerrobj = $.parseJSON(error.responseText);
				handleError(oGadget, "saveAuth", jsonerrobj.message);
		    }
		});
	};

	var deleteAuth = function(oGadget, authclient){
		$('.divError', oGadget.body).hide();
		$('.divInfo', oGadget.body).html("Deleting...");
		$('.divInfo', oGadget.body).show();
		
		$.ajax({
		    url: "/portalserver/proxy?pipe=multiauthconfigPipe&type=delete&authclientname=" + authclient,
		    type:'GET',
		    dataType:'html',
		    async:false,
		    success: function(proxycontent){
				//divList //listBody
				$('.divRAW', oGadget.body).append(proxycontent);
				var jsonobj = $.parseJSON(proxycontent);
				$('.divInfo', oGadget.body).hide();
				buildList(oGadget, jsonobj.records.data);
		    },
		    error: function(error){
				$('.divInfo', oGadget.body).hide();
				$('.divRAW', oGadget.body).append(error.responseText);
				var jsonerrobj = $.parseJSON(error.responseText);
				handleError(oGadget, "deleteAuth", jsonerrobj.message);
		    }
		});
	};
	
	var buildList = function(oGadget, jsonData){
		$('.divList .listBody', oGadget.body).html("");
		for(var i=0;i<jsonData.length;i++){
			var record = jsonData[i];
			var deletebuttonhtml = "<button class=\"btn btn-mini\" id=\"delete" + record[0] + "\" type=\"button\">Delete</button>";
			$('.divList .listBody', oGadget.body).append("<tr><td>" + translateCode(oGadget, record[0]) + "</td><td>" + record[2] + "</td><td>" + deletebuttonhtml + "</td></tr>");
			$('.divList #delete' + record[0], oGadget.body).click(function(){
				deleteAuth(oGadget, record[0]);
			});
		}
	};
	
	
	var translateCode = function(oGadget, value){
		var returnvalue = value;
		$('#inputAuthClient option', oGadget.body).each(function(i){
			if(value==$(this).val()) returnvalue = $(this).text(); 
	    });
		return returnvalue;
	};

	var loadProviderList = function(oGadget){
		$.ajax({
		    url: "/portalserver/proxy?pipe=multiauthconfigPipe&type=config",
		    type:'GET',
		    dataType:'html',
		    async:false,
		    success: function(proxycontent){
				$('.divRAW', oGadget.body).append(proxycontent);
				var jsonobj = $.parseJSON(proxycontent);
				var authclientlist = $('.divEdit #inputAuthClient', oGadget.body)
				for(var i=0;i<jsonobj.records.length;i++){
					authclientlist.append($('<option></option>').val(jsonobj.records[i][0]).html(jsonobj.records[i][1]));
				}
		    },
		    error: function(error){
				$('.divRAW', oGadget.body).append(error.responseText);
				var jsonerrobj = $.parseJSON(error.responseText);
				handleError(oGadget, "loadProviderList", jsonerrobj.message);
		    }
		});	
	}
	
	var handleError = function(oGadget, errorSource, errorMessage){
		var diverror = $('.divError', oGadget.body);
		diverror.html("<strong>" + errorSource + ":</strong> " + errorMessage);
		diverror.show();
	};
	
	//public functions
	return {
		onload: onload
	};

}());