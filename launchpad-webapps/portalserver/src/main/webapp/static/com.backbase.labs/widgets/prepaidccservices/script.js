if (!window.bb_labs) window.bb_labs = {};

bb_labs.PrepaidCCServicesWidget = (function() {
	var onload = function(oGadget) {
		init(oGadget);

		//$('.divRAW', oGadget.body).show();
		$('.divRAW .close', oGadget.Body).click(function(){
			$('.divRAW', oGadget.body).hide();
		});

		//What to do in designmode/portal manager
		if(bd!=null){
			if (bd.designMode == 'true') {
				$('.divRAW', oGadget.body).show();
			}
		}
	};
	
	
	var init = function(oGadget) { 

		var feature =  oGadget.model.getPreference('feature');

		jQuery.ajax({
		    url: "/portalserver/proxy?pipe=prepaidcardservicesPipe&feature=" + feature,
		    type:'GET',
		    dataType:'html',
		    async:false,
		    success: function(proxycontent){
				var div = null;

				$('.divRAW', oGadget.body).append(proxycontent);

				switch(feature){
					case "TransactionHistory":
						div = $('.divTransactionHistory', oGadget.body);
						var jsonthobj = $.parseJSON(proxycontent);
						if(jsonthobj.responseCode=="SUCCESS"){
							var thhtmlrows = "";
							for(var x=0; x<jsonthobj.response.transactions.length;x++){
								thhtmlrows += "<tr><td>" + formatDate(jsonthobj.response.transactions[x].transactionDate) + "</td><td>" + jsonthobj.response.transactions[x].description + "</td><td class='usd'>$" + addCommaToUSD(jsonthobj.response.transactions[x].amount) + "</td></tr>";
							}
							$('.thgridbody', oGadget.body).html(thhtmlrows);
							div.show();
						} else {
							handleError(oGadget, "init", jsonthobj.message);
						}
						break;
					case "DashboardBalances":
						div = $('.divDashboardBalances', oGadget.body);
						var jsondbobj = $.parseJSON(proxycontent);
						if(jsondbobj.success){
							var tablehtmltr = "";
							for(var x=0; x<jsondbobj.cards.length;x++){
								tablehtmltr += "<tr class=\"_rem\"><td>" + jsondbobj.cards[x].name + "</td><td class=\"Money\">" + jsondbobj.cards[x].balance + "</td></tr>";
							}
							tablehtmltr += "<tr class=\"success\"><td>Grand Total</td><td id=\"ajxCardTotal\" class=\"Money\">" + jsondbobj.cardsTotal + "</td></tr>";

							$(".divDashboardBalances .ajxAsOfDate", oGadget.body).html(jsondbobj.date);
							$(".divDashboardBalances .ajxCards tbody", oGadget.body).html(tablehtmltr);
							div.show();
						} else {
							handleError(oGadget, "init", jsondbobj.message);
						}
						break;
					case "ExpensesByCategory":
						div = $('.divExpensesByCategory', oGadget.body);
						var jsondbobj = $.parseJSON(proxycontent);
						if(jsondbobj.success){
							$('.expensescontainer', oGadget.body).highcharts({
					            chart: {
					                plotBackgroundColor: null,
					                plotBorderWidth: null,
					                plotShadow: true
					            },
					            title: null,
					            //title: {
					            //    text: 'Browser market shares at a specific website, 2010'
					            //},
					            tooltip: {
					        	    pointFormat: '<b>{point.percentage}%</b>',
					            	percentageDecimals: 0
					            },
					            plotOptions: {
					                pie: {
					                    allowPointSelect: true,
					                    cursor: 'pointer',
					                    dataLabels: {
					                        enabled: true,
					                        color: '#000000',
					                        connectorColor: '#000000',
					                        formatter: function() {
					                            return '<b>'+ this.point.name +'</b>';
					                        }
					                    }
					                }
					            },
					            series: [{
					                type: 'pie',
					                name: 'Expenses by Category',
					                data: jsondbobj.data
					            }]
					        });
							
							div.show();
						} else {
							handleError(oGadget, "init", jsondbobj.message);
						}
						break;
					case "Profile":
						div = $('.divProfile', oGadget.body);
						var jsonprofileobj = $.parseJSON(proxycontent);
						if(jsonprofileobj.success){
							$('.divProfile .loginname', oGadget.body).html(jsonprofileobj.loginData.firstName + " " + jsonprofileobj.loginData.lastName);
							$('.divProfile .lastlogindate', oGadget.body).html(jsonprofileobj.loginData.lastLogin);
							div.show();
						} else {
							handleError(oGadget, "init", jsonprofileobj.message);
						}
						break;
					case "Summary":
						div = $('.divSummary', oGadget.body);
						var jsonprofileobj = $.parseJSON(proxycontent);
						if(jsonprofileobj.success){
							$('.divSummary .cashoncards', oGadget.body).html("Cash on cards: $" + addCommaToUSD(jsonprofileobj.loginData.currentCardBalance));
							$('.divSummary .loginuser', oGadget.body).html("You are logged in as " + b$.portal.loggedInUserId);
							$('.divSummary .cardholder', oGadget.body).html("Cardholder:" + jsonprofileobj.loginData.firstName + " " + jsonprofileobj.loginData.lastName);
							$('.divSummary .lastlogindate', oGadget.body).html("Last login:" +  formatDate(jsonprofileobj.loginData.lastLogin));
							div.show();
						} else {
							handleError(oGadget, "init", jsonprofileobj.message);
						}
						break;
				}
		
				$('.divLoading', oGadget.body).hide();
		    },
		    error: function(error){
				$('.divLoading', oGadget.body).hide();
				$('.divRAW', oGadget.body).append(error.responseText);
				handleError(oGadget, "init", error.responseText);
		    }
		});
	};

	
	var addCommaToUSD = function(amountString){
		var stringtoconvert = amountString.toString();
		return stringtoconvert.substring(0, stringtoconvert.length-2) + "," + stringtoconvert.substring(stringtoconvert.length-2);
	};
	
	var formatDate = function(dateString){
		var myDate_string = dateString.substring(0, 16);
		myDate_string = myDate_string.replace("T"," ");
		return myDate_string;
	};

	var handleError = function(oGadget, errorSource, errorMessage){
		var diverror = $('.divError', oGadget.body);
		diverror.html("<strong>" + errorSource + ":</strong> " + errorMessage);
		diverror.show();
	};
	
	//public functions
	return {
		onload: onload
	};

}());