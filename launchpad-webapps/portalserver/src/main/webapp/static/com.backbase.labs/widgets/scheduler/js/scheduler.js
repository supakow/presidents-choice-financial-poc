bb_labs.Scheduler 				= function(oWidget){
	this.widget = oWidget;
	this.init();
};
bb_labs.Scheduler.prototype 		= {};
bb_labs.Scheduler.prototype.init	= function(){
	
	var self = this;
	var today = new Date();
	
	$(".scheduler", this.widget.body).kendoScheduler({
        date: today,
        startTime: new Date(today.getYear() + "/" + today.getMonth() + "/" + today.getDate() + " 07:00 AM"),
        height: "28em",
        views: [
            { type: "day" },
            { type: "week",  selected: true, selectedDateFormat: "{0:ddd,MMM dd,yyyy} - {1:ddd,MMM dd,yyyy}" },
            "month",
            { type: "agenda", selectedDateFormat: "{0:ddd, M/dd/yyyy} - {1:ddd, M/dd/yyyy}" },
        ],
        mobile: "phone",
        timezone: "Etc/UTC",
        dataSource: {
            batch: true,
            transport: {
                read: {
                    url: self.widget.getPreference("readurl"),
                    dataType: "jsonp"
                },
                update: {
                    url: self.widget.getPreference("updateurl"),
                    dataType: "jsonp"
                },
                create: {
                    url: self.widget.getPreference("createurl"),
                    dataType: "jsonp"
                },
                destroy: {
                    url: self.widget.getPreference("destroyurl"),
                    dataType: "jsonp"
                },
                parameterMap: function(options, operation) {
                    if (operation !== "read" && options.models) {
                        return {models: kendo.stringify(options.models)};
                    }
                }
            },
            schema: {
                model: {
                    id: "meetingID",
                    fields: {
                        meetingID: { from: "MeetingID", type: "number" },
                        title: { from: "Title", defaultValue: "No title", validation: { required: true } },
                        start: { type: "date", from: "Start" },
                        end: { type: "date", from: "End" },
                        startTimezone: { from: "StartTimezone" },
                        endTimezone: { from: "EndTimezone" },
                        description: { from: "Description" },
                        recurrenceId: { from: "RecurrenceID" },
                        recurrenceRule: { from: "RecurrenceRule" },
                        recurrenceException: { from: "RecurrenceException" },
                        roomId: { from: "RoomID", nullable: true },
                        atendees: { from: "Atendees", nullable: true },
                        isAllDay: { type: "boolean", from: "IsAllDay" }
                    }
                }
            }
        },
        resources: [
        {
            field: "roomId",
            dataSource: [
                { text: "Meeting Room 101", value: 1, color: "#6eb3fa" },
                { text: "Meeting Room 201", value: 2, color: "#f58a8a" }
            ],
            title: "Room"
        },
        {
            field: "atendees",
            dataSource: [
                { text: "Alex", value: 1, color: "#f8a398" },
                { text: "Bob", value: 2, color: "#51a0ed" },
                { text: "Charlie", value: 3, color: "#56ca85" }
            ],
            multiple: true,
            title: "Atendees"
        }
        ]
    });
	
};

