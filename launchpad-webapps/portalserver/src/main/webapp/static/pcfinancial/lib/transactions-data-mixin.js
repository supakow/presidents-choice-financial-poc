/*global b$, gadgets, console */
define([
    "jquery"
], function($) {
    'use strict';

    var transactionTypeMap = {
        'Bill Payment': 'fc9c910b-bd5d-4705-9817-8fa81d4fd930',
        'Cash Withdrawl': '95bca6f4-d332-4899-b290-a20d406f1496',
        'Online Transfer': '4ad5d251-62d0-4ace-830d-6a96cd77605a',
        'Payment Debit Card': '9ab60271-b6bd-42e8-b53a-1e5118a0622f',
        'Payment Credit Card': '9ab60271-b6bd-42e8-b53a-1e5118a0622f',
        'Salary' : '9ab60271-b6bd-42e8-b53a-1e5118a0622f'
    },
    categoryColorMap = {
        '00cc9919-ba0c-4702-917b-1fba4c256b4d' : '#00a651', // uncategorized
        '4ad5d251-62d0-4ace-830d-6a96cd77605a' : '#662d91', //Transfer
        'fc9c910b-bd5d-4705-9817-8fa81d4fd930' : '#bf1e2e', //Payment
        '95bca6f4-d332-4899-b290-a20d406f1496' : '#939598', //Utilities
        '9ab60271-b6bd-42e8-b53a-1e5118a0622f' : '#9f1f63'//fee
    };
    var changeCurrency = function(){

        },
        getAllAccountTransaction = function(accountsModel, transactionsModel, categoryModel){
            var dfd = $.Deferred(),
                accounts = accountsModel.accounts,
                allTransactions = [], count = 1,
                i, l = accounts.length,
                thenFn = function(){
                    //make copy
                    allTransactions = allTransactions.concat(transactionsModel.transactions.slice(0));
                    if(count === l){
                        // console.log(allTransactions);
                        allTransactions.sort(function(a,b){
                            if(a.bookingDateTime > b.bookingDateTime){ return -1; }
                            else if(a.bookingDateTime < b.bookingDateTime){ return 1; }
                            else{ return 0; }
                        });
                        //changeColorForCategory(categoryModel);
                        changeTransactionCategory(allTransactions);
                        dfd.resolve(allTransactions);
                        //$scope.allTransactions = allTransactions;
                    }else{
                        count++;
                    }
                };
            for(i=0; i<l; i++){
                transactionsModel.loadTransactions(accounts[i]).then(thenFn);
            }

            return dfd.promise();
        },
        changeColorForCategory = function(categoryModel){
            var i,l,c;
            for(i = 0, l = categoryModel.categories.length; i < l; i++){
                c = categoryModel.categories[i];
                if(categoryColorMap[c.id]){
                    categoryModel.categories[i].color = categoryColorMap[c.id];
                }
            }
        },
        changeTransactionCategory = function(allTransactions){
            var i,l,c;
            for(i = 0, l = allTransactions.length; i < l; i++){
                c = allTransactions[i];
                allTransactions[i].transactionCurrency = 'USD';
                allTransactions[i].categoryId = c.categoryId === 'a1fae863-01e9-4f73-81a6-8d4e26c00f9d' && transactionTypeMap[c.transactionType] ?
                    transactionTypeMap[c.transactionType] : '00cc9919-ba0c-4702-917b-1fba4c256b4d';
            }
        };

    return {
        getAllAccountTransaction: getAllAccountTransaction,
        changeColorForCategory : changeColorForCategory,
        changeTransactionCategory : changeTransactionCategory
    };
});
