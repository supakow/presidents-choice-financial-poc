(function($, gadgets, TweenMax, TimelineMax, Ease){

    $(document).ready(function(){
        var count = 0;

        gadgets.pubsub.subscribe("widget.ready", function() {
            //$('body').find('.pcf-loading-div').remove();
            //tl.staggerFrom(".lp-transactions", 0.5, {scale:0.1, autoAlpha:0, ease: Ease.easeOut}, 0.03);
            var allReady = $('.pcf-transactions, .pcf-spending').length + 1;
            count++;
            if(count >= allReady){
                var widgets = $(".bp-widget, .lp-chrome-launcher-slide, .lp-chrome-launcher-tab, .widget-animate-on")
                                    .filter(function() {
                                        var $this = $(this);
                                        return !$this.is( "[data-pid*='widget-navbar-advanced'], [data-pid*='CatalogBrowser'],[data-pid*='pcf-pc-plus'], .widget-animate-off" );
                                    }).get();

                var tl = new TimelineMax({
                    autoRemoveChildren: true,
                    onComplete : function(){
                        gadgets.pubsub.unsubscribe("widget.ready");
                    },
                    align: 'sequence',
                    tweens : [
                        //fade out the loading cover
                        TweenMax.to(".pcf-loading-div", 0.5, {opacity: 0, ease: Ease.easeOut, onComplete: function(){ $('.pcf-loading-div').remove(); }})
                    ]
                });

                if(deviceType === 'desktop') {
                    tl.tweens = [
                        //zoom in the widgets
                        TweenMax.staggerFrom(widgets, 0.3, { scale:0.1, autoAlpha:0, ease: Ease.easeOut,
                            onCompleteParams:["{self}"], onComplete: function(self){ $(self.target).attr('style', '');  }}
                        )
                    ];
                }

            }
        });


        // FIXME: put this in the global js file
        gadgets.pubsub.subscribe("launchpad-retail.activeContextChanged", function(obj){
            if(obj.newActiveContext === 'PCF PC Plus' || obj.newActiveContext === 'Transactions'){
                $('body').addClass('PCF-modal-on');
                $('#lp-launcher-overlay').hide();
            }else if(obj.newActiveContext === ""){
                $('body').removeClass('PCF-modal-on');
                $('div[data-pid^="launcher-container"]').css("padding-bottom","0px");
            }
        });


        //disabled the auto select on the title
        if($('html').hasClass('mobile') || $('html').hasClass('tablet')){
            $('.pcf-widget-title').each(function() {
                this.onselectstart = function() { return false; };
                this.unselectable = "on";
                $(this).css('-moz-user-select', 'none');
                $(this).css('-webkit-user-select', 'none');
            });
        }

        var clickTimer, isShow = false;
        $(document)
            .on('mousedown.deleteIcon, touchstart.deleteIcon', '.pcf-widget-title',function(){
                if(!isShow){
                    clickTimer = setTimeout(function(){
                        isShow = true;
                        $('.pcf-delete-icon').closest('[data-pid]').addClass('pcf-edit-on');
                    }, 1000);
                }
            })
            .on('mouseup.deleteIcon, touchend.deleteIcon', function(){
                clearTimeout(clickTimer);

                if(isShow){
                    setTimeout(function(){
                        $(document).on('click.deleteIcon, tap.deleteIcon', '.bp-widget',function(){
                            //delay for the first mouse down/up conflict
                            if(isShow){
                                $('.pcf-edit-on').removeClass('pcf-edit-on');
                                isShow = false;
                                $(document).off('click.deleteIcon, tap.deleteIcon');
                            }
                        });
                    }, 500);
                }

            });



    });


})(jQuery, gadgets, TweenMax, TimelineMax, Ease);