budgets = function(oWidget) {
    this.widget = oWidget;
    this.init();
    this.viewMore();
};
budgets.prototype = {};
budgets.prototype.init = function() {

    // stub data
    var current_budgets = [{
            'desc': 'Auto and Transport',
            'image': '/portalserver/static/pcfinancial/widgets/pcf-budgets/images/auto.png',
            'budget_value': 1010,
            'current_value': 676
        }, {
            'desc': 'Overall Budget',
            'image': '',
            'budget_value': 5646,
            'current_value': 3854
        }, {
            'desc': 'Gifts and Donations',
            'image': '/portalserver/static/pcfinancial/widgets/pcf-budgets/images/gift.png',
            'budget_value': 50,
            'current_value': 35
        }, {
            'desc': 'Home',
            'image': '/portalserver/static/pcfinancial/widgets/pcf-budgets/images/home.png',
            'budget_value': 1537,
            'current_value': 1435
        }],
        elementViewed = 0;

    // this should be in a separate file
    function isElementInViewport(el) {
        if (el instanceof jQuery) {
            el = el[0];
        }

        var rect = el.getBoundingClientRect();

        return (
            rect.top >= 0 &&
            rect.left >= 0 &&
            (rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) + rect.height) && rect.right <= (window.innerWidth || document.documentElement.clientWidth)
            );
    }

    function fireIfElementVisible(el, callback) {
        return function() {
            if (isElementInViewport(el)) {
                if (elementViewed === 0) {
                    elementViewed += 1;
                    callback();
                }
            }
        };
    }
    // really should move the above

    $(window).on('DOMContentLoaded load resize scroll', fireIfElementVisible($('.budgets'), callback));


    $.each(current_budgets, function(index, value) {
        $.get('/portalserver/static/pcfinancial/widgets/pcf-budgets/partials/budget.html', function(data) {
            $('#budgets').append(data);
            $('#budgets .budget').last().addClass(current_budgets[index].desc.replace(/ /g, '_').toLowerCase());
            $('#budgets .budget_desc').last().text(current_budgets[index].desc).css({
                'background-image': 'url(' + current_budgets[index].image + ')'
            });
            $('#budgets .current_value').last().append(current_budgets[index].current_value);
            $('#budgets .budget_value').last().append(current_budgets[index].budget_value);
            $('#budgets .budget_completed').last().css({
                'width': '0'
            });

            var budget_completion = current_budgets[index].current_value / current_budgets[index].budget_value;
            if(budget_completion >= 0.9) {
                $('#budgets .budget_completed').last().css({
                    'background-color': '#EE2E24'
                });
            } else if(budget_completion >= 0.7) {
                $('#budgets .budget_completed').last().css({
                    'background-color': '#FDD01C'
                });
            } else {
                $('#budgets .budget_completed').last().css({
                    'background-color': '#39B54A'
                });
            }

            if(index === current_budgets.length - 1){
                $('.overall_budget').prependTo('#budgets');
                if(deviceType === ('mobile' || 'tablet')) {
                    callback();
                }
            }
        });


    });

    function callback() {
        $.each(current_budgets, function(index, value) {
            $('#budgets .' + current_budgets[index].desc.replace(/ /g, '_').toLowerCase()).delay((index + 1) * 500).queue(function() {
                $('#budgets .budget_completed:eq(' + index + ')').css({
                    'transition': 'width ' + (1 + current_budgets[index].current_value / current_budgets[index].budget_value) + 's',
                    'width': (current_budgets[index].current_value / current_budgets[index].budget_value * 100) + '%'
                });
            });
        });
    }
};

budgets.prototype.viewMore = function(){
    $('.manage_budgets', this.widget.body).on('click', function(){
        var pcPlusWidget = $('.lp-chrome-launcher-tab-hidden')
            .filter('[data-pid*="LabsMD"]')
            .find('a[href*="#LabsMD"]');
        pcPlusWidget.click();
    });
};
