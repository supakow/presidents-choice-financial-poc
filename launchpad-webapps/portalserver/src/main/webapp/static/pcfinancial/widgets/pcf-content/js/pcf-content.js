/**
 * Copyright © 2011 Backbase B.V.
 * Content template html description:
 *      1: data-parentId: Unique Id for the content path.
 *      2: data-propertyId: the content MetaData property Id at the CMIS document.
 *      3: contenteditable: set the tag to be editable.
 *      4: All image src attr should use {{xxxx.content}}. Do not use src="{{xxxx.content}}"
 *      5: All content stream should use {{xxxx.content}}.
 * Mustache syntax requirement:
 *      1: Do not contain numbers on the value of data-parentId.
 *          Bad:  {{topImage2.content}}
 *          Good: {{topImage.content}}
 *      2: All variables are HTML-escaped by default. If you want to render unescaped HTML, use the triple mustache: {{{name}}}.
 */



(function (){
    // Add Namespace
    if(!window.examples) window.examples = {};
    if(!window.examples.widgets) window.examples.widgets = {};
    var iceAdvanceTemplateWidget = {
        onLoad: function(oGadget) {
            // Extend widget in design mode
            if(bd && bd.iceCommon) {
                //the ckEditor configuration object, will overwrite the global config
                oGadget.ckEditorConfig = {
                    // specified to certain fields in the widget
                    // FORMAT: data-propertyId : { /* ckeditor config */}
                    'bb:title' : {
                        allowedContent : '!*',
                        extraAllowedContent : '!*'
                    },
                    'bb:subTitle' : {
                        allowedContent : '!*',
                        extraAllowedContent : '!*'
                    }
                };

                bd.iceCommon.onLoad(oGadget);
                //example of the local event
                oGadget.addEventListener('bdDrop.drop', function(e) {
                    var items = e.detail.info.helper.bdDragData.fileDataArray;
                    if(items && items.length && items[0].type.indexOf('folder')>-1){
                        e.stopPropagation();
                        console.log(items[0]);
                    }
                });
/*                oGadget.addEventListener('bdDrop.enter', function(e) {
                    console.log('enter into widget');
                });
                oGadget.addEventListener('bdDrop.leave', function(e) {
                    console.log('leave widget');
                });
                oGadget.addEventListener('bdDrop.move', function(e) {
                    console.log('move on widget');
                });
                oGadget.addEventListener('bdDrop.activate', function(e) {
                    console.log('activate widget');
                });
                oGadget.addEventListener('bdDrop.deactivate', function(e) {
                    console.log('deactivate widget');
                });*/
            }
        }
    };
    // Register
    window.examples.widgets.iceAdvanceTemplateWidget = iceAdvanceTemplateWidget;
})();
