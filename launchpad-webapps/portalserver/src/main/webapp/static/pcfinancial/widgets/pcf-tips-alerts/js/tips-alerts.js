tips_alerts = function(oWidget) {
    this.widget = oWidget;
    this.init();
};
tips_alerts.prototype = {};
tips_alerts.prototype.init = function() {

    // stub data
    var current_tips_alerts = [{
        'desc': 'Mom\'s Birthday',
        'image': 'gift.jpg',
        'text': 'is Aug. 15. Do you want to send a gift?'
    }, {
        'desc': 'Bill Reminder',
        'image': 'caution.jpg',
        'text': 'Your Car Payment of $543.76 is due Aug. 18.'
    }, {
        'desc': 'Large Deposit',
        'image': 'credit.jpg',
        'text': 'Your $5,531.67 (payroll deposit, Thrillworks) to Chequing (2345) is now available.'
    }, {
        'desc': 'Over Budget',
        'image': 'alert.jpg',
        'text': 'You are close to your budget limit for the month.'
    }];
    elementViewed = 0;

    // this should be in a separate file
    function isElementInViewport(el) {
        if (el instanceof jQuery) {
            el = el[0];
        }

        var rect = el.getBoundingClientRect();

        return (
            rect.top >= 0 &&
            rect.left >= 0 &&
            (rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) + rect.height) && rect.right <= (window.innerWidth || document.documentElement.clientWidth)
        );
    }

    function fireIfElementVisible(el, callback) {
        return function() {
            if (isElementInViewport(el)) {
                if (elementViewed === 0) {
                    elementViewed += 1;
                    callback();
                }
            }
        };
    }
    // really should move the above

    $(window).on('DOMContentLoaded load resize scroll', fireIfElementVisible($('.tips_alerts'), callback));

    $.each(current_tips_alerts, function(index, value) {
        $.get('/portalserver/static/pcfinancial/widgets/pcf-tips-alerts/partials/tips-alerts.html', function(data) {
            $('#tips_alerts').append(data);

            $('#tips_alerts .tip_alert').last().addClass(current_tips_alerts[index].desc.replace(' ', '_').replace('\'', '').toLowerCase());
            $('#tips_alerts .tip_alert_image').last().css({
                'background-image': 'url(/portalserver/static/pcfinancial/widgets/pcf-tips-alerts/images/' + current_tips_alerts[index].image + ')'
            });
            $('#tips_alerts .tip_alert_desc').last().text(current_tips_alerts[index].desc);
            $('#tips_alerts .tip_alert_text').last().text(current_tips_alerts[index].text);
        });
    });

    function callback() {
        $.each(current_tips_alerts, function(index, value) {
            var current_tip_alert = $('#tips_alerts .tip_alert.' + current_tips_alerts[index].desc.replace(' ', '_').replace('\'', '').toLowerCase());
            current_tip_alert.delay((index + 1) * 100).queue(function() {
                $(current_tip_alert).find('.tip_alert_desc').animate({
                    marginLeft: 0
                }, {
                    step: function(position, fx) {
                        $(this).css('margin-left', position + 'px');
                    },
                    duration: 1500
                }).parent().find('.tip_alert_text').animate({
                    marginLeft: 0
                }, {
                    step: function(position, fx) {
                        $(this).css('margin-left', position + 'px');
                    },
                    duration: 2500
                });
            });


        });
    }
};