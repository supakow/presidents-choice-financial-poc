reward_points = function(oWidget) {
    this.widget = oWidget;
    this.init();
    this.viewMore();
};
reward_points.prototype = {};
reward_points.prototype.init = function() {
    var graph_layers = ['default', 'earned', 'redeemable'],
        elementViewed = 0;

    // this should be in a separate file
    function isElementInViewport(el) {
        if (el instanceof jQuery) {
            el = el[0];
        }

        var rect = el.getBoundingClientRect();

        return (
            rect.top >= 0 &&
            rect.left >= 0 &&
            (rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) + rect.height) && rect.right <= (window.innerWidth || document.documentElement.clientWidth)
        );
    }

    function fireIfElementVisible(el, callback) {
        return function() {
            if (isElementInViewport(el)) {
                if (elementViewed === 0) {
                    elementViewed += 1;
                    callback();
                }
            }
        };
    }
    // really should move the above

    $(window).on('DOMContentLoaded load resize scroll', fireIfElementVisible($('.reward_points'), callback));
    $.each(graph_layers, function(index) {
        $.get('/portalserver/static/pcfinancial/widgets/pcf-reward-points/partials/chart.html', function(data) {
            $('.reward_points_redemption_chart').append(data);
            $('.reward_points_redemption_chart_layer').last().addClass(graph_layers[index]);
        });
    });

    function callback() {
        if($.browser.safari) {
            $('.speedometer').animate({
                opacity: 45
            }, {
                step: function(now, fx) {
                    $(this).css('transform', 'rotate(' + now + 'deg)');
                },
                duration: 1
            });
        }
        $.each(graph_layers, function(index) {
            switch (graph_layers[index]) {
                case 'default':
                    $('.reward_points_redemption_chart_layer.' + graph_layers[index]).delay(500).queue(function() {
                        $('.reward_points_redemption_chart_layer.' + graph_layers[index]).find('.left').animate({
                            opacity: 180
                        }, {
                            step: function(now, fx) {
                                $(this).css('transform', 'rotate(' + now + 'deg)');
                            },
                            duration: 1000
                        });
                    });
                    break;
                case 'earned':
                    $('.reward_points_redemption_chart_layer.' + graph_layers[index]).delay(750).queue(function() {
                        $('.reward_points_redemption_chart_layer.' + graph_layers[index]).find('.left').animate({
                            opacity: 125
                        }, {
                            step: function(now, fx) {
                                $(this).css('transform', 'rotate(' + now + 'deg)');
                            },
                            duration: 1750
                        });
                    });
                    break;
                case 'redeemable':
                    $('.reward_points_redemption_chart_layer.' + graph_layers[index]).delay(1000).queue(function() {
                        $('.reward_points_redemption_chart_layer.' + graph_layers[index]).find('.left').animate({
                            opacity: 90
                        }, {
                            step: function(now, fx) {
                                $(this).css('transform', 'rotate(' + now + 'deg)');
                            },
                            duration: 2500
                        });
                    });
                    break;
            }
        });
        $('.speedometer').css({'opacity':'1'});
    }
};
reward_points.prototype.viewMore = function(){
    $('.pc-plus-view-more', this.widget.body).on('click', function(){
        var pcPlusWidget = $('.lp-chrome-launcher-tab-hidden')
                    .filter('[data-pid*="pcf-pc-plus"]')
                        .find('a[href*="#pcf-pc-plus"]');
        pcPlusWidget.click();
    });
};
