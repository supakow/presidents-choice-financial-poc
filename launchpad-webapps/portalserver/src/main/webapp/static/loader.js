jQuery(document).ready(function ($) {
    window.pageUuid = '${item.uuid}';
    b$.portal.startup("main", function() {

        if(lp.portalExtensions.length) {
            $.each(lp.portalExtensions, function(index, fn) {
                fn();
            });
        }

        //Trigger portalLoaded event and kick off ADM for portal client extensions
        $(document).trigger("portalLoaded");

        //initalize page modules
        require([
            "launchpad/pages/launchpad-page/modules/session-timeout",
            "launchpad/pages/launchpad-page/modules/server-client-push",
            "launchpad/pages/launchpad-page/modules/skip-links"
        ], function(sessionTimeout, serverClientPubSubController, skipLinks) {

            //initialize session timeout functionality
            sessionTimeout.getInstance().initializeSessionTimeoutHandling();

            //open connection to back end events
            serverClientPubSubController.getInstance().init("/event");

            // add skip links
            skipLinks.initializeSkipLinks();
        });
    });

    if(navigator.userAgent.match(/iPad/i) != null){
        init();
    }

    function touchHandler(event)
    {
        var target = $(event.target).parents(".bp-ui-dragGrip").first();

        if(target.length === 0){
            return false;
        }else{
            if($(target).hasClass("pm-icon2") == false){
                return false;
            }
        }

        var touches = event.changedTouches,
            first = touches[0],
            type = "";
        switch(event.type)
        {
            case "touchstart": type = "mousedown"; break;
            case "touchmove":  type="mousemove"; break;
            case "touchend":   type="mouseup"; break;
            default: return;
        }

        var simulatedEvent = document.createEvent("MouseEvent");
        simulatedEvent.initMouseEvent(type, true, true, window, 1,
            first.screenX, first.screenY,
            first.clientX, first.clientY, false,
            false, false, false, 0/*left*/, null);

        first.target.dispatchEvent(simulatedEvent);
        event.preventDefault();
    };

    function init()
    {
        document.addEventListener("touchstart", touchHandler, true);
        document.addEventListener("touchmove", touchHandler, true);
        document.addEventListener("touchend", touchHandler, true);
        document.addEventListener("touchcancel", touchHandler, true);


        var viewportmeta = document.querySelector('meta[name="viewport"]');
        if (viewportmeta) {
            viewportmeta.content = 'width=device-width, minimum-scale=.25, maximum-scale=1.6, initial-scale=.8,user-scalable=0';
            document.body.addEventListener('gesturestart', function () {
                viewportmeta.content = 'width=device-width, minimum-scale=0.25, maximum-scale=1.6,user-scalable=0';
            }, false);
        }
    };


});