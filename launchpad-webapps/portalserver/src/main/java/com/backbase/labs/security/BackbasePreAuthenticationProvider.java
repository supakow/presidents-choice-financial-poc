package com.backbase.labs.security;

import com.backbase.labs.common.AuthUtilities;
import com.backbase.portal.foundation.business.service.GroupBusinessService;
import com.backbase.portal.foundation.business.service.UserBusinessService;
import com.backbase.portal.foundation.domain.model.Group;
import com.backbase.portal.foundation.domain.model.User;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.mapping.GrantedAuthoritiesMapper;
import org.springframework.security.core.authority.mapping.NullAuthoritiesMapper;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

import java.util.*;

import org.slf4j.LoggerFactory;


public class BackbasePreAuthenticationProvider implements AuthenticationProvider{

    static ch.qos.logback.classic.Logger log = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger("com.backbase.crowd.BackbaseAuthenticationProvider");
	private GrantedAuthoritiesMapper authoritiesMapper = new NullAuthoritiesMapper();
	AuthUtilities bbauthutils = null;
	
	public Authentication authenticate(Authentication authentication)
			throws AuthenticationException {

		PreAuthenticatedAuthenticationToken preauth = (PreAuthenticatedAuthenticationToken)authentication;
		String username = preauth.getName();
		String password = "password";
		
		boolean isauthenticatedagainstothersystem = true;
		if(isauthenticatedagainstothersystem){

			//We can set a list of groups the user belong to here
			String usergroups = "";
			Boolean isadmin = true;
			Boolean issys2sys = false;
			Boolean isuser = true;

			//Get the groups from Backbase
			List<Group> groups = bbauthutils.validateGroupsInBackbase(bbauthutils.getBackbaseSystemGroups(usergroups,isadmin,issys2sys,isuser) );

			//Get the user from Backbase
			User user = bbauthutils.validateUserInBackbase(username, password, groups);
			
			//Set user attributes in Backbase
			//user.getPropertyDefinitions().put("custContext", new UserPropertyDefinition("custContext", new StringPropertyValue(custContext)));
			
			//Report a successful authentication attempt
		    PreAuthenticatedAuthenticationToken result = new PreAuthenticatedAuthenticationToken(user, authentication.getCredentials(), user.getAuthorities());
		    result.setDetails(authentication.getDetails());
		    return result;

		} else {
			System.out.println("Authentication Failed: " + username);
			throw new BadCredentialsException("Refused authentication for " + username);
		}
		
	}

	
	public boolean supports(Class<? extends Object> arg0) {
		return true;
	}

	public void setBackbaseAuthUtils(AuthUtilities backbaseAuthUtils) {
		this.bbauthutils = backbaseAuthUtils;
	}

}
