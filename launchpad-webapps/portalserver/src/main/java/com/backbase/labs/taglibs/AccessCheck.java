package com.backbase.labs.taglibs;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.springframework.security.core.context.SecurityContextHolder;

import com.backbase.portal.foundation.domain.model.Group;
import com.backbase.portal.foundation.domain.model.User;


//http://tomcat.apache.org/tomcat-5.5-doc/jspapi/javax/servlet/jsp/tagext/Tag.html

public class AccessCheck extends TagSupport {

	private static final long serialVersionUID = 1L;

	private Set<String> groups = new HashSet<String>();
	private boolean debug = false;
	
	/*
	EVAL_BODY_INCLUDE Evaluate body into existing out stream.
	EVAL_PAGE Continue evaluating the page.
	SKIP_BODY Skip body evaluation.
	SKIP_PAGE Skip the rest of the page.
	*/
	public int doStartTag() throws JspException {
		return EVAL_BODY_INCLUDE;
	}

	public int doEndTag() {
		return EVAL_PAGE;
	}

	public boolean hasAccess(){
		boolean returnvalue = false;
		if(SecurityContextHolder.getContext().getAuthentication().getPrincipal()!=null){
			User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal(); 
			List<Group> usergroups = user.getGroups();
			for(Group group : usergroups){
				if(this.groups.contains(group.getName())){
					returnvalue = true;
				}
			}
		}
		return returnvalue;
	}
	
	public void setGroups(String groups) {
		this.groups = new HashSet<String>(Arrays.asList(groups.split(","))) ;
	}
	public void setDebug(Boolean degug) {
		this.debug = debug;
	}
}
