package com.backbase.labs.ptc.provider.impl;
 
import java.io.IOException;
import java.util.List;

import net.bican.wordpress.Page;
import net.bican.wordpress.PageDefinition;
import net.bican.wordpress.PostAndPageStatus;
import net.bican.wordpress.Wordpress;
 


import com.backbase.labs.common.HttpClient;
import com.backbase.portal.ptc.config.element.DataProviderConfig;
import com.backbase.portal.ptc.context.MutableProxyContext;
import com.backbase.portal.ptc.provider.DataProvider;
import com.backbase.portal.ptc.request.ProxyRequest;
import com.backbase.portal.ptc.response.MutableProxyResponse;
 
public class WordPressDataProvider implements DataProvider {
 
        @Override
        public MutableProxyResponse executeRequest(DataProviderConfig dataProviderConfig,
                        MutableProxyContext mutableProxyContext, ProxyRequest proxyRequest) throws IOException {
                
            MutableProxyResponse mpr = new MutableProxyResponse();
            
            //We use the current logged in user
            String primaryusername = mutableProxyContext.getInternalParameterValue("__auth_username");
 
                String standardjsonresponse = "{\"state\": \"#state#\",\"message\": \"#message#\",\"data\": #data#}";

            boolean debug = false;
            if(mutableProxyContext.getInternalParameterValue("debug")!=null) debug = Boolean.parseBoolean(mutableProxyContext.getInternalParameterValue("debug"));
            
            try {
 
            	//Read the type of action to perform
            	String action = mutableProxyContext.getInternalParameterValue("action");
            	if(action==null||action.equals("")) throw new Exception("action parameter required");
            	
            	//Get the word press rpc settings
            	String xmlrpcurl = dataProviderConfig.getParamValue("xmlrpcurl");
            	String username = dataProviderConfig.getParamValue("username");
            	String password = dataProviderConfig.getParamValue("password");
            	
            	//Instanciate wordpress rpc client (https://code.google.com/p/wordpress-java)
            	Wordpress wp = new Wordpress(username, password, xmlrpcurl);
                String jsondata = "{}";
                        
                //Create a object array of posts
                if(action.equals("recentposts")){
                    int count = 10;
                    if(mutableProxyContext.getInternalParameterValue("count")!=null) count = Integer.parseInt(mutableProxyContext.getInternalParameterValue("count"));
                	
                    jsondata = "";
					List<Page> recentPosts = wp.getRecentPosts(count);
					for (Page page : recentPosts) {
						if(debug) System.out.println(page.getPostid() + ":" + page.getTitle());
						if(!jsondata.equals("")) jsondata += ",";
						jsondata += jsonFromPage(page);
					}
					jsondata = "[" + jsondata + "]";
                }
                

                if(action.equals("pagelist")){
                    jsondata = "";
					List<PageDefinition> pagelist = wp.getPageList();
					for (PageDefinition pagedefinition : pagelist) {
						if(debug) System.out.println(pagedefinition.getPage_parent_id() + ":" + pagedefinition.getPage_title());
						if(!jsondata.equals("")) jsondata += ",";
						jsondata += jsonFromPageDefinition(pagedefinition);
					}
					jsondata = "[" + jsondata + "]";
                }

                if(action.equals("page")){
                    int pageid = 0;
                    if(mutableProxyContext.getInternalParameterValue("id")!=null) pageid = Integer.parseInt(mutableProxyContext.getInternalParameterValue("id"));
                    if(pageid==0) throw new Exception("Page id required (id)");

                    Page page = wp.getPage(pageid);
                    jsondata = jsonFromPage(page);
                }

                if(action.equals("post")){
                    int postid = 0;
                    if(mutableProxyContext.getInternalParameterValue("id")!=null) postid = Integer.parseInt(mutableProxyContext.getInternalParameterValue("id"));
                    if(postid==0) throw new Exception("Post id required (id)");
                    Page page = wp.getPost(postid);
                    jsondata = jsonFromPage(page);
                }
                
                if(action.equals("statuslist")){
                    List<PostAndPageStatus> statuses = wp.getPageStatusList();
					for (PostAndPageStatus status : statuses) {
						if(debug) System.out.println(status.getStatus());
						if(!jsondata.equals("")) jsondata += ",";
						jsondata += "{";
						jsondata += "\"status\" : \"" + status.getStatus() + "\",";
						jsondata += "\"description\" : \"" + status.getDescription() + "\"";
						jsondata += "}";
					}
				}                
                
                if(action.equals("create")){
                	String title = mutableProxyContext.getInternalParameterValue("title");
                	String description = mutableProxyContext.getInternalParameterValue("description");
                	if(title==null||description==null||title.equals("")||description.equals("")){
                		throw new Exception("title and description mandatory");
                	}
                	
                	Page post = new Page();
                	post.setTitle(title);
                	post.setDescription(description);
                	if(mutableProxyContext.getInternalParameterValue("excerpt")!=null){
                		post.setExcerpt(mutableProxyContext.getInternalParameterValue("excerpt"));
                	}
                	wp.newPost(post, true);
                    jsondata = jsonFromPage(post);
				}                
                
                if(action.equals("delete")){
                    int pageid = 0;
                    if(mutableProxyContext.getInternalParameterValue("id")!=null) pageid = Integer.parseInt(mutableProxyContext.getInternalParameterValue("id"));
                    if(pageid==0) throw new Exception("Post/Page id required (id)");

                    wp.deletePost(pageid, "true");
                }                
                
                if(debug) System.out.println("jsondata: " + jsondata);
                
                //Create the final json response
                String jsonresponse = standardjsonresponse.replace("#state#", "success");
                jsonresponse = jsonresponse.replace("#message#", "");
                jsonresponse = jsonresponse.replace("#data#", jsondata);
                mpr.addContentTypeHeader("application/json", "utf-8");
                mpr.setStatusCode(200);
                mpr.setStatusText("ok");
                mpr.setBody(jsonresponse);
                    
            } catch (Exception e) {
                String jsonresponse = standardjsonresponse.replace("#state#", "error");
                if(debug){
                    jsonresponse = jsonresponse.replace("#message#", e.getMessage() + "\n" + org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(e));
                } else {
                    jsonresponse = jsonresponse.replace("#message#", "Wordpress: " + e.getMessage());
                }
                jsonresponse = jsonresponse.replace("#data#", "{}");
                mpr.addContentTypeHeader("application/json", "utf-8");
                mpr.setStatusCode(500);
                mpr.setStatusText(e.getLocalizedMessage());
                mpr.setBody(jsonresponse);
            }
                
            return mpr;
        }
 
        private String jsonFromPageDefinition(PageDefinition pagedefinition){
        	String json = "{";
        	json += "\"Page_parent_id\" : \"" + pagedefinition.getPage_parent_id() + "\",";
        	json += "\"Page_title\" : \"" + pagedefinition.getPage_title() + "\",";
        	json += "\"Date_created_gmt\" : \"" + pagedefinition.getDate_created_gmt() + "\",";
        	json += "\"DateCreated\" : \"" + pagedefinition.getDateCreated() + "\",";
        	json += "\"StringHeader\" : \"" + pagedefinition.getStringHeader() + "\"";
        	json += "}";
        	return json;
        }
        
        private String jsonFromPage(Page page){
        	String json = "{";
        	json += "\"Categories\" : {},";
        	//page.getCategories();
        	//page.getCustom_fields();
            json += "\"Date_created_gmt\" : \"" + page.getDate_created_gmt() + "\",";
        	json += "\"DateCreated\" : \"" + page.getDateCreated() + "\",";
        	json += "\"Description\" : \"" + escape(page.getDescription()) + "\",";
        	json += "\"Excerpt\" : \"" + page.getExcerpt() + "\",";
        	json += "\"Link\" : \"" + page.getLink() + "\",";
        	json += "\"Mt_allow_comments\" : \"" + page.getMt_allow_comments() + "\",";
        	json += "\"Mt_allow_pings\" : \"" + page.getMt_allow_pings() + "\",";
        	json += "\"Mt_keywords\" : \"" + page.getMt_keywords() + "\",";
        	json += "\"Mt_text_more\" : \"" + page.getMt_text_more() + "\",";
        	json += "\"Page_id\" : \"" + page.getPage_id() + "\",";
        	json += "\"Page_status\" : \"" + page.getPage_status() + "\",";
        	json += "\"PermaLink\" : \"" + page.getPermaLink() + "\",";
        	json += "\"Post_status\" : \"" + page.getPost_status() + "\",";
        	json += "\"Post_type\" : \"" + page.getPost_type() + "\",";
        	json += "\"Postid\" : \"" + page.getPostid() + "\",";
        	json += "\"StringHeader\" : \"" + page.getStringHeader() + "\",";
        	json += "\"Title\" : \"" + page.getTitle() + "\",";
        	json += "\"Userid\" : \"" + page.getUserid() + "\",";
        	json += "\"Wp_author\" : \"" + page.getWp_author() + "\",";
        	json += "\"Wp_author_display_name\" : \"" + page.getWp_author_display_name() + "\",";
        	json += "\"Wp_author_id\" : \"" + page.getWp_author_id() + "\",";
        	json += "\"Wp_page_order\" : \"" + page.getWp_page_order() + "\",";
        	json += "\"Wp_page_parent_id\" : \"" + page.getWp_page_parent_id() + "\",";
        	json += "\"Wp_page_parent_title\" : \"" + page.getWp_page_parent_title() + "\",";
        	json += "\"Wp_password\" : \"" + page.getWp_password() + "\",";
        	json += "\"Wp_slug\" : \"" + page.getWp_slug() + "\"";
        	json += "}";
        	return json;
        }
 
        private String escape(String string) {
           String returnvalue = string;
           returnvalue = returnvalue.replace("\"", "&#34;");
           returnvalue = returnvalue.replace("\n", "<br/>");
           return returnvalue;
        }
        
}
 