package com.backbase.labs.services.moneydesktop;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Camel processor handling the /moneydesktop/{widgetname} instance endpoint.
 */
@Component
public class SSOWidgetProcessor implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {
        Message in = exchange.getIn();
        Message out = exchange.getOut();
        Object header = in.getHeader(Exchange.HTTP_METHOD);
        String backbase_user = (String) in.getHeader("BackbaseUser");
        if ("GET".equals(header)) {
            handleGet(backbase_user, in, out);
        } else {
            setErrorMessage(out, "GET are supported on moneybuilder sso-widgets instance endpoint", 405);
        }
    }

    private void handleGet(String backbaseUser, Message in, Message out) {
    	String widgetname = (String) in.getHeader("widgetname");
    	out.setBody("{\"success\":true}");
    }

    protected void setErrorMessage(Message out, String message, int code) {
    	out.setBody("{\"error\": \"" + message + "\"}");
    	out.setHeader(Exchange.HTTP_RESPONSE_CODE, code);
    }

}
