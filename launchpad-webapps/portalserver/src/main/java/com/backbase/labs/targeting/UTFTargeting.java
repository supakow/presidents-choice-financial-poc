package com.backbase.labs.targeting;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.backbase.portal.targeting.connectorframework.content.contexts.definition.ContextCollector;
import com.backbase.portal.targeting.connectorframework.content.contexts.definition.PossibleValue;
import com.backbase.portal.targeting.connectorframework.content.contexts.definition.ResultEntries;
import com.backbase.portal.targeting.connectorframework.content.contexts.definition.SegmentDefinition;
import com.backbase.portal.targeting.connectorframework.content.contexts.definition.SelectorDefinition;
import com.backbase.portal.targeting.rulesengine.type.RuleEngineTypes;

public class UTFTargeting extends ContextCollector {

	static String contextId = "utf";
	static String description = "UTF 8 Collector";
	static String iconPath = ""; 
	static String label = "UTF";
	public UTFTargeting() {
		super(contextId, description, iconPath, label);
	}
	
	@Override
	public ResultEntries executeTask(Map<String, String> requestMap, ResultEntries resultEntries) {
		
		resultEntries.addSegmentEntry("Sete Léguas");
		resultEntries.addSelectorEntry("brand", "Dupé");
		return resultEntries;
	}
	
	
	@Override
	//public List<SegmentDefinition> defineSegments(String arg0) {
	public List<SegmentDefinition> defineSegments(String arg0, Map<String, String> arg1) {
		
		List<SegmentDefinition> listsegements = new ArrayList<SegmentDefinition>();
		
		listsegements.add(new SegmentDefinition("Havaianas", "Havaianas"));
		listsegements.add(new SegmentDefinition("Dupé", "Dupé"));
		listsegements.add(new SegmentDefinition("Sete Léguas", "Sete Léguas"));
		
		return listsegements;
	}
	@Override
	//public List<SelectorDefinition> defineSelectors(String arg0) {
	public List<SelectorDefinition> defineSelectors(String arg0, Map<String, String> arg1) {

		List<SelectorDefinition> arrayofoptions = new ArrayList<SelectorDefinition>();
		
		SelectorDefinition utfselector = new SelectorDefinition("brand", RuleEngineTypes.STRING, "Brand");
		PossibleValue[] listofvalues = new PossibleValue[] { new PossibleValue("Havaianas"), new PossibleValue("Dupé")};
		utfselector.addPossibleValues(listofvalues);
		
		arrayofoptions.add(utfselector);
		return arrayofoptions;
	}


}
