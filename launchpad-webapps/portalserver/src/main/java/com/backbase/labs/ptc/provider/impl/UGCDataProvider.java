package com.backbase.labs.ptc.provider.impl;
 
import java.io.IOException;
import java.util.HashMap;

import org.json.simple.parser.JSONParser;

import com.backbase.labs.common.UGCClient;
import com.backbase.portal.ptc.config.element.DataProviderConfig;
import com.backbase.portal.ptc.context.MutableProxyContext;
import com.backbase.portal.ptc.provider.DataProvider;
import com.backbase.portal.ptc.request.ProxyRequest;
import com.backbase.portal.ptc.response.MutableProxyResponse;
 
public class UGCDataProvider implements DataProvider {
 
	@Override
	public MutableProxyResponse executeRequest(DataProviderConfig dataProviderConfig,
		MutableProxyContext mutableProxyContext, ProxyRequest proxyRequest) throws IOException {
		            
		MutableProxyResponse mpr = new MutableProxyResponse();
		        
		//We use the current logged in user
		String primaryusername = mutableProxyContext.getInternalParameterValue("__auth_username");
		String standardjsonresponse = "{\"state\": \"#state#\",\"message\": \"#message#\",\"data\": #data#}";
		
		boolean debug = false;
		if(mutableProxyContext.getInternalParameterValue("debug")!=null) debug = Boolean.parseBoolean(mutableProxyContext.getInternalParameterValue("debug"));
		
		try {
			String status = "success";
			String jsondata = "{}";

			//Get UGC PTC Settings
			String ugc_type = dataProviderConfig.getParamValue("type");
			String ugc_path = dataProviderConfig.getParamValue("path");

			//Check for anonymous
			boolean allowanonymous = Boolean.parseBoolean(dataProviderConfig.getParamValue("allowanonymous"));
			if(mutableProxyContext.getInternalParameterValue("widget_allowanonymous")!=null) allowanonymous = Boolean.parseBoolean(mutableProxyContext.getInternalParameterValue("widget_allowanonymous"));
			if((!allowanonymous)&&(primaryusername==null)) throw new Exception("Anonymous action not allowed");
			
			//check if default type overwritten
			if(mutableProxyContext.getInternalParameterValue("type")!=null) ugc_type = mutableProxyContext.getInternalParameterValue("type");

			//Check for anonymous
			boolean shared = Boolean.parseBoolean(dataProviderConfig.getParamValue("shared"));
			if(mutableProxyContext.getInternalParameterValue("widget_sharedblog")!=null) shared = Boolean.parseBoolean(mutableProxyContext.getInternalParameterValue("widget_sharedblog"));

			
			String portalname = "";
			String reference = "";
			String identity = "anonymous";

			//Obtain mandatory / location variables
			if(mutableProxyContext.getInternalParameterValue("portalname")!=null) { 
				portalname = mutableProxyContext.getInternalParameterValue("portalname");
			} else {
				throw new Exception("Portal name not provided.  (portalname)");
			}
			if(mutableProxyContext.getInternalParameterValue("reference")!=null) reference = mutableProxyContext.getInternalParameterValue("reference");
			if(primaryusername!=null) identity = primaryusername;
			
			
			UGCClient ugcclient = new UGCClient(ugc_path, identity, portalname, reference, shared);
			
			String ugc_id = mutableProxyContext.getInternalParameterValue("id");
			
			if(proxyRequest.getMethod().toLowerCase().equals("post")){
				if(proxyRequest.getPostParameterValue("ugc_data")==null) throw new Exception("UGC Data not supplied (ugc_data)");
				String ugc_content = proxyRequest.getPostParameterValue("ugc_data"); //this will be the post body
				if(ugc_id==null){
					ugc_id = ugcclient.createEntry(ugc_type, ugc_content);
				} else {
					ugcclient.updateEntry(ugc_type, ugc_id, ugc_content);
				}
			} else if(proxyRequest.getMethod().toLowerCase().equals("delete")){
				if(ugc_id==null) throw new Exception("ID not specified (id)");
				boolean success = ugcclient.deleteEntry(ugc_type, ugc_id);
			} else {
				if(ugc_id==null){
					String entriesjson = "";
					HashMap<String,HashMap<String,String>> entries;;
					if(mutableProxyContext.getInternalParameterValue("filter")!=null) { 
						if(mutableProxyContext.getInternalParameterValue("filter")=="user"){
							entries = ugcclient.listContentEntriesByCurrentUser(ugc_type);
						} else {
							entries = ugcclient.listContentEntriesByUser(ugc_type);
						}
					} else {
						entries = ugcclient.listContentEntriesByUser(ugc_type);
					}
					for(String user: entries.keySet()){
						HashMap<String,String> userentries = entries.get(user);
						for(String key: userentries.keySet()){
							if(entriesjson!="") entriesjson += ",";
							entriesjson += createJSONEntry(portalname, reference, user, key, userentries.get(key), identity);
						}
					}
					
					jsondata = "[" + entriesjson + "]";
					
				} else {
					String ugc_content = ugcclient.getContent(ugc_type, ugc_id);
					jsondata = createJSONEntry(portalname, reference, identity, ugc_id, ugc_content, identity);
				}
			}
			
			//Create the final json response
			String jsonresponse = standardjsonresponse.replace("#state#", status);
			jsonresponse = jsonresponse.replace("#message#", "");
			jsonresponse = jsonresponse.replace("#data#", jsondata);
			mpr.addContentTypeHeader("application/json", "utf-8");
			mpr.setStatusCode(200);
			mpr.setStatusText("ok");
			mpr.setBody(jsonresponse);
			
		} catch (Exception e) {
			String jsonresponse = standardjsonresponse.replace("#state#", "error");
			if(debug){
				jsonresponse = jsonresponse.replace("#message#", e.getMessage() + "\n" + org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(e));
			} else {
				jsonresponse = jsonresponse.replace("#message#", e.getMessage());
			}
			jsonresponse = jsonresponse.replace("#data#", "{}");
			mpr.addContentTypeHeader("application/json", "utf-8");
			mpr.setStatusCode(500);
			mpr.setStatusText(e.getLocalizedMessage());
			mpr.setBody(jsonresponse);
		}

		return mpr;
	}
	
	private String createJSONEntry(String portalname, String reference, String identity, String id, String content, String currentIdentity){
		String json = "";
		json += "{";
		json += "\"portal\":\"" + portalname + "\",";
		json += "\"reference\":\"" + reference + "\",";
		json += "\"identity\":\"" + identity + "\",";
		if(identity.equals(currentIdentity)){
			json += "\"allowedit\":" + true + ",";
		} else {
			json += "\"allowedit\":" + false + ",";
		}
		json += "\"id\":\"" + id + "\",";	
		json += "\"content\":\"" + removeControlCharacters(content) + "\"";
		json +=	"}";
		return json;
	}
	
	/*
	 The JSON definition tells us that you cannot have literal control characters in string literals, you must use an escape sequence such as \\b, \\r, \\n, or \\uXXXX where XXXX is a hex code for a Unicode "code point" (character).
	 */
	private String removeControlCharacters(String input){
		String response = input;
		response = response.replace("\n", "");
		response = response.replace("\b", "");
		response = response.replace("\r", "");
		response = response.replace("\"", "\\\"");
		return response;
	}
}
 