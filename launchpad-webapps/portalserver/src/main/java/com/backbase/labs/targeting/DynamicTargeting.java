package com.backbase.labs.targeting;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;











import javax.naming.InitialContext;

import com.backbase.labs.common.PortalBusinessServices;
import com.backbase.labs.common.XmlUtilities;
import com.backbase.portal.foundation.business.service.GroupBusinessService;
import com.backbase.portal.foundation.business.service.PortalBusinessService;
import com.backbase.portal.foundation.business.service.UserBusinessService;
import com.backbase.portal.foundation.commons.exceptions.ItemNotFoundException;
import com.backbase.portal.foundation.domain.conceptual.UserPropertyDefinition;
import com.backbase.portal.foundation.domain.model.Filter;
import com.backbase.portal.foundation.domain.model.Group;
import com.backbase.portal.foundation.domain.model.Groups;
import com.backbase.portal.foundation.domain.model.Sorter;
import com.backbase.portal.foundation.domain.model.User;
import com.backbase.portal.targeting.connectorframework.content.contexts.definition.ContextCollector;
import com.backbase.portal.targeting.connectorframework.content.contexts.definition.PossibleValue;
import com.backbase.portal.targeting.connectorframework.content.contexts.definition.ResultEntries;
import com.backbase.portal.targeting.connectorframework.content.contexts.definition.SegmentDefinition;
import com.backbase.portal.targeting.connectorframework.content.contexts.definition.SelectorDefinition;
import com.backbase.portal.targeting.rulesengine.type.RuleEngineTypes;

import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public class DynamicTargeting extends ContextCollector {

	private XmlUtilities xmlutils;
	
	
	/*
	 * This builds a button for selecting the SelectorDefinitions
	 */
	public DynamicTargeting() {
		super("dynamicCollector", "Dynamic Portal User Information Collector", "$(contextRoot)/static/backbase.com.2012.darts/media/contexts/targeting-context-crm.png", "User Information");
		
		try {
			if(xmlutils == null){
				InitialContext initialContext = new InitialContext();  
				String filelocation = (String) initialContext.lookup("java:comp/env/targeting/config");
				xmlutils = new XmlUtilities(filelocation, false);
			}
		} catch (Exception e) {
			xmlutils = null;
			System.out.println("[DynamicTargeting] Unable to load targeting config");
		}
		
	}
	

	
	/*
	 * Segments can be setup based on predefined rules.  
	 * Rules that is basically determined outside the scope of the business user
	 */ 
	@Override
	public List<SegmentDefinition> defineSegments(String portalName, Map<String, String> context) {
	    List<SegmentDefinition> segments = new ArrayList<SegmentDefinition>();
	    return segments;
	}
	
	
	/*
	 * Selector Definitions creates the fields the business user can see in targeting properties screen in the UI
	 */
	@Override
	public List<SelectorDefinition> defineSelectors(String portalName, Map<String, String> context) {
    	//System.out.println("definineSelectors: " + portalName);

    	List<SelectorDefinition> selectors = new ArrayList<SelectorDefinition>();

    	try{
	    	selectors.add(createSelectorDefinition("user.size"));
	    	selectors.add(createSelectorDefinition("user.states"));
	    	selectors.add(createSelectorDefinition("user.gender"));
	    	selectors.add(createSelectorDefinition("user.amount"));
	    	selectors.add(createSelectorDefinition("user.b2b"));
    	} catch(Exception ex){
    		ex.printStackTrace();
    	}
    	
	    //Adding all groups as selector
	    try{
		    Groups groups = PortalBusinessServices.INSTANCE.groupBusinessService.getGroups(Sorter.NULL, Filter.NULL);
		    
		    SelectorDefinition selectordefinition = new SelectorDefinition("backbase.group", RuleEngineTypes.ENUM, "Group Segmentation");
			PossibleValue[] possiblevalues = new PossibleValue[groups.size()];
			for(int i=0; i<groups.size();i++){
				Group group = groups.get(i);
				PossibleValue possiblevalue = new PossibleValue(group.getId().toString(), group.getName());
	    		possiblevalues[i] = possiblevalue;
			}
			selectordefinition.addPossibleValues(possiblevalues);
		    selectors.add(selectordefinition);

	    } catch(Exception ex){
	    	ex.printStackTrace();
	    }

    	
	    return selectors;
	}

	/*
	 * This automatically assemble the settings for a selector defintion
	 * based on the targeting configuration file.
	 */
	private SelectorDefinition createSelectorDefinition(String selectorId){
		
		RuleEngineTypes ruleenginetype = RuleEngineTypes.STRING;
		String description = selectorId;
		
	    SelectorDefinition selectordefinition = new SelectorDefinition(selectorId, ruleenginetype, description);

	    if(xmlutils!=null){
			try{
				Node fieldtypenode = xmlutils.performXPathAsNode("//fieldTypes/field[@id='" + selectorId + "']");
				if(fieldtypenode!=null){
					String fieldtype = XmlUtilities.performXPathAsString(fieldtypenode, "./@type").toLowerCase();
					if(fieldtype.equals("list")){
						ruleenginetype = RuleEngineTypes.ENUM;
					} else if(fieldtype.equals("integer")){
						ruleenginetype = RuleEngineTypes.INTEGER;
					} else if(fieldtype.equals("boolean")){
						ruleenginetype = RuleEngineTypes.BOOLEAN;
					} else if(fieldtype.equals("range")){
						ruleenginetype = RuleEngineTypes.RANGE;						
					}

					String descriptionstring = XmlUtilities.performXPathAsTextContent(fieldtypenode, ".");
					if(!descriptionstring.isEmpty()) description = descriptionstring;

				    selectordefinition = new SelectorDefinition(selectorId, ruleenginetype, description);
				    
				    if(ruleenginetype.equals(RuleEngineTypes.ENUM)){
				    	NodeList listitems = xmlutils.performXPathAsNodeList("//lists/list[@id='" + selectorId + "']/item");
						PossibleValue[] possiblevalues = new PossibleValue[listitems.getLength()];
						for(int i=0; i<listitems.getLength(); i++){
				    		Node listnode = listitems.item(i);
				    		String id = XmlUtilities.performXPathAsString(listnode, "./@id");
				    		String label = XmlUtilities.performXPathAsTextContent(listnode, ".");
				    		PossibleValue possiblevalue = new PossibleValue(id, label);
				    		possiblevalues[i] = possiblevalue;
				    	}
			    		selectordefinition.addPossibleValues(possiblevalues);
				    }

				    if(ruleenginetype.equals(RuleEngineTypes.RANGE)){
				    	Node rangeitem = xmlutils.performXPathAsNode("//ranges/range[@id='" + selectorId + "']");
				    	if(rangeitem!=null){
				    		String min = XmlUtilities.performXPathAsString(rangeitem, "./@min");
				    		String max = XmlUtilities.performXPathAsTextContent(rangeitem, "./@max");
				    		String step = XmlUtilities.performXPathAsTextContent(rangeitem, "./@step");
				    		selectordefinition.setMax(Integer.parseInt(max));
				    		selectordefinition.setMin(Integer.parseInt(min));
				    		selectordefinition.setStep(Integer.parseInt(step));
				    	}
				    }
				}
				
			} catch(Exception ex){
				
			}
		}
		
	    
	    return selectordefinition;
		
	}
	
	
	/*
	 * This executes for each user(non-Javadoc)
	 * The data collected at login is mapped to the current targeting sessions
	 */
	@Override
	public ResultEntries executeTask(Map<String, String> requestMap, ResultEntries resultEntries) {

		String userName = (String)requestMap.get("session.authentication.username");
    	//System.out.println("executeTask: " + userName);

    	try {
			//Get the user from Backbase sessions
	    	User user = PortalBusinessServices.INSTANCE.userBusinessService.getUser(userName);
			//Get all the properties collected during login
	    	Map<String, UserPropertyDefinition> properties = user.getPropertyDefinitions();
			//Set all these properties as values for targeting
	    	for(String key: properties.keySet()){
				resultEntries.addSelectorEntry(key, getUserProperty(properties, key, ""));
			}
	    	
	    	//Add the groups selection
	    	List<Group> groups = user.getGroups();
	    	for(Group group: groups){
	    		resultEntries.addSelectorEntry("backbase.group", group.getId().toString());
	    	}
			
	    } catch (ItemNotFoundException e1) {
		}
	    return resultEntries;
	}
	
	
	
	/*
	 * A safe way to retreive a users property, to make sure its not null etc.
	 */
	private String getUserProperty(Map<String, UserPropertyDefinition> userProperties, String propertyName, String defaultValue){
		String returnvalue = defaultValue;
		if(userProperties.containsKey(propertyName)){
			returnvalue = userProperties.get(propertyName).getValue().getInternalValue();
		}
		System.out.println(propertyName + ":" + returnvalue);
		return returnvalue;
	}
}
