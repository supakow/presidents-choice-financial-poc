package com.backbase.labs.common;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashMap;
import org.htmlcleaner.CleanerProperties;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.PrettyHtmlSerializer;
import org.htmlcleaner.TagNode; 



public class ClipperClient {
	HttpClient httpclient = null;
	
	String loginurl = "";
	String username = "";
	String password = "";
	
	HashMap<String,String> loginpostparams = new HashMap<String,String>();

	String proxyurl = "";
	int proxyport = 80;
	String proxypassword = "";
	String proxyusername = "";

	String binaryextensions = ".css,.js,.gif,.jpg,.png,.jpeg";
	String homepageurl = "";
	
	public ClipperClient(){
		httpclient = new HttpClient();
	}
	
	public ClipperClient(HttpClient httpClient){
		httpclient = httpClient;
	}
	
	public ClipperClient(String loginUrl, String username, String password) throws Exception{
		this.loginurl = loginUrl;
		this.username = username;
		this.password = password;
		
		httpclient = new HttpClient();
		try {
			httpclient.authenticate(this.loginurl, this.username, this.password);
		} catch (Exception e) {
			throw new Exception("Unable to create HTTP Client");
		}
	}

	public ClipperClient(String loginUrl, String username, String password, boolean userFormLogin) throws Exception{
		this.loginurl = loginUrl;
		this.username = username;
		this.password = password;
		loginpostparams = new HashMap<String,String>();
		loginpostparams.put("password", password);
		loginpostparams.put("username", username);
		
		httpclient = new HttpClient();
		try {
			httpclient.authenticate(this.loginurl, loginpostparams);
		} catch (Exception e) {
			throw new Exception("Unable to create HTTP Client");
		}
	}
	
	public ClipperClient(String loginUrl, HashMap<String,String> postParameters) throws Exception{
		this.loginurl = loginUrl;
		this.loginpostparams = postParameters;
		
		httpclient = new HttpClient();
		try {
			httpclient.authenticate(this.loginurl, loginpostparams);
		} catch (Exception e) {
			throw new Exception("Unable to create HTTP Client");
		}
	}
	
	public ClipperClient(String loginUrl, String username, String password, String proxyUrl, int proxyPort, String proxyUsername, String proxyPassword) throws Exception{
		this.loginurl = loginUrl;
		this.username = username;
		this.password = password;
		this.proxyurl = proxyUrl;
		this.proxyport = proxyPort;
		this.proxyusername = proxyUsername;
		this.proxypassword = proxyPassword;
		
		httpclient = new HttpClient(this.proxyurl, this.proxyport, this.proxyusername, this.proxypassword);
		try {
			httpclient.authenticate(this.loginurl, this.username, this.password);
		} catch (Exception e) {
			throw new Exception("Unable to create HTTP Client");
		}
	}
	
	public ClipperClient(String loginUrl, String username, String password, boolean useFormLogin, String proxyUrl, int proxyPort, String proxyUsername, String proxyPassword) throws Exception{
		this.loginurl = loginUrl;
		this.username = username;
		this.password = password;
		loginpostparams = new HashMap<String,String>();
		loginpostparams.put("password", password);
		loginpostparams.put("username", username);
		this.proxyurl = proxyUrl;
		this.proxyport = proxyPort;
		this.proxyusername = proxyUsername;
		this.proxypassword = proxyPassword;
		
		httpclient = new HttpClient(this.proxyurl, this.proxyport, this.proxyusername, this.proxypassword);
		try {
			if(useFormLogin){
				httpclient.authenticate(this.loginurl, loginpostparams);
			} else {
				httpclient.authenticate(this.loginurl, this.username, this.password);
			}
		} catch (Exception e) {
			throw new Exception("Unable to create HTTP Client");
		}
	}
	
	public ClipperClient(String loginUrl, HashMap<String,String> postParameters, String proxyUrl, int proxyPort, String proxyUsername, String proxyPassword) throws Exception{
		this.loginurl = loginUrl;
		this.loginpostparams = postParameters;
		this.proxyurl = proxyUrl;
		this.proxyport = proxyPort;
		this.proxyusername = proxyUsername;
		this.proxypassword = proxyPassword;

		httpclient = new HttpClient(this.proxyurl, this.proxyport, this.proxyusername, this.proxypassword);
		try {
			httpclient.authenticate(this.loginurl, this.loginpostparams);
		} catch (Exception e) {
			throw new Exception("Unable to create HTTP Client");
		}
	}
	
	//No security, but has proxy
	public ClipperClient(String proxyUrl, int proxyPort, String proxyUsername, String proxyPassword) throws Exception{
		this.proxyurl = proxyUrl;
		this.proxyport = proxyPort;
		this.proxyusername = proxyUsername;
		this.proxypassword = proxyPassword;

		httpclient = new HttpClient(this.proxyurl, this.proxyport, this.proxyusername, this.proxypassword);
	}

	

	public String performStringClipping(String targetUrl) throws Exception{
		return httpclient.getHTMLHttpResponse(targetUrl);
	}
	
	public String performStringClipping(String targetUrl, HashMap<String, String> postParameters) throws Exception{
		return httpclient.postHTMLHttpResponse(targetUrl, null, postParameters);
	}

	
	public String resolveHTML(String html, String contextPath, String baseUrl){
		String proxyurl = HttpClient.getProxyURLPrefix(contextPath, baseUrl);
		return HttpClient.resolveHTML(html, proxyurl);
	}
	
	public InputStream resolveBinary(String targetUrl) throws Exception{
		return new ByteArrayInputStream(httpclient.getBinaryHttpResponse(targetUrl));
	}
	
	public boolean isUrlRequestBinary(String targetUrl){
		return HttpClient.isUrlRequestBinary(targetUrl, this.binaryextensions);
	}
	
	public String performXPATH(String html, String xpath){
		String responsehtml = html;
		try {
	        HtmlCleaner cleaner = new HtmlCleaner();
	        CleanerProperties props = cleaner.getProperties();
	        props.setAllowHtmlInsideAttributes(true);
	        props.setAllowMultiWordAttributes(true);
	        props.setRecognizeUnicodeChars(true);
	        props.setOmitXmlDeclaration(true);
            
	        PrettyHtmlSerializer serializer = new PrettyHtmlSerializer(props);
            
	        TagNode node = cleaner.clean(html);
	        Object[] content_nodes = node.evaluateXPath(xpath);
	        if (content_nodes.length > 0) {
	        	responsehtml = "";
	        	for(int i=0; i<content_nodes.length; i++){
		            TagNode content_node = (TagNode) content_nodes[i];
		            responsehtml += serializer.getAsString(content_node, "UTF-8", true);
	        	}
	        }
	        
	        return responsehtml;
	        
		} catch (Exception e) {
			return "Exception Issue: "+e.toString();
		}
	}



	/* GETTERS SETTERS */
	public String getBinaryextensions() {
		return binaryextensions;
	}

	public void setBinaryextensions(String binaryextensions) {
		this.binaryextensions = binaryextensions;
	}

	public String getHomepageurl() {
		return homepageurl;
	}

	public void setHomepageurl(String homepageurl) {
		this.homepageurl = homepageurl;
	}
	
}
