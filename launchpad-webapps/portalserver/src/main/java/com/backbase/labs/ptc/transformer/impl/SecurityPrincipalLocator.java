package com.backbase.labs.ptc.transformer.impl;

import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import com.backbase.portal.ptc.context.MutableProxyContext;
import com.backbase.portal.ptc.request.MutableProxyRequest;
import com.backbase.portal.ptc.transform.request.AbstractRequestTransformer;


public class SecurityPrincipalLocator extends AbstractRequestTransformer {

    private static final String SECURITY_PRINCIPAL = "__auth_username";

    public void transform(MutableProxyContext mutableProxyContext, MutableProxyRequest mutableProxyRequest) {
        SecurityContext context = SecurityContextHolder.getContext();
        if (context != null) {
            attachPrincipal(mutableProxyContext, context);
        }
    }

    private void attachPrincipal(MutableProxyContext mutableProxyContext, SecurityContext context) {
        String username = context.getAuthentication().getName();
        System.out.println("Security Principal: " + username);
        mutableProxyContext.getInternalParameters().put(SECURITY_PRINCIPAL, new String[]{username});
    }
}
