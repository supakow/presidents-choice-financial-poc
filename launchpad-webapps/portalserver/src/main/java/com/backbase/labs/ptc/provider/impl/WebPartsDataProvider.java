package com.backbase.labs.ptc.provider.impl;
 
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpResponse;

import com.backbase.labs.common.ClipperClient;
import com.backbase.labs.common.HttpClient;
import com.backbase.portal.ptc.config.element.DataProviderConfig;
import com.backbase.portal.ptc.context.MutableProxyContext;
import com.backbase.portal.ptc.provider.DataProvider;
import com.backbase.portal.ptc.request.ProxyRequest;
import com.backbase.portal.ptc.response.MutableProxyResponse;
 
public class WebPartsDataProvider implements DataProvider {
 
	@Override
	public MutableProxyResponse executeRequest(DataProviderConfig dataProviderConfig,
		MutableProxyContext mutableProxyContext, ProxyRequest proxyRequest) throws IOException {
		            
		MutableProxyResponse mpr = new MutableProxyResponse();
		        
		//We use the current logged in user
		String primaryusername = mutableProxyContext.getInternalParameterValue("__auth_username");
		
		boolean debug = false;
		if(mutableProxyContext.getInternalParameterValue("debug")!=null) debug = Boolean.parseBoolean(mutableProxyContext.getInternalParameterValue("debug"));
		
		try {
			String targeturl = mutableProxyContext.getInternalParameterValue("url");
			String xpath = mutableProxyContext.getInternalParameterValue("xpath");
			String webpart = mutableProxyContext.getInternalParameterValue("webpart");
			String contexturl = "/portalserver/proxy?pipe=webpartPipe";
			if((targeturl==null)||(targeturl.equals(""))) throw new Exception("Webpart page url not specified");
			if(webpart!=null) {
				contexturl += "&webpart=" + webpart;
				if(targeturl.indexOf("?")>-1){ 
					targeturl += "&webpart=" + webpart;
				} else {
					targeturl += "?webpart=" + webpart;
				}
			}


			
			String htmlresponse = "";
			String username = dataProviderConfig.getParamValue("username");
			String password = dataProviderConfig.getParamValue("password");
			
			//Get the http client
			HttpClient client = new HttpClient();
			client.setCredentials(username, password);

			//perform the HTML clipping
			ClipperClient clipper = new ClipperClient(client);
			
			if(proxyRequest.getMethod().toLowerCase().equals("get")){
				htmlresponse = clipper.performStringClipping(targeturl);
			} else {
				HashMap<String, String> clipperparams = new HashMap<String, String>();
				Map<String, String[]> postparams = proxyRequest.getPostParameters();
				for(String key: postparams.keySet()){
					clipperparams.put(key, proxyRequest.getPostParameterValue(key));
				}
				htmlresponse = clipper.performStringClipping(targeturl, clipperparams);
			}

			//Resolve relative URL's to post back to target page
			URL baseurl = new URL(targeturl);
			htmlresponse = clipper.resolveHTML(htmlresponse, contexturl, baseurl.getProtocol() + "://" + baseurl.getAuthority());
			
			//Resolve XPATH if specified
			if((xpath!=null)&&(!xpath.equals(""))&&(!xpath.equals("//"))){
				htmlresponse = clipper.performXPATH(htmlresponse, xpath);
			}
			
			mpr.addContentTypeHeader("text/html", "utf-8");
			mpr.setStatusCode(200);
			mpr.setStatusText("ok");
			mpr.setBody(htmlresponse);
			
		} catch (Exception e) {
			mpr.addContentTypeHeader("text/html", "utf-8");
			mpr.setStatusCode(500);
			mpr.setStatusText(e.getLocalizedMessage());
			if(debug){
				mpr.setBody(e.getMessage() + "\n" + org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(e));
			} else {
				mpr.setBody(e.getMessage());
			}
		}

		return mpr;
	}
}
 