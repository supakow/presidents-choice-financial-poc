package com.backbase.labs.ptc.provider.impl;
 
import java.io.IOException;
import java.util.HashMap;

import com.backbase.labs.i18n.i18nContentServices;
import com.backbase.labs.i18n.i18nInterface;
import com.backbase.labs.i18n.i18nResourceProperties;
import com.backbase.portal.ptc.config.element.DataProviderConfig;
import com.backbase.portal.ptc.context.MutableProxyContext;
import com.backbase.portal.ptc.provider.DataProvider;
import com.backbase.portal.ptc.request.ProxyRequest;
import com.backbase.portal.ptc.response.MutableProxyResponse;
 
public class i18nDataProvider implements DataProvider {
 
	@Override
	public MutableProxyResponse executeRequest(DataProviderConfig dataProviderConfig,
		MutableProxyContext mutableProxyContext, ProxyRequest proxyRequest) throws IOException {
		            
		MutableProxyResponse mpr = new MutableProxyResponse();
		        
		//We use the current logged in user
		String primaryusername = mutableProxyContext.getInternalParameterValue("__auth_username");
		String standardjsonresponse = "{\"state\": \"#state#\",\"message\": \"#message#\",\"data\": #data#}";
		
		boolean debug = false;
		if(mutableProxyContext.getInternalParameterValue("debug")!=null) debug = Boolean.parseBoolean(mutableProxyContext.getInternalParameterValue("debug"));
		
		try {
			String status = "success";
			String jsondata = "{}";

			//Get the locale
			String locale = "en_us";
			if(mutableProxyContext.getInternalParameterValue("locale")!=null) locale = mutableProxyContext.getInternalParameterValue("locale");

			//Determine the i18n implementation
			String type = "properties";
			if(dataProviderConfig.getParamValue("type")!=null) type = dataProviderConfig.getParamValue("type");

			//Determine the root location (this is a relative path)
			String location = "/WEB-INF/com.backbase.labs/i18n";
			if(dataProviderConfig.getParamValue("location")!=null) location = dataProviderConfig.getParamValue("location");

			//set the type;
			i18nInterface i18n = null;
			if(type.equals("contentservices")){
				i18n = new i18nContentServices();
			} else {
				i18n = new i18nResourceProperties();
				//convert location to file location
				location = mutableProxyContext.getServletContext().getRealPath(location);
			}
			i18n.initialize(locale, location);
			
			//Determine action to perform
			String method = proxyRequest.getMethod().toLowerCase();
			if(method.equals("post")){
				String resourceid = mutableProxyContext.getInternalParameterValue("resourceid");
				if(resourceid==null) throw new Exception("resourceid not specified");
				String resourcevalue = mutableProxyContext.getInternalParameterValue("value");
				if(resourcevalue==null) throw new Exception("value not specified");

				jsondata = "\"" + i18n.updateResource(resourceid, resourcevalue) + "\"";
				
			} else if(method.equals("delete")){
				String resourceid = mutableProxyContext.getInternalParameterValue("resourceid");
				if(resourceid==null) throw new Exception("resourceid not specified");
				if(!i18n.deleteResource(resourceid)){
					throw new Exception("Unable to delete " + resourceid);
				}				
			} else {
				if(mutableProxyContext.getInternalParameterValue("resourceid")!=null){
					String resourceid = mutableProxyContext.getInternalParameterValue("resourceid");
					jsondata = "\"" + i18n.getResource(resourceid) + "\"";
				} else {
					HashMap<String,String> resources = i18n.listResources();
					jsondata = "";
					for(String key: resources.keySet()){
						if(jsondata!="") jsondata += ",";
						jsondata += "{\"key\":\"" + key + "\", \"value\":\"" + resources.get(key) + "\"}";
					}
					jsondata = "[" + jsondata + "]";
				}
			}
			
			//Create the final json response
			String jsonresponse = standardjsonresponse.replace("#state#", status);
			jsonresponse = jsonresponse.replace("#message#", locale);
			jsonresponse = jsonresponse.replace("#data#", jsondata);
			mpr.addContentTypeHeader("application/json", "utf-8");
			mpr.setStatusCode(200);
			mpr.setStatusText("ok");
			mpr.setBody(jsonresponse);
			
		} catch (Exception e) {
			String jsonresponse = standardjsonresponse.replace("#state#", "error");
			if(debug){
				jsonresponse = jsonresponse.replace("#message#", e.getMessage() + "\n" + org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(e));
			} else {
				jsonresponse = jsonresponse.replace("#message#", e.getMessage());
			}
			jsonresponse = jsonresponse.replace("#data#", "{}");
			mpr.addContentTypeHeader("application/json", "utf-8");
			mpr.setStatusCode(500);
			mpr.setStatusText(e.getLocalizedMessage());
			mpr.setBody(jsonresponse);
		}

		return mpr;
	}
}
 