package com.backbase.labs.common;

import java.util.HashMap;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.backbase.portal.commons.api.logging.LoggerFactory;
import com.backbase.portal.commons.api.logging.StrictLogger;
import com.sun.org.apache.xml.internal.security.utils.XMLUtils;

public class SearchClient {
	private static final StrictLogger LOGGER = LoggerFactory.MP.logger(SearchClient.class);
	private String resturl = "http://localhost:8888/solr/collection1/update";
	private String defaultfields = "";
	private String idfieldname = "id";
	
	public SearchClient() throws Exception{
		XmlUtilities xmlutils = null;
		try {
			InitialContext initialContext = new InitialContext();  
			String filelocation = (String) initialContext.lookup("java:comp/env/search/config");
			xmlutils = new XmlUtilities(filelocation, false);
		} catch (NamingException e) {
			xmlutils = new XmlUtilities(SearchClient.class.getResource("search.xml").getFile(), false);
		}  
		
		try{
			if(xmlutils!=null){
				this.resturl = xmlutils.performXPathAsTextContent("//resturl");
				if(!this.resturl.endsWith("/")&&!this.resturl.endsWith("\\")) this.resturl += "/";
				this.resturl += xmlutils.performXPathAsTextContent("//collection");
				
				this.idfieldname = xmlutils.performXPathAsTextContent("//idfieldname");

				NodeList defaultfieldsnodes = xmlutils.performXPathAsNodeList("//defaultfields/field");
				for(int x=0; x<defaultfieldsnodes.getLength(); x++){
					Node node = defaultfieldsnodes.item(x);
					this.defaultfields += "<field name=\"" + node.getAttributes().getNamedItem("name").getTextContent() + "\" indexed=\"true\">" + node.getTextContent() + "</field>";
				}
			}
		} catch(Exception ex){
			if(LOGGER.isDebugEnabled())	ex.printStackTrace();
		}
	}
	
	public String query(String query){
		String response = "";
		
		HttpClient httpclient = new HttpClient();
		String solrurl = this.resturl + "/select/?q=" + query + "&wt=json";
		try {
			response = httpclient.getHTMLHttpResponse(solrurl);
		} catch (Exception ex) {
			if(LOGGER.isDebugEnabled())	ex.printStackTrace();
		}
		return response;
	}
	
	public void updateCollection(String itemType, String id, HashMap<String, String> dataFields){		
		String xmltopost = "<add>";
		xmltopost += "<doc>";
		xmltopost += this.defaultfields;
		xmltopost += "<field name=\"datasource_name\">backbase_" + itemType + "</field>";
		xmltopost += "<field name=\"" + this.idfieldname + "\">" + id + "</field>";
		
		for(String key:dataFields.keySet()){
			xmltopost += "<field name=\"" + key + "\" stored=\"true\" update=\"set\">" + dataFields.get(key) + "</field>";
		}
		
		xmltopost += "</doc>";
		xmltopost += "</add>";

		postToSolr(xmltopost);
		
	}
	
	public void cleanupCollection(String itemType, String itemId){
		postToSolr("<delete><" + this.idfieldname + ">" + itemId + "</" + this.idfieldname + "></delete>");
	}
	
	private void postToSolr(String xmlToPost){
		
		String solrurl = this.resturl + "/update?commit=true";
		
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("URL used for post: " + solrurl);
			LOGGER.debug("XML posted to solr:" + xmlToPost);
		}
		
		HttpClient client = new HttpClient();
		try {
			String xmlresponse = client.postXMLHttpResponse(solrurl, xmlToPost);
			if(!xmlresponse.contains("<int name=\"status\">0</int>")){
				LOGGER.debug("Error occured in solr update: " + xmlresponse);
			}
		} catch (Exception e) {
			if(LOGGER.isDebugEnabled())	e.printStackTrace();
		}
	}
}
