package com.backbase.labs.common;
 
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;
 
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
 

//http://www.oracle.com/technetwork/java/javasebusiness/downloads/java-archive-downloads-eeplat-419426.html#javamail-1.4.7-oth-JPR
public class EmailClient {
        
		String host = "";
		String port = "25";
		String username = "";
		String password = "";
		boolean sslenabled = true;
        boolean debug = true;
        
        String emailfileslocation = "/localhost/portalserver/WEB-INF/com.backbase.labs/emails/"; 
        
        public EmailClient(String host, String port, String username, String password, boolean sslEnabled){
        	this.host = host;
        	this.port = port;
        	this.username = username;
        	this.password = password;
        	this.sslenabled = sslEnabled;
        }
        public EmailClient(String host, String port, String username, String password){
        	this.host = host;
        	this.port = port;
        	this.username = username;
        	this.password = password;
        }
        
        public void send(String to, String from, String subject, String messageContent) throws Exception{

            Properties props = new Properties();
            if(this.sslenabled){
                props.put("mail.smtps.auth", "true");
                props.put("mail.smtps.starttls.enable", "true");
                props.put("mail.smtps.port", port);
            } else {
                props.put("mail.smtp.auth", "true");
                props.put("mail.smtp.starttls.enable", "false");
                props.put("mail.smtp.port", port);
            }

            Session session = Session.getInstance(props);
            MimeMessage msg = new MimeMessage(session);
            // Set From: header field of the header.
            msg.setFrom(new InternetAddress(from));

            // Set To: header field of the header.
            msg.setRecipient(Message.RecipientType.TO, new InternetAddress(to));

            // Set Subject: header field
            msg.setSubject(subject);

            // Send the actual HTML message, as big as you like
            msg.setContent(messageContent, "text/html" );
            // set the message content here
            //Transport t = session.getTransport("smtps");
            Transport t = session.getTransport("smtp");
            try {
            	t.connect(host, username, password);
            	//t.connect();
        	t.sendMessage(msg, msg.getAllRecipients());
            if(debug) System.out.println("Email successfully....");
            }catch (MessagingException mex) {
                if(debug) mex.printStackTrace();
                throw new Exception("Unable to send email.", mex);             
            } finally {
            	t.close();
            }
            
        }        	

        
        public String getEmailTemplateHTML(String templateName, HashMap<String,String> mergeMap) throws IOException{
            String templatehtmlstring = "";
            
            //Load the xml in streamreader.
            String filename = this.emailfileslocation + templateName.toLowerCase() + ".html";
            if(debug) System.out.println("html template file: " + filename);
            
            File file = new File(filename);
            templatehtmlstring = getContents(file);
            if(debug) System.out.println("html template file content: " + templatehtmlstring);
            
            
            //Merge fields
            if(mergeMap!=null){
                    for(String key : mergeMap.keySet()){
                    	templatehtmlstring = templatehtmlstring.replace("[#" + key + "#]", mergeMap.get(key));
                    }
                    if(debug) System.out.println("html template file merged: " + templatehtmlstring);
            }
            
            return templatehtmlstring;
    }
    
    
    
    private String getContents(File aFile) throws IOException {
        StringBuilder contents = new StringBuilder();
        
        try {
          BufferedReader input =  new BufferedReader(new FileReader(aFile));
          try {
            String line = null; //not declared within while loop
            while (( line = input.readLine()) != null){
              contents.append(line);
              contents.append(System.getProperty("line.separator"));
            }
          }
          finally {
            input.close();
          }
        }
        catch (IOException ex){
          throw ex;
        }
        
        return contents.toString();
    }
    
    
    /*SETTERS and GETTERS*/
    public void setHTMLTemplateLocation(String folderPath){
            this.emailfileslocation = folderPath;
            if(!emailfileslocation.endsWith("/")) emailfileslocation += "/";
    }
    
    public void setDebug(boolean enabled){
    	this.debug = enabled;
    }
}

class UsernamePasswordAuthenticator extends Authenticator {
    String user;
    String pw;
    public UsernamePasswordAuthenticator (String username, String password)
    {
       super();
       this.user = username;
       this.pw = password;
    }
   public PasswordAuthentication getPasswordAuthentication()
   {
      return new PasswordAuthentication(user, pw);
   }
}
