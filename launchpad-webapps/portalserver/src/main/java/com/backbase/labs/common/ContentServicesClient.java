package com.backbase.labs.common;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import javax.naming.InitialContext;

import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.ItemIterable;
import org.apache.chemistry.opencmis.client.api.ObjectType;
import org.apache.chemistry.opencmis.client.api.QueryResult;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.client.api.SessionFactory;
import org.apache.chemistry.opencmis.client.bindings.spi.LinkAccess;
import org.apache.chemistry.opencmis.client.runtime.SessionFactoryImpl;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.enums.BindingType;
import org.apache.chemistry.opencmis.commons.enums.VersioningState;
import org.apache.chemistry.opencmis.commons.exceptions.CmisObjectNotFoundException;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

import com.backbase.portal.commons.api.logging.LoggerFactory;
import com.backbase.portal.commons.api.logging.StrictLogger;

public class ContentServicesClient {
	private static final StrictLogger LOGGER = LoggerFactory.MP.logger(ContentServicesClient.class);
	private String username;
	private String password;
	private String cmisserverurl;
	private String repositoryid;
	private Session session = null;
	
	public ContentServicesClient() throws Exception{
		InitialContext initialContext = new InitialContext();  
		String filelocation = (String) initialContext.lookup("java:comp/env/backbase/config");
		Properties properties = new Properties();
		properties.load(new FileInputStream(filelocation));
		this.username = properties.getProperty("foundation.content.proxy.username");
		this.password = properties.getProperty("foundation.content.proxy.password");
		this.cmisserverurl = properties.getProperty("foundation.content.proxy.destination") + "/atom";
		this.repositoryid = "contentRepository";
		this.session = getSession();
	}

	public ContentServicesClient(String cmisUsername, String cmisPassword, String cmisServerUrl, String cmisRepositoryId) throws Exception{
		this.username = cmisUsername;
		this.password = cmisPassword;
		this.cmisserverurl = cmisServerUrl;
		this.repositoryid = cmisRepositoryId;
		this.session = getSession();
	}	

	public String getContentById(String contentId){
		String content = "";
		CmisObject cmisobject = this.session.getObject(contentId);
		
	    if ((cmisobject != null) && ((cmisobject instanceof Document))) {
			Document document = (Document)cmisobject;
			try{
				content = resolveDocument(document);
			} catch(Exception ex){
				
			}
	    }
		
		return content;
	}

	
	public String getContentByPath(String contentPath){
		String content = "";
		CmisObject cmisobject = getCmisObject(contentPath);
		
	    if ((cmisobject != null) && ((cmisobject instanceof Document))) {
			Document document = (Document)cmisobject;
			try{
				content = resolveDocument(document);
			} catch(Exception ex){
				
			}
	    }
		
		return content;
	}

	
	public HashMap<String, Object> getRawContent(String contentPath){
		HashMap<String, Object> rawcontent = new HashMap<String, Object>();
		CmisObject cmisobject = getCmisObject(contentPath);
		
	    if ((cmisobject != null) && ((cmisobject instanceof Document))) {
			Document document = (Document)cmisobject;
			try{
				rawcontent = resolveRawDocument(document);
			} catch(Exception ex){
				rawcontent = null;
			}
	    }

		return rawcontent;
	}

	
	public boolean updateContentByPath(String folderPath, String filename,  String content) throws Exception{
		return updateContentByPath(folderPath, filename, content, "text/plain; charset=UTF-8");
	}
	public boolean updateContentByPath(String folderPath, String filename, String content, String mimetype) throws Exception{

		boolean response = false;
		try{
			CmisObject cmisobject = getCmisObject(folderPath + "/" + filename);
			ContentStream cs = getContentStream(mimetype, content, filename);
			Document document = (Document)cmisobject;
			document.setContentStream(cs, true);
			response = true;
		} catch(CmisObjectNotFoundException ex){
			
			Folder parentfolder = getFolder(folderPath, true);
			if(parentfolder!=null){
				Document document = createDocument(mimetype, content, filename, parentfolder);
				response = true;
			}
		}
		
		return response;
	}
	
	public boolean deleteContentByPath(String contentPath){
		boolean success = true;
		
		CmisObject cmisobject = getCmisObject(contentPath);
		if(cmisobject!=null){
			Document document = (Document)cmisobject;
			document.deleteAllVersions();
		}
		
		return success;
	}
	
	
	public boolean createFolder(String folderPath, String folderName){
		boolean success = false;
		String folderpath = folderPath + "/" + folderName;
		try{
			Folder folder = getFolder(folderpath, true);
			success = true;
		} catch(Exception ex){
			
		}
		
		return success;
	}

	public HashMap<String,String> getListFolders(String folderPath) throws Exception{
		HashMap<String, String> listitems = new HashMap<String, String>();
		
		Folder folder = getFolder(folderPath);
		ItemIterable<CmisObject> folderitems = getFolderChildren(folder);
		
		for (CmisObject o : folderitems) {
			String type = o.getType().getId();
			if(!type.equals(ObjectType.DOCUMENT_BASETYPE_ID)){
				listitems.put(o.getName(), folderPath + "/" + o.getName());
			}
		}
		
		return listitems;
	}
	
	public HashMap<String,String> getList(String folderPath, boolean documentsOnly) throws Exception{
		return getList(folderPath, documentsOnly, true);
	}
	
	public HashMap<String,String> getList(String folderPath, boolean documentsOnly, boolean resolveContent) throws Exception{
		HashMap<String, String> listitems = new HashMap<String, String>();
		
		Folder folder = getFolder(folderPath);
		ItemIterable<CmisObject> folderitems = getFolderChildren(folder);
		
		for (CmisObject o : folderitems) {
			String type = o.getType().getId();

			String friendlyname = o.getName();
			if(friendlyname.lastIndexOf(".")>-1){
				friendlyname = friendlyname.substring(0, friendlyname.lastIndexOf("."));
			}
			
			if(documentsOnly){
				if(type.equals(ObjectType.DOCUMENT_BASETYPE_ID)){
					//document
					if(resolveContent){
						listitems.put(friendlyname, resolveDocument((Document)o));
					} else {
						listitems.put(friendlyname, folderPath + "/" + o.getName());
					}
				}
			} else {
				if(type.equals(ObjectType.DOCUMENT_BASETYPE_ID)){
					//document
					if(resolveContent){
						listitems.put(friendlyname, resolveDocument((Document)o));
					} else {
						listitems.put(friendlyname, folderPath + "/" + o.getName());
					}
				} else {
					//folder
					listitems.put(friendlyname, folderPath + "/" + o.getName());
				}
			}
		}

		return listitems;
	}
	
	
	/* PRIVATE */
	
	private Session getSession()
	{
		SessionFactory factory = SessionFactoryImpl.newInstance();
		HashMap<String, String> parameter = new HashMap<String, String>();
		parameter.put("org.apache.chemistry.opencmis.user", this.username);
		parameter.put("org.apache.chemistry.opencmis.password", this.password);
		parameter.put("org.apache.chemistry.opencmis.binding.atompub.url", this.cmisserverurl);
		parameter.put("org.apache.chemistry.opencmis.binding.spi.type", BindingType.ATOMPUB.value());
		parameter.put("org.apache.chemistry.opencmis.session.repository.id", this.repositoryid);
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(String.format("getting cmis session for cmis server= '%s', repositoryId= '%s'", new Object[] { this.cmisserverurl, this.repositoryid }));
		}
		
		return factory.createSession(parameter);
	}
	
	/* FOLDER */

	private Folder getFolder(String folderPath) throws Exception{
		return getFolder(folderPath, false);
	}
	private Folder getFolder(String folderPath, boolean createIfNotExist) throws Exception{
	
		Folder folder = null;
		try{
			CmisObject folderobject = getCmisObject(folderPath); 
			folder = (Folder)folderobject;
		} catch(CmisObjectNotFoundException ex){
			if(createIfNotExist){
				String[] folderpatharr = folderPath.split("/");
				Folder rootfolder = this.session.getRootFolder();
				Folder currentfolder = rootfolder;
				for(String subfoldername: folderpatharr){
					if(!subfoldername.equals("")){
						currentfolder = getFolder(subfoldername, currentfolder,true);
					}
				}
				folder = currentfolder;
			} else {
				throw ex;
			}
		}
		return folder;
	}
	
	private Folder getFolder(String folderName, Folder parentFolder, boolean create){
		Folder folder = null;
		boolean found = false;
		
		ItemIterable<CmisObject> folderitems = parentFolder.getChildren();
		
		for (CmisObject o : folderitems) {
			if(o.getType().getId().equals(ObjectType.FOLDER_BASETYPE_ID)){
				if(o.getName().equals(folderName)){
					folder = (Folder)o;
					found = true;
				}
			}
		}
		if((!found)&&(create)){
			folder = createFolder(parentFolder, folderName);
		}
		
		return folder;
	}
	
	private ItemIterable<CmisObject> getFolderChildren(Folder parentFolder){
		return parentFolder.getChildren();
	}

	private Folder createFolder(Folder parentFolder, String folderName){
		Map<String, String> newFolderProps = new HashMap<String, String>();
		newFolderProps.put(PropertyIds.OBJECT_TYPE_ID, "cmis:folder");
		newFolderProps.put(PropertyIds.NAME, folderName);

		Folder newfolder = parentFolder.createFolder(newFolderProps);
		return newfolder;
	}

	
	/* DOCUMENT */
	
	private Document createDocument(String mimetype, String content, String filename, Folder parentFolder) throws UnsupportedEncodingException{
		
		ContentStream contentstream = getContentStream(mimetype, content, filename);
		return createDocument(contentstream, filename, parentFolder);
	}
	private Document createDocument(ContentStream contentStream, String filename, Folder parentFolder) throws UnsupportedEncodingException{
		Map<String, Object> properties = new HashMap<String, Object>();
		properties.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document");
		properties.put(PropertyIds.NAME, filename);

		Document document = parentFolder.createDocument(properties, contentStream, VersioningState.NONE);

		return document;
	}

	private ContentStream getContentStream(String mimetype, String content, String filename) throws UnsupportedEncodingException{
		byte[] buf = content.getBytes("UTF-8");
		ByteArrayInputStream input = new ByteArrayInputStream(buf);

		ContentStream contentStream = session.getObjectFactory().createContentStream(filename, buf.length, mimetype, input);
		return contentStream;
	}
	
	private String resolveDocument(Document cmisDocument) throws Exception{
		String content = "";
		String contentType = cmisDocument.getContentStreamMimeType();
		if ((contentType.contains("text")) || (contentType.contains("xml")))
        {
			Object contentStream = cmisDocument.getContentStream();
			if (contentStream != null) {
				content = IOUtils.toString(cmisDocument.getContentStream().getStream(), "UTF-8");
			}
        } else {
        	content = getContentURL(cmisDocument);
        }
		return content;
    }

	private HashMap<String, Object> resolveRawDocument(Document cmisDocument) throws Exception{
		
		HashMap<String, Object> rawdocument = new HashMap<String, Object>();
		rawdocument.put("mimetype", cmisDocument.getContentStreamMimeType());
		rawdocument.put("filename", cmisDocument.getContentStreamFileName());
		
		ContentStream contentStream = cmisDocument.getContentStream();
		if (contentStream != null) {
			rawdocument.put("content", cmisDocument.getContentStream().getStream());
			
		} else {
			rawdocument.put("content", null);
		}

		return rawdocument;
    }
	
	/*CMIS OBJECT*/

	private CmisObject getCmisObject(String contentPath)
	{
		String[] pathComponents = contentPath.split("\\?versionLabel=");
		CmisObject cmisObject = null;
		if (pathComponents.length == 1) {
			if (!StringUtils.isEmpty(pathComponents[0])) {
				cmisObject = session.getObjectByPath(pathComponents[0]);
			}
		} else if (pathComponents.length == 2) {
			cmisObject = getCMISObjectByPathAndVersion(pathComponents[0], pathComponents[1]);
        }
		return cmisObject;
	}

	
	private CmisObject getCMISObjectByPathAndVersion(String path, String versionLabel)
	{
		CmisObject cmisObject = null;
		if ((!StringUtils.isEmpty(path)) && (!StringUtils.isEmpty(versionLabel))) {
			ItemIterable resultset = session.query("SELECT * FROM cmis:document WHERE cmis:path = '" + path + "' AND cmis:versionLabel = '" + versionLabel + "'", true);
		
			Iterator resultSetIterator = resultset.iterator();
			if (resultSetIterator.hasNext()) {
				QueryResult result = (QueryResult)resultset.iterator().next();
				String objectId = (String)result.getPropertyById("cmis:objectId").getFirstValue();
				cmisObject = session.getObject(objectId);
			}
		}
		return cmisObject;
	}

	
	/* UTILITIES */
	
	private String getContentURL(Document cmisDocument)
	{
		if ((session.getBinding().getObjectService() instanceof LinkAccess)) {
			String result = ((LinkAccess)session.getBinding().getObjectService()).loadContentLink(session.getRepositoryInfo().getId(), cmisDocument.getId());
			if (result != null) {
				String[] parts = result.split("/atom");
				if (parts.length == 2) {
					result = this.cmisserverurl + parts[1];
				}
			}
			return result;
		}
	
		return "";
	}

}
