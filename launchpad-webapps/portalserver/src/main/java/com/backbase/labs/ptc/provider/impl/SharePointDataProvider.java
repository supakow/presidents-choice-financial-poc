package com.backbase.labs.ptc.provider.impl;

import java.io.IOException;

import com.backbase.labs.common.SharePointRestClient;
import com.backbase.portal.ptc.config.element.DataProviderConfig;
import com.backbase.portal.ptc.context.MutableProxyContext;
import com.backbase.portal.ptc.provider.DataProvider;
import com.backbase.portal.ptc.request.ProxyRequest;
import com.backbase.portal.ptc.response.MutableProxyResponse;

public class SharePointDataProvider implements DataProvider {

	@Override
	public MutableProxyResponse executeRequest(DataProviderConfig dataProviderConfig,
			MutableProxyContext mutableProxyContext, ProxyRequest proxyRequest) throws IOException {
		
		MutableProxyResponse mpr = new MutableProxyResponse();
		
		try{
			SharePointRestClient sprc = new SharePointRestClient(dataProviderConfig.getParamValue("liveloginurl"), dataProviderConfig.getParamValue("endpointurl"), dataProviderConfig.getParamValue("username"), dataProviderConfig.getParamValue("password"));
			
			String action = mutableProxyContext.getInternalParameterValue("action");
			if(action.equals("getlists")){
				mpr.setBody(sprc.getLists());
			} else if(action.equals("getlist")){
				mpr.setBody(sprc.getList(mutableProxyContext.getInternalParameterValue("listname")));
			} else if(action.equals("getlistitems")){
				mpr.setBody(sprc.getListItems(mutableProxyContext.getInternalParameterValue("listname")));
			} else if(action.equals("createlist")){
				mpr.setBody(sprc.createList(mutableProxyContext.getInternalParameterValue("listname"), mutableProxyContext.getInternalParameterValue("listdescription")));
			} else if(action.equals("additems")){
				mpr.setBody(sprc.addItems(mutableProxyContext.getInternalParameterValue("listname"), mutableProxyContext.getInternalParameterValue("listitems")));
			}
			
			mpr.setStatusCode(200);
			mpr.setStatusText("OK");
			
		} catch (Exception ex){
			mpr.setBody(ex.getMessage());
			mpr.setStatusCode(500);
		}
		
		return mpr;
	}

}
