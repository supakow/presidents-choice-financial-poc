package com.backbase.labs.ptc.provider.impl;
 
import java.io.IOException;
 


import com.backbase.labs.common.HttpClient;
import com.backbase.labs.common.SearchClient;
import com.backbase.portal.ptc.config.element.DataProviderConfig;
import com.backbase.portal.ptc.context.MutableProxyContext;
import com.backbase.portal.ptc.provider.DataProvider;
import com.backbase.portal.ptc.request.ProxyRequest;
import com.backbase.portal.ptc.response.MutableProxyResponse;
 
public class SearchDataProvider implements DataProvider {
 
	@Override
	public MutableProxyResponse executeRequest(DataProviderConfig dataProviderConfig, MutableProxyContext mutableProxyContext, ProxyRequest proxyRequest) throws IOException {
	            
		MutableProxyResponse mpr = new MutableProxyResponse();
		            
		//We use the current logged in user
		String primaryusername = mutableProxyContext.getInternalParameterValue("__auth_username");
		String standardjsonresponse = "{\"state\": \"#state#\",\"message\": \"#message#\",\"data\": #data#}";
		
		boolean debug = false;
		if(mutableProxyContext.getInternalParameterValue("debug")!=null) debug = Boolean.parseBoolean(mutableProxyContext.getInternalParameterValue("debug"));
		
		
		try {
			String status = "success";
			String query = "";
			if(mutableProxyContext.getInternalParameterValue("q")!=null) query = mutableProxyContext.getInternalParameterValue("q");

			SearchClient searchclient = new SearchClient();
			String jsondata = searchclient.query(query);
			                  
			if(jsondata == "") jsondata = "{}";
			
			//Create the final json response
			String jsonresponse = standardjsonresponse.replace("#state#", status);
			jsonresponse = jsonresponse.replace("#message#", "");
			jsonresponse = jsonresponse.replace("#data#", jsondata);
			mpr.addContentTypeHeader("application/json", "utf-8");
			mpr.setStatusCode(200);
			mpr.setStatusText("ok");
			mpr.setBody(jsonresponse);
		} catch (Exception e) {
			String jsonresponse = standardjsonresponse.replace("#state#", "error");
			if(debug){
				jsonresponse = jsonresponse.replace("#message#", e.getMessage() + "\n" + org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(e));
			} else {
				jsonresponse = jsonresponse.replace("#message#", e.getMessage());
			}
			jsonresponse = jsonresponse.replace("#data#", "{}");
			mpr.addContentTypeHeader("application/json", "utf-8");
			mpr.setStatusCode(500);
			mpr.setStatusText(e.getLocalizedMessage());
			mpr.setBody(jsonresponse);
		}
		    
		return mpr;
	}
 
}
 