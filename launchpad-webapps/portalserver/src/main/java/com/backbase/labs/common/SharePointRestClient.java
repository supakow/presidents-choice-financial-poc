package com.backbase.labs.common;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.NTCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;

public class SharePointRestClient {

	private HttpClient httpclient = null;
	private String liveloginurl = "";
	private String entrypointurl = "";
	private String username = "";
	private String password = "";
	private String authenticationtype = "integrated"; //both||integrated||form
	
	private Header[] postheaders = null;
	
	public SharePointRestClient(String liveloginurl, String entrypointurl, String username, String password, String authenticationType){
		this.liveloginurl = liveloginurl;
		this.authenticationtype = authenticationType; 
		this.entrypointurl = entrypointurl;
		this.username = username;
		this.password = password;
		InitializeRestClient();
	}
	public SharePointRestClient(String liveloginurl, String entrypointurl, String username, String password){
		this.liveloginurl = liveloginurl;
		this.entrypointurl = entrypointurl;
		this.username = username;
		this.password = password;
		InitializeRestClient();
	}
	
	private void InitializeRestClient(){
		this.authenticationtype = "form";
		Map<String, String[]> params = new HashMap<String, String[]>();
		params.put("login", new String[] {this.username});
		params.put("passwd", new String[] {this.password});
		httpclient = getHttpClient(liveloginurl, params);
		
		
		//httpclient = getHttpClient(this.entrypointurl, this.username, this.password);
	}
	
	
	public String getLists(){
		String restquery = "/web/lists";
		
		return getStringHttpResponse(httpclient, this.entrypointurl + restquery);
		
		//return "[{\"name\":\"lista\",\"description\":\"list a\"},{\"name\":\"listb\", \"description\":\"list b\"}]";
	}

	public String getList(String listName){
		String restquery = "/web/lists/getbytitle('" + listName + "')";
		return "getList " + listName;
	}
	
	public String getListItems(String listName){
		String restquery = "/web/lists/getbytitle('" + listName + "')/items";
		return "[\"name\":\"" + listName + "\", \"description\":\"list description\", \"items\":[{\"name\":\"item1\", \"description\":\"item 1\"},{\"name\":\"item2\", \"description\":\"item 2\"},{\"name\":\"item2\", \"description\":\"item 2\"}]]";
	}
	public String getListItems(String listName, String selectProperties){
		String restquery = "/web/lists/getbytitle('" + listName + "')/items?select=" + selectProperties;
		return "getListItems " + listName + " " + selectProperties;
	}
	
	public String createList(String title, String description){
		return "createList " + title + " " + description;
	}
	
	public String addItems(String listName, String items){
		return "addItems " + listName + " " + items;
	}

	private HttpClient getHttpClient(String baseUrl, String userName, String password) {
		return getHttpClient(baseUrl, null, userName, password, null);
	}	
	private HttpClient getHttpClient(String loginUrl, Map<String,String[]> postParameters) {
		return getHttpClient(null, loginUrl, null, null, postParameters);
	}	
	private HttpClient getHttpClient(String baseUrl, String loginUrl, String userName, String password, Map<String,String[]> postParameters) {
		if(httpclient == null) {
			
			httpclient = new HttpClient();
			String responsehtml = "";
			
			if(authenticationtype.equals("both")||authenticationtype.equals("integrated")){
				//Credentials defaultcreds = new UsernamePasswordCredentials(userName, password);
				//httpclient.getState().setCredentials(AuthScope.ANY, defaultcreds);
				
				NTCredentials credentials = new NTCredentials("POCUser", "PyramisPOC2013", "JAVA-MACHINE-NAME", "fidelitypga.onmicrosoft.com");
	            httpclient.getState().setCredentials(AuthScope.ANY, credentials);
	            httpclient.getParams().setAuthenticationPreemptive(true);

	            responsehtml = getStringHttpResponse(httpclient, baseUrl);
			}
			
			if(authenticationtype.equals("both")||authenticationtype.equals("form")){
				responsehtml = postStringHttpResponse(httpclient, loginUrl, "text/html",  postParameters);
			}
			
		}
		
		return httpclient;
	}
	
	
	
	private String getStringHttpResponse(HttpClient client, String URL) {
		String htmlresponse = "";
        
		GetMethod method = new GetMethod(URL);
        
		for(Header header : postheaders){
			if(!header.getName().equals("Connection")&&!header.getName().equals("Content-Length")){
				System.out.println(header.getName() + " : " + header.getValue());
				method.addRequestHeader(header);
			}
		}
		
		try {
			// Execute the method.
			int statusCode = client.executeMethod(method);
			if (statusCode != HttpStatus.SC_OK) {
				System.err.println("Method failed: " + method.getStatusLine());
			}
			if(statusCode != HttpStatus.SC_NO_CONTENT) {
				byte[] responseBody = method.getResponseBody();
				
				htmlresponse = new String(responseBody);
			}
			
		} catch (HttpException e) {
			System.err.println("Fatal protocol violation: " + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Fatal transport error: " + e.getMessage());
			e.printStackTrace();
		} finally {
			// Release the connection.
			method.releaseConnection();
		}  
        
		return htmlresponse;
	}
	
	
	
	private String postStringHttpResponse(HttpClient client, String URL, String contentType, Map<String,String[]> postParam) {
		String htmlresponse = "";
		String contenttype = "application/x-www-form-urlencoded; charset=UTF-8";
		if(contentType!=null) contenttype = contentType;
		PostMethod method = new PostMethod(URL);

		if(postParam!=null && postParam.size() > 0){
		    Iterator pParams	= postParam.entrySet().iterator();
		    while(pParams.hasNext()){
		    	Map.Entry pairs	= (Map.Entry)pParams.next();
		    	String[] val = (String[]) pairs.getValue();
		    	method.addParameter(pairs.getKey().toString(),val[0].toString());
		    }
		    method.setRequestHeader("Content-Type", contenttype);
		    //method.setRequestHeader("Faces-Request", "partial/ajax");	
		    //method.setRequestHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
	    }else{
	    	method.setRequestHeader("Content-type", contenttype);
	    }

		
		try {
			// Execute the method.
			int statusCode = client.executeMethod(method);
			if (statusCode != HttpStatus.SC_OK) {
				System.err.println("Method failed: " + method.getStatusLine());
			}
			if(statusCode != HttpStatus.SC_NO_CONTENT) {
				if(postheaders==null) postheaders = method.getResponseHeaders();
				byte[] responseBody = method.getResponseBody();
				htmlresponse = new String(responseBody);
				System.out.println(htmlresponse);
			}
			
		} catch (HttpException e) {
			System.err.println("Fatal protocol violation: " + e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Fatal transport error: " + e.getMessage());
			e.printStackTrace();
		} finally {
			// Release the connection.
			method.releaseConnection();
		}  
        
		return htmlresponse;
	}
}
