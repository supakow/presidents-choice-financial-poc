package com.backbase.labs.common;
 
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
 
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.xpath.XPathExpressionException;
 
import org.xml.sax.SAXException;
 
 
public class StubWebServiceClient {
        
        String stubfolderlocation = "/localhost/portalserver/WEB-INF/com.backbase.labs/stubs/";
        
        boolean debug = false;
        
        public StubWebServiceClient(){
        }
                        
 
        public String performPost(String targetUrl, String soapAction, String serviceName) throws Exception{
                return performPost(targetUrl, soapAction, serviceName, null);
        }
 
        public String performPost(String targetUrl, String soapAction, String serviceName, HashMap<String, String> mergeMap) throws Exception{
            String responsexml = "";
                
	        SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
	        SOAPConnection soapConnection = soapConnectionFactory.createConnection();
 
            String xmlPostData = loadStubFileData(serviceName,mergeMap);
            if(debug) System.out.println("targeturl: " + targetUrl);
 
	        SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(xmlPostData, soapAction), targetUrl);
	 
	        responsexml = createSOAPResponse(soapResponse);
	 
	         if(this.debug){
	                System.out.println("Response SOAP Message = " + responsexml);
	         }
	 
	        soapConnection.close();
	        return responsexml;
	        }
 
        private String createSOAPResponse(SOAPMessage soapResponse) throws SOAPException, IOException, XPathExpressionException, ParserConfigurationException, SAXException{
	        ByteArrayOutputStream out = new ByteArrayOutputStream();
	        soapResponse.writeTo(out);
	        String responsexml = new String(out.toByteArray()); 
	 
	        XmlUtilities xmlutils = new XmlUtilities(responsexml, null);
	        responsexml = xmlutils.getXML("//self::node()[local-name()='Body']");
	 
	        return responsexml;
 
        }
        
	    private SOAPMessage createSOAPRequest(String soapEnvelopeXml, String soapAction) throws SOAPException, IOException, TransformerConfigurationException, TransformerException, TransformerFactoryConfigurationError, ParserConfigurationException, SAXException{
	         MessageFactory messageFactory = MessageFactory.newInstance();
	         
	         XmlUtilities xmlutils = new XmlUtilities(soapEnvelopeXml, "");  
	 
	        MimeHeaders headers = new MimeHeaders();
	        headers.addHeader("SOAPAction", soapAction);
	        headers.addHeader("Content-Type", "text/xml;charset=UTF-8");
	         
	         SOAPMessage soapMessage = messageFactory.createMessage(headers, xmlutils.getInputStream());
	 
	         if(this.debug){
	                /* Print the request message */
	                System.out.print("Request SOAP Message = ");
	                soapMessage.writeTo(System.out);
	                System.out.println();
	         }
	         
	         return soapMessage;
	    }
	 
        
        private String loadStubFileData(String serviceName, HashMap<String,String> mergeMap) throws IOException{
                String filexmlstring = "";
                
                //Load the xml in streamreader.
                String filename = this.stubfolderlocation + serviceName.toLowerCase() + ".xml";
                if(debug) System.out.println("stub file: " + filename);
                
                File file = new File(filename);
                filexmlstring = getContents(file);
                if(debug) System.out.println("stub file content: " + filexmlstring);
                
                
                //Merge fields
                if(mergeMap!=null){
                        for(String key : mergeMap.keySet()){
                                filexmlstring = filexmlstring.replace("[#" + key + "#]", mergeMap.get(key));
                        }
                        if(debug) System.out.println("stub file merged: " + filexmlstring);
                }
                
                return filexmlstring;
        }
        
        
        
        private String getContents(File aFile) throws IOException {
            StringBuilder contents = new StringBuilder();
            
            try {
              BufferedReader input =  new BufferedReader(new FileReader(aFile));
              try {
                String line = null; //not declared within while loop
                while (( line = input.readLine()) != null){
                  contents.append(line);
                  contents.append(System.getProperty("line.separator"));
                }
              }
              finally {
                input.close();
              }
            }
            catch (IOException ex){
              throw ex;
            }
            
            return contents.toString();
        }
        
        
        /*SETTERS and GETTERS*/
        public void setStubFolderLocation(String folderPath){
                this.stubfolderlocation = folderPath;
                if(!stubfolderlocation.endsWith("/")) stubfolderlocation += "/";
        }
        
        public void setDebug(boolean enabled){
                this.debug = enabled;
        }
}
