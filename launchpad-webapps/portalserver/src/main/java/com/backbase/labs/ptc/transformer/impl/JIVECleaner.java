package com.backbase.labs.ptc.transformer.impl;

import com.backbase.portal.ptc.context.MutableProxyContext;
import com.backbase.portal.ptc.request.ProxyRequest;
import com.backbase.portal.ptc.response.MutableProxyResponse;
import com.backbase.portal.ptc.transform.response.AbstractResponseTransformer;

public class JIVECleaner extends AbstractResponseTransformer{
	
	public void transform(MutableProxyContext proxyContext,ProxyRequest proxyRequest, MutableProxyResponse proxyResponse){
		
		String jsonString = proxyResponse.getBody();
		
		jsonString = jsonString.replaceAll("throw 'allowIllegalResourceCall is false.';","");
		
		proxyResponse.setBody(jsonString);
		proxyResponse.addContentTypeHeader("application/json","UTF-8");
		
	}
	
}