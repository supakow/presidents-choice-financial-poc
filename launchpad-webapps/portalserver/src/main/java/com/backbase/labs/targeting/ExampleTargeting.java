package com.backbase.labs.targeting;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.backbase.portal.targeting.connectorframework.content.contexts.definition.ContextCollector;
import com.backbase.portal.targeting.connectorframework.content.contexts.definition.PossibleValue;
import com.backbase.portal.targeting.connectorframework.content.contexts.definition.ResultEntries;
import com.backbase.portal.targeting.connectorframework.content.contexts.definition.SegmentDefinition;
import com.backbase.portal.targeting.connectorframework.content.contexts.definition.SelectorDefinition;
import com.backbase.portal.targeting.rulesengine.type.RuleEngineTypes;


public class ExampleTargeting extends ContextCollector {

	static String contextId = "audience";
	static String description = "Audience"; 
	static String iconPath = "$(contextRoot)/static/backbase.com.2012.darts/media/contexts/targeting-context-crm.png";
	static String label = "Audience";
	
	public ExampleTargeting() {
		super(contextId, description, iconPath, label);
	}


	//Define the segments
	@Override
	public List<SegmentDefinition> defineSegments(String arg0, Map<String, String> arg1) {
	//public List<SegmentDefinition> defineSegments(String arg0) {
	    List<SegmentDefinition> segmentDefinitions = new ArrayList<SegmentDefinition>();
	    segmentDefinitions.add(new SegmentDefinition("consultant", "Consultants"));
	    segmentDefinitions.add(new SegmentDefinition("client", "Clients"));
	    segmentDefinitions.add(new SegmentDefinition("prospect", "Prospects"));
	    return segmentDefinitions;
	}


	//Define the conditions
	@Override
	//public List<SelectorDefinition> defineSelectors(String arg0) {
	public List<SelectorDefinition> defineSelectors(String arg0, Map<String, String> arg1) {
        List<SelectorDefinition> selectorDefinitions = new ArrayList<SelectorDefinition>();
        SelectorDefinition customerclient = new SelectorDefinition("audience", RuleEngineTypes.ENUM, "Type of Client");
        customerclient.addPossibleValues(new PossibleValue[] {
            new PossibleValue("consultant"), new PossibleValue("client"), new PossibleValue("prospect")
        });
        selectorDefinitions.add(customerclient);
        return selectorDefinitions;
	}


	//This is the execution of the actual rules
	@Override
	public ResultEntries executeTask(Map<String, String> requestMap, ResultEntries resultEntries) {

		String username = requestMap.get("session.authentication.username");
		String usertypestring = "client";
		
		//SF query here to determine if user is client or customer.
		resultEntries.addSelectorEntry("audience", usertypestring);
		resultEntries.addSegmentEntry(usertypestring);

	    return resultEntries;		
	}


}
