package com.backbase.labs.ptc.transformer.impl;

import org.htmlcleaner.CleanerProperties;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.PrettyHtmlSerializer;
import org.htmlcleaner.TagNode;

import com.backbase.portal.ptc.context.MutableProxyContext;
import com.backbase.portal.ptc.request.ProxyRequest;
import com.backbase.portal.ptc.response.MutableProxyResponse;
import com.backbase.portal.ptc.transform.response.AbstractResponseTransformer;

public class HtmlToXml extends AbstractResponseTransformer{

	@Override
	public void transform(MutableProxyContext mutableProxyContext, ProxyRequest proxyRequest,
			MutableProxyResponse proxyResponse) {
		
        try{
        	//convert html to xhtml
        	HtmlCleaner cleaner = new HtmlCleaner();
	        CleanerProperties props = cleaner.getProperties();
	        props.setAllowHtmlInsideAttributes(true);
	        props.setAllowMultiWordAttributes(true);
	        props.setRecognizeUnicodeChars(true);
	        props.setOmitXmlDeclaration(true);
            props.setOmitUnknownTags(true);
            props.setOmitComments(true);
            props.setPruneTags("script");
	        
	        String xmlstring = "";
	        PrettyHtmlSerializer serializer = new PrettyHtmlSerializer(props);
        	TagNode node = cleaner.clean(proxyResponse.getBody());
	        Object[] content_nodes = node.evaluateXPath("//body");
	        if (content_nodes.length > 0) {
	        	xmlstring = "";
	        	for(int i=0; i<content_nodes.length; i++){
		            TagNode content_node = (TagNode) content_nodes[i];
		            xmlstring += serializer.getAsString(content_node, "UTF-8", true);
	        	}
	        }
	        
	        proxyResponse.setBody(xmlstring);
        	            
        } catch(Exception ex){
            ex.printStackTrace();
        }
		
	}

}
