package com.backbase.labs.ptc.formsbasic.provider.impl;

 
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.backbase.labs.common.EmailClient;
import com.backbase.portal.ptc.config.element.DataProviderConfig;
import com.backbase.portal.ptc.context.MutableProxyContext;
import com.backbase.portal.ptc.provider.DataProvider;
import com.backbase.portal.ptc.request.ProxyRequest;
import com.backbase.portal.ptc.response.MutableProxyResponse;
 
public class FormsBasicEmailProvider implements DataProvider {
 
	@Override
	public MutableProxyResponse executeRequest(DataProviderConfig dataProviderConfig,
		MutableProxyContext mutableProxyContext, ProxyRequest proxyRequest) throws IOException {
		            
		MutableProxyResponse mpr = new MutableProxyResponse();
		        
		//We use the current logged in user
		String primaryusername = mutableProxyContext.getInternalParameterValue("__auth_username");
		String standardjsonresponse = "{\"state\": \"#state#\",\"message\": \"#message#\",\"data\": #data#}";
		
		boolean debug = false;
		if(mutableProxyContext.getInternalParameterValue("debug")!=null) debug = Boolean.parseBoolean(mutableProxyContext.getInternalParameterValue("debug"));
		
		try {
			String status = "success";
			String jsondata = "{}";

			String fromemail = dataProviderConfig.getParamValue("username");
            String toemail = mutableProxyContext.getInternalParameterValue("toemail");
            if(toemail==null) toemail = dataProviderConfig.getParamValue("toemail");
            
            //Get the email client
            EmailClient emailclient = new EmailClient(dataProviderConfig.getParamValue("smtpserver"), dataProviderConfig.getParamValue("smtpport"), dataProviderConfig.getParamValue("username"), dataProviderConfig.getParamValue("password"));
            emailclient.setDebug(debug);
            emailclient.setHTMLTemplateLocation(mutableProxyContext.getServletContext().getRealPath("/WEB-INF/com.backbase.labs/emails/"));
            
            String fieldshtml = "<table style=\"width:100%\">";
			Map<String,String[]> params = mutableProxyContext.getInternalParameters();
			for(String key: params.keySet()){
				fieldshtml += "<tr><td><b>" + key + "</b></td><td>" + mutableProxyContext.getInternalParameterValue(key) + "</td></tr>";
			}
            fieldshtml += "</table>";
            
            //Get the html to send
            HashMap<String, String> emailfields = new HashMap<String, String>();
            emailfields.put("toemail", toemail);
            emailfields.put("fields", fieldshtml);
            String emailhtml = emailclient.getEmailTemplateHTML("formsbasic", emailfields);
            
            //Send the email
            emailclient.send(toemail, fromemail, "Forms Basic Email Sample", emailhtml);
            jsondata = "{\"send_to\":\"" + toemail + "\"}";

			//Create the final json response
			String jsonresponse = standardjsonresponse.replace("#state#", status);
			jsonresponse = jsonresponse.replace("#message#", "");
			jsonresponse = jsonresponse.replace("#data#", jsondata);
			mpr.addContentTypeHeader("application/json", "utf-8");
			mpr.setStatusCode(200);
			mpr.setStatusText("ok");
			mpr.setBody(jsonresponse);
			
		} catch (Exception e) {
			String jsonresponse = standardjsonresponse.replace("#state#", "error");
			if(debug){
				jsonresponse = jsonresponse.replace("#message#", e.getMessage() + "\n" + org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(e));
			} else {
				jsonresponse = jsonresponse.replace("#message#", e.getMessage());
			}
			jsonresponse = jsonresponse.replace("#data#", "{}");
			mpr.addContentTypeHeader("application/json", "utf-8");
			mpr.setStatusCode(500);
			mpr.setStatusText(e.getLocalizedMessage());
			mpr.setBody(jsonresponse);
		}

		return mpr;
	}
}
 