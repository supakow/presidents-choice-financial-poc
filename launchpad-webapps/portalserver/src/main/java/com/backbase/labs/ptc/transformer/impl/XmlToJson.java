package com.backbase.labs.ptc.transformer.impl;
 
import net.sf.json.JSON;
import net.sf.json.JSONSerializer;
import net.sf.json.xml.XMLSerializer;
 
import com.backbase.portal.ptc.context.MutableProxyContext;
import com.backbase.portal.ptc.request.ProxyRequest;
import com.backbase.portal.ptc.response.MutableProxyResponse;
import com.backbase.portal.ptc.transform.response.AbstractResponseTransformer;
 
public class XmlToJson extends AbstractResponseTransformer{
        
        public void transform(MutableProxyContext proxyContext,ProxyRequest proxyRequest, MutableProxyResponse proxyResponse){
                
                try{
                        String xmlstring = proxyResponse.getBody();
                        XMLSerializer serializer = new XMLSerializer();
                        JSON json = serializer.read(xmlstring);
                        proxyResponse.setBody(json.toString());
                        proxyResponse.addContentTypeHeader("application/json","UTF-8");
                } catch(Exception ex){
                        ex.printStackTrace();
                }
                
                
        }
        
}
