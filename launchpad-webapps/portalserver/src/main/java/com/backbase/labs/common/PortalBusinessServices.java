package com.backbase.labs.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ApplicationContextEvent;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.event.ContextRefreshedEvent;

import com.backbase.portal.foundation.business.service.GroupBusinessService;
import com.backbase.portal.foundation.business.service.ItemBusinessService;
import com.backbase.portal.foundation.business.service.PortalBusinessService;
import com.backbase.portal.foundation.business.service.UserBusinessService;

public class PortalBusinessServices implements ApplicationListener<ApplicationContextEvent> {
    
    public static PortalBusinessServices INSTANCE;
    
    @Autowired
    public UserBusinessService userBusinessService;
    
    @Autowired
    public GroupBusinessService groupBusinessService;

    @Autowired
    public PortalBusinessService portalBusinessService;
    
    @Autowired
    public ItemBusinessService itemBusinessService;

    @Override
    public void onApplicationEvent(ApplicationContextEvent event) {
        if (event instanceof ContextRefreshedEvent) {
            INSTANCE = this;
        } else if (event instanceof ContextClosedEvent) {
            INSTANCE = null;
        }
    }
    
}
