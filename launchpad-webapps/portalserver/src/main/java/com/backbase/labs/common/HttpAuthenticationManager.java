package com.backbase.labs.common;

import java.util.Calendar;
import java.util.HashMap;

import javax.naming.InitialContext;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class HttpAuthenticationManager {

	String primaryusername = "";
	
	HashMap<String, HttpClient> httpclients = new HashMap<String, HttpClient>();
	XmlUtilities xmlutils = null;
	
	String proxyurl = "";
	int proxyport = 0;
	String proxyusername = "";
	String proxypassword = "";

	boolean cacheenabled = false;
	int cacheduration = 60;
	Cache usershttpsessions = null;
	
	String dbconnectionstring = "";
	String dbdriverclassname = "";
	String dbusername = "";
	String dbpassword = "";
		
	public HttpAuthenticationManager(String primaryUsername) throws Exception{
		this.primaryusername = primaryUsername;
		loadXMLUtilities();

		if(cacheenabled){
			this.cacheduration = Integer.parseInt(xmlutils.performXPathAsString("//cache/@durationseconds"));
			loadCache();
			setCache();
		}

	}
	
	private void loadXMLUtilities() throws Exception{
		try {
			if(xmlutils == null){
				InitialContext initialContext = new InitialContext();  
				String filelocation = (String) initialContext.lookup("java:comp/env/labs/authmanager");
				boolean useclasspath = false;
				if(filelocation.equals("")){
					useclasspath = true;
					filelocation = "authmanager.xml";
				}
				
				xmlutils = new XmlUtilities(filelocation, useclasspath);
			}
		
			//Read the proxy settings from teh config file 
			//if it was not overwriten with setProxy method
			if(this.proxyurl.equals("")){
				if(!xmlutils.performXPathAsString("//proxy/@url").equals("")){
					this.proxyurl = xmlutils.performXPathAsString("//proxy/@url");
					this.proxyport = Integer.parseInt(xmlutils.performXPathAsString("//proxy/@port"));
					this.proxyusername = xmlutils.performXPathAsString("//proxy/@username");
					this.proxypassword = xmlutils.performXPathAsString("//proxy/@password");
				}
			}

			//read cache enabled
			this.cacheenabled = xmlutils.performXPathAsString("//cache/@enabled").equals("true") ? true : false; 
			
			//Read the db settings
			this.dbconnectionstring =  xmlutils.performXPathAsString("//database/@connectionString");
			this.dbdriverclassname =  xmlutils.performXPathAsString("//database/@driverClassname");
			this.dbusername =  xmlutils.performXPathAsString("//database/@username");
			this.dbpassword =  xmlutils.performXPathAsString("//database/@password");
						
		} catch (Exception e) {
			throw new Exception("Unable to read Authentication Manager settings xml file.", e);
		}
	}
	
	private void loadCache(){
		CacheManager cachemanager = CacheManager.getInstance();
		usershttpsessions = cachemanager.getCache("com.backbase.labs.authmanager");
		if(usershttpsessions==null){
			usershttpsessions = new Cache("com.backbase.labs.authmanager", 200, false, false, this.cacheduration, this.cacheduration);
			cachemanager.addCache(usershttpsessions);
		} else {
			Element httpclientselement = usershttpsessions.get(primaryusername); 
			if(httpclientselement!=null){
				httpclients = (HashMap<String, HttpClient>) httpclientselement.getObjectValue();
			}
		}
	}

	private void setCache(){
		if(usershttpsessions==null){
			loadCache();
		}
		usershttpsessions.put(new Element(primaryusername,httpclients));
	}
	
	public void setProxy(String proxyUrl, int proxyPort, String proxyUsername, String proxyPassword){
		this.proxyurl = proxyUrl;
		this.proxyport = proxyPort;
		this.proxyusername = proxyUsername;
		this.proxypassword = proxyPassword;
	}

	public HttpClient getHttpClient(String AuthenticationClientName) throws Exception{

		HttpClient httpclient = null;

		if(httpclients.containsKey(AuthenticationClientName)){
			httpclient = httpclients.get(AuthenticationClientName);
			//validate if the client was created outside the cache duration bound
			Calendar now = Calendar.getInstance();
			int nosecondssincecreated = (int)(now.getTimeInMillis() - httpclient.getTimeCreated().getTimeInMillis())/1000; 
			if(nosecondssincecreated >= this.cacheduration){
				httpclient = null;
			}
		}
		
		
		if(httpclient==null) {

			Node providerxmlnode = null;
			String identityusername = "";
			String identitypassword = "";

			//Get the provider node
			try {
				//Get the provider node
				providerxmlnode = xmlutils.performXPathAsNode("//providers/provider[@name='" + AuthenticationClientName + "']");
				if(providerxmlnode==null) throw new Exception("Unable to find provider node");
				
			} catch (Exception e) {
				throw new Exception("Unable to read Authentication Manager settings xml file.", e);
			}
			
			//Get the login details for the provider
			try{
				String jsonrecord = this.getIdentityAsJSON(AuthenticationClientName);
				JSONObject jsonobject = (JSONObject) JSONSerializer.toJSON(jsonrecord);
				JSONArray jsondata = jsonobject.getJSONArray("data");
				if(jsondata.size()>0){
					JSONArray jsonidentityrecord = jsondata.getJSONArray(0);
					identityusername = jsonidentityrecord.getString(2);
					identitypassword = jsonidentityrecord.getString(3);
				} else {
					throw new Exception("Unable to locate identity information for user " + this.primaryusername + " for the authentication client name " + AuthenticationClientName);
				}
				
			} catch(Exception ex){
				throw new Exception("Unable to read identity information", ex);
			}
			
			
			//Get the client
			try{
				if(providerxmlnode!=null){
					if(this.proxyurl.equals("")){
						httpclient = new HttpClient();
					} else {
						httpclient = new HttpClient(this.proxyurl, this.proxyport, this.proxyusername, this.proxypassword);
					}
					
					String providertype = XmlUtilities.performXPathAsString(providerxmlnode, "@type");
					
					if(providertype.equals("form")) httpclient = formClientProvider(httpclient, providerxmlnode, identityusername, identitypassword);
					if(providertype.equals("basic")) httpclient = basicClientProvider(httpclient, providerxmlnode, identityusername, identitypassword);
					if(providertype.equals("token")) httpclient = tokenClientProvider(httpclient, providerxmlnode, identityusername, identitypassword);
	
					httpclients.put(AuthenticationClientName, httpclient);
				}
				
			} catch(Exception e){
				throw new Exception("Unable to create HTTP Client", e);
			}

			//Update the cache
			if(this.cacheenabled) setCache();

		}
		
		return httpclient;
	}
	
	
	
	//*** IDENTITY FUNCTIONS ***//
	
	public String getIdentityAsJSON(String authenticationClientName) throws Exception{
		DatabaseClient dbclient = new DatabaseClient(this.dbconnectionstring, this.dbdriverclassname, this.dbusername, this.dbpassword);
		return dbclient.executeQueryAsJSON("select * from identities where `identities`.`primaryusername` = '" + this.primaryusername + "' and authclientname = '" + authenticationClientName + "'");		
	}
	
	public String saveIdentityAsJSON(String authenticationClientName, String username, String password) throws Exception{
		DatabaseClient dbclient = new DatabaseClient(this.dbconnectionstring, this.dbdriverclassname, this.dbusername, this.dbpassword);
		return dbclient.executeQueryAsJSON("call saveIdentity('" + authenticationClientName + "','" + this.primaryusername + "','" + username + "','" + password + "')");
	}
	
	public String deleteIdentityAsJSON(String authenticationClientName) throws Exception{
		DatabaseClient dbclient = new DatabaseClient(this.dbconnectionstring, this.dbdriverclassname, this.dbusername, this.dbpassword);
		return dbclient.executeQueryAsJSON("call deleteIdentity('" + authenticationClientName + "','" + this.primaryusername + "')");
	}
	
	public String listIdentityAsJSON() throws Exception{
		DatabaseClient dbclient = new DatabaseClient(this.dbconnectionstring, this.dbdriverclassname, this.dbusername, this.dbpassword);
		return dbclient.executeQueryAsJSON("select * from identities where `identities`.`primaryusername` = '" + this.primaryusername + "'");
	}
	
	public HashMap<String,String> getListProviders() throws Exception{
		HashMap<String,String> listproviders = new HashMap<String,String>();
		try {
			NodeList nodelist = xmlutils.performXPathAsNodeList("//providers/provider");
			for(int i=0;i<nodelist.getLength();i++){
				Node node = nodelist.item(i);
				String providername = XmlUtilities.performXPathAsString(node, "@name");
				String providertitle =  XmlUtilities.performXPathAsString(node, "@title"); 
				listproviders.put(providername, providertitle);
			}
		} catch (Exception e) {
			throw new Exception("Unable to read Authentication Manager settings xml file.", e);
		}
		
		return listproviders;
	}
	
	
	//*** PROVIDERS ***//
	
	private HttpClient basicClientProvider(HttpClient httpClient, Node providerNode, String username, String password) throws Exception{
		httpClient.setCredentials(username, password);
		String defaulturl = XmlUtilities.performXPathAsString(providerNode, "defaulturl");
		if(!defaulturl.equals("")){
			httpClient.authenticate(defaulturl);
		}
		return httpClient;
	}
	
	private HttpClient formClientProvider(HttpClient httpClient, Node providerNode, String username, String password) throws Exception{
		
		//get the initial request.  this creates cookies etc.
		String defaulturl = XmlUtilities.performXPathAsString(providerNode, "defaulturl");
		if(!defaulturl.equals("")){
			httpClient.authenticate(defaulturl);
		}

		String loginurl = XmlUtilities.performXPathAsString(providerNode, "loginurl");
		if((loginurl==null)||(loginurl.equals(""))){
			throw new Exception("Login url required for Form Authentication in authmanager.xml");
		}
		
		//perform a json login
		String jsonloginpostdata = XmlUtilities.performXPathAsString(providerNode, "jsonpost"); //"{ \"token\": \"\", \"params\": { \"password\": \"" + "redwage2012" + "\", \"userName\": \"" + "sotoole" + "\" } }";
		if((jsonloginpostdata!=null)&&(!jsonloginpostdata.equals(""))){
			jsonloginpostdata = jsonloginpostdata.replace("@@username@@", username);
			jsonloginpostdata = jsonloginpostdata.replace("@@password@@", password);
			httpClient.authenticate(loginurl, jsonloginpostdata);
		} else {
			NodeList formfieldnodes = XmlUtilities.performXPathAsNodeList(providerNode, "formfields/fields");
			if(formfieldnodes.getLength()>0){
				HashMap<String, String> postParameters = new HashMap<String,String>();
				for(int i=0;i<formfieldnodes.getLength();i++){
					Node formfieldnode = formfieldnodes.item(i);
					String name = XmlUtilities.performXPathAsString(formfieldnode, "@name");
					String value = XmlUtilities.performXPathAsString(formfieldnode, "@value");
					value = value.replace("@@username@@", username);
					value = value.replace("@@password@@", password);
					postParameters.put(name, value);
				}
				httpClient.authenticate(loginurl, postParameters);
			} else {
				httpClient.authenticate(loginurl, username, password);
			}
		}
		return httpClient;
	}

	private HttpClient tokenClientProvider(HttpClient httpClient, Node providerNode, String username, String password){
		return httpClient;
	}

}
