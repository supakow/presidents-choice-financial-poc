package com.backbase.labs.ptc.provider.impl;
 
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.NotImplementedException;

import com.backbase.labs.common.DAMClient;
import com.backbase.labs.common.UGCClient;
import com.backbase.portal.ptc.config.element.DataProviderConfig;
import com.backbase.portal.ptc.context.MutableProxyContext;
import com.backbase.portal.ptc.provider.DataProvider;
import com.backbase.portal.ptc.request.ProxyRequest;
import com.backbase.portal.ptc.response.MutableProxyResponse;
 
public class DAMDataProvider implements DataProvider {
 
	@Override
	public MutableProxyResponse executeRequest(DataProviderConfig dataProviderConfig,
		MutableProxyContext mutableProxyContext, ProxyRequest proxyRequest) throws IOException {
		            
		MutableProxyResponse mpr = new MutableProxyResponse();
		        
		//We use the current logged in user
		String primaryusername = mutableProxyContext.getInternalParameterValue("__auth_username");
		String standardjsonresponse = "{\"state\": \"#state#\",\"message\": \"#message#\",\"data\": #data#}";
		
		boolean debug = false;
		if(mutableProxyContext.getInternalParameterValue("debug")!=null) debug = Boolean.parseBoolean(mutableProxyContext.getInternalParameterValue("debug"));
		
		try {
			String status = "success";
			String jsondata = "{}";

			//Get DAM PTC Settings
			String dam_path = dataProviderConfig.getParamValue("path");
			String dam_common_folder = dataProviderConfig.getParamValue("common_folder");

			//Check for anonymous
			boolean allowanonymous = Boolean.parseBoolean(dataProviderConfig.getParamValue("allowanonymous"));
			if(mutableProxyContext.getInternalParameterValue("widget_allowanonymous")!=null) allowanonymous = Boolean.parseBoolean(mutableProxyContext.getInternalParameterValue("widget_allowanonymous"));
			if((!allowanonymous)&&(primaryusername==null)) throw new Exception("Anonymous action not allowed");
			

			//Check to include common folder assets
			boolean includecommon = false;
			if(mutableProxyContext.getInternalParameterValue("widget_includecommon")!=null) includecommon = Boolean.parseBoolean(mutableProxyContext.getInternalParameterValue("widget_includecommon"));

			
			String reference = "";
			String identity = "anonymous";

			//Obtain location variables
			if(mutableProxyContext.getInternalParameterValue("reference")!=null) reference = mutableProxyContext.getInternalParameterValue("reference");
			if(primaryusername!=null) identity = primaryusername;
			
			DAMClient damclient = new DAMClient(dam_path, identity, reference);
			
			String filepath = mutableProxyContext.getInternalParameterValue("filepath");
			
			if(proxyRequest.getMethod().toLowerCase().equals("post")){
				//Create the final json response
				String jsonresponse = standardjsonresponse.replace("#state#", status);
				jsonresponse = jsonresponse.replace("#message#", "");
				jsonresponse = jsonresponse.replace("#data#", jsondata);
				mpr.addContentTypeHeader("application/json", "utf-8");
				mpr.setBody(jsonresponse);

				throw new NotImplementedException("Not implemented yet");
			} else if(proxyRequest.getMethod().toLowerCase().equals("delete")){
				//Create the final json response
				String jsonresponse = standardjsonresponse.replace("#state#", status);
				jsonresponse = jsonresponse.replace("#message#", "");
				jsonresponse = jsonresponse.replace("#data#", jsondata);
				mpr.addContentTypeHeader("application/json", "utf-8");
				mpr.setBody(jsonresponse);

				throw new NotImplementedException("Not implemented yet");
			} else {
				if(filepath==null){
					String entriesjson = "";

					if(includecommon){
						HashMap<String,String> commonfolders = damclient.getFoldersList(dam_path + "/" + dam_common_folder); 
						entriesjson = appendFolderJSON(damclient, entriesjson,commonfolders);
					}
					
					try{
						HashMap<String,String> userfolders = damclient.getFoldersList(); 
						entriesjson = appendFolderJSON(damclient, entriesjson, userfolders);
					} catch(Exception ex){
						if(debug) ex.printStackTrace();
					}
					jsondata = "[" + entriesjson + "]";
					
					//Create the final json response
					String jsonresponse = standardjsonresponse.replace("#state#", status);
					jsonresponse = jsonresponse.replace("#message#", "");
					jsonresponse = jsonresponse.replace("#data#", jsondata);
					mpr.addContentTypeHeader("application/json", "utf-8");
					mpr.setBody(jsonresponse);

				} else {
					
					HashMap<String, Object> document = damclient.getDocumentByPath(filepath);
					mpr.addContentTypeHeader((String) document.get("mimetype"), "utf-8");
					mpr.addHeaderValue("content-disposition", "inline; filename=\"" + document.get("filename") + "\"");
					InputStream content = (InputStream) document.get("content");
					mpr.setBodyBytes(IOUtils.toByteArray(content));
				}
			}
			
			mpr.setStatusCode(200);
			mpr.setStatusText("ok");
			
		} catch (Exception e) {
			String jsonresponse = standardjsonresponse.replace("#state#", "error");
			if(debug){
				jsonresponse = jsonresponse.replace("#message#", e.getMessage() + "\n" + org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(e));
			} else {
				jsonresponse = jsonresponse.replace("#message#", e.getMessage());
			}
			jsonresponse = jsonresponse.replace("#data#", "{}");
			mpr.addContentTypeHeader("application/json", "utf-8");
			mpr.setStatusCode(500);
			mpr.setStatusText(e.getLocalizedMessage());
			mpr.setBody(jsonresponse);
		}

		return mpr;
	}
	
	private String appendFolderJSON(DAMClient damclient, String entriesjson, HashMap<String,String> folders){
		for(String folder: folders.keySet()){
			String folderpath = folders.get(folder);
			if(!entriesjson.equals("")) entriesjson += ",";
			entriesjson += "{";
			entriesjson += "\"id\":\"" + folder.replace(" ", "_") + "\",";
			entriesjson += "\"title\":\"" + folder + "\",";
			entriesjson += "\"path\":\"" + folderpath + "\",";
			
			String documentsjson = "";
			try {
				HashMap<String,String> documentslist = damclient.getDocumentsList(folderpath);
				for(String documentname: documentslist.keySet()){
					if(!documentsjson.equals("")) documentsjson += ",";
					documentsjson += "{";
					documentsjson += "\"title\":\"" + documentname + "\",";
					documentsjson += "\"path\":\"" + documentslist.get(documentname) + "\"";
					documentsjson += "}";
				}
			} catch (Exception e) {
			}
			entriesjson += "\"documents\":[" + documentsjson + "]";
			entriesjson += "}";
		}	
		
		return entriesjson;
	}
	
	/*
	 The JSON definition tells us that you cannot have literal control characters in string literals, you must use an escape sequence such as \\b, \\r, \\n, or \\uXXXX where XXXX is a hex code for a Unicode "code point" (character).
	 */
	private String removeControlCharacters(String input){
		String response = input;
		response = response.replace("\n", "");
		response = response.replace("\b", "");
		response = response.replace("\r", "");
		return response;
	}
}
 