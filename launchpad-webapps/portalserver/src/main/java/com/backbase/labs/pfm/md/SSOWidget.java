package com.backbase.labs.pfm.md;
 
import java.io.IOException;

import net.sf.json.JSON;
import net.sf.json.xml.XMLSerializer;

import org.apache.http.Header;
import org.apache.http.message.BasicHeader;

import com.backbase.labs.common.HttpClient;
import com.backbase.portal.ptc.config.element.DataProviderConfig;
import com.backbase.portal.ptc.context.MutableProxyContext;
import com.backbase.portal.ptc.provider.DataProvider;
import com.backbase.portal.ptc.request.ProxyRequest;
import com.backbase.portal.ptc.response.MutableProxyResponse;
 
public class SSOWidget implements DataProvider {
 
	@Override
	public MutableProxyResponse executeRequest(DataProviderConfig dataProviderConfig,
		MutableProxyContext mutableProxyContext, ProxyRequest proxyRequest) throws IOException {
		            
		MutableProxyResponse mpr = new MutableProxyResponse();
		        
		//We use the current logged in user
		String primaryusername = mutableProxyContext.getInternalParameterValue("__auth_username");
		String standardjsonresponse = "{\"state\": \"#state#\",\"message\": \"#message#\",\"data\": #data#}";
		
		boolean debug = false;
		if(mutableProxyContext.getInternalParameterValue("debug")!=null) debug = Boolean.parseBoolean(mutableProxyContext.getInternalParameterValue("debug"));
		
		try {
			String status = "success";
			String jsondata = "{}";

			String ssourl = dataProviderConfig.getParamValue("SSO-URL");
			String client_id = dataProviderConfig.getParamValue("client_id");
			String apikey = dataProviderConfig.getParamValue("MD-API-Key");
			
			String widgetname = "master_widget";
			if(mutableProxyContext.getInternalParameterValue("widgetname")!=null) widgetname = mutableProxyContext.getInternalParameterValue("widgetname");
			
			//We only have one test user for now.  override the logged in username
			primaryusername = dataProviderConfig.getParamValue("debug_user");
			
			String urltosso = ssourl + client_id + "/users/" + primaryusername + "/urls/" + widgetname;
			Header[] headers = new Header[3];
			headers[0] = new BasicHeader("MD-API-KEY",apikey);
			headers[1] = new BasicHeader("Content-type","application/vnd.moneydesktop.sso.v3");
			headers[2] = new BasicHeader("Accept", "application/vnd.moneydesktop.sso.v3");
			
			HttpClient client = new HttpClient();
			String xmlstring = client.getHTMLHttpResponse(urltosso, headers);

			XMLSerializer serializer = new XMLSerializer();
			JSON json = serializer.read(xmlstring);
			jsondata = json.toString();
			
			//Create the final json response
			String jsonresponse = standardjsonresponse.replace("#state#", status);
			jsonresponse = jsonresponse.replace("#message#", "");
			jsonresponse = jsonresponse.replace("#data#", jsondata);
			mpr.addContentTypeHeader("application/json", "utf-8");
			mpr.setStatusCode(200);
			mpr.setStatusText("ok");
			mpr.setBody(jsonresponse);
			
		} catch (Exception e) {
			String jsonresponse = standardjsonresponse.replace("#state#", "error");
			if(debug){
				jsonresponse = jsonresponse.replace("#message#", e.getMessage() + "\n" + org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(e));
			} else {
				jsonresponse = jsonresponse.replace("#message#", e.getMessage());
			}
			jsonresponse = jsonresponse.replace("#data#", "{}");
			mpr.addContentTypeHeader("application/json", "utf-8");
			mpr.setStatusCode(500);
			mpr.setStatusText(e.getLocalizedMessage());
			mpr.setBody(jsonresponse);
		}

		return mpr;
	}
}
 