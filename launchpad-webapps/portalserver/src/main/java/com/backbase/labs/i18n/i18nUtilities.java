package com.backbase.labs.i18n;

import java.util.Locale;

public class i18nUtilities {

	
	public static Locale localeFromString(String locale){
		String language = "en";
		String country = "US";
		
		String[] localearr = locale.split("-");
		if(localearr.length>0){
			language = localearr[0];
		}
		if(localearr.length>1){
			language = localearr[1].toUpperCase();
		}
		
		return new Locale(language, country);
	}
		
}
