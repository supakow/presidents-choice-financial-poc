package com.backbase.labs.i18n;

import java.util.HashMap;
import java.util.Locale;

import com.backbase.labs.common.ContentServicesClient;

public class i18nContentServices implements i18nInterface {
	
	Locale locale = i18nUtilities.localeFromString("en_us");
	ContentServicesClient csclient = null;
	String rootpath = "/";

	public void initialize(String locale, String location) throws Exception {
		this.locale = i18nUtilities.localeFromString(locale);
		csclient = new ContentServicesClient();
		rootpath = location + "/" + locale;
	};

	 public String getResource(String resourceId){
		 return csclient.getContentByPath(getContentPath(resourceId));
	 };
	 
	 public boolean deleteResource(String resourceId){
		 return csclient.deleteContentByPath(getContentPath(resourceId));
	 };
	 
	 public String updateResource(String resourceId, String resourceValue) throws Exception{
		String returnvalue = null;
		if(csclient.updateContentByPath(this.rootpath, getFilename(resourceId), resourceValue)){
			 returnvalue = resourceValue;
		}
		return returnvalue;
	 };
	 
	 public HashMap<String,String> listResources(){
		 HashMap<String, String> listresources = new HashMap<String, String>();
		 try {
			 HashMap<String, String> cslistresources = csclient.getList(this.rootpath, true);
			
			 //Perform clean up by removing file extensions from responses
			 for(String key: cslistresources.keySet()){
				 String newkey = key;
				 if(key.lastIndexOf(".")>1){
					newkey = key.substring(0, key.lastIndexOf("."));
				 }
				 listresources.put(newkey, cslistresources.get(key));
			 }
			
		 } catch (Exception e) {
		 }
		 return listresources;
	 }
	 
	 private String getContentPath(String resourceId){
		 return this.rootpath + "/" + getFilename(resourceId);
	 }
	 private String getFilename(String resourceId){
		 return resourceId + ".txt";
	 }

}
