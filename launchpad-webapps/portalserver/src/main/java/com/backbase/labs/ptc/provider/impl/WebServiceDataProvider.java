package com.backbase.labs.ptc.provider.impl;
 
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
 
import com.backbase.labs.common.HttpClient;
import com.backbase.labs.common.StubWebServiceClient;
import com.backbase.portal.ptc.config.element.DataProviderConfig;
import com.backbase.portal.ptc.config.element.ParamConfig;
import com.backbase.portal.ptc.context.MutableProxyContext;
import com.backbase.portal.ptc.provider.DataProvider;
import com.backbase.portal.ptc.request.ProxyRequest;
import com.backbase.portal.ptc.response.MutableProxyResponse;
 
public class WebServiceDataProvider implements DataProvider {
 
    @Override
    public MutableProxyResponse executeRequest(DataProviderConfig dataProviderConfig,
		MutableProxyContext mutableProxyContext, ProxyRequest proxyRequest) throws IOException {
                
        MutableProxyResponse mpr = new MutableProxyResponse();
                
        //We use the current logged in user
        String primaryusername = mutableProxyContext.getInternalParameterValue("__auth_username");
 
        boolean debug = false;
        if(mutableProxyContext.getInternalParameterValue("debug")!=null) debug = Boolean.parseBoolean(mutableProxyContext.getInternalParameterValue("debug"));


        try {
        	//use all query string parameters as possible merge values
            Map<String, String[]> parameters = mutableProxyContext.getInternalParameters();
            HashMap<String,String> mergemap = new HashMap<String,String>();
            for(String key : parameters.keySet()){
            	mergemap.put(key, parameters.get(key)[0]);
            }
            
            //use all the data provider parameters as possible merge values
            Map<String, ParamConfig> dataparams = dataProviderConfig.getParamMap();
            for(String key: dataparams.keySet()){
            	ParamConfig confparam = dataparams.get(key);
            	if(!mergemap.containsKey(confparam.getName())){
                	mergemap.put(confparam.getName(), confparam.getValue());
            	}
            }
            
            //perform the webservice request
            StubWebServiceClient wsclient = new StubWebServiceClient();
            wsclient.setDebug(debug);
            wsclient.setStubFolderLocation(mutableProxyContext.getServletContext().getRealPath(dataProviderConfig.getParamValue("requestheadersfolder")));                        
    	   	String serviceresponse = wsclient.performPost(dataProviderConfig.getParamValue("serviceurl"), dataProviderConfig.getParamValue("soapaction"), dataProviderConfig.getParamValue("servicename"), mergemap);

    	   	
    	   	//Do some clean up
    	   	serviceresponse = serviceresponse.replace("<?xml version=\"1.0\" encoding=\"UTF-16\"?>", "");
    	   	serviceresponse = serviceresponse.replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>", "");
    	   	
    	   	
            //Create the final xml response
            mpr.addContentTypeHeader("text/xml", "utf-8");
            mpr.setStatusCode(200);
            mpr.setStatusText("ok");
            mpr.setBody(serviceresponse);

        } catch (Exception e) {
            if(debug){
                mpr.setBody("<error>" + e.getMessage() + "\n" + org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(e) + "</error>");
            } else {
                mpr.setBody("<error>" + e.getMessage() + "</error>");
            }
            mpr.addContentTypeHeader("text/xml", "utf-8");
            mpr.setStatusCode(500);
            mpr.setStatusText(e.getLocalizedMessage());
        }

        return mpr;
    }
}
 