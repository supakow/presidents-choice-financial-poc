package com.backbase.labs.targeting;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.backbase.labs.common.EnvironmentUtilities;
import com.backbase.portal.targeting.connectorframework.content.contexts.definition.ContextCollector;
import com.backbase.portal.targeting.connectorframework.content.contexts.definition.PossibleValue;
import com.backbase.portal.targeting.connectorframework.content.contexts.definition.ResultEntries;
import com.backbase.portal.targeting.connectorframework.content.contexts.definition.SegmentDefinition;
import com.backbase.portal.targeting.connectorframework.content.contexts.definition.SelectorDefinition;
import com.backbase.portal.targeting.rulesengine.type.RuleEngineTypes;


public class EnvironmentTargeting extends ContextCollector {

	static String contextId = "environment";
	static String description = "Environment"; 
	static String iconPath = "$(contextRoot)/static/com.backbase.labs/media/targeting-environment.png";
	static String label = "Environment";
	
	public EnvironmentTargeting() {
		super(contextId, description, iconPath, label);
	}


	//Define the segments
	@Override
	public List<SegmentDefinition> defineSegments(String arg0, Map<String, String> arg1) {
	    List<SegmentDefinition> segmentDefinitions = new ArrayList<SegmentDefinition>();
	    
	    //CPU Usage
	    segmentDefinitions.add(new SegmentDefinition("cpuhigh", "CPU High"));
	    segmentDefinitions.add(new SegmentDefinition("cpumedium", "CPU Medium"));
	    segmentDefinitions.add(new SegmentDefinition("cpulow", "CPU Low"));

	    return segmentDefinitions;
	}


	//Define the conditions
	@Override
	public List<SelectorDefinition> defineSelectors(String arg0, Map<String, String> arg1) {
        List<SelectorDefinition> selectorDefinitions = new ArrayList<SelectorDefinition>();
        
        //AB TESTING
        SelectorDefinition abtest = new SelectorDefinition("abtest", RuleEngineTypes.RANGE, "A/B Percentage");
        abtest.setMin(0);
        abtest.setMax(100);
        abtest.setStep(10);
        selectorDefinitions.add(abtest);

        
        //Has Internet connection
        SelectorDefinition connected = new SelectorDefinition("connected", RuleEngineTypes.BOOLEAN, "Internet Connected");
        selectorDefinitions.add(connected);
        
        
        return selectorDefinitions;
	}


	//This is the execution of the actual rules
	@Override
	public ResultEntries executeTask(Map<String, String> requestMap, ResultEntries resultEntries) {

		EnvironmentUtilities envutils = new EnvironmentUtilities();
		String username = requestMap.get("session.authentication.username");
		
		//AB TESTING
		int abtest = envutils.getABTestVarient();
		resultEntries.addSelectorEntry("abtest", String.valueOf(abtest));
		
		//CPU
		double threadcpuperc = envutils.getCPUPercentage();
		if(threadcpuperc<=25){
			resultEntries.addSegmentEntry("cpulow");
		} else if(threadcpuperc>=75){
			resultEntries.addSegmentEntry("cpuhigh");
		} else {
			resultEntries.addSegmentEntry("cpumedium");
		}
		
		
		//Internet connected
		boolean connected = envutils.getInternetConnected();
		resultEntries.addSelectorEntry("connected", String.valueOf(connected));

		
	    return resultEntries;		
	}


}
