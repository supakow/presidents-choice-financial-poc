package com.backbase.labs.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

import com.backbase.portal.foundation.business.service.GroupBusinessService;
import com.backbase.portal.foundation.business.service.UserBusinessService;
import com.backbase.portal.foundation.commons.exceptions.FoundationDataException;
import com.backbase.portal.foundation.commons.exceptions.FoundationReadOnlyException;
import com.backbase.portal.foundation.commons.exceptions.FoundationRuntimeException;
import com.backbase.portal.foundation.commons.exceptions.ItemAlreadyExistsException;
import com.backbase.portal.foundation.commons.exceptions.ItemNotFoundException;
import com.backbase.portal.foundation.domain.model.Group;
import com.backbase.portal.foundation.domain.model.Role;
import com.backbase.portal.foundation.domain.model.User;

public class AuthUtilities {
    static ch.qos.logback.classic.Logger log = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger("com.backbase.crowd.BackbaseAuthUtils");

    private UserBusinessService userBusinessService;
	private GroupBusinessService groupBusinessService;
	private boolean debug = false;
	private String auditsystemusername = "system";
	
	public AuthUtilities(UserBusinessService userBusinessService, GroupBusinessService groupBusinessService){
		this.groupBusinessService = groupBusinessService;
		this.userBusinessService = userBusinessService;
	}
	
	public void setSystemUserContext(){
		User systemUser = new User(); 
		systemUser.setUsername(auditsystemusername); 
		Authentication auth = new PreAuthenticatedAuthenticationToken(systemUser, null); 
		SecurityContextHolder.getContext().setAuthentication(auth);
	}
	
	public User validateUserInBackbase(String username, String password, List<Group> groups){
		//Set initial user for Backbase Audit purposes
		setSystemUserContext();

		User user = null;
		try {
			
			//Get the user from Backbase
			user = userBusinessService.getUser(username);
			//Get their current groups as a collection
			Collection<Group> toRemove = new ArrayList<Group>(user.getGroups());
			//Remove the groups they still have from that collection
			toRemove.removeAll(groups);
			//Iterate through the removal, and make that call
			Iterator<Group> removal = toRemove.iterator();
			while(removal.hasNext()){
				String thisGroup=removal.next().getName();
				userBusinessService.removeUserFromGroup(thisGroup,username);
				if(debug) System.out.println("Removed from group: "+thisGroup);
			}
			//Now set the user to be a member of the correct groups
			user.setGroups(groups);
			try {
				userBusinessService.updateUser(username, user);
			}
			catch (FoundationDataException e) {
				throw new FoundationRuntimeException(e);
			}
			catch (FoundationReadOnlyException e) {
				throw new FoundationRuntimeException(e);
			}
		}
		catch (ItemNotFoundException e) {

			// Create new user
			if(debug) System.out.println("Creating new user: "+username);
			user = new User();
			user.setUsername(username);
			user.setPassword(password);
			user.setEnabled(true);
			user.getGroups().addAll(groups);

			try {
				userBusinessService.createUser(user);
			} catch (FoundationDataException e1) {
				throw new FoundationRuntimeException(e1);
			} catch (ItemNotFoundException e1) {
				throw new FoundationRuntimeException(e1);
			} catch (ItemAlreadyExistsException e1) {
				throw new FoundationRuntimeException(e1);
			}
		}

		return user;
	}
	
	public List<Group> validateGroupsInBackbase( String listgroups ){
		return validateGroupsInBackbase(listgroups, true);
	}	
	public List<Group> validateGroupsInBackbase( String listgroups, boolean createGroup){
		String[] groupsArray=listgroups.split(",");
		List<String> groupsList=Arrays.asList(groupsArray);
		List<Group> groups = new ArrayList<Group>();
 
		if( groupsList != null && !groupsList.isEmpty()){
			Iterator<String> iterator = groupsList.iterator();
			if(debug) System.out.println("Groups to map:");
			while(iterator.hasNext()){
				String groupName = iterator.next();
				String[] backbaseGroups={"admin","user","anonymous","sys2sys"};
				Group group = null;
				try {
					group = groupBusinessService.getGroup(groupName);
				}
				catch (ItemNotFoundException e) {
					if(debug) System.out.println("Could not find the " + groupName + " group, creating now.");

					if(createGroup){
						//Set initial user for Backbase Audit purposes
						setSystemUserContext();
						
						group = new Group();
						group.setDescription(groupName);
						group.setName(groupName);
						group.setRole( Role.USER );
						try {
							groupBusinessService.createGroup(group);
	 					}
						catch (ItemAlreadyExistsException e1) {
							throw new FoundationRuntimeException(e1);
						}
						catch (FoundationDataException e1) {
							throw new FoundationRuntimeException(e1);
						}
					}
				}
				if(group!=null) groups.add( group );
			}
		}
		else{
			if(debug) System.out.println("No groups found for this user");
		}
 
		return groups;
	}
	
	//Create or append a existing list of groups with the backbase required system groups
    public String getBackbaseSystemGroups(String groups, boolean isAdmin, boolean isSys2Sys, boolean isAuthenticated) {
        if(groups!="")
        {
            String groupslist = groups;
            if(isSys2Sys){
            	groupslist = "sys2sys";
            } else {
            	if(isAdmin){
            		groupslist = "admin";
	            } else {
	            	groupslist += ",user";
	            }
            }
            groupslist=groupslist.replace(" ", "");
            return groupslist;
        }
        else
        {
            if(isAuthenticated)
            {
                if(isSys2Sys){
                	return "sys2sys";
                } else {
                	if(isAdmin){
                		return "admin";
    	            } else {
    	            	return "user";
    	            }
                }
            }
            else
            {
                return "anonymous";
            }
        }
    }


	public void setUserBusinessService(UserBusinessService userBusinessService) {
		this.userBusinessService = userBusinessService;
	}
	
	public void setGroupBusinessService(GroupBusinessService groupBusinessService) {
		this.groupBusinessService = groupBusinessService;
	}

}
