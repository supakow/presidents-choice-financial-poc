package com.backbase.labs.ptc.provider.impl;
 
import java.io.IOException;
 
import com.backbase.labs.common.HttpClient;
import com.backbase.portal.ptc.config.element.DataProviderConfig;
import com.backbase.portal.ptc.context.MutableProxyContext;
import com.backbase.portal.ptc.provider.DataProvider;
import com.backbase.portal.ptc.request.ProxyRequest;
import com.backbase.portal.ptc.response.MutableProxyResponse;
 
public class SAPDataProvider implements DataProvider {
 
    @Override
    public MutableProxyResponse executeRequest(DataProviderConfig dataProviderConfig,
		MutableProxyContext mutableProxyContext, ProxyRequest proxyRequest) throws IOException {
                
        MutableProxyResponse mpr = new MutableProxyResponse();
                
        //We use the current logged in user
        String primaryusername = mutableProxyContext.getInternalParameterValue("__auth_username");
 
        String standardjsonresponse = "{\"state\": \"#state#\",\"message\": \"#message#\",\"data\": #data#}";

        boolean debug = false;
        if(mutableProxyContext.getInternalParameterValue("debug")!=null) debug = Boolean.parseBoolean(mutableProxyContext.getInternalParameterValue("debug"));


        try {
    		String status = "success";
            String jsondata = "{}";

        	String jco_client_lang = dataProviderConfig.getParamValue("jco.client.lang");
    	   	String jco_client_client = dataProviderConfig.getParamValue("jco.client.client");
       	   	String jco_client_passwd = dataProviderConfig.getParamValue("jco.client.passwd");
    	   	String jco_client_user = dataProviderConfig.getParamValue("jco.client.user");
    	   	String jco_client_sysnr = dataProviderConfig.getParamValue("jco.client.sysnr");
    	   	String jco_client_ashost = dataProviderConfig.getParamValue("jco.client.ashost");
       	   	String jco_destination_peak_limit = dataProviderConfig.getParamValue("jco.destination.peak_limit");
    	   	String jco_destination_pool_capacity = dataProviderConfig.getParamValue("jco.destination.pool_capacity");

    	   	
    	   	HttpClient httpclient = new HttpClient();
    	   	jsondata = httpclient.getHTMLHttpResponse("http://localhost:8080/portalserver/static/alpargatas/stubs/json-table.json");
    	   	
            //Create the final json response
            String jsonresponse = standardjsonresponse.replace("#state#", status);
            jsonresponse = jsonresponse.replace("#message#", "");
            jsonresponse = jsonresponse.replace("#data#", jsondata);
            mpr.addContentTypeHeader("application/json", "utf-8");
            mpr.setStatusCode(200);
            mpr.setStatusText("ok");
            mpr.setBody(jsonresponse);

        } catch (Exception e) {
            String jsonresponse = standardjsonresponse.replace("#state#", "error");
            if(debug){
                jsonresponse = jsonresponse.replace("#message#", e.getMessage() + "\n" + org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(e));
            } else {
                jsonresponse = jsonresponse.replace("#message#", e.getMessage());
            }
            jsonresponse = jsonresponse.replace("#data#", "{}");
            mpr.addContentTypeHeader("application/json", "utf-8");
            mpr.setStatusCode(500);
            mpr.setStatusText(e.getLocalizedMessage());
            mpr.setBody(jsonresponse);
        }

        return mpr;
    }
}
 