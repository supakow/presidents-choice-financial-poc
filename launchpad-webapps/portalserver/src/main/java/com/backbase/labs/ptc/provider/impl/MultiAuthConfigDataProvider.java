package com.backbase.labs.ptc.provider.impl;

import java.io.IOException;
import java.util.HashMap;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;

import com.backbase.labs.common.DatabaseClient;
import com.backbase.labs.common.HttpAuthenticationManager;
import com.backbase.portal.foundation.integration.context.BackbaseContextInitializer;
import com.backbase.portal.ptc.config.element.DataProviderConfig;
import com.backbase.portal.ptc.context.MutableProxyContext;
import com.backbase.portal.ptc.provider.DataProvider;
import com.backbase.portal.ptc.request.ProxyRequest;
import com.backbase.portal.ptc.response.MutableProxyResponse;

public class MultiAuthConfigDataProvider implements DataProvider {

	@Override
	public MutableProxyResponse executeRequest(DataProviderConfig dataProviderConfig,
			MutableProxyContext mutableProxyContext, ProxyRequest proxyRequest) throws IOException {

		MutableProxyResponse mpr = new MutableProxyResponse();
		String jsonresponse = "";
		String type = mutableProxyContext.getInternalParameterValue("type");
	
		try{
			if((type==null)||(type.equals(""))) throw new Exception("Type of action not defined");

			//Get the current logged in username here
			String primaryusername = mutableProxyContext.getInternalParameterValue("__auth_username");

			HttpAuthenticationManager ham = new HttpAuthenticationManager(primaryusername);
			
			if(type.equals("get")){
				String authclientname = mutableProxyContext.getInternalParameterValue("authclientname");
				if((authclientname==null)||(authclientname.equals(""))) throw new Exception("authclientname not defined");
				jsonresponse = ham.getIdentityAsJSON(authclientname);

			} else if(type.equals("save")){
				String authclientname = mutableProxyContext.getInternalParameterValue("authclientname");
				String username = mutableProxyContext.getInternalParameterValue("username");
				String password = mutableProxyContext.getInternalParameterValue("password");

				if((authclientname==null)||(authclientname.equals(""))) throw new Exception("authclientname not defined");
				if((username==null)||(username.equals(""))) throw new Exception("username not defined");
				if(password==null) throw new Exception("password not defined");

				jsonresponse = ham.saveIdentityAsJSON(authclientname, username, password);

			} else if(type.equals("delete")){
				String authclientname = mutableProxyContext.getInternalParameterValue("authclientname");
				if((authclientname==null)||(authclientname.equals(""))) throw new Exception("authclientname not defined");
				jsonresponse = ham.deleteIdentityAsJSON(authclientname);
			} else if(type.equals("config")){
				HashMap<String,String> listproviders = ham.getListProviders();
				for(String key:listproviders.keySet()){
					if(!jsonresponse.equals("")) jsonresponse += ",";
					jsonresponse += "[\"" + key + "\",\"" + listproviders.get(key) + "\"]";
				}
				jsonresponse = "[" + jsonresponse + "]";
			} else {
				jsonresponse = ham.listIdentityAsJSON();
			}
			mpr.setBody("{\"success\":true,\"message\":\"" + type + " performed for " + primaryusername + "\", \"records\":" + jsonresponse + "}");
			mpr.setStatusCode(200);
		
		} catch(Exception ex){
			mpr.setBody("{\"success\":false,\"message\":\"" + ex.getMessage() + "\"}");
			mpr.setStatusCode(500);
		}

		return mpr;
	}

}
