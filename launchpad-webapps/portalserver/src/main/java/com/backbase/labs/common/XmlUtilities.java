package com.backbase.labs.common;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.SAXException;

public class XmlUtilities {

	Document xmldocument = null;
	
	public XmlUtilities(String fileName, boolean useClassPath) throws ClassNotFoundException, SAXException, IOException, ParserConfigurationException{
		xmldocument = loadXMLDocumentFromDisk(fileName, useClassPath);
	}
	
	public XmlUtilities(String xmlString, String defaultNS) throws ParserConfigurationException, SAXException, IOException{
		InputStream is = new java.io.ByteArrayInputStream(xmlString.getBytes());
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		xmldocument = builder.parse(is);		
	}
	
	public String performXPathAsString(String xPath) throws XPathExpressionException{
		return performXPathAsString(this.xmldocument, xPath);
	}
	public static String performXPathAsString(Document xmlDocument, String xPath) throws XPathExpressionException{
		String responsevalue = "";
		
	    NodeList nodes = performXPathAsNodeList(xmlDocument, xPath);
	    for (int i = 0; i < nodes.getLength(); i++) {
	    	responsevalue += nodes.item(i).getNodeValue(); 
	    }	
	    
	    return responsevalue;
	}
	
	public String performXPathAsTextContent(String xPath) throws XPathExpressionException{
		return performXPathAsTextContent(this.xmldocument, xPath);
	}
	public static String performXPathAsTextContent(Document xmlDocument, String xPath) throws XPathExpressionException{
		String responsevalue = "";
		
	    NodeList nodes = performXPathAsNodeList(xmlDocument, xPath);
	    for (int i = 0; i < nodes.getLength(); i++) {
	    	responsevalue += nodes.item(i).getTextContent(); 
	    }	
	    
	    return responsevalue;
	}
	
	public static String performXPathAsTextContent(Node node, String xPath) throws XPathExpressionException{
		String responsevalue = "";
		
	    NodeList nodes = performXPathAsNodeList(node, xPath);
	    for (int i = 0; i < nodes.getLength(); i++) {
	    	responsevalue += nodes.item(i).getTextContent(); 
	    }	
	    
	    return responsevalue;
	}

	public static String performXPathAsString(Node node, String xPath) throws XPathExpressionException{
		String responsevalue = "";
		
		XPathFactory factory = XPathFactory.newInstance();
	    XPath xpath = factory.newXPath();
	    XPathExpression expr = xpath.compile(xPath);

	    Object result = expr.evaluate(node, XPathConstants.NODESET);
	    NodeList nodes = (NodeList) result;
	    for (int i = 0; i < nodes.getLength(); i++) {
	    	String nodevalue = nodes.item(i).getNodeValue();
	    	if(nodevalue==null){
	    		responsevalue += nodes.item(i).getTextContent();
	    	} else {
	    		responsevalue += nodevalue;
	    	}
	    }		    
	    return responsevalue;
	}

	public NodeList performXPathAsNodeList(String xPath) throws XPathExpressionException{
		return performXPathAsNodeList(this.xmldocument, xPath);
	}
	public static NodeList performXPathAsNodeList(Document xmlDocument, String xPath) throws XPathExpressionException{
		XPathFactory factory = XPathFactory.newInstance();
	    XPath xpath = factory.newXPath();
	    XPathExpression expr = xpath.compile(xPath);

	    Object result = expr.evaluate(xmlDocument, XPathConstants.NODESET);
	    return (NodeList) result;
	}
	public static NodeList performXPathAsNodeList(Node node, String xPath) throws XPathExpressionException{
		XPathFactory factory = XPathFactory.newInstance();
	    XPath xpath = factory.newXPath();
	    XPathExpression expr = xpath.compile(xPath);

	    Object result = expr.evaluate(node, XPathConstants.NODESET);
	    return (NodeList) result;
	}


	public Node performXPathAsNode(String xPath) throws XPathExpressionException{
		return performXPathAsNode(this.xmldocument, xPath);
	}
	public static Node performXPathAsNode(Document xmlDocument, String xPath) throws XPathExpressionException{
		XPathFactory factory = XPathFactory.newInstance();
	    XPath xpath = factory.newXPath();
	    XPathExpression expr = xpath.compile(xPath);

	    Object result = expr.evaluate(xmlDocument, XPathConstants.NODE);
	    return (Node) result;
	}

	
	public static Document loadXMLDocumentFromDisk(String fileName, boolean useClassPath) throws SAXException, IOException, ParserConfigurationException, ClassNotFoundException{
		if(useClassPath){
			InputStream is = XmlUtilities.class.getResourceAsStream(fileName);
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			return builder.parse(is);
		} else {
			FileInputStream is = new FileInputStream(fileName);
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			return builder.parse(is);
		}
	}
	
    public String getXML(String xPath) throws XPathExpressionException{
        Node node = performXPathAsNode(xPath);
        return innerXml(node);
   }
       
   public String innerXml(Node node) {
       DOMImplementationLS lsImpl = (DOMImplementationLS)node.getOwnerDocument().getImplementation().getFeature("LS", "3.0");
       LSSerializer lsSerializer = lsImpl.createLSSerializer();
       NodeList childNodes = node.getChildNodes();
       StringBuilder sb = new StringBuilder();
       for (int i = 0; i < childNodes.getLength(); i++) {
          sb.append(lsSerializer.writeToString(childNodes.item(i)));
       }
       return sb.toString(); 
   }
   
   public Document getXmlDocument(){
       return this.xmldocument;
	}
	
	public InputStream getInputStream() throws TransformerConfigurationException, TransformerException, TransformerFactoryConfigurationError{
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		Source xmlSource = new DOMSource(this.xmldocument);
		Result outputTarget = new StreamResult(outputStream);
		TransformerFactory.newInstance().newTransformer().transform(xmlSource, outputTarget);
		InputStream is = new ByteArrayInputStream(outputStream.toByteArray());
		return is;
	}

	
    public static String removeXmlStringNamespaceAndPreamble(String xmlString) {
  	  return xmlString.replaceAll("(<\\?[^<]*\\?>)?", ""). /* remove preamble */
  	  replaceAll("xmlns.*?(\"|\').*?(\"|\')", "") /* remove xmlns declaration */
  	  .replaceAll("(<)(\\w+:)(.*?>)", "$1$3") /* remove opening tag prefix */
  	  .replaceAll("(</)(\\w+:)(.*?>)", "$1$3"); /* remove closing tags prefix */
  	}     

}
