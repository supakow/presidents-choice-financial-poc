package com.backbase.labs.common;

import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;
import java.lang.management.ThreadMXBean;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;

import org.hyperic.sigar.Sigar;
import org.hyperic.sigar.SigarException;

public class EnvironmentUtilities {

	
	public EnvironmentUtilities(){
		
	}
	
	
	public static int getCPUPercentage(){
		int percentage = 0;
		try {
			Sigar sigar = new Sigar();
			double percentagevalue = sigar.getCpuPerc().getCombined();
			percentage = (int) Math.round(percentagevalue*100);
		} catch (SigarException e) {
		}
		return percentage;
	}
	
	
	public static int getABTestVarient(){
		return 20;
	}

	public static boolean getInternetConnected(){
		try {
            //make a URL to a known source
            URL url = new URL("http://www.google.com");

            //open a connection to that source
            HttpURLConnection urlConnect = (HttpURLConnection)url.openConnection();

            //trying to retrieve data from the source. If there
            //is no connection, this line will fail
            Object objData = urlConnect.getContent();

        } catch (Exception e) {              
            e.printStackTrace();
            return false;
        }

        return true;
	}

}
