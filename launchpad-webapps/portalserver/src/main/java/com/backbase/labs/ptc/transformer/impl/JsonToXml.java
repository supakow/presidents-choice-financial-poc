package com.backbase.labs.ptc.transformer.impl;

import net.sf.json.JSON;
import net.sf.json.JSONSerializer;
import net.sf.json.xml.XMLSerializer;

import com.backbase.portal.ptc.context.MutableProxyContext;
import com.backbase.portal.ptc.request.ProxyRequest;
import com.backbase.portal.ptc.response.MutableProxyResponse;
import com.backbase.portal.ptc.transform.response.AbstractResponseTransformer;

public class JsonToXml extends AbstractResponseTransformer{
	
	public void transform(MutableProxyContext proxyContext,ProxyRequest proxyRequest, MutableProxyResponse proxyResponse){
		
		String jsonString = proxyResponse.getBody();
		
		XMLSerializer serializer = new XMLSerializer();
		
		serializer.setRootName("JSONObject");
		
		serializer.setTypeHintsEnabled(true);
		serializer.setRemoveNamespacePrefixFromElements(false);
		JSON json = JSONSerializer.toJSON(jsonString);
		String xml = serializer.write(json);

		proxyResponse.setBody(xml);
		
		proxyResponse.addContentTypeHeader("text/html","UTF-8");
		
	}
	
}