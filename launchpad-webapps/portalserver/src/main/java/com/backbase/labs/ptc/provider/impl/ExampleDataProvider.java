package com.backbase.labs.ptc.provider.impl;

import java.io.IOException;
import java.io.PrintWriter;

import com.backbase.labs.common.HttpAuthenticationManager;
import com.backbase.labs.common.HttpClient;
import com.backbase.portal.ptc.config.element.DataProviderConfig;
import com.backbase.portal.ptc.context.MutableProxyContext;
import com.backbase.portal.ptc.request.ProxyRequest;
import com.backbase.portal.ptc.response.MutableProxyResponse;

public class ExampleDataProvider implements com.backbase.portal.ptc.provider.DataProvider {

	@Override
	public MutableProxyResponse executeRequest(DataProviderConfig dataProviderConfig,
			MutableProxyContext mutableProxyContext, ProxyRequest proxyRequest) throws IOException {
		
		MutableProxyResponse mpr = new MutableProxyResponse();
		
		//We use the current logged in user
		String primaryusername = mutableProxyContext.getInternalParameterValue("__auth_username");

		try {

			String responsexml = "<items><item title='Feature 1'>Lorem ipsum dolor sit amet</item><item title='Feature 2'>consectetur adipisicing elit</item><item title='Feature 3'>sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</item></items>";
			
			//Obtain the http client through the multi authentication manager from Backbase
			//HttpAuthenticationManager ham = new HttpAuthenticationManager(primaryusername);
		
			 //facebook, sf, olb, cc, basic, etc
			//HttpClient httpclient = ham.getHttpClient("basic");

			//String responsexml = httpclient.getHTMLHttpResponse("http://localhost:8080/portalserver/static/com.backbase.labs/rest/examplePipe.xml"); 
			
			mpr.setStatusText("ok");
			mpr.setStatusCode(200);
			mpr.setBody(responsexml);
			
		} catch (Exception e) {
			mpr.setStatusCode(500);
			mpr.setBody(e.getMessage() + "\n" + org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(e));
		}
		

		
		return mpr;
	}


}
