package com.backbase.labs.ptc.provider.impl;
 
import java.io.IOException;
import java.util.List;

import org.apache.commons.lang.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;

import com.backbase.labs.common.AuthUtilities;
import com.backbase.labs.common.PortalBusinessServices;
import com.backbase.portal.foundation.business.service.GroupBusinessService;
import com.backbase.portal.foundation.business.service.UserBusinessService;
import com.backbase.portal.foundation.domain.model.Group;
import com.backbase.portal.foundation.domain.model.User;
import com.backbase.portal.ptc.config.element.DataProviderConfig;
import com.backbase.portal.ptc.context.MutableProxyContext;
import com.backbase.portal.ptc.provider.DataProvider;
import com.backbase.portal.ptc.request.ProxyRequest;
import com.backbase.portal.ptc.response.MutableProxyResponse;


public class SecurityDataProvider implements DataProvider {
 

    @Override
    public MutableProxyResponse executeRequest(DataProviderConfig dataProviderConfig,
                    MutableProxyContext mutableProxyContext, ProxyRequest proxyRequest) throws IOException {
            
        MutableProxyResponse mpr = new MutableProxyResponse();
        
        //We use the current logged in user
        String primaryusername = mutableProxyContext.getInternalParameterValue("__auth_username");
 
        String standardjsonresponse = "{\"state\": \"#state#\",\"message\": \"#message#\",\"data\": #data#}";

        boolean debug = false;
        if(mutableProxyContext.getInternalParameterValue("debug")!=null) debug = Boolean.parseBoolean(mutableProxyContext.getInternalParameterValue("debug"));
            
            try {
 
            	//Read the type of action to perform
        	String action = mutableProxyContext.getInternalParameterValue("action");
        	if(action==null||action.equals("")) throw new Exception("action parameter required");
        	action = action.toLowerCase();
        	
        	
            String jsondata = "{}";
                    
            UserBusinessService userBusinessService = PortalBusinessServices.INSTANCE.userBusinessService;
            GroupBusinessService groupBusinessService = PortalBusinessServices.INSTANCE.groupBusinessService;
            AuthUtilities authutils = new AuthUtilities(userBusinessService, groupBusinessService);
            
            if(action.equals("createuser")){
            	String username = mutableProxyContext.getInternalParameterValue("username");
            	if(username==null||username.equals("")) throw new Exception("username parameter required");
            	String password = mutableProxyContext.getInternalParameterValue("password");
            	if(password==null||password.equals("")) throw new Exception("password parameter required");
            	String groups = mutableProxyContext.getInternalParameterValue("groups");  //", seperated"
            	if(groups==null||groups.equals("")) throw new Exception("groups parameter required");
            	
            	List<Group> bbgroups = authutils.validateGroupsInBackbase(groups, false);
            	User bbuser = authutils.validateUserInBackbase(username, password, bbgroups);
            	
            	jsondata = jsonFromUser(bbuser);
            	
            } else {
            	throw new NotImplementedException("Action " + action + " not supported.");
            }
            
            if(debug) System.out.println("jsondata: " + jsondata);
            
            //Create the final json response
            String jsonresponse = standardjsonresponse.replace("#state#", "success");
            jsonresponse = jsonresponse.replace("#message#", "");
            jsonresponse = jsonresponse.replace("#data#", jsondata);
            mpr.addContentTypeHeader("application/json", "utf-8");
            mpr.setStatusCode(200);
            mpr.setStatusText("ok");
            mpr.setBody(jsonresponse);
                
        } catch (Exception e) {
            String jsonresponse = standardjsonresponse.replace("#state#", "error");
            jsonresponse = jsonresponse.replace("#message#", e.getMessage() + "\n" + org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(e));
            jsonresponse = jsonresponse.replace("#data#", "{}");
            mpr.addContentTypeHeader("application/json", "utf-8");
            mpr.setStatusCode(500);
            mpr.setStatusText(e.getLocalizedMessage());
            mpr.setBody(jsonresponse);
        }
            
        return mpr;
    }

    public String jsonFromUser(User user){
    	String json = "{";
    	json += "\"username\" : \"" + user.getUsername() + "\",";
    	//json += "\"password\" : \"" + user.getPassword() + "\"";
    	json += "\"account_non_expired\" : \"" + user.isAccountNonExpired() + "\",";
    	json += "\"account_non_locked\" : \"" + user.isAccountNonLocked() + "\",";
    	json += "\"credentials_non_expired\" : \"" + user.isCredentialsNonExpired() + "\",";
    	json += "\"last_logged_in\" : \"" + user.getLastLoggedIn() + "\",";
    	json += "\"groups\" : [";
    	for(Group group : user.getGroups()){
    		json += "\"" + group.getName() + "\"";
    	}
    	json += "]";
    	json += "}";
    	return json;
    }
    
}
 