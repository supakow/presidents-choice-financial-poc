package com.backbase.labs.common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;


public class HttpClient extends DefaultHttpClient {

	Calendar timecreated = Calendar.getInstance();
	
	String username = null;
	String password = null;
	
	
	boolean authenticated = false;
	
	HashMap<String, String> cacheddata = new HashMap<String,String>();
	
	public HttpClient() {
		super();
	}
	
	public HttpClient(ClientConnectionManager conman, HttpParams params){
		super(conman, params);
	}

	/****************************
	 * PROXY 
	 ****************************/

	public HttpClient(String proxyUrl, int proxyPort, String proxyUsername, String proxyPassword){
		super();
		HttpHost proxy = new HttpHost(proxyUrl, proxyPort);
		Credentials proxycreds = new UsernamePasswordCredentials(proxyUsername, proxyPassword);
		this.getCredentialsProvider().setCredentials(new AuthScope(proxyUrl, proxyPort), proxycreds);
		this.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);
	}
	

	/****************************
	 * CREDENTIALS  
	 ****************************/
	
	//This does normal basic authentication
	public void setCredentials(String userName, String password){
		this.username = userName;
		this.password = password;
		Credentials defaultcreds = new UsernamePasswordCredentials(userName, password);
		this.getCredentialsProvider().setCredentials(AuthScope.ANY, defaultcreds);
	}
	
	
	/****************************
	 * AUTHENTICATE  
	 ****************************/
	//This will do a form login, and pass a username and password input field
	public void authenticate(String loginUrl, String userName, String password) throws Exception {
		this.username = userName;
		this.password = password;

		HashMap<String, String> params = new HashMap<String, String>();
		params.put("username", userName);
		params.put("password", password);
		this.authenticate(loginUrl, params);
	}

	//This will do a form login.  Send the manual login forms.
	public void authenticate(String loginUrl, HashMap<String, String> postParameters) throws Exception {
		this.postHTMLHttpResponse(loginUrl, "text/html",  postParameters);
	}

	//this is intended for json login
	public void authenticate(String loginUrl, String JSONPostData) throws Exception {
		this.postJSONHttpResponse(loginUrl, JSONPostData);
	}

	//This will do a login using basic auth by getting a login url page.
	public void authenticate(String loginUrl) throws Exception {
		this.getHTMLHttpResponse(loginUrl);
	}
	
	/****************************
	 * POST AND GET  
	 ****************************/
	
	//normal html get
	public String getHTMLHttpResponse(String URL) throws Exception {
		return getHTMLHttpResponse(URL, null);
	}
	public String getHTMLHttpResponse(String URL, Header[] headers) throws Exception {
		String htmlresponse = "";
        
        HttpGet request = new HttpGet(URL);
        if(headers!=null){
	        for(Header header: headers){
		        request.addHeader(header);
	        }
        }
        
		try {
			
			// Execute the method.
			HttpResponse httpresponse = this.execute(request);

			//if status not ok
			int statusCode = httpresponse.getStatusLine().getStatusCode();
			if (statusCode != HttpStatus.SC_OK) {
				System.err.println("Method failed: " + httpresponse.getStatusLine().getReasonPhrase());
			}
			
			//if status ok
			if(statusCode != HttpStatus.SC_NO_CONTENT) {
				htmlresponse = readResponse(httpresponse);
				this.setCachedData("lasthttpresponse", htmlresponse);
			}
			
		} catch (IOException e) {
			e.printStackTrace();
			throw new Exception("Fatal transport error: " + e.getMessage(), e);
		} finally {
			// Release the connection.
			request.abort();
		}  
        
		return htmlresponse;
	}
	
	//normal html post
	public String postHTMLHttpResponse(String URL, String contentType, HashMap<String, String> postParameters) throws Exception {
		String htmlresponse = "";
		HttpPost request = new HttpPost(URL);
		String contenttype = "application/x-www-form-urlencoded; charset=UTF-8";

		//Set the content type
		if(contentType!=null) contenttype = contentType;
	    request.addHeader("Content-Type", contenttype);

	    //Set the post parameters
		if(postParameters!=null && postParameters.size() > 0){
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(postParameters.size());
		    Iterator<Entry<String, String>> pParams	= postParameters.entrySet().iterator();
		    while(pParams.hasNext()){
		    	Map.Entry entry	= (Map.Entry)pParams.next();
		    	nameValuePairs.add(new BasicNameValuePair(entry.getKey().toString(), entry.getValue().toString()));		    	
		    }
		    request.setEntity(new UrlEncodedFormEntity(nameValuePairs));
	    }

		try {
			
			// Execute the method.
			HttpResponse httpresponse = this.execute(request);

			//if status not ok
			int statusCode = httpresponse.getStatusLine().getStatusCode();
			if (statusCode != HttpStatus.SC_OK) {
				System.err.println("Method failed: " + httpresponse.getStatusLine().getReasonPhrase());
			}
			
			//if status ok
			if(statusCode != HttpStatus.SC_NO_CONTENT) {
				htmlresponse = readResponse(httpresponse);
				this.setCachedData("lasthttpresponse", htmlresponse);
			}

		} catch (IOException e) {
			e.printStackTrace();
			throw new Exception("Fatal transport error: " + e.getMessage(), e);
		} finally {
			// Release the connection.
			request.abort();
		}  
        
		return htmlresponse;
	}
	
	//json post
	public String postJSONHttpResponse(String URL, String jsonPostData) throws Exception {
		String jsonresponse = "";
		HttpPost request = new HttpPost(URL);

		StringEntity entity = new StringEntity(jsonPostData, "UTF-8");
        entity.setContentType("application/json;charset=UTF-8"); //text/plain;charset=UTF-8
        entity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,"application/json;charset=UTF-8"));
        request.setEntity(entity); 

		try {
			
			// Execute the method.
			HttpResponse httpresponse = this.execute(request);

			//if status not ok
			int statusCode = httpresponse.getStatusLine().getStatusCode();
			if (statusCode != HttpStatus.SC_OK) {
				System.err.println("Method failed: " + httpresponse.getStatusLine().getReasonPhrase());
			}
			
			//if status ok
			if(statusCode != HttpStatus.SC_NO_CONTENT) {
				jsonresponse = readResponse(httpresponse);
				this.setCachedData("lasthttpresponse", jsonresponse);
			}

		} catch (IOException e) {
			e.printStackTrace();
			throw new Exception("Fatal transport error: " + e.getMessage(), e);
		} finally {
			// Release the connection.
			request.abort();
		}  
        
		return jsonresponse;
	}
	
	//xml post
	public String postXMLHttpResponse(String URL, String xmlPostData) throws Exception {
		String xmlresponse = "";
		HttpPost request = new HttpPost(URL);

		StringEntity entity = new StringEntity(xmlPostData, "UTF-8");
        entity.setContentType("text/xml; charset=utf-8");
        entity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,"text/xml; charset=utf-8"));
        request.setEntity(entity); 

		try {
			
			// Execute the method.
			HttpResponse httpresponse = this.execute(request);

			//if status not ok
			int statusCode = httpresponse.getStatusLine().getStatusCode();
			if (statusCode != HttpStatus.SC_OK) {
				System.err.println("Method failed: " + httpresponse.getStatusLine().getReasonPhrase());
			}
			
			//if status ok
			if(statusCode != HttpStatus.SC_NO_CONTENT) {
				xmlresponse = readResponse(httpresponse);
				this.setCachedData("lasthttpresponse", xmlresponse);
			}

		} catch (IOException e) {
			e.printStackTrace();
			throw new Exception("Fatal transport error: " + e.getMessage(), e);
		} finally {
			// Release the connection.
			request.abort();
		}  
        
		return xmlresponse;
	}

	//normal binary get
	public byte[] getBinaryHttpResponse(String URL) throws Exception {
		byte[] response = null;

		HttpGet request = new HttpGet(URL);

		try {
			
			// Execute the method.
			HttpResponse httpresponse = this.execute(request);
			//if status not ok
			int statusCode = httpresponse.getStatusLine().getStatusCode();
			if (statusCode != HttpStatus.SC_OK) {
				System.err.println("Method failed: " + httpresponse.getStatusLine().getReasonPhrase());
			}
			
			//if status ok
			if(statusCode != HttpStatus.SC_NO_CONTENT) {
				InputStream is = httpresponse.getEntity().getContent();
				response = IOUtils.toByteArray(is);
			}
			
		} catch (IOException e) {
			e.printStackTrace();
			throw new Exception("Fatal transport error: " + e.getMessage(), e);
		} finally {
			// Release the connection.
			request.abort();
		}  

		return response;
	}

	//normal binary post
	public String postBinaryJSONResponse(String URL, InputStream content, String fileName) throws Exception {
		String jsonresponse = "";

		HttpPost request = new HttpPost(URL);

		MultipartEntity mpEntity = new MultipartEntity();
		ContentBody bodycontent = new InputStreamBody(content, fileName);
	    mpEntity.addPart("userfile", bodycontent);
		request.setEntity(mpEntity); 

	    
		//InputStreamEntity entity = new InputStreamEntity(content, content.available());
		//String contenttype = "image/jpg;charset=UTF-8";
		//if(contentType!=null) contenttype = contentType;
		//entity.setContentType(contenttype); //text/plain;charset=UTF-8
		//entity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,contenttype));
		
        
		try {
			
			// Execute the method.
			HttpResponse httpresponse = this.execute(request);

			//if status not ok
			int statusCode = httpresponse.getStatusLine().getStatusCode();
			if (statusCode != HttpStatus.SC_OK) {
				System.err.println("Method failed: " + httpresponse.getStatusLine().getReasonPhrase());
			}
			
			//if status ok
			if(statusCode != HttpStatus.SC_NO_CONTENT) {
				jsonresponse = readResponse(httpresponse);
				this.setCachedData("lasthttpresponse", jsonresponse);
			}
			
		} catch (IOException e) {
			e.printStackTrace();
			throw new Exception("Fatal transport error: " + e.getMessage(), e);
		} finally {
			// Release the connection.
			request.abort();
		}  

		return jsonresponse;
	}
	
	
	/****************************
	 * STATIC METHODS  
	 ****************************/
	
	public static boolean isUrlRequestBinary(String targetUrl, String binaryExtensions){
		boolean isbinary = false;
		String url = targetUrl.toLowerCase();
		String binaryextensions = binaryExtensions;
		String[] binaryextensionsarray = binaryextensions.split(",");
		for(String extension: binaryextensionsarray){
			if(url.contains(extension)) isbinary = true;
		}
		return isbinary;
	}

	
	public static String getProxyURLPrefix(String contextPath, String targetUrl){
		return contextPath + "&url=" + targetUrl + "/";
	}
	
	
	public static String resolveHTML(String html, String proxyURL){
		String responsehtml = html;
		try {
			responsehtml = replaceLinks(proxyURL, responsehtml);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		
		return responsehtml;
	}
	
	public static String replaceLinks(String address, String content) throws URISyntaxException{
		String responsestring = replaceLinks(address, content, "(href|src|action|background)=\"[^\"]*\"", Character.toString('"')); 
		return replaceLinks(address, responsestring, "(href|src|action|background)='[^']*'", "'");
	}
	
	public static String replaceLinks(String address, String content, String regex, String quote) throws URISyntaxException{
	    //absolute URI used for change all relative links
	    URI addressUri = new URI(address);
	    //finds all link atributes (href, src, etc.)
	    Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
	    Matcher m = pattern.matcher(content);
	    //determines if the link is allready absolute
	    Pattern absoluteLinkPattern = Pattern.compile("[a-z]+://.+");
	    //buffer for result saving
	    StringBuffer buffer = new StringBuffer();
	    //position from where should next interation take content to append to buffer
	    int lastEnd = 0;
	    while(m.find()){
	        //position of link in quotes
	        int startPos = content.indexOf(quote,m.start())+1;
	        int endPos = m.end()-1;
	        String link = content.substring(startPos,endPos);
	        Matcher absoluteMatcher = absoluteLinkPattern.matcher(link);
	        //is the link relative?
	        if(!absoluteMatcher.find())
	        {
	            //create relative URL
	            //URI tmpUri = addressUri.resolve(link);
	        	String newurl = address + link;
	            //append the string between links
	            buffer.append(content.substring(lastEnd,startPos-1));
	            //append new link
	            //buffer.append(tmpUri.toString());
	            buffer.append(newurl);
	            lastEnd =endPos+1;
	        }
	    }
	    //append the end of file
	    buffer.append(content.substring(lastEnd));
	    return buffer.toString();
	}
	
	public String readResponse(HttpResponse httpResponse) throws IllegalStateException, IOException{
		String htmlresponse = "";
		BufferedReader rd = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent(), "UTF-8"));
		String line = "";
		while ((line = rd.readLine()) != null) {
			htmlresponse += line;
		}		
		return htmlresponse;
	}

	
	public String lastHttpResponse(){
		return this.getCachedData("lasthttpresponse");
	}
	
	public String Username(){
		return this.username;
	}
	public String Password(){
		return this.password;
	}

	public boolean isAuthenticated(){
		return this.authenticated;
	}
	public boolean isAuthenticated(boolean isAuthenticated){
		return this.authenticated = isAuthenticated;
	}
	
	public Calendar getTimeCreated(){
		return this.timecreated;
	}
	

	public void setCachedData(String key, String value){
		this.cacheddata.put(key, value);
	}
	public String getCachedData(String key){
		return this.cacheddata.get(key);
	}
}
