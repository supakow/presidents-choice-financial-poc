package com.backbase.labs.i18n;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;

import javassist.NotFoundException;

public interface i18nInterface {
	public abstract void initialize(String locale, String location) throws FileNotFoundException, IOException, Exception;
	public abstract String getResource(String resourceId) throws NotFoundException;
	public abstract boolean deleteResource(String resourceId) throws FileNotFoundException, IOException;
	public abstract String updateResource(String resourceId, String resourceValue) throws FileNotFoundException, IOException, Exception;
	public abstract HashMap<String,String> listResources();
}
