package com.backbase.labs.ptc.provider.impl;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;




import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;




import com.backbase.labs.common.HttpAuthenticationManager;
import com.backbase.labs.common.HttpClient;
import com.backbase.labs.common.XmlUtilities;
import com.backbase.portal.ptc.config.element.DataProviderConfig;
import com.backbase.portal.ptc.context.MutableProxyContext;
import com.backbase.portal.ptc.provider.DataProvider;
import com.backbase.portal.ptc.request.ProxyRequest;
import com.backbase.portal.ptc.response.MutableProxyResponse;


/**********************************************************************************
 * This data provider talks to www.prepaidcardservices.com, and allow json actions
 **********************************************************************************/

public class PrepaidCardServicesDataProvider implements DataProvider {

	@Override
	public MutableProxyResponse executeRequest(DataProviderConfig providerconfig,
			MutableProxyContext proxycontext, ProxyRequest proxyrequest) throws IOException {
		
		MutableProxyResponse mpr = new MutableProxyResponse();

		//Read parameters sent from the widget
		String feature = proxycontext.getInternalParameterValue("feature");

		
		//Call the Backbase Clipper API
		try{
			
			//validate input parameters
			if((feature==null)||(feature.equals(""))) throw new Exception("feature not specified");

			String primaryusername = proxycontext.getInternalParameterValue("__auth_username");

			HttpAuthenticationManager ham = new HttpAuthenticationManager(primaryusername);
			HttpClient httpclient = ham.getHttpClient("cc");
			
			String responsevalue = "";

			if(!httpclient.isAuthenticated()){
				responsevalue = httpclient.lastHttpResponse();
				JSONObject loginjson = (JSONObject) JSONSerializer.toJSON(responsevalue);
	
				if(!loginjson.containsKey("message")){
						//This likely mean authentication expired				
					httpclient.isAuthenticated(false);
				}
				
				if(loginjson.getString("message").equals("AUTHENTICATED")){
					httpclient.isAuthenticated(true);
					//read the token
					httpclient.setCachedData("logindata", responsevalue);
					httpclient.setCachedData("token", loginjson.getString("token"));
				} else {
					throw new Exception("AUTHENTICATED to theapprovedcard failed.");
				}				
			}
			
			
			String token = httpclient.getCachedData("token");
			
			//Get start and end date
			Calendar today = Calendar.getInstance();
			String enddate = today.get(Calendar.YEAR) + "-" + today.get(Calendar.MONTH) + "-" + today.get(Calendar.DAY_OF_MONTH) + "T00:00:00";
			today.add(Calendar.MONTH, -1);
			String startdate = today.get(Calendar.YEAR) + "-" + today.get(Calendar.MONTH) + "-01T00:00:00";
			
			if(feature.equals("TransactionHistory")){
				responsevalue = token;
				String jsonthpostdata = "{ \"token\": \"" + token + "\", \"params\": { \"request\": { \"cardId\": \"\", \"startDate\": \"" + startdate + "\", \"endDate\": \"" + enddate + "\" } } }";
				responsevalue = httpclient.postJSONHttpResponse("https://www2.theapprovedcard.com/txrpc/AccountHistory/TransactionHistoryService", jsonthpostdata);
			} else if(feature.equals("DashboardBalances")){
				responsevalue = "{\"success\":true,\"date\":\"8:30 AM, June 3, 2013\",\"cards\":[{\"name\":\"John\",\"primary\":\"true\",\"balance\":\"$22.36\"},{\"name\":\"Colm\",\"primary\":\"false\",\"balance\":\"$6.20\"}],\"cardsTotal\":\"$28.56\",\"goals\":[{\"id\":6570,\"name\":\"Emergency Fund\",\"balance\":\"$0.00\",\"target\":\"$0.00\",\"percent\":\"0%\",\"icon\":\"tac_icons-savings.jpg\"}],\"totalGoals\":\"$0.00\",\"totalCash\":\"$28.56\"}";
				//First perform a request to the pfm subsystem to obtain a session.
				//responsevalue = httpclient.getHTMLHttpResponse("https://pfm.theapprovedcard.com/images/test.gif");
				//responsevalue = httpclient.getHTMLHttpResponse("https://pfm.theapprovedcard.com/?token=" + token + "&page=dashboard");
				//responsevalue = httputils.getHTMLHttpResponse(httpclient, "https://pfm.theapprovedcard.com/dashboard-balances");
			} else if(feature.equals("ExpensesByCategory")){
				responsevalue = "<chart caption=\"Expenses by Category\" bgAlpha=\"0\" numberPrefix=\"$\" showBorder=\"0\" palette=\"1\" formatNumberScale=\"0\" decimals=\"2\" forceDecimals=\"1\" pieRadius=\"150\"><set label=\"Out to Eat\" value=\"16.4\" link=\"javascript:PopModal(&apos;expense-drilldown?startDate=04/01/2013&amp;endDate=04/30/2013&amp;view=Category&amp;accountId=&amp;id=92&apos;);\"/><set label=\"Other\" value=\"66.89\" link=\"javascript:PopModal(&apos;expense-drilldown?startDate=04/01/2013&amp;endDate=04/30/2013&amp;view=Category&amp;accountId=&amp;id=8&apos;);\"/><set label=\"Food / Dining\" value=\"15.1\" link=\"javascript:PopModal(&apos;expense-drilldown?startDate=04/01/2013&amp;endDate=04/30/2013&amp;view=Category&amp;accountId=&amp;id=87&apos;);\"/></chart>";
				XmlUtilities xmlutils = new XmlUtilities(responsevalue, null);
				String datajson = "";
				NodeList sets = xmlutils.performXPathAsNodeList("//set");
			    for (int i = 0; i < sets.getLength(); i++) {
			    	Node set = sets.item(i);
			    	if(!datajson.equals("")) datajson+=",";
			    	datajson += "[\"" + XmlUtilities.performXPathAsString(set, "@label") + "\", " + XmlUtilities.performXPathAsString(set, "@value") + "]";
			    }					
				responsevalue = "{\"success\":true,\"data\":[" + datajson + "]}";
				
				//https://pfm.theapprovedcard.com/get-chart/pie?startDate=2013-05-01&endDate=2013-05-31&view=Category&accountId=&_=1368713846582
				//responsevalue = httpclient.getHTMLHttpResponse("https://pfm.theapprovedcard.com/get-chart/pie?startDate=2013-05-01&endDate=2013-05-31&view=Category&accountId=");
			} else if(feature.equals("Profile")){
				responsevalue = httpclient.getCachedData("logindata");
			} else if(feature.equals("Summary")){
				responsevalue = httpclient.getCachedData("logindata");
			} else {
				throw new Exception("Unknown feature request");
			}
				
		
			mpr.setStatusCode(200);
			mpr.setBody(responsevalue);
			mpr.setStatusText("OK");
			
		} catch(Exception ex){
			String responsevalue = ex.getMessage(); 
			responsevalue += "\n" + Arrays.toString(ex.getStackTrace());
			mpr.setStatusCode(500);
			mpr.setBody(responsevalue);
		}
		
		return mpr;
	}

}
