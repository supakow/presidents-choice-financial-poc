package com.backbase.labs.i18n;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.Locale;
import java.util.Properties;

import javassist.NotFoundException;

public class i18nResourceProperties implements i18nInterface {
	
	Locale locale = i18nUtilities.localeFromString("en_us");
	String filepath = "";
	Properties properties = new Properties();
	
	public void initialize(String locale, String location) throws IOException {
		this.locale = i18nUtilities.localeFromString(locale);
		this.filepath = location + "\\i18n_" + locale + ".properties";
		File f = new File(filepath);
		if(!f.exists()){
			f.createNewFile();
		}
		InputStreamReader is = new InputStreamReader(new FileInputStream(filepath), "UTF-8");
		properties.load(is);
	}
	
	public String getResource(String resourceId) throws NotFoundException{
		 if(properties.containsKey(resourceId)){
			 return properties.getProperty(resourceId);
		 } else {
			 throw new NotFoundException(resourceId + " for locale " + this.locale.toString() + " not found.");
		 }
	 };
	 
	 public boolean deleteResource(String resourceId) throws FileNotFoundException, IOException{
		 properties.remove(resourceId);
		 properties.store(new FileOutputStream(new File(filepath)), "updated by class");
		 return true;
	 };
	 
	 public String updateResource(String resourceId, String resourceValue) throws FileNotFoundException, IOException{
		 if(properties.containsKey(resourceId)){
			 properties.setProperty(resourceId, resourceValue);
		 } else {
			 properties.put(resourceId, resourceValue);
		 }

		 OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(filepath), "UTF-8");
		 properties.store(out, "updated by class");
		 return resourceValue;
	 };
	 
	 public HashMap<String,String> listResources(){
		 HashMap<String, String> listresources = new HashMap<String, String>();
		 for(Object key: properties.keySet()){
			 listresources.put((String)key, properties.getProperty((String) key));
		 }
		 return listresources;
	 }

}
