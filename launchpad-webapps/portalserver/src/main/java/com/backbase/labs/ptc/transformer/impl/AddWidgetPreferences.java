package com.backbase.labs.ptc.transformer.impl;

import java.util.HashMap;

import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import com.backbase.labs.common.PortalUtilities;
import com.backbase.portal.foundation.commons.exceptions.ItemNotFoundException;
import com.backbase.portal.ptc.context.MutableProxyContext;
import com.backbase.portal.ptc.request.MutableProxyRequest;
import com.backbase.portal.ptc.transform.request.AbstractRequestTransformer;


public class AddWidgetPreferences extends AbstractRequestTransformer {

    private static final String prefix = "widget_";

    public void transform(MutableProxyContext mutableProxyContext, MutableProxyRequest mutableProxyRequest) {
    	
        String widgetid = mutableProxyContext.getInternalParameterValue("widgetid");
        String portalname = mutableProxyContext.getInternalParameterValue("portalname");
    	
        if((widgetid!=null)&&(portalname!=null)){
			try {
		    	PortalUtilities pu = new PortalUtilities(portalname);
		    	HashMap<String,String> widgetproperties = pu.getWidgetPreferences(widgetid);
		    	for(String key:widgetproperties.keySet()){
		            mutableProxyContext.getInternalParameters().put(prefix + key, new String[]{widgetproperties.get(key)});
		            System.out.println("Added parameter: " + prefix + key + ": " + widgetproperties.get(key));
		    	}
			} catch (ItemNotFoundException e) {
				System.out.println("AddWidgetPreferences: Unable to find widget " + widgetid);
			}
		} else {
			System.out.println("AddWidgetPreferences: widgetid and portalname not supplied.");
		}
        
    }

}
