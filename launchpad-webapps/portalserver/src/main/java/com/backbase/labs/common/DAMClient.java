package com.backbase.labs.common;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

public class DAMClient {

	String rootpath = "";
	String identity = "";
	String reference = "";
	
	ContentServicesClient csclient = null;
	
	public DAMClient(String rootPath, String identity) throws Exception{
		this.rootpath = rootPath;
		this.identity = identity;
		
		csclient = new ContentServicesClient();
	}
	public DAMClient(String rootPath, String identity, String reference) throws Exception{
		this.rootpath = rootPath;
		this.identity = identity;
		this.reference = reference;
		
		csclient = new ContentServicesClient();
	}

	public HashMap<String, String> getFoldersList() throws Exception{
		String path = this.getIdentityPath();		
		return getFoldersList(path);
	}
	public HashMap<String, String> getFoldersList(String path) throws Exception{
		return csclient.getListFolders(path);
	}

	public HashMap<String, String> getDocumentsList(String path) throws Exception{
		HashMap<String, String> documentslist = new HashMap<String, String>();
		documentslist = csclient.getList(path, true, false);
		return documentslist;
	}
	
	
	public HashMap<String, Object> getDocument(String filename){
		String path = this.getIdentityPath();
		return getDocument(path, filename);
	}
	
	public HashMap<String, Object> getDocument(String path, String filename){
		return getDocumentByPath(path + "/" + filename);
	}

	public HashMap<String, Object> getDocumentByPath(String filepath){
		return csclient.getRawContent(filepath);
	}

	private String getIdentityPath(){
		if((this.reference!=null)&&(!this.reference.equals(""))){
			return this.rootpath + "/" + this.reference + "/" + this.identity;
		} else {
			return this.rootpath + "/" + this.identity;
		}
	}
}
