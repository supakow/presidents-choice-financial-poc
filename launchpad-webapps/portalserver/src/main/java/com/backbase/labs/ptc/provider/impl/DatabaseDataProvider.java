package com.backbase.labs.ptc.provider.impl;

import java.io.IOException;

import com.backbase.labs.common.DatabaseClient;
import com.backbase.portal.ptc.config.element.DataProviderConfig;
import com.backbase.portal.ptc.context.MutableProxyContext;
import com.backbase.portal.ptc.provider.DataProvider;
import com.backbase.portal.ptc.request.ProxyRequest;
import com.backbase.portal.ptc.response.MutableProxyResponse;

public class DatabaseDataProvider implements DataProvider {

	@Override
	public MutableProxyResponse executeRequest(DataProviderConfig dataProviderConfig,
			MutableProxyContext mutableProxyContext, ProxyRequest proxyRequest) throws IOException {

		MutableProxyResponse mpr = new MutableProxyResponse();
		
		try{
			DatabaseClient dbclient = new DatabaseClient(dataProviderConfig.getParamValue("connectionString"), dataProviderConfig.getParamValue("driverClassname"), dataProviderConfig.getParamValue("username"), dataProviderConfig.getParamValue("password"));
			mpr.setBody(dbclient.executeQueryAsJSON("Select * from audit_events"));
			mpr.setStatusCode(200);
		} catch(Exception ex){
			mpr.setBody(ex.getMessage());
			mpr.setStatusCode(500);
		}
		
		return mpr;
	}

}
