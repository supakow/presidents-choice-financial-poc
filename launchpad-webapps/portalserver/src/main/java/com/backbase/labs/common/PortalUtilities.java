package com.backbase.labs.common;

import java.util.Collection;
import java.util.HashMap;

import com.backbase.portal.foundation.domain.conceptual.PropertyDefinition;
import com.backbase.portal.foundation.commons.exceptions.ItemNotFoundException;
import com.backbase.portal.foundation.domain.model.Portal;
import com.backbase.portal.foundation.domain.model.Widget;

public class PortalUtilities {

	String portalname = "";
	
	public PortalUtilities(String portalName) throws ItemNotFoundException{
		this.portalname = portalName;
	}
	
	public HashMap<String,String> getWidgetPreferences(String widgetId){
		HashMap<String,String> preferences = new HashMap<String, String>();
		try{
			Widget widget = getWidget(widgetId);
			if(widget!=null){
				Collection<PropertyDefinition> wpreferences = widget.getPreferences();
				for(PropertyDefinition prop: wpreferences){
		        	preferences.put(prop.getName(), prop.getValue().getValue().toString());
				}
			}
			
        } catch(Exception ex){
        	
        }
		return preferences;
	}
	
	public String getWidgetPreference(String widgetId, String preference){
		String response = "";
        
        try{
        	Widget widget = getWidget(widgetId);
        	response = (String)widget.getProperty(preference).getValue();
        } catch(Exception ex){
        	response = null;
        }
        return response;
	}
	
	public Portal getPortal(String portalName) throws ItemNotFoundException{
		return PortalBusinessServices.INSTANCE.portalBusinessService.getPortal(portalName, true);
	}
	
	public Widget getWidget(String widgetId) throws ItemNotFoundException{
		return (Widget) PortalBusinessServices.INSTANCE.itemBusinessService.getItem(this.portalname, widgetId, true);  //boolean processChildren
	}
	
	
}
