package com.backbase.labs.common;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

public class UGCClient {

	String rootpath = "";
	String identity = "";
	String portal = "";
	String reference = "";
	Boolean shared = false;
	
	ContentServicesClient csclient = null;
	
	public UGCClient(String rootPath, String identity, String portal) throws Exception{
		this.rootpath = rootPath;
		this.identity = identity;
		this.portal = portal;
		
		csclient = new ContentServicesClient();
	}
	public UGCClient(String rootPath, String identity, String portal, String reference, Boolean shared) throws Exception{
		this.rootpath = rootPath;
		this.identity = identity;
		this.portal = portal;
		this.reference = reference;
		this.shared = shared;
		
		csclient = new ContentServicesClient();
	}

	public HashMap<String,HashMap<String,String>> listContentEntriesByCurrentUser(String type){
		return listContentEntriesByCurrentUser(type, this.reference);
	}
	public HashMap<String,HashMap<String,String>> listContentEntriesByCurrentUser(String type, String reference){
		return listContentEntriesByUser(type, this.reference, true);
	}

	public HashMap<String,HashMap<String,String>> listContentEntriesByUser(String type){
		return listContentEntriesByUser(type, this.reference, false);
	}
	public HashMap<String,HashMap<String,String>> listContentEntriesByUser(String type, String reference, boolean currentIdentityOnly){
		//Each user as its own folder under the parent/reference
		HashMap<String, String> folders = new HashMap<String, String>();
		HashMap<String,HashMap<String,String>> entries = new HashMap<String, HashMap<String,String>>();
		try{
			String parentpath = getParentPath(reference);
			folders = csclient.getListFolders(parentpath);
			for(String user: folders.keySet()){
				//key will be the user folder name
				if((currentIdentityOnly)&&(user.equals(this.identity))){
					entries.put(user, listContentEntries(type, reference, user));
				} else {
					entries.put(user, listContentEntries(type, reference, user));
				}
			}
		} catch(Exception ex){
			
		}
		return entries;
	}

	
	public HashMap<String,String> listContentEntries(String type){
		return listContentEntries(type, this.reference);
	}
	public HashMap<String,String> listContentEntries(String type, String reference){
		return listContentEntries(type, reference, this.identity);
	}
	public HashMap<String,String> listContentEntries(String type, String reference, String user){
		HashMap<String,String> cleanentries = new HashMap<String, String>();
		try{
			HashMap<String,String> entries = new HashMap<String, String>();
			entries = csclient.getList(getContentPath(reference,user), true);
			
			//cleanup entries and only display valid type entries
			for(String key: entries.keySet()){
				String[] filesplit = key.split(" - ");
				if(type.equals(filesplit[0])){
					cleanentries.put(key, entries.get(key));
				}
			}
		} catch(Exception ex){
			
		}
		return cleanentries;
	}

	
	public String getContent(String type, String id){
		return getContent(type, id, this.reference);
	}
	public String getContent(String type, String id, String reference){
		String response = "";
		try{
			response = csclient.getContentByPath(getContentPath(reference) + "/" + getFilename(id));
		} catch(Exception ex){
			//fail silent
		}
		return response;
	}
	

	public String createEntry(String type, String content) throws Exception{
		return createEntry(type, this.reference, content);
	}
	public String createEntry(String type, String reference, String content) throws Exception{
		String id = createId(type);
		csclient.updateContentByPath(this.getContentPath(reference), this.getFilename(id), content);
		return id;
	}
	
	
	public boolean updateEntry(String type, String id, String content) throws Exception{
		return updateEntry(type, id, this.reference, content);
	}
	public boolean updateEntry(String type, String id, String reference, String content) throws Exception{
		boolean response = csclient.updateContentByPath(this.getContentPath(reference), this.getFilename(id), content);
		return response;
	}
	
	
	public boolean deleteEntry(String type, String id){
		return deleteEntry(type, id, this.reference);
	}
	public boolean deleteEntry(String type, String id, String reference){
		return csclient.deleteContentByPath(getContentPath(reference) + "/" + getFilename(id));
	}
	
	
	private String createId(String type){
		if(type.equals("comments")||type.equals("bookmark")){
			TimeZone tz = TimeZone.getTimeZone("UTC");
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH-mm-ss");
			df.setTimeZone(tz);
			String nowAsISO = df.format(new Date());
			return type + " - " + nowAsISO;
		} else {
			return type;
		}
	}
	
	
	 private String getContentPath(String reference){
		 return getContentPath(reference, this.identity);
	 }
	 private String getContentPath(String reference, String identity){
		 if(this.shared){
			 return getParentPath(reference);
			 
		 } else {
			 return getParentPath(reference) + "/" + identity;
		 }
	 }
	 
	 private String getParentPath(String reference){
		 if((reference=="")||(reference==null)){
			 if(this.reference!=""){
				 return this.rootpath + "/" + this.portal + "/" + this.reference;
			 } else {
				 return this.rootpath + "/" + this.portal;
			 }
		 } else {
			 return this.rootpath + "/" + this.portal + "/" + reference;
		 }
	 }
	 
	 private String getFilename(String id){
		 return id + ".txt";
	 }
}
