package com.backbase.labs.common;

import java.sql.Connection;
import java.sql.SQLException;

import java.sql.*;
import java.util.HashMap;

import javax.naming.*;
import javax.sql.*;

public class DatabaseClient {

	Connection connection = null;
	
	public DatabaseClient(String datasourceContext) throws Exception{
		connection = getJNDIConnection(datasourceContext);
	}
	
	public DatabaseClient(String dbConnectionString, String driverClassName, String username, String password) throws Exception{
		connection = getSimpleConnection(dbConnectionString, driverClassName, username, password);
	}

	public ResultSet executeQuery(String query) throws Exception{
		ResultSet rs = null;

		if(connection!=null){
			try {
				Statement statement = connection.createStatement();
				rs = statement.executeQuery(query);
			} catch (SQLException e) {
				throw new Exception("Unable to execute query [" + query + "].", e);
			}
		}
		
		return rs;
	}

	
	public String executeQueryToValue(String query, String columnLabel) throws Exception{
		String rsvalue = null;
		if(connection!=null){
			try {
				Statement statement = connection.createStatement();
				ResultSet rs = statement.executeQuery(query);
				if(rs.next())
				{
					rsvalue = rs.getString(columnLabel);
				}
			} catch (SQLException e) {
				throw new Exception("Unable to execute query [" + query + "].", e);
			}
		}
		
		return rsvalue;
	}
	
	public HashMap<String,String> executeQueryToValues(String query, String[] columnLabels) throws Exception{
		HashMap<String,String> rsvalues = new HashMap<String,String>();
		if(connection!=null){
			try {
				Statement statement = connection.createStatement();
				ResultSet rs = statement.executeQuery(query);
				if(rs.next())
				{
					for(String columnlabel: columnLabels){
						rsvalues.put(columnlabel, rs.getString(columnlabel));
					}
				}
			} catch (SQLException e) {
				throw new Exception("Unable to execute query [" + query + "].", e);
			}
		}
		
		return rsvalues;
	}
	
	public String executeQueryAsJSON(String query) throws Exception{
		String jsonresponse = "{";
		
		String jsoncolumns = "";
		String jsonvalues = "";
		
		try {
			ResultSet rs = executeQuery(query);
			if(rs!=null){
				ResultSetMetaData rsmd = rs.getMetaData();
				int columnCount = rsmd.getColumnCount();
	
				// The column count starts from 1
				for (int i = 1; i < columnCount + 1; i++ ) {
				  String name = rsmd.getColumnName(i);
				  if(i>1) jsoncolumns += ", ";
				  jsoncolumns += "\"" + name + "\"";
				}	

				while (rs.next()) {
					if(!jsonvalues.equals("")) jsonvalues += ",";
					jsonvalues += "[";

					String jsonrecord = "";
					for (int i = 1; i < columnCount + 1; i++ ) {
						if(!jsonrecord.equals("")) jsonrecord += ",";
						jsonrecord += "\"" + rs.getString(i) + "\"";
					}
					
					jsonvalues += jsonrecord + "]";
				}
			}
		} catch (SQLException e) {
			throw new Exception("Unable to create JSON out of query",e);
		}
		
		jsoncolumns = "\"columns\":[" + jsoncolumns + "]";
		jsonvalues = "\"values\":[" + jsonvalues + "]";
		
		
		jsonresponse += jsoncolumns + "," + jsonvalues;
		jsonresponse += "}";
		return jsonresponse;
	}
	
	
	public void close(){
		try {
			connection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//Close the connection if the close method is not called manually.
	@Override
	public void finalize(){
		this.close();
	}
	
	
	
	/** Uses JNDI and Datasource (preferred style).   
	 * @throws Exception */
	private Connection getJNDIConnection(String datasourceContext) throws Exception{
		Connection result = null;
		
		try {
			Context initialContext = new InitialContext();
			if (initialContext == null){
				throw new Exception("JNDI problem. Cannot get InitialContext.");
			}
			DataSource datasource = (DataSource)initialContext.lookup(datasourceContext);
			if (datasource != null) {
				result = datasource.getConnection();
			} else {
				throw new Exception("Failed to lookup datasource.");
			}
		} catch ( NamingException ex ) {
			throw new Exception("Cannot get connection: ", ex);
		} catch(SQLException ex){
			throw new Exception("Cannot get connection: ", ex);
		}
		
		return result;
	}

	/** Uses DriverManager.  
	 * @throws Exception */
 	private Connection getSimpleConnection(String dbConnectionString, String driverClassName, String username, String password) throws Exception { 		
		Connection result = null;
		
		try {
			Class.forName(driverClassName).newInstance();
		} catch (Exception ex){
			throw new Exception("Check classpath. Cannot load db driver: " + driverClassName, ex);
		}

		try {
			result = DriverManager.getConnection(dbConnectionString, username, password);
		} catch (SQLException e){
			throw new Exception( "Driver loaded, but cannot connect to db: " + dbConnectionString,e);
		}
		
		return result;
 	}

	private static void dlog(Object aObject){
		System.out.println(aObject);
	}
	
}
