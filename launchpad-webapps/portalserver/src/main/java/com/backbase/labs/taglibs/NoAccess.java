package com.backbase.labs.taglibs;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.TagSupport;


//http://tomcat.apache.org/tomcat-5.5-doc/jspapi/javax/servlet/jsp/tagext/Tag.html

public class NoAccess extends TagSupport {

	private static final long serialVersionUID = 1L;

	/*
	EVAL_BODY_INCLUDE Evaluate body into existing out stream.
	EVAL_PAGE Continue evaluating the page.
	SKIP_BODY Skip body evaluation.
	SKIP_PAGE Skip the rest of the page.
	*/
	public int doStartTag() throws JspException {
		int returnvalue = SKIP_BODY;
		try {
			AccessCheck accesscheck = (AccessCheck)this.getParent();
			if(!accesscheck.hasAccess()){
				returnvalue = EVAL_BODY_INCLUDE;
			}
		} catch (Exception ioe) {
			throw new JspTagException("Security Check Failed");
		}
		return returnvalue;
	}

	public int doEndTag() {
		return EVAL_PAGE;
	}
}
