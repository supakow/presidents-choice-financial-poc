package com.backbase.labs.ptc.provider.impl;
 
import java.io.IOException;
import java.net.URLEncoder;

import com.backbase.labs.common.HttpClient;
import com.backbase.portal.ptc.config.element.DataProviderConfig;
import com.backbase.portal.ptc.context.MutableProxyContext;
import com.backbase.portal.ptc.provider.DataProvider;
import com.backbase.portal.ptc.request.ProxyRequest;
import com.backbase.portal.ptc.response.MutableProxyResponse;
 
public class GoogleTranslateDataProvider implements DataProvider {
 
	@Override
	public MutableProxyResponse executeRequest(DataProviderConfig dataProviderConfig,
		MutableProxyContext mutableProxyContext, ProxyRequest proxyRequest) throws IOException {
		            
		MutableProxyResponse mpr = new MutableProxyResponse();
		        
		//We use the current logged in user
		String primaryusername = mutableProxyContext.getInternalParameterValue("__auth_username");
		String standardjsonresponse = "{\"state\": \"#state#\",\"message\": \"#message#\",\"data\": #data#}";
		
		boolean debug = false;
		if(mutableProxyContext.getInternalParameterValue("debug")!=null) debug = Boolean.parseBoolean(mutableProxyContext.getInternalParameterValue("debug"));

		//Read parameters sent from the widget
		String from_locale = mutableProxyContext.getInternalParameterValue("from_locale");
		String to_locale = mutableProxyContext.getInternalParameterValue("to_locale");
		String value = mutableProxyContext.getInternalParameterValue("value");

		
		try {
			String status = "success";
			String jsondata = "{}";

			String googleapi_url = "https://translate.google.com/translate_a/t?client=t&sl=en&tl=" + to_locale + "&hl=en&sc=2&ie=UTF-8&oe=UTF-8&oc=1&otf=2&rom=1&ssel=3&tsel=4&q=" +  URLEncoder.encode(value,"UTF-8");
			
			HttpClient client = new HttpClient();
			jsondata = client.getHTMLHttpResponse(googleapi_url);
			if(debug) System.out.println(jsondata);
			jsondata = jsondata.replace(",,", ",");
			jsondata = jsondata.replace(",,", ",");
			jsondata = jsondata.replace("[,", "[");
			if(debug) System.out.println(jsondata);
			
			//Create the final json response
			String jsonresponse = standardjsonresponse.replace("#state#", status);
			jsonresponse = jsonresponse.replace("#message#", "");
			jsonresponse = jsonresponse.replace("#data#", jsondata);
			mpr.addContentTypeHeader("application/json", "utf-8");
			mpr.setStatusCode(200);
			mpr.setStatusText("ok");
			mpr.setBody(jsonresponse);
			
		} catch (Exception e) {
			String jsonresponse = standardjsonresponse.replace("#state#", "error");
			if(debug){
				jsonresponse = jsonresponse.replace("#message#", e.getMessage() + "\n" + org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(e));
			} else {
				jsonresponse = jsonresponse.replace("#message#", e.getMessage());
			}
			jsonresponse = jsonresponse.replace("#data#", "{}");
			mpr.addContentTypeHeader("application/json", "utf-8");
			mpr.setStatusCode(500);
			mpr.setStatusText(e.getLocalizedMessage());
			mpr.setBody(jsonresponse);
		}

		return mpr;
	}
}
 