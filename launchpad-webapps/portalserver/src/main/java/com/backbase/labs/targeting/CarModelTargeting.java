package com.backbase.labs.targeting;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.backbase.portal.targeting.connectorframework.content.contexts.definition.ContextCollector;
import com.backbase.portal.targeting.connectorframework.content.contexts.definition.PossibleValue;
import com.backbase.portal.targeting.connectorframework.content.contexts.definition.ResultEntries;
import com.backbase.portal.targeting.connectorframework.content.contexts.definition.SegmentDefinition;
import com.backbase.portal.targeting.connectorframework.content.contexts.definition.SelectorDefinition;
import com.backbase.portal.targeting.rulesengine.type.RuleEngineTypes;

public class CarModelTargeting extends ContextCollector {

	static String contextId = "car";
	static String description = "Car Collector";
	static String iconPath = ""; 
	static String label = "Car";
	public CarModelTargeting() {
		super(contextId, description, iconPath, label);
	}
	
	@Override
	public ResultEntries executeTask(Map<String, String> requestMap, ResultEntries resultEntries) {
		
		//jan - golf
		String personcar = "golf";
		String personcarcolor = "blue";
		
		resultEntries.addSelectorEntry("carmodel", personcar);
		resultEntries.addSelectorEntry("carcolour", personcarcolor);

		
		if(personcar.equals("golf")){
			resultEntries.addSegmentEntry("student");
		} else {
			resultEntries.addSegmentEntry("yuppie");
		}
		
		return resultEntries;
	}
	
	
	@Override
	//public List<SegmentDefinition> defineSegments(String arg0) {
	public List<SegmentDefinition> defineSegments(String arg0, Map<String, String> arg1) {
		
		List<SegmentDefinition> listsegements = new ArrayList<SegmentDefinition>();
		
		listsegements.add(new SegmentDefinition("yuppie", "Yuppie"));
		listsegements.add(new SegmentDefinition("student", "Student"));
		
		return listsegements;
	}
	@Override
	//public List<SelectorDefinition> defineSelectors(String arg0) {
	public List<SelectorDefinition> defineSelectors(String arg0, Map<String, String> arg1) {

		List<SelectorDefinition> arrayofoptions = new ArrayList<SelectorDefinition>();
		
		SelectorDefinition carmodel = new SelectorDefinition("carmodel", RuleEngineTypes.STRING, "Car Model");
		PossibleValue[] listcarmodels = new PossibleValue[] { new PossibleValue("bmw"), new PossibleValue("golf"), new PossibleValue("toyota")};
		carmodel.addPossibleValues(listcarmodels);

		SelectorDefinition carcolour = new SelectorDefinition("carcolour", RuleEngineTypes.STRING, "Car Colour");
		PossibleValue[] carcolours = new PossibleValue[] { new PossibleValue("red"), new PossibleValue("green"), new PossibleValue("blue")};
		carcolour.addPossibleValues(carcolours);

		
		arrayofoptions.add(carmodel);
		arrayofoptions.add(carcolour);
		return arrayofoptions;
	}


}
