package com.backbase.labs.events;

import java.util.HashMap;

import org.apache.commons.lang.StringEscapeUtils;

import com.backbase.labs.common.ContentServicesClient;
import com.backbase.labs.common.SearchClient;
import com.backbase.portal.commons.api.logging.LoggerFactory;
import com.backbase.portal.commons.api.logging.StrictLogger;
import com.backbase.portal.foundation.business.events.impl.content.*;
import com.backbase.portal.foundation.business.events.impl.items.*;
import com.backbase.portal.foundation.domain.conceptual.Item;
import com.backbase.portal.foundation.domain.conceptual.ItemTag;
import com.backbase.portal.foundation.extensionapi.event.domain.Event;
import com.backbase.portal.foundation.extensionapi.event.handler.AbstractAsynchronousEventHandler;
import com.backbase.portal.foundation.extensionapi.event.handler.EventHandlerException;

public class SearchEventHandler extends AbstractAsynchronousEventHandler {

	private static final StrictLogger LOGGER = LoggerFactory.MP.logger(SearchEventHandler.class);
	private SearchClient searchclient;
	
	public SearchEventHandler(){
	    super(new Class[] { CreateItemEvent.class, UpdateItemEvent.class, UpdateItemListEvent.class, DeleteItemEvent.class, CreateContentEvent.class, UpdateContentEvent.class, DeleteContentEvent.class});
	    
	    try {
			searchclient = new SearchClient();
		} catch (Exception e) {
			if(LOGGER.isDebugEnabled()){
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void handleEvent(Event event) throws EventHandlerException {

		if(LOGGER.isDebugEnabled()) LOGGER.debug("Search Event Triggered: " + event.toString());

		//perform item indexing
		if (event instanceof CreateItemEvent) {
			indexItem(((CreateItemEvent) event).getItemToBeCreated());
		} else if (event instanceof UpdateItemEvent) {
			indexItem(((UpdateItemEvent) event).getExistingItem());
		} else if (event instanceof UpdateItemListEvent){
			for(Item item:((UpdateItemListEvent) event).getDeltaItems()){
				indexItem(item);
			}
		} else if (event instanceof DeleteItemEvent){
			removeItem(((DeleteItemEvent) event).getExistingItem());
		}

		//perform content indexing
		if (event instanceof CreateContentEvent) {
			indexContent(((CreateContentEvent) event).getPath(), true);
		} else if (event instanceof UpdateContentEvent) {
			indexContent(((UpdateContentEvent) event).getPath(), false);
		} else if (event instanceof DeleteContentEvent) {
			removeContent(((DeleteContentEvent) event).getPath());
		}
	}
	
	
	
	private void indexContent(String itemPath, boolean isNew){
		HashMap<String,String> datafields = new HashMap<String,String>();
		String content = "";
		
		try {
			ContentServicesClient csclient = new ContentServicesClient();
			content = csclient.getContentById(itemPath);
			
			content = StringEscapeUtils.escapeHtml(content);
			
		} catch (Exception e) {
			if(LOGGER.isDebugEnabled()){
				e.printStackTrace();
			}
		}
		if(!content.equals("")||!isNew){
			datafields.put("body", content);
			datafields.put("path", itemPath);
			
			searchclient.updateCollection("content", "backbase_" + itemPath, datafields);
		}
	}
	private void removeContent(String itemPath){
		searchclient.cleanupCollection("content", "backbase_" + itemPath);
	}
	
	private void indexItem(Item item){
		HashMap<String,String> datafields = new HashMap<String,String>();

		//Send item owner details
		if(item.getTitle()!=null) datafields.put("title", item.getTitle());
		if(item.getCreatedBy()!=null) datafields.put("created_by", item.getCreatedBy());
		if(item.getCreatedTimestamp()!=null) datafields.put("created_date", item.getCreatedTimestamp().toString());
		if(item.getLastModifiedBy()!=null) datafields.put("last_modified_by", item.getLastModifiedBy());
		if(item.getLastModifiedTimestamp()!=null) datafields.put("last_modified_date", item.getLastModifiedTimestamp().toString());
		if(item.getPageId()!=null) datafields.put("page_id", item.getPageId().toString());
		if(item.getParentItemName()!=null) datafields.put("parent_item_name", item.getParentItemName());
		if(item.getPublicationStatus()!=null) datafields.put("publication_status", item.getPublicationStatus().name());		
		
		//Send tags
		if(item.getItemTags()!=null){
			for(ItemTag tag: item.getItemTags()){
				datafields.put("keywords", tag.getName());
			}
		}
				
		//Send properties
		for(String key: item.getPropertyKeys())
		{
			String value = item.getProperty(key).getValue().toString();
			datafields.put(key, value);
		}
		
		searchclient.updateCollection(item.getItemType().getItemTypeName(), "backbase_" + item.getId().toString(), datafields);
	}
	private void removeItem(Item item){
		searchclient.cleanupCollection(item.getItemType().getItemTypeName(), "backbase_" + item.getId());
	}

}
