package com.backbase.labs.ptc.provider.impl;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.HashMap;

import com.backbase.labs.common.ClipperClient;
import com.backbase.labs.common.HttpAuthenticationManager;
import com.backbase.labs.common.HttpClient;
import com.backbase.portal.ptc.config.element.DataProviderConfig;
import com.backbase.portal.ptc.context.MutableProxyContext;
import com.backbase.portal.ptc.provider.DataProvider;
import com.backbase.portal.ptc.request.ProxyRequest;
import com.backbase.portal.ptc.response.MutableProxyResponse;

public class ClipperDataProvider implements DataProvider {

	@Override
	public MutableProxyResponse executeRequest(DataProviderConfig providerconfig,
			MutableProxyContext proxycontext, ProxyRequest proxyrequest) throws IOException {
		
		MutableProxyResponse mpr = new MutableProxyResponse();

		//Read parameters sent from the widget
		String urltoclip = proxycontext.getInternalParameterValue("url");
		String xpath = proxycontext.getInternalParameterValue("xpath");

		
		//Call the Backbase Clipper API
		try{
			
			if((urltoclip==null)||(urltoclip.equals(""))) throw new Exception("url not specified");
			
			String responsevalue = "";
			ClipperClient clipper = null;
					
			//There are two ways you can instanciate the Clipper client
			//1. by initializing it with login parameters, and it will then create the http client
			//2. by passing the httpclient which has already been initialized
			//We use the parameter called authclientname to distiguese between 1 and 2.
			
			String authclientname = proxycontext.getInternalParameterValue("authclientname");
			if((authclientname!=null)&&(!authclientname.equals("")))
			{
				String primaryusername = proxycontext.getInternalParameterValue("__auth_username");
				HttpAuthenticationManager ham = new HttpAuthenticationManager(primaryusername);
				clipper = new ClipperClient(ham.getHttpClient(authclientname));
				
			} else {
				
				//Change the clipper initialization below if you have user credential requirements
				//You can either send it basic authantication username/password credentials
				//e.g. ClipperClient clipper = new ClipperClient(loginUrl, username, password);
				//Or you can do form authentication by passing the form parameters as a hashmap
				//e.g. ClipperClient clipper = new ClipperClient(loginUrl, postParameters, proxyUrl, proxyPort, proxyUsername, proxyPassword)
	
				//The logic below will collect the proxy parameters from the dataprovider configuration,
				//and will then check if a loginurl is specified.  In such case a form authentication is performed.
				
	
				//check if proxy configured
				if((providerconfig.getParamValue("proxyurl")==null)||(providerconfig.getParamValue("proxyurl").equals(""))){
					//login url found, form authentication *no proxy*
					if((proxycontext.getInternalParameterValue("loginurl")==null)||(proxycontext.getInternalParameterValue("loginurl").equals(""))){
						clipper = new ClipperClient();
					} else {
						clipper = new ClipperClient(proxycontext.getInternalParameterValue("loginurl"), getLoginPostParamsFromRequest(proxycontext));
					}
				} else {
					//login url found, do proxy and form authentication
					if((proxycontext.getInternalParameterValue("loginurl")==null)||(proxycontext.getInternalParameterValue("loginurl").equals(""))){
						clipper = new ClipperClient(providerconfig.getParamValue("proxyurl"), Integer.parseInt(providerconfig.getParamValue("proxyport")), providerconfig.getParamValue("proxyusername"), providerconfig.getParamValue("proxypassword"));
					} else {
						clipper = new ClipperClient(proxycontext.getInternalParameterValue("loginurl"), getLoginPostParamsFromRequest(proxycontext), providerconfig.getParamValue("proxyurl"), Integer.parseInt(providerconfig.getParamValue("proxyport")), providerconfig.getParamValue("proxyusername"), providerconfig.getParamValue("proxypassword"));
					}
				}
			}

			if(clipper!=null){
				responsevalue = clipper.performStringClipping(urltoclip);				
				if((xpath!=null)&&(!xpath.equals(""))&&(!xpath.equals("//"))){
					responsevalue = clipper.performXPATH(responsevalue, xpath);
				} else {
					responsevalue = HttpClient.resolveHTML(responsevalue, "/portalserver/proxy?pipe=clipperPipe&url=" + getBaseUrlFromClippingUrl(urltoclip));
				}
			}
			
			mpr.setStatusCode(200);
			mpr.setBody(responsevalue);
			mpr.setStatusText("OK");
			
		} catch(Exception ex){
			String responsevalue = ex.getMessage(); 
			responsevalue += "\n" + org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(ex);
			mpr.setStatusCode(500);
			mpr.setBody(responsevalue);
		}
		
		return mpr;
	}

	
	private HashMap getLoginPostParamsFromRequest(MutableProxyContext proxycontext){
		HashMap<String,String> loginpostparams = new HashMap<String,String>();
		return loginpostparams;
	}
	
	private String getBaseUrlFromClippingUrl(String clippingUrl){
		String responseurl = clippingUrl;
		try {
			URI uri = new URI(clippingUrl);
			//only get the root/base url
			if(!uri.getPath().equals("")){
				responseurl = clippingUrl.substring(0,clippingUrl.length()-(uri.getPath().length()));
			}
			if(!responseurl.endsWith("/")){
				responseurl+= "/";
			}
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return responseurl;
	}
}
