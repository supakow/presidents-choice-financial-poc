package com.backbase.labs.ptc.provider.impl;
 
import java.io.IOException;
import java.util.List;

import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
 






import com.backbase.labs.common.AuthUtilities;
import com.backbase.labs.common.PortalBusinessServices;
import com.backbase.labs.common.HttpClient;
import com.backbase.labs.common.PortalUtilities;
import com.backbase.labs.common.XmlUtilities;
import com.backbase.portal.foundation.business.service.GroupBusinessService;
import com.backbase.portal.foundation.business.service.UserBusinessService;
import com.backbase.portal.foundation.domain.model.Group;
import com.backbase.portal.foundation.domain.model.Groups;
import com.backbase.portal.ptc.config.element.DataProviderConfig;
import com.backbase.portal.ptc.context.MutableProxyContext;
import com.backbase.portal.ptc.provider.DataProvider;
import com.backbase.portal.ptc.request.ProxyRequest;
import com.backbase.portal.ptc.response.MutableProxyResponse;
 
public class WidgetDataProvider implements DataProvider {
 
        @Override
        public MutableProxyResponse executeRequest(DataProviderConfig dataProviderConfig,
                        MutableProxyContext mutableProxyContext, ProxyRequest proxyRequest) throws IOException {
                
                MutableProxyResponse mpr = new MutableProxyResponse();
                
                //We use the current logged in user
                String primaryusername = mutableProxyContext.getInternalParameterValue("__auth_username");
                System.out.println("Primary Username: " + primaryusername);
                
                //The user executing this thread is: sys2sys
                SecurityContext context = SecurityContextHolder.getContext();
                if (context != null) {
                	if(context.getAuthentication()!=null){
                        String username = context.getAuthentication().getName();
                        System.out.println("DataProvider Security Principal: " + username);
                	} else {
                        System.out.println("DataProvider Security Principal: null");
                	}
                }

                
                String standardjsonresponse = "{\"state\": \"#state#\",\"message\": \"#message#\",\"data\": #data#}";

                boolean debug = false;
                if(mutableProxyContext.getInternalParameterValue("debug")!=null) debug = Boolean.parseBoolean(mutableProxyContext.getInternalParameterValue("debug"));


                try {
                		String status = "success";
                        String jsondata = "{}";

                        String portalname = dataProviderConfig.getParamValue("portalname");
                        PortalUtilities pu = new PortalUtilities(portalname);
                        
                        String widgetid = mutableProxyContext.getInternalParameterValue("widgetid");
                        jsondata = "{";
                        jsondata += "\"title\": \"" + pu.getWidgetPreference(widgetid, "title") + "\"";
                        

                        UserBusinessService userBusinessService = PortalBusinessServices.INSTANCE.userBusinessService;
                        GroupBusinessService groupBusinessService = PortalBusinessServices.INSTANCE.groupBusinessService;
                        AuthUtilities authutils = new AuthUtilities(userBusinessService, groupBusinessService);

                        List<Group> groups = userBusinessService.getGroupsForUser(primaryusername);
                        for(Group group:groups){
                        	jsondata += "\"group\":\"" + group.getName() + "\"";
                        }
                        
                        jsondata += "}";
                        
                        //Create the final json response
                        String jsonresponse = standardjsonresponse.replace("#state#", status);
                        jsonresponse = jsonresponse.replace("#message#", "");
                        jsonresponse = jsonresponse.replace("#data#", jsondata);
                        mpr.addContentTypeHeader("application/json", "utf-8");
                        mpr.setStatusCode(200);
                        mpr.setStatusText("ok");
                        mpr.setBody(jsonresponse);
                        
                } catch (Exception e) {
                        String jsonresponse = standardjsonresponse.replace("#state#", "error");
                        if(debug){
                            jsonresponse = jsonresponse.replace("#message#", e.getMessage() + "\n" + org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(e));
                        } else {
                            jsonresponse = jsonresponse.replace("#message#", e.getMessage());
                        }
                        jsonresponse = jsonresponse.replace("#data#", "{}");
                        mpr.addContentTypeHeader("application/json", "utf-8");
                        mpr.setStatusCode(500);
                        mpr.setStatusText(e.getLocalizedMessage());
                        mpr.setBody(jsonresponse);
                }
                
 
                
                return mpr;
        }
 
}
 