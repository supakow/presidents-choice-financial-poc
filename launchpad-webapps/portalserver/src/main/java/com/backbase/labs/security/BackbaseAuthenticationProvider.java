package com.backbase.labs.security;

import com.backbase.labs.common.AuthUtilities;
import com.backbase.portal.foundation.business.service.GroupBusinessService;
import com.backbase.portal.foundation.business.service.UserBusinessService;
import com.backbase.portal.foundation.domain.model.Group;
import com.backbase.portal.foundation.domain.model.User;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.mapping.GrantedAuthoritiesMapper;
import org.springframework.security.core.authority.mapping.NullAuthoritiesMapper;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.*;

import org.slf4j.LoggerFactory;


public class BackbaseAuthenticationProvider implements AuthenticationProvider{

    static ch.qos.logback.classic.Logger log = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger("com.backbase.crowd.BackbaseAuthenticationProvider");
	private GrantedAuthoritiesMapper authoritiesMapper = new NullAuthoritiesMapper();
	AuthUtilities bbauthutils = null;
	
	public Authentication authenticate(Authentication authentication)
			throws AuthenticationException {

		//get the attempting log in username and password
		String username = authentication.getName();
		String password = (String) authentication.getCredentials();


		//Authenticate against other system
		//Here we will build logic to determine 
		//if the user is successfully authenticated
		boolean isauthenticatedagainstothersystem = true;
		if(isauthenticatedagainstothersystem){

			//We can set a list of groups the user belong to here
			String usergroups = "";
			Boolean isadmin = (username.equals("admin"));
			Boolean issys2sys = (username.equals("sys2sys"));
			Boolean isuser = (!username.equals("admin")&&!username.equals("sys2sys"));

			//Get the groups from Backbase
			List<Group> groups = bbauthutils.validateGroupsInBackbase(bbauthutils.getBackbaseSystemGroups(usergroups,isadmin,issys2sys,(isuser||isadmin||issys2sys)) );

			//Get the user from Backbase
			User user = bbauthutils.validateUserInBackbase(username, password, groups);
			
			//Set user attributes in Backbase
			//user.getPropertyDefinitions().put("custContext", new UserPropertyDefinition("custContext", new StringPropertyValue(custContext)));
			
			//Report a successful authentication attempt
			Authentication result = createSuccessAuthentication(user,authentication,user);
			return result;
		} else {
			System.out.println("Authentication Failed: " + username);
			throw new BadCredentialsException("Refused authentication for " + username);
		}
	}

	private Authentication createSuccessAuthentication(Object principal, Authentication authentication, UserDetails user) {
		UsernamePasswordAuthenticationToken result = new UsernamePasswordAuthenticationToken(principal,
				authentication.getCredentials(), authoritiesMapper.mapAuthorities(user.getAuthorities()));

		System.out.println("Principal Name: "+authentication.getName());
		result.setDetails(authentication.getDetails());
		return result;
	}
	
	
	public boolean supports(Class<? extends Object> arg0) {
		return true;
	}

	public void setBackbaseAuthUtils(AuthUtilities backbaseAuthUtils) {
		this.bbauthutils = backbaseAuthUtils;
	}

}
