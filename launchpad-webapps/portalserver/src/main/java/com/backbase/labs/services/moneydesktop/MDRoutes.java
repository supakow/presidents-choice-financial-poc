package com.backbase.labs.services.moneydesktop;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Camel route to handle MoneyDesktop requests.
 */
@Component
public class MDRoutes extends RouteBuilder {

    public static final String SSO_WIDGET_ENDPOINT = "seda:moneydesktop/ssowidget";

    @Autowired
    private SSOWidgetProcessor ssoWidgetProcessor;

    @Override
    public void configure() throws Exception {
        from(SSO_WIDGET_ENDPOINT)
                .process(ssoWidgetProcessor);

    }
}
