In order to revert portal data changes you need to run in the launchpad-data folder the following:

mvn clean

How to commit portal data changes:

1. Update first the launchpad-data folder to avoid merging issues later.
2. Issue "mvn clean install" on the launchpad-data folder, to make sure you have the portal data state the same as is within svn.
2. When you do data changes in portal manager on retail banking portal the portal.h2.db file is modified in the
launchpad-data/target directory.
3. When you are done with the data changes stop portalserver.
4. Run the "mvn package" inside launchpad-data folder in order to extract these binary changes into the
launchpad-data/target/sql/portal/launchpad-demo-portal.sql file.
5. Then with a copy command you copy this launchpad-data/target/sql/portal/launchpad-demo-portal.sql
into the the launchpad-data/src/import/portal/launchpad-demo-portal.sql which you can commit.
6. This way the next "mvn clean" command on the launchpda-data folder will generate the portal.h2.db file with
the needed updates.
7. When you did a portal data update, send an email to the team.

All these (step 4 and 5) is automated with update-portal-sql.[sh|bat] file.


