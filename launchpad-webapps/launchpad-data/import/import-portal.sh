#!/bin/sh
java -cp h2-1.3.174.jar org.h2.tools.RunScript -url jdbc:h2:file:../portal/portal -user sa -script sql/portal/launchpad-demo-portal.sql
java -cp h2-1.3.174.jar org.h2.tools.RunScript -url jdbc:h2:file:../cs/content -user sa -script sql/cs/launchpad-demo-cs-content.sql
java -cp h2-1.3.174.jar org.h2.tools.RunScript -url jdbc:h2:file:../cs/configuration -user sa -script sql/cs/launchpad-demo-cs-configuration.sql
java -cp h2-1.3.174.jar org.h2.tools.RunScript -url jdbc:h2:file:../cs/resource -user sa -script sql/cs/launchpad-demo-cs-resource.sql