;             
CREATE USER IF NOT EXISTS SA SALT '541329d34ee15a88' HASH 'd7523025da44ae565e89969a1df0183a58cec3026f24603f7fc90677198bac8a' ADMIN;           
CREATE SEQUENCE PUBLIC.SYSTEM_SEQUENCE_8792318E_0AF7_4EFD_859A_877B7083B2B6 START WITH 1 BELONGS_TO_TABLE;    
CREATE SEQUENCE PUBLIC.SYSTEM_SEQUENCE_CE93B59C_6D26_4592_9425_148290031895 START WITH 65 BELONGS_TO_TABLE;   
CREATE SEQUENCE PUBLIC.SYSTEM_SEQUENCE_E7FC814B_CCB1_4A17_BD81_82B3A062E2AB START WITH 33 BELONGS_TO_TABLE;   
CREATE SEQUENCE PUBLIC.SYSTEM_SEQUENCE_D1ABD5BA_BCE0_4454_B09C_688BEF6B9EBE START WITH 1 BELONGS_TO_TABLE;    
CREATE SEQUENCE PUBLIC.SYSTEM_SEQUENCE_3D4E8715_ABB9_4F0C_9F33_1CC1DE9670D4 START WITH 33 BELONGS_TO_TABLE;   
CREATE SEQUENCE PUBLIC.SYSTEM_SEQUENCE_DA375330_91F4_4F97_9302_427C20850D2F START WITH 33 BELONGS_TO_TABLE;   
CREATE CACHED TABLE PUBLIC.CONTENT_STREAM(
    ID BIGINT DEFAULT (NEXT VALUE FOR PUBLIC.SYSTEM_SEQUENCE_8792318E_0AF7_4EFD_859A_877B7083B2B6) NOT NULL NULL_TO_DEFAULT SEQUENCE PUBLIC.SYSTEM_SEQUENCE_8792318E_0AF7_4EFD_859A_877B7083B2B6,
    CONTENT BLOB NOT NULL
);     
ALTER TABLE PUBLIC.CONTENT_STREAM ADD CONSTRAINT PUBLIC.CONSTRAINT_3 PRIMARY KEY(ID);         
-- 0 +/- SELECT COUNT(*) FROM PUBLIC.CONTENT_STREAM;          
CREATE CACHED TABLE PUBLIC.OBJECT_DATA(
    ID BIGINT DEFAULT (NEXT VALUE FOR PUBLIC.SYSTEM_SEQUENCE_3D4E8715_ABB9_4F0C_9F33_1CC1DE9670D4) NOT NULL NULL_TO_DEFAULT SEQUENCE PUBLIC.SYSTEM_SEQUENCE_3D4E8715_ABB9_4F0C_9F33_1CC1DE9670D4,
    OBJECTID VARCHAR(255) NOT NULL,
    OBJECT_TYPE_ID BIGINT NOT NULL,
    PARENT_ID BIGINT,
    CS_ID BIGINT
);   
ALTER TABLE PUBLIC.OBJECT_DATA ADD CONSTRAINT PUBLIC.CONSTRAINT_35 PRIMARY KEY(ID);           
-- 1 +/- SELECT COUNT(*) FROM PUBLIC.OBJECT_DATA;             
INSERT INTO PUBLIC.OBJECT_DATA(ID, OBJECTID, OBJECT_TYPE_ID, PARENT_ID, CS_ID) VALUES
(1, '356dcd88-b879-48bf-9c14-c4a1bef2358e', 2, NULL, NULL);             
CREATE INDEX PUBLIC.FK_OD_IDX_OBJECT_TYPE_ID ON PUBLIC.OBJECT_DATA(OBJECT_TYPE_ID);           
CREATE INDEX PUBLIC.FK_OD_IDX_CS_ID ON PUBLIC.OBJECT_DATA(CS_ID);             
CREATE INDEX PUBLIC.FK_OD_IDX_PARENT ON PUBLIC.OBJECT_DATA(PARENT_ID);        
CREATE CACHED TABLE PUBLIC.OBJECT_TYPE_DEFINITION(
    ID BIGINT DEFAULT (NEXT VALUE FOR PUBLIC.SYSTEM_SEQUENCE_E7FC814B_CCB1_4A17_BD81_82B3A062E2AB) NOT NULL NULL_TO_DEFAULT SEQUENCE PUBLIC.SYSTEM_SEQUENCE_E7FC814B_CCB1_4A17_BD81_82B3A062E2AB,
    BASETYPE VARCHAR(255) NOT NULL,
    CONTENTSTREAMALLOWED VARCHAR(255),
    CREATABLE BOOLEAN,
    DESCRIPTION VARCHAR(255),
    DISPLAYNAME VARCHAR(255),
    FILEABLE BOOLEAN,
    FULLTEXTINDEXED BOOLEAN,
    LOCALNAME VARCHAR(255),
    LOCALNAMESPACE VARCHAR(255),
    OBJECTID VARCHAR(255) NOT NULL,
    PARENT_ID BIGINT,
    QUERYNAME VARCHAR(255),
    QUERYABLE BOOLEAN,
    VERSIONABLE BOOLEAN
);    
ALTER TABLE PUBLIC.OBJECT_TYPE_DEFINITION ADD CONSTRAINT PUBLIC.CONSTRAINT_8 PRIMARY KEY(ID); 
-- 10 +/- SELECT COUNT(*) FROM PUBLIC.OBJECT_TYPE_DEFINITION; 
INSERT INTO PUBLIC.OBJECT_TYPE_DEFINITION(ID, BASETYPE, CONTENTSTREAMALLOWED, CREATABLE, DESCRIPTION, DISPLAYNAME, FILEABLE, FULLTEXTINDEXED, LOCALNAME, LOCALNAMESPACE, OBJECTID, PARENT_ID, QUERYNAME, QUERYABLE, VERSIONABLE) VALUES
(1, 'cmis:document', 'allowed', TRUE, NULL, 'Document Type', TRUE, TRUE, 'document', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'cmis:document', NULL, 'cmis:document', TRUE, FALSE),
(2, 'cmis:folder', 'notallowed', TRUE, NULL, 'Folder Type', TRUE, TRUE, 'folder', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'cmis:folder', NULL, 'cmis:folder', TRUE, NULL),
(3, 'cmis:relationship', 'notallowed', TRUE, NULL, 'Relationship Type', FALSE, FALSE, 'relationship', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'cmis:relationship', NULL, 'cmis:relationship', TRUE, NULL),
(4, 'cmis:policy', 'notallowed', TRUE, NULL, 'Policy Type', FALSE, FALSE, 'policy', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'cmis:policy', NULL, 'cmis:Policy', TRUE, NULL),
(5, 'cmis:document', 'allowed', TRUE, NULL, 'Backbase Custom Resource Bundle', TRUE, FALSE, 'Resource Bundle', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'bb:resourcebundle', 1, 'bb:resourcebundle', TRUE, FALSE),
(6, 'cmis:document', 'allowed', TRUE, NULL, 'Backbase Custom Resource Bundle Request', TRUE, FALSE, 'Resource Bundle Request', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'bb:resourcebundlerequest', 1, 'bb:resourcebundlerequest', TRUE, FALSE),
(7, 'cmis:document', 'allowed', TRUE, NULL, 'Backbase Custom Type Rich Text', TRUE, TRUE, 'RichText', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'bb:richtext', 1, 'bb:richtext', TRUE, TRUE),
(8, 'cmis:document', 'allowed', TRUE, NULL, 'Backbase Custom Type Image', TRUE, TRUE, 'image', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'bb:image', 1, 'bb:image', TRUE, TRUE),
(9, 'cmis:document', 'notallowed', TRUE, NULL, 'Backbase Custom Type Link', TRUE, TRUE, 'link', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'bb:link', 1, 'bb:link', TRUE, TRUE),
(10, 'cmis:document', 'allowed', TRUE, NULL, 'Backbase Custom Type Version History', TRUE, FALSE, 'VersionHistory', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'bb:versionhistory', 1, 'bb:versionhistory', TRUE, FALSE); 
CREATE INDEX PUBLIC.FK_OTD_IDX_PARENT ON PUBLIC.OBJECT_TYPE_DEFINITION(PARENT_ID);            
CREATE CACHED TABLE PUBLIC.PROPERTY_DATA(
    ID BIGINT DEFAULT (NEXT VALUE FOR PUBLIC.SYSTEM_SEQUENCE_DA375330_91F4_4F97_9302_427C20850D2F) NOT NULL NULL_TO_DEFAULT SEQUENCE PUBLIC.SYSTEM_SEQUENCE_DA375330_91F4_4F97_9302_427C20850D2F,
    DISPLAYNAME VARCHAR(255),
    LOCALNAME VARCHAR(255),
    OBJECT_DATA_ID BIGINT NOT NULL,
    OBJECTID VARCHAR(255) NOT NULL,
    PROPERTY_DEFINITION_ID BIGINT NOT NULL,
    QUERYNAME VARCHAR(255),
    VALUE VARCHAR(765)
);               
ALTER TABLE PUBLIC.PROPERTY_DATA ADD CONSTRAINT PUBLIC.CONSTRAINT_7 PRIMARY KEY(ID);          
-- 10 +/- SELECT COUNT(*) FROM PUBLIC.PROPERTY_DATA;          
INSERT INTO PUBLIC.PROPERTY_DATA(ID, DISPLAYNAME, LOCALNAME, OBJECT_DATA_ID, OBJECTID, PROPERTY_DEFINITION_ID, QUERYNAME, VALUE) VALUES
(1, 'Last Modifed By', NULL, 1, 'cmis:lastModifiedBy', 28, 'cmis:lastModifiedBy', 'admin'),
(2, 'Object Type', NULL, 1, 'cmis:objectTypeId', 32, 'cmis:objectTypeId', 'cmis:folder'),
(3, 'Path', NULL, 1, 'cmis:path', 33, 'cmis:path', '/'),
(4, 'Created By', NULL, 1, 'cmis:createdBy', 30, 'cmis:createdBy', 'admin'),
(5, 'Name', NULL, 1, 'cmis:name', 27, 'cmis:name', '--ROOT--'),
(6, 'Object Id', NULL, 1, 'cmis:objectId', 26, 'cmis:objectId', '356dcd88-b879-48bf-9c14-c4a1bef2358e'),
(7, 'Creation Date', NULL, 1, 'cmis:creationDate', 37, 'cmis:creationDate', '2014-01-15 14:22:19 +0100'),
(8, 'Base Type', NULL, 1, 'cmis:baseTypeId', 36, 'cmis:baseTypeId', 'cmis:folder'),
(9, 'Parent Id', NULL, 1, 'cmis:parentId', 34, 'cmis:parentId', NULL),
(10, 'Last Modification Date', NULL, 1, 'cmis:lastModificationDate', 29, 'cmis:lastModificationDate', '2014-01-15 14:22:19 +0100');         
CREATE INDEX PUBLIC.FK_PD_IDX_OBJECT_DATA_ID ON PUBLIC.PROPERTY_DATA(OBJECT_DATA_ID);         
CREATE INDEX PUBLIC.FK_PD_IDX_OBJECTID ON PUBLIC.PROPERTY_DATA(OBJECTID);     
CREATE INDEX PUBLIC.FK_PD_IDX_PROP_DEF_ID ON PUBLIC.PROPERTY_DATA(PROPERTY_DEFINITION_ID);    
CREATE CACHED TABLE PUBLIC.PROPERTY_DEFINITION(
    ID BIGINT DEFAULT (NEXT VALUE FOR PUBLIC.SYSTEM_SEQUENCE_CE93B59C_6D26_4592_9425_148290031895) NOT NULL NULL_TO_DEFAULT SEQUENCE PUBLIC.SYSTEM_SEQUENCE_CE93B59C_6D26_4592_9425_148290031895,
    CARDINALITY VARCHAR(255),
    DESCRIPTION VARCHAR(255),
    DISPLAYNAME VARCHAR(255),
    INHERITED BOOLEAN,
    LOCALNAME VARCHAR(255),
    LOCALNAMESPACE VARCHAR(255),
    OBJECTID VARCHAR(255) NOT NULL,
    OBJECT_TYPE_ID BIGINT NOT NULL,
    ORDERABLE BOOLEAN,
    QUERYNAME VARCHAR(255),
    QUERYABLE BOOLEAN,
    REQUIRED BOOLEAN,
    TYPE VARCHAR(255) NOT NULL,
    UPDATABILITY VARCHAR(255)
);      
ALTER TABLE PUBLIC.PROPERTY_DEFINITION ADD CONSTRAINT PUBLIC.CONSTRAINT_4 PRIMARY KEY(ID);    
-- 55 +/- SELECT COUNT(*) FROM PUBLIC.PROPERTY_DEFINITION;    
INSERT INTO PUBLIC.PROPERTY_DEFINITION(ID, CARDINALITY, DESCRIPTION, DISPLAYNAME, INHERITED, LOCALNAME, LOCALNAMESPACE, OBJECTID, OBJECT_TYPE_ID, ORDERABLE, QUERYNAME, QUERYABLE, REQUIRED, TYPE, UPDATABILITY) VALUES
(1, 'single', NULL, 'Version series checked out Id', FALSE, 'cmis:versionSeriesCheckedOutId', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'cmis:versionSeriesCheckedOutId', 1, NULL, 'cmis:versionSeriesCheckedOutId', NULL, FALSE, 'id', 'readonly'),
(2, 'single', NULL, 'Version series checked out by', FALSE, 'cmis:versionSeriesCheckedOutBy', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'cmis:versionSeriesCheckedOutBy', 1, NULL, 'cmis:versionSeriesCheckedOutBy', NULL, FALSE, 'string', 'readonly'),
(3, 'single', NULL, 'Parent Id', FALSE, 'cmis:parentId', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'cmis:parentId', 1, NULL, 'cmis:parentId', NULL, FALSE, 'id', 'readonly'),
(4, 'single', NULL, 'Base Type', FALSE, 'cmis:baseTypeId', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'cmis:baseTypeId', 1, NULL, 'cmis:baseTypeId', NULL, FALSE, 'id', 'oncreate'),
(5, 'single', NULL, 'Latest major version', FALSE, 'cmis:isLatestMajorVersion', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'cmis:isLatestMajorVersion', 1, NULL, 'cmis:isLatestMajorVersion', NULL, FALSE, 'boolean', 'readonly'),
(6, 'single', NULL, 'Checkin comment', FALSE, 'cmis:checkinComment', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'cmis:checkinComment', 1, NULL, 'cmis:checkinComment', NULL, FALSE, 'string', 'readonly'),
(7, 'single', NULL, 'Created By', FALSE, 'cmis:createdBy', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'cmis:createdBy', 1, NULL, 'cmis:createdBy', NULL, FALSE, 'string', 'oncreate'),
(8, 'single', NULL, 'Immutable', FALSE, 'cmis:isImmutable', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'cmis:isImmutable', 1, NULL, 'cmis:isImmutable', NULL, FALSE, 'boolean', 'readonly'),
(9, 'single', NULL, 'Creation Date', FALSE, 'cmis:creationDate', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'cmis:creationDate', 1, NULL, 'cmis:creationDate', NULL, FALSE, 'datetime', 'readonly'),
(10, 'single', NULL, 'Last Modifed By', FALSE, 'cmis:lastModifiedBy', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'cmis:lastModifiedBy', 1, NULL, 'cmis:lastModifiedBy', NULL, FALSE, 'string', 'readwrite'),
(11, 'single', NULL, 'Version series Id', FALSE, 'cmis:versionSeriesId', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'cmis:versionSeriesId', 1, NULL, 'cmis:versionSeriesId', NULL, FALSE, 'id', 'readonly'),
(12, 'single', NULL, 'Content stream mime type', FALSE, 'cmis:contentStreamMimeType', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'cmis:contentStreamMimeType', 1, NULL, 'cmis:contentStreamMimeType', NULL, FALSE, 'string', 'oncreate'),
(13, 'single', NULL, 'Object Id', FALSE, 'cmis:objectId', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'cmis:objectId', 1, NULL, 'cmis:objectId', NULL, FALSE, 'id', 'oncreate'),
(14, 'single', NULL, 'Version', FALSE, 'cmis:versionLabel', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'cmis:versionLabel', 1, NULL, 'cmis:versionLabel', NULL, FALSE, 'string', 'readonly'),
(15, 'single', NULL, 'Name', FALSE, 'cmis:name', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'cmis:name', 1, NULL, 'cmis:name', NULL, TRUE, 'string', 'readwrite'),
(16, 'single', NULL, 'Major version', FALSE, 'cmis:isMajorVersion', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'cmis:isMajorVersion', 1, NULL, 'cmis:isMajorVersion', NULL, FALSE, 'boolean', 'readonly'),
(17, 'single', NULL, 'Content stream length', FALSE, 'cmis:contentStreamLength', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'cmis:contentStreamLength', 1, NULL, 'cmis:contentStreamLength', NULL, FALSE, 'integer', 'readonly'),
(18, 'single', NULL, 'Last Modification Date', FALSE, 'cmis:lastModificationDate', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'cmis:lastModificationDate', 1, NULL, 'cmis:lastModificationDate', NULL, FALSE, 'datetime', 'readonly'),
(19, 'single', NULL, 'Path', FALSE, 'cmis:path', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'cmis:path', 1, NULL, 'cmis:path', NULL, FALSE, 'string', 'readonly');        
INSERT INTO PUBLIC.PROPERTY_DEFINITION(ID, CARDINALITY, DESCRIPTION, DISPLAYNAME, INHERITED, LOCALNAME, LOCALNAMESPACE, OBJECTID, OBJECT_TYPE_ID, ORDERABLE, QUERYNAME, QUERYABLE, REQUIRED, TYPE, UPDATABILITY) VALUES
(20, 'single', NULL, 'Change Token', FALSE, 'cmis:changetoken', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'cmis:changetoken', 1, NULL, 'cmis:changetoken', NULL, FALSE, 'string', 'readonly'),
(21, 'single', NULL, 'Version series checked out', FALSE, 'cmis:isVersionSeriesCheckedOut', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'cmis:isVersionSeriesCheckedOut', 1, NULL, 'cmis:isVersionSeriesCheckedOut', NULL, FALSE, 'boolean', 'readonly'),
(22, 'single', NULL, 'Content stream file name', FALSE, 'cmis:contentStreamFileName', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'cmis:contentStreamFileName', 1, NULL, 'cmis:contentStreamFileName', NULL, FALSE, 'string', 'oncreate'),
(23, 'single', NULL, 'Content stream id', FALSE, 'cmis:contentStreamId', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'cmis:contentStreamId', 1, NULL, 'cmis:contentStreamId', NULL, FALSE, 'id', 'readonly'),
(24, 'single', NULL, 'Object Type', FALSE, 'cmis:objectTypeId', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'cmis:objectTypeId', 1, NULL, 'cmis:objectTypeId', NULL, TRUE, 'id', 'oncreate'),
(25, 'single', NULL, 'Latest version', FALSE, 'cmis:isLatestVersion', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'cmis:isLatestVersion', 1, NULL, 'cmis:isLatestVersion', NULL, FALSE, 'boolean', 'readonly'),
(26, 'single', NULL, 'Object Id', FALSE, 'cmis:objectId', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'cmis:objectId', 2, NULL, 'cmis:objectId', NULL, FALSE, 'id', 'oncreate'),
(27, 'single', NULL, 'Name', FALSE, 'cmis:name', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'cmis:name', 2, NULL, 'cmis:name', NULL, TRUE, 'string', 'readwrite'),
(28, 'single', NULL, 'Last Modifed By', FALSE, 'cmis:lastModifiedBy', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'cmis:lastModifiedBy', 2, NULL, 'cmis:lastModifiedBy', NULL, FALSE, 'string', 'readonly'),
(29, 'single', NULL, 'Last Modification Date', FALSE, 'cmis:lastModificationDate', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'cmis:lastModificationDate', 2, NULL, 'cmis:lastModificationDate', NULL, FALSE, 'datetime', 'readonly'),
(30, 'single', NULL, 'Created By', FALSE, 'cmis:createdBy', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'cmis:createdBy', 2, NULL, 'cmis:createdBy', NULL, FALSE, 'string', 'oncreate'),
(31, 'single', NULL, 'Allowed child object type ids', FALSE, 'cmis:allowedChildObjectTypeIds', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'cmis:allowedChildObjectTypeIds', 2, NULL, 'cmis:allowedChildObjectTypeIds', NULL, FALSE, 'id', 'readonly'),
(32, 'single', NULL, 'Object Type', FALSE, 'cmis:objectTypeId', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'cmis:objectTypeId', 2, NULL, 'cmis:objectTypeId', NULL, TRUE, 'id', 'oncreate'),
(33, 'single', NULL, 'Path', FALSE, 'cmis:path', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'cmis:path', 2, NULL, 'cmis:path', NULL, FALSE, 'string', 'readonly'),
(34, 'single', NULL, 'Parent Id', FALSE, 'cmis:parentId', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'cmis:parentId', 2, NULL, 'cmis:parentId', NULL, FALSE, 'id', 'readonly'),
(35, 'single', NULL, 'Change Token', FALSE, 'cmis:changetoken', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'cmis:changetoken', 2, NULL, 'cmis:changetoken', NULL, FALSE, 'string', 'readonly'),
(36, 'single', NULL, 'Base Type', FALSE, 'cmis:baseTypeId', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'cmis:baseTypeId', 2, NULL, 'cmis:baseTypeId', NULL, FALSE, 'id', 'readonly'),
(37, 'single', NULL, 'Creation Date', FALSE, 'cmis:creationDate', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'cmis:creationDate', 2, NULL, 'cmis:creationDate', NULL, FALSE, 'datetime', 'readonly'),
(38, 'multi', NULL, 'Allowed source types', FALSE, 'cmis:allowedSourceTypes', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'cmis:allowedSourceTypes', 3, NULL, 'cmis:allowedSourceTypes', NULL, NULL, 'string', 'oncreate');       
INSERT INTO PUBLIC.PROPERTY_DEFINITION(ID, CARDINALITY, DESCRIPTION, DISPLAYNAME, INHERITED, LOCALNAME, LOCALNAMESPACE, OBJECTID, OBJECT_TYPE_ID, ORDERABLE, QUERYNAME, QUERYABLE, REQUIRED, TYPE, UPDATABILITY) VALUES
(39, 'multi', NULL, 'Allowed target types', FALSE, 'cmis:allowedTargetTypes', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'cmis:allowedTargetTypes', 3, NULL, 'cmis:allowedTargetTypes', NULL, NULL, 'string', 'oncreate'),
(40, 'single', NULL, 'Title', FALSE, 'bb:title', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'bb:title', 7, NULL, 'bb:title', NULL, FALSE, 'string', 'readwrite'),
(41, 'single', NULL, 'Description', FALSE, 'bb:description', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'bb:description', 7, NULL, 'bb:description', NULL, FALSE, 'string', 'readwrite'),
(42, 'single', NULL, 'Sub Title', FALSE, 'bb:subTitle', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'bb:subTitle', 7, NULL, 'bb:subTitle', NULL, FALSE, 'string', 'readwrite'),
(43, 'single', NULL, 'Sub Title', FALSE, 'bb:subTitle', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'bb:subTitle', 8, NULL, 'bb:subTitle', NULL, FALSE, 'string', 'readwrite'),
(44, 'single', NULL, 'Alt Text', FALSE, 'bb:altText', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'bb:altText', 8, NULL, 'bb:altText', NULL, FALSE, 'string', 'readwrite'),
(45, 'single', NULL, 'Height', FALSE, 'bb:height', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'bb:height', 8, NULL, 'bb:height', NULL, FALSE, 'integer', 'readwrite'),
(46, 'single', NULL, 'Title', FALSE, 'bb:title', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'bb:title', 8, NULL, 'bb:title', NULL, TRUE, 'string', 'readwrite'),
(47, 'single', NULL, 'Width', FALSE, 'bb:width', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'bb:width', 8, NULL, 'bb:width', NULL, FALSE, 'integer', 'readwrite'),
(48, 'single', NULL, 'Tag', FALSE, 'bb:tag', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'bb:tag', 8, NULL, 'bb:tag', NULL, FALSE, 'string', 'readwrite'),
(49, 'single', NULL, 'Description', FALSE, 'bb:description', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'bb:description', 8, NULL, 'bb:description', NULL, FALSE, 'string', 'readwrite'),
(50, 'single', NULL, 'Sub Title', FALSE, 'bb:subTitle', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'bb:subTitle', 9, NULL, 'bb:subTitle', NULL, FALSE, 'string', 'readwrite'),
(51, 'single', NULL, 'Url', FALSE, 'bb:url', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'bb:url', 9, NULL, 'bb:url', NULL, TRUE, 'string', 'readwrite'),
(52, 'single', NULL, 'Title', FALSE, 'bb:title', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'bb:title', 9, NULL, 'bb:title', NULL, TRUE, 'string', 'readwrite'),
(53, 'single', NULL, 'Uuid', FALSE, 'bb:uuid', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'bb:uuid', 10, NULL, 'bb:uuid', NULL, TRUE, 'string', 'oncreate'),
(54, 'single', NULL, 'Package Type', FALSE, 'bb:packageType', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'bb:packageType', 10, NULL, 'bb:packageType', NULL, TRUE, 'string', 'oncreate'),
(55, 'single', NULL, 'publishId', FALSE, 'bb:publishId', 'http://docs.oasis-open.org/ns/cmis/core/200908/', 'bb:publishId', 10, NULL, 'bb:publishId', NULL, TRUE, 'string', 'oncreate');               
CREATE INDEX PUBLIC.FK_PDEF_IDX_OBJECT_TYPE_ID ON PUBLIC.PROPERTY_DEFINITION(OBJECT_TYPE_ID); 
CREATE CACHED TABLE PUBLIC.RENDITION(
    ID BIGINT DEFAULT (NEXT VALUE FOR PUBLIC.SYSTEM_SEQUENCE_D1ABD5BA_BCE0_4454_B09C_688BEF6B9EBE) NOT NULL NULL_TO_DEFAULT SEQUENCE PUBLIC.SYSTEM_SEQUENCE_D1ABD5BA_BCE0_4454_B09C_688BEF6B9EBE,
    FILEPATH VARCHAR(765),
    HEIGHT DECIMAL(19, 2),
    KIND VARCHAR(255),
    LENGTH DECIMAL(19, 2),
    MIMETYPE VARCHAR(255),
    OBJECT_DATA_ID BIGINT NOT NULL,
    RENDITIONDOCUMENTID VARCHAR(255),
    TITLE VARCHAR(255),
    WIDTH DECIMAL(19, 2),
    OBJECTID VARCHAR(255) NOT NULL,
    STREAMID VARCHAR(255) NOT NULL,
    CS_ID BIGINT
);            
ALTER TABLE PUBLIC.RENDITION ADD CONSTRAINT PUBLIC.CONSTRAINT_1 PRIMARY KEY(ID);              
-- 0 +/- SELECT COUNT(*) FROM PUBLIC.RENDITION;               
CREATE INDEX PUBLIC.FK_RND_IDX_OBJECT_DATA_ID ON PUBLIC.RENDITION(OBJECT_DATA_ID);            
ALTER TABLE PUBLIC.OBJECT_TYPE_DEFINITION ADD CONSTRAINT PUBLIC.UK_MH6A3UQ00W5DT5NCRB1BSSF9I UNIQUE(OBJECTID);
ALTER TABLE PUBLIC.OBJECT_DATA ADD CONSTRAINT PUBLIC.UK_9QXIOE1WRRD5D48CW062JC161 UNIQUE(OBJECTID);           
ALTER TABLE PUBLIC.RENDITION ADD CONSTRAINT PUBLIC.FK_L7RL4ULHH823P0T9OSWG761C9 FOREIGN KEY(CS_ID) REFERENCES PUBLIC.CONTENT_STREAM(ID) NOCHECK;              
ALTER TABLE PUBLIC.OBJECT_DATA ADD CONSTRAINT PUBLIC.FK_MHP750ONSG14DEQSC5MKP4DX FOREIGN KEY(PARENT_ID) REFERENCES PUBLIC.OBJECT_DATA(ID) NOCHECK;            
ALTER TABLE PUBLIC.OBJECT_TYPE_DEFINITION ADD CONSTRAINT PUBLIC.FK_83A12DVJDI2PSDS4T3PFRWBFB FOREIGN KEY(PARENT_ID) REFERENCES PUBLIC.OBJECT_TYPE_DEFINITION(ID) NOCHECK;     
ALTER TABLE PUBLIC.OBJECT_DATA ADD CONSTRAINT PUBLIC.FK_354Y502CT1JU7TG6V1RLFPWGO FOREIGN KEY(OBJECT_TYPE_ID) REFERENCES PUBLIC.OBJECT_TYPE_DEFINITION(ID) NOCHECK;           
ALTER TABLE PUBLIC.PROPERTY_DATA ADD CONSTRAINT PUBLIC.FK_R5PFQ0QMO565NB85Q01Q6R5OG FOREIGN KEY(OBJECT_DATA_ID) REFERENCES PUBLIC.OBJECT_DATA(ID) NOCHECK;    
ALTER TABLE PUBLIC.PROPERTY_DATA ADD CONSTRAINT PUBLIC.FK_C2YSHJU50DY6NUTODTEOGO28T FOREIGN KEY(PROPERTY_DEFINITION_ID) REFERENCES PUBLIC.PROPERTY_DEFINITION(ID) NOCHECK;    
ALTER TABLE PUBLIC.PROPERTY_DEFINITION ADD CONSTRAINT PUBLIC.FK_S6SCXSPSKRPJULQSJ4U98US2G FOREIGN KEY(OBJECT_TYPE_ID) REFERENCES PUBLIC.OBJECT_TYPE_DEFINITION(ID) NOCHECK;   
ALTER TABLE PUBLIC.OBJECT_DATA ADD CONSTRAINT PUBLIC.FK_I6HWFPQ7PPVKTIIM2THPQXIAC FOREIGN KEY(CS_ID) REFERENCES PUBLIC.CONTENT_STREAM(ID) NOCHECK;            
ALTER TABLE PUBLIC.RENDITION ADD CONSTRAINT PUBLIC.FK_AW8VTQ9LH7EHWFU0OC30Q0HQN FOREIGN KEY(OBJECT_DATA_ID) REFERENCES PUBLIC.OBJECT_DATA(ID) NOCHECK;        
