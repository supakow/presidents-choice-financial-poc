package com.backbase.launchpad.presentation.tag;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.tags.RequestContextAwareTag;

import com.backbase.portal.foundation.domain.model.User;
import org.springframework.web.util.TagUtils;

/**
 * Set a user property on the page context
 *
 * @author Vladimir Raskin, Phil Mander
 */
public class UserPropertyTag extends RequestContextAwareTag {

    private String userPropertyName;

    private String defaultValue;

    private String var;

    private String scope = "page";

    @Override
    protected int doStartTagInternal() throws Exception {
        String propertyValue = getDefaultValue();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.getPrincipal() != null) {
            User user = (User) authentication.getPrincipal();
            if(user.getPropertyDefinitions().containsKey(userPropertyName)) {
                propertyValue = user.getPropertyDefinitions().get(userPropertyName).getValue().getInternalValue();
            }
        }

        pageContext.setAttribute(var, propertyValue, TagUtils.getScope(scope) );

        return SKIP_BODY;
    }

    public String getUserPropertyName() {
        return userPropertyName;
    }

    public void setUserPropertyName(String userPropertyName) {
        this.userPropertyName = userPropertyName;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getVar() {
        return var;
    }

    public void setVar(String var) {
        this.var = var;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }
}
