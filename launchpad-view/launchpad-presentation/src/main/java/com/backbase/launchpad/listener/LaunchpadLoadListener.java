package com.backbase.launchpad.listener;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.List;

import com.backbase.portal.commons.configuration.BackbaseConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationObjectSupport;

/**
 * Created with IntelliJ IDEA.
 * User: philip
 * Date: 19-3-13
 * Time: 18:52
 * To change this template use File | Settings | File Templates.
 */

public class LaunchpadLoadListener extends WebApplicationObjectSupport {

    private static final Logger log = LoggerFactory.getLogger(LaunchpadLoadListener.class);

    private static final String LAUNCHPAD_ATTR_PREFIX = "lpconf_";

    private static final String LAUNCHPAD_PROPERTY_PREFIX = "launchpad.";

    private static final String PROP_BUNDLES = "bundles";

    private static final String PROP_THEMEPATH = "themePath";

    private static final String PROP_USEFRONTENDBUILD = "useFrontendBuild";

    private List<String> propertiesFilesToLoad = null;

    @Autowired
    @Qualifier("backbaseConfiguration")
    private BackbaseConfiguration backbaseConfiguration;

    public LaunchpadLoadListener(List<String> propertiesFiles) {

        this.propertiesFilesToLoad = propertiesFiles;
    }

    protected void initApplicationContext(ApplicationContext context) {

        //the list of bundles the page template should attempt to include
        String[] bundles = backbaseConfiguration.getStringArray(LAUNCHPAD_PROPERTY_PREFIX + PROP_BUNDLES);
        getServletContext().setAttribute(LAUNCHPAD_ATTR_PREFIX  + PROP_BUNDLES, bundles);

        //should compressed resource be used?
        boolean useFrontendBuild = backbaseConfiguration.getBoolean(LAUNCHPAD_PROPERTY_PREFIX + PROP_USEFRONTENDBUILD, false);
        getServletContext().setAttribute(LAUNCHPAD_ATTR_PREFIX  + PROP_USEFRONTENDBUILD, useFrontendBuild);

        //define a theme path
        String themePath = backbaseConfiguration.getString(LAUNCHPAD_PROPERTY_PREFIX + PROP_THEMEPATH);
        getServletContext().setAttribute(LAUNCHPAD_ATTR_PREFIX  + PROP_THEMEPATH, themePath);

        for(String propertiesFile : propertiesFilesToLoad) {
            Properties props = loadProperties(propertiesFile + ".properties");
            if(props != null) {
                getServletContext().setAttribute(LAUNCHPAD_ATTR_PREFIX + propertiesFile, props);
            }
        }
    }

    private Properties loadProperties(String propertiesFile) {

        InputStream propsStreamIn = Thread.currentThread().getContextClassLoader().getResourceAsStream(propertiesFile);
        Properties props = null;
        try {
            if (propsStreamIn == null) {
                throw new IOException(new NullPointerException());
            }
            props = new Properties();
            props.load(propsStreamIn);
            log.info("Properties file loaded: " + propertiesFile);
        } catch (IOException e) {
            log.warn("Failed to load properties: " + propertiesFile);
        } finally {
            try {
                if(propsStreamIn != null) {
                    propsStreamIn.close();
                }
            } catch(IOException e) {
                log.warn("Could not close input stream for properties file: " + propertiesFile, e);
            }
        }
        return props;
    }
}
