package com.backbase.launchpad.utils;

import com.backbase.portal.foundation.domain.conceptual.BasePropertyValue;
import com.backbase.portal.foundation.domain.conceptual.Item;
import com.backbase.portal.foundation.domain.model.ItemType;
import com.backbase.portal.foundation.security.SecurityProfile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Set of functions for working Portal 5 items. Mainly intended for use as jsp expression language functions.
 *
 * @author philip
 *
 */
public final class PortalFunctions {

    private static final Logger log = LoggerFactory.getLogger(PortalFunctions.class);

    private PortalFunctions(){}

    /**
     * Quick way to get String representation of a property value from an Item
     *
     * @param item
     * @param propertyName
     */
    public static Object getPropertyValue(Item item, String propertyName) {

        Object value;

        BasePropertyValue<?> property = item.getProperty(propertyName);
        if (property != null) {
            value = property.getValue();
        } else {
            log.warn("The property named '" + propertyName + "' could not be found for '" + item.getName() + "' at "
                    + item.getURI());
            value = "";
        }

        return value;
    }


    /**
     * Returns true if the property name exists on an item and is not an empty
     * string
     *
     * @param item The item to check
     * @param propertyName
     * @return
     */
    public static boolean hasProperty(Item item, String propertyName) {

        boolean hasProperty = false;
        try {
            hasProperty = item.getPropertyKeys().contains(propertyName)
                    && !item.getProperty(propertyName).getValue().toString().isEmpty();
        }
        catch(Exception e){
            log.error("Could not do a check item property", e);
        }
        return hasProperty;
    }

    /**
     * Check if the item is of type PAGE
     *
     * @param item
     * @return A boolean value indicating if the type matches
     */
    public static boolean isPage(Item item) {

        return item.getItemType() == ItemType.PAGE;
    }

    /**
     * Check if the item is of type CONTAINER
     *
     * @param item
     * @return A boolean value indicating if the type matches
     */
    public static boolean isContainer(Item item) {

        return item.getItemType() == ItemType.CONTAINER;
    }

    /**
     * Check if the item is of type WIDGET
     *
     * @param item
     * @return A boolean value indicating if the type matches
     */
    public static boolean isWidget(Item item) {

        return item.getItemType() == ItemType.WIDGET;
    }

    /**
     * Check if the item is of type CATALOG
     *
     * @param item
     * @return A boolean value indicating if the type matches
     */
    public static boolean isCatalog(Item item) {

        return item.getItemType() == ItemType.CATALOG;
    }

    /**
     * Check if the item is of type LINK
     *
     * @param item
     * @return A boolean value indicating if the type matches
     */
    public static boolean isLink(Item item) {

       return item.getItemType() == ItemType.LINK;
    }

    /**
     * Returns true if the item us personalizable by the user.
     * @param item The item to test
     * @return true if the use can at least personlize
     */
    public static boolean canPersonalize(Item item) {

        boolean canPersonalize = SecurityProfile.compare(item.getSecurityProfile(), SecurityProfile.COLLABORATOR) >= 0;
        return canPersonalize;
    }
}