package com.backbase.launchpad.presentation.tag;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.tags.RequestContextAwareTag;
import org.springframework.web.util.TagUtils;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NameNotFoundException;
import javax.servlet.jsp.PageContext;

/**
 * Temporary tag to get a jndi environment entry
 */
public class EnvEntryLookupTag extends RequestContextAwareTag {

    private static final Logger log = LoggerFactory.getLogger(EnvEntryLookupTag.class);

    private static final String LAUNCHPAD_ATTR_PREFIX = "launchpad.env.";

    private String name = null;

    private String var = null;

    private String scope = "page";

    @Override
    protected int doStartTagInternal() throws Exception {

        String envValue = "";
        String attrName = LAUNCHPAD_ATTR_PREFIX + name;

        if(pageContext.getAttribute(attrName, PageContext.APPLICATION_SCOPE) == null) {
            try {
                Context env = (Context) new InitialContext().lookup("java:comp/env");
                envValue = (String) env.lookup(name);

                //cache the env entry on the application scope
                pageContext.setAttribute(attrName, PageContext.APPLICATION_SCOPE);
            } catch (NameNotFoundException e) {
                log.warn("Could not find a JNDI env entry for [" + name + "].");
            }
        } else {
            envValue = (String) pageContext.getAttribute(attrName, PageContext.APPLICATION_SCOPE);
        }

        pageContext.setAttribute(var, envValue, TagUtils.getScope(scope));

        return SKIP_BODY;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVar() {
        return var;
    }

    public void setVar(String var) {
        this.var = var;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }
}
