package com.backbase.launchpad.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

/**
 * Created with IntelliJ IDEA.
 * User: bartv
 * Date: 1-8-12
 * Time: 14:58
 * To change this template use File | Settings | File Templates.
 */
public class ContentServicesLocalePathTransformer {


    private static final Logger log = LoggerFactory.getLogger(ContentServicesLocalePathTransformer.class);

    /**
     * Transforms the Content Services Path to include the browser sessions locale
     *
     * @param path   The Content Services Path
     * @param locale The Request Context Locale
     * @return a Path which is prefixed with the locale. Input: /Generated Content/bla.html Output /en/Generated Content/bla.html
     */
    public static String transformPath(String path, Locale locale) {
        String result = null;
        if (StringUtils.hasText(path))
            result = "/" + locale.getLanguage() + path;
        else
            result = path;
        log.info("Transformed {} to: {}", path, result);
        return result;
    }

    /**
     * Transforms teh Content Services Path and replaces the value in the parameters
     *
     * @param request
     */
    public static void transformPath(HttpServletRequest request) {
        String path = request.getParameter("path");
        if(path == null)
            return;
        String newPath = transformPath(path, request.getLocale());
        request.getParameterMap().put("path", newPath);
    }
}
