package com.backbase.launchpad.utils;

import org.apache.commons.fileupload.RequestContext;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author Vladimir Raskin
 */
public class PostBodyRequestContext implements RequestContext {

    private String characterEncoding;

    private String contentType;

    private InputStream stream;

    private int contentLength;

    public PostBodyRequestContext(String characterEncoding, String contentType, byte[] postBody, int contentLength) {
        this.characterEncoding = characterEncoding;
        this.contentType = contentType;
        this.stream = new ByteArrayInputStream(postBody);
        this.contentLength = contentLength;
    }

    public String getCharacterEncoding() {
        return characterEncoding;
    }

    public String getContentType() {
        return contentType;
    }

    public int getContentLength() {
        return contentLength;
    }

    public InputStream getInputStream() throws IOException {
        return stream;
    }
}
