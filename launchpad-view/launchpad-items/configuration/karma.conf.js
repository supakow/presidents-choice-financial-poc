// Karma configuration
// install karma, then run karma start,

module.exports = function(config) {
  config.set({

    // base path, that will be used to resolve files and exclude
    basePath: '../webapp',


    // frameworks to use
    frameworks: ['jasmine', 'requirejs'],


    // list of files / patterns to load in the browser
	  files: [

		  { pattern: 'static/launchpad/behaviors/**/*.js', included: false, watched: false },
		  { pattern: 'static/launchpad/support/**/*.js', included: false, watched: false },
		  { pattern: 'static/launchpad/lib/**/*.js', included: false, watched: false },
          { pattern: 'static/launchpad/pages/**/*.js', included: false, watched: false },
		  { pattern: 'static/launchpad/containers/**/*.js', included: false, watched: false },
		  { pattern: 'static/launchpad/widgets/**/*.js', included: false, watched: false },
		  { pattern: 'static/launchpad/test/**/*.js', included: false, watched: false },

		  'static/launchpad/conf/test-main.js'
	  ],

    // test results reporter to use
    // possible values: 'dots', 'progress', 'junit', 'growl', 'coverage'
	  reporters: ['dots','progress', 'junit', 'coverage'],

	  junitReporter: {
		  // will be resolved to basePath (in the same way as files/exclude patterns)
		  outputFile: '../target/reports/karma-test-results.xml'
	  },


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // Start these browsers, currently available:
    // - Chrome
    // - ChromeCanary
    // - Firefox
    // - Opera (has to be installed with `npm install karma-opera-launcher`)
    // - Safari (only Mac; has to be installed with `npm install karma-safari-launcher`)
    // - PhantomJS
    // - IE (only Windows; has to be installed with `npm install karma-ie-launcher`)
    browsers: ['PhantomJS'],


    // If browser does not capture in given timeout [ms], kill it
    captureTimeout: 60000,


    preprocessors : {
      'static/launchpad/lib/**/*.js': ['coverage'],
      'static/launchpad/widgets/**/*.js': ['coverage'],
      'static/launchpad/containers/*/*.js': ['coverage']
    },

    coverageReporter: {
      type : 'cobertura',
      dir : '../target/reports/karma-coverage'
    },

    // Continuous Integration mode
    // if true, it capture browsers, run tests and exit
    singleRun: false
  });
};
