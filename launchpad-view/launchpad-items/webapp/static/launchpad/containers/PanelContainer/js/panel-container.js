/*global b$ */
(function (b$, window) {
    "use strict";
    var Container = b$.bdom.getNamespace('http://backbase.com/2013/portalView').getClass('container');
//  ----------------------------------------------------------------
    var PanelContainer = Container.extend(function (bdomDocument, node) {
        Container.apply(this, arguments);
        this.isPossibleDragTarget = true;
    }, {
        localName: 'PanelContainer',
        namespaceURI: 'launchpad',
        reflow: function(e) {
            window.setTimeout(function(){
                Container.prototype.reflow.call(this, e);
            }, 0);

        }
    }, {
        template: function(json) {
            var data = {item: json.model.originalItem};
            var sTemplate = window.launchpad.PanelContainer(data);
            return sTemplate;
        }
    });
})(b$, window);
