/*global b$, gadgets, lp, bd, window, console $*/
(function (b$, gadgets, lp, bd, window, $) {
    "use strict";

    var MAIN_PANEL_AREA = "0";
    var PANEL_NAME_PREFIX = "Panel";

    //TODO: this is a text representation of a Panel Container, will need to fetch this from server catlaog
    var text = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><container><name>panel-container</name><contextItemName>' + b$.portal.portalName + '</contextItemName><extendedItemName>panel-container</extendedItemName><properties><property name="TemplateName" readonly="false" itemName="panel-container" manageable="true"><value type="string">PanelContainer</value></property><property label="Title" name="title" readonly="false" itemName="panel-container" viewHint="text-input,designModeOnly,user" manageable="true"><value type="string">Panel Container</value></property><property name="Description" readonly="false" itemName="panel-container" manageable="true"><value type="string">Generic empty panel which can be added to a Deck Container as a child.</value></property><property name="thumbnailUrl" readonly="false" itemName="panel-container" manageable="true"><value type="string">$(contextRoot)/static/launchpad/containers/PanelContainer/media/icon.png</value></property><property name="state" readonly="false" itemName="panel-container" manageable="true"><value type="string"></value></property><property name="config" readonly="false" itemName="panel-container" manageable="true"><value type="string">$(contextRoot)/static/launchpad/containers/PanelContainer/config.xml</value></property><property name="csr" readonly="false" itemName="panel-container" manageable="true"><value type="boolean">false</value></property></properties><itemHandlerBeanName>containerDefinitionHandler</itemHandlerBeanName><createdBy>admin</createdBy><createdTimestamp>2014-03-27T13:56:44.598+01:00</createdTimestamp><lastModifiedBy>admin</lastModifiedBy><lastModifiedTimestamp>2014-03-27T13:56:44.598+01:00</lastModifiedTimestamp><securityProfile>ADMIN</securityProfile><hidden>false</hidden><contents/><lastModifiedItem><name>panel-container</name><createdBy>admin</createdBy><createdTimestamp>2014-03-27T13:56:44.598+01:00</createdTimestamp><lastModifiedBy>admin</lastModifiedBy><lastModifiedTimestamp>2014-03-27T13:56:44.598+01:00</lastModifiedTimestamp></lastModifiedItem><tags><tag manageable="true" type="section">layouts</tag></tags><publishState>NOT_PUBLISHED</publishState><uuid>2450794d-9df5-449b-af3c-c6f988c70e71</uuid><lockState>UNLOCKED</lockState><children/></container>';
    var NS = b$.bdom.getNamespace('http://backbase.com/2013/portalView'),
        panelsActive = {}, //stores a list of the active panels inside the deck containers (keys are the names of the deck containers)
        initialized = false,
        savingPanels = 0; //number of panels asynchronously being saved
//  ----------------------------------------------------------------
    var Container = NS.classes.container;
    var DeckContainer = Container.extend(function (bdomDocument, node) {
        Container.apply(this, arguments);
        this.isPossibleDragTarget = true;
    }, {
        localName: 'DeckContainer',
        namespaceURI: 'launchpad',
        /**
         * Returns the number of panels inside this container including those being saved asynchcronously
         */
        _getNumberOfPanels: function(){
            return this.childNodes.length + savingPanels;
        },
        /**
         * Returns the list of panels inside the container
         */
        _getPanels: function(){
          return this.childNodes;
        },
        _displayInitialPanel: function(){
            this.showPanel(0);
        },
        /*
        * Initializes the state and establish the events handlers for state change
        */
        reflow: function(){
            var self = this;
            //update display on back
            Container.prototype.reflow.call(this);
        },
        /**
         * Sets up the container by creating 3 initial panels inside the container
         * @constructor
         */
        DOMReady: function(e) {
            var self = this;
            var initialized = lp.util.parseBoolean(this.getPreference('initialized'));
            if(!initialized){
                this.setPreference('initialized', true);
                this.model.save();
                var initialNumPanels = 3;
                var i;
                for(i = 0; i < initialNumPanels; i++) {
                    this.addPanel();
                }
            }
            Container.prototype.createDisplay.call(this, e);

            window.setTimeout(function(){
                self._displayInitialPanel();
            }, 0);
        },
        /**
         * Returns a specific panel
         *  @param id: (string) name or index of the panel to be removed
         */
        getPanel: function(id){
          var panels = this._getPanels();

          if(typeof id === "number" && id < panels.length){
            return panels[id];
          } else if(typeof id === "string"){
            var panelsNamedAsid = $.grep(panels, function( p ) {
              return p.model.name === id;
            });
            if(panelsNamedAsid.length > 0){
              return panelsNamedAsid[0];
            }
            console.warn("Panel " + id + " doesn't exist");
          }

          return null;
        },
        getActivePanel: function(){
          var panels = this._getPanels();
          return panels[panelsActive[this.model.name] || 0];
        },
        /**
         * Displays a panel given an index or a name. All other panels are hidden.
         *  @param panelId: (string) name or index of the panel to be removed
         */
        showPanel: function(panelId){
          var panels = this._getPanels(),
              panel;
          if(typeof panelId === "number" || typeof panelId === "string"){
            panel = this.getPanel(panelId);
          }else if(typeof panelId === "object"){
            panel = panelId;
          }

          if(panel){
            if(panels[panelsActive[this.model.name] || 0]){
                $(panels[panelsActive[this.model.name] || 0].htmlNode).removeClass("active");
            }
            $(panel.htmlNode).addClass("active");
            panelsActive[this.model.name] = panels.indexOf(panel);
            return true;
          }else{
            return false;
          }
        },
        /**
         * Adds a new panel into the container
         *  @param name: (string) dafaulted to 'Panel' + <sequence number>
         */
        addPanel: function(name) {
            var self = this;
            var xml = $.parseXML(text);

            var panelContainer = b$.portal.portalServer.itemXMLDOC2JSON(xml);

            var newItem = this.model.createExtendedElementFromJSON(panelContainer);
            newItem.parentItemName = this.model.name;
            newItem.setPreference('area', MAIN_PANEL_AREA);
            newItem.setPreference('title', name || PANEL_NAME_PREFIX + (this._getNumberOfPanels() + 1) );

            // missing preferences on the originalItem. TODO: what does this do
            newItem.originalItem.preferences = newItem.originalItem.preferences || {};

            // counter for the number of panels being saved at the moment
            savingPanels++;

            newItem.save(function(){
                self.model.appendChild(newItem);
                savingPanels--;

                // redraw the container when all panels are saved
                if(savingPanels === 0) {
                    window.setTimeout(function(){
                        self._displayInitialPanel();
                        self.refreshHTML();
                    }, 0);
                }
            });
        },
        /**
         * Removes a panel inside the container
         *  @param id: (string) name or index of the panel to be removed
         */
        removePanel: function(id){
          var panel = this.getPanel(id);
          try {
            panel.model.destroyAndSave();
          }
          catch(e){
            console.warn(e.message);
          }
        },
        /**
         * Changes the name of a panel
         *  @param id: (string) name or index of the panel to be renamed
         *  @param name: (string) new name for the panel
         */
        renamePanel: function(id, name){
          var panel = this.getPanel(id);
          panel.setPreference('title', name);
          panel.model.save();
        },
        /**
         * TODO: this method will lazy load panels based on behaviors
         */
        loadByBehavior: function(childBehaviorTag, callback) {
          var matchingChildren = lp.util.findMatchingChildrenByTag(this, childBehaviorTag);
          if(matchingChildren.length > 0) {
              if(typeof callback === "function") {
                  callback();
              }
          } else {
              console.warn("Couldn't load widget, no children with the behavior tag [" + childBehaviorTag + "] were found.");
          }
        },
        /**
         * TODO: this method will display panels based on behaviors
         */
        showByBehavior: function(childBehaviorTag) {
          var matchingChildren = lp.util.findMatchingChildrenByTag(this, childBehaviorTag);
          if(matchingChildren.length === 0) {
              console.warn("Couldn't show widget, no children with the behavior tag [" + childBehaviorTag + "] were found.");
          } else {
              //warn, but still proceed to show the first matching child
              if(matchingChildren.length > 1) {
                  console.warn("More than one widget with the behavior tag [" + childBehaviorTag + "] was  found. Selecting the first.");
              }
              this.showPanel(matchingChildren[0]);
          }
        }

    }, {
        template: function(json) {
            var data = {item: json.model.originalItem};
            var sTemplate = window.launchpad.DeckContainer(data);
            return sTemplate;
        }
    });
})(b$, gadgets, lp, bd, window, $);
