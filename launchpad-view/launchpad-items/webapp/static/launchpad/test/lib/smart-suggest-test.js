/* global define assertEquals */
require([ "launchpad/lib/ui",
		  "launchpad/support/date" ], function(SmartSuggest) {

	var smartSuggest;
	module("smartsearch", {

		setup: function() {
			smartSuggest = new SmartSuggest();
		}
	});

	test("Date suggestions ", function() {

		var term, suggs, from, to;

		smartSuggest.addSuggester({
			type: SmartSuggest.types.DATE,
			suggest: SmartSuggest.builtIn.getDateSuggestions
		});

		//single dates

		//test "June"
		term = "June";
		suggs = smartSuggest.getSuggestions(term);
		from = new Date(2013, 5, 1);
		to = from.clone().addMonths(1).addMilliseconds(-1);
		assertFalse("Non range suggestion " + term, suggs[0].displayAsRange);
		assertEquals("From date for " + term, fDate(from), fDate(suggs[0].search.from));
		assertEquals("To date for " + term, fDate(to), fDate(suggs[0].search.to));

		to = from.clone().addMonths(1).addMilliseconds(-1);
		assertTrue("Range suggestion " + term, suggs[1].displayAsRange);
		assertEquals("From date for " + term, fDate(from), fDate(suggs[1].search.from));
		assertEquals("To date for " + term, fDate(to), fDate(suggs[1].search.to));

		//test "Yesterday"
		term = "Yesterday";
		suggs = smartSuggest.getSuggestions(term);
		from = new Date.today().addDays(-1);
		to = from.clone().addDays(1).addMilliseconds(-1);
		assertFalse("Non range suggestion " + term, suggs[0].displayAsRange);
		assertEquals("From date for " + term, fDate(from), fDate(suggs[0].search.from));
		assertEquals("To date for " + term, fDate(to), fDate(suggs[0].search.to));

		to = from.clone().addWeeks(1).addMilliseconds(-1);
		assertTrue("Range suggestion" + term, suggs[1].displayAsRange);
		assertEquals("From date for " + term, fDate(from), fDate(suggs[1].search.from));
		assertEquals("To date for " + term, fDate(to), fDate(suggs[1].search.to));

		//test "Yesterday"
		term = "31st August";
		suggs = smartSuggest.getSuggestions(term);
		from = Date.parse("8/31/2013")
		to = from.clone().addDays(1).addMilliseconds(-1);
		assertFalse("Non range suggestion " + term, suggs[0].displayAsRange);
		assertEquals("From date for " + term, fDate(from), fDate(suggs[0].search.from));
		assertEquals("To date for " + term, fDate(to), fDate(suggs[0].search.to));

		to = from.clone().addWeeks(1).addMilliseconds(-1);
		assertTrue("Range suggestion" + term, suggs[1].displayAsRange);
		assertEquals("From date for " + term, fDate(from), fDate(suggs[1].search.from));
		assertEquals("To date for " + term, fDate(to), fDate(suggs[1].search.to));

		//test "2008"
		term = "2008";
		suggs = smartSuggest.getSuggestions(term);
		from = new Date(2008, 0, 1);
		to = from.clone().addYears(1).addMilliseconds(-1);
		assertFalse("Non range suggestion" + term, suggs[0].displayAsRange);
		assertEquals("From date for " + term, fDate(from), fDate(suggs[0].search.from));
		assertEquals("To date for " + term, fDate(to), fDate(suggs[0].search.to));

		assertTrue("Range suggestion " + term, suggs[1].displayAsRange);
		assertEquals("From date for " + term, fDate(from), fDate(suggs[1].search.from));
		assertEquals("To date for " + term, fDate(to), fDate(suggs[1].search.to));

		//user specified date ranges
		term = "July to September";
		suggs = smartSuggest.getSuggestions(term);
		from = Date.parse("July");
		to = Date.parse("October").addMilliseconds(-1);

		assertEquals("Only 1 result", 1, suggs.length);
		assertTrue("Range suggestion" + term, suggs[0].displayAsRange);
		assertEquals("From date for " + term, fDate(from), fDate(suggs[0].search.from));
		assertEquals("To date for " + term, fDate(to), fDate(suggs[0].search.to));

		term = "2010 to 2012";
		suggs = smartSuggest.getSuggestions(term);
		from = new Date(2010, 0, 1);
		to =  new Date(2013, 0, 1).addMilliseconds(-1);

		assertEquals("Only 1 result", 1, suggs.length);
		assertTrue("Range suggestion" + term, suggs[0].displayAsRange);
		assertEquals("From date for " + term, fDate(from), fDate(suggs[0].search.from));
		assertEquals("To date for " + term, fDate(to), fDate(suggs[0].search.to));
	});

	test("Amount suggestions ", function() {

		var term, suggs, from, to;

		smartSuggest.addSuggester({
			type: SmartSuggest.types.AMOUNT,
			suggest: SmartSuggest.builtIn.getAmountSuggestions
		});
		smartSuggest.addSuggester({
			type: SmartSuggest.types.DATE,
			suggest: SmartSuggest.builtIn.getDateSuggestions
		});

		//single amounts
		term = "4.5";
		suggs = smartSuggest.getSuggestions(term);
		assertEquals("2 results", 4, suggs.length);
		assertFalse("Range suggestion" + term, suggs[0].displayAsRange);
		assertEquals("From date for " + term, 4, suggs[0].search.from);
		assertEquals("To date for " + term, 5, suggs[0].search.to);
		assertTrue("Range suggestion" + term, suggs[1].displayAsRange);
		assertEquals("From date for " + term, 4.5, suggs[1].search.from);
		assertEquals("To date for " + term, 7, suggs[1].search.to);

		term = "67.7";
		suggs = smartSuggest.getSuggestions(term);
		assertEquals("2 results", 2, suggs.length);
		assertFalse("Range suggestion" + term, suggs[0].displayAsRange);
		assertEquals("From date for " + term, 64, suggs[0].search.from);
		assertEquals("To date for " + term, 72, suggs[0].search.to);
		assertTrue("Range suggestion" + term, suggs[1].displayAsRange);
		assertEquals("From date for " + term, 67.7, suggs[1].search.from);
		assertEquals("To date for " + term, 102, suggs[1].search.to);

		term = "150 to 400";
		suggs = smartSuggest.getSuggestions(term);
		assertEquals("1 result", 1, suggs.length);
		assertTrue("Range suggestion" + term, suggs[0].displayAsRange);
		assertEquals("From date for " + term, 150, suggs[0].search.from);
		assertEquals("To date for " + term, 400, suggs[0].search.to);

		term = "34";
		suggs = smartSuggest.getSuggestions(term);
		assertEquals("1 result", 2, suggs.length);
	});

	test("Contact suggestions ", function() {

		var term, suggs;

		smartSuggest.addSuggester({
			data: [{
				id: "1",
				name: "Geoff Capes",
				account: 123456789
			}, {
				id: "2",
				name: "Aunt June",
				account: 987654321
			}, {
				id: "3",
				name: "Julian Villers",
				account: 13244657
			}, {
				id: "4",
				name: "Julian Clarey",
				account: 8877665544
			}, {
                id: "5",
                name: "Николай Иванов",
                account: 38645132
            }],
			type: SmartSuggest.types.CONTACT,
			suggest: SmartSuggest.builtIn.getContactSuggestions
		});

		term = "June";
		suggs = smartSuggest.getSuggestions(term);
		assertEquals("Found June", "2", suggs[0].contact.id);

		term = "132";
		suggs = smartSuggest.getSuggestions(term);
		assertEquals("Found June", "3", suggs[0].contact.id);

		term = "987.654-321";
		suggs = smartSuggest.getSuggestions(term);
		assertEquals("Found June", "2", suggs[0].contact.id);

		term = "Jul";
		suggs = smartSuggest.getSuggestions(term);
		assertEquals("Found June", "3", suggs[0].contact.id);
		assertEquals("Found June", "4", suggs[1].contact.id);

        term = "Николай";
        suggs = smartSuggest.getSuggestions(term);
        assertEquals("Found Николай", "5", suggs[0].contact.id);

		smartSuggest.addSuggester({
			type: SmartSuggest.types.DATE,
			suggest: SmartSuggest.builtIn.getDateSuggestions
		});
		term = "Jul";
		suggs = smartSuggest.getSuggestions(term);
		assertEquals("Found June", 4, suggs.length);
	});

	function fDate(date) {
		return date.toString("yyyy-MM-dd HH:mm:ss");
	}
});