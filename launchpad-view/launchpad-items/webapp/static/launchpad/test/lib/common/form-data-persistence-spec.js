define(["angular",
    "launchpad/lib/common",
    "angular-mocks"], function(angular) {

    var testObject = {name: "Dave", location: "Amsterdam", currentDate: new Date(), innerObject: {innerDate: new Date()}};
    describe("Form Data Persistence controller", function() {

        var persistenceManager;
        var formName = "test-form";

        beforeEach(module('common'));
        beforeEach(function() {
            inject(function (formDataPersistence) {
                persistenceManager = formDataPersistence.getInstance();
            });

            sessionStorage.clear(); // Do not allow cache http calls
        });

        it("saves form data to session storage", function() {

            persistenceManager.saveFormData(formName, testObject);

            var recoveredObject = persistenceManager.getFormData(formName);

            expect(recoveredObject.name).toBe("Dave");
            expect(recoveredObject.location).toBe("Amsterdam");
            expect(recoveredObject.innerObject instanceof Object).toBe(true);
        });

        it("saves form data and verifies it exists", function() {
            persistenceManager.saveFormData(formName, testObject);

            var dataSaved = persistenceManager.isFormSaved(formName);

            expect(dataSaved).toBe(true);
        });

        it("saves form data, and asseses the instance of the date fields", function() {
            persistenceManager.saveFormData(formName, testObject);

            var recoveredObject = persistenceManager.getFormData(formName);

            expect(recoveredObject.currentDate instanceof Date).toBe(true);
            expect(recoveredObject.innerObject.innerDate instanceof Date).toBe(true);
        });

        it("saves the form data, verifies that it exists, deletes, and verifies it's deletion", function() {
            persistenceManager.saveFormData(formName, testObject);

            var dataSaved = persistenceManager.isFormSaved(formName);

            expect(dataSaved).toBe(true);

            persistenceManager.removeFormData(formName);

            dataSaved = persistenceManager.isFormSaved(formName);

            expect(dataSaved).toBe(false);
        });




    });


});