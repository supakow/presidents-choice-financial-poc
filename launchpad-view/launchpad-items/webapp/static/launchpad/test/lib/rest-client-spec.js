define(["jquery", "launchpad/support/mustache", "launchpad/lib/common/rest-client",
	"jquery.mockjax" ], function($, Mustache, restClient) {

	window.Mustache = Mustache;

	describe("Launchpad Rest Client", function() {

		describe("is making requests to", function() {

			var name = "my-item";
			var contextItemName = "my-portal"
			var parentItemName = "my-parent";
			var extendedItemName = "my-extended";


			var server = "http://local.com"
			var contextPath = "/ps";
			beforeEach(function() {
				restClient.setDefaultServer(server);
				restClient.setDefaultContextPath(contextPath)
			});

			it("updates a page", function() {
				var req = restClient.makePutPageRequest({
					contextItemName: contextItemName,
					name: name,
					properties: [{
						name : "A",
						type: "string",
						value: "A"
					},{
						name : "B",
						type: "boolean",
						value: false
					}]
				});

				expect(req.url).toBe("http://local.com/ps/portals/my-portal/pages/my-item.xml");
				expect(req.method).toBe(restClient.http.PUT);
				var body = $(req.body);
				console.log(body);
				console.log(body.html());
				expect(body.find("> name").text()).toBe(name);
				expect(body.find("> contextItemName").text()).toBe(contextItemName);

				expect(body.find("> properties > property").eq(0).attr("name")).toBe("A");
				expect(body.find("> properties > property > value").eq(0).attr("type")).toBe("string");
				expect(body.find("> properties > property > value").eq(0).text()).toBe("A");

				expect(body.find("> properties > property").eq(1).attr("name")).toBe("B");
				expect(body.find("> properties > property > value").eq(1).attr("type")).toBe("boolean");
				expect(body.find("> properties > property > value").eq(1).text()).toBe("false");
			});

			it("gets a page", function() {

			});

			it("creates a page", function() {

			});

			it("deletes a page", function() {

			});

			it("updates a container", function() {

			});

			it("gets a container", function() {

			});

			it("creates a container", function() {

			});

			it("deletes a container", function() {

			});

			it("updates a widget", function() {

			});

			it("gets a widget", function() {

			});

			it("creates a widget", function() {

			});

			it("deletes a widget", function() {

			});
		});
	});
});