define([ "angular",
    "launchpad/lib/common",
    "angular-mocks",
    "launchpad/lib/accounts"], function(angular) {

    var testAccounts = [{"availableBalance":-11418,"balance":-10418,"bban":"694251588","currency":"EUR","groupCode":"CREDIT","id":"411b02e8-6a9f-4c23-8ee9-1e0b53fedd48","name":"Business Account","partyId":"3","status":"ENABLED"},{"availableBalance":10456,"balance":10456,"bban":"441511726","currency":"EUR","groupCode":"INVESTMENT","id":"cd9459fb-b970-49a0-b622-da8c618a2de4","name":"Shared Account","partyId":"3","status":"BLOCKED"},{"availableBalance":10283,"balance":10283,"bban":"984165202","currency":"EUR","groupCode":"CASH","id":"5233d7a6-0c16-4b54-87a1-0c7b0e0a3cff","name":"Personal Checking Account","partyId":"3","status":"ENABLED"}];
    var testGroups = [{"code": "CREDIT"}, {"code": "INVESTMENT"}, {"code": "CASH"}];
    describe("Accounts Model", function() {


        var $httpBackend;

        var accountsModel;

        var getFakeAccounts = function() {
            return testAccounts;
        };

        beforeEach(module('accounts'));
        beforeEach(module('common'));

        beforeEach(function() {
            inject(function (_$httpBackend_, AccountsModel, _httpService_) {
                $httpBackend = _$httpBackend_;
                httpService = _httpService_;
                accountsModel = AccountsModel.getInstance({
                    accountsEndpoint: "/my/accounts/end/point",
                    cacheTimeout: 10000,
                    urlVars: {
                        partyId: 3
                    }
                });
            });

            sessionStorage.clear(); // Do not allow cache http calls
        });

        beforeEach(function() {
            $httpBackend.expectGET('/my/accounts/end/point');
            $httpBackend.when('GET', '/my/accounts/end/point').respond(function(method, url, data, headers){
                return [200, testAccounts];
            });

            //expect(accountsModel.accounts.length).toBe(0);
            accountsModel.load();

            $httpBackend.flush();
        });

        it("loads accounts from the server", function() {

            expect(accountsModel.accounts.length).toBe(3);
        });

        it("loads accounts from the server and finds one by ID", function() {

            expect(accountsModel.accounts.length).toBe(3);

            var account = accountsModel.findById("411b02e8-6a9f-4c23-8ee9-1e0b53fedd48");

            //Check to see if the function has returned the correct account
            expect(account.availableBalance).toBe(-11418);
            expect(account.balance).toBe(-10418);
            expect(account.bban).toBe('694251588');
            expect(account.name).toBe('Business Account');
            expect(account.partyId).toBe('3');
            expect(account.status).toBe('ENABLED');
        });

        it("loads accounts from the server and finds one by BBAN", function() {

            expect(accountsModel.accounts.length).toBe(3);

            var account = accountsModel.findByAccountNumber("441511726");

            //Check to see if the function has returned the correct account
            expect(account.availableBalance).toBe(10456);
            expect(account.balance).toBe(10456);
            expect(account.id).toBe("cd9459fb-b970-49a0-b622-da8c618a2de4");
            expect(account.name).toBe('Shared Account');
            expect(account.partyId).toBe('3');
            expect(account.status).toBe('BLOCKED');
        });

        it("loads accounts from the server, finds an account by ID and get's it's pending balance", function() {

            expect(accountsModel.accounts.length).toBe(3);

            var account = accountsModel.findById("411b02e8-6a9f-4c23-8ee9-1e0b53fedd48");

            expect(accountsModel.getPending(account)).toBe(1000);
        });

        it("loads accounts from the server, confirms their balance on the previousBalances object", function() {

            expect(accountsModel.accounts.length).toBe(3);

            for(var i = 0; i < accountsModel.accounts.length; i++) {
                expect(accountsModel.previousBalances[accountsModel.accounts[i].id]).toBe(accountsModel.accounts[i].availableBalance);
            }

        });

        it("loads accounts from the server, reloads with updated balances, and checks the account delta", function() {

            expect(accountsModel.accounts.length).toBe(3);

            //on intial load, deltas should all be zero
            expect(accountsModel.accounts[0].delta).toBe(0);
            expect(accountsModel.accounts[1].delta).toBe(0);
            expect(accountsModel.accounts[2].delta).toBe(0);

            testAccounts[0].availableBalance = testAccounts[0].availableBalance - 100;
            testAccounts[1].availableBalance = testAccounts[1].availableBalance + 100;

            accountsModel.refreshAccounts(testAccounts);


            expect(accountsModel.accounts[0].delta).toBe(-1);
            expect(accountsModel.accounts[1].delta).toBe(1);
            expect(accountsModel.accounts[2].delta).toBe(0);

        });


    });


});