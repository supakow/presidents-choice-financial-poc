define([ "angular",
    "launchpad/lib/common",
    "angular-mocks",
    "launchpad/lib/accounts"], function(angular) {

    var testChartAccounts = [{"date":1392245999768,"amount":10283},{"date":1392332399768,"amount":10283},{"date":1392418799768,"amount":10283},{"date":1392505199768,"amount":10283},{"date":1392591599768,"amount":10283},{"date":1392677999768,"amount":10283},{"date":1392764399768,"amount":10283}]

    describe("Accounts Charts Model", function() {


        var $httpBackend;

        var accountsChartModel;

        var getFakeChartAccounts = function() {
            return testChartAccounts;
        };

        beforeEach(module('accounts'));
        beforeEach(module('common'));

        beforeEach(function() {
            inject(function (_$httpBackend_, AccountsChartModel, _httpService_) {
                $httpBackend = _$httpBackend_;
                httpService = _httpService_;
                accountsChartModel =  new AccountsChartModel.getInstance({
                    accountsChartEndpoint: "/my/chart/accounts/end/point",
                    accountId: 3
                });
            });

            sessionStorage.clear(); // Do not allow cache http calls
        });

        it("loads chart accounts from the server", function() {
            var params = {
                end: 1392730479768,
                start: 1392125679768
            };

            $httpBackend.expectGET('/my/chart/accounts/end/point?end=1392730479768&start=1392125679768');
            $httpBackend.when('GET', '/my/chart/accounts/end/point?end=1392730479768&start=1392125679768').respond(function(method, url, data, headers){
                return [200, testChartAccounts];
            });

            accountsChartModel.load(params);

            $httpBackend.flush();

            expect(accountsChartModel.chartData.length).toBe(7);
        });




    });
});