define([ 'angular',
	'jquery',
	'angular-mocks',
	'launchpad/lib/ui'], function(angular, $) {

	describe('Nav Icon Directive', function() {


		var scope, $template, $element;

		var iconNames = [
			'vertical-ellipsis',
			'large-hamburger',
			'small-hamburger',
			'arrow-left'
		];

		beforeEach(module("ui"));

		beforeEach(inject(function(_$compile_, _$rootScope_) {

			scope = _$rootScope_;

			//Add the first item in the list as the specified icon
			scope.navIcon = iconNames[0];

			//remove that icon from the list to the icons are cycled through
			iconNames.splice(0, 1);

			//Nav-icon is the attribute to add to invoke the directive, and it takes the variable name of the icon as an icon attribute
			$template = _$compile_('<span nav-icon="nav-icon" icon="navIcon"></span>')(scope);

			$element = $template;

			scope.$digest();

		}));

		it("Asserts mark up for vertical ellipsis", function() {

			var span = angular.element($element.find("span")[0]);
			expect(span.hasClass("glyphicon-sort-by-attributes")).toBe(true);
		});

		it("Asserts markup for large hamburger", function() {

			var spans = angular.element($element.find("span"));

			for(var i = 0; i < spans.length; i++) {
				var span = angular.element(spans[i]);
				expect(span.hasClass("icon-bar")).toBe(true);
			}
		});

		it("Asserts markup for small hamburger", function() {

			var spans = angular.element($element.find("span"));

			for(var i = 0; i < spans.length; i++) {
				var span = angular.element(spans[i]);
				expect(span.hasClass("glyphicon-minus")).toBe(true);
			}
		});

		it("Asserts mark up for left arrow icon", function() {

			var span = angular.element($element.find("span")[0]);
			expect(span.hasClass("glyphicon-chevron-left")).toBe(true);
		});
	});
});
