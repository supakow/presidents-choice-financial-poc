define([ 'angular',
    'jquery',
    'angular-mocks',
    'launchpad/lib/payments',
    'launchpad/lib/common',
    'launchpad/lib/payments/iban-input'
], function(angular, $) {

    var testCountryList = [{"country_code":"NL","country_name":"The Netherlands","iban_length":18,"iban_regex":"^NL\\d{2}[A-Za-z]{4}\\d{10}$"},{"country_code":"HU","country_name":"Hungary","iban_length":28,"iban_regex":"^HU\\d{26}$"},{"country_code":"IE","country_name":"Ireland","iban_length":22,"iban_regex":"^IE\\d{2}[A-Za-z]{4}\\d{14}$"},{"country_code":"DE","country_name":"Germany","iban_length":22,"iban_regex":"^DE\\d{20}$"},{"country_code":"GR","country_name":"Greece","iban_length":27,"iban_regex":"^GR\\d{25}$"},{"country_code":"GB","country_name":"United Kingdom","iban_length":22,"iban_regex":"^GB\\d{2}[A-Za-z]{4}\\d{14}$"}];
    var testIban = ['NL39RABO0300065264', 'HU42117730161111101800000000', 'DE89370400440532013000', 'ES8023100001180000012345', 'IE29AIBK93115212345678', 'GB29RBOS60161331926819'];

	describe('IbanInput directive', function() {

		var scope, $template;

		beforeEach(module('payments'));
        beforeEach(module('common'));

        beforeEach(inject(function(_$compile_, _$rootScope_, IbanModel) {

            scope = _$rootScope_;

            var ibanModel = IbanModel.getInstance({
                countryListEndpoint: "/my/iban/countryList/endpoint",
                enableCountrySearch: true
            });

            scope.ibanModel = ibanModel;

            spyOn(ibanModel, "loadCountryList").andCallFake(function() {
                scope.ibanModel.countryList = testCountryList;
            });
            scope.ibanModel.loadCountryList();
        }));

        it('loads country list from the server', function() {

            expect(scope.ibanModel.countryList.length).toBe(6);
        });

        it('extracts the country code from an IBAN', function() {
            
            // check a dutch IBAN
            scope.ibanModel.value = testIban[0];
            expect(scope.ibanModel.getCountryCode()).toBe('NL');

            // check an irish IBAN
            scope.ibanModel.value = testIban[4];
            expect(scope.ibanModel.getCountryCode()).toBe('IE');

            // check a spanish IBAN (not in the country list)
            scope.ibanModel.value = testIban[3];
            expect(scope.ibanModel.getCountryCode()).toBe(null);

        });

        it('validates IBAN', function() {

            // country code in list, valid IBAN
            scope.ibanModel.value = testIban[0];
            expect(scope.ibanModel.getCountryCode()).not.toBe(null);
            expect(scope.ibanModel.validate()).toBe(true);

            // country code in list, invalid IBAN
            scope.ibanModel.value = testIban[5];
            expect(scope.ibanModel.getCountryCode()).not.toBe(null);
            expect(scope.ibanModel.validate()).toBe(false);
        });

	});

});