define([ "angular",
    "jquery",
    "angular-mocks",
    "launchpad/lib/payments"], function(angular, $) {

    describe("Email transfer directive", function() {


        var scope, $template, $element, $emailInput;

        beforeEach(module("payments"));

        beforeEach(inject(function(_$compile_, _$rootScope_) {

            scope = _$rootScope_;

            var counterpartyEmail = "";

            scope.counterpartyEmail = counterpartyEmail;

            scope.paymentOrder = {
                transferType: "P2P_EMAIL"
            };

            $template = _$compile_('<form><div email-transfer="email-transfer" ng-model="counterpartyEmail" name="counterpartyEmail"  lp-transfer-type="paymentOrder.transferType"></div></form>')(scope);

            $emailInput = angular.element($template.find("input")[0]);

            $element = $template;

            scope.$digest();

        }));

        var setEmailInputValue = function(value) {
            $emailInput.val(value);
            $emailInput.triggerHandler('change');
            scope.$digest();
        };


        it("Asserts default values", function() {

            expect($emailInput.val()).toBe("");
            expect($emailInput.parent().hasClass("has-feedback")).toBe(false);
        });

        it("Sets the values for email input with invalid email and checks the value and validation", function() {

            setEmailInputValue("david@backbase");

            expect($emailInput.val()).toBe("david@backbase");

            expect($emailInput.parent().hasClass("has-feedback")).toBe(true);
            expect($emailInput.parent().hasClass("has-error")).toBe(true);

            expect(scope.counterpartyEmail).toBe("david@backbase");
        });

        it("Sets the values for email input with invalid email and clicks the red X to clear the field", function() {

            setEmailInputValue("david@backbase");

            expect($emailInput.val()).toBe("david@backbase");
            expect(scope.counterpartyEmail).toBe("david@backbase");

            var $redX = angular.element($emailInput.parent().find("span")[0]);
            $redX.triggerHandler("click");
            scope.$digest();

            expect($emailInput.val()).toBe("");
            expect(scope.counterpartyEmail).toBe("");

        });

        it("Checks the validation of a valid email", function() {

            setEmailInputValue("david@backbase.com");

            expect($emailInput.val()).toBe("david@backbase.com");
            expect(scope.counterpartyEmail).toBe("david@backbase.com");

            expect($emailInput.parent().hasClass("has-feedback")).toBe(true);
            expect($emailInput.parent().hasClass("has-error")).toBe(false);
            expect($emailInput.parent().hasClass("has-success")).toBe(true);

        });

    });


});
