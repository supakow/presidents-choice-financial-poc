define([
	"jquery",
	"angular",
    "angular-mocks",
    "launchpad/lib/payments"], function($, angular) {


        describe("lpInputCurrency directive", function() {

            beforeEach(module('payments'));

            var scope, $template, $element, order;

            beforeEach(inject(function (_$compile_, _$rootScope_) {
                order = {};
                scope = _$rootScope_;
                $template = _$compile_('<form name="orderForm"><input name="amount" lp-input-currency="" ng-model="order.amount"/></form>')(scope);
                $element = $template.find('input');
                scope.$digest();
            }));

            var setValue = function(value) {
                $element.val(value);
                $element.triggerHandler('input');
                scope.$digest();
            };

            it("Should allow only digits", function(){
                setValue("20$");
                expect(scope.orderForm.amount.$error.lpCurrencyFormat).toEqual(true);
            });

            it("Shouldn't allow zero", function(){
                setValue("0.00");
                expect(scope.orderForm.amount.$error.lpCurrencyNonZero).toEqual(true);
            });

            it("Shouldn't allow more than 15 digits", function(){
                setValue("1234567890123,45");
                expect(scope.orderForm.amount.$error.lpCurrencyMaxLength).toEqual(true);
            });

            it("Should allow formatted values", function(){
                setValue("14.50");
                expect(scope.orderForm.amount.$valid).toEqual(true);
            });

            it("Should allow small values", function(){
                setValue("0.50");
                expect(scope.orderForm.amount.$valid).toEqual(true);
            });

            it("Should replace commas with dots", function(){
                setValue("14,50");
                expect(scope.order.amount).toEqual("14.50");
            });

        });
});