define([ "angular",
    "jquery",
    "angular-mocks",
    "launchpad/lib/payments"], function(angular, $) {

    describe("Payment ref/desc directive", function() {


        var scope, $template, $element, $input, $textarea;

        beforeEach(module("payments"));

        beforeEach(inject(function(_$compile_, _$rootScope_) {

            scope = _$rootScope_;

            var paymentOrder = {
                paymentReference: "",
                paymentDescription: ""
            };

            scope.paymentOrder = paymentOrder;


            $template = _$compile_('<form><div lp-payment-ref-description="lp-payment-ref-description" ng-model="paymentOrder"></div></form>')(scope);

            $input = $template.find("input");
            $textarea = $template.find("textarea");

            $element = $template;

            scope.$digest();

        }));


        var setInputValue = function(value) {

            $input.val(value);
            $input.triggerHandler('change');
            scope.$digest();
        };

        var setTextAreaValue = function(value) {

            $textarea.val(value);
            $textarea.triggerHandler('change');
            scope.$digest();
        };


        it("Asserts default values", function() {
            expect(scope.paymentOrder.paymentReference).toBe("");
            expect(scope.paymentOrder.paymentDescription).toBe("");
            expect($input.parent().hasClass("has-feedback")).toBe(false);
        });

        it("Sets the value of the payment ref to 45678 and assesses validation and disabling", function() {

            setInputValue("45678");

            expect(scope.paymentOrder.paymentReference).toBe("RF45678");
            expect(scope.paymentOrder.paymentDescription).toBe("");
            expect($input.val()).toBe("RF45 678");

            expect($input.parent().parent().hasClass("has-feedback")).toBe(true);
            expect($input.parent().parent().hasClass("has-error")).toBe(true);

            expect($textarea.attr("disabled")).toBe("disabled");
            expect($input.attr("disabled")).toBe(undefined);
        });

        it("Sets the value of the payment ref to RF45G72UUR (valid pr) and assesses validation and disabling", function() {

            setInputValue("RF45G72UUR");

            expect(scope.paymentOrder.paymentReference).toBe("RF45G72UUR");
            expect(scope.paymentOrder.paymentDescription).toBe("");
            expect($input.val()).toBe("RF45 G72U UR");

            expect($input.parent().parent().hasClass("has-feedback")).toBe(true);
            expect($input.parent().parent().hasClass("has-success")).toBe(true);

            expect($textarea.attr("disabled")).toBe("disabled");
            expect($input.attr("disabled")).toBe(undefined);
        });

        it("Sets the value of the payment description to be Hello World and assesses validation and disabling", function() {

            setTextAreaValue("Hello World");

            expect(scope.paymentOrder.paymentReference).toBe("");
            expect(scope.paymentOrder.paymentDescription).toBe("Hello World");
            expect($textarea.val()).toBe("Hello World");

            expect($input.parent().parent().hasClass("has-feedback")).toBe(false);

            expect($input.attr("disabled")).toBe("disabled");
            expect($textarea.attr("disabled")).toBe(undefined);
        });

        it("Switches between payment reference and payment description", function() {

            setInputValue("RF45G72UUR");

            expect(scope.paymentOrder.paymentReference).toBe("RF45G72UUR");
            expect(scope.paymentOrder.paymentDescription).toBe("");
            expect($input.val()).toBe("RF45 G72U UR");

            expect($input.parent().parent().hasClass("has-feedback")).toBe(true);
            expect($input.parent().parent().hasClass("has-success")).toBe(true);

            expect($textarea.attr("disabled")).toBe("disabled");
            expect($input.attr("disabled")).toBe(undefined);

            setInputValue("");
            expect(scope.paymentOrder.paymentReference).toBe("");
            expect(scope.paymentOrder.paymentDescription).toBe("");
            expect($input.val()).toBe("");

            expect($textarea.attr("disabled")).toBe(undefined);
            expect($input.attr("disabled")).toBe(undefined);

            setTextAreaValue("Hello World");

            expect(scope.paymentOrder.paymentReference).toBe("");
            expect(scope.paymentOrder.paymentDescription).toBe("Hello World");
            expect($textarea.val()).toBe("Hello World");

            expect($input.parent().parent().hasClass("has-feedback")).toBe(false);

            expect($input.attr("disabled")).toBe("disabled");
            expect($textarea.attr("disabled")).toBe(undefined);

        });

    });


});
