define([ "angular",
    "jquery",
    "angular-mocks",
    "launchpad/lib/payments/payments-module",
    "launchpad/lib/transactions",
    "launchpad/lib/common"], function(angular, $) {

    var testCurrencies = {"preferred":[{"currency_code":"USD","exchange_rate":1.38},{"currency_code":"GBP","exchange_rate":0.82}],"rest":[{"currency_code":"HUF","exchange_rate":310.11},{"currency_code":"PLN","exchange_rate":4.22},{"currency_code":"CHF","exchange_rate":1.22}]};

    describe("LP-Balance-Update directive", function() {


        var scope, $template, $element, $inputs, $divs, $spans, $amountInput, $decimalAmountInput, $domesticCurrencyMessage, $currencySelect, currencyModel, paymentOrder, $httpBackend;



        beforeEach(module('transactions'));
        beforeEach(module('payments'));
        beforeEach(module('common'));

        beforeEach(inject(function(_$compile_, _$rootScope_, CurrencyModel, _$httpBackend_) {

            scope = _$rootScope_;
            $httpBackend = _$httpBackend_;

            var paymentOrder = {
                instructedCurrency: 'EUR',
                instructedAmount: ''
            };

            scope.paymentOrder = paymentOrder;

            var currencyModel = CurrencyModel.getInstance({
                defaultCurrencyEndpoint: "/default/currency/endpoint",
                currencyListEndpoint: "/other/currency/endpoint",
                disableExtraCurrencies: false
            });

            currencyModel.configureDefaultCurrency({
                    currency_code: "EUR"
            });

            scope.currencyModel = currencyModel;

            spyOn(currencyModel, "loadOtherCurrencies").andCallFake(function() {
                scope.currencyModel.sortCurrencies(testCurrencies);
            });

            scope.currencyModel.loadOtherCurrencies();

            scope.disableCurrencySelection = false;

            $template = _$compile_("<lp-currency-amount-input ng-model='paymentOrder.instructedAmount' lp-currency-list='currencyModel' lp-payment-order='paymentOrder' lp-disable-currency-selection='disableCurrencySelection'></lp-currency-amount-input>")(scope);

            $inputs = $template.find("input");
            $amountInput = angular.element($inputs["0"]);
            $decimalAmountInput = angular.element($inputs["1"]);

            $spans = $template.find("span");
            $divs = $template.find("div");

            $domesticCurrencyMessage = angular.element($spans["1"]);
            $currencySelect = angular.element($divs["1"]);

            $element = $template;

            scope.$digest();

        }));


        var setAmountValue = function(value) {

            $amountInput.val(value);
            $amountInput.triggerHandler('change');
            scope.$digest();
        };

        var setDecimalAmountValue = function(value) {

            $decimalAmountInput.val(value);
            $decimalAmountInput.triggerHandler('change');
            scope.$digest();
        };

        var setSelectedCurrency = function(value) {

            scope.currencyModel.selectCurrency(value);
            scope.$digest();
        };

        it("Sets the whole amount to 100", function() {

            expect(scope.currencyModel.orderedCurrencies.length).toBe(6);

            setAmountValue("100");

            expect(scope.currencyModel.selected.currency_code).toBe("EUR");
            expect($amountInput.val()).toBe("100");

            expect(scope.paymentOrder.instructedAmount).toBe("100.00");
            expect(scope.paymentOrder.instructedCurrency).toBe("EUR");

            //expect($domesticCurrencyMessage.text()).toBe("This transfer is 100.00 in EUR");
        });

        it("Sets the whole amount to 100.75", function() {

            setAmountValue("100");
            setDecimalAmountValue("75");

            expect($amountInput.val()).toBe("100");
            expect($decimalAmountInput.val()).toBe("75");

            expect(scope.paymentOrder.instructedAmount).toBe("100.75");
            expect(scope.paymentOrder.instructedCurrency).toBe("EUR");

            //expect($domesticCurrencyMessage.text()).toBe("This transfer is 100.75 in EUR");
        });

        it("Sets the whole amount to 100.75 and changes the currency", function() {

            setAmountValue("100");
            setDecimalAmountValue("75");

            //expect($domesticCurrencyMessage.text()).toBe("This transfer is 100.75 in EUR");

            setSelectedCurrency("GBP");

            expect(scope.currencyModel.selected.currency_code).toBe("GBP");

            expect(scope.paymentOrder.instructedAmount).toBe("100.75");
            expect(scope.paymentOrder.instructedCurrency).toBe("GBP");

            expect($domesticCurrencyMessage.text()).toBe("This transfer is 122.87 in EUR");
        });

        xit("Sets the whole amount to 2000.50, changes to low value currency and changes input length", function(){

            setAmountValue("2000");
            setDecimalAmountValue("50");

            expect($domesticCurrencyMessage.text()).toBe("This transfer is 2000.50 in EUR");
            expect($amountInput.attr("maxlength")).toBe("6");

            setSelectedCurrency("HUF");

            expect(scope.currencyModel.selected.currency_code).toBe("HUF");

            expect(scope.paymentOrder.instructedAmount).toBe("2000.50");
            expect(scope.paymentOrder.instructedCurrency).toBe("HUF");

            expect($domesticCurrencyMessage.text()).toBe("This transfer is 6.45 in EUR");
            expect($amountInput.attr("maxlength")).toBe("8");

            setSelectedCurrency("GBP");
            setAmountValue("1000");

            expect(scope.currencyModel.selected.currency_code).toBe("GBP");

            expect(scope.paymentOrder.instructedAmount).toBe("1000.50");
            expect(scope.paymentOrder.instructedCurrency).toBe("GBP");

            expect($domesticCurrencyMessage.text()).toBe("This transfer is 1220.12 in EUR");
            expect($amountInput.attr("maxlength")).toBe("6");

        });

    });


});
