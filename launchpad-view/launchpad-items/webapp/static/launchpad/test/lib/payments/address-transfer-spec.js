define([ "angular",
    "jquery",
    "angular-mocks",
    "launchpad/lib/payments"], function(angular, $) {

    describe("Address transfer directive", function() {


        var scope, $template, $element, $addressOneInput, $addressTwoInput, $cityInput, $stateInput, $zipInput;

        beforeEach(module("payments"));

        beforeEach(inject(function(_$compile_, _$rootScope_) {

            scope = _$rootScope_;

            var counterpartyAddress = "";

            scope.counterpartyAddress = counterpartyAddress;

            scope.paymentOrder = {
                transferType: "P2P_ADDRESS"
            };

            $template = _$compile_('<form><div address-transfer="address-transfer" ng-model="counterpartyAddress" name="counterpartyAddress"  lp-transfer-type="paymentOrder.transferType"></div></form>')(scope);

            $addressOneInput = angular.element($template.find("input")[0]);
            $addressTwoInput = angular.element($template.find("input")[1]);
            $cityInput = angular.element($template.find("input")[2]);
            $stateInput = angular.element($template.find("input")[3]);
            $zipInput = angular.element($template.find("input")[4]);

            $element = $template;

            scope.$digest();

        }));

        var setInputValue = function(input, value) {
            input.val(value);
            input.triggerHandler("change");
            scope.$digest();
        }


        it("Asserts default values", function() {

            expect($addressOneInput.val()).toBe("");
            expect($addressTwoInput.val()).toBe("");
            expect($cityInput.val()).toBe("");
            expect($stateInput.val()).toBe("");
            expect($zipInput.val()).toBe("");

            expect($zipInput.parent().hasClass("has-feedback")).toBe(false);

        });

        it("Sets the value of a full address and asserts the counterpartyAddress", function () {

            setInputValue($addressOneInput, "25");
            setInputValue($addressTwoInput, "Brosetown Avenue");
            setInputValue($cityInput, "Tullock Village");
            setInputValue($stateInput, "Clonsilla");
            setInputValue($zipInput, "24");

            expect(scope.counterpartyAddress).toBe("25, Brosetown Avenue, Tullock Village, Clonsilla, 24");
            expect($zipInput.parent().hasClass("has-feedback")).toBe(true);
            expect($zipInput.parent().hasClass("has-error")).toBe(true);

        });

        it("Sets the value of the zip and asserts it's validation", function() {

            //needs to be five digits long
            setInputValue($zipInput, "24");
            expect($zipInput.parent().hasClass("has-feedback")).toBe(true);
            expect($zipInput.parent().hasClass("has-error")).toBe(true);

            setInputValue($zipInput, "24242");
            expect($zipInput.parent().hasClass("has-feedback")).toBe(true);
            expect($zipInput.parent().hasClass("has-success")).toBe(true);

            //needs to be five digits plus four, seperated by hyphen
            setInputValue($zipInput, "24242-242");
            expect($zipInput.parent().hasClass("has-feedback")).toBe(true);
            expect($zipInput.parent().hasClass("has-error")).toBe(true);

            setInputValue($zipInput, "24242-2422");
            expect($zipInput.parent().hasClass("has-feedback")).toBe(true);
            expect($zipInput.parent().hasClass("has-success")).toBe(true);
        });

    });


});
