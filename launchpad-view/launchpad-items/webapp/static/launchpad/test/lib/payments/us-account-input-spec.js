define([ "angular",
    "jquery",
    "angular-mocks",
    "launchpad/lib/payments"], function(angular, $) {

    describe("US account input directive", function() {


        var scope, $template, $element, $accountInput, $routingInput;

        beforeEach(module("payments"));

        beforeEach(inject(function(_$compile_, _$rootScope_) {

            scope = _$rootScope_;

            var counterpartyAccount = "";

            scope.counterpartyAccount = counterpartyAccount;

            $template = _$compile_('<form><div us-account-input="us-account-input" ng-model="counterpartyAccount"></div></form>')(scope);

            $accountInput = angular.element($template.find("input")[1]);
            $routingInput = angular.element($template.find("input")[0]);

            $accountContainer = $accountInput.parent().parent();
            $routingContainer = $routingInput.parent();

            $element = $template;

            scope.$digest();

        }));


        var setAccountInputValue = function(value) {

            $accountInput.val(value);
            $accountInput.triggerHandler('change');
            scope.$digest();
        };

        var setRoutingInputValue = function(value) {

            $routingInput.val(value);
            $routingInput.triggerHandler('change');
            scope.$digest();
        };


        it("Asserts default values", function() {

            expect($accountInput.val()).toBe("");
            expect($routingInput.val()).toBe("");
            expect($accountContainer.hasClass("has-feedback")).toBe(false);
            expect($routingContainer.hasClass("has-feedback")).toBe(false);
        });

        it("Sets the values for account number and routing number and inspects the counterpartyAccount value", function() {

            setAccountInputValue("123456");
            setRoutingInputValue("112233445")

            expect($accountInput.val()).toBe("1234 56");
            expect($routingInput.val()).toBe("112233445");

            expect($accountContainer.hasClass("has-feedback")).toBe(true);
            expect($routingContainer.hasClass("has-feedback")).toBe(true);

            expect(scope.counterpartyAccount).toBe("112233445 123456");
        });

        it("Checks for correct validation of routing number", function() {

            var validRoutingNumber = "111000025";
            var invalidRoutingNumber = "110000025";

            setRoutingInputValue(invalidRoutingNumber);

            expect($routingInput.val()).toBe(invalidRoutingNumber);
            expect($routingContainer.hasClass("has-feedback")).toBe(true);
            expect($routingContainer.hasClass("has-error")).toBe(true);

            setRoutingInputValue(validRoutingNumber);

            expect($routingInput.val()).toBe(validRoutingNumber);
            expect($routingContainer.hasClass("has-feedback")).toBe(true);
            expect($routingContainer.hasClass("has-error")).toBe(false);
            expect($routingContainer.hasClass("has-success")).toBe(true);

            expect(scope.counterpartyAccount).toBe(validRoutingNumber + " ");

        });

        it("Checks for correct validation of account number", function() {

            var accountNumber = "123456789012";

            setAccountInputValue(accountNumber);

            expect($accountInput.val()).toBe("1234 5678 9012");
            expect($accountContainer.hasClass("has-feedback")).toBe(true);
            expect($accountContainer.hasClass("has-warning")).toBe(true);

            var $warningIcon = angular.element($accountContainer.find("span")[3]);
            $warningIcon.triggerHandler("click");
            scope.$digest();

            expect($accountContainer.hasClass("has-feedback")).toBe(true);
            expect($accountContainer.hasClass("has-warning")).toBe(false);
            expect($accountContainer.hasClass("has-success")).toBe(true);

            expect(scope.counterpartyAccount).toBe(" " + accountNumber);

        });

        it("Checks the clearing functionality of the routing input", function() {

            setRoutingInputValue("123456");

            expect($routingInput.val()).toBe("123456");
            expect($routingContainer.hasClass("has-feedback")).toBe(true);
            expect($routingContainer.hasClass("has-error")).toBe(true);

            expect(scope.counterpartyAccount).toBe("123456 ");


            $errorIcon = angular.element($routingContainer.find("span")[0]);
            $errorIcon.triggerHandler("click");
            scope.$digest();

            expect($routingInput.val()).toBe("");
            expect($routingContainer.hasClass("has-feedback")).toBe(false);

            expect(scope.counterpartyAccount).toBe(" ");


        });

    });


});
