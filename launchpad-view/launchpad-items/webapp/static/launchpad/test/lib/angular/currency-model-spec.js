/**
 * Created by david on 3/7/14.
 */
define([ "angular",
    "angular-mocks",
    "launchpad/lib/transactions",
    "launchpad/lib/common"
    ], function() {

    var testDefaultCurrency = {"currency_code": "EUR"};
    var testCurrencies = {"preferred": [{"currency_code": "USD","exchange_rate": "0.72"},{ "currency_code": "GBP", "exchange_rate": "1.20"}], "rest": [{ "currency_code": "HUF","exchange_rate": "0.00323"},{ "currency_code": "RON","exchange_rate": "4.5"},{"currency_code": "CHF","exchange_rate": "1.22"}]};

    describe("Currency Model", function() {


        var $httpBackend;

        var currencyModel;

        beforeEach(module('transactions'));
        beforeEach(module('common'));

        beforeEach(function() {
            inject(function (_$httpBackend_, CurrencyModel, _httpService_) {
                $httpBackend = _$httpBackend_;
                httpService = _httpService_;
                currencyModel = CurrencyModel.getInstance({
                    defaultCurrencyEndpoint: "/default/currency/endpoint",
                    currencyListEndpoint: "/currency/list/endpoint",
                    disableExtraCurrencies: false
                });
            });

            sessionStorage.clear(); // Do not allow cache http calls
        });

        describe("Default Currency functionality", function() {
            it("loads the default currency from the server", function() {
                $httpBackend.expectGET('/default/currency/endpoint');
                $httpBackend.when('GET', '/default/currency/endpoint').respond(function(method, url, data, headers){
                    return [200, testDefaultCurrency];
                });

                spyOn(currencyModel, "loadDefaultCurrency").andCallThrough();

                var a = currencyModel.loadDefaultCurrency();

                a.success(function(data) {
                    currencyModel.configureDefaultCurrency(data);
                })

                $httpBackend.flush();


                expect(currencyModel.loadDefaultCurrency).toHaveBeenCalled();
                expect(currencyModel.orderedCurrencies.length).toBe(1);
            });
        });

        describe("Other functionality", function() {

            beforeEach(function() {
                $httpBackend.expectGET('/default/currency/endpoint');
                $httpBackend.when('GET', '/default/currency/endpoint').respond(function(method, url, data, headers){
                    return [200, testDefaultCurrency];
                });

                spyOn(currencyModel, "loadDefaultCurrency").andCallThrough();

                var a = currencyModel.loadDefaultCurrency();

                a.success(function(data) {
                    currencyModel.configureDefaultCurrency(data);
                })

                $httpBackend.flush();


                expect(currencyModel.loadDefaultCurrency).toHaveBeenCalled();
                expect(currencyModel.orderedCurrencies.length).toBe(1);
            });

            it("loads the rest of the currencies using the default currency as a parameter", function() {

                spyOn(currencyModel, "sortCurrencies").andCallThrough();

                $httpBackend.expectGET('/currency/list/endpoint?currency=' + currencyModel.defaultCurrency.currency_code);
                $httpBackend.when('GET', '/currency/list/endpoint?currency=' + currencyModel.defaultCurrency.currency_code).respond(function(method, url, data, headers){
                    return [200, testCurrencies];
                });

                currencyModel.loadOtherCurrencies();

                $httpBackend.flush();

                //confirming order of currencies
                expect(currencyModel.orderedCurrencies[0].currency_code).toBe("EUR");
                expect(currencyModel.orderedCurrencies[2].currency_code).toBe("GBP");
                expect(currencyModel.orderedCurrencies[3].currency_code).toBe("CHF");

                expect(currencyModel.sortCurrencies).toHaveBeenCalled();
                expect(currencyModel.orderedCurrencies.length).toBe(6)

            });



            it("loads currencies and finds a currency by currency code", function() {

                $httpBackend.expectGET('/currency/list/endpoint?currency=' + currencyModel.defaultCurrency.currency_code);
                $httpBackend.when('GET', '/currency/list/endpoint?currency=' + currencyModel.defaultCurrency.currency_code).respond(function(method, url, data, headers){
                    return [200, testCurrencies];
                });

                currencyModel.loadOtherCurrencies();

                $httpBackend.flush();

                var currency = currencyModel.findCurrency("USD");

                expect(currency.currency_code).toBe("USD");
                expect(currency.exchange_rate).toBe("0.72")
            });

            it("loads currencies and selects a currency", function() {

                $httpBackend.expectGET('/currency/list/endpoint?currency=' + currencyModel.defaultCurrency.currency_code);
                $httpBackend.when('GET', '/currency/list/endpoint?currency=' + currencyModel.defaultCurrency.currency_code).respond(function(method, url, data, headers){
                    return [200, testCurrencies];
                });

                currencyModel.loadOtherCurrencies();

                $httpBackend.flush();

                var currency = currencyModel.selectCurrency("GBP");

                expect(currencyModel.selected.currency_code).toBe("GBP");
                expect(currencyModel.selected.exchange_rate).toBe("1.20")
            });
        });
    });


});