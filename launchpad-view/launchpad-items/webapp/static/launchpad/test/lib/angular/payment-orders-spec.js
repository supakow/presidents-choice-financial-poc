define([ "angular",
    "launchpad/lib/common",
    "angular-mocks",
    "launchpad/lib/payments"], function(angular) {

    var testPaymentOrders = [{"@id":"411b02e8-6a9f-4c23-8ee9-1e0b53fedd48","paymentOrders":[{"accountId":"411b02e8-6a9f-4c23-8ee9-1e0b53fedd48","counterpartyAccount":"984165202","counterpartyName":"Personal Checking Account","creditorReference":[],"id":"a2da2cc2-f2a5-48bd-ef8b-521dd43b4c2d","initiationDateTime":1.392720818604E12,"instructedAmount":300,"instructedCurrency":"EUR","paymentCategory":[],"remittanceInformation":[],"schedule":null,"status":"Received"},{"accountId":"411b02e8-6a9f-4c23-8ee9-1e0b53fedd48","counterpartyAccount":"984165202","counterpartyName":"Personal Checking Account","creditorReference":[],"id":"2bbe7af6-2cba-4518-a942-da9d889bba40","initiationDateTime":1.392720762664E12,"instructedAmount":5000,"instructedCurrency":"EUR","paymentCategory":[],"remittanceInformation":[],"schedule":null,"status":"Received"},{"accountId":"411b02e8-6a9f-4c23-8ee9-1e0b53fedd48","counterpartyAccount":"984165202","counterpartyName":"Personal Checking Account","creditorReference":[],"id":"6f084df4-62b3-4441-8c31-4326d89dc478","initiationDateTime":1.392720728571E12,"instructedAmount":500,"instructedCurrency":"EUR","paymentCategory":[],"remittanceInformation":[],"schedule":null,"status":"Received"}]},{"@id":"5233d7a6-0c16-4b54-87a1-0c7b0e0a3cff","paymentOrders":[{"accountId":"5233d7a6-0c16-4b54-87a1-0c7b0e0a3cff","counterpartyAccount":"694251588","counterpartyName":"Business Account","creditorReference":[],"id":"1a3c1bb7-edc1-41fc-a23f-7c7624a97a49","initiationDateTime":1.392720702753E12,"instructedAmount":20,"instructedCurrency":"EUR","paymentCategory":[],"remittanceInformation":[],"schedule":null,"status":"Received"}]}];

    describe("Payment Orders Model", function() {


        var $httpBackend;

        var paymentOrdersModel;

        beforeEach(module('payments'));
        beforeEach(module('common'));

        beforeEach(function() {
            inject(function (_$httpBackend_, PaymentOrdersModel, _httpService_) {
                $httpBackend = _$httpBackend_;
                httpService = _httpService_;
                paymentOrdersModel =  new PaymentOrdersModel({
                    paymentOrdersEndpoint: "/my/payment/orders/end/point",
                    pendingPaymentOrdersEndpoint: "/my/pending/payment/orders/end/point",
                    paymentOrdersSubmitPoint: "/my/pending/orders/submit/end/point"
                });
            });

            sessionStorage.clear(); // Do not allow cache http calls
        });

        it("loads payment orders from the server", function() {
            $httpBackend.expectGET(new RegExp("^/my/pending/payment/orders/end/point"));
            $httpBackend.when('GET', new RegExp("^/my/pending/payment/orders/end/point")).respond(function(method, url, data, headers){
                return [200, testPaymentOrders];
            });

            paymentOrdersModel.load();

            $httpBackend.flush();

            expect(paymentOrdersModel.pendingOrdersGroups.length).toBe(2);
        });

        it("submits a  payment order", function() {
            var pending = "";

            $httpBackend.expectPUT('/my/pending/orders/submit/end/point');
            $httpBackend.when('PUT', '/my/pending/orders/submit/end/point').respond(function(method, url, data, headers){
                pending = {"status" : "Pending"};
                return [204, pending];
            });
            paymentOrdersModel.submit(testPaymentOrders[0].paymentOrders[0].accountId);

            $httpBackend.flush();

            expect(pending.status).toBe("Pending");
        });


    });


});


