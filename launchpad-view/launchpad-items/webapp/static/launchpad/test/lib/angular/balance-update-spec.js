define([ "angular",
    "angular-mocks",
    "launchpad/lib/transactions",
    "launchpad/lib/i18n"], function() {

    describe("LP-Balance-Update directive", function() {


        var scope, $template, $element, account;

        var classNames = {
            increase: "lp-increase-amount",
            decrease: "lp-decrease-amount"
        };

        var newAccount = {
            balance: 157,
            currency: "EUR",
            delta: 0
        };

        beforeEach(module('transactions'));
        beforeEach(module('i18n'));

        beforeEach(inject(function(_$compile_, _$rootScope_, _$timeout_) {
            account = {};
            account.balance = 100;
            account.currency = "EUR";
            scope = _$rootScope_;
            scope.account = account;
            $template = _$compile_("<p lp-amount='account.balance' lp-amount-currency='account.currency' lp-balance-update='lp-balance-update' ng-model='account'></p>")(scope);
            $element = $template;
            scope.$digest();
        }));

        it("initializes and text should read 100", function() {
            expect($element.text()).toBe("€ 100.00");

            //no balance changes occurred
            expect($element.hasClass(classNames.increase)).toBe(false);
            expect($element.hasClass(classNames.decrease)).toBe(false);
         });

        it("increases balance and flashes green", function() {

            jasmine.Clock.useMock();

            newAccount.delta = 1;
            scope.account = newAccount;

            scope.$digest();

            expect($element.text()).toBe("€ 157.00");
            expect($element.hasClass(classNames.increase)).toBe(true);

            jasmine.Clock.tick(2500);

            expect($element.hasClass(classNames.increase)).toBe(false);

        });

        it("decreases balance and flashes red", function() {

            jasmine.Clock.useMock();

            newAccount.delta = -1;
            newAccount.balance = 57;
            scope.account = newAccount;

            scope.$digest();

            expect($element.text()).toBe("€ 57.00");
            expect($element.hasClass(classNames.decrease)).toBe(true);

            jasmine.Clock.tick(2500);

            expect($element.hasClass(classNames.decrease)).toBe(false);
        });

    });


});