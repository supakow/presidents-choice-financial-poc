define([ "angular",
    "launchpad/lib/common",
    "angular-mocks",
    "launchpad/lib/contacts"], function(angular) {

    var testContacts = [{"account":"123456","active":true,"id":"1f6e3dfa-dabc-4d8a-c22f-31da90aab45c","name":"Dave","partyId":"3","photoUrl":"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAE0AAABNCAYAAADjCemwAAADlUlEQVR4Xu3aS2sTURQH8JOkTWqb2lYsbe2DNEXQjaj4BQQXLroQXLhx4UIURfAL+C268AFddiOIC8UKCqIb8YFKRVEwfVDSpk1MX4T0kbaeExJIJndmOgdcJPO/UArJnMD8+M+dM/dO4N7U1D5heBIIAM2TV/FgoHk3A5rCDGhA0wgoajCnAU0hoChB0oCmEFCUIGlAUwgoSpA0oCkEFCVIGtAUAooSJA1oCgFFCZIGNIWAogRJA5pCQFGCpAFNIaAoQdKAphBQlCBpQFMIKEqQNKApBBQlSBrQFAKKkrpL2sWeHoq1ttKe4mR39/cpVyjQ3+1t+rmxQcl8XvErdfiq1c3hYeqJRFQnay3aYMA/uRw9W1z09Ht1l7TrsRgda2nxdJJuB2/t7dGbdJo+rqy4HVr8HmglJnmH9uvqKj1PpVzhGgJN5ir5cxpNwSAFXTmIPnHaJpeWHI9sCLTv6+v0dGHBlSTe1kYnolGS/0fCYePxQv96eZneZ7O2v9cQaD8Y7ckB0CoVznV10YXubgpzAq1jk+e4BzMztL6zY4TzLZpodHHabvCNJWKAc0qvr9EE7mR7O13u76+Z7/K7u/RwdtaYNt+jCdzVwcHiPFc5ZG57m8nQO/6zDqCxyHG+OVwZGKhJW3Jzk8Y5bUCzuSfeicdr7qh2lyiSVkK81NdHpzo6qkil95Okpba2qj4HWonDdInKvPaSG11peCsH0FzQJvmx6jM/XgHNMK/ZJc10B0XSSoDyZHB3ZIQOhUJVpL943e1xMomkmW6gdmjyDPqKn0VxeRrUTGhyI8CcZtOjycd2cxrQHNBOc482yr1a5ZoHWg4HMPnK1NwWuLl9xEtEGd6IwZxmALzNGzZHLRs2K4w1Nj2NZ09T4GRdTdBCgYBruyEHoE9jhNHeXjrb2VkFJvOZbO19W1tD0qwCh5ub6RanzLp6m+NFyLFEgrZ56ds6fJ8001wmSPK8+cJmO8/XaHa79U4pa5g57aBbeOXL7Az3ZOd5Jyra1FRz6flmCy/Ni4SLvDQdMuwqlVWER+6SHTyHmXafyscl+N2Oifl5x66uIS5Pl771wF8L/n1uZt0G0EpC8trV+Nycm1fxe9+jZbnr/8DL2dYlbSe9ukO7NjREQ/xSn3bI86TsMqV4DvzCjetvXmT0OuoOzesJ/o/jgaZQBRrQFAKKEiQNaAoBRQmSBjSFgKIESQOaQkBRgqQp0P4BDpKUv786uMkAAAAASUVORK5CYII="},{"account":"345678","active":true,"id":"737e85e3-c8ea-4fd7-faec-3fb11e9ed259","name":"Carmel","partyId":"3","photoUrl":"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAE0AAABNCAYAAADjCemwAAAE5klEQVR4Xu2by29NcRDHp+8XfdEHWn2RYKFICF0QC4KERCyIBWIjUg17/4NFF4TEayVCIjYiiERCiEdCE4QU1bRU3w/Vd+s7Ny6nv57Tc+7cmya/aya5sbhnTs/vc78zv/nNHAmnGxqmSC0iAgkKLSJeoYsVWuTMFJqAmUJTaBICAh/NaQpNQEDgokpTaAICAhdVmkITEBC4qNIUmoCAwEWVptAEBAQuqjSFJiAgcFGlKTQBAYGLKk2hCQgIXFRpCk1AQOCiSlNoAgICF1WaQhMQELio0hSagIDARZWm0P4RWDV/PpVmZFB2cjKlJSWFvuB3ykYnJ6l3bIxah4bo3cCAAFmcvTW0NieH1uXmUlF6OqUkJPgCGZ+aoq7RUXrT10dPu7t9rw9fEBfhWZWVRbuKiig/NTXwws0LO0dG6HprK3UCop9ZD21bYSFtzM+nRL+VBvh+Asp70NFBz3xUZzW03cXFoXAMYpO4KAhYznv329tnDVdroe1EOG7Iy/PkNTQxQY2Dg/S2v58+/Pz59zreIFbgU4mQzvqzQZg3YcVdaW6mFmwWbmYltGok/D2LFrkqh5XCif329+++AtyO0F4P8Mkum0YHctzZL1/iB9qpqirKSUmZsaARlBM3v32jRoey/MjlYfM4Vl5OaYkzg5d31HsIVdOsU9rWhQtpMz6mcThe/Po1VEJEal7K7cG96j9/th9aXWXljNKCQ/IGyoX3wmKVqRwqLaUK5DmncR13samJ2hCqTrNKaZzE9y1ZMiOXNSNhX4bKorHl8+bRgZKSaffmH+Pujx/0vKfHXmj7AYx3Pqfxwjjpc/KPxlKR0zi35SLHTUJhbInYIB64lB9WKc0tNAfGx+lMY2M0vCL2tQYa11R12DXNXe4jdsprLS0RLzwaB2ugueUzDqLHXV30EEefuTRroHkl6jttbfSyt3cumdnz33zW4BSw2zgFjCFhX0DVHqQzEUuq1ijN7XDODcULqKMkBW00EK2B5nZAZ6Wdh9IUmocE3HKaKs0nXnQjECQUr5JDd89ZYJZgsnRk6VJKcvS+uE571NkZ+sTCuHW+Ojt72jGq+dcvuoF2k5UHdj4b1qLDwSM5pzVhUVfRZY2FHa+ooMK0tGm3cmsGWLN78kqOlpWFZplOi1Wtlo2m5gn8KObo77VLF9gqaNye3oTJk2kv0Lq5gxZONMahWWPc26uDYhU0bk3XIoSceY1BDaJrW//pU2h6LrWTaAbkGi30YdzvHOrAfkzkrcxp4Yd267Dyd9F0O7xGgV73tEppDIdz2hHkNrcZpiRMeRq1A+NA835erW5+Buug8UMfRFuai10345daeF4QxHhAswUftx+gAfPSW0apEb6nldC4IcnlR4bHsJcnUzwgfoWWkdvAt2bBAqpGPWaWF2EoXlMoq6Hxw3sNWUyFcTIfBkSemnM5kYU6z9xInD4M/BKGNLO1m6xUWniRyxCie9Fjy/RQXJAQdV7D8wYulP26JlZDCy/4MI5X5ZmZkTKadj2HMQ+bg1hcQOOFrsRob2tBARVE+I4av5f2ArnPnG3OBi9uoIUXWQHFrcXrV4vxNiRvGMk4s4bnmPwvF8ADyFv8+ugTvKthFq7/ldKCLDZW18Sd0mIF5r8KT4U2FwQEf0PDU6EJCAhcVGkKTUBA4KJKU2gCAgKX3wfQnnLdm59cAAAAAElFTkSuQmCC"},{"account":"23457","active":true,"id":"c36b57cd-187a-44d7-82d4-47a6f2663fe5","name":"Grace","partyId":"3","photoUrl":"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAE0AAABNCAYAAADjCemwAAAE40lEQVR4Xu2aWUtVURTH13XWm1OlOaSlVtRLEkQFvQQVQRFR0EMPERHNfYe+REUEBRXYXDQSQX2DhCZpwKHM0iwccii1bP2lW9vjOfecszS8+7YW+KJn3cv++V/jPpGjT5+OklooAhGFForX2MMKLTwzhSZgptAUmoSAwEdzmkITEBC4qNIUmoCAwEWVptAEBAQuqjSFJiAgcFGlKTQBAYGLKk2hCQgIXFRpCk1AQOCiSlNoAgICF1WaQhMQELio0hSagIDARZWm0AQEBC6qNIUmICBwUaUpNKLlhYVUlJlJ2SkpFIlE/iDpGxmhzu/fqb67W4BpvEtSKG19cTEtnDGDCjIy6C8mdzY/+de9w8PU1N9PDz99oqGf+E04sxrahjlzqLaggNINRYU5PnA19fXRpffvw7jZ+dZQBofernnzqJjDcCps8McPqnv3jto5fIOYdUqLpqbSnqoqyk1LC3K+wM8gTOtaW6ltcNDXxypoQYAh2TdzvmoeGKA+zl3R9HQq5J/SrCyqyM6mQs57XtbPijvR2Oib56yCtq+6moo8Dt01NET32tuphWHFs6polNYUFVEZQ3SzVlbaubdv436GNdBw0NWzZrke5vXXr3Slrc03rMwH1nHFXTFz5oRqi1fdzzM0wPMyK6Ah8R+qqSGEp2k4YH1XF93v6AgFLPbw5tJSWpqfP8H3eW8v3fzwwW5oa1kVq1gVTnvD7cLlkO2C8zOO8D8jn3OeaQj1E01NdkM7zAcrcBwM1e5MSwt94QNOxhCmKx3/kJHRUTrLn+3VgiR8eC7OzaWt5eWU4iDzivPY1ZB5zA1uOVfUnZWVlGo0yAj7WxyeCFM3S3hoG0tKaBl3/c5c5pesJ6M+P9+Eh7Z7/vwJ7UEP91/HuJ+aLktoaKiaB7g3c3b/GLYvcPc+XZbw0FDdsh2txpOeHrrz8eN0MUvsgR0jz16eM80tBpL0XQYGcH6GgX4Br4wkhhTwwsZC4FY5Ae0+j0tBloluRSQowG6GdtwjbyZ0eEIl2+fOHdduhIGGfRs2uRJD/3fSo8G1ElrQ8FRohlweB5w3/0toaDncqmcDJ+gbcQZqMxxLfLa7WHlj17aJh3fzfsHa8PSCNlUjVAyuW8GxFhoO5bZ47OfrOGwhJDdJbkXBLYythradh/VFPLQ7Z8/b3Ks9C9CrBamcGNgrc3LGPWo1NKxtsE9z3mdO1SiFxeZ+HtWcU4fV0HCogzxKZXJRMA07r9PNzfT5H+zT8D1WQ8MBtpWV0ZK8vAmRFuQSJF54eqksKaBhBt3PM6i5KIzB8Nvnx4O2jz8T7324mfVKi6c2/O0lb3Gvhdji4jIFeTLHsT0x4SUFtLEbKU7YUY+bdbQhgPeos9OzFallWMu5sPg1vEkRnjEF1GCA5xbELUxNlQDgAN+Wf/v9RlAWA8/jixlnMYn5oKgM87NmBU0KpcUOWM035Fu4MMQLrSC9mQnsOoc2pgLzDjSpoMUOu6OigvCKgd/7aPEAYtF4kdfmaFsQuub8mZTQAAOqw73lbK6AYeAhfBs4/z0wbuYRvqimsRDGizSnuA90s4TepwUNM6y1MTlgW4FLmDRnI8z5Cq+PQlEoFl5r7KDflxTQgh52qp5TaAKSCk2hCQgIXFRpCk1AQOCiSlNoAgICF1WaQhMQELj8AkeKtqaQ7Il8AAAAAElFTkSuQmCC"}];
    var testDetails = {"email":"david.morris88@gmail.com","id":"1f6e3dfa-dabc-4d8a-c22f-31da90aab45c","phone":"0857037052"};

    describe("Contacts Model", function() {


        var $httpBackend;

        var contactsModel;

        var getFakeContacts = function() {
            return testContacts;
        };

        beforeEach(module('contacts'));
        beforeEach(module('common'));

        beforeEach(function() {
            inject(function (_$httpBackend_, ContactsModel, _httpService_) {
                $httpBackend = _$httpBackend_;
                httpService = _httpService_;
                contactsModel = new ContactsModel({
                    contacts: "/my/contacts/end/point",
                    contactData: "/contact/data/end/point",
                    contactDetails: "/contact/details/end/point",
                    locale: "EN",
                    lazyload: true
                });
            });

            sessionStorage.clear(); // Do not allow cache http calls
        });

        it("loads contacts from the server", function() {
            $httpBackend.expectGET('/my/contacts/end/point');
            $httpBackend.when('GET', '/my/contacts/end/point').respond(function(method, url, data, headers){
                return [200, testContacts];
            });

            //expect(accountsModel.accounts.length).toBe(0);
            contactsModel.loadContacts();

            $httpBackend.flush();

            expect(contactsModel.contacts.length).toBe(3);
        });

        it("loads contacts from the server and selects a contact", function() {
            $httpBackend.expectGET('/my/contacts/end/point');
            $httpBackend.when('GET', '/my/contacts/end/point').respond(function(method, url, data, headers){
                return [200, testContacts];
            });

            //expect(accountsModel.accounts.length).toBe(0);
            contactsModel.loadContacts();

            $httpBackend.flush();

            var contact = contactsModel.contacts[0];

            contactsModel.selectContact(contact);

            //Check to see if the function has selected the correct contact
            expect(contactsModel.selected).toBe("737e85e3-c8ea-4fd7-faec-3fb11e9ed259");
            expect(contactsModel.currentContact.account).toBe("345678");
            expect(contactsModel.currentContact.active).toBe(true);
            expect(contactsModel.currentContact.name).toBe("Carmel");
            expect(contactsModel.currentContact.partyId).toBe("3");

        });

        it("loads contacts from the server and loads the details for the selected contact", function() {
            $httpBackend.expectGET('/my/contacts/end/point');
            $httpBackend.when('GET', '/my/contacts/end/point').respond(function(method, url, data, headers){
                return [200, testContacts];
            });

            //expect(accountsModel.accounts.length).toBe(0);
            contactsModel.loadContacts();

            $httpBackend.flush();

            $httpBackend.expectGET('/contact/details/end/point');
            $httpBackend.when('GET', '/contact/details/end/point').respond(function(method, url, data, headers){
                return [200, testDetails];
            });

            contactsModel.loadContactDetails("1f6e3dfa-dabc-4d8a-c22f-31da90aab45c");

            $httpBackend.flush();

            //Check to see if the function has selected the correct contact
            expect(contactsModel.contactDetailsData[0].email).toBe("david.morris88@gmail.com");
            expect(contactsModel.contactDetailsData[0].id).toBe("1f6e3dfa-dabc-4d8a-c22f-31da90aab45c");
            expect(contactsModel.contactDetailsData[0].phone).toBe("0857037052");
        });

    });


});