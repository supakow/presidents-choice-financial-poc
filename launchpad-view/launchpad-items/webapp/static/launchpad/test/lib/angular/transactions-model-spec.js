define([ "angular",
	"launchpad/lib/common",
	"angular-mocks",
	"launchpad/lib/transactions"], function(angular) {

	var testTransactions = [{"@type":"object","accountId":"c114b066-6639-477d-8a84-673b20c772b0","bookingDateTime":1.384526731266E12,"counterpartyAccount":"146051090","counterpartyName":"Business Account","id":"1d76a62b-5dde-4fae-bd19-8cd2d14ae9a6","instructedAmount":123,"instructedCurrency":"EUR","creditDebitIndicator":"DBIT","tags":[null],"remittanceInformation":[],"transactionCurrency":"EUR","transactionAmount":123,"transactionType":"Online Transfer"},{"@type":"object","accountId":"c114b066-6639-477d-8a84-673b20c772b0","bookingDateTime":1.384248809587E12,"counterpartyAccount":"146051090","counterpartyName":"Business Account","id":"dd5cb57f-69b2-4f73-ae1e-5319c3b18239","instructedAmount":123,"instructedCurrency":"EUR","creditDebitIndicator":"DBIT","tags":[null],"remittanceInformation":[],"transactionCurrency":"EUR","transactionAmount":123,"transactionType":"Online Transfer"},{"@type":"object","accountId":"c114b066-6639-477d-8a84-673b20c772b0","bookingDateTime":1.381799904162E12,"counterPartyLogoPath":"/portalserver/static/launchpad/widgets/transactions/media/francine.png","counterpartyAccount":"846820671","counterpartyName":"Francine","creditDebitIndicator":"DBIT","id":"39b4ec47-0327-4088-ae13-d6eb72d71931","instructedAmount":0,"instructedCurrency":[],"tags":["Withdrawal"],"transactionAmount":399.1,"transactionCurrency":"EUR","transactionType":"Online Transfer"},{"@type":"object","accountId":"c114b066-6639-477d-8a84-673b20c772b0","bookingDateTime":1.381610064163E12,"counterPartyLogoPath":"/portalserver/static/launchpad/widgets/transactions/media/atlanticelec.png","counterpartyAccount":"045545412","counterpartyName":"Atlantic City Electric","creditDebitIndicator":"DBIT","id":"2b1dd9e4-5a2a-42af-81b0-3bb3cf041b65","instructedAmount":0,"instructedCurrency":[],"tags":["Withdrawal"],"transactionAmount":162.1,"transactionCurrency":"EUR","transactionType":"Payment Debit Card"},{"@type":"object","accountId":"c114b066-6639-477d-8a84-673b20c772b0","bookingDateTime":1.381524804163E12,"counterPartyLogoPath":[],"counterpartyAccount":"128244839","counterpartyName":"Murray's Bagels","creditDebitIndicator":"DBIT","id":"6e5d8f47-5877-49f7-b862-ec6ce1a746b6","instructedAmount":0,"instructedCurrency":[],"tags":["Withdrawal"],"transactionAmount":311.1,"transactionCurrency":"EUR","transactionType":"Bill Payment"},{"@type":"object","accountId":"c114b066-6639-477d-8a84-673b20c772b0","bookingDateTime":1.381423044165E12,"counterPartyLogoPath":"/portalserver/static/launchpad/widgets/transactions/media/tealounge.png","counterpartyAccount":"348647270","counterpartyName":"The Tea Lounge","creditDebitIndicator":"DBIT","id":"67291bb3-f954-4c29-8876-5c51e1850484","instructedAmount":0,"instructedCurrency":[],"tags":["Withdrawal"],"transactionAmount":380.1,"transactionCurrency":"EUR","transactionType":"Online Transfer"},{"@type":"object","accountId":"c114b066-6639-477d-8a84-673b20c772b0","bookingDateTime":1.381324824165E12,"counterPartyLogoPath":[],"counterpartyAccount":"863691841","counterpartyName":"Macy's","creditDebitIndicator":"DBIT","id":"1c8ef9bc-04a9-4c53-a9db-64d99ff5c3bd","instructedAmount":0,"instructedCurrency":[],"tags":["Withdrawal"],"transactionAmount":175.1,"transactionCurrency":"EUR","transactionType":"Bill Payment"},{"@type":"object","accountId":"c114b066-6639-477d-8a84-673b20c772b0","bookingDateTime":1.381179744167E12,"counterPartyLogoPath":[],"counterpartyAccount":"676149676","counterpartyName":"Murray's Bagels","creditDebitIndicator":"DBIT","id":"9340a7e1-5777-435c-b02f-d2bc091bc21c","instructedAmount":0,"instructedCurrency":[],"tags":["Withdrawal"],"transactionAmount":193.1,"transactionCurrency":"EUR","transactionType":"Payment Credit Card"},{"@type":"object","accountId":"c114b066-6639-477d-8a84-673b20c772b0","bookingDateTime":1.381179144167E12,"counterPartyLogoPath":[],"counterpartyAccount":"028738668","counterpartyName":"CH Energy Group","creditDebitIndicator":"DBIT","id":"e9d26010-3f82-42fb-a94d-89669c4bdbaa","instructedAmount":0,"instructedCurrency":[],"tags":["Withdrawal"],"transactionAmount":168.1,"transactionCurrency":"EUR","transactionType":"Bill Payment"},{"@type":"object","accountId":"c114b066-6639-477d-8a84-673b20c772b0","bookingDateTime":1.381161384167E12,"counterPartyLogoPath":[],"counterpartyAccount":"831027676","counterpartyName":"Safeway Inc.","creditDebitIndicator":"DBIT","id":"96d664ef-6a97-45f3-86bf-b8d366cf8dd7","instructedAmount":0,"instructedCurrency":[],"tags":["Withdrawal"],"transactionAmount":15.1,"transactionCurrency":"EUR","transactionType":"Online Transfer"},{"@type":"object","accountId":"c114b066-6639-477d-8a84-673b20c772b0","bookingDateTime":1.381125084169E12,"counterPartyLogoPath":[],"counterpartyAccount":"983717479","counterpartyName":"AT&T","creditDebitIndicator":"DBIT","id":"cfda326f-e7ea-4ddb-84d2-359dec3c9c3e","instructedAmount":0,"instructedCurrency":[],"tags":["Withdrawal"],"transactionAmount":94.1,"transactionCurrency":"EUR","transactionType":"Online Transfer"},{"@type":"object","accountId":"c114b066-6639-477d-8a84-673b20c772b0","bookingDateTime":1.381029504169E12,"counterPartyLogoPath":"/portalserver/static/launchpad/widgets/transactions/media/atlanticelec.png","counterpartyAccount":"998672599","counterpartyName":"Atlantic City Electric","creditDebitIndicator":"DBIT","id":"5f0d4879-30f7-4e51-9d33-53784123fb59","instructedAmount":0,"instructedCurrency":[],"tags":["Withdrawal"],"transactionAmount":398.1,"transactionCurrency":"EUR","transactionType":"Bill Payment"},{"@type":"object","accountId":"c114b066-6639-477d-8a84-673b20c772b0","bookingDateTime":1.38094088417E12,"counterPartyLogoPath":[],"counterpartyAccount":"995634682","counterpartyName":"Macy's","creditDebitIndicator":"DBIT","id":"a13074a6-21d5-4efc-9354-286b91ac0120","instructedAmount":0,"instructedCurrency":[],"tags":["Withdrawal"],"transactionAmount":316.1,"transactionCurrency":"EUR","transactionType":"Online Transfer"},{"@type":"object","accountId":"c114b066-6639-477d-8a84-673b20c772b0","bookingDateTime":1.38092954417E12,"counterPartyLogoPath":[],"counterpartyAccount":"497136037","counterpartyName":"Macy's","creditDebitIndicator":"DBIT","id":"53ee7c56-478b-4725-8e9c-34adbeacd6f4","instructedAmount":0,"instructedCurrency":[],"tags":["Withdrawal"],"transactionAmount":41.1,"transactionCurrency":"EUR","transactionType":"Bill Payment"},{"@type":"object","accountId":"c114b066-6639-477d-8a84-673b20c772b0","bookingDateTime":1.380789024172E12,"counterPartyLogoPath":[],"counterpartyAccount":"733708987","counterpartyName":"CH Energy Group","creditDebitIndicator":"DBIT","id":"79122c16-2796-4ea9-b91f-7310a96c4ff0","instructedAmount":0,"instructedCurrency":[],"tags":["Withdrawal"],"transactionAmount":328.1,"transactionCurrency":"EUR","transactionType":"Online Transfer"},{"@type":"object","accountId":"c114b066-6639-477d-8a84-673b20c772b0","bookingDateTime":1.380630924172E12,"counterPartyLogoPath":[],"counterpartyAccount":"219794729","counterpartyName":"Murray's Bagels","creditDebitIndicator":"DBIT","id":"6fb0846b-cca8-4370-aa93-c35c1ccc1013","instructedAmount":0,"instructedCurrency":[],"tags":["Withdrawal"],"transactionAmount":292.1,"transactionCurrency":"EUR","transactionType":"Bill Payment"},{"@type":"object","accountId":"c114b066-6639-477d-8a84-673b20c772b0","bookingDateTime":1.380340524174E12,"counterPartyLogoPath":[],"counterpartyAccount":"297756251","counterpartyName":"AT&T","creditDebitIndicator":"DBIT","id":"9c5020f3-0a54-4edf-b46a-35309ac45331","instructedAmount":0,"instructedCurrency":[],"tags":["Withdrawal"],"transactionAmount":219.1,"transactionCurrency":"EUR","transactionType":"Online Transfer"},{"@type":"object","accountId":"c114b066-6639-477d-8a84-673b20c772b0","bookingDateTime":1.380316524174E12,"counterPartyLogoPath":[],"counterpartyAccount":"942292525","counterpartyName":"Safeway Inc.","creditDebitIndicator":"DBIT","id":"43cfbae4-4d22-4e50-863f-2ab8eee765af","instructedAmount":0,"instructedCurrency":[],"tags":["Withdrawal"],"transactionAmount":366.1,"transactionCurrency":"EUR","transactionType":"Cash Withdrawl"},{"@type":"object","accountId":"c114b066-6639-477d-8a84-673b20c772b0","bookingDateTime":1.380113064176E12,"counterPartyLogoPath":"/portalserver/static/launchpad/widgets/transactions/media/francine.png","counterpartyAccount":"182713802","counterpartyName":"Francine","creditDebitIndicator":"DBIT","id":"65975ff4-d357-4488-b118-a0d43e1f1ea9","instructedAmount":0,"instructedCurrency":[],"tags":["Withdrawal"],"transactionAmount":296.1,"transactionCurrency":"EUR","transactionType":"Payment Credit Card"},{"@type":"object","accountId":"c114b066-6639-477d-8a84-673b20c772b0","bookingDateTime":1.380100344176E12,"counterPartyLogoPath":"/portalserver/static/launchpad/widgets/transactions/media/tealounge.png","counterpartyAccount":"777917997","counterpartyName":"The Tea Lounge","creditDebitIndicator":"DBIT","id":"e52b4fff-0c57-44bb-bf81-2ffddc709758","instructedAmount":0,"instructedCurrency":[],"tags":["Withdrawal"],"transactionAmount":34.1,"transactionCurrency":"EUR","transactionType":"Online Transfer"}];

	describe("Transactions Model", function() {


		var $httpBackend;

		var transactionsModel;

		var getFakeTransactions = function(numberOfTransactions, start) {
			return testTransactions.slice(start || 0, numberOfTransactions);
		};

		beforeEach(module('transactions'));
		beforeEach(module('common'));

		beforeEach(function() {
			inject(function (_$httpBackend_, TransactionsModel, _httpService_) {
				$httpBackend = _$httpBackend_;
				httpService = _httpService_;
				transactionsModel = TransactionsModel.getInstance({
					transactionsEndpoint: "/my/transactions/end/point",
					transactionDetailsEndpoint: "/my/close/end/point",
					pageSize: 10
				});
			});

			sessionStorage.clear(); // Do not allow cache http calls
		});

		it("loads transactions from the server", function() {
			$httpBackend.expectGET('/my/transactions/end/point?f=1&l=10&sort=-bookingDateTime');
			$httpBackend.when('GET', '/my/transactions/end/point?f=1&l=10&sort=-bookingDateTime').respond(function(method, url, data, headers){
				return [200, testTransactions];
			});
			
			spyOn(transactionsModel, "preprocessTransactions").andCallThrough();

			expect(transactionsModel.transactions.length).toBe(0);
			transactionsModel.loadTransactions({
				id: "123"
			});

			$httpBackend.flush();

			expect(transactionsModel.transactions.length).toBe(20);
			expect(transactionsModel.preprocessTransactions).toHaveBeenCalled();
		});

		it("loads transactions from the server using pagination", function() {
			$httpBackend.expectGET('/my/transactions/end/point?f=1&l=10&sort=-bookingDateTime');
			$httpBackend.when('GET', '/my/transactions/end/point?f=1&l=10&sort=-bookingDateTime').respond(function(method, url, data, headers){
				return [200, testTransactions];
			});

			var transactionsToReturn = 10;

			transactionsModel.loadTransactions({
				id: "123"
			});

			$httpBackend.flush();
			expect(transactionsModel.allowMoreResults()).toBe(true);

			transactionsToReturn = 5;
			transactionsModel.loadMoreTransactions({
				id: "123"
			});

			$httpBackend.expectGET('/my/transactions/end/point?f=11&l=10&sort=-bookingDateTime');
			$httpBackend.when('GET', '/my/transactions/end/point?f=11&l=10&sort=-bookingDateTime').respond(function(method, url, data, headers){
				return [200, getFakeTransactions(10, 5)];
			});
			$httpBackend.flush();

			expect(transactionsModel.allowMoreResults()).toBe(false);
		});

		it("loads transactions from the server with filtering", function() {
			var requestUrl;

			//search by date
			transactionsModel.setFilters({
				fromDate: 123456789,
				toDate: 987654321
			});
			requestUrl = '/my/transactions/end/point?df=123456789&dt=987654321&f=1&l=10&sort=-bookingDateTime';
			$httpBackend.expectGET(requestUrl);
			$httpBackend.when('GET', requestUrl).respond(function(method, url, data, headers){
				return [200, testTransactions];
			});

			transactionsModel.loadTransactions({
				id: "123"
			});
			$httpBackend.flush();


			//search by amount
			transactionsModel.setFilters({
				fromAmount: 100,
				toAmount: 200
			});
			requestUrl = '/my/transactions/end/point?af=100&at=200&f=1&l=10&sort=-bookingDateTime';
			$httpBackend.expectGET(requestUrl);
			$httpBackend.when('GET', requestUrl).respond(function(method, url, data, headers){
				return [200, testTransactions];
			});
			transactionsModel.loadTransactions({
				id: "123"
			});
			$httpBackend.flush();

			//search by query
			transactionsModel.setFilters({
				query: "Expensive things"
			});
			requestUrl = '/my/transactions/end/point?a=search&f=1&l=10&q=Expensive+things&sort=-bookingDateTime';
			$httpBackend.expectGET(requestUrl);
			$httpBackend.when('GET', requestUrl).respond(function(method, url, data, headers){
				return [200, testTransactions];
			});
			transactionsModel.loadTransactions({
				id: "123"
			});
			$httpBackend.flush();

			//search by contact
			transactionsModel.setFilters({
				contact: "123.456.789"
			});
			requestUrl = '/my/transactions/end/point?ca=123.456.789&f=1&l=10&sort=-bookingDateTime';
			$httpBackend.expectGET(requestUrl);
			$httpBackend.when('GET', requestUrl).respond(function(method, url, data, headers){
				return [200, testTransactions];
			});
			transactionsModel.loadTransactions({
				id: "123"
			});
			$httpBackend.flush();
		});

		it("loads transactions from the server with sorting", function() {
			var requestUrl;

			//search ascending
			transactionsModel.sort = 'mysorting';
			requestUrl = '/my/transactions/end/point?f=1&l=10&sort=mysorting';
			$httpBackend.expectGET(requestUrl);
			$httpBackend.when('GET', requestUrl).respond(function(method, url, data, headers){
				return [200, testTransactions];
			});
			transactionsModel.loadTransactions({
				id: "123"
			});
			$httpBackend.flush();

			//search ascending
			transactionsModel.sort = '-mysorting';
			requestUrl = '/my/transactions/end/point?f=1&l=10&sort=-mysorting';
			$httpBackend.expectGET(requestUrl);
			$httpBackend.when('GET', requestUrl).respond(function(method, url, data, headers){
				return [200, testTransactions];
			});
			transactionsModel.loadTransactions({
				id: "123"
			});
			$httpBackend.flush();
		});
	});


});