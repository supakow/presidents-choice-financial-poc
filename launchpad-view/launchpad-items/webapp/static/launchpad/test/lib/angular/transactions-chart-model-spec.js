define([ "angular",
    "launchpad/lib/common",
    "angular-mocks",
    "launchpad/lib/transactions"], function(angular) {

    var testChartTransactions = [{"date":1392245999148,"deposit":0,"withdrawal":0},{"date":1392332399148,"deposit":0,"withdrawal":0},{"date":1392418799148,"deposit":0,"withdrawal":0},{"date":1392505199148,"deposit":0,"withdrawal":0},{"date":1392591599148,"deposit":0,"withdrawal":0},{"date":1392677999148,"deposit":0,"withdrawal":0},{"date":1392764399148,"deposit":0,"withdrawal":20}];

    describe("Transaction Charts Model", function() {


        var $httpBackend;

        var transactionChartsModel;

        var getFakeChartTransactions = function() {
            return testChartTransactions;
        };

        beforeEach(module('transactions'));
        beforeEach(module('common'));

        beforeEach(function() {
            inject(function (_$httpBackend_, TransactionsChartModel, _httpService_) {
                $httpBackend = _$httpBackend_;
                httpService = _httpService_;
                transactionChartsModel =  new TransactionsChartModel.getInstance({
                    transactionsChartEndpoint: "/my/chart/transactions/end/point",
                    accountId: 3
                });
            });

            sessionStorage.clear(); // Do not allow cache http calls
        });

        it("loads chart transactions from the server", function() {
            var params = {
                end: 1392726440148,
                start: 139212164014
            };

            $httpBackend.expectGET('/my/chart/transactions/end/point?end=1392726440148&start=139212164014');
            $httpBackend.when('GET', '/my/chart/transactions/end/point?end=1392726440148&start=139212164014').respond(function(method, url, data, headers){
                return [200, testChartTransactions];
            });

            transactionChartsModel.load(params);

            $httpBackend.flush();

            expect(transactionChartsModel.chartData.length).toBe(7);
        });




    });
});