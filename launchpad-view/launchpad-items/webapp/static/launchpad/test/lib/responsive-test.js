/* global assertEquals */

	require([ "jquery", "launchpad/lib/ui/responsive" ], function($, Responsive) {

		module("responsive", {

			setup: function() {
			}
		});

		function makeEl(width) {
			var $el = $("<div />");
			$el.css({
				width: width,
				height: 100
			});
			$("#qunit-fixture").append($el);
			return $el;
		}

		test("testEnable", function() {

			var $el = makeEl(100);
			var resp = Responsive.enable($el[0]);

			assertInstanceOf(Responsive, resp);
		});

		asyncTest("rules", 21, function() {

			var $el = makeEl(100);
			var resp = Responsive.enable($el[0]);

			var triggerCount = [];
			var anyCount = 0;

			//rule 0
			resp.rule({
				minWidth : 0,
				maxWidth : 99,
				then: function() {
					triggerCount[0]++
				}
			});
			triggerCount.push(0);

			//rule 1
			resp.rule({
				minWidth : "100",
				maxWidth : "1000px",
				then: function() {
					triggerCount[1]++;
				}
			});
			triggerCount.push(0);

			//rule 2
			resp.rule({
				maxWidth : 1000.0,
				then: function() {
					triggerCount[2]++;
				}
			});
			triggerCount.push(0);

			//rule 3
			resp.rule({
				minWidth : "100.0",
				then: function() {
					triggerCount[3]++;
				}
			});
			triggerCount.push(0);

			//rule 4
			resp.rule({
				minWidth : 1001,
				maxWidth : 1100,
				then: function() {
					triggerCount[4]++;
				}
			});
			triggerCount.push(0);

			//any change
			resp.any(function() {
				anyCount++;
			});

			var tests = [
				function() {
					//first check (100)
					assertEquals("first check, rule 0", triggerCount[0], 0);
					assertEquals("first check, rule 1", triggerCount[1], 1);
					assertEquals("first check, rule 2", triggerCount[2], 1);
					assertEquals("first check, rule 3", triggerCount[3], 1);
					assertEquals("first check, rule 4", triggerCount[4], 0);

					//update width that shouldn't trigger new rules
					$el.width(200);
				},
				function() {

					assertEquals("second check (width=200), rule 0", triggerCount[0], 0);
					assertEquals("second check (width=200), rule 1", triggerCount[1], 1);
					assertEquals("second check (width=200), rule 2", triggerCount[2], 1);
					assertEquals("second check (width=200), rule 3", triggerCount[3], 1);
					assertEquals("second check (width=200), rule 4", triggerCount[4], 0);

					//less than 100
					$el.width(50);
				},
				function() {

					assertEquals("third check (width=200), rule 0", triggerCount[0], 1); //this should increment
					assertEquals("third check (width=200), rule 1", triggerCount[1], 1);
					assertEquals("third check (width=200), rule 2", triggerCount[2], 1); //this shouldn't (already active)
					assertEquals("third check (width=200), rule 3", triggerCount[3], 1);
					assertEquals("third check (width=200), rule 4", triggerCount[4], 0);

					//greater than 1000
					$el.width(1050);
				},
				function() {
					assertEquals("fourth check (width=200), rule 0", triggerCount[0], 1);
					assertEquals("fourth check (width=200), rule 1", triggerCount[1], 1);
					assertEquals("fourth check (width=200), rule 2", triggerCount[2], 1);
					assertEquals("fourth check (width=200), rule 3", triggerCount[3], 2); //this should increment
					assertEquals("fourth check (width=200), rule 4", triggerCount[4], 1); //this should increment

					assertEquals("check any value", 6, anyCount)
				}
			];

			var count = 0;
			var interval = window.setInterval(function() {
				tests[count++]();
				if(tests.length === count) {
					clearInterval(interval);
					start();
				}
			}, 350);
		});

		asyncTest("rules_AltNames", 1, function() {

			var $el = makeEl(502);
			var resp = Responsive.enable($el[0]);

			resp.rule({
				"min-width" : "400px",
				"max-width" : 402.03,
				then: function() {
					fail("False positive check");
					start();
				}
			});
			resp.rule({
				"min-width" : 500,
				"max-width" : 502,
				then: function() {
					ok("min-width and max-width work");
					start();
				}
			});
		});

		asyncTest("rules_nonNumberValues", 1, function() {

			var $el = makeEl(602);
			var resp = Responsive.enable($el[0]);

			resp.rule({
				"min-width" : "400px",
				"max-width" : 402.03,
				then: function() {
					fail("False positive check");
					start();
				}
			});
			resp.rule({
				"min-width" : "600px",
				"max-width" : 602.03,
				then: function() {
					ok("Non number values parsed ok");
					start();
				}
			});
		});

		asyncTest("rules_other", 2, function() {

			var $el = makeEl(5000);
			var resp = Responsive.enable($el[0]);

			resp.rule({
				"min-width" : 0,
				"max-width" : 99999,
				then: function(e) {
					assertEquals("testing event object", 5000, e.width);
					assertSame("testing context", $el[0], this);
					start();
				}
			});
		});

		asyncTest("resize", 1, function() {

			var $el = makeEl(100);
			var resp = Responsive.enable($el[0]);

			resp.resize(function() {

				assertTrue("resize called");
				start();
			});

			window.setTimeout(function() {
				$el.width(200);
			});
		});
	});
