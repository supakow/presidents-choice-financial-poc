define([ "launchpad/lib/common/util" ], function(util) {

	describe("Launchpad Utilities", function() {

		describe("Parsing as a boolean", function() {

			var parsedVal;
			it("is already is  a boolean and is true", function() {
				parsedVal = util.parseBoolean(true);
				expect(parsedVal).toBe(true);
			});

			it("is already a boolean and is false", function() {
				parsedVal = util.parseBoolean(false);
				expect(parsedVal).toBe(false);
			});

			it("is a string and is 'true'", function() {
				parsedVal = util.parseBoolean("true");
				expect(parsedVal).toBe(true);
			});

			it("is a string and is 'false'", function() {
				parsedVal = util.parseBoolean("false");
				expect(parsedVal).toBe(false);
			});

			it("is a string and is 'TRUE'", function() {
				parsedVal = util.parseBoolean("TRUE");
				expect(parsedVal).toBe(true);
			});

			it("is a string and is 'FALSE'", function() {
				parsedVal = util.parseBoolean("FALSE");
				expect(parsedVal).toBe(false);
			});

			it("is a string and is 'TruE'", function() {
				parsedVal = util.parseBoolean("true");
				expect(parsedVal).toBe(true);
			});

			it("is a string and is 'FaLsE'", function() {
				parsedVal = util.parseBoolean("false");
				expect(parsedVal).toBe(false);
			});

			it("is an untrimmed string and is 'true'", function() {
				parsedVal = util.parseBoolean(" true   ");
				expect(parsedVal).toBe(true);
			});

			it("is an untrimmed string and is 'false'", function() {
				parsedVal = util.parseBoolean("false\t");
				expect(parsedVal).toBe(false);
			});

			it("is a number and is not 0", function() {
				parsedVal = util.parseBoolean(1);
				expect(parsedVal).toBe(true);
			});

			it("is a number and is 0", function() {
				parsedVal = util.parseBoolean(0);
				expect(parsedVal).toBe(false);
			});
		});
	});
});