/* global define assertEquals */


require([ "launchpad/lib/common", "jquery", "jquery.mockjax" ], function(services, $) {
	module("services", {

		setup: function() {
			$.mockjax({
				url: "/tests/*",
				responseTime: 200,
				response: function(settings) {
					reqCounter[settings.url]++;
					this.responseText = responses[settings.url] ;
				}
			})
		},
		tearDown: function() {
			$.mockjaxClear();
		}
	});
	var responses = {
		"/tests/one" : "hello one",
		"/tests/two" : {
			"two" : 2
		},
		"/tests/three" : {
			"three" : 3
		},
		"/tests/five" : {
			"five" : 5
		}
	};
	var reqCounter = {
		"/tests/one" : 0,
		"/tests/two" : 0,
		"/tests/three" : 0,
		"/tests/five" : 0
	};


	test("Test service singleton", 3, function() {

		var service1 = services.getService({ endpoint: "/tests/one"});
		var service2 = services.getService({ endpoint: "/tests/one"});
		var service3 = services.getService({ endpoint: "/tests/one"});
		var service4 = services.getService({ endpoint: "/tests/two"});
		assertSame("Services are same", service1, service2);
		assertSame("Services are same", service2, service3);
		assertNotSame("Services are different", service3, service4);
	});

	test("Test url templates", 6, function() {

		//services 1 and 2 should be the same instance
		var service1 = services.getService({
			endpoint: "/tests/$(varA)/blah/$(varB)/$(varA)",
			urlVars: {
				varA: "A",
				varB: "B"
			}
		});
		var service2 = services.getService({
			endpoint: "/tests/$(varA)/blah/$(varB)/$(varA)",
			urlVars: {
				varA: "A",
				varB: "B"
			}
		});

		//servies 3 and 4 should be different instances
		var service3 = services.getService({
			endpoint: "/tests/$(varA)/blah/$(varB)/$(varA)",
			urlVars: {
				varA: "C",
				varB: "D"
			}
		});
		var service4 = services.getService({
			endpoint: "/tests/$(varA)/blah/$(varB)/$(varA)",
			urlVars: {
				varA: "E",
				varB: "F"
			}
		});

		assertSame("Service 1 url ok", "/tests/A/blah/B/A", service1.endpoint);
		assertSame("Service 2 url ok", "/tests/A/blah/B/A", service2.endpoint);
		assertSame("Service 3 url ok", "/tests/C/blah/D/C", service3.endpoint);
		assertSame("Service 4 url ok", "/tests/E/blah/F/E", service4.endpoint);

		assertSame("Services are same", service1, service2);
		assertNotSame("Services are same", service3, service4);

	});

	asyncTest("Test multiple same reads", 1, function() {

		var service = services.getService({ endpoint: "/tests/two"});

		var resp1 = service.read();
		var resp2 = service.read();
		window.setTimeout(function() {
			var resp3 = service.read();
			var resp4 = service.read();

			$.when(resp1, resp2, resp3, resp4).done(function() {
				assertEquals("One xhr made for multiple reads to same service", 1, reqCounter["/tests/two"]);
				start();
			});
		}, 1000);
	});

	asyncTest("Test read with cache", 3, function() {

		var service = services.getService({
			endpoint: "/tests/three",
			cacheTimeout: 500000
		});
		var response = service.read(null, true);
		response.done(function(data) {
			var actualResponse = responses["/tests/three"];
			assertEquals("response is good", actualResponse.three, data.three);

			window.setTimeout(function() {
				response = service.read();
				response.done(function(data2) {
					deepEqual(data, data2);
					assertEquals("One xhr made. second is from cache", 1, reqCounter["/tests/three"]);
					start();
				});
			}, 500);
		});
	});
	asyncTest("Test read no cache", 1, function() {

		var service = services.getService({
			endpoint: "/tests/five",
			cacheTimeout: 0
		});
		var response = service.read();
		window.setTimeout(function() {
			response = service.read();
			response.done(function() {
				assertEquals("Twos xhr made. none from cache", 2, reqCounter["/tests/five"]);
				start();
			});
		}, 400);
	});
});

var require1 = $.Deferred();
var require2 = $.Deferred();
var service1, service2;
require([ "launchpad/lib/common", "jquery", "jquery.mockjax" ], function(services, $) {
	service1 = services.getService({ endpoint: "/tests/four"});
	require1.resolve();
});
require([ "launchpad/lib/common", "jquery", "jquery.mockjax" ], function(services, $) {
	service2 = services.getService({ endpoint: "/tests/four"});
	require2.resolve();
});
$.when(require1, require2).done(function() {
	module("services");
	test("asserting singletons across require module loads", 1, function() {
		assertSame("services are the same", service1, service2);
	});
});