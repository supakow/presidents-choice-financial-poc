/* global assertEquals */
$(function() {

    var TestHelpers = window.TestHelpers;

    var $fixture,
        springboard,
        cellWidth,
        cellHeight;

    module("Springboard Manager Test", {

        setup: function() {

            $fixture = $("#qunit-fixture");

            springboard = new window.lp.Springboard({
                rows: 3,
                cols: 7,
                height: $fixture.height()
            });

            cellWidth = parseInt($fixture.width() / 7, 10);
            cellHeight = parseInt($fixture.height() / 3, 10);

            springboard.build();
            var html = springboard.getHtmlElement();
            $fixture.html(html);

            springboard.render();
        }
    });

    test("build", function() {

        assertEquals("Checking 21 areas in dom", 21, getNumAreas());
    });

/*    test("fillSpace", function() {

    });*/

    test("resize", function() {

        springboard.makeResizable();

        //drag first cell one cell east
        var east = $fixture.find(".lp-springboard-area-holder").find(".ui-resizable-e")[0];
        TestHelpers.resizable.drag(east, cellWidth, 0);
        assertEquals("Checking 20 areas in dom", 20, getNumAreas());

        //drag first cell one cell south
        var south = $fixture.find(".lp-springboard-area-holder").find(".ui-resizable-s")[0];
        TestHelpers.resizable.drag(south, 0, cellHeight + cellHeight);

        assertEquals("Checking 18 areas in dom", 18, getNumAreas());
    });

    var getNumAreas = function() {
        return $fixture.find(".bp-area").length;
    }
});