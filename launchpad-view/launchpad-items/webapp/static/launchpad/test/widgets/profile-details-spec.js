define([
    'angular',
    'launchpad/widgets/profile-details/js/profile-details',
    'angular-mocks'], function(angular)  {
    
    describe('Profile details', function() {

        var $rootScope, $httpBackend, ProfileDetailsService;

        var storedData = {
            'username': 'lisa',
            'firstName': 'Lisa',
            'lastName': 'Nijenhuis',
            'details': {
                'dateOfBirth': '1985-03-10',
                'gender': 'FEMALE',
                'citizenship': 'American',
                'SSN':'xxxxxxx91'
            },
            'activities': {
                'lastLoggedIn':'2013-12-10T12:14:45'
            }
        };

        var profileData = {
            fullname: 'Lisa Nijenhuis',
            details: [
                { key: 'Birth date', value: 'Mar 10, 1985' },
                { key: 'Gender', value: 'Female' },
                { key: 'Citizenship', value: 'American' },
                { key: 'SSN', value: 'xxxxxxx91' }
            ],
            activities: [
                { key: 'Last Logged In', value: 'Dec 10, 2013 12:14:45 PM' }
            ]
        };

        var mockMessages = {
            'FEMALE': 'Female',
            'dateOfBirth' : 'Birth date',
            'gender' : 'Gender',
            'citizenship' : 'Citizenship',
        }

        // Create the mock
        var mockLoginServive  = {
            getStoredData: function() {
                return JSON.stringify(storedData);
            }
        };

        beforeEach(module('profileDetailsWidget', function($provide) {
            $provide.value('LoginService', mockLoginServive);
        }));

        beforeEach(inject(function (_$rootScope_, _$httpBackend_, _ProfileDetailsService_) {
            $rootScope = _$rootScope_;
            $httpBackend = _$httpBackend_;
            ProfileDetailsService = _ProfileDetailsService_;
        }));

        describe('ProfileDetailsService', function () {
            it('should exist', function () { 
                expect(ProfileDetailsService).toBeDefined();
            });

            it('should have expected functions', function () { 
                expect(angular.isFunction(ProfileDetailsService.restoreUsername)).toBe(true);
                expect(angular.isFunction(ProfileDetailsService.formatResponse)).toBe(true);
                expect(angular.isFunction(ProfileDetailsService.getData)).toBe(true);
            });

            it('`getData` should return the correctly formatted response', function () {
                $httpBackend.when('GET', '/portalserver/services/rest/party-data-management/party').respond(storedData);

                var result;

                ProfileDetailsService.getData().success(function(response) {
                    result = response;
                });
                $httpBackend.flush();
                expect(result).toEqual(storedData);

                expect(ProfileDetailsService.formatResponse(result, mockMessages)).toEqual(profileData);
            });
        });
    });
});