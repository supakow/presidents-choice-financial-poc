define([
	"angular",
	"launchpad/lib/common",
	"launchpad/widgets/notifications/js/notifications",
	"angular-mocks"], function(angular, notifications)  {

	var httpService;
	var testNotifications = {
		"timestamp":1.384506572763E12,
		"messages":[
			{"closable":true,"createdOn":1.384506476897E12,"deactivated":false,"expiresOn":1.384510153888E12,"id":"n1","level":"INFO","links":[{"rel":[],"uri":[]}],"message":"I am the first message","partyId":"3"},
			{"closable":true,"createdOn":1.384506503194E12,"deactivated":false,"expiresOn":1.384511980221E12,"id":"n2","level":"WARNING","links":[{"rel":[],"uri":[]}],"message":"I am the second message","partyId":"3"},
			{"closable":true,"createdOn":1.384506553636E12,"deactivated":false,"expiresOn":1.384512030671E12,"id":"n3","level":"SEVERE","links":[{"rel":[],"uri":[]}],"message":"I am the third message","partyId":"3"}]};

	describe("Notifications Model", function() {

		var $httpBackend, $timeout;

		beforeEach(module('launchpad-notifications'));
		beforeEach(module('common'));

		beforeEach(inject(function(_httpService_, _$httpBackend_, _$timeout_) {
			httpService = _httpService_;
			$httpBackend = _$httpBackend_;
			$timeout = _$timeout_;
			var service = httpService.getInstance({
				endpoint: "/my/notifications/end/point"
			});
			spyOn(service, "read").andCallThrough();

			$httpBackend.when('GET', '/my/notifications/end/point').respond(testNotifications);

			sessionStorage.clear(); // Do not allow cache http calls
		}));

		it("Polls the server for new notifications", function() {

			var notificationsModel;
			inject(function (_$timeout_, NotificationsModel) {
				notificationsModel = NotificationsModel.getInstance({
					notificationsEndpoint: "/my/notifications/end/point",
					closeNotificationEndpoint: "/my/close/end/point"
				});
			});

			var loadCount = 0;
			spyOn(notificationsModel, "loadNotifications").andCallThrough();

			expect(notificationsModel.loadNotifications.callCount).toBe(0);
			notificationsModel.startPolling(100);
			expect(notificationsModel.loadNotifications.callCount).toBe(0);
			$timeout.flush();
			expect(notificationsModel.loadNotifications.callCount).toBe(1);
			$httpBackend.flush();
			sessionStorage.clear();

			$timeout.flush();
			expect(notificationsModel.loadNotifications.callCount).toBe(2);
			$httpBackend.flush();
			sessionStorage.clear();

			$timeout.flush();
			expect(notificationsModel.loadNotifications.callCount).toBe(3);
			$httpBackend.flush();
			sessionStorage.clear();

			notificationsModel.stopPolling();

			//attempt to flush the timeout, however if operating correctly this should throw an error
			//because there is no timeout waiting that can be flushed
			expect(function() {
				$timeout.flush()
			}).not.toThrow();

			expect(notificationsModel.loadNotifications.callCount).toBe(3);
		});

		it("loads all the notifications from the server", function() {

			var onNotificationAddedCount = 0;
			var notificationsModel;
			inject(function (NotificationsModel) {
				notificationsModel = NotificationsModel.getInstance({
					notificationsEndpoint: "/my/notifications/end/point",
					closeNotificationEndpoint: "/my/close/end/point",
					pollInterval: 0,
					onNotificationAdded: function(notification) {
						onNotificationAddedCount++;
					}
				});
			});

			notificationsModel.loadNotifications();
			$httpBackend.flush();

			expect(notificationsModel.notifications.length).toBe(3);
			expect(onNotificationAddedCount).toBe(3);
		});

		it("adds a notification", function() {

			inject(function (_$timeout_, NotificationsModel) {
				notificationsModel = NotificationsModel.getInstance();
			});

			notificationsModel.addNotification(testNotifications.messages[0]);
			//second add of same notification should not work
			notificationsModel.addNotification(testNotifications.messages[0]);
			notificationsModel.addNotification(testNotifications.messages[1]);

			expect(notificationsModel.notifications.length).toBe(2);
		});


		it("closes a notification", function() {

			var notificationsModel;
			inject(function (_$timeout_, NotificationsModel) {
				notificationsModel = NotificationsModel.getInstance({
					notificationsEndpoint: "/my/notifications/end/point",
					closeNotificationEndpoint: "/my/close/$(id)"
				});
			});

			$httpBackend.when('GET', '/my/notifications/end/point').respond(testNotifications);
			notificationsModel.loadNotifications();
			$httpBackend.flush();

			var closeNotificationService = httpService.getInstance({
				endpoint: "/my/close/$(id)",
				urlVars: {
					id: "n2"
				}
			});

			$httpBackend.expectPUT('/my/close/n2');
			$httpBackend.when('PUT', '/my/close/n2').respond({});

			expect(notificationsModel.notifications.length).toBe(3);
			notificationsModel.closeNotification(notificationsModel.notifications[1]);
			$timeout.flush();
			$httpBackend.flush();
			expect(notificationsModel.notifications.length).toBe(2);
			expect(notificationsModel.notifications[0].id).toBe("n1");
			expect(notificationsModel.notifications[1].id).toBe("n3");
		});
	});
});