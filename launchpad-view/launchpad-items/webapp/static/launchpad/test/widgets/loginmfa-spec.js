define([
    'angular',
    'launchpad/lib/common',
    'launchpad/widgets/login-multifactor/js/main',
    'angular-mocks'], function(angular)  {

    describe('Login Multifactor', function() {

        var ctrl;
        var mockMessages = {
            messages: {
                'SERVICE_ERROR' : 'Sorry, an unexpected error has occurred',
                'MISSING_PASSWORD' : 'Please fill in your password.'
            }
        };

        var $rootScope, $httpBackend, mockI18n;


        var mockWidget = {
            getPreference: function (key) {
                var preferences = {
                    sessionUrl: '/example/initiate',
                    messageSrc: '/my/messages'
                }
                return preferences[key];
            },
            addEventListener: function(callback) {

            }
        };

        var sessionIntiateResponse = {
            'session':{
                'id':'805961f1-9e05-4489-930c-d26fdf7145dc',
                'status':'Initiated'
            },
            'next':{
                'rel':'verifyOTP',
                'actions':[
                    {
                        'rel':'verify',
                        'uri':'/authentication/session/{id}/verifyOTP'
                    }
                ]
            }
        };


        beforeEach(function() {
            module('launchpad.loginmfa', function($provide) {
                $provide.value('widget', mockWidget);
            });
        });
        beforeEach(module('common'));

        beforeEach(inject(function (_$rootScope_, _$httpBackend_, $q) {
            $rootScope = _$rootScope_;
            $httpBackend = _$httpBackend_;

            // $httpBackend.flush();

            // mockI18n = {
            //     loadMessages: function(widget, locale) {
            //         var xhr = $.Deferred();
            //         xhr.resolve(mockMessages, 200);
            //         return xhr;
            //     }
            // };

            // http://localhost:7777/portalserver/static/launchpad/conf/messages.json

        }));

        describe('', function() {
            beforeEach(inject(function($controller) {
                sessionStorage.clear(); // Do not allow cache http calls
                ctrl = $controller('LoginmfaCtrl', { $scope: $rootScope });
            }));

            describe('main controller', function () {

                it('has master object', function() {
                    expect($rootScope.master).toBeDefined();
                });

                describe('has been initialized correctly and', function () {
                    it('is not loading', function() {
                        expect($rootScope.master.loading).toBe(false);
                    });

                    it('has no alerts', function() {
                        expect($rootScope.master.alerts).toEqual([]);
                    });

                    it('shows the initial template', function() {
                        expect($rootScope.master.template).not.toBe(null);
                    });

                    it('has no session id initiated', function() {
                        expect($rootScope.sessionId).toBe(null);
                    });

                    it('has only one valid step option', function() {
                        expect($rootScope.steps).toEqual({
                            'login': {
                                actions: [{
                                    rel: 'initiate',
                                    uri: '/example/initiate'
                                }]
                            }
                        });
                    });
                });


                describe('alerts', function() {
                    beforeEach(inject(function($controller) {
                        $httpBackend.expectGET('/my/messages');
                        $httpBackend.when('GET', '/my/messages').respond(mockMessages);
                        $httpBackend.flush();

                    }));

                    it('can be added by message code and style', function() {
                        $rootScope.addAlert('MISSING_PASSWORD', 'error');
                        expect($rootScope.master.alerts).toEqual([
                            {
                                type: 'error',
                                msg: 'Please fill in your password.'
                            }
                        ]);

                        $rootScope.addAlert('SERVICE_ERROR', 'warning');
                        expect($rootScope.master.alerts.length).toBe(2);
                        expect($rootScope.master.alerts[1]).toEqual({
                            type: 'warning',
                            msg: 'Sorry, an unexpected error has occurred'
                        });
                    });

                    it('will fallback if message code is unkwown', function() {
                        $rootScope.addAlert('UNKNOWN_CODE', 'error');
                        expect($rootScope.master.alerts).toEqual([
                            {
                                type: 'error',
                                msg: 'Sorry, an unexpected error has occurred'
                            }
                        ]);
                    });

                    it('will default to `error` if type is missing', function() {
                        $rootScope.addAlert('MISSING_PASSWORD');
                            expect($rootScope.master.alerts).toEqual([
                            {
                                type: 'warning',
                                msg: 'Please fill in your password.'
                            }
                        ]);
                    });

                    it('can be cleared', function() {
                        $rootScope.addAlert('MISSING_PASSWORD', 'error');
                        $rootScope.addAlert('SERVICE_ERROR', 'warning');
                        expect($rootScope.master.alerts.length).toBe(2);

                        $rootScope.clearAlerts();
                        expect($rootScope.master.alerts.length).toBe(0);
                    });
                });

                /*describe('connect', function() {
                    var service;
                    beforeEach(inject(function(LoginmfaApi, $httpBackend) {
                        service = LoginmfaApi;

                        $httpBackend.when('POST', '/services/rest/example/initiate').respond(sessionIntiateResponse);
                        $httpBackend.expectPOST('/services/rest/example/initiate');

                        spyOn(service, 'connect').andCallThrough();
                    }));

                    function sendInitiateRequest(flush) {
                        $rootScope.connect('initiate', {});
                        if (flush) {
                            $httpBackend.flush();
                        }
                    }

                    afterEach(function() {
                        $httpBackend.verifyNoOutstandingExpectation();
                        $httpBackend.verifyNoOutstandingRequest();
                    });

                    it('should make a call to service when tries to process a step', function() {
                        sendInitiateRequest(true);
                        expect(service.connect).toHaveBeenCalled();
                    });

                    it('enable / disable loading appropriatelly', function() {
                        expect($rootScope.master.loading).toBe(false);

                        sendInitiateRequest(false);
                        expect($rootScope.master.loading).toBe(true);

                        $httpBackend.flush();
                        expect($rootScope.master.loading).toBe(false);
                    });

                    it('shoul move to next step', function() {
                        sendInitiateRequest(true);
                        expect($rootScope.steps.verifyOTP).toBeDefined();
                        expect($rootScope.master.template).toBe('/static/launchpad/widgets/login-multifactor/steps/verifyOTP.html');
                    });

                    it('should `store` the session id', function() {
                        sendInitiateRequest(true);
                        expect($rootScope.sessionId).toBe('805961f1-9e05-4489-930c-d26fdf7145dc');
                    });
                });*/

                describe('moveToStep', function() {
                    beforeEach(function() {
                        $rootScope.steps = {
                            'login': {
                                actions: [{
                                    rel: 'initiate',
                                    uri: '/example/initiate'
                                }]
                            },
                            'verifyOTP': {
                                actions: [{
                                    rel:'verify',
                                    uri:'/authentication/session/{id}/verifyOTP'
                                }]
                            }
                        };
                    });

                    it('should add the step & actions when moving on new one', function() {
                        expect($rootScope.steps.test).toBeUndefined();
                        $rootScope.moveToStep('test', [{}, {}, {}]);
                        expect($rootScope.steps.test).toBeDefined();
                        expect($rootScope.steps.test.actions.length).toBe(3);
                    });

                    it('should change the template when step changes', function() {
                        expect($rootScope.master.template.indexOf('test')).toBe(-1);
                        $rootScope.moveToStep('test');
                        expect($rootScope.master.template.indexOf('test')).not.toBe(-1);
                    });

                    it('should clear the session id when moving to initial step', function() {
                        $rootScope.sessionId = 'A_SESSION_ID';
                        $rootScope.moveToStep('login');
                        expect($rootScope.sessionId).toBe(null);
                    });

                    it('should clear alerts when changin step', function() {
                        $rootScope.master.alerts = [{}, {}, {}, {}];
                        expect($rootScope.master.alerts.length).toBe(4);
                        $rootScope.moveToStep('any_step');
                        expect($rootScope.master.alerts.length).toBe(0);
                    });

                });
            });
        });

    });
});
