define(["jquery",
    "launchpad/test/mock/mock-portal-client",
    "launchpad/pages/launchpad-page/modules/session-timeout",
    "jquery.mockjax" ], function($, b$, sessionTimeout) {

    describe("is handling a session timeout and", function() {
        var timeRemaing = 0;
        var timeoutResponse = false;
        var config = {
            sessionEndpoint: "/session",
            sessionValidateEndpoint: "/session/vaidate"
        };

        var st;

        var getTimeRemaing = function() {

            if(timeoutResponse) {
                this.statusText = "timeout";
                this.isTimeout = true;
            } else {
                this.responseText = { remainingTime: timeRemaing };
                this.status = timeRemaing > 0 ? 200 : 401;
            }
        };

        $.mockjax({
            url: "/session/validate",
            type: "GET",
            response: getTimeRemaing
        });

        $.mockjax({
            url: "/session",
            type: "PUT",
            response: getTimeRemaing
        });

        var mockPage = new b$.portal.mock.Item();

        beforeEach(function() {

            st = sessionTimeout.getInstance();

            timeoutResponse = false;
            timeRemaing = 7;
            jasmine.Clock.useMock();

            spyOn(b$.portal.portalView, "getElementsByTagName").andCallFake(function() {
                return [ mockPage ];
            });
            spyOn(mockPage, "getPreference").andCallFake(function(pref) {
                if(pref === "enableTimeout") {
                    return true;
                } else if(pref === "startCountdownAt")  {
                    return 4000
                } else if(pref === "sessionCheckInterval") {
                    return 2000;
                } else if(pref === "sessionEndpoint") {
                    return "/session";
                } else if(pref === "sessionValidateEndpoint") {
                    return "/session/validate";
                } else if(pref === "logoutUrl") {
                    return "#logout";
                }
                return null;
            });

            spyOn(st, "publishMessage").andCallFake(function(id, data) {
                gadgets.pubsub.publish.mostRecentCall.args = [];
                gadgets.pubsub.publish.mostRecentCall.args.push(id);
                gadgets.pubsub.publish.mostRecentCall.args.push(data);

            });

            spyOn(window.location, "replace").andCallFake(function() {});

            window.location.hash = "";
        });



        it("is starting the countdown then logging out after the session times out", function() {
            st.initializeSessionTimeoutHandling();

            timeRemaing -= 3;
            jasmine.Clock.tick(3001); //4 seconds left

            expect(gadgets.pubsub.publish.mostRecentCall.args[0]).toEqual("launchpad.add-notification");
            expect(gadgets.pubsub.publish.mostRecentCall.args[1].notification.id).toEqual("session-timeout");
            expect(gadgets.pubsub.publish.mostRecentCall.args[1].notification.timeLeft).toBe(4);

            timeRemaing -= 3;
            jasmine.Clock.tick(3001); //1 seconds left

            expect(window.location.hash).not.toBe("#logout");
            timeRemaing -= 1;
            jasmine.Clock.tick(1001); //0 seconds left

            expect(window.location.hash).toBe("#logout");
        });

        it("is never timing out if there has been user interaction", function() {


            st.initializeSessionTimeoutHandling();

            for(var i = 0; i < 10; i++) {

                jasmine.Clock.tick(1001);
                $(document).trigger("click");
                timeRemaing = 7;
            }
            expect(window.location.hash).not.toBe("#logout");
        });

        it("is clearing the warning after the countdown begins if there has been user interaction", function() {
            st.initializeSessionTimeoutHandling();

            timeRemaing -= 4;
            jasmine.Clock.tick(4001); //3 seconds left

            expect(gadgets.pubsub.publish.mostRecentCall.args[0]).toEqual("launchpad.add-notification");
            expect(gadgets.pubsub.publish.mostRecentCall.args[1].notification.id).toEqual("session-timeout");

            timeRemaing = 7;
            $(document).trigger("click");

            jasmine.Clock.tick(1001);
            /*expect(gadgets.pubsub.publish.mostRecentCall.args[0]).toEqual("launchpad.remove-notification");
            expect(gadgets.pubsub.publish.mostRecentCall.args[1].notification.id).toEqual("session-timeout");*/
        });

        it("is sending a warning notification if there a subsequent error responses/timeouts from the server", function() {
            st.initializeSessionTimeoutHandling();

            timeoutResponse = true;
            jasmine.Clock.tick(6001);
            expect(gadgets.pubsub.publish.mostRecentCall.args[0]).toEqual("launchpad.add-notification");
            expect(gadgets.pubsub.publish.mostRecentCall.args[1].notification.id).toEqual("offline-warning");

        });
    });
});