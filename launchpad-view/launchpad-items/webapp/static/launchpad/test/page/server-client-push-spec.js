define(["jquery",
    "launchpad/pages/launchpad-page/modules/server-client-push",
    "jquery.mockjax" ], function($, serverClientPushController) {

    describe("is managing subscribing and publishing to the server", function() {
        var config = {
            url: "/event",
            onMessage: function() {}
        };

        var scpc;

        beforeEach(function() {

            scpc = serverClientPushController.getInstance();

            spyOn(scpc.socket, "subscribe").andCallFake(function(request) {
                return { request: request};
            });
        });

        it("creating a new subscription request object", function() {
            var request = scpc.createNewRequest(config);

            expect(request.url).toBe("/portalserver/event");
            expect(typeof request.onMessage).toBe("function");
            expect(request.logLevel).toBe("info");
            expect(request.contentType).toBe("application/json");
            expect(request.transport).toBe("websocket");
            expect(request.fallbackTransport).toBe("long-polling");
            expect(request.timeout).toBe(300000);

        });

        it("is creating and registering a request seperately and setting logLevel to debug", function() {
            config.logLevel = "debug";
            var request = scpc.createNewRequest(config);

            scpc.registerNewRequest(request);

            expect(scpc.socket.subscribe).toHaveBeenCalled();
            expect(scpc.registeredRequests.length).toBe(1);
            expect(scpc.registeredRequests[0].request.logLevel).toBe("debug");

        });

        it("is creating and registering a sub request in the same function, with url set to /event2", function() {

            scpc.createAndRegisterNewRequest("/event2", function() {}, {});

            expect(scpc.socket.subscribe).toHaveBeenCalled();
            expect(scpc.registeredRequests.length).toBe(1);
            expect(scpc.registeredRequests[0].request.url).toBe("/portalserver/event2");
        });

        it("is adding to registeredRequests, checking it's length, clearing, and checking again", function() {

            scpc.createAndRegisterNewRequest("/event1", function() {}, {});
            scpc.createAndRegisterNewRequest("/event2", function() {}, {});
            scpc.createAndRegisterNewRequest("/event3", function() {}, {});

            expect(scpc.socket.subscribe).toHaveBeenCalled();
            expect(scpc.registeredRequests.length).toBe(3);

            scpc.clearRegisteredRequests();

            expect(scpc.registeredRequests.length).toBe(0);

        });

        it("publishes a mock message from the server and executes a requests callback", function() {

            var request = scpc.createNewRequest(config);

            spyOn(request, "onMessage");

            scpc.registerNewRequest(request);
            scpc.publishServerMessage("/event");

            expect(scpc.socket.subscribe).toHaveBeenCalled();
            expect(request.onMessage).toHaveBeenCalled();
        });
    });
});