//mock out the portal client structure and use jasmine to spy on method calls and return desired values
window.b$ = {};

window.b$.mixin = function() {};

window.b$.portal = {};
window.b$.portal.config = {};
window.b$.portal.config.serverRoot = "/portalserver";
window.b$.portal.portalView = {};
window.b$.portal.portalView.getElementsByTagName = function() {};

window.b$.portal.mock = {};
window.b$.portal.mock.Item = function() {};
window.b$.portal.mock.Item.prototype.getPreference = function() {};
window.b$.portal.mock.Item.prototype.buildDisplay = function() {};

window.b$.bdom = {};
window.b$.bdom.getNamespace = function() {
	return mockNS;
};

var mockNS = {};
mockNS.getClass = function() {
	return window.b$.portal.mock.Item;
};

window.gadgets = {};
window.gadgets.pubsub = {};
window.gadgets.pubsub.publish = function(id, data) {};
window.gadgets.pubsub.subscribe = function(id) {};
window.gadgets.pubsub.publish.mostRecentCall = {};
window.gadgets.pubsub.publish.mostRecentCall.args = [];
