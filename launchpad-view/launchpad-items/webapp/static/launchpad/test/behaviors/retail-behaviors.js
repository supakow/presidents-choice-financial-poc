define([
	"launchpad/behaviors/retail-behaviors",
	"launchpad/test/mock/mock-portal-client"
	], function(retailBehaviors) {

	describe("Retail behaviors", function() {

		var initMappings = retailBehaviors.behaviors.DOMNodeInsertedIntoDocument;

		var mockContainer = {
			id: "Container"
		};
		var mockChild = {
			id: "Widget"
		};

		it("ignore DOMNodeInsertedIntoDocument events propogated from children", function() {

			//initialize the mappings with an event who's target is not equal to the container and check that
			//there were no pubsub subscriptions

			var mockEvent = {
				target: mockChild
			};

			var subscribeCount = 0;
			spyOn(gadgets.pubsub, "subscribe").andCallFake(function() {
				subscribeCount++;
			});

			initMappings.call(mockContainer, mockEvent);

			expect(subscribeCount).toBe(0);
		});

		it("ignore items which do not implement the behavior loading interface", function() {

			//initalize mappings with a container that does not have any loadBy* methods
			//expect a warning message on the console

			var mockEvent = {
				target: mockContainer
			};

			var subscribeCount = 0;
			spyOn(gadgets.pubsub, "subscribe").andCallFake(function() {
				subscribeCount++;
			});
			spyOn(console, "warn");
			initMappings.call(mockContainer, mockEvent);

			expect(subscribeCount).toBe(0);
			expect(console.warn).toHaveBeenCalled();

		});

		it("map retail events to container actions", function() {

			//emulate normal behavior (mocking the pubsub)

			var mockEvent = {
				target: mockContainer
			};
			mockContainer.loadByBehavior = function() {};
			mockContainer.showByBehavior = function() {};

			var subscribeCallbacks = [];

			spyOn(mockContainer, "loadByBehavior");
			spyOn(mockContainer, "showByBehavior");
			spyOn(gadgets.pubsub, "subscribe").andCallFake(function(channel, fn) {

				if(channel === "launchpad-retail.accountSelected") {
					subscribeCallbacks[0] = fn;
				} else if(channel === "launchpad-retail.requestMoneyTransfer") {
					subscribeCallbacks[1] = fn;
				}
			});

			initMappings.call(mockContainer, mockEvent);

			subscribeCallbacks[0].call();
			expect(mockContainer.loadByBehavior).toHaveBeenCalledWith("transactions", jasmine.any(Function));

			subscribeCallbacks[1].call();
			expect(mockContainer.loadByBehavior).toHaveBeenCalledWith("new-transfer", jasmine.any(Function));
		});
	});
});