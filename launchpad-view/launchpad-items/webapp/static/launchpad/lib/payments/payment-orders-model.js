define("launchpad/lib/payments/payment-orders-model", [
	"jquery",
    "angular",
    "launchpad/lib/common/util",
    "launchpad/lib/payments/payments-module"], function($, angular, util, paymentsModule) {

    "use strict";

    paymentsModule.factory("PaymentOrdersModel", [ '$rootScope', 'customerId', 'pendingPaymentOrdersTimeout', 'httpService',
	    function($rootScope, customerId, pendingPaymentOrdersTimeout, httpService) {

        /**
         * Transactions service constructor
         * @param config
         * @constructor
         */
        var PaymentOrdersModel = function(config) {

            if (config.paymentOrdersEndpoint) {
                this.paymentOrdersEndpoint = config.paymentOrdersEndpoint;
            }
            if (config.pendingPaymentOrdersEndpoint) {

                this.pendingPaymentOrdersService = httpService.getInstance({
                    endpoint: config.pendingPaymentOrdersEndpoint,
                    urlVars: {
                        partyId: customerId
                    },
                    cacheTimeout: pendingPaymentOrdersTimeout
                });
            }
            if (config.paymentOrdersSubmitPoint) {
                this.paymentOrdersSubmitPoint = config.paymentOrdersSubmitPoint;
            }

            this.locale = config.locale;
            this.pendingOrdersGroups = null;
            this.createError = null;
            this.loadError = null;
            this.submitError = null;
        };

        /**
         * Create new order
         *
         * @param paymentOrder
         * @param uuid
         * @returns {*}
         */
        PaymentOrdersModel.prototype.create = function (paymentOrder, orderId) {
            var self = this,
                service = httpService.getInstance({
                    endpoint: this.paymentOrdersEndpoint,
                    urlVars: {
                        orderId: orderId
                    }
                }),
                xhr = service.create({
                    data: paymentOrder
                });

            xhr.error(function(data){
	            if(data.errors) {
		            self.createError = data.errors[0].code;
	            }
            });

            return xhr;
        };

        /**
         * Load pending orders
         * @returns {*}
         */
        PaymentOrdersModel.prototype.load = function (force) {
            var self = this,
                xhr = this.pendingPaymentOrdersService.read({}, force);

            xhr.success(function(data){
                if(data && data !== 'null') {
                    self.preprocessPendingOrdersGroups(data);
                    self.pendingOrdersGroups = data;
                } else {
                    // TODO: should return empty array
                    self.pendingOrdersGroups = [];
                }
            });
            xhr.error(function(data){
                self.submitError = data.errorCode || 500;
            });

            return xhr;
        };

        /**
         * Submit pending order.
         *
         * @param uuid
         * @returns {*}
         */
        PaymentOrdersModel.prototype.submit = function (orderId, authorization) {
            var self = this,
                service = httpService.getInstance({
                    endpoint: this.paymentOrdersSubmitPoint,
                    urlVars: {
                        orderId: orderId
                    }
                }),
                data = {};

            if (authorization) {
                data.auth_password = authorization;
            }

            var xhr = service.update({
                data: data
            });

            xhr.error(function(data){
                self.submitError = data.errorCode || 500;
            });

            return xhr;
        };


        /**
         * Adds account details field to payment order in order to present it on front end
         * @param data - list of payment order groups from server
         */
        PaymentOrdersModel.prototype.preprocessPendingOrdersGroups = function(data) {
            var poGroup;
            var po;

            for(var i = 0; i < data.length; i++) {
                poGroup = data[i];

                for(var j = 0; j < poGroup.paymentOrders.length; j++) {
                    po = poGroup.paymentOrders[j];

                    if(po.counterpartyIban && po.counterpartyIban.length > 0) {
                        po.accountDetails = po.counterpartyIban;
                    } else if(po.counterpartyAccount && po.counterpartyAccount.length > 0) {
                        po.accountDetails = po.counterpartyAccount;
                    } else if(po.counterpartyEmail && po.counterpartyEmail.length > 0) {
                        po.accountDetails = po.counterpartyEmail;
                    }
                }
            }
        };

        return PaymentOrdersModel;
    }]);
});
