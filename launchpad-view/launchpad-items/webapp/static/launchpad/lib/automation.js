define([
    'launchpad/lib/automation/automation-module',
    'launchpad/lib/automation/automation-model'
], function(uiModule) {
    'use strict';

    return uiModule;
});
