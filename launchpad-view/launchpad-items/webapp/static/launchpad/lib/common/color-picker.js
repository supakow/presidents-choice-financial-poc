/*global gadgets, console*/
define('launchpad/lib/common/color-picker', [
    'angular',
    'jquery',
    'launchpad/lib/common/common-module'
], function(angular, $, module) {

    'use strict';

    module.directive('lpColorPicker', ['$templateCache', '$timeout', function($templateCache, $timeout) {

        $templateCache.put('$colorPicker.html',
            '<ul class="color-picker clearfix">' +
                '<li ng-repeat="color in colors" tabIndex="0" class="color cursor-pointer" title="{{color.name}}" ng-click="selectColor(color.value)" ng-keydown="keyDown($event, color.value)">' +
                    '<div ng-style="{\'background-color\':color.value}"></div>' +
                '</li>' +
            '</ul>'
        );

        return {
            restrict : 'A',
            replace: true,
            scope: {
                selectColor: '='
            },
            template: $templateCache.get('$colorPicker.html'),
            link: function (scope, element, attrs) {

                scope.colors = [
                    { name: 'Cantaloupe', value: '#FFD479' },
                    { name: 'Honeydew', value: '#D4FB79' },
                    { name: 'Spindrift', value: '#73FCD6' },
                    { name: 'Sky', value: '#76D6FF' },
                    { name: 'Lavender', value: '#D783FF' },
                    { name: 'Carnation', value: '#FF8AD8' },
                    { name: 'Licorice', value: '#000000' },
                    { name: 'Snow', value: '#FFFFFF' },
                    { name: 'Salmon', value: '#FF7E79' },
                    { name: 'Banana', value: '#FFFC79' },
                    { name: 'Flora', value: '#73FA79' },
                    { name: 'Ice', value: '#73FDFF' },
                    { name: 'Orchid', value: '#7A81FF' },
                    { name: 'Bubblegum', value: '#FF85FF' },
                    { name: 'Lead', value: '#212121' },
                    { name: 'Mercury', value: '#EBEBEB' },
                    { name: 'Tangerine', value: '#FF9300' },
                    { name: 'Lime', value: '#8EFA00' },
                    { name: 'Sea Foam', value: '#00FA92' },
                    { name: 'Aqua', value: '#0096FF' },
                    { name: 'Grape', value: '#9437FF' },
                    { name: 'Strawberry', value: '#FF2F92' },
                    { name: 'Tungsten', value: '#424242' },
                    { name: 'Silver', value: '#D6D6D6' },
                    { name: 'Maraschino', value: '#FF2600' },
                    { name: 'Lemon', value: '#FFFB00' },
                    { name: 'Spring', value: '#00F900' },
                    { name: 'Turquoise', value: '#00FDFF' },
                    { name: 'Blueberry', value: '#0433FF' },
                    { name: 'Iron', value: '#5E5E5E' },
                    { name: 'Magnesium', value: '#C0C0C0' },
                    { name: 'Mocha', value: '#945200' },
                    { name: 'Fern', value: '#4F8F00' }
                ];

                // focus first color
                $timeout(function() {
                    element.find('li')[0].focus();
                }, 0);

                scope.keyDown = function(event, color) {
                    if (event.which === 13 || event.which === 32) {
                        event.preventDefault();
                        event.stopPropagation();
                        
                        scope.selectColor(color);
                    }
                };

            }
        };

    }]);
});
