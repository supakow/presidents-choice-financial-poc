define('launchpad/lib/common/profile-detail-service', [
    'angular',
    'jquery',
    'launchpad/lib/common/common-module',
    'launchpad/lib/common/util',
    'launchpad/lib/i18n',
    'launchpad/lib/user'
], function(angular, $, module, util) {

    'use strict';

    module.service('ProfileDetailsService', ['$http', 'LoginService', 'dateFilter', function ($http, LoginService, dateFilter) {

        // Get stored data
        this.restoreUsername = function () {
            var storedData = LoginService.getStoredData(),
                data = JSON.parse(storedData || '{}');
            return data.username;
        };

        // Return data in correct key, value format
        this.formatResponse = function (response, messages) {
            var data = {
                fullname: [response.firstName, response.lastName].join(' '),
                details: [],
                activities: []
            };

            // Details
            if (response.dateOfBirth) {
                data.details.push({ key: 'Birth date', value: dateFilter(response.dateOfBirth) });
            }
            if (response.gender) {
                data.details.push({ key: 'Gender', value: messages && messages[response.gender] ? messages[response.gender] : response.gender });
            }

            angular.forEach(response.details, function (value, key) {
                var label = messages && messages[key] ? messages[key] : key;

                if (key === 'dateOfBirth') {
                    value = dateFilter(value);
                } else {
                    value = messages && messages[value] ? messages[value] : value;
                }
                data.details.push({ key: label, value: value });
            });

            // Activities
            if (response.activities) {
                if (response.activities.lastLoggedIn) {
                    data.activities.push({ key: 'Last Logged In', value: dateFilter(response.activities.lastLoggedIn, 'medium') });
                }
            }

            return data;
        };

        this.getData = function () {
            var username = this.restoreUsername();
            var url = util.getContextPath() + '/services/rest/party-data-management/party';

            return $http.get(url, null, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/x-www-form-urlencoded;'
                }
            });
        };
    }]);
});