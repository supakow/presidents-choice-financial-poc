define("launchpad/lib/common/common-module",[
	"angular"
	], function(angular) {
		"use strict";

		var module = angular.module("common", []);

		return module;
});
