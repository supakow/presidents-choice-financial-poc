define('launchpad/lib/common/preference-service', [
    'angular',
    'jquery',
    'launchpad/lib/common/common-module',
    'launchpad/lib/common/util'
], function(angular, $, module, util) {

    'use strict';

    module.service('PreferenceService', ['$http', 'widget', function($http, widget) {
        var url = widget.getPreference('preferenceService');
        url = util.replaceUrlVars(url, {
            contextPath: util.getContextPath()
        });
        
        this.read = function() {
            //prevents caching behavior in IE8
            var rand = Math.random() + '';
            var num = rand * 1000000000000000000;
            
            return $http.get(url + '?qi=' + num, null, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/x-www-form-urlencoded;'
                }
            });
        };

        this.save = function(field, value) {
            var data = {};
            data[field] = value;

            return $http.put(url, $.param(data), {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/x-www-form-urlencoded;'
                }
            });
        };
    }]);

});