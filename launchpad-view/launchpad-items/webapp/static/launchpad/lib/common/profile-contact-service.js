define('launchpad/lib/common/profile-contact-service', [
    'angular',
    'jquery',
    'launchpad/lib/common/common-module',
    'launchpad/lib/common/util'
], function(angular, $, module, util) {

    'use strict';

    module.service('ProfileContactService', ['$http', 'widget', function($http, widget) {

        var url = util.getContextPath() + widget.getPreference('saveUrl');

        this.read = function() {
            return $http.get(url);
        };

        this.save = function(field, value) {
            var data = {};
            data[field] = value;

            return $http.put(url, $.param(data), {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/x-www-form-urlencoded;'
                }
            });
        };
    }]);

});