define([
	"angular",
	"launchpad/lib/charts/charts-module",
	"launchpad/lib/charts/bar-chart",
	"launchpad/lib/charts/line-chart"
	], function(angular, chartsModule) {
		"use strict";

		return chartsModule;
});
