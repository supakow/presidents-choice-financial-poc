define([
    "launchpad/lib/payments/payments-module",
    "launchpad/lib/payments/currency-amount-input",
    "launchpad/lib/payments/currency-input",
    "launchpad/lib/payments/payment-orders-model",
    "launchpad/lib/payments/payment-ref-description",
    "launchpad/lib/payments/scheduled-transfer",
    "launchpad/lib/payments/iban-input",
    "launchpad/lib/payments/us-account-input",
    "launchpad/lib/payments/email-transfer",
    "launchpad/lib/payments/address-transfer",
    "launchpad/lib/payments/p2p-enrollment",
    "launchpad/lib/payments/counter-party-filter"
], function(paymentsModule) {
    "use strict";

    return paymentsModule;
});