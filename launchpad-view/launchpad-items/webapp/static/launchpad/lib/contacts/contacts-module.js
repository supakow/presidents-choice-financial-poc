define("launchpad/lib/contacts/contacts-module",[
	"angular",
	"launchpad/lib/common"
	], function(angular) {
		"use strict";

		var module = angular.module("contacts", ["common"]);

		return module;
});
