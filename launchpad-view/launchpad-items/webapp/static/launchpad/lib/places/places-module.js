define("launchpad/lib/places/places-module",[
	"angular"
	], function(angular) {
		"use strict";

		var module = angular.module("launchpad-foundation.places", ["ui.bootstrap", "AngularGM", "common", 'ui']);

		return module;
});
