define([
	"launchpad/lib/contacts/contacts-module",
	"launchpad/lib/contacts/contacts-model"
	], function(contactsModule) {
		"use strict";

		return contactsModule;
});