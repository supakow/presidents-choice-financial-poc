define([
	"launchpad/lib/i18n/i18n-module"
	], function(i18nModule) {
		"use strict";

		return i18nModule;
});
