define([
	"launchpad/lib/transactions/transactions-module",
	"launchpad/lib/transactions/transactions-model",
	"launchpad/lib/transactions/transactions-chart-model",
	"launchpad/lib/transactions/balance-update",
	"launchpad/lib/transactions/currency-model"
	], function(uiModule) {
		"use strict";

		return uiModule;
});
