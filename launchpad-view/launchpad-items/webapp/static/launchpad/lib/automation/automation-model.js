/*global console*/
define('launchpad/lib/automation/automation-model', [
	'jquery',
	'angular',
	'launchpad/lib/common/util',
	'launchpad/lib/automation/automation-module'
], function($, angular, util, automationModule) {

	'use strict';

	automationModule.factory('AutomationModel', ['$resource', function($resource) {

		/**
		 * Automation service constructor
		 * @param config
		 * @constructor
		 */
		var AutomationModel = function(config) {

			this.locale = config.locale;
			this.automationsEndpoint = config.automationsEndpoint;
			this.automationTriggersEndpoint = config.automationTriggersEndpoint;
			this.automationActionsEndpoint = config.automationActionsEndpoint;

			this.automations = [];
			this.automationsResource = $resource(this.automationsEndpoint, {automationId: '@id'}, {
				'update': { method: 'PUT' }
			});

			// mocking the automation triggers and actions
			this.actions = [
				{
					'id': 'action001',
					'name': 'Send email',
					'type': 'EMAIL',
					'available': true,
					'icon': '/portalserver/static/launchpad/widgets/automation/media/icon_mail.png',
					'fields': [
						{
							'name': 'emailAddress',
							'type': 'STRING'
						}
					]
				},
				{
					'id': 'action002',
					'name': 'Send SMS',
					'type': 'SMS',
					'available': false,
					'icon': '/portalserver/static/launchpad/widgets/automation/media/icon_sms.png',
					'fields': [
						{
							'name': 'phoneNumber',
							'type': 'STRING'
						}
					]
				},
				{
					'id': 'action003',
					'name': 'Post on Facebook',
					'type': 'ALT',
					'available': false,
					'icon': '/portalserver/static/launchpad/widgets/automation/media/icon_fb.png',
					'fields': []
				}
			];
			this.triggers = [
				{
					'id': 'trigger001',
					'name': 'Balance Level',
					'type': 'BALANCE_LEVEL',
					'available': false,
					'icon': '/portalserver/static/launchpad/widgets/automation/media/icon_balanceLevel.png',
					'fields': []
				},
				{
					'id': 'trigger002',
					'name': 'Transaction Amount',
					'type': 'TRANSACTION_AMOUNT',
					'available': true,
					'icon': '/portalserver/static/launchpad/widgets/automation/media/icon_transactionAmount.png',
					'fields': [
						{

						}
					],
					'actionIds': ['action001', 'action002', 'action003']
				},
				{
					'id': 'trigger003',
					'name': 'Transaction Contact',
					'type': 'TRANSACTION_CONTACT',
					'available': false,
					'icon': '/portalserver/static/launchpad/widgets/automation/media/icon_transactionContact.png',
					'fields': []
				},
				{
					'id': 'trigger004',
					'name': 'New E-Bill',
					'type': 'EBILL_NEW',
					'available': false,
					'icon': '/portalserver/static/launchpad/widgets/automation/media/icon_ebillNew.png',
					'fields': []
				},
				{
					'id': 'trigger005',
					'name': 'E-Bill Alert',
					'type': 'EBILL_ALERT',
					'available': false,
					'icon': '/portalserver/static/launchpad/widgets/automation/media/icon_ebillAlert.png',
					'fields': []
				},
				{
					'id': 'trigger006',
					'name': 'Goal Level',
					'type': 'GOAL_LEVEL',
					'available': false,
					'icon': '/portalserver/static/launchpad/widgets/automation/media/icon_goalLevel.png',
					'fields': []
				},
				{
					'id': 'trigger007',
					'name': 'Budget Level',
					'type': 'BUDGET_LEVEL',
					'available': false,
					'icon': '/portalserver/static/launchpad/widgets/automation/media/icon_goalLevel.png',
					'fields': []
				}
			];
			this.amountDirection = [
				{
					'name': 'Less than',
					'value': 'LESS'
				},
				{
					'name': 'Less than or Equal',
					'value': 'LESS_THAN_EQUAL'
				},
				{
					'name': 'Equal',
					'value': 'EQUAL'
				},
				{
					'name': 'More than or Equal',
					'value': 'MORE_THAN_EQUAL'
				},
				{
					'name': 'More than',
					'value': 'MORE'
				}
			];
		};

		/**
		 * Load automations
		 */
		AutomationModel.prototype.loadAutomations = function() {
			var self = this;

			self.automations = self.automationsResource.query(function(response) {
				for (var i = 0; i < self.automations.length; i++) {
					self.automations[i].lastExecution = new Date(self.automations[i].lastExecution);
				}
			});

			return self.automations.$promise;
		};

		/**
		 * Create new automation
		 * @param  {object} automation
		 * @return {promise}
		 */
		AutomationModel.prototype.create = function(automation) {
			var self = this;

			var newAutomation = self.automationsResource.save(automation, function(response) {
				self.automations.unshift(response);
			}, function() {
				console.log('save error');
			});

			return newAutomation.$promise;
		};

		/**
		 * Enable or disable the specified automation
		 * @param  {object} automation
		 * @return {promise} 
		 */
		AutomationModel.prototype.toggleStatus = function(automation) {
			var self = this;

			automation.enabled = !automation.enabled;
			return automation.$update(automation, function(response) {
				console.log('update done', response);
			}, function(response) {
				// on error, set the enabled value back
				automation.enabled = !automation.enabled;
			});
		};

		AutomationModel.prototype.loadDetails = function(automation) {
			var self = this;

			automation.loadingDetails = true;
			return automation.$get({ automationId: automation.id }, function(response) {
				delete automation.loadingDetails;
				automation.detailsLoaded = true;
			});
		};

		AutomationModel.prototype.update = function(automation) {
			var self = this;

			return automation.$update(function(response) {
				console.log('automation update', response);
			});
		};

		/**
		 * Delete an automation
		 * @param  {object} automation
		 * @return {promise}
		 */
		AutomationModel.prototype.remove = function(automation) {
			var self = this;

			return automation.$delete(automation, function() {
				for (var i = 0; i < self.automations.length; i++) {
					if (self.automations[i].id === automation.id) {
						self.automations.splice(i, 1);
					}
				}
			});
		};

		return  {
            getInstance: function(config) {
                return new AutomationModel(config);
            }
        };
	}]);
});
