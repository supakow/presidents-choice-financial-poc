define('launchpad/lib/automation/automation-module',[
	'angular',
	'launchpad/lib/common'
], function(angular) {

	'use strict';

	var module = angular.module('automation', ['ngResource', 'common', 'i18n', 'ui.bootstrap', 'ui']);

	return module;
});
