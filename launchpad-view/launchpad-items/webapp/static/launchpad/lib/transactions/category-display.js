/*global $, Hammer, gadgets, console */
define('launchpad/lib/transactions/category-display', [
    'angular',
    'jquery',
    'launchpad/lib/transactions/transactions-module'
], function(angular, $, transactionsModule) {

    'use strict';

    transactionsModule.directive('lpCategoryDisplay', ['$templateCache', function ($templateCache) {

        $templateCache.put('$categoryDisplay.html',
            '<div class="lp-transaction-category" ng-click="categoryClick($event, transaction)">' +
                '<span class="category-marker" ng-style="markerStyle"></span>' +
                '<div class="category-name"><span class="h4">{{category.name}}</span></div>' +
            '</div>'
        );

        return {
            restrict : 'A',
            replace: true,
            require: 'ngModel',
            scope: {
                isCategoryView: '=lpCategoryView',
                categoryList: '=lpCategoryList',
                transaction: '=ngModel',
                categoryClick: '='
            },
            template: $templateCache.get('$categoryDisplay.html'),
            link: function (scope, element, attrs) {
                
                var marker = element.find('span')[0],
                    transactionRow = element.parent().parent(),
                    dragStartWidth = 0;
                scope.markerStyle = {};
                scope.category = null;

                if (!scope.isCategoryView && typeof Hammer !== 'undefined') {
                    var swipeHammer = new Hammer(transactionRow[0], {
                        dragBlockHorizontal: true
                    });
                    swipeHammer.on('dragstart', function (event) {
                        event.stopPropagation();
                        event.preventDefault();
                        dragStartWidth = parseInt(element.css('width'), 10);
                    });
                    swipeHammer.on('drag', function (event) {
                        event.stopPropagation();
                        event.preventDefault();
                        event.gesture.stopPropagation();

                        element.parent().addClass('no-animation');
                        var newWidth = dragStartWidth + Math.floor(event.gesture.deltaX);
                        if (newWidth > 160) {
                            newWidth = 160;
                        }
                        if (newWidth > dragStartWidth) {
                            element.parent().css('width', newWidth + 'px');
                        }
                    });
                    swipeHammer.on('dragend', function (event) {
                        event.stopPropagation();
                        event.preventDefault();

                        element.parent().removeClass('no-animation');
                        var newWidth = dragStartWidth + Math.floor(event.gesture.deltaX);
                        if (newWidth > 160) {
                            newWidth = 160;
                        }
                        if (newWidth > 150) {
                            if (scope.categoryClick && typeof scope.categoryClick === 'function') {
                                scope.categoryClick.apply(this, [null, scope.transaction]);
                            }
                        }
                        element.parent().css('width', '');
                    });
                }

                scope.$watch('transaction.categoryId', function(value) {
                    scope.setCategory(value);
                });

                scope.setCategory = function (id) {
                    if (scope.categoryList) {
                        for (var i = 0; i < scope.categoryList.length; i++) {
                            if (scope.categoryList[i].id === id) {
                                scope.category = scope.categoryList[i];
                            }
                        }
                    }

                    if (scope.category && id) {
                        scope.markerStyle.backgroundColor = scope.category.color;
                    } else {
                        // temporary fix to set 'Uncategorised' if transaction category id isn't valid
                        scope.transaction.categoryId = '00cc9919-ba0c-4702-917b-1fba4c256b4d';
                    }
                };

                gadgets.pubsub.subscribe('launchpad-retail.categoryDelete', function(data) {
                    if (data.id === scope.transaction.categoryId) {
                        scope.setCategory();
                    }
                });
            }
        };
    }]);
});