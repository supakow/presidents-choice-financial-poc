define("launchpad/lib/transactions/transactions-module",[
	"angular",
	"launchpad/lib/common"
	], function(angular) {
		"use strict";

		var module = angular.module("transactions", ["common"]);

		return module;
});
