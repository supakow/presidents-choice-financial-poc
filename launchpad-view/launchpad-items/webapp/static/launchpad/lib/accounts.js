/*!
 * Backbase Launchpad Accounts Utilities
 */
define([
	"launchpad/lib/accounts/accounts-module",
	"launchpad/lib/accounts/accounts-chart-model",
	"launchpad/lib/accounts/accounts-model",
	"launchpad/lib/accounts/accounts-select"
	], function(accountsModule) {
		"use strict";

		return accountsModule;
});
