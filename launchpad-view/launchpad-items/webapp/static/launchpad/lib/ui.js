define([
	"launchpad/lib/ui/ui-module",
	"launchpad/lib/ui/number-input",
	"launchpad/lib/ui/focus",
	"launchpad/lib/ui/aria",
	"launchpad/lib/ui/responsive-directive",
	"launchpad/lib/ui/smartsuggest-engine",
	"launchpad/lib/ui/smartsuggest-formatter",
    "launchpad/lib/ui/wizard",
	"launchpad/lib/ui/lp-field",
	"launchpad/lib/ui/lp-radio",
	"launchpad/lib/ui/widget-modal",
    "launchpad/lib/ui/input-overflow",
    "launchpad/lib/ui/widget-calendar",
	"launchpad/lib/ui/nav-icon",
	"launchpad/lib/ui/scrolling-hook"
	], function(uiModule) {

		"use strict";

		return uiModule;
});