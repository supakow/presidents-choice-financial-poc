define([
	"launchpad/lib/common/common-module",
	"launchpad/lib/common/rest-services",
	"launchpad/lib/common/rest-client",
    "launchpad/lib/common/form-data-persistence",
    "launchpad/lib/common/profile-detail-service",
    "launchpad/lib/common/p2p-service",
    "launchpad/lib/common/profile-contact-service"
	], function(commonModule) {
		"use strict";

		return commonModule;
});
