define("launchpad/lib/charts/charts-module",[
	"angular"
	], function(angular) {
		"use strict";

		var module = angular.module("charts", []);

		return module;
});
