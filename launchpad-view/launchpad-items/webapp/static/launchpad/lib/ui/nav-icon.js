define('launchpad/lib/ui/nav-icon', [
	'angular',
	'launchpad/lib/ui/ui-module'
], function(angular, uiModule) {

	'use strict';

	/**
	 * Input type number directive.
	 * Stops characters being added to the input field other than digits.
	 */
	uiModule.directive('navIcon', ["$templateCache", function($templateCache) {

		$templateCache.put("$smallHamburgerIcon.html", "" +
			"<span class='glyphicon glyphicon-minus'></span>" +
			"<span class='glyphicon glyphicon-minus'></span>" +
			"<span class='glyphicon glyphicon-minus'></span>");

		$templateCache.put("$largeHamburgerIcon.html", "" +
			"<span class='icon-bar'></span>" +
			"<span class='icon-bar'></span>" +
			"<span class='icon-bar'></span>");

		$templateCache.put("$verticalElipsisIcon.html", "" +
			"<span class='glyphicon glyphicon-sort-by-attributes'></span>");

		$templateCache.put("$arrowLeftIcon.html", "" +
			"<span class='glyphicon glyphicon-chevron-left'></span>");

		return {
			restrict: 'A',
			replace: true,
			scope: {
				'icon': '='
			},
			link: function(scope, element, attrs) {

				var icons = {
					"small-hamburger": "$smallHamburgerIcon.html",
					"large-hamburger": "$largeHamburgerIcon.html",
					"vertical-ellipsis": "$verticalElipsisIcon.html",
					"arrow-left": "$arrowLeftIcon.html"
				};

				scope.iconTemplate = icons[scope.icon];

				if(!scope.iconTemplate) {
					scope.iconTemplate = icons["small-hamburger"];
				}

				element.html($templateCache.get(scope.iconTemplate));
			}
		};
	}]);

});