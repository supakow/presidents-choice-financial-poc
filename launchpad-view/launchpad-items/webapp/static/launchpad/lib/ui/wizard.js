define('launchpad/lib/ui/wizard', [
	'angular',
	'launchpad/lib/ui/ui-module',
	"launchpad/lib/common/util"
], function(angular, uiModule, util) {

	'use strict';

	/**
	 * Wizard directive.
	 * Display steps with related views
	 *
	 * @example
	 * @param {string} title - Title of wizard (optional).
	 * @param {string} next-step - Name of the function for go to next step.
	 * @param {string} get-active-step - Name of the function which return current step.
	 * // <div wizard="wizard" next-step="wizardNextStep" title="Title" get-active-step="getActiveWizardStep">
	 * // 	<div wizard-step="wizard-step" heading="Step1"></div>
	 * // 	<div wizard-step="wizard-step" heading="Step2"></div>
	 * // </div>
	 */
	uiModule.directive('wizard', function() {
		return {
			restrict: 'A',
			transclude: true,
			replace: true,
			scope:{
				title:'@title'
			},
			controller: 'WizardController',
			link:function(scope, element, attrs) {
				scope.currentStepIndex = 0;
				// create functions in the parent scope of directive
				scope.$parent.$parent[attrs.nextStep] = function() {
					if (scope.wizardSteps[scope.currentStepIndex+1]) {
						scope.wizardSteps[++scope.currentStepIndex].active = true;
					}
				};
				scope.$parent.$parent[attrs.getActiveStep] = function() {
					return scope.currentStepIndex+1;
				};
			},
			template:'<div class="wizard-wrapper">' +
					 '	<div class="wizard-header" tabindex="0" aria-labelledby="{{wizardSteps[currentStepIndex].uniqueId}}">' +
					 '		<h3 class="wizard-header-title" ng-show="title">{{title}}</h3>' +
					 '		<div class="clearfix wizard-steps" role="navigation" ng-transclude></div>' +
					 '	</div>' +
					 '	<div class="widget-body panel-body">' +
					 '		<div class="wizard-views">' +
					 '			<div ng-repeat="step in wizardSteps" class="wizard-view" ng-show="step.active" wizard-content-transclude="step"></div>' +
					 '		</div>' +
					 '	</div>' +
					 '</div>'
		};
	});

	uiModule.controller('WizardController', ['$scope', function WizardCtrl($scope) {
		var ctrl = this,
			wizardSteps = ctrl.wizardSteps = $scope.wizardSteps = [];

		ctrl.select = function(selectedStep) {
			angular.forEach(wizardSteps, function(step) {
				if (step.active && step !== selectedStep) {
					step.active = false;
					step.completed = true;
				}
			});
			selectedStep.active = true;
		};

		ctrl.addStep = function addStep(step) {
			wizardSteps.push(step);
			if (wizardSteps.length === 1) {
				step.active = true;
			} else if (step.active) {
				ctrl.select(step);
			}
		};
	}]);
	uiModule.directive('wizardStep', ['$parse', function($parse) {
		var instance = 0;
		return {
			require: '^wizard',
			restrict: 'A',
			replace: true,
			template: '<div>' +
					  '  <i ng-if="stepIndex !== 1" class="glyphicon glyphicon-arrow-right pull-left wizard-step-arrow"></i>' +
					  '  <div class="wizard-step pull-left" ng-class="{\'wizard-active-step\': active}" wizard-heading-transclude="">' +
					  '    <span class="wizard-step-number" ng-class="{\'bg-success\': completed, \'text-muted\': !completed && !active}">{{stepIndex}}</span>' +
					  '    <span id="{{uniqueId}}" class="wizard-step-heading visible-xs-block visible-sm-inline visible-md-inline visible-lg-inline" ng-class="{\'hidden-sm hidden-xs\': !active,\'text-muted\': !completed && !active}">{{heading}}</span>' +
					  '  </div>' +
					  '</div>',
			transclude: true,
			scope: {
				active: '=?',
				heading: '@'
			},
			controller: function() {
			},
			compile: function(elm, attrs, transclude) {
				return function postLink(scope, elm, attrs, wizardCtrl) {
					scope.uniqueId = 'wizardStep-' + (instance++) + '-' + Math.floor(Math.random() * 10000);
					scope.stepIndex = instance;
					scope.$watch('active', function(active) {
						if (active) {
							wizardCtrl.select(scope);
						}
					});
					wizardCtrl.addStep(scope);
					scope.$transcludeFn = transclude;
				};
			}
		};
	}]);
	uiModule.directive('wizardHeadingTransclude', [function() {
		return {
			restrict: 'A',
			require: '^wizardStep',
			link: function(scope, elm, attrs, wizardCtrl) {
				scope.$watch('headingElement', function updateHeadingElement(heading) {
					if (heading) {
						elm.html('');
						elm.append(heading);
					}
				});
			}
		};
	}]);
	uiModule.directive('wizardContentTransclude', function() {
		return {
			restrict: 'A',
			require: '^wizard',
			link: function(scope, elm, attrs) {
				var step = scope.$eval(attrs.wizardContentTransclude);
				step.$transcludeFn(step.$parent, function(contents) {
					angular.forEach(contents, function(node) {
						elm.append(node);
					});
				});
			}
		};
	});
});