define('launchpad/lib/ui/scrolling-hook', [
	'angular',
	'jquery',
	'launchpad/lib/ui/ui-module'
], function(angular, $, uiModule) {

	'use strict';

	/**
	 * Provides hook to add a class to an element when the user has scrolled X amount
	 */
	uiModule.directive("scrollingHook", ["$window", function($window) {

		return {
			restrict: "A",
			scope: {
				className: "=scrollClass",
				distance: "=scrollDistance"
			},
			link: function(scope, element, attrs, ngModelCtrl) {

				//get current distance from top
				var lastScrollTop = $($window).scrollTop();

				//debounce function to restrict firing of scroll event
				var debounce = function(fn, delay) {
					var timer = null;

					return function () {
						var context = this, args = arguments;
						clearTimeout(timer);
						timer = setTimeout(function () {
							fn.apply(context, args);
						}, delay);
					};
				};

				var doClassChange = function() {
					//new distance from top once scrolled

					var st = $($window).scrollTop();

					if(st >= scope.distance) {

						if(lastScrollTop - st >= 5 || lastScrollTop <= 0) {
							//scrolling up
							if (element.hasClass(scope.className)) {
								element.removeClass(scope.className);
							}
						} else if(st > lastScrollTop) {
							//scrolling down
							if(!element.hasClass(scope.className)) {
								element.addClass(scope.className);
							}
						}
					} else {
						if (element.hasClass(scope.className)) {
							//we are not low enough on the page, return to default setting
							element.removeClass(scope.className);
						}
					}

					//reset scrollTop to current pageYOffset
					lastScrollTop = st;
				};

				angular.element($window).on("scroll", debounce(doClassChange, 10));
			}
		};
	}]);

});