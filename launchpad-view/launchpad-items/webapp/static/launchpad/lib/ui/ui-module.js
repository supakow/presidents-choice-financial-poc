define("launchpad/lib/ui/ui-module",[
	"angular"
	], function(angular) {
		"use strict";

		var module = angular.module("ui", []);

		return module;
});
