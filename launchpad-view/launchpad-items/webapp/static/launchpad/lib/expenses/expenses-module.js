define("launchpad/lib/expenses/expenses-module",[
	"angular",
	"launchpad/lib/common"
	], function(angular) {
		"use strict";

		var module = angular.module("expenses", ["common"]);

		return module;
});
//REVIEW: Discuss if expenses module is best future-proof place for this code