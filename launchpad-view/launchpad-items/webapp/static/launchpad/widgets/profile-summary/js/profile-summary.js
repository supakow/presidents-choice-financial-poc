/*global b$ */
define([
    "angular",
    "launchpad/lib/common/util",
    "launchpad/lib/ui",
    "launchpad/lib/user",
    "launchpad/lib/common",
    "launchpad/lib/common/preference-service"
], function(angular, util) {

    "use strict";

    var module = angular.module("launchpad-profilesummary", [
        "user",
        "ui",
        "common"
    ]);

    module.controller("profileSummaryController", ["$scope", "widget", "LoginService", "PreferenceService", function($scope, widget, LoginService, PreferenceService) {

        $scope.model = {
            username: "Admin"
        };
        
        //gets the link to the profile from a preference (if any)
        var profileLink = widget.getPreference("profileLink");
        
        profileLink = util.replaceUrlVars(profileLink, {
            contextPath: util.getContextPath()
        });

        $scope.profileLink = profileLink || null;

		var username = b$.portal.loggedInUserId || "Anonymous";
        angular.extend($scope.model, {
            username: username
        });

        PreferenceService.read().success(function(response) {
            $scope.model.preferredName = response.preferredName;
        });

        $scope.logout = function() {
            LoginService.doLogout();
        };

        $scope.responsiveRules = [
            { max: 100, size: 'xs' },
            { min: 101, size: 'small' },
            { min: 201, size: 'medium' }
        ];

        $scope.onSizeChange = function(size) {
            $scope.responsive = size;
        };
    }]);

    return function(widget) {
        module.value("widget", widget);
        angular.bootstrap(widget.body, ["launchpad-profilesummary"]);
    };
});