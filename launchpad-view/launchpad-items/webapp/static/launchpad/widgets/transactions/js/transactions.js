/*global b$, gadgets, console */
define([
    "jquery",
    "angular",
    "d3",
    "launchpad/lib/common/util",
    "launchpad/lib/ui/responsive",

    "pcfinancial/lib/transactions-data-mixin",

    "launchpad/lib/charts",
    "launchpad/lib/i18n",
    "launchpad/lib/ui",
    "launchpad/lib/common/preference-service",
    "launchpad/lib/transactions/category-display",
    "launchpad/lib/transactions/category-select",
    "launchpad/lib/transactions/category-model",
    "launchpad/support/angular/angular-ui-bootstrap",
    "launchpad/lib/transactions",
    "launchpad/lib/accounts",
    "launchpad/lib/contacts",
    "launchpad/lib/ui/jquery.autosuggest",
    "launchpad/support/jquery/placeholder"
], function($, angular, d3, util, responsive, dataMixin) {

    "use strict";

    var module = angular.module("launchpad-retail.transactions", ["i18n", "ui", "ui.bootstrap", "accounts", "charts", "transactions", "contacts"]);

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Main controller
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    module.controller("transactionsController",
        [ '$scope', '$element', '$timeout', 'i18nUtils', 'AccountsModel', 'CategoryModel', 'TransactionsModel', 'ContactsModel', 'PreferenceService', 'widget',
            function($scope, $element, $timeout, i18nUtils, AccountsModel, CategoryModel, TransactionsModel, ContactsModel, PreferenceService, widget) {
                var initialize = function(){

                    $scope.categoryModel = CategoryModel.getInstance({
                        endpoint: widget.getPreference("categoryDataSrc")
                    });
                    $scope.categoryModel.readList();

                    $scope.accountsModel = AccountsModel.getInstance({
                        accountsEndpoint: widget.getPreference("accountsDataSrc")
                    });
                    $scope.locale = widget.getPreference("locale");
                    $scope.title =  widget.getPreference("title");
                    $scope.showCharts = util.parseBoolean(widget.getPreference("showCharts"));
                    $scope.accountsTopBalance = widget.getPreferenceFromParents("preferredBalanceView") || "current";
                    $scope.templates = {
                        list : '/transactions/partials/list.html',
                        charts : '/transactions/partials/charts.html',
                        details: '/transactions/partials/details.html'
                    };
                    $scope.tabs = {'list': true, 'chart': false, 'combined': false};
                    $scope.showCategories = false;
                    $scope.previewAll = false;

                    var promise = $scope.accountsModel.load();
                    promise.then(function() {
                        if(!$scope.accountsModel.selected && $scope.accountsModel.accounts && $scope.accountsModel.accounts.length > 0) {
                            var accountNum = parseQueryString('accountId') || widget.getPreferenceFromParents("defaultAccount");
                            var account = $scope.accountsModel.findByAccountNumber(accountNum);
                            $scope.accountsModel.selected = account || $scope.accountsModel.accounts[0];

                            //now safe to listen for select account messages
                            gadgets.pubsub.subscribe("launchpad-retail.accountSelected", function(params) {
                                if(!params.originType || (params.originType && params.originType !== "transactions")) {
                                    angular.forEach($scope.accountsModel.accounts, function(account){
                                        if(params.accountId === account.id){
                                            $scope.accountsModel.selected = account;
                                        }
                                    });
                                    util.applyScope($scope);
                                }
                            });
                        }
                    });

                    PreferenceService.read().success(function(response) {
                        $scope.showCategories = util.parseBoolean(response.pfmEnabled);
                    });
                };

                var parseQueryString = function(paramName) {
                    var searchString = window.location.search.substring(1),
                        i, val, params = searchString.split("&");

                    for (i=0;i<params.length;i++) {
                        val = params[i].split("=");
                        if (val[0] === paramName) {
                            return val[1];
                        }
                    }
                    return null;
                };

                $scope.accountChanged = function() {
                    gadgets.pubsub.publish("launchpad-retail.accountSelected", {
                        accountId: $scope.accountsModel.selected.id,
                        originType: "transactions",
                        _noBehavior: true // Do not allow behavior to re-open the widget
                    }, true);
                };

                //Switch to show large account select or small
                $scope.accountSelectSize = "large";

                // Handlers
                $scope.selectTab = function(tab) {
                    $scope.$broadcast("tabSelected", tab);
                };
                $scope.toggleCategoryView = function() {
                    $scope.previewAll = !$scope.previewAll;
                };

                $scope.transferMoney = function() {
                    gadgets.pubsub.publish("launchpad-retail.requestMoneyTransfer");
                };



                // Events
                widget.addEventListener("preferencesSaved", function () {
                    widget.refreshHTML();
                    initialize();
                });


                responsive.enable($element)
                    .rule({
                        "max-width": 200,
                        then: function() {
                            $scope.categorySmallLayout = false;
                            $scope.responsiveClass = "lp-tile-size";
                            util.applyScope($scope);
                        }
                    })
                    .rule({
                        "min-width": 201,
                        "max-width": 359,
                        then: function() {
                            $scope.accountSelectSize = "small";
                            $scope.categorySmallLayout = true;
                            $scope.responsiveClass = "lp-small-size";
                            util.applyScope($scope);
                        }
                    })
                    .rule({
                        "min-width": 351,
                        "max-width": 600,
                        then: function() {
                            $scope.accountSelectSize = "large";
                            $scope.categorySmallLayout = false;
                            $scope.responsiveClass = "lp-medium-size";
                            util.applyScope($scope);
                        }
                    })
                    .rule({
                        "min-width": 601,
                        then: function() {
                            $scope.categorySmallLayout = false;
                            $scope.responsiveClass = "lp-large-size";
                            util.applyScope($scope);
                        }
                    });

                initialize();
            }]);

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // List controller
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    module.controller("transactionsListController",
        [ '$scope', '$element', '$timeout', 'i18nUtils', 'AccountsModel', 'TransactionsModel', 'ContactsModel', 'widget',
            function($scope, $element, $timeout, i18nUtils, AccountsModel, TransactionsModel, ContactsModel, widget) {

                var initialize = function() {
                    $scope.showTransactionIcons = util.parseBoolean(widget.getPreference("showTransactionIcons"));
                    i18nUtils.loadMessages(widget, $scope.locale).success(function(bundle) {
                        $scope.messages = bundle.messages;
                    });

                    $scope.transactionsModel = TransactionsModel.getInstance({
                        transactionsEndpoint: widget.getPreference("transactionsDataSrc"),
                        transactionDetailsEndpoint: widget.getPreference("transactionDetailsDataSrc"),
                        transactionMessagesEndpoint: widget.getPreference("messageSrc"),
                        locale: $scope.locale
                    });

                    $scope.contactsModel = new ContactsModel({
                        contacts: widget.getPreference("contactsDataSrc")
                    });

                    $scope.contactsModel.loadContacts();
                };

                //search
                var currentSuggestion = null;
                $scope.query = "";
                $scope.updateSuggestion = function(suggestion) {
                    currentSuggestion = suggestion;
                    $scope.doSearch();
                };
                $scope.doSearch = function() {
                    var filters = {};
                    if(currentSuggestion) {
                        if(currentSuggestion.search.query) {
                            filters.query = currentSuggestion.search.query;
                        }
                        //else if data and from and to
                        else if(currentSuggestion.type === "date") {
                            filters.fromDate = currentSuggestion.search.from.getTime();
                            filters.toDate = currentSuggestion.search.to.getTime();
                        }
                        //else if amount and from and to
                        else if(currentSuggestion.type === "amount") {
                            filters.fromAmount = currentSuggestion.search.from;
                            filters.toAmount = currentSuggestion.search.to;
                        }
                        //else if contact
                        else if(currentSuggestion.type === "contact") {
                            filters.contact = currentSuggestion.search.contact;
                        }
                    }
                    $scope.transactionsModel.setFilters(filters);
                    $scope.transactionsModel.loadTransactions($scope.accountsModel.selected);
                };
                $scope.resetSearch = function() {
                    $scope.transactionsModel.clearFilters();
                    $scope.transactionsModel.loadTransactions($scope.accountsModel.selected);
                };

                // Watches
                $scope.$watch("accountsModel.selected", function(value) {
                    if(value) {
                        $scope.transactionsModel.loadTransactions($scope.accountsModel.selected).then(function(){
                            dataMixin.changeTransactionCategory($scope.transactionsModel.transactions);
                        });
//                        gadgets.pubsub.publish("launchpad-retail.accountSelected", {
//                            accountId: $scope.accountsModel.selected.id,
//                            originType: "transactions"
//                        });
                    }
                });

                // Sorting options
                var ascIconClass = 'lp-icon lp-icon-caret-up', descIconClass = 'lp-icon lp-icon-caret-down';

                $scope.sort = {
                    options: [
                        { label: 'Date', icon: descIconClass, sort: '-bookingDateTime', aria: 'Decreasing' },
                        { label: 'Date', icon: ascIconClass, sort: 'bookingDateTime', aria: 'Increasing' },
                        { label: 'Amount', icon: descIconClass, sort: '-transactionAmount', aria: 'Decreasing' },
                        { label: 'Amount', icon: ascIconClass, sort: 'transactionAmount', aria: 'Increasing' }
                    ]
                };
                $scope.sort.selected = $scope.sort.options[0];

                $scope.changeSort = function() {
                    var value = $scope.sort.selected;
                    $scope.transactionsModel.sort = value.sort;

                    if ($scope.transactionsModel && $scope.transactionsModel.transactions.length) {
                        $scope.transactionsModel.loadTransactions($scope.accountsModel.selected);
                    }
                };

                $scope.transactionKeydown = function(evt, transaction) {
                    if (evt.which === 13 || evt.which === 32) {
                        evt.preventDefault();
                        evt.stopPropagation();
                        // $scope.loadTransactionDetails(transaction);
                        $scope.openDetails(transaction);
                    }
                };

                $scope.loadTransactionDetails = function(transaction) {
                    $scope.transactionsModel.loadTransactionDetails(transaction);
                };

                $scope.loadMoreTransactions = function() {
                    var length = $scope.transactionsModel.transactions.length;
                    $scope.transactionsModel.loadMoreTransactions().then(function() {
                        dataMixin.changeTransactionCategory($scope.transactionsModel.transactions);
                        setTimeout(function() {
                            var selector = $scope.tabs.combined === true ? '.lp-transactions-combined .transactions-list-row' : '.transactions-list-row';
                            var row = $(widget.body).find(selector).eq(length);
                            row.focus();
                        }, 100);
                    });
                };

                $scope.updateTransactionCategory = function(transaction, categoryId, similar) {
                    var promise;
                    if (!similar) {
                        promise = $scope.transactionsModel.updateTransactionCategory(transaction, categoryId);
                    } else {
                        promise = $scope.transactionsModel.updateSimilarTransactionCategory(transaction, categoryId);
                    }

                    promise.success(function() {
                        // console.log('updateTransactionCategory', transaction.id, categoryId);
                    });
                };

                // ------------------
                $scope.openDetails = function(transaction, selectedTab) {

                    var setDetailTabValues = function(tabs, selectedTab) {
                        for (var tab in tabs){
                            if (tabs.hasOwnProperty(selectedTab)) {
                                tabs[tab] = false;
                                if (tab === selectedTab) {
                                    tabs[tab] = true;
                                }
                            }
                        }
                    };

                    if (selectedTab === null || selectedTab === undefined) {
                        selectedTab = 'details';
                    }

                    transaction.showDetails = !transaction.showDetails;
                    if (transaction.showDetails) {
                        $timeout(function() {
                            setDetailTabValues(transaction.detailTabs, selectedTab);
                        }, 0);
                    }
                    if (selectedTab === 'details') {
                        $scope.loadTransactionDetails(transaction);
                    }
                    $scope.closePreview(transaction);

                    if ($scope.categorySmallLayout && transaction.showDetails) {
                        $('body').animate({
                            scrollTop: $('#transaction-' + transaction.id).offset().top - 5
                        }, 500);
                    }

                    // fix for chrome redraw issue
                    if (!transaction.showDetails) {
                        var tabset = document.getElementById('transactions-tabs');
                        tabset.style.display = 'none';
                        tabset.style.display = 'block';
                    }
                };

                $scope.selectDetailsTab = function(transaction) {
                    $scope.loadTransactionDetails(transaction);
                };

                $scope.openPreview = function(transaction) {
                    transaction.preview = true;
                    if (!transaction.showDetails) {
                        $('#transaction-' + transaction.id).addClass('preview');
                    }
                };
                $scope.closePreview = function(transaction) {
                    transaction.preview = false;
                    $('#transaction-' + transaction.id).removeClass('preview');
                };

                $scope.categoryClick = function(event, transaction) {
                    if (event !== null && event !== undefined) {
                        event.preventDefault();
                        event.stopPropagation();
                    }

                    $scope.openDetails(transaction, 'categories');
                };

                // Events
                gadgets.pubsub.subscribe("launchpad-retail.transactions.applyFilter", function(data) {
                    $scope.tabs.list = true;
                    $scope.searchTerm = data.contactName;
                    $scope.transactionsModel.setFilters(data.filters);
                    $scope.transactionsModel.loadTransactions($scope.accountsModel.selected);
                });

                gadgets.pubsub.subscribe("launchpad-retail.transactions.newTransferSubmitted", function() {
                    // For demo purposes adding a 3 sec delay
                    $timeout(function() {
                        $scope.transactionsModel.clearTransactionsList();
                        $scope.transactionsModel.loadMoreTransactions();
                    }, 3000);
                });

                $scope.$on("tabSelected", function(event, tab){
                    if(tab === "combined"){
                        $scope.showTransactionIcons = false;
                    } else {
                        $scope.showTransactionIcons = util.parseBoolean(widget.getPreference("showTransactionIcons"));
                    }
                });

                initialize();
            }]);

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Charts controller
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    module.controller("transactionsChartController",
        [ '$scope', '$element', 'widget', 'i18nUtils', 'AccountsChartModel', 'TransactionsChartModel', '$q',
            function($scope, $element, widget, i18nUtils, AccountsChartModel, TransactionsChartModel, $q){
                if(!$scope.showCharts){
                    return;
                }

                var initialize = function() {
                    $scope.now = new Date();
                    $scope.endDate = new Date($scope.now);
                    $scope.scale = 'WEEK';
                    $scope.series = 'WITHDRAWAL';
                    $scope.timePeriod = getNiceTimePeriod(getStartDate().getTime, $scope.endDate.getTime);

                };

                var getNiceTimePeriod = function(startTime, endTime) {
                    var monthNames = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sept", "Oct", "Nov", "Dec"];

                    var start = new Date(startTime);
                    var end = new Date(endTime);

                    //Same dates are fed into the chart but days are cut off at the start of the chart
                    //This fix brings the display in line together
                    start.setDate(start.getDate() + 1);

                    //return nicely formatted time period
                    return start.getDate() + " " + monthNames[start.getMonth()] + " - " + end.getDate() + " " + monthNames[end.getMonth()];
                };

                var getStartDate = function(){
                    var result = new Date($scope.endDate);
                    switch($scope.scale) {
                        case 'WEEK':
                            result.setDate(result.getDate() - 7);
                            break;
                        case 'MONTH':
                            result.setMonth(result.getMonth() - 1);
                            break;

                    }
                    return result;
                };

                $scope.nextPeriod = function() {
                    switch($scope.scale) {
                        case 'WEEK':
                            $scope.endDate.setDate($scope.endDate.getDate() + 7);
                            break;
                        case 'MONTH':
                            $scope.endDate.setMonth($scope.endDate.getMonth() + 1);
                            break;
                    }

                    updateCharts("next");
                };

                $scope.prevPeriod = function() {
                    switch($scope.scale) {
                        case 'WEEK':
                            $scope.endDate.setDate($scope.endDate.getDate() - 7);
                            break;
                        case 'MONTH':
                            $scope.endDate.setMonth($scope.endDate.getMonth() - 1);
                            break;
                    }
                    updateCharts("prev");
                };


                $scope.setScale = function(scale) {
                    $scope.scale = scale;
                    updateCharts("prev");
                };

                $scope.setSeries = function(series) {
                    $scope.series = series;
                    updateCharts("prev", true);
                };

                $scope.showNextPeriod = function() {
                    return $scope.now.getTime() > $scope.endDate.getTime();
                };


                var updateCharts = function(direction, onlyTransactions) {
                    var params = {
                        start : getStartDate().getTime(),
                        end : $scope.endDate.getTime()
                    };

                    //Refresh the nice time period
                    $scope.timePeriod = getNiceTimePeriod(params.start, params.end);

                    $scope.accountsChartModel = AccountsChartModel.getInstance({
                        accountsChartEndpoint: widget.getPreference("accountBalanceChartDataSrc"),
                        accountId: $scope.accountsModel.selected.id
                    });

                    $scope.transactionsChartModel = TransactionsChartModel.getInstance({
                        transactionsChartEndpoint: widget.getPreference("transactionsChartDataSrc"),
                        accountId: $scope.accountsModel.selected.id
                    });

                    var getDate = function(date){
                        var result = new Date(date);
                        result.setHours(0);
                        result.setMinutes(0);
                        result.setSeconds(0);
                        return result;
                    };

                    var getTransactionsValue = function(data){
                        return $scope.series === 'DEPOSIT' ? data.deposit : data.withdrawal;
                    };

                    var formatAmount = function(amount){
                        return i18nUtils.formatAmount($scope.locale, amount, $scope.accountsModel.selected.currency);
                    };

                    $q.all([$scope.accountsChartModel.load(params), $scope.transactionsChartModel.load(params)]).then(function() {
                        $scope.transactionsChartOptions = {
                            data: $scope.transactionsChartModel.chartData,
                            height: 200,
                            padding: [30, 30, 30, 90],
                            parsers: {
                                x : function(data) {
                                    return getDate(data.date);
                                },
                                y : function(data) {
                                    return getTransactionsValue(data);
                                }
                            },
                            formatters: {
                                y : function(amount){
                                    return formatAmount(amount);
                                },
                                x : function(date) {
                                    return d3.time.format("%e")(date);
                                },
                                tooltip : function(data) {
                                    return d3.time.format("%B %e")(getDate(data.date)) + "<br>" + formatAmount(getTransactionsValue(data));
                                }
                            }
                        };

                        if(!onlyTransactions){
                            $scope.accountBalanceChartOptions = {
                                data: $scope.accountsChartModel.chartData,
                                height: 200,
                                padding: [30, 30, 30, 90],
                                parsers: {
                                    x : function(data) {
                                        return getDate(data.date);
                                    },
                                    y : function(data) {
                                        return data.amount;
                                    }
                                },
                                formatters: {
                                    y : function(amount){
                                        return formatAmount(amount);
                                    },
                                    x : function(date) {
                                        return d3.time.format("%e")(date);
                                    },
                                    tooltip : function(data) {
                                        return d3.time.format("%B %e")(getDate(data.date)) + "<br>" + formatAmount(data.amount);
                                    }
                                },
                                animation : {
                                    direction : direction === "prev" ? "left" : "right"
                                }
                            };
                        }

                    });
                };

                $scope.$watch("accountsModel.selected", function(value) {
                    if(value) {
                        updateCharts("prev");
                    }
                });

                $scope.$on("tabSelected", function(event, tab){
                    if(tab === "chart" || tab === "combined"){
                        updateCharts("prev");
                    }
                });

                initialize();

            }]);

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Smart suggest
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    module.directive("lpSmartsuggest", [ 'ContactsModel', 'SmartSuggestEngine', 'SmartSuggestFormatter',
        function(ContactsModel, SmartSuggestEngine, SmartSuggestFormatter){

            return {
                restrict : "A",
                scope: {
                    "smartsuggestSelect": "=lpSmartsuggestSelect",
                    "smartsuggestClear": "=lpSmartsuggestClear",
                    "currency": "=lpCurrency",
                    "contacts": "=lpContacts"
                },
                link : function(scope, element, attrs) {

                    //setup the smart suggest engine
                    var smartSuggest  = new SmartSuggestEngine();
                    smartSuggest.addSuggester({
                        data: [],
                        suggest: SmartSuggestEngine.builtIn.getContactSuggestions
                    });
                    smartSuggest.addSuggester({
                        type: SmartSuggestEngine.types.DATE,
                        suggest: SmartSuggestEngine.builtIn.getDateSuggestions
                    });
                    smartSuggest.addSuggester({
                        type: SmartSuggestEngine.types.AMOUNT,
                        suggest: SmartSuggestEngine.builtIn.getAmountSuggestions
                    });
                    smartSuggest.addSuggester({
                        type: SmartSuggestEngine.types.GENERAL,
                        suggest: SmartSuggestEngine.builtIn.getGeneralSuggestions
                    });

                    var formatter = new SmartSuggestFormatter({
                        locale: "en-US",
                        currency: scope.currency
                    });

                    scope.$watch("currency", function(currency) {
                        formatter.currency = currency;
                    });

                    scope.$watch("contacts", function(contacts) {
                        //TODO: why is this not an empty array when empty?
                        if($.isArray(contacts)) {
                            smartSuggest.addSuggester({
                                data: contacts,
                                suggest: SmartSuggestEngine.builtIn.getContactSuggestions
                            });
                        }
                    });

                    $(element).autosuggest({
                        lookup: function(q) {
                            var suggs = smartSuggest.getSuggestions(q);
                            suggs = suggs.map(function(suggestion) {
                                var values = formatter.format(suggestion);

                                var displayValue;
                                if(suggestion.contact) {
                                    displayValue = suggestion.contact.name;
                                } else if(values.length === 2) {
                                    displayValue = values[0] + " to " + values[1];
                                } else {
                                    displayValue = values[0];
                                }

                                return {
                                    data: suggestion,
                                    value: displayValue
                                };
                            });
                            return suggs;
                        },
                        onSelect: function (suggestion) {
                            if(scope.smartsuggestSelect) {
                                scope.$apply(function() {
                                    scope.smartsuggestSelect.call({}, suggestion.data);
                                });
                            }
                        },
                        onClear: function() {
                            if(scope.smartsuggestClear) {
                                scope.$apply(function() {
                                    scope.smartsuggestClear.call({});
                                });
                            }
                        },
                        formatResult: function(suggestion) {
                            return formatter.getSuggestionHtml(suggestion.data);
                        },
                        autoSelectFirst: true
                    });
                }
            };
        }]);

    //TODO: move me to a common place
    module.directive('placeholder', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var placeholder = attrs.placeholder;
                if (placeholder && $.fn.placeholder) {
                    $(element).placeholder();
                }
            }
        };
    });

    return function(widget) {
        module.value("widget", widget);
        angular.bootstrap(widget.body, ["launchpad-retail.transactions"]);
    };
});
