/*global b$, be, gadgets console */
define([
	'jquery',
	'angular',
	'launchpad/lib/common/util',
	'launchpad/lib/ui'
], function($, angular, util) {

	'use strict';

	var module = angular.module('launchpad-retail.navbar-advanced', ['ui', 'ui.bootstrap']);

	module.controller('NavBarAdvancedController', ['$scope', 'widget', function($scope, widget) {
		var navRoot, navShow;

		var initialize = function () {


			//set up widget preferences
			//data sources
			navRoot = widget.getPreference('navRoot');

			//general nav preferences
			navShow = widget.getPreference('navShow');
			$scope.navSticky = widget.getPreference('navSticky');
			$scope.scrollSetting = 'lp-' + widget.getPreference('scrollSetting') + '-scroll' || 'lp-normal-scroll';
			$scope.showPageTitle = util.parseBoolean(widget.getPreference('showPageTitle'));

			//logo preferences
			$scope.logoUrl = widget.getPreference('logoURL') || '';
			$scope.logoUrl = util.replaceUrlVars($scope.logoUrl, {
				contextPath: util.getContextPath()
			});
			$scope.mobileLogoUrl = widget.getPreference('mobileLogoURL') || '';
			$scope.mobileLogoUrl = util.replaceUrlVars($scope.mobileLogoUrl, {
				contextPath: util.getContextPath()
			});
			$scope.launcherIcon = widget.getPreference('launcherIcon') || 'arrow-left';

			//nav and launcher icon preferences
			$scope.animationHook = widget.getPreference('navigationIconAnimationHook') || 'arrow';
			$scope.showNotificationsBadge = util.parseBoolean(widget.getPreference('showNotificationsBadge'));

			$scope.links = [];
			$scope.showMenu = true;
			//the current active page
			$scope.activePage = '';
			//the current active context
			$scope.activeContext = '';
			//fix for issue in portal manager
			$scope.isDesignMode = util.isDesignMode();

			//preset animation hooks
			$scope.animationPrepend = 'animation-';
			$scope.defaultAnimationClass = 'none';
			$scope.animationClass = $scope.animationPrepend + $scope.defaultAnimationClass;


			//button enum
			$scope.buttons = {
				logo: 'logo',
				icon: 'icon'
			};

			//scroll settings
			$scope.scrollSettingsEnum = {
				'normal': 'lp-normal-scroll',
				'transparency': 'lp-transparency-scroll',
				'hide-show': 'lp-hide-show-scroll'
			};

			gadgets.pubsub.subscribe("launchpad-retail.activeContextChanged", function(data) {
				$scope.$apply(function() {
					if (data.newActiveContext.length > 0) {
						$scope.activeContext = data.newActiveContext;
					} else {
						$scope.activeContext = $scope.activePage;
					}
				});
			});

			if (util.parseBoolean(navShow)) {
				var root = navRoot || 'navroot_mainmenu';
				var url = b$.portal.config.serverRoot + '/portals/' + b$.portal.portalName + '/links/' + root + '.xml';


				be.utils.ajax({
					url: url,
					dataType: 'xml',
					cache: false,
					success: function (responseData) {

						var json = be.xmlToJson({xml: responseData});

						$scope.$apply(function () {

							for (var i = 0; i < json.link.children.link.length; i++) {
								if (json.link.children.link[i].uuid === b$.portal.linkUUID) {
									$scope.activePage = json.link.children.link[i].properties.title.value;
									$scope.activeContext = $scope.activePage;
								}
								$scope.links.push(json.link.children.link[i]);
							}
						});
					}
				});
			}

			if($scope.navSticky) {
				//nav must be sticky
				gadgets.pubsub.publish("launchpad-retail.stickyNavBar");
			}
		};

		//toggle the menu open/closed by changing the $scope.animationClass variable
		$scope.toggleMenu = function () {

			$scope.showMenu = !$scope.showMenu;

			if($scope.animationHook.length > 0) {
				//apply animation
				if(!$scope.showMenu) {
					//showing menu
					$scope.animationClass = $scope.animationPrepend + $scope.animationHook;
				} else {
					//hiding menu
					$scope.animationClass = $scope.animationPrepend + $scope.defaultAnimationClass;
				}
			}


		};

		//toggle the launcher menu open or closed
		$scope.toggleLauncherMenu = function() {

			gadgets.pubsub.publish("launchpad-retail.toggleLauncherMenu");
			$scope.activeContext = $scope.activePage;
		};


		initialize();

	}]);


	return function(widget) {
		module.value('widget', widget);
		angular.bootstrap(widget.body, ['launchpad-retail.navbar-advanced']);
	};

});