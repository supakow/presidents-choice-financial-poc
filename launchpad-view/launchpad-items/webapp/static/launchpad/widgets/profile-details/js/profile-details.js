/*global b$, gadgets */
define([
    'jquery',
    'angular',
    'launchpad/lib/common/util',
    'launchpad/lib/i18n',
    'launchpad/lib/common',
    'launchpad/lib/ui',
    'launchpad/lib/user'
], function($, angular, util) {

    'use strict';
    var MODULE_NAME = 'profileDetailsWidget';

    var module = angular.module(MODULE_NAME, ['user', 'ui', 'i18n', 'common']);

    module.controller('ProfileDetailsCtrl', ['$scope', '$rootElement', 'widget', 'ProfileDetailsService', 'i18nUtils',
    function($scope, $rootElement, widget, ProfileDetailsService, i18nUtils) {

        $scope.getMessages = function() {
            return i18nUtils.loadMessages(widget, 'en').success(function(bundle) {
                $scope.messages = bundle.messages;
            });
        };

        var initialize = function() {
            $scope.title =  widget.getPreference('title');
            $scope.getMessages().success(function() {
                $scope.getProfileData();
            });
        };

        $scope.getProfileData = function() {
            ProfileDetailsService.getData().success(function(response) {
                $scope.profile = ProfileDetailsService.formatResponse(response, $scope.messages);
            });
        };

        widget.addEventListener('preferencesSaved', function () {
            widget.refreshHTML();
            $scope.$apply(function() {
                initialize();
            });
        });

        initialize();
    }]);

    return function(widget) {
        module.value('widget', widget);
        angular.bootstrap(widget.body, [MODULE_NAME]);
    };
});