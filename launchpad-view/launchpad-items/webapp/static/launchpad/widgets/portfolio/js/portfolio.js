/*
*   Written by: Dave Morris
*   Email: david.morris88@gmail.com
*   Copyright: Backbase 2013
*/

define([
    "jquery",
    "angular",
    "launchpad/lib/common/util",
    "launchpad/lib/i18n"
], function($, angular, util) {

        "use strict";

        var module = angular.module("launchpad-foundation.portfolio", ["i18n"]);

        module.directive("lpPercentage", function() {
            return function(scope, element, attrs){
                attrs.$observe('lpPercentage',function(){
                    element.text(attrs.lpPercentage + "%");
                });
            };
        });

        /**
        * Portfolio Service
        */
        module.service('PortfolioService', ['$http', 'widget', function($http, widget) {
            var url = widget.getPreference('data-source');
            url = util.replaceUrlVars(url, {
                contextPath: util.getContextPath()
            });

            this.read = function() {
                return $http.get(url);
            };
        }]);

        module.controller("portfolioController", ["$scope", "$rootElement", "$timeout", "widget", "i18nUtils", "PortfolioService", function($scope, $rootElement, $timeout, widget, i18nUtils, PortfolioService){
            var getPortfolio = function() {
                var xhr = PortfolioService.read();
                
                xhr.success(function(data) {
                    if(data && data !== 'null') {
                        $scope.isLoading = false;
                        for(var i = 0; i < data.length; i++) {
                            for(var x = 0; x < data[i].conditions.length ; x++) {
                                var condition = data[i].conditions[x];
                                if(condition.value instanceof Array) {
                                    data[i].conditions[x].type = "list";
                                }
                            }
                            $scope.products.push(data[i]);
                        }
                    }
                });

                xhr.error(function(error) {
                    $scope.isLoading = false;
                    $scope.errorCode = error.errors[0].code || 500;
                    $scope.errorText =  "There is no account information available for this user";
                });

                xhr['finally'](function() {
                    $scope.isLoading = false;
                });
            }; 

            var initialize = function() {
                $scope.title = "Products";
                $scope.isLoading = true;
                $scope.errorCode = null;
                $scope.errorText = "";
                
                $scope.data_source = widget.getPreference("data-source");

                $scope.products = [];

                $scope.locale = widget.getPreference("locale");
                i18nUtils.loadMessages(widget, $scope.locale).success(function(bundle) {
                    $scope.messages = bundle.messages;
                });

                getPortfolio();
            };

            initialize();
    }]);

    return function(widget) {
        module.value("widget", widget);
        angular.bootstrap(widget.body, ["launchpad-foundation.portfolio"]);
    };
});