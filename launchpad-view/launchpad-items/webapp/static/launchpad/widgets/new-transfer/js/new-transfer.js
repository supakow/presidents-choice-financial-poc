/*global b$, gadgets */
define([
    "jquery",
    "angular",
    "launchpad/lib/common/util",
    "launchpad/lib/ui/responsive",
    "launchpad/lib/i18n",
    "launchpad/lib/accounts",
    "launchpad/lib/contacts",
    "launchpad/lib/payments",
    "launchpad/lib/transactions",
    "launchpad/lib/ui",
    "launchpad//lib/common",
    "launchpad/support/angular/angular-ui-validate",
    "launchpad/support/jquery/placeholder",
    "launchpad/lib/ui/jquery.autosuggest"
], function($, angular, util, responsive) {

    "use strict";

    var module = angular.module("launchpad-retail.newPayment", ["i18n", "ui.validate", "ui", "ui.bootstrap", "accounts", "payments", "transactions", "contacts", "common", "user"]);

    module.directive('lpFutureTime', function() {
        return {

            require: '?ngModel',

            link: function(scope, elm, attrs, ngModel) {

                var now = new Date();
                now.setDate(now.getDate() - 1);
                now = now.getTime();

                ngModel.$parsers.unshift(function(value) {
                    var date;

                    // Empty field
                    if (!value) {
                        ngModel.$setValidity('lpFutureTime', true);
                        return;
                    }

                    date = Date.parse(value);

                    // Unparsable date
                    if (isNaN(date) || date < 0) {
                        ngModel.$setValidity('lpFutureTime', false);
                        return value;
                    }

                    // Valid date, but in the past or present
                    if (date <= now) {
                        ngModel.$setValidity('lpFutureTime', false);
                        return value;
                    }

                    // Future Date
                    ngModel.$setValidity('lpFutureTime', true);
                    return value;
                });
            }
        };
    });

    module.directive('placeholder', function() {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var placeholder = attrs.placeholder;
                if (placeholder) {
                    $(element).placeholder();
                }
            }
        };
    });


    module.controller("NewTransferController", function($scope, $rootScope, $rootElement, $timeout, AccountsModel, PaymentOrdersModel, ContactsModel, CurrencyModel, IbanModel, widget, i18nUtils, customerId, formDataPersistence, P2PService, transferTypes) {

        // Wheter to auto save new contacts
        var autoSave = widget.getPreference("autosaveContactsPreference");

        var formName = "new-transfer-form";

        var paymentIntervals = {
            recurring: "RECURRING",
            non_recurring: "NON_RECURRING"
        };

        var initialize = function() {

            $scope.todaysDate = new Date();

            /**
             * Manage transfer type to hide/show certain parts of the form
             */
            $scope.poTypeEnum = transferTypes;

            $scope.p2pService = P2PService;

            $scope.p2pService.getUserEnrollmentDetails().then(function(response) {
                $scope.userEnrolledForP2P = true;
            }, function(response) {

                gadgets.pubsub.subscribe("launchpad-retail.userP2PEnrolled", function(data) {
                    $scope.userEnrolledForP2P = data.enrolled;
                });

                if(response.status === 404) {
                    //user not enrolled
                    $scope.userEnrolledForP2P = false;
                    $scope.p2pUserEnrollment = {
                        email: "",
                        mobile: "",
                        receivingAccountNumber: "",
                        customerId: ""
                    };
                }
            });


            $scope.urgentTranfer = false;

            $scope.locale = widget.getPreference("locale");

            //flag for which kind of transfer this is
            $scope.usTransfer = $scope.locale === "en-US";

            $scope.title =  widget.getPreference("title");

            $scope.accountsTopBalance = widget.getPreferenceFromParents("defaultBalanceView") || "current";

            $scope.modalShown = false;
            $scope.exchangeRateModalShown = false;
            $scope.ibanModalShown = false;
            $scope.routingModalShown = false;

            $scope.templates = {
                saveContacts: '/new-transfer/partials/saveContacts.html',
                urgentTransfer: '/new-transfer/partials/urgentTransfer.html',
                exchangeRate: '/new-transfer/partials/exchangeRate.html',
                iban: '/new-transfer/partials/iban.html',
                routingAndAccount: '/new-transfer/partials/routingAndAccountNumber.html'
            };

            i18nUtils.loadMessages(widget, $scope.locale).success(function(bundle) {
                $scope.messages = bundle.messages;
            });

            $scope.accountsModel = AccountsModel.getInstance({
                accountsEndpoint: widget.getPreference("accountsDataSrc")
            });

            $scope.paymentOrdersModel = new PaymentOrdersModel({
                paymentOrdersEndpoint: widget.getPreference("paymentOrdersDataSrc")
            });

            $scope.dateOptions = {
                'show-button-bar': false,
                'show-weeks': false
            };

            var promise = $scope.accountsModel.load();

            promise.then(function() {

                if (util.parseBoolean(widget.getPreference("forceAccountSelection"))) {
                    return;
                }

                if(!$scope.accountsModel.selected && $scope.accountsModel.accounts && $scope.accountsModel.accounts.length > 0) {
                    $scope.accountsModel.selected = $scope.accountsModel.findByAccountNumber(widget.getPreferenceFromParents("defaultAccount")) || $scope.accountsModel.accounts[0];
                }

                //now safe to listen for account Select message
                gadgets.pubsub.subscribe("launchpad-retail.accountSelected", function(params) {
                    if (!$scope.accountsModel.accounts) {
                        return;
                    }

                    $.each($scope.accountsModel.accounts, function(index, account){
                        if(params.accountId === account.id){
                            $scope.accountsModel.selected = account;
                        }
                    });
                    util.applyScope($scope);
                });

            });

            $scope.currencyModel = CurrencyModel.getInstance({
                defaultCurrencyEndpoint: widget.getPreference("defaultCurrencyEndpoint"),
                currencyListEndpoint: widget.getPreference("currencyListEndpoint"),
                disableCurrencySelection: widget.getPreference("disableCurrencySelection")
            });

            $scope.currencyModel.loadDefaultCurrency().success(function(data) {
                $scope.currencyModel.configureDefaultCurrency(data);

                $scope.paymentOrder.instructedCurrency = $scope.paymentOrder.instructedCurrency === "" ?  $scope.currencyModel.defaultCurrency.currency_code : $scope.paymentOrder.instructedCurrency;
                $scope.currencyModel.loadOtherCurrencies().success(function() {

                    //if there is payment order data saved, select the appropriate currency
                    var currency = $scope.paymentOrder.instructedCurrency === '' ? $scope.currencyModel.defaultCurrency.currency_code : $scope.paymentOrder.instructedCurrency;
                    $scope.currencyModel.selectCurrency(currency);
                });
            });

            $scope.disableCurrencySelection = false;

            $scope.contactsModel = new ContactsModel({
                contacts: widget.getPreference('contactListDataSrc'),
                contactData: widget.getPreference('contactDataSrc'),
                contactDetails: widget.getPreference('contactDetailsDataSrc')

            });
            $scope.contactsModel.loadContacts();

            $scope.ibanModel = IbanModel.getInstance({
                countryListEndpoint: widget.getPreference('ibanDataSrc'),
                enableCountrySearch: util.parseBoolean(widget.getPreference("enableIbanCountrySearch"))
            });
            $scope.ibanModel.loadCountryList().then(function(response) {
                $scope.ibanModel.validate();
            });

            resetModel();

            $scope.toggleTabs = {
                oneTime: $scope.paymentOrder.isScheduledTransfer ? false : true,
                scheduled: $scope.paymentOrder.isScheduledTransfer ? true : false
            };

            $scope.activeTransferTab = {
                bank: true,
                p2pEmail: false,
                p2pAddress: false
            };

            //persisting form data that has been filled in but not processed
            $scope.persistenceManager = formDataPersistence.getInstance();

            //reload previously saved form data
            if($scope.persistenceManager.isFormSaved(formName)) {

                var newPaymentOrder = $scope.persistenceManager.getFormData(formName);

                var excludedProperties = [
                    "uuid",
                    "scheduleDate"
                ];

                //extend the properties of the new payment order to the one on the scope
                for(var key in newPaymentOrder) {
                    if(newPaymentOrder.hasOwnProperty(key) && excludedProperties.indexOf(key) === -1) {
                        $scope.paymentOrder[key] = newPaymentOrder[key];
                    }
                }

                //reset active transfer tabs
                setActiveTransferTabs();
            }

            // close all modal popups when the widget is closed (perspective minimized on launcher container and widget on springboard)
            widget.addEventListener("PerspectiveModified", function(event) {
                if (event.newValue === "Minimized" || event.newValue === "Widget") {
                    $scope.hideAllModals();
                }
            });

            //deep watch the payment order object, save form data when object changes
            $scope.$watch("paymentOrder", function(newValue, oldValue) {

                if(newValue !== oldValue) {
                    $scope.persistenceManager.saveFormData(formName, $scope.paymentOrder);

                    if(newValue && newValue.type !== $scope.poTypeEnum.bank) {
                        $scope.setScheduledTransfer("one-time");
                    }
                }
            }, true);

            gadgets.pubsub.subscribe("launchpad-retail.requestMoneyTransfer.setTab", function(data) {
                if(data.tab) {
                    $scope.paymentOrder.type = data.tab;
                    setActiveTransferTabs();
                }
            });



            util.applyScope($scope);
        };

        /**
         * Regular function
         */

        //is the recipient a new contact?
        var isNewContact = function() {
            if ($scope.contactsModel.findByName($scope.paymentOrder.counterpartyName)) {
                return false;
            }
            return (!$scope.paymentOrder.selectedCounter ||
                $scope.paymentOrder.selectedCounter.name !== $scope.paymentOrder.counterpartyName ||
                $scope.paymentOrder.selectedCounter.account !== $scope.paymentOrder.counterpartyIban);
        };

        // Create a contact if this user's preference
        var createContact = function() {

            var contact = {
                name: $scope.paymentOrder.counterpartyName
            };

            if($scope.paymentOrder.type === $scope.poTypeEnum.bank) {
                contact.account = $scope.usTransfer ? $scope.paymentOrder.counterpartyAccount : $scope.paymentOrder.counterpartyIban;
            } else if($scope.paymentOrder.type === $scope.poTypeEnum.p2pEmail) {
                contact.email = $scope.paymentOrder.counterpartyEmail;
            }

            $scope.contactsModel.currentContact = contact;

            $scope.contactsModel.createCounterParty(true);
            gadgets.pubsub.publish("launchpad.contacts.load");
        };

        //reset the payment order model
        var resetModel = function() {

            //fix to reset isScheduledTransfer to correct value
            var scheduledTransfer = $scope.paymentOrder ? $scope.paymentOrder.isScheduledTransfer : false;

            $scope.paymentOrder = {
                uuid: util.generateUUID(),
                dateAllOptions: [
                    { id: 'today', label: 'Transfer today' },
                    { id: 'date', label: 'Scheduled transfer' }
                ],
                dateOptions: 'today',
                isScheduledTransfer: scheduledTransfer,
                scheduledTransfer: {
                    frequency: "",
                    every: 1,
                    intervals: [],
                    startDate: new Date(),
                    endDate: new Date(),
                    timesToRepeat: 1
                },
                urgentTransfer: false,
                scheduleDate: new Date(),
                isOpenDate: false,
                instructedCurrency: '',
                counterpartyIban: '',
                counterpartyAccount: '',
                counterpartyEmail: '',
                counterpartyAddress: '',
                instructedAmount: '',
                paymentReference: '',
                paymentDescription: '',
                counterpartyName: '',
                date: '',
                saveContact: autoSave === '' ? false : util.parseBoolean(autoSave),
                type: $scope.poTypeEnum.bank,
                dirty: false
            };
        };

        //set which transfer tab is currently active
        var setActiveTransferTabs = function() {

            //set all tabs active to false
            for(var tab in $scope.activeTransferTab) {
                if($scope.activeTransferTab.hasOwnProperty(tab)) {
                    $scope.activeTransferTab[tab] = false;
                }
            }

            var found = false;

            for(var item in $scope.poTypeEnum) {
                if($scope.poTypeEnum.hasOwnProperty(item)) {
                    if($scope.poTypeEnum[item] === $scope.paymentOrder.type) {
                        $scope.activeTransferTab[item] = true;
                        found = true;
                    }
                }
            }

            if(!found) {
                $scope.activeTransferTab.bank = true;
            }
        };

        //broadcaste a message for child scopes to reset their properties
        var resetChildScopes = function() {
            $scope.$broadcast("reset", {});
        };

        var checkValidAccounts = function() {

            if($scope.paymentOrderForm.counterpartyIban && $scope.paymentOrder.type === $scope.poTypeEnum.bank) {
                $scope.paymentOrderForm.counterpartyIban.$setValidity('notEqual', $scope.notEqualAccounts());
            }
        };


        //build the payment order with details needed for a bank transaction
        var buildBankPaymentOrder = function(paymentOrder) {

            paymentOrder.type = $scope.poTypeEnum.bank;

            if($scope.usTransfer) {

                paymentOrder.counterpartyAccount = $scope.paymentOrder.counterpartyAccount;

                if ($scope.paymentOrder.paymentDescription !== "") {
                    paymentOrder.paymentDescription = $scope.paymentOrder.paymentDescription;
                }
            } else {
                paymentOrder.counterpartyIban = $scope.paymentOrder.counterpartyIban;

                //set payment reference OR payment description
                if ($scope.paymentOrder.paymentReference === "" && $scope.paymentOrder.paymentDescription !== "") {
                    paymentOrder.paymentDescription = $scope.paymentOrder.paymentDescription;
                } else if ($scope.paymentOrder.paymentDescription === "" && $scope.paymentOrder.paymentReference !== "") {
                    paymentOrder.paymentReference = $scope.paymentOrder.paymentReference;
                }
            }

            //handle scheduled transfer
            if($scope.paymentOrder.isScheduledTransfer) {
                paymentOrder.scheduledTransfer = {};

                //add relevent scheduledTransfer fields
                paymentOrder.scheduledTransfer.frequency = $scope.paymentOrder.scheduledTransfer.frequency;
                paymentOrder.scheduledTransfer.every = $scope.paymentOrder.scheduledTransfer.every;

                //send array as comma-delimited string to the backend service (due to issue with camel mashup)
                paymentOrder.scheduledTransfer.intervals = $scope.paymentOrder.scheduledTransfer.intervals.join(",");
                paymentOrder.scheduledTransfer.startDate = +(new Date($scope.paymentOrder.scheduledTransfer.startDate));
                paymentOrder.scheduledTransfer.endDate = +(new Date($scope.paymentOrder.scheduledTransfer.endDate));
                paymentOrder.paymentMode = paymentIntervals.recurring;
            } else {
                paymentOrder.onDate = +(new Date($scope.paymentOrder.scheduleDate));
                paymentOrder.paymentMode = paymentIntervals.non_recurring;
                paymentOrder.urgentTransfer = $scope.paymentOrder.urgentTransfer;
            }

            return paymentOrder;
        };

        //build the payment order with details needed for a P2P transfer
        var buildP2PEmailPaymentOrder = function(paymentOrder) {
            paymentOrder.type = $scope.poTypeEnum.p2pEmail;
            paymentOrder.onDate = +(new Date($scope.paymentOrder.scheduleDate));
            paymentOrder.paymentMode = paymentIntervals.non_recurring;
            paymentOrder.counterpartyEmail = $scope.paymentOrder.counterpartyEmail;

            return paymentOrder;
        };

        //build the payment order with details needed for a P2P address transfer
        var buildP2PAddressPaymentOrder = function(paymentOrder) {
            paymentOrder.type = $scope.poTypeEnum.p2pAddress;
            paymentOrder.onDate = +(new Date($scope.paymentOrder.scheduleDate));
            paymentOrder.paymentMode = paymentIntervals.non_recurring;
            paymentOrder.counterpartyAddress = $scope.paymentOrder.counterpartyAddress;

            return paymentOrder;
        };


        /**
         * Scope functions
         */

        $scope.openCalendar = function($event) {
            $event.preventDefault();
            $event.stopPropagation();

            $scope.paymentOrder.isOpenDate = true;
        };

        //sets the payment orders transfer type
        $scope.setPaymentOrderType = function(paymentOrderType) {
            $scope.paymentOrder.type = paymentOrderType;
        };


        $scope.submitForm = function(event) {
            event.preventDefault();

            $scope.persistenceManager.removeFormData(formName);

            checkValidAccounts();

            var processPaymentOrder = true;

            $scope.paymentOrderForm.submitted = true;
            if($scope.paymentOrderForm.$invalid) {
                $scope.$broadcast('lp.retail.new-transfer.errors');
                return false;
            }

            var paymentOrder = {},
                selectedAccount = $scope.accountsModel.selected;

            //add relevent fields to payment order object
            paymentOrder.uuid = $scope.paymentOrder.uuid;
            paymentOrder.counterpartyName = $scope.paymentOrder.counterpartyName;
            paymentOrder.instructedAmount = $scope.paymentOrder.instructedAmount;
            paymentOrder.instructedCurrency = $scope.paymentOrder.instructedCurrency;
            //if for some reason the instructed currency has been set blank it defaults to account default
            if(paymentOrder.instructedCurrency === "") {
                paymentOrder.instructedCurrency = selectedAccount.currency;
            }
            paymentOrder.accountId = selectedAccount.id;
            paymentOrder.accountName = selectedAccount.name;

            switch($scope.paymentOrder.type) {
                //BANK TRANSFER
                case $scope.poTypeEnum.bank:
                    paymentOrder = buildBankPaymentOrder(paymentOrder);
                    break;
                //P2P Email Transfer
                case $scope.poTypeEnum.p2pEmail:
                    //enroll user if not already enrolled
                    if(!$scope.userEnrolledForP2P) {
                        $scope.p2pService.enrollUserForP2P({
                            email: $scope.p2pUserEnrollment.email,
                            accountNumber: $scope.p2pUserEnrollment.receivingAccountNumber
                        }).then(function(response) {

                            gadgets.pubsub.publish("launchpad-retail.userP2PEnrolled", {
                                enrolled: true
                            });

                            gadgets.pubsub.publish("launchpad-retail.p2pEnrollmentComplete", {
                                verified: true
                            });

                        }, function(response) {
                            $scope.p2pService.error = "An error occurred while connecting to the P2P Service";
                            processPaymentOrder = false;
                        });
                    }

                    paymentOrder = buildP2PEmailPaymentOrder(paymentOrder);
                    break;
                case $scope.poTypeEnum.p2pAddress:
                    if(!$scope.userEnrolledForP2P) {
                        $scope.p2pService.enrollUserForP2P({
                            email: $scope.p2pUserEnrollment.email,
                            accountNumber: $scope.p2pUserEnrollment.receivingAccountNumber
                        }).then(function(response) {

                        }, function(response) {
                            $scope.p2pService.error = "An error occurred while connecting to the P2P Service";
                            processPaymentOrder = false;
                        });
                    }

                    paymentOrder = buildP2PAddressPaymentOrder(paymentOrder);
                    break;
                default:
                    paymentOrder = buildBankPaymentOrder(paymentOrder);
                    break;
            }

            // Autosave contact if not an existing one
            if ($scope.paymentOrder.saveContact && isNewContact()) {
                createContact();
            }

            if(processPaymentOrder) {
                var xhr = $scope.paymentOrdersModel.create(paymentOrder, paymentOrder.uuid);

                xhr.success(function () {
                    gadgets.pubsub.publish("Launcher:openWidget", { widgetName: "review-transfers-v1" });
                    gadgets.pubsub.publish("launchpad-retail.paymentOrderInitiated");

                    $scope.resetForm();
                });
            }

        };

        $scope.onSaveContactsChange = function() {
            if (autoSave === '' && $scope.paymentOrder.saveContact) {
                $scope.toggleModal(); // Show
            }
        };

        $scope.setContactPreference = function(response) {
            // Call backend service to store this preference
            autoSave = !!response;
            widget.model.setPreference("autosaveContactsPreference", '' + autoSave);
            widget.model.save();

            $scope.toggleModal(); // Hide
        };

        $scope.showContactsInfo = function() {
            $scope.showContactsOptions = false;
            $scope.toggleModal(); // Hide
        };

		$scope.showContactDetailsInfo = function() {
			$scope.modalShown = false;
			$scope.toggleSaveContactDetailsModal();
		};

        $scope.toggleAutosuggest = function() {
            $(widget.body).find("[name=counterpartyName]").trigger('toggle.autosuggest');
        };

        $scope.resetForm = function() {

            resetModel();
            resetChildScopes();
            setActiveTransferTabs();

            $scope.paymentOrderForm.submitted = false;
            $scope.paymentOrderForm.$setPristine();

            $scope.persistenceManager.removeFormData(formName);
        };

        $scope.resetCounterparty = function() {
            $scope.$apply(function(){
                $scope.paymentOrder.counterpartyIban = "";
            });
        };

        $scope.updateCounterparty = function(accountDetails) {
            // Store the selection as reference, to compare
            // later if the contact is a new one or not.
            $scope.paymentOrder.selectedCounter = {
                name: $scope.paymentOrder.counterpartyName,
                account: accountDetails.account
            };

            $scope.paymentOrder.type = accountDetails.type;
            setActiveTransferTabs();


            if($scope.paymentOrder.type === $scope.poTypeEnum.bank) {
                if($scope.usTransfer) {
                    $scope.paymentOrder.counterpartyAccount = accountDetails.account;
                } else {
                    $scope.paymentOrder.counterpartyIban = accountDetails.account;
                }
            } else if($scope.paymentOrder.type === $scope.poTypeEnum.p2pEmail) {
                $scope.paymentOrder.counterpartyEmail = accountDetails.account;
            }

            $scope.paymentOrderForm.$setDirty();
        };

        // Validate that accounts ( from / to ) are not equal
        $scope.notEqualAccounts = function() {
            if (!$scope.accountsModel.selected) {
                return false;
            }

            return $scope.accountsModel.selected.iban !== $scope.paymentOrder.counterpartyIban;
        };

        $scope.onAccountChange = function() {
            checkValidAccounts();
        };

        $scope.toggleModal = function() {
            $scope.showContactsOptions = !$scope.showContactsOptions;
        };

        //close the exchange rate modal
        $scope.toggleExchangeRateModal = function() {
            $scope.exchangeRateModalShown = !$scope.exchangeRateModalShown;
        };

		$scope.toggleSaveContactDetailsModal = function() {
			$scope.modalShown = !$scope.modalShown;
		};

        $scope.toggleIbanModal = function() {
            $scope.ibanModalShown = !$scope.ibanModalShown;
        };

        $scope.toggleRoutingNumberModal = function() {
            $scope.routingModalShown = !$scope.routingModalShown;
        };

        $scope.hideAllModals = function() {
            $scope.urgentTransferModalShown = false;
            $scope.exchangeRateModalShown = false;
            $scope.ibanModalShown = false;
            $scope.modalShown = false;
        };

        $scope.toggleUrgentTransferModal = function() {
            $scope.urgentTransferModalShown = !$scope.urgentTransferModalShown;
        };

        $scope.setScheduledTransfer = function(value) {

            if(value === "scheduled") {
                $scope.paymentOrder.isScheduledTransfer = true;
                $scope.toggleTabs.oneTime = false;
                $scope.toggleTabs.scheduled = true;
            } else if(value === "one-time") {
                $scope.paymentOrder.isScheduledTransfer = false;
                $scope.toggleTabs.oneTime = true;
                $scope.toggleTabs.scheduled = false;
            }
        };

        $scope.$on("reset", function() {
            $scope.paymentOrder.isScheduledTransfer = false;
            $scope.toggleTabs.oneTime = true;
            $scope.toggleTabs.scheduled = false;
        });


        /**
         * Other set up
         */

        widget.addEventListener("preferencesSaved", function () {
            widget.refreshHTML();
            initialize();
        });

        // Responsive
        responsive.enable($rootElement)
            .rule({
                "max-width": 200,
                then: function() {
                    $scope.responsiveClass = "lp-tile-size";
                    util.applyScope($scope);
                }
            })
            .rule({
                "min-width": 201,
                "max-width": 350,
                then: function() {
                    $scope.responsiveClass = "lp-small-size";
                    util.applyScope($scope);
                }
            }).rule({
                "min-width": 351,
                "max-width": 600,
                then: function() {
                    $scope.responsiveClass = "lp-medium-size";
                    util.applyScope($scope);
                }
            }).rule({
                "min-width": 601,
                then: function() {
                    $scope.responsiveClass = "lp-large-size";
                    util.applyScope($scope);
                }
            });

        initialize();
    });

    module.directive("lpSmartsuggest", function(ContactsModel, SmartSuggestEngine, SmartSuggestFormatter){

        return {
            restrict : "A",
            scope: {
                "lpSmartsuggestSelect": "&",
                "lpSmartsuggestClear": "&",
                "contacts": "=lpContacts",
                "accounts": "=lpAccounts",
                "model": "=ngModel"
            },
            link : function(scope, element, attrs){

                //setup the smart suggest engine
                var smartSuggest  = new SmartSuggestEngine({
                    showTitles : true
                });
                smartSuggest.addSuggester({
                    data: [],
                    suggest: SmartSuggestEngine.builtIn.getContactSuggestions
                });

                scope.$watch("accounts", function(accounts) {
                    smartSuggest.addSuggester({
                        data: accounts,
                        suggest: SmartSuggestEngine.builtIn.getAccountSuggestions,
                        type: SmartSuggestEngine.types.ACCOUNT,
                        options: {
                            showAll: true
                        }
                    });
                });

                scope.$watch("contacts", function(contacts) {
                    //TODO: why is this not an empty array when empty?
                    if($.isArray(contacts)) {
                        smartSuggest.addSuggester({
                            data: contacts,
                            suggest: SmartSuggestEngine.builtIn.getContactSuggestions,
                            type: SmartSuggestEngine.types.CONTACT,
                            options: {
                                showAll: true
                            }
                        });
                    }
                });


                var formatter = new SmartSuggestFormatter({
                    locale: "en-US"
                });

                // https://github.com/angular/angular.js/issues/1924
                scope.$watch('model', function() {
                    scope.$eval(attrs.ngModel + ' = model');
                });

                scope.$watch(attrs.ngModel, function(val) {
                    scope.model = val;
                });

                $(element).autosuggest({
                    lookup: function(q) {
                        var suggs = smartSuggest.getSuggestions(q);
                        suggs = suggs.map(function(suggestion) {
                            var values = formatter.format(suggestion);

                            var displayValue;
                            if(suggestion.contact) {
                                displayValue = suggestion.contact.name;
                            } else if(values.length === 2) {
                                displayValue = values[0] + " to " + values[1];
                            } else {
                                displayValue = values[0];
                            }

                            return {
                                data: suggestion,
                                value: displayValue
                            };
                        });
                        return suggs;
                    },
                    onSelect: function (suggestion) {
                        var account,
                            name;

                        if (suggestion.data.type === SmartSuggestEngine.types.TITLE) {
                            return false;
                        }

                        switch (suggestion.data.type) {
                            case SmartSuggestEngine.types.CONTACT:
                                name = suggestion.data.contact.name;
                                account = suggestion.data.contact.account;
                                break;
                            case SmartSuggestEngine.types.ACCOUNT:
                                name = suggestion.data.account.name;
                                account = suggestion.data.account.iban;
                                break;
                        }
                        scope.model = name;
                        scope.lpSmartsuggestSelect({account: account});
                        return false;
                    },
                    onClear: function() {
                        scope.lpSmartsuggestClear();
                    },
                    formatResult: function(suggestion) {
                        return formatter.getSuggestionHtml(suggestion.data);
                    },
                    autoSelectFirst: false,
                    minChars: 0
                });
            }
        };
    });

    return function(widget) {
        module.value("widget", widget);
        angular.bootstrap(widget.body,["launchpad-retail.newPayment"]);
    };
});