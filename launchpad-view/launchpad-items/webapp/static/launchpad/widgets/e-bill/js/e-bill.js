/*global b$, gadgets */
define(function(require, exports, module) {

    'use strict';

    // base
    var ng = require('angular');

    // Modules

    var name = 'launchpad-retail.e-bill';
    var deps = [

    ];

    var app = ng.module(name, deps );

    return function(widget) {
        app.value('widget', widget);
        ng.bootstrap(widget.body,[app.name]);
    };
});
