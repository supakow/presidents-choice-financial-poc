/**
 *
 *  Web Starter Kit
 *  Copyright 2014 Google Inc. All rights reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 *
 */

'use strict';

var paths = {
    styles: ['css/e-bill.less'],
    scripts: ['js/**/*.js'],
    html: ['partials/**/*.html','index.html']
};
// Include Gulp & Tools We'll Use
var gulp = require('gulp');
var g = require('gulp-load-plugins')();
var runSequence = require('run-sequence');
var browserSync = require('browser-sync');
var reload = browserSync.reload;

// Compile Less Files
gulp.task('styles:less', function () {
    return gulp.src(paths.styles)
        .pipe(g.cached('styles'))
        .pipe(g.less())
        .pipe(gulp.dest('css'))
        .pipe(reload({stream:true}));
});
gulp.task('styles', ['styles:less']);

// JS
gulp.task('scripts:min', function () {
    return gulp.src( paths.scripts  )
        .pipe(g.cached('scripts'))
        // Add Minifier Here
        .pipe(gulp.dest('js/'))
        .pipe(reload({stream:true}));
});
// Output Final Scripts
gulp.task('scripts', ['scripts:min']);


// Views
gulp.task('html:default', function () {
    return gulp.src(paths.html)
        .pipe(g.cached('html'))
        .pipe(reload({stream:true}));
});
gulp.task('html', ['html:default']);


// Reload all Browsers
gulp.task('bs-reload', function () {
    browserSync.reload();
});
// Watch Files For Changes & Reload
gulp.task('serve', function () {
    browserSync.init(null, {
        // proxy: 'http://localhost:7777/portalserver/',
        server: {
            baseDir: './'
        },
        open: false,
        notify: false
    });

    gulp.watch(paths.styles, ['styles']);
    gulp.watch(paths.scripts, ['scripts']);
    gulp.watch(paths.html, ['html']);
});

