require('jasmine-given');

describe('Demo', function() {

    browser.get('/');

    Given(function() {
        this.num = 1;
    });

    When(function() {
        this.num++;
    });

    Then('Shoud have the CXP-manager dashboard url', function() {
        expect( this.num ).toBe(2);
        browser.sleep(5000);
    });

});
