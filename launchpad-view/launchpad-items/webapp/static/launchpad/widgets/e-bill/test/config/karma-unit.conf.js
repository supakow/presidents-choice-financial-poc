module.exports = function(config) {

    'use strict';
    var paths = require('./base.conf').paths;
    config.set({

        //urlRoot: '';

        basePath: paths.base,
        // base path, that will be used to resolve files and exclude
        // frameworks to use
        frameworks: ['jasmine', 'requirejs'],

        // list of files / patterns to load in the browser
        files: [
            /*----------------------------------------------------------------*/
            /*  boootstrap
            /*----------------------------------------------------------------*/
            { pattern: paths.test + '/unit/mock-data/globals.js',included: true, watched: true},
            { pattern: paths.test + '/config/require.config.js' },

            /*----------------------------------------------------------------*/
            /* Src's
            /*----------------------------------------------------------------*/
            // Scripts
            { pattern:  paths.src + '/**/*', included: false },
            /*----------------------------------------------------------------*/
            /* Fixtures
            /*----------------------------------------------------------------*/
           // {pattern: testPath + '/mock-data/**', watched: true, served: true, included: false},
            /*----------------------------------------------------------------*/
            /* Test Specs
            /*----------------------------------------------------------------*/
            { pattern: paths.test + '/unit/**/*.spec.js', included: false} // all the specs

        ],

        proxies:  {
            //'/': 'http://localhost:7777/'
        },

        preprocessors : [{
            // Disable all preprocessors
        }],

        // list of files to exclude
        exclude: [
            paths.src + '/**/*.less'
        ],

        // test results reporter to use
        // possible values: 'dots', 'progress', 'junit', 'growl', 'coverage'
        reporters: ['dots', 'osx'],

        // web server port
        port: 9876,
        // enable / disable colors in the output (reporters and logs)
        colors: true,
        // level of logging
        // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
        logLevel: config.LOG_INFO,
        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: true,
        // Start these browsers, currently available:
        // - Chrome
        // - ChromeCanary
        // - Firefox
        // - Opera
        // - Safari (only Mac)
        // - PhantomJS
        // - IE (only Windows)
        browsers: ['PhantomJS'],
        // If browser does not capture in given timeout [ms], kill it
        captureTimeout: 60000,

        // Continuous Integration mode
        // if true, it capture browsers, run tests and exit
        singleRun: false
    });

};
