/* globals requirejs */

'use strict';

requirejs.config({
    // Karma serves files from '/base'
    baseUrl: '/base',
    paths: {
        'jquery': 'https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min',
        'angular': 'https://ajax.googleapis.com/ajax/libs/angularjs/1.2.7/angular.min'

    },
    shim: {

    },
    deps: Object.keys(window.__karma__.files).filter(function(f) { return /spec\.js$/.test(f); }),
    // start test run, once Require.js is done
    callback: window.__karma__.start
});


