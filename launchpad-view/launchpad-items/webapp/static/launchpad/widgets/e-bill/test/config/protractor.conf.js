/**
 *  ----------------------------------------------------------------
 *  Copyright © 2003/2014 Backbase B.V.
 *  ----------------------------------------------------------------
 *  Author : Backbase R&D - Amsterdam - New York
 *  Filename : protractor.conf.js
 *  Description:
 *
 *  ----------------------------------------------------------------
 */

exports.config = {

    baseUrl: 'http://localhost:3000',

    specs: [
        '../e2e/specs/**/*.e2e.js'
    ],
    /*----------------------------------------------------------------*/
    /* The address of a running selenium server.
    /*----------------------------------------------------------------*/
    // seleniumAddress: 'http://localhost:4444/wd/hub',

    /*----------------------------------------------------------------*/
    /*  // If chromeOnly is true, we dont need to stand the selenium server.
    /* If you want to test with firefox, then set this to false and change the browserName
    /*----------------------------------------------------------------*/
    chromeOnly: false,

    capabilities: {
        browserName: 'chrome', //phantomjs / firefox
        version: '',
        platform: 'ANY'
    },
    // The timeout for each script run on the browser. This should be longer
    // than the maximum time your application needs to stabilize between tasks.
    allScriptsTimeout: 11000,
    // Options to be passed to Jasmine-node.
    jasmineNodeOpts: {

    },

    onPrepare: function() {
        global.nonAngular = function(flag) {
            browser.ignoreSynchronization = flag;
        };

        (function(){
            global.nonAngular(true);
        })();
    }
};


