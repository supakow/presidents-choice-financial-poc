var baseConfig = require('./karma-unit.conf.js');

module.exports = function(config) {

    'use strict';

    var reportsPath = 'target/reports/unit/';
    // Load base config
    baseConfig(config);

    // Override base config
    config.set({

        reporters: ['dots','coverage','junit'],
        junitReporter: {
            outputFile: reportsPath + 'xunit/junit-results.xml'
        },
        preprocessors: {
            'js/**/*.js': ['coverage']
        },
        coverageReporter : {
            type : 'cobertura',
            file : 'karma-test-results.xml',
            dir  : reportsPath + 'cobertura'
        },

        singleRun: true,
        autoWatch: false
    });
};
