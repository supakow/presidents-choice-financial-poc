/**
 *  ----------------------------------------------------------------
 *  Copyright © 2003/2013 Backbase B.V.
 *  ----------------------------------------------------------------
 *  Author : Backbase R&D - Amsterdam - New York
 *  Filename : template.spec.js
 *  Description:
 *  Template Spec
 *  ----------------------------------------------------------------
 */

define(function(require, module, exports) {

    'use strict';

    describe('Intuitive suite description', function() {
        beforeEach(function() {
            // Do something before each spec
        });
        afterEach(function() {
            // Do something after each spec
        });
        it('Property be there', function() {
          expect(3).toBe(3);
        });
    });
    // Will be skipped from testing
    // Used when the scenario is in progress
    xdescribe('Excluded suite', function() {
       // Working proggress
    });
});
