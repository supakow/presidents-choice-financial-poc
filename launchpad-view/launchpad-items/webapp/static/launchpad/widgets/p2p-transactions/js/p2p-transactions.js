/*global gadgets, widget, console*/
define([
    'angular',
    'jquery',
    'launchpad/lib/common/util',
    'launchpad/lib/ui/responsive',
    'launchpad/lib/i18n',
    'launchpad/lib/ui',
    'launchpad/support/angular/angular-ui-bootstrap',
    'launchpad/lib/contacts',
    'launchpad/lib/common',
    'launchpad/lib/transactions/p2p-transactions-model'
], function(angular, $, util, responsive) {

    'use strict';

    var module = angular.module('launchpad-retail.p2p-transactions', ['i18n', 'ui', 'ui.bootstrap', 'contacts', 'common', 'transactions']);

    module.controller('P2PTransactionsController', ['$scope', '$element', '$timeout', 'widget', 'P2PService', 'P2PTransactionsModel', function($scope, $element, $timeout, widget, P2PService, P2PTransactionsModel) {
        var initialize = function() {

            $scope.locale = widget.getPreference('locale');
            $scope.title =  widget.getPreference('title');

            $scope.userEnrolledForP2P = false;

            P2PService.getUserEnrollmentDetails().then(function(response) {
                $scope.userEnrolledForP2P = true;
                initializeP2PTransactions(response.data.accountId);
            }, function(response) {
                if (response.status === 404) {
                    //user not enrolled
                    $scope.userEnrolledForP2P = false;
                }
            });

        };

        var initializeP2PTransactions = function(accountId) {
            $scope.transactionsModel = P2PTransactionsModel.getInstance({
                // transactionsEndpoint: '/portalserver/static/launchpad/widgets/p2p-transactions/data/transactions.json',
                transactionsEndpoint: widget.getPreference('transactionsDataSrc'),
                transactionMessagesEndpoint: widget.getPreference('messageSrc'),
                pageSize: 10,
                locale: $scope.locale
            });
            // loadTransactions parameter is an array to later support aggregated view
            $scope.transactionsModel.loadTransactions([accountId]).then(function(response) {
                util.applyScope($scope);
            });
        };

        $scope.loadMoreTransactions = function() {
            if ($scope.transactionsModel.allowMoreResults()) {
                $scope.transactionsModel.loadMoreTransactions();
            }
        };

        $scope.transferMoney = function() {
            gadgets.pubsub.publish('launchpad-retail.requestMoneyTransfer');

            gadgets.pubsub.publish('launchpad-retail.requestMoneyTransfer.setTab', {
                tab: "P2P_EMAIL"
            });

        };

        $scope.enroll = function() {
            gadgets.pubsub.publish("launchpad-retail.openP2PEnrollment");
        };

        gadgets.pubsub.subscribe("launchpad-retail.p2pTransactions.newTransferSubmitted", function() {
            // For demo purposes adding a 3 sec delay
            $timeout(function() {
                $scope.transactionsModel.clearTransactionsList();
                $scope.transactionsModel.loadMoreTransactions();
            }, 3000);
        });

        initialize();

    }]);

    return function(widget) {
        module.value('widget', widget);
        angular.bootstrap(widget.body, ['launchpad-retail.p2p-transactions']);
    };
});