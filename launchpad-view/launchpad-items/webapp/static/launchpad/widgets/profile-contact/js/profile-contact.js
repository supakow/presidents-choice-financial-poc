/*global b$, gadgets */
define([
    'jquery',
    'angular',
    'launchpad/lib/common/util',
    'launchpad/lib/common',
    'launchpad/lib/i18n',
    'launchpad/lib/user',
    'launchpad/lib/ui'
], function($, angular, util) {

    'use strict';

    var module = angular.module('profileContactWidget', ['i18n', 'user', 'ui', 'ui.bootstrap', 'common' ]);

    module.controller('ProfileContactCtrl', ['$scope', '$rootElement', 'widget', 'i18nUtils', 'ProfileContactService',
    function($scope, $rootElement, widget, i18nUtils, ProfileContactService) {

        $scope.errorMessages = {
            'invalid_phone': 'This phone is not valid.',
            'invalid_email': 'This email is not valid.'
        };

        $scope.control = {
            address: {
                value: []
            },
            phoneNumber: {
                value: '',
                errors: [],
                loading: false,
                validate: function(phone) {
                    var re = /^\+(?:[0-9]?){6,14}[0-9]$/;
                    if (!re.test(phone)) {
                        return 'invalid_phone';
                    }
                    return true;
                }
            },
            emailAddress: {
                value: '',
                errors: [],
                loading: false,
                validate: function(email) {
                    var re = /^[A-Za-z0-9+_.\-]+@[A-Za-z0-9.\-]+$/;
                    if (!re.test(email)) {
                        return 'invalid_email';
                    }
                    return true;
                }
            }
        };

        ProfileContactService.read().success(function(response) {
            $scope.control.phoneNumber.value = response.phoneNumber === [] ? '' : response.phoneNumber;
            $scope.control.emailAddress.value = response.emailAddress === [] ? '' : response.emailAddress;

            var address = [];
            if (response.correspondenceAddress) {
                var addr = response.correspondenceAddress;
                address.push(addr.house + ' ' + addr.street);
                address.push(addr.zip + ' ' + addr.town + ', ' + addr.stateAbbr);
            }

            $scope.control.address.value = address.join("\n");
        });

        $scope.save = function(field, value) {
            var isLoading = $scope.control[field].loading;

            isLoading = true;
            var xhr = ProfileContactService.save(field, value)
            .error(function() {
                // console.log('there was an error saving');
            });

            xhr['finally'](function() {
                isLoading = false;
            });
        };
    }]);

    return function(widget) {
        module.value('widget', widget);
        angular.bootstrap(widget.body,['profileContactWidget']);
    };
});