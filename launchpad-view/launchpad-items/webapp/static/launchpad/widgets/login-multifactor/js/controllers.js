define([
    "angular",
    "jquery",
    "launchpad/lib/common/util"], function(angular, $, util) {

    "use strict";

    var app = angular.module("launchpad.loginmfa.controllers", ["LocalStorageModule"]);

    app.value("TokenStorageId", "login-multifactor-login");

    // Initial login controller
    app.controller("LoginCtrl", ["$scope", '$timeout', "localStorageService", "TokenStorageId",
    function($scope, $timeout, webStorage, TokenStorageId) {

        var storedId = webStorage.get(TokenStorageId);

        $scope.data = {
            id: storedId || "",
            remember: storedId ? true : false
        };

        $scope.submit = function() {
            $scope.$broadcast("autofill:update");

            $scope.clearAlerts();

            if ( !$scope.data.password ) {
                $scope.addAlert("MISSING_PASSWORD");
                return;
            }

            if ($scope.data.remember) {
                webStorage.add(TokenStorageId, $scope.data.id);
            } else {
                webStorage.remove(TokenStorageId);
            }

            // Keep reference of the ID
            $scope.master.id = angular.copy($scope.data.id);

            $scope.connect("initiate", {
                username: $scope.data.id,
                password: $scope.data.password
            });
        };

        // Hack to get the model values for autocompleted inputs
        // Fixed(?) in AngularJS 1.2
        $timeout(function() {
            $scope.$broadcast("autofill:update");
        }, 100);
    }]);


    // OTP controller
    app.controller("OtpCtrl", ["$scope", function($scope) {
        $scope.isfocus = true;

        $scope.submit = function(value) {
            $scope.clearAlerts();

            if (/^\d+$/.test(value) === false) {
                $scope.addAlert("INVALID_CODE");
                return;
            }

            $scope.connect("verify", {otp_code: value});
        };

        $scope.cancel = function() {
            $scope.moveToStep("login");
        };
    }]);

});