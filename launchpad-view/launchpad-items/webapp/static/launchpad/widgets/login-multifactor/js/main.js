/*global gadgets */
define(['jquery',
        'angular',
        'launchpad/lib/common/util',
        'launchpad/lib/i18n',
        'launchpad/lib/user',
        'launchpad/support/jquery/placeholder',
        'launchpad/widgets/login-multifactor/js/controllers',
        'launchpad/support/angular/angular-ui-bootstrap',
        'launchpad/widgets/login-multifactor/js/angular-webstorage',
        'launchpad/lib/ui'
], function($, angular, util) {

    'use strict';

    var module = angular.module('launchpad.loginmfa', [
        'i18n',
        'user',
        'launchpad.loginmfa.controllers',
        'ui.bootstrap',
        'LocalStorageModule',
        'ui'
    ]);

    /**
     * Configuration.
     */
    module.constant('loginmfaConfig', {
        stepsDirectory: util.getContextPath() + '/static/launchpad/widgets/login-multifactor/steps/',
        initialStep: 'login',
        urlSessionId: '{id}',
        sessionVerifyResponse: 'Verified',
        defaultErrorCode: 'SERVICE_ERROR'
    });

    /**
     * API service.
     */
    module.service('LoginmfaApi', ['$http', 'LoginService', 'loginmfaConfig', 'widget', function($http, LoginService, config, widget) {
        this.connect = function(sessionId, url, data) {
            if (sessionId) {
                angular.extend(data, {sessionId: sessionId});
                url = url.replace(config.urlSessionId, sessionId);
            }

            return $http.post(url, $.param(data), {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/x-www-form-urlencoded;'
                }
            });
        };

        this.doLogin = function(username, password) {
            return LoginService.getLoginPromise(username, password).success(function(response) {
	            gadgets.pubsub.publish("launchpad.login.success");
	            if (util.parseBoolean(widget.getPreference('reloadOnSuccess'))) {
                    LoginService.handleSuccessfulLogin(response);
                }
            });
        };
    }]);


    /**
     * Master controller.
     */
    module.controller('LoginmfaCtrl', ['$scope', 'LoginmfaApi', 'loginmfaConfig', 'i18nUtils', 'widget',
    function($scope, LoginmfaApi, loginmfaConfig, i18nUtils, widget) {

        // Object to store shared information between controllers
        $scope.master = {
            template: null,
            id: null,
            alerts: [],
            loading: false // Track a pending http request
        };

        // Object to store steps.
        // This will store data & actions needed for each step.
        $scope.steps = {};

        $scope.getMessages = function() {
            i18nUtils.loadMessages(widget, $scope.locale).success(function(bundle) {
                $scope.messages = bundle.messages;
            });
        };


        // Read and act based on server's response.
        // We can either move to a next step or hande a successful login.
        var handleStepResponse = function(response) {
            if (response.session.id) {
                $scope.sessionId = response.session.id;
            }

            if  ( response.session.status === loginmfaConfig.sessionVerifyResponse ) {
                // Create the session
                LoginmfaApi.doLogin($scope.master.id, $scope.sessionId)
                   .error(function(errors) {
                        $scope.master.loading = false;
                        handleErrorResponse(errors);
                    });
            } else {
                // Remove loading panel whne moving to next step
                $scope.master.loading = false;

                // Move to next step
                $scope.moveToStep(response.next.rel, response.next.actions);
            }
        };

        var handleErrorResponse = function(response) {
            if (response.errors) {
                angular.forEach(response.errors, function(error) {
                    $scope.addAlert(error.code);
                });
            } else {
                $scope.addAlert();
            }
        };

        // Move to another view.
        // If actions are specified, you can alter the possible actions of the next step.
        $scope.moveToStep = function(stepId, actions) {
            // Clear session id if we restart
            if ( stepId === loginmfaConfig.initialStep ) {
                $scope.sessionId = null;
            }

            if (!$scope.steps[stepId]) {
                $scope.steps[stepId] = {};
            }

            $scope.currentStep = $scope.steps[stepId];

            // Fetch new template
            var template = $scope.currentStep.template ? $scope.currentStep.template : stepId + '.html';
            $scope.master.template = loginmfaConfig.stepsDirectory + template;

            if ( ! angular.isUndefined(actions) ) {
                $scope.currentStep.actions = actions;
            }

            // Clear errors when we change step
            $scope.clearAlerts();
        };

        // Try to authenticate with server.
        // Next step is definied by server's response.
        $scope.connect = function(actionRel, data) {
            var actions = $scope.currentStep.actions,
                url;

            // URL is defined by the action rel of the available actions
            for (var i = 0, n = actions.length; i < n; i++) {
                if (actions[i].rel === actionRel) {
                    url = actions[i].uri;
                    break;
                }
            }

            if (url) {
	            url = util.replaceUrlVars(url, {
		            contextPath: util.getServicesPath(),
	                servicesPath: util.getServicesPath()
	            });
                url = util.replaceUrlVars(widget.getPreference('prefixSessionUrl'), {
                    contextPath: util.getServicesPath()
                }) + url;

                $scope.master.loading = true;

                return LoginmfaApi.connect($scope.sessionId, url, data)
                .success(handleStepResponse)
                .error(function(errors) {
                    handleErrorResponse(errors);
                    $scope.master.loading = false;
                 });
            }
        };

        $scope.addAlert = function(code, type) {
            if (code in $scope.messages === false) {
                code = loginmfaConfig.defaultErrorCode;
            }
            $scope.master.alerts.push({
                type: type || 'warning',
                msg: $scope.messages[code]
            });

            // Focus on errors element
            $scope.$broadcast('lp.login-multifactor.errors');
        };

        // Clear arr alert messages
        $scope.clearAlerts = function() {
            $scope.master.alerts = [];
        };


        $scope.onload = function() {
            $scope.$broadcast('lp.login.focus');
        };

        var initialize = function() {
            var initialStepId = loginmfaConfig.initialStep;

            // Define initial's step only valid action
            $scope.moveToStep( initialStepId, [{
                rel: 'initiate',
                uri: widget.getPreference('sessionUrl')
            }]);

            $scope.getMessages(); // Fetch messages
        };

        widget.addEventListener('preferencesSaved', function () {
            widget.refreshHTML();
            $scope.$apply(function() {
                initialize();
            });
        });

        initialize();
    }]);


    /**
     *
     */
    module.directive('placeholder', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var placeholder = attrs.placeholder;
                if (placeholder && $.fn.placeholder) {
                    $(element).placeholder();
                }
            }
        };
    });

    module.directive('lpAutofill', function () {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function (scope, element, attrs, ngModel) {
                scope.$on('autofill:update', function() {
                    ngModel.$setViewValue(element.val());
                });
            }
        };
    });


    return function(widget) {
        module.value('widget', widget);
        angular.bootstrap(widget.body, ['launchpad.loginmfa']);
    };
});