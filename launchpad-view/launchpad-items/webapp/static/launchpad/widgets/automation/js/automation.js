/*global gadgets, widget, console*/
define([
	'angular',
	'jquery',
	'launchpad/lib/common/util',
	'launchpad/lib/ui/responsive',
	'launchpad/lib/i18n',
	'launchpad/lib/ui',
	'launchpad/support/angular/angular-ui-bootstrap',
	'launchpad/support/angular/angular-resource.min',
	'launchpad/lib/common',
	'launchpad/lib/accounts',
	'launchpad/lib/automation',
	'launchpad/lib/automation/automation-model'
], function(angular, $, util, responsive) {

	'use strict';

	var module = angular.module('launchpad-retail.automation', ['i18n', 'ui', 'ui.bootstrap', 'common', 'accounts', 'automation']);

	module.controller('AutomationController', ['$scope', '$element', '$timeout', 'widget', 'AccountsModel', 'AutomationModel', function($scope, $element, $timeout, widget, AccountsModel, AutomationModel) {
		var initialize = function() {

			$scope.locale = widget.getPreference('locale');
			$scope.title =  widget.getPreference('title');

			$scope.automationModel = AutomationModel.getInstance({
				// automationsEndpoint: widget.getPreference('automationsDataSrc'),
				automationsEndpoint: '/portalserver/services/rest/automations/:automationId',
				locale: $scope.locale
			});
			
			$scope.automationModel.loadAutomations().then(function(response) {
				util.applyScope($scope);
			});

			$scope.accountsModel = AccountsModel.getInstance({
				accountsEndpoint: '/portalserver/services/rest/current-accounts?partyId=3'
				// accountsEndpoint: widget.getPreference("accountsDataSrc")
			});
			$scope.accountsModel.load();
		};

		$scope.createAutomation = function() {
			$scope.automationModel.automations.unshift({
				isNew: true,
				editing: true,
				showDetails: true
			});
		};

		$scope.toggleAutomationStatus = function(event, automation) {
			event.stopPropagation();

			$scope.automationModel.toggleStatus(automation).then(function(response) {
				console.log('toggleAutomationStatus', response);
			});
		};

		$scope.editAutomation = function(event, automation) {
			event.stopPropagation();

			if (!automation.editing) {
				automation.editing = true;
				automation.currentStep = 0;
				if (!automation.showDetails) {
					$scope.openDetails(automation);
				}
			} else {
				delete automation.editing;
				delete automation.currentStep;
			}
		};

		$scope.removeAutomation = function(event, automation) {
			event.stopPropagation();

			$scope.automationModel.remove(automation).then(function(response) {
				console.log('removeAutomation', response);
			});
		};

		$scope.openDetails = function(automation) {
			// automation.showDetails = !automation.showDetails;
			if (!automation.showDetails) {
				if (!automation.detailsLoaded) {
					$scope.automationModel.loadDetails(automation).then(function() {
						automation.showDetails = true;
					});
				} else {
					automation.showDetails = true;
				}
			} else {
				delete automation.showDetails;
			}
		};

		$scope.selectTrigger = function(automation, trigger) {
			automation.trigger = {};
			automation.trigger.type = trigger.type;
			automation.triggerIcon = trigger.icon;

			$scope.accountsModel.selected = {};
			automation.currentStep = 1;
		};

		$scope.selectAccount = function(automation) {
			automation.trigger.accountId = $scope.accountsModel.selected.id;
			automation.currentStep = 2;
		};

		$scope.selectAmount = function(automation) {
			if (automation.amountDirection === null || automation.amountDirection === undefined) {
				automation.amountDirection = $scope.automationModel.amountDirection[4];
			}
			automation.trigger.direction = automation.amountDirection.value;
			automation.trigger.amount = parseFloat(automation.trigger.amount);
			automation.currentStep = 3;
		};

		$scope.selectAction = function(automation, action) {
			automation.action = {};
			automation.action.type = action.type;
			automation.actionIcon = action.icon;
			automation.currentStep = 4;
		};

		$scope.selectEmail = function(automation) {

			automation.currentStep = 5;
		};

		$scope.saveAutomation = function(automation, enable) {
			automation.enabled = enable;
			if (automation.isNew) {
				$scope.automationModel.create({
					name: automation.name,
					enabled: enable,
					triggerIcon: automation.triggerIcon,
					actionIcon: automation.actionIcon,
					trigger: automation.trigger,
					action: automation.action
				}).then(function() {
					automation.editing = false;
					for (var i = 0; i < $scope.automationModel.automations.length; i++) {
						if ($scope.automationModel.automations[i].isNew === true) {
							$scope.automationModel.automations.splice(i, 1);
						}
					}
				});
			} else {
				automation.$update({
					name: automation.name,
					enabled: enable,
					trigger: automation.trigger,
					action: automation.action
				}).then(function() {
					automation.editing = false;
				});
			}

			// delete automation.amountDirection;
		};

		initialize();

	}]);

	return function(widget) {
		module.value('widget', widget);
		angular.bootstrap(widget.body, ['launchpad-retail.automation']);
	};
});