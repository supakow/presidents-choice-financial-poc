/*global b$, gadgets */
define(["jquery",
    "angular",
    "launchpad/lib/common/util",
    "launchpad/lib/accounts",
    "launchpad/lib/common/p2p-service",
    "launchpad/lib/i18n",
    "launchpad/lib/ui"], function($, angular, util) {

    "use strict";

    var app = angular.module("launchpad-retail.p2p-tab", ["i18n", "ui", "common", "accounts"]);

    /**
     * Angular Module & Controller
     */
    app.controller("P2PTabController", ['$scope', 'widget', 'P2PService', 'i18nUtils', function($scope, widget, P2PService, i18nUtils) {

        // Initialize
        var initialize = function () {

            $scope.p2pService = P2PService;

            $scope.p2pService.getUserEnrollmentDetails().then(function(response) {
                //success, we are enrolled!
                $scope.userEnrolled = true;
            }, function(response) {
                //error, we are not enrolled
                if(response.status === 404) {
                    $scope.userEnrolled = false;
                } else {
                    //error occurred
                    $scope.p2pService.error = true;
                }
            });

            //Options available in widget, list can be extended
            $scope.tabs = [
                {
                    title: "Sign Up",
                    pubsubMessage: "launchpad-retail.openP2PEnrollment",
                    visible: function() {
                        return !$scope.userEnrolled;
                    }
                },
                {
                    title: "Overview",
                    pubsubMessage: "launchpad-retail.openP2PTransactions",
                    visible: function() {
                        return $scope.userEnrolled;
                    }
                },
                {
                    title: "Preferences",
                    pubsubMessage: "launchpad-retail.openP2PPreferences",
                    visible: function() {
                        return $scope.userEnrolled;
                    }
                }
            ];

            i18nUtils.loadMessages(widget, $scope.locale).success(function(bundle) {
                //error messages
                $scope.messages = bundle.messages;
            });

            //Subscribe to this message to change the options available to choose from
            gadgets.pubsub.subscribe("launchpad-retail.p2pEnrollmentComplete", function(data) {
                //internal anonymous function, need to start digest cycle manually
                $scope.$apply(function() {
                    $scope.userEnrolled = data.verified;
                });
            });
        };


        /**
         * Load widget by behavior tag
         * @param pubsubMessage
         */
        $scope.loadP2PWidgetByBehavior = function(pubsubMessage, event) {

            if((event && event.which === 13) || !event) {
                gadgets.pubsub.publish(pubsubMessage);
            }
        };

        initialize();
    }]);

    return function(widget) {
        app.value("widget", widget);
        angular.bootstrap(widget.body, ["launchpad-retail.p2p-tab"]);
    };
});