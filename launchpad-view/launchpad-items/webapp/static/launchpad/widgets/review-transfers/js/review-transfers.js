/*global b$, gadgets console */
define([
    "jquery",
    "angular",
    "launchpad/lib/common/util",
    "launchpad/lib/ui/responsive",
    "launchpad/lib/i18n",
    "launchpad/lib/accounts",
    "launchpad/lib/common",
    "launchpad/lib/payments",
    "launchpad/lib/ui"
], function($, angular, util, responsive) {

    "use strict";

    var module = angular.module("launchpad-retail.reviewTransfersApp", ["i18n", "accounts", "payments", "ui", "ui.bootstrap", "common"]);

    // Configuration
    module.constant("reviewTransfersConfig", {
        statusSuccess: "Pending", // Success status response from server
        statusAuthorization: "AUTHORIZATION_NEEDED",
        statusAuthorizationError: "UNSUCCESSFUL_AUTHORIZATION"
    });


    module.controller("ReviewTransfersController", ["$scope", "$rootElement", "$timeout", "AccountsModel", "PaymentOrdersModel", "widget", "reviewTransfersConfig", "$log", "i18nUtils", "P2PService", "transferTypes",
    function($scope, $rootElement, $timeout, AccountsModel, PaymentOrdersModel, widget, reviewTransfersConfig, $log, i18nUtils, P2PService, transferTypes) {

        var p2pTransferMade = false, bankTransferMade = false;

        var initialize = function(){

            $scope.locale = widget.getPreference("locale");
            $scope.title =  widget.getPreference("title");

            $scope.transferTypeEnum = transferTypes;

            $scope.reviewModalShown = false;

            $scope.review = {
                modalShown: false,
                modalError: false
            };

            $scope.verify = {
                modalShown: false
            };

            /**
             * P2P functionality
             */
            $scope.p2pService = P2PService;

            $scope.p2pService.getUserEnrollmentDetails().then(function(response) {
                if(response.data) {
                    $scope.p2pUser = {
                        accountNumber: response.data.accountNumber,
                        email: response.data.email,
                        emailVerified: response.data.emailVerified
                    };
                } else {
                    $scope.p2pUser = {
                        emailVerified: false
                    };
                }

            }, function(response) {
                $scope.p2pUser = {
                    emailVerified: false
                };
            });

            gadgets.pubsub.subscribe("launchpad-retail.userP2PVerified", function(data) {
                $scope.p2pUser.emailVerified = data.verified;
            });

            gadgets.pubsub.subscribe("launchpad-retail.userP2PVerification.unverified", function() {
                $scope.p2pUser.emailVerified = false;
            });

            $scope.p2pPaymentOrders = []; //store unsent P2P payments here so widget doesn't think all po's are sent

            i18nUtils.loadMessages(widget, $scope.locale).success(function(bundle) {
                $scope.messages = bundle.messages;
            });

            $scope.ordersModel = new PaymentOrdersModel({
                pendingPaymentOrdersEndpoint: widget.getPreference("pendingPaymentOrdersDataSrc"),
                paymentOrdersSubmitPoint: widget.getPreference("paymentOrdersSubmitPoint")
            });

            $scope.accountsModel = AccountsModel.getInstance({
                accountsEndpoint: widget.getPreference("accountsDataSrc")
            });

            $scope.accountsModel.load().success(function() {
                $scope.ordersModel.load();
            });

            $scope.templates = {
                authorize: '/review-transfers/partials/authorize.html',
                verify: '/review-transfers/partials/verify-p2p-details.html'
            };

            $scope.ordersSubmitted = false;
        };

        $scope.makePayment = function(){
            // TODO: Launcher container workaround
            $scope.ordersSubmitted = false;
            gadgets.pubsub.publish("launchpad-retail.requestMoneyTransfer");
        };

        /**
         * P2P
         */
        var setValidCode = function() {
            //verification success
            $scope.isUserP2PVerified = true;
            $scope.verify.modalShown = false;
            $scope.p2pVerificationFailed = false;
            $scope.p2pUser.emailVerified = true;

            //reset warning on payment order objects
            angular.forEach($scope.ordersModel.pendingOrdersGroups, function(group) {
                if(group.paymentOrders.length > 0) {
                    angular.forEach(group.paymentOrders, function(po) {
                        if(po.transferType === transferTypes.p2pEmail) {
                            po.verificationFailure = false;
                        }
                    });
                }
            });

            //Set field validation
            $scope.verify.validationError = false;

            if($scope.verify.processOrder) {
                $scope.submitPayment($scope.currentOrder, $scope.review.authorization);
            }
        };

        var setInvalidCode = function() {
            //verification unsuccesful
            if($scope.verify.processOrder) {
                //if po's are being submitted
                $scope.verify.modalShown = false;
                $scope.p2pVerificationFailed = true; //don't show pop up multiple times
                $scope.currentOrder.verificationFailure = true;
                $scope.submitPayment($scope.currentOrder, $scope.review.authorization);
            } else {
                //set field validation, don't close modal
                $scope.verify.validationError = true;
            }
        };

        $scope.verifyP2PDetails = function(order, process) {
            //if verification has already failed, do not show modal again
            if($scope.p2pVerificationFailed) {
                order.verificationFailure = true;
                $scope.currentOrder = order;
                $scope.submitPayment($scope.currentOrder, $scope.review.authorization);
            } else {
                //show modal
                $scope.verify.modalShown = true;
                $scope.verify.verificationCode = '';
                $scope.verify.processOrder = process;
                $scope.currentOrder = order;
            }
        };

        //form submit
        $scope.handleP2PVerification = function() {
            P2PService.verifyCode($scope.p2pUser.email, $scope.verify.verificationCode).then(function(response) {
                if(response.status === 200) {
                    setValidCode();
                }
            }, function(response) {
                if(response.status === 409) {
                    setInvalidCode();
                }
            });
        };


        //Cancel button clicked
        $scope.closeVerifyModal = function() {
            setInvalidCode();
            $scope.verify.modalShown = false;
        };

        $scope.$watch("verify.verificationCode", function(newValue, oldValue) {
            //reset validation
            $scope.verify.validationError = false;
        });
        //p2p end

        // Submit all pending payments
        $scope.submitPayments = function() {
            var pendingOrders = [];

            angular.forEach($scope.ordersModel.pendingOrdersGroups, function(group) {
                pendingOrders = pendingOrders.concat(group.paymentOrders);
            });

            $scope.pendingOrders = pendingOrders;
            processOrdersSeq();
        };

        // Submit pending orders sequentially
        var processOrdersSeq = function() {
            if ( $scope.pendingOrders.length === 0 && $scope.p2pPaymentOrders.length === 0) {
                $scope.ordersSubmitted = true;
                refreshTransactionWidgets();
                return;
            }

            // Get the first order
            var order = $scope.pendingOrders.pop();

            if(order) {
                //if p2p po, add to list and reset verification status
                if(order.type === transferTypes.p2pEmail) {
                    order.verificationFailure = false;
                    $scope.p2pPaymentOrders.push(order);
                }

                $scope.submitPayment(order);
            } else {
                //pending orders list is empty, reset p2p validation and array
                $scope.p2pPaymentOrders = [];
                $scope.p2pVerificationFailed = false;
            }
        };

        var openModal = function(order) {
            $scope.review.modalShown = true;
            $scope.review.modalError = false;
            $scope.review.authorization = '';

            $scope.currentOrder = order;
        };

        $scope.authorizePayment = function() {
            $scope.submitPayment($scope.currentOrder, $scope.review.authorization);
            $scope.review.modalShown = false;
        };

        // Submit a specific pending payment
        $scope.submitPayment = function(order, authorization) {
            if(order.verificationFailure) {
                processOrdersSeq();
                return;
            }

            //if the user's P2P details need to verified
            if(order.type === transferTypes.p2pEmail && !$scope.p2pUser.emailVerified) {
                $scope.verifyP2PDetails(order, true);
                return;
            }

            var groups = $scope.ordersModel.pendingOrdersGroups,
                group = null;

            // Find the group containing this order
            for (var i = 0, n = groups.length; i < n; i++) {
                if (groups[i]['@id'] === order.accountId) {
                    group = groups[i];
                    break;
                }
            }

            if ( !group ) {
                $log.error("Error: Order " + order.id + " does not belong to any group.");
                return;
            }

            return $scope.ordersModel.submit(order.id, authorization).success(function(response) {

                //refresh transactions?
                if(order.type === transferTypes.bank) {
                    bankTransferMade = true;
                } else if(order.type === transferTypes.p2pEmail) {
                    p2pTransferMade = true;
                }

                if (response.status === reviewTransfersConfig.statusSuccess) {
                    var orders = group.paymentOrders;

                    orders.splice(orders.indexOf(order), 1);

                    if (orders.length === 0) {
                        groups.splice(groups.indexOf(group), 1);
                    }

                    //remove p2p orders from list once finally submitted
                    if($scope.p2pPaymentOrders.indexOf(order) > -1) {
                        $scope.p2pPaymentOrders.splice($scope.p2pPaymentOrders.indexOf(order), 1);
                    }

                    // Successfuly executed order. Keep processing...
                    processOrdersSeq();
                } else if (response.status === reviewTransfersConfig.statusAuthorization) {
                    openModal(order);
                } else if (response.status === reviewTransfersConfig.statusAuthorizationError) {
                    openModal(order);
                    $scope.review.modalError = 'Authorization error.';
                } else {
                    $log.error("Error: Order " + order.id + " didn't succeed.");
                }
            });
        };

        /**
         * Publish pub sub messages to instruct other widgets to refresh transaction lists
         */
        var refreshTransactionWidgets = function() {

            if(bankTransferMade) {
                gadgets.pubsub.publish("launchpad-retail.transactions.newTransferSubmitted");
            }

            if(p2pTransferMade) {
                gadgets.pubsub.publish("launchpad-retail.p2pTransactions.newTransferSubmitted");
            }

            //reset flags for next group of transfers
            bankTransferMade = false;
            p2pTransferMade = false;
        };

        widget.addEventListener("preferencesSaved", function () {
            widget.refreshHTML();
            $scope.$apply(function() {
                initialize();
            });
        });

        gadgets.pubsub.subscribe("launchpad-retail.paymentOrderInitiated", function(){
            $timeout(function() {
                $scope.ordersModel.load(true);
            });
        });

        // Responsive
        responsive.enable($rootElement)
            .rule({
                "max-width": 200,
                then: function() {
                    $scope.responsiveClass = "lp-tile-size";
                    util.applyScope($scope);
                }
            })
            .rule({
                "min-width": 201,
                "max-width": 400,
                then: function() {
                    $scope.responsiveClass = "lp-small-size";
                    util.applyScope($scope);
                }
            }).rule({
                "min-width": 401,
                then: function() {
                    $scope.responsiveClass = "lp-large-size";
                    util.applyScope($scope);
                }
            });

        initialize();
    }]);

    return function(widget) {
        module.value("widget", widget);
        angular.bootstrap(widget.body,["launchpad-retail.reviewTransfersApp"]);
    };

});