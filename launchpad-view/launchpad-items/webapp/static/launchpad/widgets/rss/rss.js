/*global window be gadgets*/

/**
 * RSS Widget
 *
 * @author predrag@backbase.com
 *
 * @param {Object} Mustache object
 * @param {Object} jQuery object
 * @param {Object} Responsive object
 */
define(["mustache",
        "jquery",
        "launchpad/lib/ui/responsive",
        "launchpad/lib/common/util",
        "jquery-ui",
        "launchpad/support/jquery/ui-resizable",
        "launchpad/support/jquery/base64",
        "launchpad/support/md5"], function(Mustache, $, Responsive, util) {

    "use strict";

    var DEBUG = true;
    var DEBUG_PREFIX = "RSS Widget";
    var _log = util.getLogger(DEBUG, DEBUG_PREFIX);


    /**
     * Constructor
     *
     * @param widget
     * @constructor
     */
    var Rss = function(widget) {
        _log("Rss()");
        var self = this;

        this.widget = widget;
        this.$widget = $(widget.body);
        this.$news = this.$widget.find(".news"); // news container
        this.$resizable = $(".news-list", self.$widget);
        this.$list = this.$news.find(".list-content"); // news list container
        this.$content = this.$news.find(".news-content"); // news content container
        this.read = []; // read article links

        this.responsive = Responsive.enable(widget.body);
    };


    /**
     * Initialization
     *
     */
    Rss.prototype.init = function() {
        _log("init()");
        var self = this;

        // bind the event handlers
        this.bindEvents();

	    this.responsive.rule({
		    "min-width" : 120,
		    then: function() {
			    var template = self.getTemplate('big');
			    self.renderWidget(name, template);
		    }
	    });
    };


    /**
     * Bind the event handlers
     *
     */
    Rss.prototype.bindEvents = function() {
        _log("bindEvents()");
        var self = this;

        this.widget.addEventListener("PerspectiveModified", function(e) {
            _log('PerspectiveModified', e);
            if (e.newValue !== "Minimized") {
                // get the news list
                self.getNews();
            }
        });

        this.widget.model.addEventListener("PrefModified", function(e) {
            _log('PrefModified', e);
            if (e.attrName === "feedSource") {
                self.$list.empty();
                self.$content.empty();
                self.getNews();
            }
        });

        // delegate onClick event
        this.$widget.on("click", ".news-list .trig", function(e) {
            e.preventDefault();
            // get the content for particular news
            self.getContent($(this).parent());
        });

        this.$widget.on("click", ".news-list .lp-icon-bullet", function(e) {
            self.markRead($(this).parent(), true);
            e.stopPropagation();
        });
    };


    /**
     * Render widget in different sizes
     *
     * @param size
     * @param template
     */
    Rss.prototype.renderWidget = function(size, template) {
        var self = this;
        _log('renderWidget()', size);

        self.$widget.removeClass('responsive-small responsive-medium responsive-large')
            .addClass('responsive-'+size)
            .find('.content')
            .html(Mustache.to_html(template));

        self.$news = this.$widget.find(".news"); // news container
        self.$resizable = $(".news-list", self.$widget);
        self.$list = this.$news.find(".list-content"); // news list container
        self.$content = this.$news.find(".news-content"); // news content container

        self.getNews();
    };


    /**
     *
     * @param name
     * @return {*|jQuery}
     */
    Rss.prototype.getTemplate = function (name) {
        return $('[data-template="'+name+'"]', this.$widget).html();
    };


    /**
     * Initialize the splitter
     *
     */
    Rss.prototype.initSplitter = function() {
        _log("initSplitter()");
        var self = this;

        if ($('.ui-resizable-handle').length) {
            return;
        }

        // initialize the splitter
        self.$resizable.resizable({
            handles: "e",
            minWidth: 200,
            maxWidth: 540,
            resize: function(e, ui) {
                var totalWidth = ui.element.parent().width();
                var elementWidth = ui.element.outerWidth();
                var remainingSpace = totalWidth - elementWidth;
                var rightDiv = ui.element.next();
                var rightWidth = Math.floor(remainingSpace / totalWidth * 100);
                rightDiv.css('width', rightWidth +'%');
                ui.element.css('width', 99 - rightWidth +'%');
                //var divTwoWidth = remainingSpace - (divTwo.outerWidth() - divTwo.width());
                //divTwo.css('width', divTwoWidth +'px');
                //_log('resize event!', ui.element.parent().width(), ui.element.outerWidth(), remainingSpace);
            }
        });

        // set the list height
        var height = self.$widget.height();
        self.$content.height(height);
        self.$list.height(height);
    };


    /**
     * Call the rssPipe with a parameter 'list' for XSLT to render the news list
     *
     */
    Rss.prototype.getNews = function() {
        _log("getNews()");
        var self = this;
        var src = this.widget.getPreference("feedSource"); // || 'http://blog.backbase.com/feed/rss/';
        var url = gadgets.io.getProxyUrl(src, {
            pipe: "rssPipe",
            mode: "list"
        });

        // start loading animation
        this.$list.empty().addClass("loading");
        $.ajax({
            url: url,
            method: "get",
            dataType: "html",
            success: function(data, status, response) {
                data = $(data);

                // stop loading animation
                self.$news.removeClass("loading");

                // append the new list to the DOM
                self.$list.fadeOut("fast", function() {
                    var links = data.find('.item[data-link]');
                    var $link, hash;
                    $.each(links, function(index, link) {
                        $link = $(link);
                        hash = window.lp.MD5.hash($link.data('link'));
                        $link.attr('data-hash', hash);
                        $link.data('hash', hash);
                    });

                    self.$list
                        .prepend(data)
                        .find(".item header .date").each(function(index, item) {
                            var $item = $(item);
                            var date = new Date($item.text());
                            var day = date.getDate();
                            var year = date.getFullYear() + "";
                            $item.text(((day < 10) ? "0" + day : day) + "/" + year.substr(-2));
                        });

                    // get read article items
                    self.read = (self.widget.getPreference("readArticles") || "").split(",");

                    $.each(self.read, function(index, hash) {
                        self.$list.find('.item[data-hash="'+ hash +'"]').addClass("read");
                    });

                    // finally show the items
                    self.$list.fadeIn();

                    // click on the first list item
                    self.getContent(self.$list.find(".list .item:first"));
                });
            },
            complete: function(response, status) {
                // stop loading animation
                self.$list.removeClass("loading");
                if (!response.responseText) {
                    self.$list.html('<p style="padding:20px;text-align:center;">Content not loaded.</p>');
                }

                // initialize the splitter
                self.initSplitter();
            },
            error: function(resonse, status, e) {
                // stop loading animation
                self.$list.removeClass("loading");
                self.$list.html('<p style="padding:20px;text-align:center;">Error loading content.</p>');
            }
        });
    };


    /**
     * Fetch the news content
     *
     * @param $item Object
     * @return void
     */
    Rss.prototype.getContent = function($item) {
        _log("getContent()");
        var self = this;
        this.$content.fadeOut("fast", function() {
            $(this).empty().addClass("loading").fadeIn();

            var link = $item.data("link");
            var src = self.widget.getPreference("feedSource"); // || "http://blog.backbase.com/feed/rss/";
            var url = gadgets.io.getProxyUrl(src, {
                pipe: "launchpadRssPipe",
                mode: "content",
                link: link
            });

            $.ajax({
                url: url,
                method: "get",
                dataType: "html",
                beforeSend: function(xhr, settings) {
                    $item.addClass("reading");
                },
                success: function(data, status, response) {
                    self.$content.hide().html(data); //$content.html(data).delay(200).fadeTo(800, 1);

                    // format date
                    var $date   = self.$content.find(".header .date");
                    var date    = new Date($date.text());
                    var hours   = date.getHours(); hours = (hours > 12) ? hours - 12 : hours;
                    var minutes = date.getMinutes(); minutes = (minutes < 10) ? '0'+minutes : minutes;
                    var ampm    = (hours > 12) ? 'PM' : 'AM';
                    var time    = hours +':'+ minutes +' '+ ampm;
                    $date.html($.datepicker.formatDate('DD, MM d, yy', date) +' '+ time);

                    // print button
                    var $print  = self.$content.find(".header .buttons .btn.lp-rss-print");
                    $print.attr("href", be.contextRoot +"/static/launchpad/widgets/rss/print.html?url=" + $.base64.encode(gadgets.io.getProxyUrl(src, {
                        pipe: "rssPipe",
                        mode: "print",
                        link: link
                    })));

                    // fix the image width
                    var width = self.$content.innerWidth() - 20; // - scrollbar width
                    self.$content.find('[width]').attr('width', 'auto');
                    self.$content.find('[style]').each(function(i, e) {
                        var $e = $(e);
                        if ($e.outerWidth(true) > width) {
                            $e.width('100%');
                        }
                    });

                    // show the content
                    self.$content.fadeIn();

                    // mark article as read
                    self.markRead($item);
                },
                error: function(resonse, status, e) {
                    // stop loading animation
                    $item.removeClass("reading");
                    self.$list.removeClass("loading");
                    self.$content.html('<p style="padding:20px;text-align:center;">Error loading content.</p>');
                },
                complete: function(response, status) {
                    // stop loading animation
                    self.$content.removeClass("loading");
                    if (!response.responseText) {
                        self.$content.html('<p style="padding:20px;text-align:center;">Content not loaded.</p>');
                    }
                }
            });
        });
    };


    /**
     * Mark as read
     *
     * @param $item Object
     * @param isMarked boolean if item is marked
     * @return void
     */
    Rss.prototype.markRead = function($item, isMarked) {
        _log("markRead()");
        var self = this;
        var link = $item.data("link");
        var hash = $item.data("hash");

        if (isMarked) {
            $item.toggleClass("read");
            var artI = this.read.indexOf(hash);
            if (artI >= 0) {
                this.read.splice(artI, 1);
            }
        } else {
            $item.addClass("read reading").siblings('li').removeClass('reading');
            if (!this.read[hash]) {
                this.read.push(hash);
            }
        }

        var pref = this.read.slice(-10).join();
        pref = (pref.charAt(0) === ',') ? pref.substr(1) : pref;

        this.widget.model.setPreference("readArticles", pref);
        this.widget.model.save();
        //_log(this.widget.model.getPreference('readArticles'));
    };


    return function(widget) {
        var widgetWrapper = new Rss(widget);
        widgetWrapper.init();
        return widgetWrapper;
    };
});
