/*global window */
jQuery(function($) {

    "use strict";

    var $target = $("#lp-widget-print-container");

    /**
     * Get URL parameter by name
     *
     * @param key String
     * @return String
     */
    function getParameter(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");

        var regexS  = "[\\?&]"+ name +"=([^&#]*)";
        var regex   = new RegExp(regexS);
        var results = regex.exec(window.location.search);

        if (!results) {
            return "";
        }
        else {
            return decodeURIComponent(results[1].replace(/\+/g, " "));
        }
    }

    /**
     * Get RSS article content
     *
     * @param url String
     * @return void
     */
    function getArticle(url) {
        $.ajax({
            url: url,
            method: "get",
            dataType: "html",
            success: function(data, status, response) {
                $target.hide().html(data);
                $target.find("button.lp-icon-print").click(function() {
                    window.print();
                });
                $target.fadeIn();
                //var link = $target.find('.header').data('link');
            },
            error: function(resonse, status, e) {
                window.console.info(arguments);
            },
            complete: function(response, status) {
                $("body").removeClass("loading");
            }
        });
    }

    /**
     * Get URL parameter by name
     *
     * @param key String
     * @return String
     */
    var main = function() {
        var url = $.base64.decode(getParameter("url"));
        window.console.log(url);
        getArticle(url);
    };

    main();
});
