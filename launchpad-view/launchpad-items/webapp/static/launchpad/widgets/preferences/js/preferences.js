/*global b$, gadgets */
define([
    "jquery",
    "angular",
    "launchpad/lib/common/util",
    "launchpad/lib/common/rest-client",
    "launchpad/lib/i18n",
    "launchpad/lib/ui",
    "launchpad/lib/user",
    "launchpad/lib/accounts",
    "launchpad/lib/common/preference-service"
], function($, angular, util, restClient) {

    "use strict";

    var module = angular.module("preferencesWidget", ["user", "i18n", "ui", "ui.bootstrap", "accounts" ]);

    /**
    * Landing links service
    */
    module.service('LandingLinksService', ['$http', '$rootScope', 'widget', function($http, $rootScope, widget) {
        //Fix for IE8 (no window.location.origin)
        if (!window.location.origin) {
          window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: '');
        }

        var params = {
            server: window.location.origin || "",
            contextPath: util.getContextPath(),
            contextItemName: b$.portal.portalName,
            name: "",
            query: "?depth=1"
        };

        this.read = function(name) {
            //prevents caching behavior in IE8
            var rand = Math.random() + "";
            var num = rand * 1000000000000000000;
            
            if(name) {
                params.name = name;    
            }
            var requestObj = restClient.makeGetLinksRequest(params);
            return $http.get(requestObj.url + "&qi=" + num, null, {});
        };

    }]);

    module.controller("PreferencesController", ["$scope", "$rootElement", "widget", "i18nUtils", "AccountsModel", "PreferenceService", "LandingLinksService", function($scope, $rootElement, widget, i18nUtils, AccountsModel, PreferenceService, LandingLinksService) {
        
        var initialize = function() {
            $scope.title =  widget.getPreference("title");    

            $scope.control = {
                preferredName: {
                    value: "",
                    errors: [],
                    loading: false,
                    validate: function(name) {
                        if(name.length < 3) {
                            return "invalid_name";
                        }
                        return true;
                    }
                },  
                locale: {
                    value: "",
                    options: [
                        {
                            "value": "en",
                            "text": "English"
                        },
                        {
                            "value": "fr",
                            "text": "French"
                        },
                        {
                            "value": "nl",
                            "text": "Dutch"
                        },
                        {
                            "value": "it",
                            "text": "Italian"
                        }
                    ],
                    loading: false
                },
                defaultView: {
                    value: "",
                    options: [],
                    loading: false
                },
                defaultAccount: {
                    value: "",
                    options: [],
                    loading: false
                },
                preferredBalanceView: {
                    value: "",
                    options: [
                        {
                            "value": "current",
                            "text": "Use 'Current Balance'"
                        },
                        {
                            "value": "available",
                            "text": "Use 'Available Balance'"
                        }
                    ],
                    loading: false
                },
                pfm: {
                    value: "",
                    options: [
                        {
                            "value": true,
                            "text": "Enabled"
                        },
                        {
                            "value": false,
                            "text": "Disabled"
                        }
                    ],
                    loading: false
                }
            };

            $scope.accountsModel = AccountsModel.getInstance({
                accountsEndpoint: widget.getPreference("accountsDataSrc")
            });

            var accountsPromise = $scope.accountsModel.load();
            accountsPromise.success(function() {
                $scope.read();
            });
        };

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Parse properties from XML
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        var processXMLFromService = function(response) {
            var xmlDoc = $.parseXML(response);

            var $response = $(xmlDoc), view;
            $response.find("properties").each(function() {
               view = parseProperties($(this));
               if(view !== null) {
                    $scope.control.defaultView.options.push(view);
                    /**
                    * successView is the name of the property in the back end
                    * defaultView is more apt on the client side 
                    */
                    $scope.control.defaultView.value = widget.getPreferenceFromParents("successView") ? widget.getPreferenceFromParents("successView") : $scope.control.defaultView.options[0];
               }
               
            });
        };

        var parseProperties = function($propertyList) {
            var obj = {}, attrList = [], propertyList = [], value = "";

            $propertyList.find("property").each(function() {
                attrList = this.attributes;

                for(var i = 0; i < attrList.length; i++) {
                    obj[attrList[i].name] = attrList[i].value;
                }
                obj.value = $(this).find("value").text();                
                propertyList.push(obj);
                obj = {};
            });

            return filterLandingProperty(propertyList);
        };

        var filterLandingProperty = function(propertyList) {
            var found = false, propList = [], obj = {};

            for(var i = 0; i < propertyList.length; i++) {
                if(propertyList[i].name === "landing" && propertyList[i].value === "true") {
                    found = true;
                    break;
                }
            }

            if(found) {
                return parseLandingView(propertyList, obj);
            } else {
                return null;
            }
        };

        /**
        * Parses the landing property element into a JS object to load into options of control.defaultView
        */
        var parseLandingView = function(propertyList, obj) {
            for(var j = 0; j < propertyList.length; j++) {
                if(propertyList[j].name === "title") {
                    obj.text = propertyList[j].value;
                }
                if(propertyList[j].name === "Url") {
                    obj.value = "/" + propertyList[j].value;
                }
            }
            return obj;
        };

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        $scope.save = function(key, value) {

            var a = util.setMasterPreference(key, value);

            if (key === 'preferredName') {
                PreferenceService.save(key, util.escapeHtml(value));
            }
            if (key === 'pfm') {
                PreferenceService.save('pfmEnabled', util.escapeHtml(value));
            }
        };

        $scope.read = function() {            
            //Preferred name
            PreferenceService.read().success(function(response) {
                $scope.control.preferredName.value = response.preferredName;
                $scope.control.pfm.value = util.parseBoolean(response.pfmEnabled);
            });

            //Locale
            $scope.control.locale.value = widget.getPreferenceFromParents("locale");

            //Default View
            LandingLinksService.read().success(processXMLFromService);

            //Default account
            $scope.control.defaultAccount.options = $scope.accountsModel.accounts;
            $scope.control.defaultAccount.value = verifyDefaultAccount(widget.getPreferenceFromParents("defaultAccount")) ? widget.getPreferenceFromParents("defaultAccount") : $scope.control.defaultAccount.options[0].bban;

            //Preferred balance view
            $scope.control.preferredBalanceView.value = widget.getPreferenceFromParents("preferredBalanceView") ? widget.getPreferenceFromParents("preferredBalanceView") : $scope.control.preferredBalanceView.options[0].value;

            util.applyScope($scope);
        };

        //verifies that the default account is present in the current list of accounts
        var verifyDefaultAccount = function(accountNumber) {
            var found = false, accounts = $scope.control.defaultAccount.options;
            for(var i = 0; i < accounts.length; i++) {
                if(accounts[i].bban === accountNumber) {
                    found = true;
                    break;
                }
            }

            return found;
        };

        initialize();
    }]);

    return function(widget) {
        module.value("widget", widget);
        angular.bootstrap(widget.body,["preferencesWidget"]);
    };
});