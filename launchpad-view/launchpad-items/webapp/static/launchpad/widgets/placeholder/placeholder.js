/*global console */
define(["jquery",
		"launchpad/lib/services-pool"
	], function($, services) {
    
    "use strict";

    return function() {

	    var service = services.getService("/consumer-products/summary");
	    var resp = service.read();
	    resp.done(function(data) {
		    console.log("Here's my data", data);
	    });



    };
});
