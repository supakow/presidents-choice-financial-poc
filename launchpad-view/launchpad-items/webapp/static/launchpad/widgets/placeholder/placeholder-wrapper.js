/*global console */
/**
 * This file demonstrates using a widget wrapper object
 */
define(["jquery", "mustache", "local-module"], function($, Mustache, localModule) {
    
    "use strict";
    
    /**
     * 
     * @constructor
     * @extends LaunchpadWidget
     */
    var PlaceHolder = function(widget) {
        
        this.widget = widget;
        this.$widget = $(this.widget.body);
    };
    
    PlaceHolder.prototype.init = function() {

        this.$widget.on("sizechange", function() {
            console.log("size change");
        });
        
        this.tmpl = this.$widget.find("script[data-template=comments]")[0].innerHTML;

        this.loadData();
        this.render();
    };
    
    PlaceHolder.prototype.loadData = function() {
        
        this.data = [
            { "name" : "Nikos"},
            { "name" : "Alexey"},
            { "name" : "Andrea"}
        ];
    };
    
    PlaceHolder.prototype.render = function() {
        
        var html = Mustache.render(this.tmpl, this.data);
        this.$widget.html(html);
    };

    return PlaceHolder;

});
