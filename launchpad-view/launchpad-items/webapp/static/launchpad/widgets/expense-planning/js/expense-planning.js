/*global b$, gadgets console */
define([
    "jquery",
    "angular",
    "launchpad/lib/common/util",
    "launchpad/lib/ui/responsive",
    "launchpad/support/angular/angular-ui-bootstrap",
    "launchpad/lib/i18n",
    "launchpad/lib/expenses",
    "launchpad/lib/ui"
], function($, angular, util, responsive) {

    "use strict";

    var module = angular.module("launchpad-retail.expensePlanningApp", ["i18n", "expenses", "ui", "ui.bootstrap"]);

    module.controller("ExpensePlanningController", ["$scope", "$rootElement", "$timeout", "widget", "$log", "i18nUtils", 'ExpensesModel',
        function($scope, $rootElement, $timeout, widget, $log, i18nUtils, ExpensesModel) {

            //var self = this;
            var initialize = function() {

                // create new model with these variables
                $scope.locale = widget.getPreference("locale");
                $scope.title = widget.getPreference("title");
                i18nUtils.loadMessages(widget, $scope.locale).success(function(bundle) {
                    $scope.messages = bundle.messages;
                });
                $scope.templates = {
                    list: '/expense-planning/partials/list.html'
                };

                /*Calendar cases */
                $scope.expensesModel = ExpensesModel.getInstance({
                    expensesEndpoint: widget.getPreference("expensesDataSrc"),
                    expensesDetailsEndpoint: widget.getPreference("expensesDetailsDataSrc")
                });
                $scope.today = new Date();
                $scope.lastDay = new Date($scope.today.getFullYear(), 12, 0, 23, 59, 59);
                var promise = $scope.expensesModel.loadExpenses($scope.today, $scope.lastDay);
                promise.then(function() {
                    $scope.calendarPayments = $scope.expensesModel.expenses;
                    $scope.paymentListOnDates = $scope.expensesModel.findByDate($scope.today);
                });
            };

            $scope.showList = function(obj) {
                $scope.paymentListOnDates = $scope.expensesModel.findByDate(obj);
            };

            $scope.showDetail = function(payment) {
                $scope.expensesModel.loadExpensesDetails(payment);
            };

            $scope.refreshContent = function(date) {
                if($scope.lastDay < date) {
                    var newLastDay = new Date($scope.lastDay.getFullYear()+1, 12, 0, 23, 59, 59);
                    $scope.expensesModel.loadExpenses($scope.lastDay, newLastDay);
                    $scope.lastDay = newLastDay;
                }
            };

            widget.addEventListener("preferencesSaved", function() {
                widget.refreshHTML();
                $scope.$apply(function() {
                    initialize();
                });
            });


            // Responsive
            responsive.enable($rootElement)
                .rule({
                    "max-width": 200,
                    then: function() {
                        $scope.responsiveClass = "lp-tile-size";
                        $scope.handleLargeScreen = false;
                        util.applyScope($scope);
                    }
                })
                .rule({
                    "min-width": 201,
                    "max-width": 400,
                    then: function() {
                        $scope.responsiveClass = "lp-small-size";
                        $scope.handleLargeScreen = false;
                        util.applyScope($scope);
                    }
                })
                .rule({
                    "min-width": 401,
                    "max-width": 624,
                    then: function() {
                        $scope.responsiveClass = "lp-medium-size";
                        $scope.handleLargeScreen = false;
                        util.applyScope($scope);
                    }
                })
                .rule({
                    "min-width": 625,
                    "max-width": 915,
                    then: function() {
                        $scope.responsiveClass = "lp-large-size";
                        $scope.handleLargeScreen = false;
                        util.applyScope($scope);
                    }
                })
                .rule({
                    "min-width": 915,
                    then: function() {
                        $scope.responsiveClass = "lp-large-size";
                        $scope.handleLargeScreen = true;
                        util.applyScope($scope);
                    }
                });

            initialize();
        }
    ]);


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Detail controller
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    return function(widget) {
        module.value("widget", widget);
        angular.bootstrap(widget.body, ["launchpad-retail.expensePlanningApp"]);
    };

});