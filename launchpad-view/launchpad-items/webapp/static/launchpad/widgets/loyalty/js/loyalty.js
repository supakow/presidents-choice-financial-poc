/*global b$, gadgets, console */
define([
    "jquery",
    "angular",
    "launchpad/lib/common/util",
    "launchpad/lib/ui/responsive",
    "launchpad/lib/common"
], function($, angular, util, responsive, common) {

    "use strict";

    var module = angular.module("launchpad-loyalty", ["common"]);

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Main controller
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    module.controller("loyaltyController", [ '$scope', '$element', 'httpService', 'widget',
            function($scope, $element, httpService, widget) {

                var loyaltiesService;    // http service

                function initialize() {
                    $scope.loyaltyData = [];


                    loyaltiesService = httpService.getInstance({
                        endpoint: widget.getPreference("loyaltiesSrc")
                    });

                    readLoyaltyData();
                }
     
                 /**
                 * Fetch data from source.
                 */
                function readLoyaltyData() {

                    loyaltiesService.read({}, true)
                        .success(function(data) {
                            $scope.loyaltyData = data;
                        })
                        .error(function() {
                        }
                    );
                }

                initialize();
            }]);


    return function(widget) {
        module.value("widget", widget);
        angular.bootstrap(widget.body, ["launchpad-loyalty"]);
    };
});