/*global b$, gadgets */
define(["jquery",
    "angular",
    "launchpad/lib/common/util",
    "launchpad/lib/accounts",
    "launchpad/lib/common",
    "launchpad/lib/common/p2p-service",
    "launchpad/lib/i18n",
    "launchpad/lib/ui"], function($, angular, util) {

    "use strict";

    var app = angular.module("launchpad-retail.p2p-enrollment", ["i18n", "ui", "common", "accounts"]);


    app.directive("stepFocus", [function() {

        return {
            restrict: 'A',
            scope: {
                'step': '=stepFocus'
            },
            link: function(scope, element) {

                scope.$watch("step", function(newValue) {

                    if(newValue === 2) {
                        element.focus();
                    }
                });
            }
        };
    }]);


    /**
     * Angular Module & Controller
     */
    app.controller("P2PEnrollmentController", ['$scope', 'widget', 'AccountsModel', 'P2PService', 'i18nUtils', 'ProfileContactService', function($scope, widget, AccountsModel, P2PService, i18nUtils, ProfileContactService) {


        // Initialize
        var initialize = function () {


            P2PService.getUserEnrollmentDetails().then(function(response) {
                $scope.userEnrolled = true;
            }, function(response) {
                $scope.userEnrolled = false;

                if(response.status !== 404) {
                    $scope.errorOccurred = false;
                }
            });

            $scope.p2pService = P2PService;

            $scope.errorOccurred = false;

            /**
             * Manages enrollment form
             * @type {{email: {value: string, valid: boolean}, depositAccount: {value: string, options: Array}}}
             */
            $scope.options = {
                email: {
                    value: "",
                    valid: false
                },
                depositAccount: {
                    value: "",
                    options: []
                }
            };

            ProfileContactService.read().success(function(response) {

                $scope.options.email.value = response.emailAddress === [] ? '' : response.emailAddress;
                $scope.validateEmail($scope.options.email.value);
            });

            //initialize accounts
            $scope.accountsModel = AccountsModel.getInstance({

                accountsEndpoint: widget.getPreference("accountsDataSrc")
            });

            $scope.accountsModel.load().success(function() {

                //set the options and default selection of the accounts dropdown
                $scope.options.depositAccount.options = $scope.accountsModel.accounts;
                $scope.options.depositAccount.value = $scope.accountsModel.accounts[0].id;
            });

            i18nUtils.loadMessages(widget, $scope.locale).success(function(bundle) {
                //error messages
                $scope.messages = bundle.messages;
            });

            /**
             * manages verification of email address
             * @type {{code: string, error: boolean}}
             */
            $scope.verification = {
                code: "",
                error: false
            };

            $scope.terms = {
                doesAgreeWithTerms: false
            };
        };

        /**
         * validates email using regex and assigns it to the email error
         * @param email
         */
        $scope.validateEmail = function(email) {
            $scope.errorOccurred = false;

            var result = util.isValidEmail(email);

            $scope.options.email.valid = result;
        };

        /**
         * move to the next step
         */
        $scope.toNextStep = function(event) {

            event.preventDefault();
            event.stopPropagation();

            $scope.errorOccurred = false;

            if($scope.getActiveWizardStep() === 1) {
                doP2PEnrollment();
            } else if($scope.getActiveWizardStep() === 2) {
                doP2PVerification();
            } else if($scope.getActiveWizardStep() === 3) {
                $scope.openP2PTransfers();
            }
        };

        /**
         * publish a message to open the P2P Preferences widget
         */
        $scope.openP2PTransfers = function() {
            gadgets.pubsub.publish("launchpad-retail.p2pEnrollmentComplete", {
                verified: true
            });

            gadgets.pubsub.publish("launchpad-retail.openP2PTransactions");
        };

        /**
         * Enroll the user
         */
        var doP2PEnrollment = function() {
            //intial enrollment
            if(!$scope.options.email.valid || !$scope.terms.doesAgreeWithTerms) {
                //error
                $scope.errorOccurred = true;
                return;
            } else {
                //send request to back end
                $scope.p2pService.enrollUserForP2P({
                    email: $scope.options.email.value,
                    accountNumber: $scope.accountsModel.findById($scope.options.depositAccount.value).iban
                }).then(function(response) {
                    //success
                    if(response.status === 201) {
                        //move to next step
                        $scope.wizardNextStep();
                        //let other widgets know the user is enrolled
                        gadgets.pubsub.publish("launchpad-retail.userP2PEnrolled", {
                            enrolled: true,
                            enrollment: {
                                account: $scope.accountsModel.findById($scope.options.depositAccount.value),
                                email: $scope.options.email
                            }
                        });
                    }
                }, function(response) {
                    //error
                    $scope.errorOccurred = true;
                    $scope.enrollmentError = true;
                });
            }
        };

        var doP2PVerification = function() {
            //verification
            $scope.p2pService.error = false;

            $scope.p2pService.verifyCode($scope.options.email.value, $scope.verification.code).then(function(response) {
				if(response.status === 200) {
                    //success
					$scope.wizardNextStep();
                    gadgets.pubsub.publish("launchpad-retail.userP2PVerified", {
                        verified: true
                    });
                }
            }, function(response) {
                //error
                if(response.status === 409) {
                    $scope.errorOccurred = true;
                    $scope.verification.error = true;
                } else {
                    $scope.p2pService.error = true;
                }
            });
        };

        $scope.$watch("terms.doesAgreeWithTerms", function(newValue, oldValue) {

            if(newValue && $scope.options.email.valid) {
                $scope.errorOccurred = false;
            }
        });

        initialize();

    }]);

    return function(widget) {
        app.value("widget", widget);
        angular.bootstrap(widget.body, ["launchpad-retail.p2p-enrollment"]);
    };
});