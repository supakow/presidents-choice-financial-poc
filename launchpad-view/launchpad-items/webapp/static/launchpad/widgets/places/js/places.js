/*global b$, gadgets, google, Modernizr */
define([
    "angular",
    "launchpad/lib/common/util",
    "launchpad/lib/i18n",
    "launchpad/lib/ui/responsive",
    "launchpad/lib/places/places-module",
    "launchpad/lib/places/places-autocomplete",
    "launchpad/lib/places/places-list",
    'launchpad/lib/ui',
    "launchpad/lib/common",
    "launchpad/support/angular/angular-ui-bootstrap",
    "launchpad/support/angular/angular-gm"
], function(angular, util, i18nUtil, responsive, module) {

    "use strict";

    // Configuration
    module.constant("placesConfig", {
        maxLengthLabel: 3, // Marker"s label maximum size
        markerColorPool: ["#FF8355", "#6FADD4", "#E69215", "#74AED3", "#C73935", "#443647", "#38706D", "#1D415B"],
        markerWidth: 25,
        markerHeight: 35,
        markerFontFamily: "Arial",
        titleField: "name", // Field used as title on the marker
        alertTimeout: 5000 // Milliseconds until an alert will auto-close
    });


    // Common functionallity shared between all widget instances.
    module.service("PlacesService", ["placesConfig", function(placesConfig) {

        this.getFilterOptions = function(data){

            var types = data.types || [],
                services = data.services || [];

            return types.concat(services);
        };

        // Get url for a google pin with custom letter and color
        this.googleIcon = function(label, color) {
            if (color.charAt(0) === "#") {
                color = color.substring(1);
            }
            return "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=" + label.charAt(0) + "|" + (color || "FF0000");
        };

        // Darw a canvas pin
        var drawPin = function(context, width, height) {
            var radius = width / 2;
            context.beginPath();
            context.moveTo(radius, height);
            context.arc(radius, radius, radius, 0, Math.PI, true);
            context.closePath();
            context.fill();
            context.stroke();
        };

        // Create a data url for a canvas pin with custom label and color
        this.canvasIcon = function(label, color, width, height) {
            var canvas = document.createElement("canvas");
            var context = canvas.getContext("2d");

            width = width || placesConfig.markerWidth;
            height = height || placesConfig.markerHeight;

            canvas.width = width;
            canvas.height = height;

            context.clearRect(0, 0, width, height);

            context.fillStyle = color;
            context.strokeStyle = color;

            drawPin(context, width, height);

            context.fillStyle = "white";
            context.strokeStyle = "black";

            // Render Label
            var fontSize = 10 - label.length; // Decide font size based on label's length
            context.font = "normal " + fontSize + "pt " + placesConfig.markerFontFamily;
            context.textBaseline  = "top";

            // Centre text
            var textWidth = context.measureText(label);
            context.fillText(label, Math.floor((width / 2) - (textWidth.width / 2)), 4);

            return canvas.toDataURL();
        };
    }]);

    // The controller to dipslay & filter a list of locations in map
    module.controller("PlacesCtrl", ["$scope", "$filter", "$timeout", "widget", "PlacesService", "placesConfig", 'httpService', "$element", "angulargmUtils",
    function($scope, $filter, $timeout, widget, PlacesService, placesConfig, httpService, $element, angulargmUtils) {

        var locationService,                        // http service
            filterService,                          // http service
            bodyEl = angular.element(widget.body),  // Root element
            assignedColors = {},                    // Colors assigned to types
            markerColorPool = angular.copy(placesConfig.markerColorPool); // Color polor for instance

        $scope.mapId = widget.id;
        $scope.data  = [];
        $scope.title = widget.getPreference("title");
        
        // Initial options
        $scope.options = {
          map: {
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            panControl: util.parseBoolean(widget.getPreference("panControl")),
            styles: [{
                featureType: "poi",
                elementType: "labels",
                stylers:[{
                    visibility: util.parseBoolean(widget.getPreference("showPOI")) ? "on" : "off"
                }]
            }]
          },
          typeOrder: "name",  // Order type dropdown by this field
          emptyTypeLabel: "Services"
        };

        // Filters
        $scope.filters = [];
        $scope.activeFilters = [];

        $scope.map = {};
        $scope.map.zoom = parseInt(widget.getPreference("zoom"), 10);
        $scope.map.center = new google.maps.LatLng(widget.getPreference("latitude"), widget.getPreference("longitude"));
        $scope.map.staticMapApiUrl = widget.getPreference("staticMapApiUrl");
        $scope.map.staticMapZoom = widget.getPreference("staticMapZoom");
        $scope.map.staticMapThumbSize = widget.getPreference("staticMapThumbSize");
        $scope.map.directionApiUrl = widget.getPreference("directionApiUrl");

        $scope.params = {};
        $scope.params.pageSize = parseInt(widget.getPreference("placesPageSize"), 10);
        $scope.params.offset = 0;
        $scope.params.nextPage = 1;
        $scope.params.radius = widget.getPreference("placesFilterRadius");

        $scope.currentFilterLabel = $scope.options.emptyTypeLabel;

        gadgets.pubsub.subscribe("launchpad-retail.places.loadMore", function() {
            readLocationData();
            $scope.params.nextPage++;
        });

        function centerUserPosition(position) {
            $scope.$apply(function() {
                $scope.map.center = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            });
        }

        /**
         * Setup map & service options.
         */
        function initialize() {
            // Setup data service
            locationService = httpService.getInstance({
                endpoint: widget.getPreference("placesDataSrc")
            });

            filterService = httpService.getInstance({
                endpoint: widget.getPreference("filterDataSrc")
            });
            // filterDataSrc

            // Get initial data
            readLocationData();
            readFilterData();

            if (Modernizr.geolocation && widget.getPreference("currentPosition")) {
                navigator.geolocation.getCurrentPosition(centerUserPosition);
            }

            angular.extend($scope.options.map, $scope.map);

            responsive.enable($element)
                    .rule({
                        "max-width": 400,
                        then: function() {
                            $scope.responsiveClass = "lp-tile-size";
                            util.applyScope($scope);
                        }
                    })
                    .rule({
                        "max-width": 910,
                        then: function() {
                            $scope.hiddenXs = "hidden";
                        }
                    })
                    .rule({
                        "min-width": 911,
                        then: function() {
                            $scope.hiddenXs = "";
                        }
                    });
        }

        // Start application
        initialize();

        function readFilterData() {

            filterService.read({}, true)
            .success(function(data) {
                $scope.filters = PlacesService.getFilterOptions(data);
            })
            .error(function() {
            });
        }

        /**
         * Fetch data from source.
         */
        function readLocationData() {

            var params = buildGetParams();
            
            locationService.read(params, true)
                .success(function(data) {
                    $scope.data = data;
                })
                .error(function() {
                }
            );
        }

        function buildGetParams(){

            var params = {},
                filterParam;

            //Pagination
            params.limit = $scope.params.pageSize;
            params.offset = $scope.params.nextPage * $scope.params.pageSize;

            //Radius in ok from current location
            params.radius = $scope.params.radius;

            //Filter drop-down 
            filterParam = $scope.activeFilters.reduce(function(previousValue, currentValue, index, array){
              return previousValue + "," + currentValue.id;
            }, "");

            if("" !== filterParam) {
                if(filterParam.charAt(0) === ","){
                    //strip out leading coma
                    filterParam = filterParam.substring(1);
                }
                params.services = filterParam;
            }
            return params;
        }

        $scope.reloadLocationData = function() {
            //expose this function to $scope
            readLocationData();
        };

        // Check if given id is already assigned a color, otherwise return one from the pool
        function getColor(type) {
            if ( !assignedColors[type] ) {
                assignedColors[type] = markerColorPool.shift();
            }
            return assignedColors[type];
        }

        /**
         * Define marker's properties and visibility.
         * Called every time map is redraw.
         */
        $scope.getMarkerOptions = function(object) {
            var label = object.abbr || object.type.label,
                icon  = object.icon,
                title = object[placesConfig.titleField];

            // Shorten label to fit inside the marker
            if (label.length > placesConfig.maxLengthLabel) {
                label = label.charAt(0);
            }

            // Create custom icon
            if ( !icon ) {
                var color = object.color || getColor(object.type.id);
                icon = Modernizr.canvas ? PlacesService.canvasIcon(label, color) : PlacesService.googleIcon(label, color);
                object.icon = icon;

            }

            return {
                title: title ? title + '' : '', // Make sure this is a string
                icon: icon
            };
        };

        /**
         * Search places based on lat/lng coordinates.
         */
        $scope.search = function(lat, lng) {
            if (!lat || !lng) {
                $scope.addAlert('The location you provided is not valid!', 'warning', placesConfig.alertTimeout);
                return;
            }

            $scope.map.center = new google.maps.LatLng(lat, lng);
            $scope.map.zoom = parseInt(widget.getPreference("zoom"), 10);
        };

        // Refresh map
        $scope.redraw = function() {
            $scope.$broadcast("gmMarkersRedraw", "places");
        };

        $scope.$watch("data", function() {
            // Redraw locations
            $scope.places = $scope.data.locations;
            $scope.redraw();

        }, true);

        $scope.$watch("filters.type", function() {
            $scope.redraw();
        });


        /**
         * Alert messages.
         */
        $scope.alerts = [];

        $scope.addAlert = function(msg, type, timeout) {
            var alert = { msg: msg, type: type || 'error' };
            $scope.alerts.push(alert);

            if (timeout) {
                $timeout(function() {
                    $scope.closeAlert($scope.alerts.indexOf(alert));
                }, timeout);
            }
        };

        $scope.closeAlert = function(index) {
            if ( index > -1 ){
                $scope.alerts.splice(index, 1);
            }
        };

        /**
         * Open info window for specific marker
         */
        $scope.openInfoWindow = function(object, marker) {
            $scope.place = object;
            $scope.infoWindow.open(marker.getMap(), marker);
        };

        /**
         * Responsive logic to handle size changes.
         */

        $scope.sizeRules = [
             { max: 200, size: 'tile' },
             { min: 201, max: 450, size: 'small' },
             { min: 451, size: 'normal' }
        ];

        $scope.resized = function( width ) {
            $scope.$broadcast("gmMapResize", $scope.mapId);
        };

        $scope.setView = function(tab) {
            var splitSize = "col-xs-6 col-sm-6",
                fullSize = "col-xs-12 col-sm-12",
                hideMe = 'hidden';
                
            $scope.viewStatus = tab;
            $scope.mapClass = '';
            $scope.listClass = '';
            $scope.listSize = '';
            $scope.mapSize = '';

            switch ($scope.viewStatus) {
                case "map":
                    $scope.listClass = hideMe;
                    $scope.mapSize = fullSize;
                    break;
                case "list":
                    $scope.mapClass = hideMe;
                    $scope.listSize = fullSize;
                    break;
                case "split":
                    $scope.listSize = splitSize;
                    $scope.mapSize = splitSize;
            }
        };

        $scope.resetActiveFilters = function() {
            $scope.activeFilters = [];
        };

        // Re-initialize on preferences change
        widget.addEventListener("preferencesSaved", function () {
            widget.refreshHTML();
            $timeout(function() {
                initialize();
            });
        });
    }]);

    module.controller('DropdownCtrl', ['$scope', function($scope) {

        $scope.status = {
            isopen: false
        };

        $scope.toggled = function(open) {
            if(open === false) {
                //load new data when drop down close up
                $scope.reloadLocationData();
            }
        };

        $scope.toggleFilter = function($event, filter) {

            $event.stopPropagation(); // avoid the dropdown to close after click
            $scope.status.isopen = true;
            $scope.resetActiveFilters();

            angular.forEach($scope.filters, function(filter) {
                if(filter.selected === true) {
                   $scope.activeFilters.push(filter);
                }
            });
        };
    }

    ]);


    return function(widget) {
        module.value("widget", widget);
        angular.bootstrap(widget.body, ["launchpad-foundation.places"]);
    };
});