<xsl:stylesheet	version="1.0" exclude-result-prefixes="dc content"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:content="http://purl.org/rss/1.0/modules/content/">

	<xsl:output method="html" encoding="UTF-8" indent="no" />


    <xsl:param name="mode"/>
    <xsl:param name="link"/>


    <!--
    /**
      * Depending on the param $mode,
      * we will choose either to render the list of news
      * or the content for the particular news item
      *
      */
    -->
	<xsl:template match="/">
        <xsl:choose>
            <xsl:when test="$mode='content'">
                <xsl:apply-templates select="//item[link=$link]" mode="content"/>
            </xsl:when>
            <xsl:when test="$mode='list'">
                <ul class="list">
                    <xsl:apply-templates select="//item" mode="list"/>
                </ul>
            </xsl:when>
            <xsl:when test="$mode='print'">
                <xsl:apply-templates select="//item[link=$link]" mode="print"/>
            </xsl:when>
            <xsl:otherwise>
                <ul class="list">
                    <xsl:apply-templates select="//item" mode="list"/>
                </ul>
            </xsl:otherwise>
        </xsl:choose>
	</xsl:template>


    <!--
    /**
      * News list
      *
      */
    -->
	<xsl:template match="item" mode="list">
        <li class="item">
            <xsl:attribute name="data-link"><xsl:value-of select="link"/></xsl:attribute>
            <xsl:attribute name="data-creator"><xsl:value-of select="dc:creator"/></xsl:attribute>
            <span class="lp-icon-bullet"></span>
            <header class="clearfix trig">
                <a class="title">
                    <xsl:attribute name="title"><xsl:value-of select="title"/></xsl:attribute>
                    <xsl:attribute name="href"><xsl:value-of select="link"/></xsl:attribute>
                    <xsl:value-of select="title" disable-output-escaping="yes"/>
                </a>
                <span class="date"><xsl:value-of select="substring(pubDate, 0, 17)"/></span>
            </header>
            <article class="preview trig">
                <xsl:value-of select="description" disable-output-escaping="yes"/>
            </article>
        </li>
	</xsl:template>


    <!--
    /**
      * News content
      * renders the content of the news item specified by the $link param
      *
      */
    -->
	<xsl:template match="item[link=$link]" mode="content">
        <header class="header clearfix">
            <div class="bar clearfix">
                <div class="info">
                    <div class="date"><xsl:value-of select="substring(pubDate, 0, 23)"/></div>
                    <a class="link" target="_blank" title=""><xsl:attribute name="href"><xsl:value-of select="link"/></xsl:attribute>Full article</a>
                    <!--&#160;&#8226;&#160;-->
                    <span class="creator"><xsl:value-of select="dc:creator"/></span>
                </div>
                <div class="pull-right btn-group">
                    <button class="btn"><i class="lp-icon-buttons-share"></i> Share</button>
                    <a class="btn lp-rss-print" href="" target="_blank" title="Print this article"><i class="lp-icon-buttons-print"></i> Print</a>
                </div>
            </div>
            <h2 class="title">
                <a class="link" target="_blank" title="">
                    <xsl:attribute name="href"><xsl:value-of select="link"/></xsl:attribute>
                    <xsl:value-of select="title" disable-output-escaping="yes"/>
                </a>
            </h2>
        </header>
        <article class="article clearfix">
            <div class="content">
                <xsl:choose>
                    <xsl:when test="content:encoded/text()">
                        <xsl:value-of select="content:encoded" disable-output-escaping="yes"/>
                    </xsl:when>
                    <xsl:when test="description/text()">
                        <p>
                            <xsl:value-of select="description" disable-output-escaping="yes"/>
                        </p>
                        <p>
                            Read the full article:
                            <a class="read-more" target="_blank" title="">
                                <xsl:attribute name="href"><xsl:value-of select="link"/></xsl:attribute>
                                <xsl:value-of select="link" disable-output-escaping="yes"/>
                            </a>
                        </p>
                    </xsl:when>
                    <xsl:otherwise>
                        <p>
                            Read the full article:
                            <a class="read-more" target="_blank" title="">
                                <xsl:attribute name="href"><xsl:value-of select="link"/></xsl:attribute>
                                <xsl:value-of select="link" disable-output-escaping="yes"/>
                            </a>
                        </p>
                    </xsl:otherwise>
                </xsl:choose>
            </div>
        </article>
 	</xsl:template>


    <!--
    /**
      * Print news article
      * renders the content for printing
      *
      */
    -->
	<xsl:template match="item[link=$link]" mode="print">
        <header class="header clearfix">
            <xsl:attribute name="data-link"><xsl:value-of select="link"/></xsl:attribute>
            <div class="bar clearfix">
                <div class="info">
                    <div class="date"><xsl:value-of select="substring(pubDate, 0, 23)"/></div>
                    <a class="link" target="_blank" title=""><xsl:attribute name="href"><xsl:value-of select="link"/></xsl:attribute>Full article</a>
                    <span class="creator"><xsl:value-of select="dc:creator"/></span>
                </div>
                <div class="buttons">
                    <button class="lp-button lp-button-inactive lp-button-icon lp-icon-print" type="button">Print</button>
                </div>
            </div>
            <h2 class="title"><xsl:value-of select="title"  disable-output-escaping="yes"/></h2>
        </header>
        <article class="article clearfix">
            <xsl:choose>
                <xsl:when test="content:encoded/text()">
                    <xsl:value-of select="content:encoded" disable-output-escaping="yes"/>
                </xsl:when>
                <xsl:when test="description/text()">
                    <p>
                        <xsl:value-of select="description" disable-output-escaping="yes"/>
                    </p>
                    <p>
                        Read the full article:
                        <a class="read-more" target="_blank" title="">
                            <xsl:attribute name="href"><xsl:value-of select="link"/></xsl:attribute>
                            <xsl:value-of select="link" disable-output-escaping="yes"/>
                        </a>
                    </p>
                </xsl:when>
                <xsl:otherwise>
                    <p>
                        Read the full article:
                        <a class="read-more" target="_blank" title="">
                            <xsl:attribute name="href"><xsl:value-of select="link"/></xsl:attribute>
                            <xsl:value-of select="link" disable-output-escaping="yes"/>
                        </a>
                    </p>
                </xsl:otherwise>
            </xsl:choose>
        </article>
 	</xsl:template>

</xsl:stylesheet>


