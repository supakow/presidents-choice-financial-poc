<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="../common/directives.jspf" %>

<c:set var="lpVersion" value="${lpconf_build['launchpad.version']}"/>

<script src="${contextPath}/static/launchpad/behaviors/retail-behaviors.js"></script>
<script src="${contextPath}/static/launchpad/behaviors/pm-behaviors.js"></script>

