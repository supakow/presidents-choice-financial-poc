<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8"  %>
<%@ include file="../common/directives.jspf" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <title>Springboard Container</title>

    <%-- TODO: Have server minimize these into a single file for production
         Also: why are containers not using require.js?
    --%>

    <script src="${contextPath}/static/launchpad/support/jquery/jquery-ui.custom.min.js"></script>
    <script src="${contextPath}/static/launchpad/lib/ui/responsive.js"></script>
    <script src="${contextPath}/static/launchpad/containers/springboard/springboard-behavior.js"></script>
    <script src="${contextPath}/static/launchpad/containers/springboard/springboard-manager.js"></script>
    <script src="${contextPath}/static/launchpad/containers/springboard/springboard-container.js"></script>
    <script src="${contextPath}/static/launchpad/lib/ui/lp-anim.js"></script>


</head>
<body>
    <div data-pid="${item.name}" class="bp-container lp-springboard-container" data-allow-personalize="${lp:canPersonalize(item)}"></div>
</body>
</html>