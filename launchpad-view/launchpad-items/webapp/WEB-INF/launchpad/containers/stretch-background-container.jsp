<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8"  %>
<%@ include file="../common/directives.jspf" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <title>Stretch Background Container Layout</title>

    <script src="${contextPath}/static/launchpad/containers/stretch-background/stretch-background-container.js"></script>
</head>
<body>
    <c:set var="themeModifier" value="${item.propertyDefinitions['themeModifier'].value.value}"/>

    <div data-pid="${item.name}" class="bp-container bp-ui-dragRoot lp-stretch-background-container ${themeModifier}" >
        <div class="lp-stretch-background ${themeModifier}"></div>
        <div class="bp-area clearfix">
            <c:forEach items="${item.children}" var="child">
                <b:include src="${child}"/>
            </c:forEach>
        </div>
    </div>
</body>
</html>