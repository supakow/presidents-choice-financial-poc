<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8"  %>
<%@ include file="../common/directives.jspf" %>
<!DOCTYPE html>
    <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
            <title>Column Container Contents</title>

            <script src="${contextPath}/static/launchpad/containers/simple-container/simple-container.js"></script>
            <link rel="stylesheet" href="${contextPath}/static/launchpad/containers/simple-container/simple-container.css" />

        </head>
        <body>
            <div data-pid="${item.name}" class="bp-container bp-ui-dragRoot lp-columns row">
                <div class="bp-area lp-launcher-area --area">
                    <c:forEach items="${item.children}" var="child">
                        <b:include src="${child}"/>
                    </c:forEach>
                </div>
            </div>
        </body>
</html>
