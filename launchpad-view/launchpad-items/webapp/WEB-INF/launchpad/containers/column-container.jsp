<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8"  %>
<%@ include file="../common/directives.jspf" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
    <title>Column Container Contents</title>

    <script src="${pageContext.request.contextPath}/static/launchpad/containers/column/column-container.js"></script>

</head>
<body>
    <c:catch var="exception">

        <%-- Get column sizes (default to null) --%>
        <c:choose>
            <c:when test="${lp:hasProperty(item, 'columns')}">
                <c:set var="columns" value="${fn:split(lp:property(item, 'columns'), ',')}" />
            </c:when>
            <c:otherwise>
                <c:set var="columns" value="${null}" />
            </c:otherwise>
        </c:choose>

        <div data-pid="${item.name}" class="bp-container bp-ui-dragRoot lp-columns row">
            <c:if test="${not empty columns}">

                <%-- Each column --%>
                <c:forEach begin="0" end="${fn:length(columns) - 1}" varStatus="status">
                    <div class="bp-area col-sm-${columns[status.index]}">

                        <%-- Each child for the current column --%>
                        <c:forEach items="${item.children}" var="child">
                            <c:if test="${lp:property(child, 'area') == status.index}">
                                <b:include src="${child}"/>
                            </c:if>
                        </c:forEach>
                    </div>

                </c:forEach>
            </c:if>
        </div>

    </c:catch>

    <c:if test="${not empty exception}">
        <div class="bp-container">
            <p>Sorry, there was an error rendering this container (${exception.message})</p>
        </div>
    </c:if>
</body>
</html>