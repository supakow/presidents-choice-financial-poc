<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8"  %>
<%@ include file="../common/directives.jspf" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<c:set var="bundle" value="com.backbase.expert.common"/>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
        <title>Lightbox Container</title>

        <script src="${contextPath}/static/launchpad/containers/lightbox/lightbox-container.js"></script>
     </head>
    <body>
        <c:catch var="exception">

            <div data-pid="${item.name}" data-width="${lp:property(item, 'width')}" data-height="${lp:property(item, 'height')}" class="bp-container lp-lightbox-container panel-chrome-default">

                <div class="lp-lightbox-inner panel panel-chrome-default">
                    <div class="panel-heading clearfix bp-ui-dragGrip" >
                        <div class="lp-widget-title panel-title pull-left"></div>
                        <div class="lp-widget-controls pull-right">
                            <button class="lp-widget-control lp-lightbox-close" title="Close">
                                <i class="lp-icon lp-icon-remove"></i>
                            </button>
                        </div>
                    </div>
                    <div class="bp-area lp-lightbox-area">
                        <c:forEach items="${item.children}" var="child">
                             <b:include src="${child}"/>
                        </c:forEach>
                    </div>
                    <div class="lp-lightbox-close"></div>
               </div>
               <div class="clear"></div>
            </div>
        </c:catch>
        <c:if test="${not empty e}">
            <div class="bp-container">
                <p>Sorry, there was an error rendering this container (${e.message})</p>
            </div>
        </c:if>
    </body>
</html>