<!DOCTYPE html>
<%@page import="com.backbase.portal.foundation.presentation.util.BuildConfigUtils"%>
<%@page import="com.backbase.portal.foundation.business.utils.context.ThreadLocalRequestContext"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="b" uri="http://www.backbase.com/taglib" %>
<%@ page import="com.backbase.portal.foundation.presentation.tags.LinkedItemInitTag" %>
<%@ page session="false"%>
<%String buildVersion = BuildConfigUtils.getBuildVersion();%>
<%LinkedItemInitTag.init();%>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
    <title>Tab Layout Container Contents</title>

    <c:choose>
        <c:when test="${devmode}">
            <link type="text/css" rel="stylesheet" href="${linkedItem.contextPath}/static/backbase.com.2012.aurora/containers/containers.shared.css" />
            <script src="${linkedItem.contextPath}/static/backbase.com.2012.aurora/containers/containers.shared.js"></script>
            <link type="text/css" rel="stylesheet" href="${linkedItem.contextPath}/static/backbase.com.2012.nexus/css/tabContainer.shared.css" />
            <link type="text/css" rel="stylesheet" href="${linkedItem.contextPath}/static/backbase.com.2012.nexus/containers/tabTemplate/css/bbVerticalTab.css" />
            <link type="text/css" rel="stylesheet" href="${linkedItem.contextPath}/static/backbase.com.2012.nexus/css/tabContainer.IE7.css" />
            <link type="text/css" rel="stylesheet" href="${linkedItem.contextPath}/static/backbase.com.2012.nexus/css/tabContainer.IE8.css" />

            <script src="${linkedItem.contextPath}/static/backbase.com.2012.nexus/containers/tabContainer/linkedItemAPI.js"></script>
            <script src="${linkedItem.contextPath}/static/backbase.com.2012.nexus/containers/tabContainer/linkedItemPortalClientIntegration.js"></script>
            <script src="${linkedItem.contextPath}/static/backbase.com.2012.nexus/containers/tabContainer/tabContainerPortalClientIntegration.js"></script>
            <script src="${linkedItem.contextPath}/static/backbase.com.2012.nexus/containers/tabContainer/tabContainerDnd.js"></script>
            <script src="${linkedItem.contextPath}/static/backbase.com.2012.nexus/containers/tabContainer/tabContainerDesignMode.js"></script>
            <script src="${linkedItem.contextPath}/static/backbase.com.2012.nexus/containers/tabContainer/tabContainerLiveMode.js"></script>
            <script src="${linkedItem.contextPath}/static/backbase.com.2012.nexus/containers/tabContainer/tabContainerPageMgmtIntegration.js"></script>
            <script src="${linkedItem.contextPath}/static/backbase.com.2012.nexus/containers/tabContainer/tabContainerUrlManager.js"></script>
        </c:when>
        <c:otherwise>
            <link type="text/css" rel="stylesheet" href="${linkedItem.contextPath}/static/backbase.com.2012.aurora/build/aurora.containers.css?v=${buildVersion}" />
            <link type="text/css" rel="stylesheet" href="${linkedItem.contextPath}/static/launchpad/containers/launcher-tabs/launcher-tab-container.css?v=${buildVersion}" />
            <script src="${linkedItem.contextPath}/static/backbase.com.2012.aurora/build/aurora.containers.js?v=${buildVersion}"></script>
            <script src="${linkedItem.contextPath}/static/launchpad/containers/launcher-tabs/launcher-tab-container.js?v=${buildVersion}"></script>
        </c:otherwise>
    </c:choose>

</head>
<body>
    <div class="bp-container bp-TabCont-vertical bp-ui-dragRoot bp-tabContFn-main" data-pid="${linkedItem.name}" data-pUuid="${linkedItem.uuid}">
        <div class="lp-launcher-tabs">
            <ul data-enhance="false" class="nav nav-pills nav-stacked bp-tabContFn-header">
                <c:if test='${linkedItem.canEdit}'>
                    <a class="bp-TabCont-editBar-btn bp-TabCont-gradient-grey bd-roundcorner-5 bp-tabContFn-addTab bp-TabCont-add-vertical"  title='Add New Tab'>
                        <span>+ ADD</span>
                    </a>
                </c:if>
                <c:forEach items="${linkedItem.childList}" var="child">
                    <li data-enhance="false" class="lp-launcher-tab bp-ui-dragRoot bp-tabContFn-tab bp-ui-dragGrip"
                        data-index="${child.name}" data-uuid="${child.uuid}" data-url="${child.linkUrl}"
                        data-linkuuid = "${child.linkUuid}">
                        <a href='<c:out value='${child.urlString}'/>' class="bp-tabContFn-link" data-linkType='state'>
                            <c:out value='${child.title}'/>
                        </a>
                       <c:if test='${child.canEdit}'>
                           <ul data-enhance="false" class="pagination pagination-sm bp-TabCont-tabEditBar">
                                <li class="bp-tabContFn-editTab" title='Edit'>
                                    <a><i class="lp-icon lp-icon-pencil"></i></a>
                                </li>
                                <li class="bp-tabContFn-settingsTab bd-icon-edit" title='Settings'>
                                    <a><i class="lp-icon lp-icon-cog"></i></a>
                                </li>
                                <li class="bp-tabContFn-delTab" title='Delete'>
                                    <a><i class="lp-icon lp-icon-remove"></i></a>
                                </li>
                           </ul>
                       </c:if>
                    </li>
                </c:forEach>
            </ul>
        </div>

        <div class="lp-launcher-tab-container bp-area bp-tabContFn-body"></div>
        <div class="bp-tabCont-loadingCover"></div>
    </div>
</body>
</html>