<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8"  %>
<%@ include file="../common/directives.jspf" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
    <title>Carousel Container</title>

    <%-- If frontend build is on, container resources are bundled together --%>
    <script src="${pageContext.request.contextPath}/static/launchpad/containers/carousel/carousel-container.js"></script>

</head>
<body>
    <c:catch var="exception">

        <c:set var="numSlides" value="${lp:property(item, 'numSlides')}" />

        <%-- Get column sizes (default to null) --%>
        <div data-pid="${item.name}" class="bp-container lp-carousel-container bp-ui-dragRoot">

            <c:if test="${not empty numSlides}">
                <div class="lp-carousel-slides">

                    <%-- Each column --%>
                    <c:forEach begin="0" end="${numSlides - 1}" varStatus="status">

                        <div class="bp-area lp-carousel-slide" data-area="${status.index}">

                            <%-- Each child for the current column --%>
                            <c:forEach items="${item.children}" var="child">
                                <c:if test="${lp:property(child, 'area') == status.index}">
                                    <b:include src="${child}"/>
                                </c:if>
                            </c:forEach>
                        </div>
                    </c:forEach>
                </div>
                <div class="clearfix"></div>
            </c:if>
        </div>
    </c:catch>

    <c:if test="${not empty exception}">
        <div class="bp-container">
            <p>Sorry, there was an error rendering this container (${exception.message})</p>
        </div>
    </c:if>
</body>
</html>