<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8"  %>
<%@ include file="../common/directives.jspf" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <title>Page Layout Container</title>


    <script src="${contextPath}/static/launchpad/containers/page-layout/page-layout-container.js"></script>
</head>
<body>

    <c:set var="pageChildren" value="${item.children}"/>
    <div data-pid="${item.name}" class="bp-container bp-ui-dragRoot lp-page-layout-container container">
        <header class="lp-page-header clearfix">
            <div class="bp-area lp-header-left">
                <c:forEach items="${pageChildren}" var="child">
                    <c:if test="${lp:property(child, 'area') == '0'}">
                        <b:include src="${child}"/>
                    </c:if>
                </c:forEach>
            </div>
            <div class="bp-area lp-header-right ">
                <c:forEach items="${pageChildren}" var="child">
                   <c:if test="${lp:property(child, 'area') == '1'}">
                        <b:include src="${child}"/>
                    </c:if>
                </c:forEach>
            </div>
        </header>
        <div class="lp-page-main clearfix">
            <div class="bp-area">
                <c:forEach items="${pageChildren}" var="child">
                    <c:if test="${lp:property(child, 'area') == '2'}">
                        <b:include src="${child}"/>
                    </c:if>
                </c:forEach>
            </div>
        </div>
        <footer class="lp-page-footer">
            <div class="bp-area">
                <c:forEach items="${pageChildren}" var="child">
                    <c:if test="${lp:property(child, 'area') == '3'}">
                        <b:include src="${child}"/>
                    </c:if>
                </c:forEach>
            </div>
        </footer>
    </div>
</body>
</html>