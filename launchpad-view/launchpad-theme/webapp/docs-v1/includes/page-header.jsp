    <!-- Bootstrap core CSS -->
    <link id="theme-link" rel="stylesheet" type="text/css" href="../static/themes/default/base.less"/>

    <!-- Documentation extras -->
    <link href="assets/css/docs.css" rel="stylesheet">
    <link href="assets/css/pygments-manni.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.js"></script>
        <script src="assets/js/respond.min.js"></script>
    <![endif]-->

    <!-- Place anything custom after this. -->
</head>