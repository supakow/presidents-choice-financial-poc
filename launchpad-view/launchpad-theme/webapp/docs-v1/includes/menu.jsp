<a class="sr-only" href="#content">Skip navigation</a>

    <!-- Docs master nav -->
    <header class="navbar navbar-inverse navbar-fixed-top bs-docs-nav" role="banner">
  <div class="container">
    <div class="navbar-header">
        <div style="width:70%; float:left;">
      <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <div class="navbar-brand">Bootstrap</div>

    <nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">
      <ul class="nav navbar-nav">
        <li class="nav-launchpad">
          <a href="index.jsp"><i class="lp-icon lp-icon-rocket"></i> Launchpad</a>
        </li>
        <li class="nav-css">
          <a href="css.jsp">CSS</a>
        </li>
        <li class="nav-components">
          <a href="components.jsp">Components</a>
        </li>
        <li class="nav-javascript">
          <a href="javascript.jsp">JavaScript</a>
        </li>
      </ul>
    </nav>
    </div>
<!--   <div class="text-right theme-dd"> -->
<!--       <div style="position:relative; top:2px; display:inline-block;"> -->
<!--       <i style="color:#ed1c24; padding-right: .175em" class="lp-icon lp-icon-bb-logo"></i> -->
<!--       <span><i class="lp-icon lp-icon-bb-b"></i>-->
<!--       <i class="lp-icon lp-icon-bb-a"></i>-->
<!--       <i class="lp-icon lp-icon-bb-c"></i>-->
<!--       <i class="lp-icon lp-icon-bb-k"></i>-->
<!--       <i class="lp-icon lp-icon-bb-b"></i>-->
<!--       <i class="lp-icon lp-icon-bb-a"></i>-->
<!--       <i class="lp-icon lp-icon-bb-s"></i>-->
<!--       <i class="lp-icon lp-icon-bb-e"></i> -->
<!--       </span> -->
<!--       </div> -->
<!--       <select style="margin-left:10px;"> -->
<!--         <option>themes</option> -->
<!--       </select> -->
<!--   </div> -->
<!--   <div style="width:100%"> -->
<!--     <a href="#" data-theme="amelia" class="theme-link">Amelia</a> -->
<!--     <a href="#" data-theme="cerulean" class="theme-link">cerulean</a> -->
<!--     <a href="#" data-theme="cosmo" class="theme-link">cosmo</a> -->
<!--     <a href="#" data-theme="cyborg" class="theme-link">cyborg</a> -->
<!--     <a href="#" data-theme="flatly" class="theme-link">flatly</a> -->
<!--     <a href="#" data-theme="journal" class="theme-link">journal</a> -->
<!--     <a href="#" data-theme="readable" class="theme-link">readable</a> -->
<!--     <a href="#" data-theme="simplex" class="theme-link">simplex</a> -->
<!--     <a href="#" data-theme="slate" class="theme-link">slate</a> -->
<!--     <a href="#" data-theme="spacelab" class="theme-link">spacelab</a> -->
<!--     <a href="#" data-theme="united" class="theme-link">united</a> -->
<!--   </div> -->
</header>
