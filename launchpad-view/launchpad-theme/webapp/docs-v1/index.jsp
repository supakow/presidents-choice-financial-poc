<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Launchpad &middot; Bootstrap</title>
        <%@include file="includes/page-header.jsp" %>
    </head>

    <body class="nav-launchpad">
    <%@include file="includes/menu.jsp" %>

    <!-- Docs page layout -->
    <div class="bs-header" id="content">
      <div class="container">
        <h1 class="bs-h1"><i class="lp-icon lp-icon-rocket"></i> Launchpad</h1>
        <p>An overview of Launchpad-specific components including widget chromes, containers, and widget layout examples.</p>

      </div>
    </div>



    <div class="container bs-docs-container">
      <div class="row">
        <div class="col-md-3">
          <div class="bs-sidebar hidden-print">
            <ul class="nav bs-sidenav">
                <li>
                  <a href="#widget-layout">Widget Layout</a>
                  <ul class="nav">
                    <li><a href="#example-widget-assets">Example Widget Assets</a></li>
                    <li><a href="#example-widget-code">Example Widget Code</a></li>
                    <li><a href="#springboard-widget-tiles">Springboard Widget Tiles</a></li>
                    <li><a href="#launcher-tabs">Launcher Tabs</a></li>
                    <li><a href="#chrome-panels">Chrome Panels</a></li>
                    <li><a href="#widget-panels">Widget Panels</a></li>
                    <li><a href="#responsive-widgets">Responsive Widgets</a></li>
                  </ul>
                </li>
                <li>
                  <a href="#launchpadiconfont">Launchpad Icons</a>
                  <ul class="nav">
                    <li><a href="#widget-icon-aliases">Widget Icon Aliases</a></li>
                    <li><a href="#backbase-icons">Backbase Icons</a></li>
                    <li><a href="#widget-icons">Widget Icons</a></li>
                    <li><a href="#social-icons">Social Icons</a></li>
                    <li><a href="#chrome-icons">Chrome Icons</a></li>
                    <li><a href="#ui-icons">UI Icons</a></li>
                    <li><a href="#active-icons">Active Icons</a></li>
                  </ul>
                </li>
                <li>
                  <a href="#icon-sizes">Icon Sizes</a>
                  <ul class="nav">
                    <li><a href="#small-icons">Small Icons</a></li>
                    <li><a href="#medium-icons">Medium Icons</a></li>
                    <li><a href="#large-icons">Large Icons</a></li>
                    <li><a href="#extra-large-icons">Extra Large Icons</a></li>
                  </ul>
                </li>
                <li>
                  <a href="#icon-sizes">Typography</a>
                  <ul class="nav">
                    <li><a href="#widget-header-tags">Widget Header Tags</a></li>
                    <li><a href="#page-content-header-tags">Page Content Header Tags</a></li>
                  </ul>
                </li>
                <li>
                  <a href="#lp-helper-classes">Launchpad Helper Classes</a>
                  <ul class="nav">
                    <li><a href="#remove-padding">Remove Padding</a></li>
                    <li><a href="#remove-margins">Remove Margins</a></li>
                    <li><a href="#force-cursors">Force Cursors</a></li>
                    <li><a href="#remove-backgrounds">Remove Backgrounds</a></li>
                    <li><a href="#display-hide">Display / Hide Content</a></li>
                  </ul>
                </li>
            </ul>
          </div>
        </div>
        <div class="col-md-9">

    <div class="page-header">
      <h1 class="bs-h1" id="widget-layout">Widget Layout</h1>

    </div>




<h3 class="bs-h3" id="example-widget-assets">Example Widget Assets</h3>

<div class="bs-example">
    <div class="clearfix">
    <div class="col-xs-6">
        <div class="widget-tile-container">
            <div class="lp-tile-size">
                <div class="lp-tile">
                    <a class="bp-ui-dragGrip ng-binding">
                        <i class="lp-icon lp-icon-xxl lp-icon-new-transfer"></i> New Transfer
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-6">
        <div class="bp-widget-head lp-launcher-tab bp-ui-dragGrip active">
            <a class="clearfix" href="#widget-new-transfer-2828715-body">
                <span class="lp-launcher-tab-icon pull-left">
                    <i class="lp-icon lp-icon-new-transfer"></i>
                </span>
                <span class="lp-launcher-tab-text pull-left">Tab Active</span>
                <span class="lp-launcher-tab-arrow pull-right">
                    <i class="lp-icon lp-icon-angle-right"></i>
                </span>
            </a>
        </div>
        <div class="bp-widget-head lp-launcher-tab bp-ui-dragGrip">
            <a class="clearfix" href="#widget-new-transfer-2828715-body">
                <span class="lp-launcher-tab-icon pull-left">
                    <i class="lp-icon lp-icon-new-transfer"></i>
                </span>
                <span class="lp-launcher-tab-text pull-left">Tab Inactive</span>
                <span class="lp-launcher-tab-arrow pull-right">
                    <i class="lp-icon lp-icon-angle-right"></i>
                </span>
            </a>
        </div>
    </div>
</div>
<br/>
<div class="panel panel-chrome panel-chrome-default">
    <div class="panel-heading clearfix">
        <div class="panel-title pull-left">
            <i class="lp-icon lp-icon-new-transfer"></i>New Transfer
        </div>
        <ul class="pull-right list-inline lp-widget-controls">
            <li>
                <button class="lp-widget-control">
                    <i class="lp-icon lp-icon-remove"></i>
                </button>
            </li>
            <li>
                <button class="lp-widget-control">
                    <i class="lp-icon lp-icon-cog"></i>
                </button>
            </li>
        </ul>
    </div>
    <div class="panel-body">
        <div class="panel panel-widget">
            <div class="panel-heading">
                <div class="form-horizontal">
                    <div class="col-sm-7">
                        <div class="dropdown header-dropdown">
                            <button data-toggle="dropdown" class="btn dropdown-toggle dropdown-large btn-block" href="#">
                                <div class="dropdown-title">
                                    <div class="h2">Business Account</div>
                                    <div class="h5 text-muted">694251588</div>
                                </div>
                                <span class="lp-icon lp-icon-caret-down"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                               <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
                               <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
                               <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
                               <li role="presentation" class="divider"></li>
                               <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
                             </ul>
                        </div>
                    </div>
                    <div class="lp-acct-info col-sm-5 text-right hide-small-widget visible-md visible-lg">
                        <div class="lp-amount-negative h2">-&euro; 11,418.00</div>
                        <div class="h4">
                            <small>Current balance:</small>
                            <span class="lp-dd-available-bal">
                                <span class="lp-amount-negative"> -&euro; 11,418.00</span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <!-- <div class="form-example"> -->
<form class="form form-horizontal">
     <input type="hidden" name="uuid"/>
     <div class="clearfix">
         <div class="form-group col-sm-6">
             <div class="input-group">
                 <input type="text" class="form-control" placeholder="transfer to"/>
                   <span class="input-group-btn">
                         <button type="button" class="btn btn-default dropdown-toggle">
                             <i class="lp-icon lp-icon-addressbook"></i>
                         </button>
                   </span>
             </div>
        </div>
        <div class="form-group col-sm-3">
             <input required="required" class="form-control" placeholder="amount"/>
         </div>
         <div class="form-group col-sm-3">
             <div class="dropdown">
                 <button type="button" class="btn btn-default btn-block dropdown-toggle" data-toggle="dropdown">
                     <span class="dropdown-title">Transfer today</span>
                     <span class="lp-icon lp-icon-caret-down"></span>
                 </button>
                 <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                     <li>
                          <a tabindex="-1" role="menuitem" href="#">Transfer today</a>
                     </li>
                     <li>
                          <a tabindex="-1" role="menuitem" href="#">Scheduled transfer</a>
                     </li>
                 </ul>
             </div>
         </div>
     </div>
     <div class="clearfix">
         <div class="form-group col-sm-3">
             <input type="text" class="form-control" placeholder="account number"/>
         </div>
         <div class="form-group col-sm-3">
             <input type="text" required="required" class="form-control" placeholder="bank name"/>
         </div>
         <div class="form-group col-sm-6">
             <input type="text" name="onDate" class="form-control" placeholder="select date"/>
         </div>
     </div>
     <div class="clearfix">
         <div class="form-group col-sm-6">
             <textarea rows="3" maxlength="140" class="form-control resize-none" placeholder="description (optional)"></textarea>
         </div>
         <div class="form-group col-sm-6">
             <input type="text" class="form-control" placeholder="payment reference (optional)"/>
         </div>
     </div>
</form>
</div>
            </div>
            <div class="panel-footer">
                <button name="resetForm" class="btn btn-link">Clear</button>
                <button class="btn btn-primary">Next</button>
            </div>
        </div>
    </div>
</div>


<h3 class="bs-h3" id="example-widget-code">Example Widget Code</h3>

    <div class="highlight"><pre><code class="html">
&lt;div class="bp-widget"&gt;
    &lt;div class="bp-widget-pref"&gt;&lt;/div&gt;

    &lt;!-- Launcher Tab --&gt;
    &lt;div class="bp-widget-head lp-launcher-tab bp-ui-dragGrip"&gt;
        &lt;!-- content here --&gt;
    &lt;/div&gt;

    &lt;!-- Chrome Panel --&gt;
    &lt;div class="panel panel-chrome-default&gt;

        &lt;!-- Chrome Heading --&gt;
        &lt;div class="panel-heading"&gt;
            &lt;!-- content here --&gt;
        &lt;/div&gt;

        &lt;!-- Main Widget --&gt;
        &lt;div class="panel-body bp-widget-body"&gt;

            &lt;!--  Tile --&gt;
            &lt;div class="lp-tile"&gt;
                &lt;!-- content here --&gt;
            &lt;/div&gt;

            &lt;!--  Widget Panel --&gt;
            &lt;div class="panel panel-widget lp-widget-content"&gt;

                &lt;!--  Alerts --&gt;
                &lt;div class="lp-alerts"&gt;
                    &lt;!-- content here --&gt;
                &lt;/div&gt;

                &lt;!--  Widget Heading --&gt;
                &lt;div class="panel-heading"&gt;
                    &lt;!-- content here --&gt;
                &lt;/div&gt;

                &lt;!--  Widget Content --&gt;
                &lt;div class="panel-body lp-widget-body"&gt;
                    &lt;!-- content here --&gt;
                &lt;/div&gt;
                &lt;!--  Widget Footer --&gt;
                &lt;div class="panel-footer"&gt;
                    &lt;!-- content here --&gt;
                &lt;/div&gt;
            &lt;/div&gt;
        &lt;/div&gt;
    &lt;/div&gt;
&lt;/div&gt;
</code></pre>
    </div>

    <h3 class="bs-h3" id="springboard-widget-tiles">Springboard Widget Tiles</h3>
    <p>In the springboard, when the widget responsivness detects <b>lp-tile-size</b> (the widget is 160px or less),
    the <b>lp-widget-content</b> is hidden, and the <b>lp-tile</b> is shown instead:</p>

    <div class="widget-tile-container">
        <div class="lp-tile-size">
            <div class="lp-tile">
                <button class="bp-ui-dragGrip ng-binding" data-action="lp-springboard-widget-maximize">
                    <i class="lp-icon lp-icon-xxl lp-icon-places"></i> Places
                </button>
            </div>
        </div>
    </div>

    <div class="widget-tile-container">
        <div class="lp-tile-size">
            <div class="lp-tile">
                <button class="bp-ui-dragGrip ng-binding" data-action="lp-springboard-widget-maximize">
                    <i class="lp-icon lp-icon-xxl lp-icon-transactions"></i> Transactions
                </button>
            </div>
        </div>
    </div>

    <div class="widget-tile-container">
        <div class="lp-tile-size">
            <div class="lp-tile">
                <button class="bp-ui-dragGrip ng-binding" data-action="lp-springboard-widget-maximize">
                    <i class="lp-icon lp-icon-xxl lp-icon-accounts"></i> Accounts
                </button>
            </div>
        </div>
    </div>

    <div class="highlight"><pre><code class="html">
&lt;div class="lp-tile-size"&gt;
    &lt;div class="lp-tile"&gt;
        &lt;button class="bp-ui-dragGrip ng-binding" data-action="lp-springboard-widget-maximize"&gt;
            &lt;i class="lp-icon lp-icon-xxl lp-icon-places"&gt;&lt;/i&gt; Places
        &lt;/button&gt;
    &lt;/div&gt;
&lt;/div&gt;
    </code></pre></div>



<div class="bs-docs-section clearfix" id="launcher-tabs">

<h3 class="bs-h3">Launcher Tabs</h3>
<h5 class="bs-h5">.panel-launcher-tab</h5>
<div class="bp-widget-head lp-launcher-tab bp-ui-dragGrip active">
    <a class="clearfix" href="#widget-new-transfer-2828715-body">
        <span class="lp-launcher-tab-icon pull-left">
            <i class="lp-icon lp-icon-new-transfer"></i>
        </span>
        <span class="lp-launcher-tab-text pull-left">Tab Active</span>
        <span class="lp-launcher-tab-arrow pull-right">
            <i class="lp-icon lp-icon-angle-right"></i>
        </span>
    </a>
</div>
<div class="bp-widget-head lp-launcher-tab bp-ui-dragGrip">
    <a class="clearfix" href="#widget-new-transfer-2828715-body">
        <span class="lp-launcher-tab-icon pull-left">
            <i class="lp-icon lp-icon-new-transfer"></i>
        </span>
        <span class="lp-launcher-tab-text pull-left">Tab Inactive</span>
        <span class="lp-launcher-tab-arrow pull-right">
            <i class="lp-icon lp-icon-angle-right"></i>
        </span>
    </a>
</div>

<h3 class="bs-h3" id="chrome-panels">Chrome Panels</h3>
<h5 class="bs-h5">.panel-chrome-default</h5>


    <div class="panel panel-chrome-default">
        <div class="panel-heading clearfix">
            <div class="panel-title pull-left">
                <i class="lp-icon lp-icon-new-transfer"></i>New Transfer
            </div>
            <ul class="pull-right list-inline lp-widget-controls">
                <li>
                    <button class="lp-widget-control">
                        <i class="lp-icon lp-icon-remove"></i>
                    </button>
                </li>
                <li>
                    <button class="lp-widget-control">
                        <i class="lp-icon lp-icon-cog"></i>
                    </button>
                </li>
            </ul>
        </div>
        <div class="panel-body">
            <p class="text-center bs-empty text-muted">insert .panel-widget</p>
        </div>
    </div>

<h3 class="bs-h3" id="widget-panels">Widget Panels</h3>
<h5 class="bs-h5">.panel-widget</h5>
    <div class="panel panel-widget">
        <div class="panel-heading">
            Widget Heading
        </div>
        <div class="panel-body">
            Widget Body
        </div>
        <div class="panel-footer">
            <button name="resetForm" class="btn btn-link">Clear</button>
            <button class="btn btn-primary">Next</button>
        </div>
    </div>


    <h3 class="bs-h3" id="responsive-widgets">Responsive Widgets</h3>
    <p>There are 4 levels of responsiveness for widgets that are seperate from the reponsiveness of the page and depend
    solely on the current width of the widget iteslf.  These include <b>lp-tile-size</b>, <b>lp-small-size</b>, <b>lp-medium-size</b>
     and <b>lp-large-size</b>.</p>
    <p>The responsive width is defined within the widget's javascript, however
    the recommended breakpoints are 160px, 350px, 600px, and 600px+ respectively.</p>
    <p>Included are various helper classes to hide and display widget content based on the width of the widget:</p>

    <div class="table-responsive">
      <table class="table table-bordered table-striped responsive-utilities">
        <thead>
          <tr>
            <th></th>
            <th>
              .lp-widget-tile
              <small>&lt;160px</small>
            </th>
            <th>
              .lp-widget-small
              <small>&gt;161px &amp; &lt;350px</small>
            </th>
            <th>
              .lp-widget-medium
              <small>&gt;351px &amp; &lt;600px</small>
            </th>
            <th>
              .lp-widget-large
              <small>&gt;601px</small>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th><code>.hide-tile-widget</code></th>
            <td class="is-hidden">Hidden</td>
            <td class="is-visible">Visible</td>
            <td class="is-visible">Visible</td>
            <td class="is-visible">Visible</td>
          </tr>
          <tr>
            <th><code>.hide-small-widget</code></th>
            <td class="is-hidden">Hidden</td>
            <td class="is-hidden">Hidden</td>
            <td class="is-visible">Visible</td>
            <td class="is-visible">Visible</td>
          </tr>
          <tr>
            <th><code>.hide-medium-widget</code></th>
            <td class="is-hidden">Hidden</td>
            <td class="is-hidden">Hidden</td>
            <td class="is-hidden">Hidden</td>
            <td class="is-visible">Visible</td>
          </tr>
          <tr>
            <th><code>.hide-large-widget</code></th>
            <td class="is-visible">Visible</td>
            <td class="is-visible">Visible</td>
            <td class="is-visible">Visible</td>
            <td class="is-hidden">Hidden</td>
          </tr>
        </tbody>
      </table>
    </div>

</div>
<!--  LAUNCHPAD ICONS -->
  <div class="bs-docs-section bs-icon-list">
    <div class="page-header">
      <h1 class="bs-h1" id="launchpad-icons">Launchpad Icons</h1>
    </div>

    <p class="lead">This is a custom icon subset created through the combination of selected glyphs from a few icon fonts at <a href="http://icomoon.io/" target="_blank">IcoMoon</a>.</p>
    <div class="highlight"><pre><code class="html"><span class="nt">&lt;i</span> <span class="na">class=</span><span class="s">&quot;lp-icon lp-icon-accounts&quot;</span><span class="nt">&gt;&lt;/i&gt;</span></code></pre></div>
    <h2 class="bs-h2" id="widget-icon-aliases">Widget Icon Aliases</h2>
    <ul class="bs-glyphicons">
        <li><i class="lp-icon lp-icon-accounts"></i> lp-icon lp-icon-accounts</li>
        <li><i class="lp-icon lp-icon-transactions"></i> lp-icon lp-icon-transactions</li>
        <li><i class="lp-icon lp-icon-addressbook"></i> lp-icon lp-icon-addressbook</li>
        <li><i class="lp-icon lp-icon-new-transfer"></i> lp-icon lp-icon-new-transfer</li>
        <li><i class="lp-icon lp-icon-review-transfers"></i> lp-icon lp-icon-review-transfers</li>
        <li><i class="lp-icon lp-icon-profile"></i> lp-icon lp-icon-profile</li>
        <li><i class="lp-icon lp-icon-dossier"></i> lp-icon lp-icon-dossier</li>
        <li><i class="lp-icon lp-icon-accounts"></i> lp-icon lp-icon-accounts</li>
        <li><i class="lp-icon lp-icon-transactions"></i> lp-icon lp-icon-transactions</li>
        <li><i class="lp-icon lp-icon-atm"></i> lp-icon lp-icon-atm</li>
        <li><i class="lp-icon lp-icon-places"></i> lp-icon lp-icon-places</li>
        <li><i class="lp-icon lp-icon-login"></i> lp-icon lp-icon-login</li>
        <li><i class="lp-icon lp-icon-icon-logout"></i> lp-icon lp-icon-logout</li>
        <li><i class="lp-icon lp-icon-video-player"></i> lp-icon lp-icon-video-player</li>
    </ul>

    <h2 class="bs-h2" id="backbase-icons">Backbase Icons</h2>
    <ul class="bs-glyphicons">
        <li><i class="lp-icon lp-icon-bb-logo"></i> lp-icon lp-icon-bb-logo</li>
        <li><i class="lp-icon lp-icon-bb-b"></i> lp-icon lp-icon-bb-b</li>
        <li><i class="lp-icon lp-icon-bb-a"></i> lp-icon lp-icon-bb-a</li>
        <li><i class="lp-icon lp-icon-bb-c"></i> lp-icon lp-icon-bb-c</li>
        <li><i class="lp-icon lp-icon-bb-k"></i> lp-icon lp-icon-bb-k</li>
        <li><i class="lp-icon lp-icon-bb-s"></i> lp-icon lp-icon-bb-s</li>
        <li><i class="lp-icon lp-icon-bb-e"></i> lp-icon lp-icon-bb-e</li>
    </ul>
    <div class="panel panel-default">
        <div class="panel-body h3" style="letter-spacing:-.15em;">
            <i style="color:#ed1c24; padding-right: .175em;" class="lp-icon lp-icon-bb-logo"></i>
            <i class="lp-icon lp-icon-bb-b"></i><!--
            --><i class="lp-icon lp-icon-bb-a"></i><!--
            --><i class="lp-icon lp-icon-bb-c"></i><!--
            --><i class="lp-icon lp-icon-bb-k"></i><!--
            --><i class="lp-icon lp-icon-bb-b"></i><!--
            --><i class="lp-icon lp-icon-bb-a"></i><!--
            --><i class="lp-icon lp-icon-bb-s"></i><!--
            --><i class="lp-icon lp-icon-bb-e"></i>
        </div>
    </div>


    <h2 class="bs-h2" id="widget-icons">Widget Icons</h2>
    <ul class="bs-glyphicons">
        <li><i class="lp-icon lp-icon-rocket"></i> lp-icon lp-icon-rocket</li>
        <li><i class="lp-icon lp-icon-user2"></i> lp-icon lp-icon-user2</li>
        <li><i class="lp-icon lp-icon-key2"></i> lp-icon lp-icon-key2</li>
        <li><i class="lp-icon lp-icon-briefcase"></i> lp-icon lp-icon-briefcase</li>
        <li><i class="lp-icon lp-icon-transfer"></i> lp-icon lp-icon-transfer</li>
        <li><i class="lp-icon lp-icon-archive"></i> lp-icon lp-icon-archive</li>
        <li><i class="lp-icon lp-icon-envelope-alt"></i> lp-icon lp-icon-envelope-alt</li>
        <li><i class="lp-icon lp-icon-feed"></i> lp-icon lp-icon-feed</li>
        <li><i class="lp-icon lp-icon-bars"></i> lp-icon lp-icon-bars</li>
        <li><i class="lp-icon lp-icon-graph"></i> lp-icon lp-icon-graph</li>
        <li><i class="lp-icon lp-icon-th-list"></i> lp-icon lp-icon-th-list</li>
        <li><i class="lp-icon lp-icon-rss"></i> lp-icon lp-icon-rss</li>
        <li><i class="lp-icon lp-icon-dashboard"></i> lp-icon lp-icon-dashboard</li>
    </ul>

    <h2 class="bs-h2" id="social-icons">Social Icons</h2>
    <ul class="bs-glyphicons">
        <li><i class="lp-icon lp-icon-twitter-sign"></i> lp-icon lp-icon-twitter-sign</li>
        <li><i class="lp-icon lp-icon-facebook-sign"></i> lp-icon lp-icon-facebook-sign</li>
        <li><i class="lp-icon lp-icon-facebook"></i> lp-icon lp-icon-facebook</li>
        <li><i class="lp-icon lp-icon-twitter"></i> lp-icon lp-icon-twitter</li>
        <li><i class="lp-icon lp-icon-linkedin"></i> lp-icon lp-icon-linkedin</li>
        <li><i class="lp-icon lp-icon-linkedin-sign"></i> lp-icon lp-icon-linkedin-sign</li>
        <li><i class="lp-icon lp-icon-google-plus-sign"></i> lp-icon lp-icon-google-plus-sign</li>
        <li><i class="lp-icon lp-icon-google-plus"></i> lp-icon lp-icon-google-plus</li>
    </ul>

    <h2 class="bs-h2" id="chrome-icons">Chrome Icons</h2>
    <ul class="bs-glyphicons">
        <li><i class="lp-icon lp-icon-cog"></i> lp-icon lp-icon-cog</li>
        <li><i class="lp-icon lp-icon-remove"></i> lp-icon lp-icon-remove</li>
        <li><i class="lp-icon lp-icon-refresh"></i> lp-icon lp-icon-refresh</li>
        <li><i class="lp-icon lp-icon-repeat"></i> lp-icon lp-icon-repeat</li>
        <li><i class="lp-icon lp-icon-cross"></i> lp-icon lp-icon-cross</li>
        <li><i class="lp-icon lp-icon-arrow-up-right"></i> lp-icon lp-icon-arrow-up-right</li>
        <li><i class="lp-icon lp-icon-arrow-down-left"></i> lp-icon lp-icon-arrow-down-left</li>
        <li><i class="lp-icon lp-icon-resize-full"></i> lp-icon lp-icon-resize-full</li>
        <li><i class="lp-icon lp-icon-resize-small"></i> lp-icon lp-icon-resize-small</li>
        <li><i class="lp-icon lp-icon-caret-up"></i> lp-icon lp-icon-caret-up</li>
        <li><i class="lp-icon lp-icon-caret-down"></i> lp-icon lp-icon-caret-down</li>
        <li><i class="lp-icon lp-icon-caret-right"></i> lp-icon lp-icon-caret-right</li>
        <li><i class="lp-icon lp-icon-caret-left"></i> lp-icon lp-icon-caret-left</li>
        <li><i class="lp-icon lp-icon-angle-right"></i> lp-icon lp-icon-angle-right</li>
        <li><i class="lp-icon lp-icon-angle-left"></i> lp-icon lp-icon-angle-left</li>
        <li><i class="lp-icon lp-icon-angle-up"></i> lp-icon lp-icon-angle-up</li>
        <li><i class="lp-icon lp-icon-angle-down"></i> lp-icon lp-icon-angle-down</li>
    </ul>


    <h2 class="bs-h2" id="ui-icons">UI Icons</h2>
    <ul class="bs-glyphicons">
        <li><i class="lp-icon lp-icon-house"></i> lp-icon lp-icon-house</li>
        <li><i class="lp-icon lp-icon-launcher"></i> lp-icon lp-icon-launcher</li>
        <li><i class="lp-icon lp-icon-th"></i> lp-icon lp-icon-th</li>
        <li><i class="lp-icon lp-icon-th-large"></i> lp-icon lp-icon-th-large</li>
        <li><i class="lp-icon lp-icon-globe"></i> lp-icon lp-icon-globe</li>
        <li><i class="lp-icon lp-icon-star-empty"></i> lp-icon lp-icon-star-empty</li>
        <li><i class="lp-icon lp-icon-star-half"></i> lp-icon lp-icon-star-half</li>
        <li><i class="lp-icon lp-icon-star-half-full"></i> lp-icon lp-icon-star-half-full</li>
        <li><i class="lp-icon lp-icon-plus"></i> lp-icon lp-icon-plus</li>
        <li><i class="lp-icon lp-icon-minus"></i> lp-icon lp-icon-minus</li>
        <li><i class="lp-icon lp-icon-spinner"></i> lp-icon lp-icon-spinner</li>
        <li><i class="lp-icon lp-icon-undo"></i> lp-icon lp-icon-undo</li>
        <li><i class="lp-icon lp-icon-filter"></i> lp-icon lp-icon-filter</li>
        <li><i class="lp-icon lp-icon-comment"></i> lp-icon lp-icon-comment</li>
        <li><i class="lp-icon lp-icon-comments"></i> lp-icon lp-icon-comments</li>
        <li><i class="lp-icon lp-icon-calendar"></i> lp-icon lp-icon-calendar</li>
        <li><i class="lp-icon lp-icon-check-empty"></i> lp-icon lp-icon-check-empty</li>
        <li><i class="lp-icon lp-icon-check"></i> lp-icon lp-icon-check</li>
        <li><i class="lp-icon lp-icon-minus-sign"></i> lp-icon lp-icon-minus-sign</li>
        <li><i class="lp-icon lp-icon-info-sign"></i> lp-icon lp-icon-info-sign</li>
        <li><i class="lp-icon lp-icon-question-sign"></i> lp-icon lp-icon-question-sign</li>
        <li><i class="lp-icon lp-icon-ok-sign"></i> lp-icon lp-icon-ok-sign</li>
        <li><i class="lp-icon lp-icon-remove-sign"></i> lp-icon lp-icon-remove-sign</li>
        <li><i class="lp-icon lp-icon-plus-sign"></i> lp-icon lp-icon-plus-sign</li>
        <li><i class="lp-icon lp-icon-tag"></i> lp-icon lp-icon-tag</li>
        <li><i class="lp-icon lp-icon-download"></i> lp-icon lp-icon-download</li>
        <li><i class="lp-icon lp-icon-eye"></i> lp-icon lp-icon-eye</li>
        <li><i class="lp-icon lp-icon-eye-blocked"></i> lp-icon lp-icon-eye-blocked</li>
        <li><i class="lp-icon lp-icon-screen"></i> lp-icon lp-icon-screen</li>
        <li><i class="lp-icon lp-icon-mobile"></i> lp-icon lp-icon-mobile</li>
        <li><i class="lp-icon lp-icon-mobile2"></i> lp-icon lp-icon-mobile2</li>
        <li><i class="lp-icon lp-icon-tablet"></i> lp-icon lp-icon-tablet</li>
        <li><i class="lp-icon lp-icon-paperclip"></i> lp-icon lp-icon-paperclip</li>
        <li><i class="lp-icon lp-icon-box"></i> lp-icon lp-icon-box</li>
        <li><i class="lp-icon lp-icon-search2"></i> lp-icon lp-icon-search2</li>
        <li><i class="lp-icon lp-icon-pencil"></i> lp-icon lp-icon-pencil</li>
        <li><i class="lp-icon lp-icon-user-add"></i> lp-icon lp-icon-user-add</li>
        <li><i class="lp-icon lp-icon-printer"></i> lp-icon lp-icon-printer</li>
        <li><i class="lp-icon lp-icon-install"></i> lp-icon lp-icon-install</li>
        <li><i class="lp-icon lp-icon-list-and-chart-view"></i> lp-icon lp-icon-list-and-chart-view</li>
        <li><i class="lp-icon lp-icon-line-chart-view"></i> lp-icon lp-icon-line-chart-view</li>
        <li><i class="lp-icon lp-icon-list-view"></i> lp-icon lp-icon-list-view</li>
        <li><i class="lp-icon lp-icon-alert-success"></i> lp-icon lp-icon-alert-success</li>
        <li><i class="lp-icon lp-icon-alert-info"></i> lp-icon lp-icon-alert-info</li>
        <li><i class="lp-icon lp-icon-alert-warning"></i> lp-icon lp-icon-alert-warning</li>
        <li><i class="lp-icon lp-icon-alert-danger"></i> lp-icon lp-icon-alert-danger</li>
        <li><i class="lp-icon lp-icon-alert-error"></i> lp-icon lp-icon-alert-error</li>
    </ul>

      <!--  ACTIVE ICONS -->
      <h2 class="bs-h2" id="active-icons">Active Icons</h2>

    <p>When the widget is active, an .active class will be added to the parent html node.</p>
    <div class="highlight"><pre><code class="html"><span class="nt">&lt;li</span> <span class="na">class=</span><span class="s">&quot;active&quot;</span><span class="nt">&gt;&lt;i</span> <span class="na">class=</span><span class="s">&quot;lp-icon lp-icon-accounts&quot;</span><span class="nt">&gt;&lt;/i&gt;&lt;/li&gt;</span>
</code></pre></div>
      <ul class="bs-glyphicons active-icons">
            <li class="active"><i class="lp-icon lp-icon-accounts"></i> lp-icon lp-icon-accounts</li>
            <li class="active"><i class="lp-icon lp-icon-transactions"></i> lp-icon lp-icon-transactions</li>
            <li class="active"><i class="lp-icon lp-icon-addressbook"></i> lp-icon lp-icon-addressbook</li>
            <li class="active"><i class="lp-icon lp-icon-new-transfer"></i> lp-icon lp-icon-new-transfer</li>
            <li class="active"><i class="lp-icon lp-icon-review-transfers"></i> lp-icon lp-icon-review-transfers</li>
      </ul>

  </div>

  <!--  Icon Sizes -->
  <div class="bs-docs-section">
    <div class="page-header">
      <h1 class="bs-h1" id="icon-sizes">Icon Sizes</h1>
    </div>

    <p class="lead">The size of an icon is, by default, the size of the text defined in the HTML node.  If you want to specify a icon to be larger or smaller, use one of the class modifiers listed in the examples below.</p>

  <h2 class="bs-h2" id="small-icons">Small Icons</h2>
  <div class="highlight"><pre><code class="html"><span class="nt">&lt;i</span> <span class="na">class=</span><span class="s">&quot;lp-icon lp-icon-small lp-icon-accounts&quot;</span><span class="nt">&gt;&lt;/i&gt;</span></code></pre></div>
  <ul class="bs-glyphicons">
        <li><i class="lp-icon lp-icon-small lp-icon-accounts"></i> lp-icon lp-icon-small lp-icon-accounts</li>
        <li><i class="lp-icon lp-icon-small lp-icon-transactions"></i> lp-icon lp-icon-small lp-icon-transactions</li>
        <li><i class="lp-icon lp-icon-small lp-icon-addressbook"></i> lp-icon lp-icon-small lp-icon-addressbook</li>
        <li><i class="lp-icon lp-icon-small lp-icon-new-transfer"></i> lp-icon lp-icon-small lp-icon-new-transfer</li>
        <li><i class="lp-icon lp-icon-small lp-icon-review-transfers"></i> lp-icon lp-icon-small lp-icon-review-transfers</li>
  </ul>

  <h2 class="bs-h2" id="medium-icons">Medium Icons</h2>
  <div class="highlight"><pre><code class="html"><span class="nt">&lt;i</span> <span class="na">class=</span><span class="s">&quot;lp-icon lp-icon-medium lp-icon-accounts&quot;</span><span class="nt">&gt;&lt;/i&gt;</span></code></pre></div>
  <ul class="bs-glyphicons">
        <li><i class="lp-icon lp-icon-medium lp-icon-accounts"></i> lp-icon lp-icon-medium lp-icon-accounts</li>
        <li><i class="lp-icon lp-icon-medium lp-icon-transactions"></i> lp-icon lp-icon-medium lp-icon-transactions</li>
        <li><i class="lp-icon lp-icon-medium lp-icon-addressbook"></i> lp-icon lp-icon-medium lp-icon-addressbook</li>
        <li><i class="lp-icon lp-icon-medium lp-icon-new-transfer"></i> lp-icon lp-icon-medium lp-icon-new-transfer</li>
        <li><i class="lp-icon lp-icon-medium lp-icon-review-transfers"></i> lp-icon lp-icon-medium lp-icon-review-transfers</li>
  </ul>

  <h2 class="bs-h2" id="large-icons">Large Icons</h2>
  <div class="highlight"><pre><code class="html"><span class="nt">&lt;i</span> <span class="na">class=</span><span class="s">&quot;lp-icon lp-icon-large lp-icon-accounts&quot;</span><span class="nt">&gt;&lt;/i&gt;</span></code></pre></div>
  <ul class="bs-glyphicons">
        <li><i class="lp-icon lp-icon-large lp-icon-accounts"></i> lp-icon lp-icon-large lp-icon-accounts</li>
        <li><i class="lp-icon lp-icon-large lp-icon-transactions"></i> lp-icon lp-icon-large lp-icon-transactions</li>
        <li><i class="lp-icon lp-icon-large lp-icon-addressbook"></i> lp-icon lp-icon-large lp-icon-addressbook</li>
        <li><i class="lp-icon lp-icon-large lp-icon-new-transfer"></i> lp-icon lp-icon-large lp-icon-new-transfer</li>
        <li><i class="lp-icon lp-icon-large lp-icon-review-transfers"></i> lp-icon lp-icon-large lp-icon-review-transfers</li>
  </ul>

  <h2 class="bs-h2" id="extra-large-icons">Extra Large Icons</h2>
  <div class="highlight"><pre><code class="html"><span class="nt">&lt;i</span> <span class="na">class=</span><span class="s">&quot;lp-icon lp-icon-xxl lp-icon-accounts&quot;</span><span class="nt">&gt;&lt;/i&gt;</span></code></pre></div>
  <ul class="bs-glyphicons">
        <li><i class="lp-icon lp-icon-xxl lp-icon-accounts"></i> lp-icon lp-icon-xxl lp-icon-accounts</li>
        <li><i class="lp-icon lp-icon-xxl lp-icon-transactions"></i> lp-icon lp-icon-xxl lp-icon-transactions</li>
        <li><i class="lp-icon lp-icon-xxl lp-icon-addressbook"></i> lp-icon lp-icon-xxl lp-icon-addressbook</li>
        <li><i class="lp-icon lp-icon-xxl lp-icon-new-transfer"></i> lp-icon lp-icon-xxl lp-icon-new-transfer</li>
        <li><i class="lp-icon lp-icon-xxl lp-icon-review-transfers"></i> lp-icon lp-icon-xxl lp-icon-review-transfers</li>
  </ul>
  </div>
<!--  WIDGET LAYOUT -->
  <div class="bs-docs-section">
    <div class="page-header">
      <h1 class="bs-h1" id="typography">Typography</h1>

    </div>

    <p class="lead">Launchpad extends the implementation of Bootstrap typography to allow for a separation of heading styles within
    widgets versus marketing content.</p>

    <h3 class="bs-h3" id="widget-header-tags">Widget Header Tags</h3>
    <p>Within a widget, header tags are namespaced because they are used for the size of the text in the chromes, headers and
    other places as necessary.</p>
    <p>In addition to the examples below, you can also use the .lp-h1, .lp-h2, .lp-h3, etc. classes as mixins to set the font-size
    of a certain class.</p>

    <div class="bs-example">
        <div class="panel-chrome">
            <div class="h1">This is a widget with a class of h1</div>
            <h1>This is an h1 tag within a widget.</h1>
            <div class="h2">This is a widget with a class of h2</div>
            <h2>This is an h2 tag within a widget.</h2>
            <div class="h3">This is a widget with a class of h3</div>
            <h3>This is an h3 tag within a widget.</h3>
            <div class="h4">This is a widget with a class of h4</div>
            <h4>This is an h4 tag within a widget.</h4>
            <div class="h5">This is a widget with a class of h5</div>
            <h5>This is an h5 tag within a widget.</h5>
            <div class="h6">This is a widget with a class of h6</div>
            <h6>This is an h6 tag within a widget.</h6>
        </div>
    </div>

    <h3 class="bs-h3" id="page-content-header-tags">Page Content Header Tags</h3>

    <p>Within content, header tags use the styles as documented in the Bootstrap docs.</p>

    <div class="bs-example">
        <div class="h1">This is page content with a class of h1</div>
            <h1>This is an h1 tag within page content.</h1>
            <div class="h2">This is page content with a class of h2</div>
            <h2>This is an h2 tag within page content.</h2>
            <div class="h3">This is page content with a class of h3</div>
            <h3>This is an h3 tag within page content.</h3>
            <div class="h4">This is page content with a class of h4</div>
            <h4>This is an h4 tag within page content.</h4>
            <div class="h5">This is page content with a class of h5</div>
            <h5>This is an h5 tag within page content.</h5>
            <div class="h6">This is page content with a class of h6</div>
            <h6>This is an h6 tag within page content.</h6>

    </div>

</div>

<div class="bs-docs-section">
    <div class="page-header">
      <h1 class="bs-h1" id="lp-helper-classes">Launchpad Helper classes</h1>
    </div>

    <h3 class="bs-h3" id="remove-padding">Remove Padding</h3>
    <p>Remove the inherited left, right, top, bottom, or all margins from an element. In the future this will be a mixin.</p>
    <div class="highlight">
        <pre><code class="html"><span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"no-padding"</span><span class="nt">&gt;</span><span class="nt">&lt;/div&gt;</span>
        <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"no-padding-right"</span><span class="nt">&gt;</span><span class="nt">&lt;/div&gt;</span>
        <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"no-padding-left"</span><span class="nt">&gt;</span><span class="nt">&lt;/div&gt;</span>
        <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"no-padding-top"</span><span class="nt">&gt;</span><span class="nt">&lt;/div&gt;</span>
        <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"no-padding-bottom"</span><span class="nt">&gt;</span><span class="nt">&lt;/div&gt;</span>
        </code></pre>
    </div>

    <h3 class="bs-h3" id="remove-margins">Remove Margins</h3>
    <p>Remove the inherited left, right, top, bottom, or all margins from an element. In the future this will be a mixin.</p>
    <div class="highlight">
    <pre><code class="html"><span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"no-margin"</span><span class="nt">&gt;</span><span class="nt">&lt;/div&gt;</span>
<span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"no-margin-right"</span><span class="nt">&gt;</span><span class="nt">&lt;/div&gt;</span>
<span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"no-margin-left"</span><span class="nt">&gt;</span><span class="nt">&lt;/div&gt;</span>
<span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"no-margin-top"</span><span class="nt">&gt;</span><span class="nt">&lt;/div&gt;</span>
<span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"no-margin-bottom"</span><span class="nt">&gt;</span><span class="nt">&lt;/div&gt;</span>
</code></pre>
    </div>

    <h3 class="bs-h3" id="force-cursors">Force Cursors</h3>
    <p>Force an element to display a specific type of cursor.  In the future this will be a mixin to accomodate all cursors.</p>
    <div class="highlight">
    <pre><code class="html"><span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"cursor-pointer"</span><span class="nt">&gt;</span>...<span class="nt">&lt;/div&gt;</span>
    </code></pre>
    </div>

    <h3 class="bs-h3" id="remove-backgrounds">Remove Backgrounds</h3>
    <p>Remove the inherited background from an element.  In the future this will be a mixin to specify a specific background.</p>
    <div class="highlight">
        <pre><code class="html"><span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"no-bg"</span><span class="nt">&gt;</span>...<span class="nt">&lt;/div&gt;</span>
        </code></pre>
    </div>


  <h3 class="bs-h3" id="display-hide">Display / Hide Content</h3>
    <p>Toogle display:none and display:block.  In the future this will be a mixin to display content using a custom value.</p>

    <div class="highlight">
    <pre><code class="html">
&lt;div class="display-none"&gt;...&lt;/div&gt;
&lt;div class="display-block"&gt;...&lt;/div&gt;
    </code></pre>
    </div>

</div>

<!-- <div class="bs-docs-section"> -->
<!--     <div class="page-header"> -->
<!--       <h1 class="bs-h1" id="lp-ff">Launchpad Fluid/Fixed Layout Mixin</h1> -->
<!--     </div> -->

<!--     <p class="lead">Since Bootstrap no longer allows for fixed/fluid columns and opts for a grid-based design, we have -->
<!--     included a simple mixin to create 2 column layouts with one fixed, one fluid column (because we still needed it).</p> -->

<!--     <h3 class="bs-h3" id="lp-ff-left">Fixed Left</h3> -->
<!--     <p>Create a two column layout with a fixed left column and fluid right column by passing the left column width.</p> -->

<!--     <div class="highlight"> -->
<!--     <pre><code class="css"> -->
<!-- .ff-left-42 { -->
<!--     .ff-fixed-left(42px); -->
<!-- } -->
<!--     </code></pre> -->
<!--     </div> -->

<!--     <div class="highlight"> -->
<!--     <pre><code class="html"> -->
<!-- &lt;div class="ff-left-150 ff-row"&gt; -->
<!--     &lt;div class="ff-fixed"&gt; -->
<!--         &lt;div class="ff-content"&gt; -->
<!--             ... -->
<!--         &lt;/div&gt; -->
<!--     &lt;/div&gt; -->
<!--     &lt;div class="ff-fluid"&gt; -->
<!--         &lt;div class="ff-content"&gt; -->
<!--             ... -->
<!--         &lt;/div&gt; -->
<!--     &lt;/div&gt; -->
<!-- &lt;/div&gt; -->
<!--     </code></pre> -->
<!--     </div> -->

<!--     <div class="bs-example"> -->
<!--         <div class="ff-row ff-left-150"> -->
<!--             <div class="ff-fixed"> -->
<!--                 <div class="ff-content"> -->
<!--                     Lorem ipsum dolor sit amet, consectetur adipiscing elit. -->
<!--                 </div> -->
<!--             </div> -->
<!--             <div class="ff-fluid"> -->
<!--                 <div class="ff-content"> -->
<!--                    Curabitur suscipit enim nibh, facilisis facilisis nunc lobortis dapibus. Proin arcu nisl, ullamcorper a tempus nec, sodales eget odio. Vivamus accumsan, neque et blandit venenatis, neque neque blandit erat, vitae viverra augue felis ultrices dolor. Etiam augue augue, malesuada sit amet blandit vel, tristique non felis. -->
<!--                 </div> -->
<!--             </div> -->
<!--         </div> -->
<!--     </div> -->

<!--     <h3 class="bs-h3" id="lp-ff-right">Fixed Right</h3> -->
<!--     <p>Create a two column layout with a fixed left column and fluid right column by passing the right column width.</p> -->

<!--     <div class="highlight"> -->
<!--     <pre><code class="css"> -->
<!-- .ff-right-42 { -->
<!--     .ff-fixed-right(42px); -->
<!-- } -->
<!--     </code></pre> -->
<!--     </div> -->

<!--     <div class="highlight"> -->
<!--     <pre><code class="html"> -->
<!-- &lt;div class="ff-right-150 ff-row"&gt; -->
<!--     &lt;div class="ff-fluid"&gt; -->
<!--         &lt;div class="ff-content"&gt; -->
<!--             ... -->
<!--         &lt;/div&gt; -->
<!--     &lt;/div&gt; -->
<!--     &lt;div class="ff-fixed"&gt; -->
<!--         &lt;div class="ff-content"&gt; -->
<!--             ... -->
<!--         &lt;/div&gt; -->
<!--     &lt;/div&gt; -->
<!-- &lt;/div&gt; -->
<!--     </code></pre> -->
<!--     </div> -->

<!--     <div class="bs-example"> -->
<!--         <div class="ff-row ff-right-150"> -->
<!--             <div class="ff-fluid"> -->
<!--                 <div class="ff-content"> -->
<!--                    Curabitur suscipit enim nibh, facilisis facilisis nunc lobortis dapibus. Proin arcu nisl, ullamcorper a tempus nec, sodales eget odio. Vivamus accumsan, neque et blandit venenatis, neque neque blandit erat, vitae viverra augue felis ultrices dolor. Etiam augue augue, malesuada sit amet blandit vel, tristique non felis. . -->
<!--                 </div> -->
<!--             </div> -->
<!--             <div class="ff-fixed"> -->
<!--                 <div class="ff-content"> -->
<!--                     Lorem ipsum dolor sit amet, consectetur adipiscing elit. -->
<!--                 </div> -->
<!--             </div> -->
<!--         </div> -->
<!--     </div> -->


<!-- </div> -->




</div>
</div>
</div>


<%@include file="includes/page-footer.jsp" %>



<script>
// if (!window.bd) window.bd = {};

// // Get custom styles from uiEditingOptions.js and display in Custom Styles
// $( function() {

//     var $customStyleContainer = $('.ui-custom-styles');
//     var customStyleObj = bd.uiEditingOptions.customStyles;
//     var customStylesOutput = '';

//     for (var key in customStyleObj) {
//        var obj = customStyleObj[key];
//        for (var prop in obj) {
//           if(obj.hasOwnProperty(prop)){
//               var newStyle = key + "-" + obj[prop];
//               customStylesOutput += '<p class="'+ newStyle+ '">'+ newStyle+ '</p>';
//           }
//        }
//     }

//     if ($customStyleContainer) {
//         $customStyleContainer.html(customStylesOutput);
//     }

// });
</script>

<!-- <script src="http://localhost:7777/portalserver/static/conf/uiEditingOptions.js"></script> -->


  </body>
</html>
