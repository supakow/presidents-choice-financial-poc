# Runs Launchpad functional tests:

./src/test/support/casperjs-1.1-beta2/bin/casperjs test src/test/suites/launchpad-retail/tests/$2 --includes=src/test/includes/common.js --server=$1 --buildDir=target --log-level=debug --verbose --xunit=target/reports/casper-results.xml