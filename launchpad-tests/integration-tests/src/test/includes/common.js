//require modules
var $ = require("../../../support/jquery");
var restClient = require("../../../lib/rest-client");
var testUtil = require("../../../lib/launchpad-test-util");
var utils = require('utils');

casper.options.viewportSize = {width: 1200, height: 778}
casper.options.waitTimeout = 10000;

//env setup
var server = casper.cli.has("server") ? casper.cli.get("server") : "http://launchpad.backbase.com:8180";
var buildDir = casper.cli.get("buildDir");

var contextPath = "/portalserver";
restClient.setDefaultServer(server);
restClient.setDefaultContextPath(contextPath);

//events
casper.captureToTarget = function(name) {
	casper.capture(buildDir + "/captures/" + name + ".png");
};
casper.on("test.fail", function(failure) {
    casper.captureToTarget("fail");
});
casper.on("waitFor.timeout", function(details) {
    casper.test.comment("Timeout happend: " + JSON.stringify(details));
	casper.captureToTarget("timeout");
});

casper.on("resource.received", function(resource) {
    if(resource) {
        casper.info(resource.status + ' - ' + resource.url);
    }
});
var errors = 0;
casper.options.onWaitTimeout = function() {
    casper.capture(buildDir + "/captures/error-" + (++errors) + ".png");
    this.exit(1);
};

//shortcut logging
casper.info = function(msg) {
    casper.log(msg, "info");
};
casper.warn = function(msg) {
    casper.log(msg, "warning");
};
casper.error = function(msg) {
    casper.log(msg, "error");
};
casper.debug = function(msg) {
    casper.log(msg, "debug");
};

//microdata support
//see http://foolip.org/microdatajs/live/
casper.options.clientScripts.push("../../../support/a.js");
casper.options.clientScripts.push("../../../support/microdata-js.js");

/**
 * Fetches microdata from the remote DOM environment, in a json object structure.
 * @param itemType The item type of item scopes to fetch data for
 * @returns {Object|mixed}
 */
casper.fetchMicrodata = function(itemType) {
	"use strict";

	return casper.evaluate(function(itemType) {

		//this function is adapted from https://github.com/abernier/microdatajs/blob/master/jquery.microdata.json.js
		var getMicrodata = function(itemType) {

			function getObject(item, memory) {
				var result = {};
				result.type = item.itemType;
				result.id = item.itemId;
				result.properties = {};
				[].slice.call(item.properties).forEach(function (elem) {
					var value;
					if (elem.itemScope) {
						if (memory.indexOf(elem) > -1) {
							memory.push(item);
							value = getObject(elem, memory);
							memory.pop();
						}
					} else {
						value = elem.itemValue;
					}
					[].slice.call(elem.itemProp).forEach(function (prop) {
						result.properties[prop] = result.properties[prop] || [];
						result.properties[prop].push(value);
					});
				});
				return result;
			}

			var result = {};
			result.items = [];
			var items = document.getItems(itemType);
			[].slice.call(items).forEach(function (item) {
				result.items.push(getObject(item, []));
			});
			return result;
		};

		return getMicrodata(itemType);
	}, itemType);
};

/**
 * Asserts a microdata item value matches the given expected value in the remote DOM environment
 * Depends on the casper.fetchMicrodata method
 *
 * @param itemType {String} The item type of the item scopes to look for properties in
 * @param property {String} The name of the property the compare the expected value against
 * @param expectedItemValue {String} The expected value of the item property
 * @param message {String} The assertion message
 * @param itemTypeIndex {Number} A specific index of an item scope to use. If omitted or null, will look for matches in
 *                                  all item scopes
 * @param itemValueIndex {Number} A specific property index to match againist. If omitted or null, will look for all
 *                                  values in the property array
 */
casper.test.assertMicrodata = function(itemType, property, expectedItemValue, message, itemTypeIndex, itemValueIndex) {
	"use strict";

	var foundMatch = false;
	var microdata = casper.fetchMicrodata(itemType);
	if(typeof itemTypeIndex === "number" && microdata.items[itemTypeIndex]) {
		microdata.items = [ microdata.items[itemTypeIndex] ];
	}

	microdata.items.forEach(function(item) {
		var props = item.properties[property];
		if(props) {
			if(typeof itemTypeIndex === "number" && props[itemValueIndex]) {
				props = [ props[itemValueIndex] ];
			}
			for(var i = 0; i < props.length && !foundMatch; i++) {
				foundMatch = utils.equals(props[i], expectedItemValue);
			}
		}
	});

	return this.assert(foundMatch, message, {
		type: "assertMicrodata",
		standard: "Matching microdata itemValue found for " + itemType + ":" + property,
		values: {
			subject: property,
			expected: expectedItemValue
		}
	});
};

//angular extensions
casper.waitForAngularController = function(controllerName, callback, timeoutCallback) {

	casper.waitUntilVisible('[ng-controller="' + controllerName + '"]', function then(){
		if(typeof callback === "function") {
			callback.call(casper);
		}
	}, function timeout() {
		if(typeof timeoutCallback === "function") {
			timeoutCallback.call(casper);
		}
	});
};
casper.thenWait = function(time, message, callback) {

    casper.then(function() {
        casper.echo("# Waiting for: " + message);
        casper.wait(time, function() {
            if(typeof callback === "function") {
                callback.call(casper);
            }
        });
    });
};
casper.ignore = function(message) {
	casper.then(function() {
		casper.echo("IGNORE " + message);
	});
};
