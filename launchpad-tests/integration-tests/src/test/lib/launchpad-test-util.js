var restClient = require("./rest-client");

var util = {};

util.login = function(casper, loginPath, username, password) {

    casper.thenOpen(loginPath, function () {

        this.fill("form.bd-loginForm-dashboard", {
            j_username: username,
            j_password: password
        }, true);
    });
};

util.loginAsAdmin = function(casper, server) {

    util.login(casper, server + "/portalserver/login/loginDashboard.jsp", "admin", "admin");
};

util.createPage = function(casper, parentName, name, props) {

    props = props || {};

    //make a new page
    var request = restClient.makePostPageRequest({
        name: name,
        contextItemName: "launchpad-tests",
        parentItemName: parentName,
        properties: [{
            name: "TemplateName",
            type: "string",
            value: props.template || "LaunchpadPageTemplate"
        }, {
            name: "title",
            type: "string",
            value: "New page: " + name
        }]
    });

    sendRequest(casper, request);
};

util.deletePage = function(casper, name) {

    //make a new page
    var request = restClient.makeDeletePageRequest({
        name: name,
        contextItemName: "launchpad-tests"
    });
    sendRequest(casper, request);
};
util.deleteWidget = function(casper, name) {

    //make a new page
    var request = restClient.makeDeleteWidgetRequest({
        name: name,
        contextItemName: "launchpad-tests"
    });
    sendRequest(casper, request);
};

util.createContainer = function(casper, parent, extended, name, defaultProps, extendedProps) {

    var properties = [
        {
            name: "TemplateName",
            type: "string",
            value: defaultProps.template
        },
        {
            name: "title",
            type: "string",
            value: defaultProps.title || "New container: " + name
        },
        {
            name: "area",
            type: "string",
            value: defaultProps.area || "0"
        },
        {
            name: "order",
            type: "double",
            value: defaultProps.order || 0
        }
    ];

    if (extendedProps) {
        properties = properties.concat(extendedProps);
    }

    //make a new page
    var request = restClient.makePostContainerRequest({
        name: name,
        contextItemName: "launchpad-tests",
        parentItemName: parent,
        extendedItemName: extended,
        properties: properties
    });
    sendRequest(casper, request);
};

util.createWidget = function(casper, parent, extended, name, props) {
    var prop = null,
        properties = [],
        _props = {
            title: {
                value: "New widget: " + name,
                type: "string"
            },
            area: {
                value: "0",
                type: "string"
            },
            order: {
                value: 0,
                type: "double"
            },
            widgetChrome: {
                value: "$(contextRoot)/static/backbase.com.launchpad/chromes/default/chrome-default.html",
                type: "string"
            }
        };
    
        for (prop in props) {
            if (!{}.hasOwnProperty.call(props, prop)) continue;
            if(_props[prop] == undefined) {
                _props[prop] = { type: "string" };
            }
            _props[prop].value = props[prop];
        }
        for (prop in _props) {
            if (!{}.hasOwnProperty.call(_props, prop)) continue;
            properties.push({
                name: prop,
                type: _props[prop].type,
                value: _props[prop].value
            });
        }
    
    //make a new widget
    var request = restClient.makePostWidgetRequest({
        name: name,
        contextItemName: "launchpad-tests",
        parentItemName: parent,
        extendedItemName: extended,
        properties: properties
    });
    sendRequest(casper, request);
};

var sendRequest = function (casper, request) {

    casper.debug("Sending request\n\turl: " + request.url + "\n\tmethod: " + request.method + "\n\tbody: " + (request.body ? "\n" + request.body : "n/a"));
    casper.thenOpen(request.url, {
        method: request.method,
        data: request.body,
        headers: {
            "Content-Type": "application/xml; charset=UTF-8"
        }
    });
    casper.debug("Request sent");
};

module.exports = util;