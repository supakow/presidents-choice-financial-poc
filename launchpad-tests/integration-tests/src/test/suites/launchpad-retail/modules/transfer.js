//export
exports.createPaymentOrder = createPaymentOrder;
exports.sendPaymentOrders = sendPaymentOrders;

var NEWTRANSFER_SELECTOR = '[id^=widget-new-transfer-v1]';
var REVIEWTRANSFER_SELECTOR = '[id^=widget-review-transfers-v1]';

/**
 * Test to create a payment order, first log in and ensure the widget is on the page
 * @param params
 * @returns {number}
 */
createPaymentOrder.planned = 1;
function createPaymentOrder(params) {

	var fromAccountIndex = params.fromAccountIndex;
	var counterpartyName = params.counterpartyName;
	var ibanInputField = params.ibanInputField;
	var instructedAmount = params.instructedAmount;
	var counterpartyBank = params.counterpartyBank;

	casper.then(function(){
		casper.waitForAngularController('NewTransferController');
	});

	casper.then(function(){
		//clicks the account select
		casper.click(NEWTRANSFER_SELECTOR + ' div[lp-accounts-select] button');
		casper.waitUntilVisible(NEWTRANSFER_SELECTOR + ' div[lp-accounts-select] .dropdown-menu', function(){

			//select an account
			casper.click(NEWTRANSFER_SELECTOR + ' div[lp-accounts-select] .dropdown-menu li:nth-child(' + fromAccountIndex + ') a');

			var paymentSummary =
				"Counterparty Name: " + counterpartyName + "\n" +
				"Counterparty Account: " + ibanInputField + "\n" +
				"Instructed Amount: " + instructedAmount + "...";

			casper.test.comment("Creating payment order for \n" + paymentSummary);

			casper.fill(NEWTRANSFER_SELECTOR + ' form[name="paymentOrderForm"]', {
				'counterpartyName' : counterpartyName,
				'ibanInputField' : ibanInputField,
				'wholeAmountInput' : instructedAmount
			}, false);



			casper.click(NEWTRANSFER_SELECTOR + ' button[name="submitForm"]');
            casper.captureToTarget("new-transfer-form");
			casper.test.comment("Payment order created.");
		});
	});

	casper.thenWait(2000);
	casper.then(function() {
        casper.captureToTarget("review-transfer-widget");
		var microdata = casper.fetchMicrodata("http://launchpad.backbase.com/vocab/retail/payment-order");

		//find the order!
		microdata.items.forEach(function(order) {
			var name = order.properties.counterpartyName[0].trim();
			if(name === counterpartyName) {
				casper.test.assertEquals(order.properties.counterpartyName[0].trim(), counterpartyName, "Pending order's name is correct");
				//casper.test.assertEquals(order.properties.wholeAmount[0].trim(), instructedAmount, "Pending order's amount is correct");
			}
		});
	});
}

sendPaymentOrders.planned = 1;
function sendPaymentOrders() {

	casper.then(function(){
		casper.waitForAngularController('ReviewTransfersController');
	});

	casper.then(function() {
		casper.click(REVIEWTRANSFER_SELECTOR + ' button[name="submitPayments"]');
	});

	casper.waitUntilVisible(".alert-success", function() {
		casper.test.pass("Success message displayed for sending orders")
	}, function timeout() {
		casper.test.comment("The success message never appeared :(");
	});
}