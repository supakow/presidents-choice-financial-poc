//export
exports.loginAsUser = loginAsUser;
exports.logout = logout;
exports.loginAsUserTimes = loginAsUserTimes;

loginAsUser.planned = 2;
loginAsUserTimes.planned = loginAsUser.planned + 0;

/**
 * Test to login as a user as a resusable component
 * @param casper The casper instance to use
 * @param test The test instance to use
 * @param server The server to test on
 * @param contextPath The context path of the server to test on
 */
function loginAsUser(username, password, displayName, forceLogout) {

	//root selectors for widgets used in this test
	var loginWidgetSelector = '[id^=widget-login-multifactor]';
	var profileWidgetSelector = '[id^=widget-profile-summary]'

	var loggedInAlready = false;
	if(forceLogout) {
		logout();
	} else {
		loggedInAlready = casper.evaluate(function(username) {
			return b$ && b$.portal.loggedInUserId === username
		}, username);
	}

	//open home page
	casper.thenOpen(server + contextPath + "/home", function then() {

		//are we on the home page?
		casper.test.assertTitle("Home", "We are on the home page");

		if(!loggedInAlready) {

			casper.test.comment("Logging in as " + username + " / " + password);

			//wait for the login widget to complete loading
			casper.waitUntilVisible(loginWidgetSelector + " form" , function then() {

				//check the form is there
				casper.test.comment("Login form is visible");

				//fill in the form
				casper.fill(loginWidgetSelector + " form", {
					'j_username': username,
					'j_password':  password
				});
			}, function timeout() {
				casper.test.comment("Login form not visible");
			});

			//wait for angular js to enable the login button, then click it
            casper.waitWhileSelector(loginWidgetSelector + ' form button[disabled]', function then() {
	            casper.test.comment("Attempting to login in...")
				casper.click(loginWidgetSelector + " form button");
			}, function timeout() {
	            casper.test.comment("Couldn't log in, the form button did not become enabled");
            });

			//wait until the page has reload and the profile summary text becomes visible
			casper.waitUntilVisible(profileWidgetSelector, function then() {
	            casper.thenWait(5000, "profile summary", function then() {

		            casper.captureToTarget("login-ready");
		            casper.test.assertMicrodata("http://launchpad.backbase.com/vocab/profile", "username", displayName);

					//check the login name is correct
		            casper.test.comment("Logging is complete. [as " + displayName + "]");
				});
			}, function timeout() {
				casper.captureToTarget("login-ready-timeout");
				casper.test.comment("Profile summary widget never appeared!");
			}, 20000);

		} else {
			casper.test.assertEval(function(username) {
				return "lisa" === username
			}, "Already logged in as " + username, {
				username: username
			});
		}

	}, function onTimeout() {
		casper.test.comment("Logging in timeout out after 10 seconds.");
	});
}

function loginAsUserTimes(username, password, displayName, max, count) {

	count = count || 0;

	if(count < max ) {
		//login as lisa
		casper.then(function() {
			loginAsUser(username, password, displayName, true);
		});
		casper.then(function() {
			count++;
			casper.test.comment("Login count is " + count);
			loginAsUserTimes(username, password, displayName, max, count)
		});
	}
}

function logout () {
	//first ensure we are logged out
	casper.test.comment("Logging out...");
	casper.thenOpen(server + contextPath + "/" +
		"j_spring_security_logout")
	casper.test.comment("Log out complete.");
}