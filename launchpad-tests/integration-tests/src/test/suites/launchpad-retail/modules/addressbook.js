//export
exports.createContact = createContact;
exports.selectUser = selectUser;
exports.createQuickPaymentOrder = createQuickPaymentOrder;

createContact.planned = 2;

var ADDRESSBOOK_SELECTOR = '[id^=widget-addressbook-v1]';

/**
 * Test to create a new contact in Address Book widget
 */


//create a new contact with microdatajs

function createContact(params){
    var contactName = params.contactName;
    var contactAccount = params.contactAccount;

    casper.thenWait(3000, "Addressbook widget to initialize");

    casper.then(function() {
	    casper.captureToTarget("address-book-add-contact-button")
        casper.click(ADDRESSBOOK_SELECTOR + ' button[name="addContact"]');
    });
    casper.thenWait(1000, "Create contact form");

    casper.then(function() {
        casper.fill(ADDRESSBOOK_SELECTOR + ' form[name="addContact"]', {
            'name' : contactName,
            'account' : contactAccount
        }, false);
        casper.captureToTarget("address-book-add-contact-form");
        casper.click(ADDRESSBOOK_SELECTOR + ' button[name="createContact"]');

        casper.waitUntilVisible(ADDRESSBOOK_SELECTOR + " .alert", function() {
            casper.test.pass("Success message displayed for adding contact")
        }, function timeout() {
            casper.test.comment("The success message for adding a contact never appeared :(");
        });
    });

	casper.thenWait(600);

    //Verify that created contacts are in the list of Address Book`
    casper.then(function() {

        var microdata = casper.fetchMicrodata("http://launchpad.backbase.com/vocab/contact/summary");

        //loop through microdata to find the contact we just added
        casper.test.comment("Looking for " + contactName + " in contacts list");
        var contactFound = false;
        for(var i = 0; i < microdata.items.length; i++) {
            var contact = microdata.items[i];
            var currentContactName = contact.properties.contactName[0].trim();
            casper.test.comment(currentContactName + " is in the contacts list");
            if(currentContactName == contactName) {
                contactFound = true;
            }
        }

        if(contactFound) {
            casper.test.pass(contactName + " was found in the contacts list");
        } else {
            casper.test.fail("Couldn't find " + contactName + " in the contacts list");
        }
    });
}

function selectUser(account) {
        casper.click(ADDRESSBOOK_SELECTOR + " [data-contactAccount='" + account + "']");
        casper.captureToTarget('selected');
}

/**
 * This makes a payment to a contact from the addressbook view. It assumes the contact is already selected.
 * @param params
 */
function createQuickPaymentOrder(params) {
    var instructedAmount = params.instructedAmount;
    var fromAccountIndex = params.fromAccountIndex;
    var counterpartyName = params.counterpartyName;

    casper.then(function(){
        casper.waitForAngularController('ContactsPaymentController');
    });

    casper.then(function(){

        //select an account
        casper.click(ADDRESSBOOK_SELECTOR + ' div[lp-accounts-select] button');
    });
    casper.thenWait(100, "Account dropdown");
    casper.then(function() {
        casper.click(ADDRESSBOOK_SELECTOR + ' div[lp-accounts-select] .dropdown-menu li:nth-child(' + fromAccountIndex + ') a');

        var paymentSummary =
                "Instructed Amount: " + instructedAmount + "...";
                "Counterparty Name: " + counterpartyName + "...";

        casper.test.comment("Creating payment order from Addressbook for \n" + paymentSummary);

        casper.fill(ADDRESSBOOK_SELECTOR + ' form[name="paymentOrderForm"]', {
            'instructedAmount' : instructedAmount
        }, false);

        casper.click(ADDRESSBOOK_SELECTOR + ' button[name="submitForm"]');

        casper.test.comment("Addressbook payment order created.");

    });
}

