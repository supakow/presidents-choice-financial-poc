//export
exports.getRandomName = getRandomName;
exports.getRandomAmount = getRandomAmount;

/**
 * Test to create a new contact in Address Book widget
 */
function getRandomName() {

    function getRandomInt (min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;


    }

    var cons = "abcdfghjklmnprstvwxyz"
    var vowels = "aeoiu";

    var getRandomChar = function(str) {
        return str.substr(getRandomInt(0, str.length - 1), 1);


    }

    function getRandomNamePart(length) {

        var name = "";
        var len = getRandomInt(3, length);
        for(var i = 0; i < len; i = i + 2) {

            var randomCons = getRandomChar(cons);


            if(i === 0) {
                console.log(i)
                randomCons = randomCons.toUpperCase();
            }
            name += randomCons;
            name += getRandomChar(vowels);
        }



        return name
    }

    return getRandomNamePart(5) +  " " + getRandomNamePart(7);
}

function getRandomAmount(from, to) {
    Math.floor(Math.random() * (to - from + 1) + from);
}
