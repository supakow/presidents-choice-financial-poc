var angular = require('../../launchpad/modules/angular');

//export
exports.createPaymentOrder = createPaymentOrder;
exports.sendPaymentOrders = sendPaymentOrders;

var NEWTRANSFER_SELECTOR = '[id^=widget-new-transfer-v1]';
var REVIEWTRANSFER_SELECTOR = '[id^=widget-review-transfers-v1]';

/**
 * Test to create a payment order, first log in and ensure the widget is on the page
 * @param params
 * @returns {number}
 */
function checkTransactionExists(params) {

	var fromAccountIndex = params.fromAccountIndex;
	var counterpartyName = params.counterpartyName;
	var counterpartyAccount = params.counterpartyAccount;
	var instructedAmount = params.instructedAmount;
	var counterpartyBank = params.counterpartyBank;

	casper.then(function(){
		angular.initialize('NewTransferController');
	});

	casper.then(function(){
		//clicks the account select
		casper.click(NEWTRANSFER_SELECTOR + ' div[lp-accounts-header] button');
		casper.waitUntilVisible(NEWTRANSFER_SELECTOR + ' div[lp-accounts-header] .dropdown-menu', function(){

			//select an account
			casper.click(NEWTRANSFER_SELECTOR + ' div[lp-accounts-header] .dropdown-menu li:nth-child(' + fromAccountIndex + ') a');

			var paymentSummary =
				"Counterparty Name: " + counterpartyName + "\n" +
				"Counterparty Account: " + counterpartyAccount + "\n" +
				"Counterparty Bank: " + counterpartyBank + "\n" +
				"Instructed Amount: " + instructedAmount + "\n";

			casper.test.comment("Creating payment order for \n" + paymentSummary + "...");

			casper.fill(NEWTRANSFER_SELECTOR + ' form[name="paymentOrderForm"]', {
				'counterpartyName' : counterpartyName,
				'counterpartyAccount' : counterpartyAccount,
				'instructedAmount' : instructedAmount,
				'counterpartyBank' : counterpartyBank
			}, false);

			casper.click(NEWTRANSFER_SELECTOR + ' button[name="submitForm"]');

			casper.test.comment("Payment order created.");
		});
	});

	casper.then(function(){
		casper.waitForAngularController('ReviewTransfersController');
	});

	casper.then(function() {

		casper.waitUntilVisible(REVIEWTRANSFER_SELECTOR + ' .lp-launcher-tab-widget', function() {

			var microdata = casper.fetchMicrodata("http://launchpad.backbase.com/vocab/retail/payment-order");
			var firstOrder = microdata.items[0];
			casper.test.assertEquals(firstOrder.properties.counterpartyName[0].trim(), counterpartyName, "Pending order's name is correct");
			casper.test.assertEquals(firstOrder.properties.counterpartyAccount[0].trim(), counterpartyAccount, "Pending order's account is correct");
			casper.test.assertEquals(firstOrder.properties.instructedAmount[0].trim(), "€ " + instructedAmount + ".00", "Pending order's amount is correct");

		}, function timeout() {
			casper.test.comment("Review transfers never became visible");
		});
	});

	//runs 3 assertions
	return 3;
}


function sendPaymentOrders() {

	casper.then(function(){
		casper.waitForAngularController('ReviewTransfersController');
	});

	casper.then(function() {
		casper.click(REVIEWTRANSFER_SELECTOR + ' button[name="submitPayments"]');
	});

	casper.waitUntilVisible(".alert-success", function() {
		casper.test.pass("Success message displayed for sending orders")
	}, function timeout() {
		casper.test.comment("The success message never appeared :(");
	});

	return 1;
}