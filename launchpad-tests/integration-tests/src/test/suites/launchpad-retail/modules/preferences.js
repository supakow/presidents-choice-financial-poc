//export
exports.editPreferences = editPreferences;
exports.assertNewPreferences = assertNewPreferences;

assertNewPreferences.planned = 4;

var WIDGET_PREFERENCES_SELECTOR = '[id^=widget-preferences]';

//change preferences for logged in user
function editPreferences(params){
    var preferredName = params.preferredName;
    var preferredLanguage = params.preferredLanguage;
    var defaultAccount = params.defaultAccount;
    var defaultBalance = params.defaultBalance;


    casper.then(function() {
        this.click(WIDGET_PREFERENCES_SELECTOR + ' .lp-icon-pencil');
        console.log('###### pencil icon is clicked OK! ', preferredName, params);
        casper.captureToTarget('edit-form');
    });

    //preferred name
    casper.then(function() {
        casper.fillSelectors(WIDGET_PREFERENCES_SELECTOR + ' form[name="preferencesForm"]', {
            'input' : preferredName

        },false);

        casper.click(WIDGET_PREFERENCES_SELECTOR + ' .btn-default .lp-icon-check');
        casper.captureToTarget('preferredName');
    });

    //preferred language
    casper.then(function() {
        casper.click(WIDGET_PREFERENCES_SELECTOR + ' .preferred-language .btn');
        casper.waitUntilVisible(WIDGET_PREFERENCES_SELECTOR + ' .preferred-language .dropdown-menu', function then(){
            casper.click(WIDGET_PREFERENCES_SELECTOR + ' .preferred-language .dropdown-menu li:nth-child(' + preferredLanguage.index + ')');
            console.log("### Preferred language has been selected: ", preferredLanguage.value);
            casper.captureToTarget('preferredLanguage')
        });
    });

    //default account
    casper.then(function() {
        casper.click(WIDGET_PREFERENCES_SELECTOR + ' .default-account .btn');
        casper.waitUntilVisible(WIDGET_PREFERENCES_SELECTOR + ' .default-account .dropdown-menu', function then(){
            casper.click(WIDGET_PREFERENCES_SELECTOR + ' .default-account .dropdown-menu li:nth-child(' + defaultAccount.index + ')');
            console.log("### Default account has been selected: ", defaultAccount.value);
            casper.captureToTarget('defaultAccount')
        });
    });

    //default balance
    casper.then(function() {
        casper.click(WIDGET_PREFERENCES_SELECTOR + ' .default-balance .btn');
        casper.waitUntilVisible(WIDGET_PREFERENCES_SELECTOR + ' .default-balance .dropdown-menu', function then(){
            casper.click(WIDGET_PREFERENCES_SELECTOR + ' .default-balance .dropdown-menu li:nth-child(' + defaultBalance.index + ')');
            console.log("### Default balance has been selected: ", defaultBalance.value);
            casper.captureToTarget('defaultBalance')
            casper.wait(2000);
        });
    });
}

function assertNewPreferences(params) {
    casper.wait(2000);
    casper.waitUntilVisible(".lp-preferences", function then(){
        var name_field, language_field, default_account_field, default_balance;
        
        var name_field = this.evaluate(function() {
           return document.querySelector('[id^=widget-preferences] .preferred-name .lp-editable-text').innerHTML;
        });
        this.test.assertEquals(name_field, params.preferredName, "Preferred name matches");
        
        var language_field = this.evaluate(function() {
            return document.querySelector('[id^=widget-preferences] .preferred-language .btn span').innerHTML;
        });
        this.test.assertEquals(language_field, params.preferredLanguage.value, "Preferred language matches");

        var default_account_field = this.evaluate(function() {
            return document.querySelector('[id^=widget-preferences] .default-account .btn span').innerHTML;
        });
        this.test.assertEquals(default_account_field, params.defaultAccount.value, "Default account matches");

        var default_balance = this.evaluate(function() {
            return document.querySelector('[id^=widget-preferences] .default-balance .btn span').innerHTML;
        });
        this.test.assertEquals(default_balance, params.defaultBalance.value, "Default balance matches");

    }, function timeout() {
        this.test.comment("Timeout out waiting for Preferences widget to become visible");
    });
}

function evaluateDomObject(selector) {
    var paragraph = this.evaluate
}
