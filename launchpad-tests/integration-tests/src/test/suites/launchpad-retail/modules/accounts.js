//export
exports.fetchAccountBalance = fetchAccountBalance;

fetchAccountBalance.planned = 0;

var ACCOUNTS_SELECTOR = '[id^=widget-accounts-v1]';

/**
 * Gets the balance of an account, does not perform any assertions
 * @param accountNumber
 * @returns {number}
 */
function fetchAccountBalance(accountNumber) {

	accountNumber += "";
	var accountBalance = null;


	var microdata = casper.fetchMicrodata("http://launchpad.backbase.com/vocab/retail/account");

	var item, i;
	for(i = 0; i < microdata.items.length; i++) {
		item = microdata.items[i];
		casper.test.comment(item.properties.accountNumber[0].trim());
		if(item.properties.accountNumber[0].trim() == accountNumber) {
			casper.test.comment(item.properties.accountBalance[0]);
			accountBalance = item.properties.accountBalance[0];

			//TODO: crude currency parsing, need better solution + l10n
			accountBalance = parseFloat(accountBalance.replace(/[€\s+,]/g, ""));
			break;
		}
	}
	if(!accountBalance) {
		casper.test.comment("Could not find account balance for [" + accountNumber + "]");
	}

	return accountBalance;
}