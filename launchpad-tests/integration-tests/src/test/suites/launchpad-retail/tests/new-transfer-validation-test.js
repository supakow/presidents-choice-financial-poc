var login = require('../modules/login');
var launcher = require('../../launchpad/modules/launcher-container');
var angular = require('../../launchpad/modules/angular');

var WIDGET_SELECTOR = '[id^=widget-new-transfer-v1]';
var CONTROLLER = 'NewTransferController';

casper.test.begin('TEST CASE: Test validation', function(test) {

    casper.start();

    casper.then(function() {
	    login.loginAsUser('lisa', 'password', 'Lisa Nijenhuis');
    });

    casper.then(function() {
        launcher.openWidget(WIDGET_SELECTOR);
    });

    casper.then(function(){
        angular.initialize(CONTROLLER);
    });

    casper.then(function() {
        //clear the form
        casper.click(WIDGET_SELECTOR + ' button[name="resetForm"]');
    });

    casper.then(function(){

        casper.click(WIDGET_SELECTOR + ' button[name="submitForm"]');

        casper.waitUntilVisible(WIDGET_SELECTOR + ' .alert', function(){
            casper.captureToTarget("new-transfer-validation");
            casper.test.assertVisible(WIDGET_SELECTOR + ' [lp-message="\'requiredAmount\'"]', 'Missing amount message is visible');
            casper.test.assertVisible(WIDGET_SELECTOR + ' [lp-message="\'requiredName\'"]', 'Missing name message is visible');
            casper.test.assertVisible(WIDGET_SELECTOR + ' [lp-message="\'requiredAccount\'"]', 'Missing account message is visible');
        });

    });

    casper.run(function() {
        casper.test.done();
    });

});