var login = require('../modules/login');
var launcher = require('../../launchpad/modules/launcher-container');
var angular = require('../../launchpad/modules/angular');

var WIDGET_SELECTOR = '[id^=widget-new-transfer-v1]';
var CONTROLLER = 'NewTransferController';

casper.test.begin('TEST CASE: Test validation', function(test) {

    casper.start();

    casper.then(function() {
	    login.loginAsUser('lisa', 'password', 'Lisa Nijenhuis');
    });

    casper.then(function() {
        launcher.openWidget(WIDGET_SELECTOR);
    });

    casper.then(function(){
        angular.initialize(CONTROLLER);
    });

    casper.then(function(){

        casper.click(WIDGET_SELECTOR + ' input[name="counterpartyName"]');

        var suggestionsCount = this.evaluate(function() {
            return __utils__.findAll('.lp-autosuggest-suggestion').length;
        });

        casper.fill(WIDGET_SELECTOR + ' form[name="paymentOrderForm"]', {
            'counterpartyName' : 'Shared'
        });

        casper.click('.lp-autosuggest-suggestions .lp-autosuggest-suggestion[data-index="1"]');

        casper.waitWhileVisible('.lp-autosuggest-suggestions', function(){
		
			var getIbanNumber = this.evaluate(function(selector) {
			  return __utils__.findOne(selector + ' .lp-iban-input input').value;
			}, WIDGET_SELECTOR);

            casper.test.assertField('counterpartyName', 'Shared Account', 'Should fill account name');
            casper.test.assertEquals(getIbanNumber, '56-ABNA-0519-4316-42', 'Should fill account number');
        });

        casper.click(WIDGET_SELECTOR + ' input[name="counterpartyName"]');

        casper.waitWhileVisible('.lp-autosuggest-suggestions', function(){

            casper.test.assertElementCount('.lp-autosuggest-suggestion', suggestionsCount, 'Should show all suggestions on focus');
        });

    });

    casper.run(function() {
        casper.test.done();
    });

});