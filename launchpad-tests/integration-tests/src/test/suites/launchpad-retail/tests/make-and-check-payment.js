var login = require('../modules/login');
var util = require('../modules/util');
var launcher = require('../../launchpad/modules/launcher-container');
var transfer = require("../modules/transfer");
var accounts = require("../modules/accounts");
var addressbook = require("../modules/addressbook");

var planned = 1;
planned += login.loginAsUser.planned;
planned += transfer.createPaymentOrder.planned;
planned += transfer.sendPaymentOrders.planned;
planned += addressbook.createContact.planned;

casper.test.begin('TEST CASE: Send payment order', planned, function(test) {

    var contact = {
        name: util.getRandomName(),
        account: 'NL56ABNA0519431642'
    };

	var startBalance;

	//random payment between 10 and 50
	var paymentAmount = Math.floor(Math.random() * (50 - 10 + 1) + 10);

	casper.start();

	//login
	casper.then(function() {
		login.loginAsUser('lisa', 'password', 'Lisa Nijenhuis');
        casper.thenWait(2000);
	});

	//get starting account balance
	casper.then(function() {
		casper.waitForAngularController("AccountsController", function() {
			startBalance = accounts.fetchAccountBalance("NL66INGB0280680457");
		})
	});

    //open Address book widget
    casper.then(function() {

        launcher.openWidget('[id^=widget-addressbook-v1]');

    });

    //Adding a new contact in Address Book
    casper.then(function() {
        casper.test.comment('Now adding contact: ' + contact.name) ;
        addressbook.createContact({
            contactName: contact.name,
            contactAccount: contact.account
        })
    });

    casper.thenWait(400);

	//create order
	casper.then(function() {

		launcher.openWidget('[id^=widget-new-transfer-v1]');
		transfer.createPaymentOrder({
			fromAccountIndex: 1,
			counterpartyName:  contact.name,
			ibanInputField:  contact.account,
			instructedAmount: paymentAmount
		});
	});

	//send payment orders
    casper.then(function() {

        var closeNotificationSelector = ".lp-notifications .close";
        if(casper.exists(closeNotificationSelector)) {
            casper.click(closeNotificationSelector);
        }
    });

    casper.thenWait(1000 * 2);
    casper.then(function() {
        transfer.sendPaymentOrders();
    });

    casper.thenWait(100, "Waiting for message to close");
    casper.thenWait(1000 * 20, "Waiting for notification of payment to appear");

	//check notifications banner
	casper.then(function() {

        var text = casper.fetchText('.lp-notifications .alert-info');
        casper.test.comment("Contact name: " + contact.name);
        casper.test.comment("Confirmation text: " + text);
        casper.test.assertTrue(text.indexOf(contact.name) > -1, "Counterparty name appears in order confirmation");

        //need to format amount for this to work
        //casper.test.assertTrue(text.indexOf(paymentAmount + "") > -1, "Instructed amount appears in order confirmation");

	});

//    casper.thenWait(1000, "Waiting briefly before reloading");
	//check final balance
//	casper.then(function() {
//		casper.reload(function() {
//			casper.thenWait(2000, "Waiting for accounts widget to load", function() {
//				var newBalance = accounts.fetchAccountBalance("019341743");
//				casper.test.comment("Balance was: " + startBalance);
//				casper.test.comment("New balance is " + newBalance);
//				casper.test.assertEquals(newBalance, startBalance - paymentAmount, "Balance has reduced");
//			});
//		});
//	});

	casper.run(function() {
		casper.test.done();
	});
});