(function() {
    var portalName = "launchpad-tests",
        pageName = "test-page",
        containerName = "tabbed-container-1";


    casper.test.begin("Tabbed Container", 6, {
        setUp: function() {
            casper.start();
            casper.then(function() {
                //login
                testUtil.loginAsAdmin(casper, server);

                //fresh page
                testUtil.deletePage(casper, pageName);
                testUtil.createPage(casper, portalName, pageName);

                //add a widget
                //actually add it to the existing "standardized" (by convention) Automation-Test-Page (nr1)
                testUtil.createContainer(casper, pageName, "tabbed-container", containerName, [],[{
                    name: "csr",
                    type: "boolean",
                    value: false
                }]);
            });
        },
        test: function(test) {
            casper.thenOpen(restClient.makeItemUrl(server, contextPath, portalName, "pages", pageName), function () {
                casper.wait(2000, function () {
                    casper.captureToTarget("tabbed-container-test");
                    test.assertEval(function(){
                        return $(".nav-tabs > li").length === 4; //should be eq to 4 since there is also a hidden reference tab in the container
                    }, "3 Child Tabs have been created");

                    test.assertEval(function(){
                        return $(".lp-deck-mainarea > div").length === 3;
                    }, "3 Child Panels have been created");

                    //-----------------------------------------------------
                    //click on tab #2
                    casper.click(".nav-tabs > li:nth-child(3) > a"); //should be the third child since there is also a hidden reference tab in the container
                    test.assertEval(function(){
                        return $(".nav-tabs > li:nth-child(3)").hasClass("bp-TabCont-tab-selected");
                    }, "Tab #2 displays as selected when clicked");
                    test.assertEval(function(){
                        return !$(".nav-tabs > li:nth-child(2)").hasClass("bp-TabCont-tab-selected");
                    }, "Tab #1 doesn't show as selected when #2 was clicked");

                    test.assertEval(function(){
                        return $(".lp-deck-mainarea > div:nth-child(2)").hasClass("active");
                    }, "The second panel is displayed");

                    test.assertEval(function(){
                        return !$(".lp-deck-mainarea > div:nth-child(1)").hasClass("active");
                    }, "The first pannel is not displayed anymore");


                });

            });
            //mark tests as done
            casper.run(function() {
                test.done();
            });
        },
        tearDown: function () {
            //collect garbage
            //testUtil.deletePage(casper, pageName);
        }
    });
})();
