var login = require("../modules/login");

var REPEAT_LOGIN = 2;
var planned = login.loginAsUserTimes.planned * REPEAT_LOGIN;

casper.test.begin('TEST CASE: Login with Lisa', planned, function(test) {

	casper.start();

	//login as lisa
	casper.then(function() {
		casper.test.comment("Logging in and out mulitple times")
		casper.captureToTarget("login-as-user");
		login.loginAsUserTimes("lisa", "password", "Lisa Nijenhuis", REPEAT_LOGIN);
	});

	casper.run(function() {
		test.done();
	});
});