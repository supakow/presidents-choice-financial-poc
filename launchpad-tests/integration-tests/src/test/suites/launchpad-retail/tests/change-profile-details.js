var login = require('../modules/login');
var launcher = require('../../launchpad/modules/launcher-container');
var util = require('../modules/util');
var preferences = require('../modules/preferences');

var planned = 5;
planned += login.loginAsUser.planned;
planned += preferences.assertNewPreferences.planned;
planned -= 4 //ignored

var WIDGET_SELECTOR = '[id^=widget-profile-summary]';
var WIDGET_PORTFOLIO_SELECTOR = '[id^=widget-portfolio]';
var WIDGET_PREFERENCES_SELECTOR = '[id^=widget-preferences]';

casper.test.begin('TEST CASE: Verify profile details and change the preferences', planned, function(test) {

    casper.start();

    var randomName = util.getRandomName();

    var params = {
        preferredName: randomName,
        preferredLanguage: {
            index: 3,
            value: "Dutch"
        },
        defaultAccount: {
            index: 3,
            value: "Personal Checking Account"
        },
        defaultBalance: {
            index: 2,
            value: "Use 'Available Balance'"
        }
    };

    //log in as lisa
    casper.then(function() {
        login.loginAsUser('lisa', 'password', 'Lisa Nijenhuis');
    });

    casper.then(function() {
        casper.waitForAngularController('profileSummaryController');
        casper.captureToTarget("summary");
    });
    casper.then(function() {
        casper.click(WIDGET_SELECTOR + ' a[name="myProfile"]', function() {
            casper.thenWait(1000);
        });
    });

    casper.then(function() {
        console.log('# clicked ok, new location is ' + this.getCurrentUrl());
    });

    //go to Profile page
    casper.then(function() {
        casper.thenWait(10000 * 2, "Profile page");
        casper.captureToTarget('profile page');
    });

    casper.then(function() {
        console.log('# We are on Profile page!');
    });

    casper.then(function() {
        test.assertTitleMatch(/Profile/, 'The page title is "Profile" - correct!');
    });

    casper.then(function() {
        test.assertTextExist('Lisa Nijenhuis', 'The name in Profile widget is correct');
        test.assertTextExist('Birth date: Mar 10, 1985', 'The date of birth in Profile widget is correct');
    });


    //Open Accounts Tab
    casper.then(function() {
        casper.thenClick('.bp-TabCont-head li:nth-child(2) a', function() {
            casper.wait(1000, function(){
                console.log('Waiting for Accounts Tab to load.....');
                casper.captureToTarget('accounts_tab_container');
            });

        });
    });


    //verify that the URL corresponds to Accounts tab content
    casper.then(function() {
        console.log('# clicked ok, new location is ' + this.getCurrentUrl());
    });

    //Verifying the Portfolio widget is present on the page
    casper.then(function() {
        test.assertExist(WIDGET_PORTFOLIO_SELECTOR);
        console.log('# The Portfolio widget exists on the page!')
    });

    //Open Preferences tab
    casper.then(function() {
        casper.thenClick('.bp-TabCont-head li:nth-child(3) a', function() {
            casper.wait(1000, function() {
                console.log('# Waiting for Preferences Tab to be loaded.....');
                casper.captureToTarget('preferences_tab');
            });
        });
    });

    //Checking that the user is navigated to corresponding URL '../profile/preferences'
    casper.then(function() {
        console.log('# clicked ok, new location is ' + this.getCurrentUrl());
    });

    //Verifying the Preferences widget is present on the page
    casper.then(function() {
        test.assertExist(WIDGET_PREFERENCES_SELECTOR);
        console.log('# Preferences widget exists on the page!');
    });

    //Fill out the form. Change the Preferred name, preferred language, default account and default balance.

	casper.ignore("Ignoring preference tests while global prefereces are not working", function() {
		casper.then(function() {
			preferences.editPreferences(params);
			casper.reload(function() {
				preferences.assertNewPreferences(params);
			})
		});
	});

    casper.run(function() {
        casper.test.done();
    });
});