var login = require("../modules/login");

casper.test.begin('TEST CASE: Assert accounts exist', function(test) {

    casper.start();

    //login as lisa
	login.loginAsUser('lisa', 'password', 'Lisa Nijenhuis');

    casper.waitForSelector(".lp-profile-summary-welcome", function then(){

	    this.test.assertExists(".lp-profile-summary-welcome");

	    //Assert account titles are present
	    this.test.assertExists(".lp-accounts-item .lp-accounts-item-title", "items title exists");

	    //Assert account numbers are present
	    this.test.assertExists(".lp-accounts-item  .lp-accounts-item-number", "account number exists");

	    //Assert positive balance present
	    this.test.assertExists(".lp-accounts-item  .lp-amount-positive", "positive amount exists");

	    //Assert negative balance present
	    this.test.assertExists(".lp-accounts-item  .lp-amount-negative", "negative amount exists");
    }, function timeout() {
	    this.test.comment("Timeout out waiting for profile summary welcome to be visible");
    });

    casper.run(function() {
        test.done();
    });
});