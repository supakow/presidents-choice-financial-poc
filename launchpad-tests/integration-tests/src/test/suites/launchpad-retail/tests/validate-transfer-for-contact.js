var util = require("../modules/util");
var login = require('../modules/login');
var launcher = require('../../launchpad/modules/launcher-container');
var addressbook = require('../modules/addressbook');
var transfer = require('../modules/transfer');

var REVIEWTRANSFER_SELECTOR = '[id^=widget-review-transfers-v1]';

var planned = 4;
planned += login.loginAsUser.planned;
planned += addressbook.createContact.planned * 2;
planned += addressbook.createQuickPaymentOrder.planned;
planned += transfer.sendPaymentOrders.planned;


casper.test.begin('TEST CASE: add new contact, make the transfer and check the transaction history', planned, function(test) {

    //random payment between 10 and 50
    var paymentAmount = Math.floor(Math.random() * (50 - 10 + 1) + 10);

    var contact1 = {
        name: util.getRandomName(),
        account: '75457878765'
    };
    var contact2 = {
        name: util.getRandomName(),
        account: '43634545345'
    };

    casper.start();

    //login
    casper.then(function() {
	    login.loginAsUser('lisa', 'password', 'Lisa Nijenhuis');
    });

    //open Address book widget
    casper.then(function() {

        launcher.openWidget('[id^=widget-addressbook-v1]');

    });

    //Adding 2 contacts in Address Book
    casper.then(function() {
        casper.test.comment('Now adding contact: ' + contact1.name) ;
        addressbook.createContact({
            contactName: contact1.name,
            contactAccount: contact1.account
        });
    });

	casper.then(function() {
		casper.test.comment('Now adding contact: ' + contact2.name) ;
		addressbook.createContact({
			contactName: contact2.name,
			contactAccount: contact2.account
		});
	});

    casper.then(function(){
        addressbook.selectUser(contact2.account);
        addressbook.createQuickPaymentOrder({
            fromAccountIndex: 1,
            instructedAmount: paymentAmount
        });
    });

    //Assert the payment order in Review Transfers widget
    casper.thenWait(1000);


        casper.then(function() {
            var microdata = casper.fetchMicrodata("http://launchpad.backbase.com/vocab/retail/payment-order");

            //find the order!
            microdata.items.forEach(function(order) {
                var name = order.properties.counterpartyName[0].trim();
                if(name === contact2) {
                    casper.test.assertEquals(order.properties.counterpartyName[0].trim(), counterpartyName, "Pending order's name is correct");
                    casper.test.assertEquals(order.properties.counterpartyAccount[0].trim(), counterpartyAccount, "Pending order's account is correct");
                    casper.test.assertEquals(order.properties.instructedAmount[0].trim(), "€ " + paymentAmount + ".00", "Pending order's amount is correct");
                }
            });
    });

         //submitting payment order

        casper.then(function() {
            transfer.sendPaymentOrders();
        });

    casper.thenWait(100, "Waiting for message to close");
    casper.thenWait(1000 * 20, "Waiting for notification of payment to appear");

    //check notifications banner
    casper.then(function() {

        casper.captureToTarget("make-and-check-payment--payment-notification");
        var text = casper.fetchText('.lp-notifications .alert-info');
        casper.test.comment("Contact name: " + contact2.name);
        casper.test.comment("Confirmation text: " + text);
    });

    //Make sure the payment was transferred



    casper.run(function() {
        casper.test.done();
    });
});
