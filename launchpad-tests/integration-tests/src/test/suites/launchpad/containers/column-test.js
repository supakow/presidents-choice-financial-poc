(function() {
    var portalName = "launchpad-tests";
    var pageName = "page_1369667139818";
    var containerName = "column-layout-667046";

    //begin suite
    casper.test.begin("Container tests", 1, {

        setUp: function() {
            //creates a test page and a column container
            casper.start();
        },

        test: function(test) {
            casper.thenOpen(restClient.makeItemUrl(server, contextPath, portalName, "pages", pageName));
            casper.then(function() {

                // Test column sizes
                test.assertEvalEquals(function(containerName) {
                    var areas =  document.querySelectorAll('[data-pid="' + containerName + '"] > .bp-area').length;
	                return areas;
                }, 2, 'Column number should be 2', containerName);
            });

            casper.run(function() {
                test.done();
            });
        }
    });
})();