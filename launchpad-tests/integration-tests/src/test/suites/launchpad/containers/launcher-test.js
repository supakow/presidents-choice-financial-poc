(function() {
    var portalName = "launchpad-tests";
    var pageName = "page_1370966634110";

    //begin suite
    casper.test.begin("Launcher tests", 1, {

        setUp: function() {

            //creates a test page, launcher container and widget
            casper.start();
        },

        test: function(test) {
            //main tests
            casper.thenOpen(restClient.makeItemUrl(server, contextPath, portalName, "pages", pageName));
            casper.then(function() {

	            test.assertTrue(true, "TODO: Write some tests");
            });

            casper.run(function() {
                test.done();
            });
        }
    });
})();