(function() {

    var portalName = "launchpad-tests";
    var pageName = "page_1369667116013"

    //begin suite
    casper.test.begin("Springboard tests", {

        setUp: function() {

            //creates a test page, launcher container and widget
            casper.start();
        },

        test: function(test) {
            //main tests
            casper.thenOpen(restClient.makeItemUrl(server, contextPath, portalName, "pages", pageName));
            casper.then(function() {
                test.assertEvalEquals(function() {
                    return document.querySelectorAll(".lp-springboard-container .bp-area").length;
                }, 18, "18 (6 x 3) areas");
            });

            casper.run(function() {
                test.done();
            });
        },
        tearDown: function() {
            testUtil.deletePage(casper, pageName);
        }
    });

})();