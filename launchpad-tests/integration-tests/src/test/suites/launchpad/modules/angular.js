/**
 * Test to initialize AngularJS controller
 * @param controller
 */
exports.initialize = function(controller) {
    casper.waitUntilVisible('[ng-controller="' + controller + '"]', function(){
	    casper.test.comment("Angular controller is ready");
    });
};