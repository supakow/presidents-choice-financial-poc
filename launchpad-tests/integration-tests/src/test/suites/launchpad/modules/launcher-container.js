/**
 * Test to open widget in Launcher container as a resable component
 * @param widgetSelector
 */
exports.openWidget = function(widgetSelector){
    casper.waitUntilVisible(widgetSelector, function(){
	    casper.test.comment("Opening tab");
        casper.click(widgetSelector + " .lp-launcher-tab");

        casper.waitUntilVisible(widgetSelector + " .lp-launcher-tab-widget", function(){
	        casper.test.comment("Tab opened for " + widgetSelector);
        });
    });
};