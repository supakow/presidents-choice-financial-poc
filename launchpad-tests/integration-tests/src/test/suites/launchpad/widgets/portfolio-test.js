casper.test.begin('Assert portfolio product details are visbile', function(test) {

	var portalName = "launchpad-tests", pageName = "page_1386851168484";

	casper.start(restClient.makeItemUrl(server, contextPath, portalName, "pages", pageName), function then () {
		casper.waitUntilVisible(".lp-portfolio", function then(){
			//Assert the Portfolio Container exists
		    this.test.assertExists(".lp-portfolio-container", "Container exists");

		    //Assert that a portfolio product header exists
		    this.test.assertExists(".lp-portfolio-product-header", "Header exists");

		    //Assert that a portfolio product details div exists
		    this.test.assertExists(".lp-portfolio-product", "Product div exists");

		   	//Assert that a portfolio product details row exists
		    this.test.assertExists(".lp-portfolio-product-detail", "Product property detail row exists");

		    //Assert that a product property label exists
		    this.test.assertExists(".lp-portfolio-product-label", "Product property label cell exists");

		    //Assert that a product property value row exists
		    this.test.assertExists(".lp-portfolio-product-value", "Product property value cell exists");
		    
		    //Assert that a related links area exists
		    this.test.assertExists(".lp-product-related-links", "Product related links exists");

		    //Ensure that there are four products from the Dummy JSON
		    this.test.assertElementCount('.lp-portfolio-product-header', 7);

	    }, function timeout() {
		    this.test.comment("Timeout out waiting for Products widget to be visible");
	    });
	});

	casper.thenWait(2000, "Content loading");

	casper.then(function() {
		//Test available microdata from markup
		var microdata = casper.fetchMicrodata("http://launchpad.backbase.com/vocab/retail/account-details");
		var firstProductName, firstProductLabel, firstProductValue, firstProductLink;
		for(var i = 0; i < microdata.items.length; i++) {
			var item = microdata.items[i];
			firstProductName = item.properties.product[0].trim(); //Personal Checking Account
			firstProductLabel = item.properties.conditionName[0].trim(); //Beneficiaries:
			firstProductValue = item.properties.conditionValue[0].trim(); // Thomas A. Anderson
			firstProductLink = item.properties.links[0].trim(); //Edit preferences
		}

		this.test.assertEquals(firstProductName, "Personal Checking Account");

		this.test.assertEquals(firstProductLabel, "Beneficiaries:");

		this.test.assertEquals(firstProductValue, "Thomas A. Anderson");

		this.test.assertEquals(firstProductLink, "[my-ref-to-message-key]");
	});

	casper.run(function() {
		test.done();
	});

});
