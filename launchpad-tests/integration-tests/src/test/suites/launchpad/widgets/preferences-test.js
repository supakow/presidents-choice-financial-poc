casper.test.begin('Assert Widget Preferences form is available', function(test) {

	var portalName = "launchpad-tests", pageName = "page_1387270687558";

	casper.start(restClient.makeItemUrl(server, contextPath, portalName, "pages", pageName), function then () {
		casper.waitUntilVisible(".lp-preferences", function then(){
		    var microdata = casper.fetchMicrodata("http://launchpad.backbase.com/vocab/foundation/widget-preferences");
			var name_field, language_field, default_view_field, default_account_field, masking_field, default_balance;
			for(var i = 0; i < microdata.items.length; i++) {
				var item = microdata.items[i];
				name_field = item.properties.name_field;
				language_field = item.properties.language_field;
				default_view_field = item.properties.default_view_field;
				default_account_field = item.properties.default_account_field;
				masking_field = item.properties.masking_field;
				default_balance = item.properties.default_balance;
			}

			this.test.assert(name_field[0].length > 0, "Name field available");

			this.test.assert(language_field[0].length > 0, "Language field available");

			this.test.assert(default_view_field[0].length > 0, "Default view field available");

			this.test.assert(default_account_field[0].length > 0, "Default account field available");

			this.test.assert(masking_field[0].length > 0, "Masking field available");

			this.test.assert(default_balance[0].length > 0, "Default balance field available");

	    }, function timeout() {
		    this.test.comment("Timeout out waiting for Preferences widget to become visible");
	    });
	});

	casper.then(function() {
		
	});

	casper.run(function() {
		test.done();
	})

});
